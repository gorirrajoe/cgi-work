<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package velonews
 */

?><!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<?php
			// code to add after opening <head> tag
			$in_head_section = cgi_bikes_get_option( 'in_head_section' );
			if( $in_head_section AND !( empty( $in_head_section ) ) ) {
				echo $in_head_section;
			}
		?>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

		<?php
			// favicon
			vn_display_favicon();

			// datalayer
			get_datalayer();

			// google analytics
			get_googleGA();

			// wordpress action hook
			wp_head();

			$publisher_key = cgi_bikes_get_option( 'social_media_publisher' );

		?>
		<script type="text/javascript">
			(function () {
				var s = document.createElement('script'),
					x = document.getElementsByTagName('script')[0];
				s.type = 'text/javascript';
				s.async = true;
				s.src = ('https:' == document.location.protocol ? 'https://s' : 'http://i')
					+ '.po.st/static/v4/post-widget.js#publisherKey=<?php echo $publisher_key; ?>';
				x.parentNode.insertBefore(s, x);

				window.pwidget_config = {
					shareQuote: false,
					onshare:bk_addSocialChannel,
					defaults: {
						gaTrackSocialInteractions: true,
						retina: true,
						mobileOverlay: true,
						afterShare: false,
						sharePopups: true,
						copypaste: false
					}
				};

				//Populate BlueKai social sharer
				function bk_addSocialChannel(channelName) {
					bk_addPageCtx('share', channelName);
					BKTAG.doTag(39226,1);
				}

			})();
		</script>


		<?php
			// code to add before closing </head> tag
			$before_closing_head = cgi_bikes_get_option( 'before_closing_head' );
			if( $before_closing_head AND !( empty( $before_closing_head ) ) ) {
				echo $before_closing_head;
			}

		?>
	</head>


	<body <?php body_class(); ?>>
		<?php
			// code to add after opening body tag
			$after_opening_body = cgi_bikes_get_option( 'after_opening_body' );
			if( $after_opening_body AND !( empty( $after_opening_body ) ) ) {
				echo $after_opening_body;
			}
		?>


		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'velonews' ); ?></a>


		<?php
			/**
			 * header
			 */
		?>
		<header class="header" id="header">

			<?php eu_regulation(); ?>

			<div class="container header__container">
				<div class="row">
					<div class="col-xs-6 col-sm-2 header__left">
						<h1 class="screen-reader-text">Velo News</h1>
						<div class="header__logo">
							<a href="<?php echo site_url( '/' ); ?>">
								<?php if( cgi_bikes_get_option( 'site_logo' ) != '' ) {
									$sitelogo = cgi_bikes_get_option( 'site_logo' );
								} else {
									$sitelogo = get_bloginfo( 'stylesheet_directory' ) . '/images/velonews-logo.svg';
								} ?>
								<img alt="<?php echo get_bloginfo( 'name' ); ?> | <?php echo get_bloginfo( 'description' ); ?>" src="<?php echo $sitelogo; ?>">
							</a>
						</div>
					</div>
					<div class="col-xs-6 col-sm-10 header__right">

						<?php if( has_nav_menu( 'primary' ) ) : ?>

							<nav id="main-nav" class="clearfix main-nav">
								<ul class="clearfix main-nav__menu" role="menubar">

									<?php
										$navargs = array(
											'menu'				=> 'primary',
											'menu_id'			=> '',
											'menu_class'		=> '',
											'walker'			=> new VeloNews_MegaMenu_Walker(),
											'echo'				=> true,
											'container'			=> '',
											'container_id'		=> '',
											'fallback_cb'		=> false,
											'theme_location'	=> 'primary',
											'container_class'	=> '',
											'items_wrap'		=> '%3$s'
										);
										wp_nav_menu( $navargs );
									?>

									<li class="main-nav__menu-item header__search" role="menuitem" aria-haspopup="true">
										<a href="#" target="_self"><i class="fa fa-search"></i></a>
										<ul class="main-nav__sub-menu" role="menu" aria-hidden="true">
											<li class="main-nav__sub-menu-item" role="menuitem">
												<?php echo get_search_form( false ) ?>
											</li>
										</ul>
									</li>

								</ul>
							</nav>

						<?php endif; ?>

						<nav id="social-nav" class="clearfix social-nav">
							<ul class="clearfix social-nav__menu">
								<li class="social-nav__menu-item">
									<a href="<?php echo cgi_bikes_get_option( 'twitter_link' ); ?>" target="_blank">
										<span class="screen-reader-text">VeloNews Twitter</span>
										<span class="fa fa-twitter"></span>
									</a>
								</li>
								<li class="social-nav__menu-item">
									<a href="<?php echo cgi_bikes_get_option( 'facebook_link' ); ?>" target="_blank">
										<span class="screen-reader-text">VeloNews Facebook</span>
										<span class="fa fa-facebook"></span>
									</a>
								</li>
								<li class="social-nav__menu-item">
									<a href="<?php echo cgi_bikes_get_option( 'instagram_link' ); ?>" target="_blank">
										<span class="screen-reader-text">VeloNews Instagram</span>
										<span class="fa fa-instagram"></span>
									</a>
								</li>
								<li class="social-nav__menu-item">
									<a href="<?php echo cgi_bikes_get_option( 'youtube_link' ); ?>" target="_blank">
										<span class="screen-reader-text">VeloNews YouTube</span>
										<span class="fa fa-youtube-play"></span>
									</a>
								</li>
							</ul>
						</nav>

					</div>
				</div>
			</div>
		</header>

		<?php
			/**
			 * alert bar - controlled thru options page
			 */
			if ( cgi_bikes_get_option( 'ticker_on' ) == 'on' ) {
				get_template_part( 'template-parts/content', 'alert-bar' );
			}


			/**
			 * race leaderboard - choose a race in options page
			 */
			if( cgi_bikes_get_option( 'leaders_on' ) == 'on' ) {
				get_leaderboard();
			}

		?>


		<section class="advert advert_xs_300x250 advert_sm_728x90 advert_md_970x250 advert_location_top">
			<div class="advert__wrap" id="leaderboard">
				<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'top' ) : ''; ?>
			</div>
		</section>
