<?php
	/**
	 * The template for displaying all single posts.
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
	 *
	 * @package velonews
	 */
	get_header();
	global $multipage;

	$no_sidebar = ( in_category( array( '2015-buyers-guide', '2016-buyers-guide', '2016-gift-guide', 'velonews-gift-guide' ) ) ) ? true : false;

	$sponsorship 	= get_presented_by();
	$in_guide 		= false;
	$cats 			= get_the_category( $id );

	//check if in gift guide or buyers guide after 2017, set in_guide true
	foreach ( $cats as $key => $value ) {
		if( strpos( $value->slug, 'guide' ) != false ) {
			//get the year for the guide
			preg_match_all('!\d+!', $value->slug, $guide_year);
			$guide_year = $guide_year[0][0];
			//start with 2017 gift guide and 2018 buyers guide
			if( ( $guide_year > 2016 && strpos($value->slug, 'gift') != false ) || ( $guide_year > 2017 && strpos( $value->slug, 'buy' ) != false ) ) {

				$in_guide = true;
				$cat_array[$value->slug] = $value->name;
				$guide_slug = current( array_keys( $cat_array ) );
				$guide_name = $cat_array[$guide_slug];
				$guide_link	=  get_category_link( get_category_by_slug( $guide_slug ) );

			}
		}
	}


?>

<section id="content" class="content template--single<?php echo $sponsorship['sponsorclass']; ?>">
	<div class="container content__container">
		<div class="row">

			<?php
				while ( have_posts() ) : the_post();

					if( get_post_type() == 'bike-reviews' ) {

						get_template_part( 'template-parts/content', 'bike-review' );

					} elseif( get_post_type() == 'apparel-reviews' ) {

						get_template_part( 'template-parts/content', 'apparel-review' );

					} elseif( in_category( 'video' ) ) {

						get_template_part( 'template-parts/content', 'old-video' );

					} elseif( ( in_category( array( 'buyers-guide', 'gift-guide' ) ) )&& !in_category( array('2015-buyers-guide', '2016-buyers-guide', '2017-buyers-guide', 'velonews-gift-guide', '2016-gift-guide' ) ) ) {

						get_template_part( 'template-parts/content', 'buyers-guide-final-single' );

					} elseif( in_category( '2015-buyers-guide' ) ) {

						get_template_part( 'template-parts/content', '2015-buyers-guide-single' );

					} elseif( in_category( '2016-buyers-guide' ) ) {

						get_template_part( 'template-parts/content', '2016-buyers-guide-single' );

					} elseif( in_category( '2017-buyers-guide' ) ) {

						get_template_part( 'template-parts/content', '2017-buyers-guide-single' );

					} elseif( in_category( 'velonews-gift-guide' ) ) {

						get_template_part( 'template-parts/content', '2015-gift-guide-single' );

					} elseif( in_category( '2016-gift-guide' ) ) {

						get_template_part( 'template-parts/content', '2016-gift-guide-single' );

					} elseif( $multipage ) {

						get_template_part( 'template-parts/content', 'multipage' );

					} elseif( get_post_meta( get_the_ID(), 'legacy_list_status', 1 ) == 'on' ) {

						get_template_part( 'template-parts/content', 'legacy-list' );

					} elseif( get_post_meta( get_the_ID(), '_liveblog', 1 ) == 'on' ) {

						get_template_part( 'template-parts/content', 'liveblog' );

					} else {

						get_template_part( 'template-parts/content', get_post_format() );

					}

				endwhile; // End of the loop.

				if( !$no_sidebar ) {
					get_sidebar( 'right' );
				}
			?>

		</div>
	</div>
</section>


<?php
	if( !$no_sidebar && !$in_guide) {

		/**
		 * Related articles based on category
		 * Using the shortcode for related with a 'article-footer' parameter
		 */
		echo do_shortcode( '[related title="Related Articles" location="article-footer"]' );

		/**
		 * outbrain stuffs
		 */
		echo get_outbrain();

	}
	if( $in_guide ){
		get_template_part( 'template-parts/content', 'buyers-guide-final-related' );
	}


/**
* ad
*/

?>
<section class="advert advert_xs_300x250 advert_sm_728x90 advert_location_bottom ">
	<div class="advert__wrap">
		<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom' ) : ''; ?>
	</div>
</section>

<?php get_footer(); ?>
