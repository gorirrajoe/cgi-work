<?php
/**
 * The template for displaying Race Overview pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package velonews
 */
global $post_id_array;
$post_id_array		= array();

$multistage			= ( get_post_meta( get_the_id(), '_vn_race_multistage', 1 ) ? true : false );
$sponsorship		= get_presented_by();
$race_category		= wp_get_post_terms( get_the_ID(), 'stagecat' )[0]->slug;
$racecat_short_name	= str_replace( '-race', '', $race_category );
$cat_id				= get_category_by_slug( $racecat_short_name )->term_id;

get_header();

if( $multistage ) { ?>

	<section id="content" class="content template--multi-stage<?php echo $sponsorship['sponsorclass']; ?>">

		<div class="container content__container">

			<!-- Title for Race overview -->
			<div class="row">
				<div class="col-xs-12">
					<?php get_breadcrumb(); ?>
				</div>
				<header class="col-xs-12">
					<h1 class="content__title"><?php echo get_the_title( ); ?></h1>
					<?php echo $sponsorship['content']; ?>
				</header>
			</div>

			<div class="row content__flex">

				<section class="content__section content__flex_position_first news-feed">
					<?php get_sidebar( 'left' ); ?>
				</section>

				<section class="content__section content__flex_position_second">

					<?php
						while ( have_posts() ) : the_post();
							include( locate_template( 'template-parts/content-race-stages.php' ) );
						endwhile; // End of the loop.
					?>

				</section>

				<section class="content__section content__flex_position_third">
					<?php
						include( locate_template( 'template-parts/content-one.php' ) );


						/**
						 * ad: 300x600
						 */
					?>
					<section class="col-xs-12 hidden-xs advert advert_sm_300x600">
						<div class="advert__wrap">
							<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'side-top' ) : ''; ?>
						</div>
					</section>

				</section>
			</div>

			<?php
				/**
				 * get the race category's originating category name and pull in content based on that
				 * check to see if the option is checked ON first
				 */

				if( get_term_meta( $cat_id, '_vn_four-across-top-enable', 1 ) == 'on' ) {
					/**
					 * four column
					 */
					include( locate_template( 'template-parts/content-four-top.php' ) );
				}

				if( get_term_meta( $cat_id, '_vn_two-across-enable', 1 ) == 'on' ) {
					/**
					 * two column - featured articles (rich media?)
					 */
					include( locate_template( 'template-parts/content-two.php' ) );
				}

				if( get_term_meta( $cat_id, '_vn_five-across-enable', 1 ) == 'on' ) {
					/**
					 * five column
					 */
					include( locate_template( 'template-parts/content-five.php' ) );
				}

				if( get_term_meta( $cat_id, '_vn_two-by-two-enable', 1 ) == 'on' ) {
					/**
					 * two by two
					 */
					include( locate_template( 'template-parts/content-two-by-two.php' ) );
				}

				if( get_term_meta( $cat_id, '_vn_four-across-bottom-enable', 1 ) == 'on' ) {
					/**
					 * four column
					 */
					include( locate_template( 'template-parts/content-four-bottom.php' ) );
				}
			?>
		</div>

	</section>

	<section class="advert advert_xs_300x250 advert_sm_728x90 advert_location_bottom ">
		<div class="advert__wrap">
			<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom' ) : ''; ?>
		</div>
	</section>

<?php } else { ?>

	<section id="content" class="content archive template--single-race">

		<div class="container content__container">

			<header class="row">

				<div class="col-xs-12">
					<?php get_breadcrumb(); ?>
				</div>

				<div class="col-xs-12">
					<h1 class="content__title"><?php echo get_the_title(); ?></h1>
				</div>

			</header>

			<?php get_template_part('template-parts/content-race-single' );?>

		</div><!-- #content__container -->

	</section><!-- #template--single-stage -->

<?php } ?>

<?php get_footer(); ?>
