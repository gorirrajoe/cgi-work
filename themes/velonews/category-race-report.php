<?php
/**
 * Template Name: Race Report Category
 *
 * @package velonews
 */
get_header();

$cat_obj		= get_category( get_query_var( 'cat' ) );
$cat_id			= ( isset( $cat_obj->term_id ) ? $cat_obj->term_id : '' );
$cat_nicename	= ( isset( $cat_obj->category_nicename ) ? $cat_obj->category_nicename : '' );
$prefix			= '_vn_';
$post_id_array	= array();
$cat_url		= get_category_link( $cat_id );
$archive_type	= get_term_meta( $cat_id, $prefix . 'archive_type', 1 );
$sponsorship	= get_presented_by();

$page = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

if( $page == 1 ) {
	/**
	 * default layout = blogroll
	 */
	if( $archive_type == '' || $archive_type == 'standard' ) { ?>
		<section id="content" class="content archive template--blogroll<?php echo $sponsorship['sponsorclass']; ?>">
			<div class="container content__container">
				<header class="row">
					<div class="col-xs-12">
						<?php
							/**
							 * breadcrumb
							 */
							get_breadcrumb();
						?>
					</div>
					<div class="col-xs-12">
						<?php
							the_archive_title( '<h1 class="content__title">', '</h1>' );

							echo $sponsorship['content'];

							the_archive_description( '<div class="content__description">', '</div>' );
						?>
					</div>
				</header>


				<div class="row">
					<section class="col-xs-12 col-sm-7 col-md-8 archive">
						<?php
							/**
							 * modify the query so it displays any post type categorized as "Race Report"
							 */
							$args = array(
								'cat'						=> $cat_obj->term_id,
								'post_type'					=> 'any',
								'posts_per_page'			=> 10,
								'paged'						=> $page,
								'post_status'				=> 'publish',
								'ignore_sticky_posts'		=> true,
								'no_found_rows'				=> false,
								'update_post_meta_cache'	=> false
							);
							$cat_query = new WP_Query( $args );

							if ( $cat_query->have_posts() ) {
								$count		= 1;

								while ( $cat_query->have_posts() ) {
									$cat_query->the_post();

									get_template_part( 'template-parts/content', 'archive' );

									if( $count == 3 ) { ?>

										<div class="advert advert_xs_300x250 advert_md_728x90 advert_location_inline">
											<div class="advert__wrap">
												<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'middle-archive' ) : ''; ?>
											</div>
										</div><!-- ad unit -->

										<?php
									}
									$count++;

								}

								echo get_paginated_links();

							} else {

								get_template_part( 'template-parts/content', 'none' );

							}
						?>
					</section>
					<?php get_sidebar( 'right' ); ?>
				</div>
			</div>
		</section>

	<?php } else {
		/**
		 * extended layout (similar to homepage)
		 */ ?>
		<section id="content" class="content template--extended<?php echo $sponsorship['sponsorclass']; ?>">
			<div class="container content__container">
				<div class="row">
					<div class="col-xs-12">
						<?php
							/**
							 * breadcrumb
							 */
							get_breadcrumb();
						?>
					</div>
					<div class="col-xs-12">
						<h1 class="content__title"><?php echo $cat_obj->name; ?></h1>
						<?php
							echo $sponsorship['content'];

							the_archive_description( '<div class="content__description">', '</div>' );
						?>
					</div>
				</div>

				<div class="row content__flex">

					<?php
						/**
						 * news feed
						 */
					?>
					<section class="content__section content__flex_position_first news-feed">
						<?php get_sidebar( 'left' ); ?>
					</section>


					<?php if( get_term_meta( $cat_id, $prefix . 'archive_type', 1 ) == 'multistage' ) {
						include( locate_template( 'template-parts/content-race-stages.php' ) );
					} elseif( get_term_meta( $cat_id, $prefix . 'archive_type', 1 ) == 'extended' ) {
						include( locate_template( 'template-parts/content-top-five.php' ) );
					} ?>


					<section class="content__section content__flex_position_third">
						<?php
							/**
							 * one column (featured post)
							 * if nothing was set in the category edit page, use the one set in velo options
							 */
							echo '<!-- featured top right -->';
							include( locate_template( 'template-parts/content-one.php' ) );


							/**
							 * ad: 300x600
							 */
						?>
						<section class="col-xs-12 hidden-xs advert advert_sm_300x600">
							<div class="advert__wrap">
								<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'side-top' ) : ''; ?>
							</div>
						</section>


						<?php
							/**
							 * two column (tablet)
							 */
							echo '<!-- two column (responsive) -->';
							include( locate_template( 'template-parts/content-two-homepage.php' ) );
						?>


					</section>
				</div>


				<?php
					/**
					 * four column
					 */
					if( get_term_meta( $cat_id, $prefix . 'four-across-top-enable', 1 ) == 'on' ) {
						echo '<!-- four column -->';
						include( locate_template( 'template-parts/content-four-top.php' ) );
					}


					/**
					 * 970x250 ad
					 */
				?>
				<div class="row content__section">
					<div class="advert advert_xs_300x250 advert_sm_728x90 advert_md_970x250">
						<div class="advert__wrap">
							<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'middle' ) : ''; ?>
						</div>
					</div>
				</div>


				<?php
					/**
					 * two column - featured articles (rich media?)
					 */
					if( get_term_meta( $cat_id, $prefix . 'two-across-enable', 1 ) == 'on' ) {
						echo '<!-- two column -->';
						include( locate_template( 'template-parts/content-two.php' ) );
					}


					/**
					 * five column
					 */
					if( get_term_meta( $cat_id, $prefix . 'five-across-enable', 1 ) == 'on' ) {
						echo '<!-- five column -->';
						include( locate_template( 'template-parts/content-five.php' ) );
					}


					/**
					 * two by two
					 */
					if( get_term_meta( $cat_id, $prefix . 'two-by-two-enable', 1 ) == 'on' ) {
						echo '<!-- two by two column -->';
						include( locate_template( 'template-parts/content-two-by-two.php' ) );
					}


					/**
					 * four column
					 */
					if( get_term_meta( $cat_id, $prefix . 'four-across-bottom-enable', 1 ) == 'on' ) {
						echo '<!-- four bottom column -->';
						include( locate_template( 'template-parts/content-four-bottom.php' ) );
					}

				?>

			</div>
		</section>

	<?php }
} else { ?>
	<section id="content" class="content archive template--blogroll<?php echo $sponsorship['sponsorclass']; ?>">
		<div class="container content__container">
			<header class="row">
				<div class="col-xs-12">
					<?php
						/**
						 * breadcrumb
						 */
						get_breadcrumb();
					?>
				</div>
				<div class="col-xs-12">
					<?php
						the_archive_title( '<h1 class="content__title">', '</h1>' );

						echo $sponsorship['content'];

						the_archive_description( '<div class="content__description">', '</div>' );
					?>
				</div>
			</header>


			<div class="row">
				<section class="col-xs-12 col-sm-7 col-md-8 archive">
					<?php
						/**
						 * modify the query so it displays any post type categorized as "Race Report"
						 */
						$args = array(
							'cat'						=> $cat_obj->term_id,
							'post_type'					=> 'any',
							'posts_per_page'			=> 10,
							'paged'						=> $page,
							'post_status'				=> 'publish',
							'ignore_sticky_posts'		=> true,
							'no_found_rows'				=> false,
							'update_post_meta_cache'	=> false
						);
						$cat_query = new WP_Query( $args );

						if ( $cat_query->have_posts() ) {
							$count		= 1;

							while ( $cat_query->have_posts() ) {
								$cat_query->the_post();

								get_template_part( 'template-parts/content', 'archive' );

								if( $count == 3 ) { ?>

									<div class="advert advert_xs_300x250 advert_md_728x90 advert_location_inline">
										<div class="advert__wrap">
											<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'middle-archive' ) : ''; ?>
										</div>
									</div><!-- ad unit -->

									<?php
								}
								$count++;

							}

							echo get_paginated_links();

						} else {

							get_template_part( 'template-parts/content', 'none' );

						}
					?>
				</section>
				<?php get_sidebar( 'right' ); ?>
			</div>
		</div>
	</section>
<?php }


/**
 * ad
 */ ?>
<section class="advert advert_xs_300x250 advert_sm_728x90 advert_location_bottom ">
	<div class="advert__wrap">
		<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom' ) : ''; ?>
	</div>
</section>


<?php get_footer();
