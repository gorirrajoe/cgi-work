<?php
	/**
	 * Magazine Category Template
	 *
	 * @package velonews
	 */

	get_header();

	$cat_obj		= get_category( get_query_var( 'cat' ) );
	$cat_id			= $cat_obj->term_id;
	$cat_nicename	= $cat_obj->category_nicename;
	$prefix			= '_vn_';
?>

<section id="content" class="content template--single">
	<div class="container content__container">
		<div class="row">

			<article id="article" class="col-xs-12 col-sm-7 col-md-8 article">

				<?php
					/**
					 * breadcrumb
					 */
					get_breadcrumb();
				?>

				<header class="article__header">
					<?php
						the_archive_title( '<h1 class="article__header__title">', '</h1>' );
						the_archive_description( '<div class="content__description">', '</div>' );
					?>
				</header>


				<section class="article__body article__magazine-archive row">

					<?php
						/**
						 * social sharing
						 */
						get_social_sharing( 'side' );

						echo '<div id="article-right" class="col-xs-12 col-md-11">';

							get_social_sharing( 'top' );

							if ( have_posts() ) {
								$count = 0;
								echo '<div class="row">';

									while ( have_posts() ) {
										the_post();

										if( ( $count % 3 ) == 0 && ( $count != 0 ) ) {
											echo '</div><div class="row">';
										}
										get_template_part( 'template-parts/content', 'magazine' );

										$count++;
									}
								echo '</div>';

								echo get_paginated_links();

							} else {

								get_template_part( 'template-parts/content', 'none' );

							}

						echo '</div>';

					?>
				</section>
			</article>
			<?php get_sidebar( 'right' ); ?>
		</div>
	</div>
</section>

<?php
	/**
	 * ad
	 */
	?>
	<section class="advert advert_xs_300x250 advert_sm_728x90 advert_location_bottom ">
		<div class="advert__wrap">
			<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom' ) : ''; ?>
		</div>
	</section>


<?php get_footer();
