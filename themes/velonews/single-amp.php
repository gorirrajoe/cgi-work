<?php
	global $multipage;

	$twitter	= cgi_bikes_get_option( 'twitter_link' );
	$facebook	= cgi_bikes_get_option( 'facebook_link' );
	$instagram	= cgi_bikes_get_option( 'instagram_link' );
	$youtube	= cgi_bikes_get_option( 'youtube_link' );
?>
<!doctype html>
<html amp <?php echo AMP_HTML_Utils::build_attributes_string( $this->get( 'html_tag_attributes' ) ); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php do_action( 'amp_post_template_head', $this ); ?>
	<style amp-custom><?php do_action( 'amp_post_template_css', $this ); ?></style>
</head>

<body <?php body_class('amp'); ?>>

	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'velonews' ); ?></a>

	<header class="header">
		<div class="container header__container">
			<div class="row">
				<div class="col-xs-6">
					<div class="header__logo">
						<a href="<?php echo site_url( '/' ); ?>">
							<?php if ( cgi_bikes_get_option( 'site_logo' ) != '' ) {
								$sitelogo = cgi_bikes_get_option( 'site_logo' );
							} else {
								$sitelogo = get_bloginfo( 'stylesheet_directory' ) . '/images/velonews-logo.svg';
							} ?>
							<amp-img src="<?php echo $sitelogo; ?>" alt="VeloNews Logo" height="25" width="135"></amp-img>
						</a>
					</div>
				</div>
				<div class="col-xs-6">
					<nav id="social-nav" class="social-nav">
						<ul class="social-nav__menu">
							<?php if( !empty( $twitter ) ): ?>
								<li class="social-nav__menu-item">
									<a href="<?php echo $twitter; ?>" target="_self">
										<span class="screen-reader-text">VeloNews Twitter</span>
										<span class="fa fa-twitter"></span>
									</a>
								</li>
							<?php endif; ?>
							<?php if( !empty( $facebook ) ): ?>
							<li class="social-nav__menu-item">
								<a href="<?php echo $facebook; ?>" target="_self">
									<span class="screen-reader-text">VeloNews Facebook</span>
									<span class="fa fa-facebook"></span>
								</a>
							</li>
							<?php endif; ?>
							<?php if( !empty( $instagram ) ): ?>
							<li class="social-nav__menu-item">
								<a href="<?php echo $instagram; ?>" target="_self">
									<span class="screen-reader-text">VeloNews Instagram</span>
									<span class="fa fa-instagram"></span>
								</a>
							</li>
							<?php endif; ?>
							<?php if( !empty( $youtube ) ): ?>
							<li class="social-nav__menu-item">
								<a href="<?php echo $youtube; ?>" target="_self">
									<span class="screen-reader-text">VeloNews YouTube</span>
									<span class="fa fa-youtube-play"></span>
								</a>
							</li>
							<?php endif; ?>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>

	<section id="content" class="content template--single">
		<div class="container content__container">
			<div class="row">

				<?php
					if ( have_posts() ) : while( have_posts() ) : the_post();

						if ( in_category( 'video' )) {
							include( locate_template( 'template-parts/amp-old-video.php' ) );
							// get_template_part( 'template-parts/amp', 'old-video' );
						} elseif ( $multipage ) {
							include( locate_template( 'template-parts/amp-multipage.php' ) );
							// get_template_part( 'template-parts/amp', 'multipage' );
						} else {
							include( locate_template( 'template-parts/amp.php' ) );
							// get_template_part( 'template-parts/amp', get_post_format() );
						}

					endwhile; endif; // End of the loop.

					echo do_shortcode( '[related title="Related Articles" location="article-footer"]' );
				?>

			</div>
		</div>
	</section>

	<footer class="footer">
		<div class="footer__copyright">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 ">
						<p>&copy; <?php echo date( 'Y' );?> POCKET OUTDOOR MEDIA, LLC. ALL RIGHTS RESEVED.</p>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<?php do_action( 'amp_post_template_footer', $this ); ?>

</body>
</html>
