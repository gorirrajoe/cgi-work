<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package velonews
 */

get_header(); ?>

	<section id="content" class="content archive template--blogroll">
		<div class="container content__container">
			<header class="row">
				<div class="col-xs-12">
					<?php
						/**
						 * breadcrumb
						 */
						get_breadcrumb();
					?>
				</div>

				<?php
					/**
					 * different treatment for author archives
					 */
					if( is_author() ) {
						$displayname	= get_the_author_meta( 'display_name' );
						$gravatar		= get_avatar( get_the_author_meta( 'ID' ), 120, '', $author_name );
						$avatarurl		= get_the_author_meta( '_vn_avatar' );
						$avatarid		= get_attachment_id_from_src( $avatarurl );
						$avatarsized	= wp_get_attachment_image_src( $avatarid, 'thumbnail' );
						$bio			= apply_filters( 'the_content', get_the_author_meta( 'description' ) );
						$twitter		= get_the_author_meta( '_vn_twitter' );
						$facebook		= get_the_author_meta( '_vn_facebook' );
						$instagram		= get_the_author_meta( '_vn_instagram' );
						$pinterest		= get_the_author_meta( '_vn_pinterest' );
						$googleplus		= get_the_author_meta( '_vn_google_plus' );

						echo '<div class="col-xs-12 content__author">
							<div class="content__author__thumbnail">';

								if( $avatarsized[0] ) {
									$author_avatar = '<img src="'. $avatarsized[0] .'" alt="'. $displayname .'">';
								} else {
									$author_avatar = $gravatar;
								}
								echo $author_avatar;

							echo '</div>

							<div class="content__author__bio">
								<h1 class="content__author__name">'. $displayname .'</h1>
								<div class="content__author__description">
									'. $bio .'
								</div>
								<ul class="content__author__social">';
									if( $twitter ) {
										echo '<li>
											<a href="'. $twitter .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Twitter</span><span class="fa fa-twitter"></span></a>
										</li>';
									}
									if( $facebook ) {
										echo '<li>
											<a href="'. $facebook .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Facebook</span>
											<span class="fa fa-facebook"></span></a>
										</li>';
									}
									if( $instagram ) {
										echo '<li>
											<a href="'. $instagram .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Instagram</span>
											<span class="fa fa-instagram"></span></a>
										</li>';
									}
									if( $pinterest ) {
										echo '<li>
											<a href="'. $pinterest .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Instagram</span>
											<span class="fa fa-pinterest-p"></span></a>
										</li>';
									}
									if( $googleplus ) {
										echo '<li>
											<a href="'. $googleplus .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Instagram</span>
											<span class="fa fa-google-plus"></span></a>
										</li>';
									}
								echo '</ul>
							</div>
						</div>';

					} else { ?>
						<div class="col-xs-12">
							<?php
								the_archive_title( '<h1 class="content__title">', '</h1>' );

								$sponsorship = get_presented_by();
								echo $sponsorship['content'];

							?>
						</div>
					<?php }
				?>
			</header>


			<div class="row">
				<section class="col-xs-12 col-sm-7 col-md-8 archive">
					
					<?php

						$tag_description = tag_description();
						$tag_description = '<div class="content__description">' . $tag_description . '</div>';
						echo apply_filters('the_content', $tag_description);

					?>

					<?php if ( have_posts() ) {
						$count		= 1;

						while ( have_posts() ) {
							the_post();

							if( is_post_type_archive( array( 'bike-reviews', 'apparel-reviews' ) ) ) {
								get_template_part( 'template-parts/content', 'archive-product' );
							} else {
								get_template_part( 'template-parts/content', 'archive' );

								if( $count == 3 ) { ?>

									<div class="advert advert_xs_300x250 advert_md_728x90 advert_location_inline">
										<div class="advert__wrap">
											<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'middle-archive' ) : ''; ?>
										</div>
									</div><!-- ad unit -->

									<?php
								}
								$count++;

							}

						}

						echo get_paginated_links();

					} else {

						get_template_part( 'template-parts/content', 'none' );

					} ?>
				</section>
				<?php get_sidebar( 'right' ); ?>
			</div>
		</div>
	</section>


	<?php
		/**
		 * ad
		 */
	?>
	<section class="advert advert_xs_300x250 advert_sm_728x90 advert_location_bottom ">
		<div class="advert__wrap">
			<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom' ) : ''; ?>
		</div>
	</section>

<?php get_footer();
