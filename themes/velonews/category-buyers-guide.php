<?php
	/**
	 * The template for displaying all Gift Guide category pages.
	 */
	get_header();
?>

<section id="content" class="content">

	<?php
		if( is_category( '2015-buyers-guide' ) ) {
			get_template_part( 'template-parts/content', '2015-buyers-guide-cat' );
		} elseif( is_category( '2016-buyers-guide' ) ) {
			get_template_part( 'template-parts/content', '2016-buyers-guide-cat' );
		}  elseif( is_category( '2017-buyers-guide' ) ) {
			get_template_part( 'template-parts/content', '2017-buyers-guide-cat' );
		} else {
			//all years after 2017
			get_template_part( 'template-parts/content', 'final-gift-guide-cat' );
		}

	?>

</section>

<?php get_footer(); ?>
