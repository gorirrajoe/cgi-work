<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package velonews
 */

get_header(); ?>

	<section id="content" class="content">
		<div class="container content__container">
			<header class="row">
				<div class="col-xs-12">
					<?php
						/**
						 * breadcrumb
						 */
						get_breadcrumb();
					?>
				</div>
				<div class="col-xs-12">
					<h1 class="content__title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'velonews' ); ?></h1>
				</div>
			</header>

			<div class="row">
				<section class="col-xs-12 col-sm-7 col-md-8">

					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try a search?', 'velonews' ); ?></p>

					<?php echo get_search_form( false ) ?>

				</section>

				<?php get_sidebar( 'right' ); ?>

			</div>
		</div>
	</section>

<?php get_footer(); ?>
