<?php
	/**
	 * sidebar-right
	 */

	if( is_page_template( 'page-templates/bike-search.php' ) || is_page_template( 'page-templates/apparel-search.php' ) ) {
		$product_search = true;
	} else {
		$product_search = false;
	}

	if( $product_search ) {
		echo '<section class="content__section content__flex_position_third sidebar">';
	} else {
		echo '<section class="hidden-xs col-sm-5 col-md-4 sidebar">';
	}


	/**
	 * news feed (shorter version)
	 */
	?>
	<section class="news-feed">
		<?php if( function_exists( 'dynamic_sidebar' ) && dynamic_sidebar( 'right-short' ) ) : else : endif; ?>
	</section>


	<?php
		/**
		 * ad
		 */

		if( $product_search ) {
			$adclass = 'hidden-xs advert advert_xs_300x250';
		} else {
			$adclass = 'advert advert_sm_300x600 advert_location_side';
		}
	?>
	<section class="<?php echo $adclass; ?>">
		<div class="advert__wrap">
			<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'side-top' ) : ''; ?>
		</div>
	</section>


	<?php
		if( !$product_search && !in_category( array( 'video', 'gallery' ) ) ) {

			/**
			 * editors have the ability to disable the sidebar newsletter subscribe widget
			 */
			if( get_post_meta( get_the_ID(), '_diasble_newsletter_subscribe', 1 ) != 'on' ) {
				/**
				 * newsletter
				 */
				newsletter_signup( 'sidebar' );
			}


			/**
			 * ad
			 */ ?>
			<section class="advert advert_xs_300x250 advert_location_side">
				<div class="advert__wrap">
					<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'side-bottom' ) : ''; ?>
				</div>
			</section>
		<?php }
	?>

</section><!-- .sidebar -->
