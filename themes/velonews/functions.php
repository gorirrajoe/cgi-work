<?php

// error_reporting( E_ALL );
// ini_set('display_errors', true);

// Theme data
require( 'inc/velonews-theme-setup.php' );
require( 'inc/velonews-theme-options.php' );

// Mods & Enhancements
require( 'inc/velonews-megamenu.php' );
require( 'inc/velonews-megamenu-walker.php' );
require( 'inc/velonews-mobile-nav-walker.php' );
require( 'inc/velonews-amp.php' );
require( 'inc/velonews-facebook-instant-articles.php' );
require( 'inc/velonews-shortcodes.php' );
require( 'inc/velonews-ads.php' );
require( 'inc/velonews-bulk-duplicate.php' );
require( 'inc/velonews-search.php' );
// require( 'inc/velonews-menus.php' );

// Guides
require( 'inc/velonews-buyers-guide.php' );
require( 'inc/velonews-gift-guide.php' );

// Metadata
require( 'inc/metadata/velonews-metadata-allraces.php' );
require( 'inc/metadata/velonews-metadata-apparel-review.php' );
require( 'inc/metadata/velonews-metadata-bike-review.php' );
require( 'inc/metadata/velonews-metadata-category.php' );
require( 'inc/metadata/velonews-metadata-faq.php' );
require( 'inc/metadata/velonews-metadata-giftguide.php' );
require( 'inc/metadata/velonews-metadata-giftguide-single.php' );
require( 'inc/metadata/velonews-metadata-latest-popular.php' );
require( 'inc/metadata/velonews-metadata-legacy-list.php' );
require( 'inc/metadata/velonews-metadata-longform.php' );
require( 'inc/metadata/velonews-metadata-product-search.php' );
require( 'inc/metadata/velonews-metadata-bike-search-template.php' );
require( 'inc/metadata/velonews-metadata-single.php' );
require( 'inc/metadata/velonews-metadata-user.php' );
require( 'inc/metadata/velonews-metadata-stage.php' );
require( 'inc/metadata/velonews-metadata-race-series.php' );
require( 'inc/metadata/velonews-metadata-race-result.php' );

// Post Types
require( 'inc/post-types/velonews-cpt-all-races.php' );
require( 'inc/post-types/velonews-cpt-apparel-review.php' );
require( 'inc/post-types/velonews-cpt-bike-review.php' );
require( 'inc/post-types/velonews-cpt-race-series.php' );
require( 'inc/post-types/velonews-cpt-stage.php' );
require( 'inc/post-types/velonews-cpt-race-result.php' );

// Taxonomies
require( 'inc/taxonomies/velonews-taxonomy-stage.php' );
require( 'inc/taxonomies/velonews-taxonomy-apparel-review.php' );
require( 'inc/taxonomies/velonews-taxonomy-bike-review.php' );

// Initiate back-end classes
add_action( 'init', array( 'VeloNews_CPT_All_Races', 'singleton' ) );
add_action( 'init', array( 'VeloNews_CPT_Apparel_Review', 'singleton' ) );
add_action( 'init', array( 'VeloNews_CPT_Bike_Review', 'singleton' ) );
add_action( 'init', array( 'VeloNews_CPT_Race_Series', 'singleton' ) );
add_action( 'init', array( 'VeloNews_CPT_Stage', 'singleton' ) );
add_action( 'init', array( 'VeloNews_CPT_Race_Result', 'singleton' ) );

add_action( 'init', array( 'VeloNews_Taxonomy_Stages', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Taxonomy_Apparel', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Taxonomy_Bike', 'singleton' ) );

add_action( 'init', array( 'VeloNews_Metadata_AllRaces', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Metadata_ApparelReviews', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Metadata_BikeReviews', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Metadata_Category', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Metadata_FAQ', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Metadata_GiftGuide', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Metadata_GiftGuide_Single', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Metadata_LatestPopular', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Metadata_LegacyList', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Metadata_Longform', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Metadata_BikeSearch', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Metadata_ProductSearch', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Metadata_Single', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Metadata_User', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Metadata_Stages', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Metadata_Race_Series', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Metadata_Race_Results', 'singleton' ) );

add_action( 'init', array( 'Cgi_Bikes_Admin', 'singleton' ) );
add_action( 'init', array( 'BuyersGuide_Admin', 'singleton' ) );
add_action( 'init', array( 'GiftGuide_Admin', 'singleton' ) );

add_action( 'init', array( 'VeloNews_Custom_Bulk_Action', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Search', 'singleton' ) );
// add_action( 'init', array( 'VeloNews_Menus', 'singleton' ) );

// Initiate front-end classes
add_action( 'init', array( 'VeloNews_MegaMenu', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Shortcodes', 'singleton' ) );
add_action( 'init', array( 'VeloNews_AMP', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Facebook_Instant_Articles', 'singleton' ) );
add_action( 'init', array( 'VeloNews_Ads', 'singleton' ) );

VeloNews_Theme_Setup::singleton();
add_action( 'after_setup_theme', array( 'VeloNews_Theme_Setup', 'do_after_theme_setup' ) );


/**
 * Enqueue scripts and styles.
 */
function velonews_scripts() {

	$version = '5.5';

	//generate guide array
	$guide_array = array();
	$this_year = date("Y");
	//set up guides for next year back to 2015 for gift guides
	for ( $i = intval( $this_year + 1 ); $i >= 2015; $i-- ) {

		$guide_array[] = "{$i}-buyers-guide";
		if($i > 2015 ) {
			$guide_array[] = "{$i}-gift-guide";
		}
	}
	if (is_category( )) {
		//set up parent category for guide check
		$thiscat = get_category( get_query_var( 'cat' ) );
		$parent = $thiscat->parent;
		$parent_cat = get_category(  $parent );
		if(!is_wp_error($parent_cat )){
			$parent_slug = $parent_cat->slug;
			}
		}else {
			$parent_slug = false;
		}
		wp_enqueue_style( 'velonews-style', get_template_directory_uri() . '/style.min.css', array(), $version );
	//check if category, or child category is in guide array
	if( in_category( $guide_array )
		|| is_category( $guide_array )
		|| has_category( $guide_array )
		|| is_page_template( 'page-templates/gift-guide.php' )
		|| (in_array($parent_slug, $guide_array) && ($parent_slug) ) ) {
		wp_enqueue_style( 'buyers-guide-style', get_template_directory_uri() . '/css/buyers-guide.min.css', array(), $version );
	}

	// footer scripts
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', '//code.jquery.com/jquery-1.11.2.min.js', array(), false, true );
	wp_enqueue_script( 'jquery' );
	wp_deregister_script( 'jquery-ui-core' );
	wp_register_script( 'jquery-ui-core', '//code.jquery.com/ui/1.11.4/jquery-ui.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'jquery-ui-core' );

	wp_enqueue_script( 'velonews-main', get_template_directory_uri() . '/js/velo.min.js', array(), $version, true );

	if( is_page_template( 'page-templates/bike-search.php' ) || is_page_template( 'page-templates/apparel-search.php' ) ) {

		wp_enqueue_script( 'product-search', get_template_directory_uri() . '/js/velo-product-search.min.js', array(), $version, true );

	}

	if( get_post_meta( get_the_ID(), '_liveblog', 1 ) == 'on' ) {

		wp_enqueue_script( 'liveblog-script', get_template_directory_uri() . '/js/live-blog.min.js', array(), filemtime( get_template_directory() . '/js/live-blog.min.js' ), true );

	}

}
add_action( 'wp_enqueue_scripts', 'velonews_scripts' );

/**
 * Enqueue admin scripts and styles.
 */
function velonews_admin_scripts() {
	wp_enqueue_style( 'velonews-style', get_template_directory_uri() . '/css/wp-admin.min.css' );
}
add_action( 'admin_enqueue_scripts', 'velonews_admin_scripts' );

/**
 * Do The Generator
 *
 * Removes the WordPress version from the site for added security.
 */
function do_the_generator() {
	return '';
}
add_filter( 'the_generator', 'do_the_generator' );


/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
if( ! function_exists( 'velonews_posted_on' ) ) {

	function velonews_posted_on() {

		if( is_archive() ) {

			$time_string = '<span class="article__published">Published <strong><span><time class="entry-date published updated" datetime="%1$s">%2$s</time></span></strong></span>';

			if( get_the_time( 'Ymd' ) < get_the_modified_time( 'Ymd' ) ) {

				$time_string = '<span class="article__published">Published <strong><span><time class="entry-date published" datetime="%1$s">%2$s</time></span></strong></span>
				<span class="article__updated">Updated <strong><time class="updated" datetime="%3$s">%4$s</time></strong></span>';

			}

		} else {

			$time_string = '<span class="article__header__published">Published <strong><time class="entry-date published updated" datetime="%1$s">%2$s</time></strong></span>';

			if( get_the_time( 'Ymd' ) < get_the_modified_time( 'Ymd' ) ) {

				$time_string = '<span class="article__header__published">Published <strong><time class="entry-date published" datetime="%1$s">%2$s</time></strong></span>
				<span class="article__header__updated">Updated <strong><time class="updated" datetime="%3$s">%4$s</time></strong></span>';

			}

		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		if( ( $guest_author = get_post_meta( get_the_ID(), 'guest_author', 1 ) ) != '' ) {
			$byline = sprintf(
				esc_html_x( 'By %s', 'post author', 'velonews' ),
				'<span class="author vcard"><strong>'. $guest_author .'</strong></span>'
			);
		} else {
			$byline = sprintf(
				esc_html_x( 'By %s', 'post author', 'velonews' ),
				'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '"><strong>' . esc_html( get_the_author() ) . '</strong></a></span>'
			);
		}

		$posted_on = sprintf(
			esc_html_x( '%s', 'post date', 'velonews' ),
			$time_string
		);

		if( is_archive() ) {

			echo '<p class="article__byline">';
				if( get_post_meta( get_the_ID(), 'meta-checkbox', 1 ) == 'on' ) {
					echo '<span class="article__header__sponsored">Sponsored Content</span>';
				}

				echo '<span class="article__author">'. $byline .'</span>
				'. $posted_on .'
			</p>';

		} else {

			echo '<p class="article__header__byline">';
				if( get_post_meta( get_the_ID(), 'meta-checkbox', 1 ) == 'on' ) {
					echo '<span class="article__header__sponsored">Sponsored Content</span>';
				}

				echo '<span class="article__header__author">'. $byline .'</span>
				'. $posted_on .'
			</p>';

		}

	}
}


/**
 * Author Information
 */
function get_author_info() {
	if( get_post_meta( get_the_ID(), '_enable_author_bio', 1 ) == 'on' ) {
		/**
		 * don't show author info if guest author is entered
		 */
		if( get_post_meta( get_the_ID(), 'guest_author', 1 ) == '' ) {
			$displayname	= get_the_author_meta( 'display_name' );

			// don't show if author is "velonews.com"
			if( $displayname != 'VeloNews.com' ) {
				$authorurl		= get_author_posts_url( get_the_author_meta( 'ID' ) );
				$gravatar		= get_avatar( get_the_author_meta( 'ID' ), 120, '', $displayname );
				$avatarurl		= get_the_author_meta( '_vn_avatar' );
				$avatarid		= get_attachment_id_from_src( $avatarurl );
				$avatarsized	= wp_get_attachment_image_src( $avatarid, 'thumbnail' );
				$bio			= apply_filters( 'the_content', get_the_author_meta( 'description' ) );
				$twitter		= get_the_author_meta( '_vn_twitter' );
				$facebook		= get_the_author_meta( '_vn_facebook' );
				$instagram		= get_the_author_meta( '_vn_instagram' );
				$pinterest		= get_the_author_meta( '_vn_pinterest' );
				$googleplus		= get_the_author_meta( '_vn_google_plus' );

				/**
				 * use gravatar as a fallback if no user profile image is uploaded
				 */
				if( function_exists('is_amp_endpoint') && is_amp_endpoint() ) {

					if( $avatarsized[0] ) {

						$author_avatar =
							'<amp-img height="'. $avatarsized[2] .'" width="'. $avatarsized[1] .'" src="'. $avatarsized[0] .'" layout="responsive">';
					} else {

						if(!empty($gravatar))
							$author_avatar .= str_replace('<img', '<amp-img', $gravatar);
					}

				} else {

					if( $avatarsized[0] ) {
						$author_avatar = '<img src="'. $avatarsized[0] .'">';
					} else {
						$author_avatar = $gravatar;
					}

				}

				$authorbio_div = '<footer class="article__footer">
					<div class="row article__author">
						<div class="col-xs-3 article__author__thumbnail">
							<a href="'. $authorurl .'">'. $author_avatar .'</a>
						</div>';

						$authorbio_div .= '<div class="col-xs-9 article__author__bio">
							<h3>By <a href="'. $authorurl .'">'. $displayname .'</a></h3>

							'. $bio .'

							<ul class="article__author__social">';
								if( $twitter ) {
									$authorbio_div .= '<li>
										<a href="'. $twitter .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Twitter</span><span class="fa fa-twitter"></span></a>
									</li>';
								}
								if( $facebook ) {
									$authorbio_div .= '<li>
										<a href="'. $facebook .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Facebook</span>
										<span class="fa fa-facebook"></span></a>
									</li>';
								}
								if( $instagram ) {
									$authorbio_div .= '<li>
										<a href="'. $instagram .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Instagram</span>
										<span class="fa fa-instagram"></span></a>
									</li>';
								}
								if( $pinterest ) {
									$authorbio_div .= '<li>
										<a href="'. $pinterest .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Instagram</span>
										<span class="fa fa-pinterest-p"></span></a>
									</li>';
								}
								if( $googleplus ) {
									$authorbio_div .= '<li>
										<a href="'. $googleplus .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Instagram</span>
										<span class="fa fa-google-plus"></span></a>
									</li>';
								}
							$authorbio_div .= '</ul>
						</div>
					</div>
				</footer>';

				return $authorbio_div;
			}
		}
	}

}


/**
 * Get image id by url
 */
function get_attachment_id_from_src ( $image_src ) {
	global $wpdb;
	$query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
	$id = $wpdb->get_var( $query );
	return $id;

}


/**
 * Mobile/Tablet Check
 */
function is_mobile() {
	require_once ( ABSPATH . 'wp-content/includes/mobiledetect/mobile_detect.php' );
	$detect = new Mobile_Detect;

	// Any mobile device ( phones or tablets ).
	if( $detect->isMobile() && !$detect->isTablet() ) {
		return true;
	}
	return false;
}


function is_tablet() {
	require_once ( ABSPATH . 'wp-content/includes/mobiledetect/mobile_detect.php' );
	$detect = new Mobile_Detect;

	// Any mobile device ( phones or tablets ).
	if( $detect->isTablet() ) {
		return true;
	}
	return false;
}


/**
 * Build the title of the current page, default is site name and description
 */
function page_title() {
	if( is_singular() && !is_home() && !is_front_page() ) {
		// Set to post title if its a post, page, or attachment
		$title = single_post_title( '', false );
	} elseif( is_category() ) {
		// Set to category name, description and site name if its a category
		$title = single_cat_title( '', false ) . ' - ' . stripslashes( strip_tags( category_description() ) ) . ' - ' . get_bloginfo( 'name' );
	} elseif( is_tag() ) {
		// Set to tag name, description and site name if its a tag
		$title = single_cat_title( '', false ) . ' - ' . get_bloginfo( 'name' );
	} elseif( is_date() ) {
		// Set to Archive by Date and site name if its a date archive
		$title = 'Archive by Date - ' . get_bloginfo( 'name' );
	} elseif( is_404() ) {
		// Set to 404 Not Found and site name if its a missing page
		$title = '404 Not Found - ' . get_bloginfo( 'name' );
	} else {
		$title = get_bloginfo( 'name' ) . ' - ' . stripslashes( get_bloginfo( 'description' ) );
	}

	return '<title>' . $title . '</title>';
}


function meta_keywords() {
	$keywords = stripslashes( get_bloginfo( 'description' ) );

	if( is_singular() && !is_home() && !is_front_page() ) {
		global $post;
		$keywords = '';
		// see if there are keywords associated with the post or attachment and if not use the title

		$posttags = get_the_tags( $post->ID );
		if( get_post_meta( $post->ID, '_headspace_metakey', true ) != '' ) {
			$keywords = get_post_meta( $post->ID, '_headspace_metakey', true );
		} elseif( $posttags ) {
			foreach( $posttags as $tag ) {
				$keywords .= $tag->name . ', ';
			}
		}
		if( $keywords != "" ) {
			$keywords = rtrim( $keywords , ', ' );
		} else {
			// Set to post title if its a post, page, or attachment and no tags
			$keywords = single_post_title( '', false );
		}
	} elseif( is_category() ) {
		// Set to category name, description and site name if its a category
		$keywords = single_cat_title( '', false ) . ', ' . stripslashes( strip_tags( category_description() ) );
	} elseif( is_tag() ) {
		// Set to tag name, description and site name if its a tag
		$keywords = single_cat_title( '', false );
	}

	return '<meta name="keywords" content="' . $keywords . '">';
}


function meta_description() {
	$description = get_bloginfo( 'name' ) . ' - ' . stripslashes( get_bloginfo( 'description' ) );

	if( is_singular() && !is_home() && !is_front_page() ) {
		global $post;
		$description = $post->post_excerpt;
		if( get_post_meta( $post->ID, '_headspace_description', true ) != '' ) {
			$description = get_post_meta( $post->ID, '_headspace_description', true );
		} elseif( $description == "" ) {
			// Set to post title if its a post, page, or attachment and there is no excerpt
			$description = single_post_title( '', false );
		}
	} elseif( is_category() ) {
		// Set to category name, description and site name if its a category
		$description = single_cat_title( '', false ) . ' - ' . stripslashes( strip_tags( category_description() ) ) . ' - ' . get_bloginfo( 'name' );
	} elseif( is_tag() ) {
		// Set to tag name, description and site name if its a tag
		$description = single_cat_title( '', false ) . ' - ' . get_bloginfo( 'name' );
	} elseif( is_date() ) {
		// Set to Archive by Date and site name if its a date archive
		$description = 'Archive by Date - ' . get_bloginfo( 'name' );
	} elseif( is_404() ) {
		// Set to 404 Not Found and site name if its a missing page
		$description = '404 Not Found - ' . get_bloginfo( 'name' );
	}
	return '<meta name="description" content="' . $description . '" >';

}


/**
 * Open Graph Protocol - ogp.me
 */
function wp_open_graph_protocol() {
	// Common
	$og_site_name	= get_bloginfo( 'name' );
	$og_image		= get_bloginfo( 'stylesheet_directory' ) . '/images/logo.jpg';

	if( is_front_page() || is_home() ) {

		// Specific to a static or blog homepage
		$og_type		= 'website';
		$og_title		= get_bloginfo( 'name' );
		$og_description	= get_bloginfo( 'description' );
		$og_url			= get_bloginfo( 'url' );

	} elseif( is_singular() ) {

		// Specific to post, page or attachment
		global $post;

		$og_title = single_post_title( '',false );
		$og_url = get_permalink();

		if( has_post_thumbnail( $post->ID ) ) {
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'fb-thumb' );
			$og_image = $image[0];
		}
		if( is_singular() && !is_attachment() ) {
			$og_type = 'article';
		} elseif( is_page() && !is_attachment() ) {
			$og_type = 'page';
		} else {
			if( wp_attachment_is_image() ) {
				$og_type = 'image';
			} else {
				$og_type = 'media';
			}
			$og_url = get_attachment_link();
		}

		if( get_post_meta( $post->ID, '_headspace_description', true ) != '' ) {
			$excerpt = get_post_meta( $post->ID, '_headspace_description', true );
		} else {
			$excerpt = get_the_excerpt();
		}
		if( strlen( $excerpt ) > 140 ) {
			$short_excerpt = substr( $excerpt, 0, 140 );
			$words = explode( " ", $short_excerpt );
			$trim_excerpt = -( strlen( $words[count( $words ) - 1] ) );
			if( $trim_excerpt < 0 ) {
				$og_description = substr( $short_excerpt, 0, $trim_excerpt );
			} else {
				$og_description = $short_excerpt;
			}
		} else {
			$og_description = $excerpt;
		}

	} elseif( is_archive() ) {

		// Specific to author, category, tag or date archives
		$og_url = get_bloginfo( 'url' ) . $_SERVER['REQUEST_URI'];

		if( is_author() ) {
			$og_type = 'author';
			if( get_query_var( 'author_name' ) ) {
				$current_author = get_user_by( 'slug', get_query_var( 'author_name' ) );
			} else {
				$current_author = get_userdata( get_query_var( 'author' ) );
			}
			$og_title = $current_author->display_name;
			$og_description = strip_tags( $current_author->description );
			$avatar = new DOMDocument();
			if( $current_author->user_email != "" && get_avatar( $current_author->user_email ) != "" ) {
				$avatar->loadHTML( get_avatar( $current_author->user_email ) );
    			$og_image = $avatar->getElementsByTagName( 'img' )->item( 0 )->getAttribute( 'src' );
			}
		} else {
			$og_type = 'archive';
			if( !is_date() ) {
				$og_title = single_cat_title( '',false );
				$og_description = strip_tags( term_description() );
			} else {
				$og_title = '';
				$og_description = '';
			}
		}

	}

	$ogp  = '<meta property="og:type" content="'. $og_type .'" />' . "\n";
	$ogp .= '<meta property="og:title" content="'. $og_title .'" />' . "\n";
	$ogp .= '<meta property="og:description" content="'. $og_description .'" />' . "\n";
	$ogp .= '<meta property="og:site_name" content="'. $og_site_name .'" />' . "\n";
	$ogp .= '<meta property="og:url" content="'. $og_url .'" />' . "\n";
	$ogp .= '<meta property="og:image" content="'. $og_image .'" />' . "\n";

	$technical_theme_options = get_option( 'cgi_media_technical_options' );
	$twitter = $technical_theme_options['site_twitter_handle'];
	$ogp .= '<meta name="twitter:card" content="summary">' . "\n";
	$ogp .= '<meta name="twitter:site" content="@'.$twitter.'">' . "\n";
	$ogp .= '<meta name="twitter:url" content="'.$og_url.'">' . "\n";
	$ogp .= '<meta name="twitter:title" content="'.$og_title.'">' . "\n";
	$ogp .= '<meta name="twitter:description" content="'.$og_description.'">' . "\n";
	$ogp .= '<meta name="twitter:image" content="'.$og_image.'">' . "\n";

	return $ogp;
}


/**
 * Meta box handling
 */
function get_short_title() {
	global $post;
	if( get_post_meta( $post->ID, 'short_title', true ) != '' ) {
		$title = strip_tags( get_post_meta( $post->ID, 'short_title', true ) );
	} else {
		$title = the_title( '', '', false );
	}
	if( strlen( $title ) > 60 ) {
		$short_title = substr( $title, 0, 60 );
		$words = explode( ' ', $short_title );
		$trim_title = -( strlen( $words[count( $words ) - 1] ) );
		if( $trim_title < 0 ) {
			return substr( $short_title, 0, $trim_title );
		} else {
			return $short_title;
		}
	} else {
		return $title;
	}
}


/*
function get_guest_author() {
	global $post;
	$post_id = $post->ID;
	if( $post->post_type == 'attachment' && $post->post_parent != 0 ) {
		$post_id = $post->post_parent;
	}
	if( get_post_meta( $post_id, 'guest_author', true  ) && get_post_meta( $post_id, 'guest_author' ) != '' ) {
		return get_post_meta( $post_id, 'guest_author', true  );
	}
	if( $post->post_type == 'attachment' && $post->post_parent != 0 ) {
		$post_parent = get_post( $post_id );
		$post_author = $post_parent->post_author;
		return get_the_author_meta( 'display_name', $post_author  );
	}
	return get_the_author();
}
*/


/**
 * comScore
function get_comscore() { ?>
	<!-- Begin comScore Tag -->
	<script>
		var _comscore = _comscore || [];
		_comscore.push( { c1: "2", c2: "9728917" } );
		( function() {
			var s = document.createElement( "script" ), el = document.getElementsByTagName( "script" )[0]; s.async = true;
			s.src = ( document.location.protocol == "https:" ? "https://sb" : "http://b" ) + ".scorecardresearch.com/beacon.js";
			el.parentNode.insertBefore( s, el );
		} )();
	</script>
	<noscript>
		<img src="http://b.scorecardresearch.com/p?c1=2&c2=9728917&cv=2.0&cj=1" />
	</noscript>
	<!-- End comScore Tag -->

<?php }
 */


/**
 * Facebook
 */
function get_facebook() {
	$facebook_app_ID = cgi_bikes_get_option( 'facebook_app_ID' );

	if( $facebook_app_ID != '' ) { ?>
		<div id="fb-root"></div>
		<script type="text/javascript">
			// load the facebook javascript on all pages if there's an app id
			<?php
				/** sferrino - 6.20.13
				 * Relocated the facebook javascript down here to handle additional facebook
				 * widgets without clashing
				 */
				if( $facebook_app_ID && $facebook_app_ID != "" ) { ?>

					window.fbAsyncInit = function() {
						FB.init( {
							appId:	'<?php echo $facebook_app_ID;?>', // App ID
							status:	true, // check login status
							cookie:	true, // enable cookies to allow the server to access the session
							xfbml:	true  // parse XFBML
						} );

						// Additional initialization code here
					};
					// Load the SDK Asynchronously
					( function( d ) {
						 var js, id = 'facebook-jssdk', ref = d.getElementsByTagName( 'script' )[0];
						 if( d.getElementById( id ) ) {return;}
						 js = d.createElement( 'script' ); js.id = id; js.async = true;
						 js.src = "//connect.facebook.net/en_US/all.js";
						 ref.parentNode.insertBefore( js, ref );
					 } ( document ) );

				<?php }
			?>
		</script>
	<?php }
}


/**
 * Google Analytics
 */
function get_googleGA() {
	//global $posts;
	$google_analytics_ID = cgi_bikes_get_option( 'google_analytics_ID' );

	if( $google_analytics_ID && $google_analytics_ID != "" ) {
		$getid		= get_post_field( 'post_author', get_the_ID() );
		$user_info	= get_userdata( $getid );
		$wp_author	= ( isset( $user_info->user_login ) ? $user_info->user_login : '' );

		$blog_url	= get_bloginfo('url');
		$blog_url	= preg_replace( '#^http(s)?://#', '', $blog_url );
		$blog_url	= preg_replace( '/^www\./', '', $blog_url );
		$domain		= strpos( $blog_url, 'competitor.' ) ? '.competitor.com' : $blog_url; ?>

		<script type="text/javascript">
			var _gaq = _gaq || [];
			var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
			_gaq.push(
				['_require', 'inpage_linkid', pluginUrl],
				['_setAccount', '<?php echo $google_analytics_ID; ?>'],
				['_setDomainName', '<?php echo $domain; ?>'],
				['_setAllowLinker', true],
				['_setCustomVar',1,'wp_author','<?php echo $wp_author ?>',3],
				['_trackPageview']
			 );

			( function() {
					var ga = document.createElement( 'script' ); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ( 'https:' == document.location.protocol ? 'https://' : 'http://' ) + 'stats.g.doubleclick.net/dc.js';
					var s = document.getElementsByTagName( 'script' )[0]; s.parentNode.insertBefore( ga, s );
			} )();
		</script>
	<?php }

}


/**
 * bike review standings
 * will traverse "wp_bikes" table and find what rank a particular bike is
 */
function get_bike_standing( $postid, $type ) {
	global $wpdb;
	$standings = $wpdb->get_results(
		'SELECT b.post_id FROM wp_bikes b, wp_posts p WHERE b.type = "'. $type .'" AND p.ID = b.post_id AND p.post_date >= CURDATE() - INTERVAL 36 MONTH AND p.post_status = "publish" ORDER BY b.overall_score DESC'
	);

	$num = 1;
	foreach( $standings as $object ) {
		if( $postid == $object->post_id ) {
			return $num;
			break;
		}
		$num++;
	}
}


/**
 * function: add favicon to head
 */
function vn_display_favicon() {
	$pathtofavicon = get_bloginfo( 'stylesheet_directory' ).'/images/favicon'; ?>
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $pathtofavicon; ?>/apple-touch-icon-57x57.png?v=pggmqzkkz5">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $pathtofavicon; ?>/apple-touch-icon-60x60.png?v=pggmqzkkz5">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $pathtofavicon; ?>/apple-touch-icon-72x72.png?v=pggmqzkkz5">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $pathtofavicon; ?>/apple-touch-icon-76x76.png?v=pggmqzkkz5">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $pathtofavicon; ?>/apple-touch-icon-114x114.png?v=pggmqzkkz5">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $pathtofavicon; ?>/apple-touch-icon-120x120.png?v=pggmqzkkz5">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $pathtofavicon; ?>/apple-touch-icon-144x144.png?v=pggmqzkkz5">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $pathtofavicon; ?>/apple-touch-icon-152x152.png?v=pggmqzkkz5">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $pathtofavicon; ?>/apple-touch-icon-180x180.png?v=pggmqzkkz5">
	<link rel="icon" type="image/png" href="<?php echo $pathtofavicon; ?>/favicon-32x32.png?v=pggmqzkkz5" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo $pathtofavicon; ?>/favicon-194x194.png?v=pggmqzkkz5" sizes="194x194">
	<link rel="icon" type="image/png" href="<?php echo $pathtofavicon; ?>/favicon-96x96.png?v=pggmqzkkz5" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo $pathtofavicon; ?>/android-chrome-192x192.png?v=pggmqzkkz5" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo $pathtofavicon; ?>/favicon-16x16.png?v=pggmqzkkz5" sizes="16x16">
	<link rel="manifest" href="<?php echo $pathtofavicon; ?>/manifest.json?v=pggmqzkkz5">
	<link rel="mask-icon" href="<?php echo $pathtofavicon; ?>/safari-pinned-tab.svg?v=pggmqzkkz5" color="#5bbad5">
	<meta name="apple-mobile-web-app-title" content="VeloNews.com">
	<meta name="application-name" content="VeloNews.com">
	<meta name="msapplication-TileColor" content="#2d89ef">
	<meta name="msapplication-TileImage" content="<?php echo $pathtofavicon; ?>/mstile-144x144.png?v=pggmqzkkz5">
	<meta name="theme-color" content="#ffffff">
<?php }


/**
 * leaderboard
 */
function get_leaderboard() {
	//get top 10 race results for overall (GC) race from stages
	//wp_reset_postdata();
	$current_race_ID = cgi_bikes_get_option('leaders_race');
	$current_year = date("Y");
	$countries =  array(
		'AFG'	=> 'Afghanistan',
		'ALB'	=> 'Albania',
		'ALG'	=> 'Algeria',
		'AND'	=> 'Andorra',
		'ANG'	=> 'Angola',
		'ANT'	=> 'Antigua and Barbuda',
		'ARG'	=> 'Argentina',
		'ARM'	=> 'Armenia',
		'ARU'	=> 'Aruba',
		'ASA'	=> 'American Samoa',
		'AUS'	=> 'Australia',
		'AUT'	=> 'Austria',
		'AZE'	=> 'Azerbaijan',
		'BAH'	=> 'Bahamas',
		'BAN'	=> 'Bangladesh',
		'BAR'	=> 'Barbados',
		'BDI'	=> 'Burundi',
		'BEL'	=> 'Belgium',
		'BEN'	=> 'Benin',
		'BER'	=> 'Bermuda',
		'BHU'	=> 'Bhutan',
		'BIH'	=> 'Bosnia and Herzegovina',
		'BIZ'	=> 'Belize',
		'BLR'	=> 'Belarus',
		'BOL'	=> 'Bolivia',
		'BOT'	=> 'Botswana',
		'BRA'	=> 'Brazil',
		'BRN'	=> 'Bahrain',
		'BRU'	=> 'Brunei',
		'BUL'	=> 'Bulgaria',
		'BUR'	=> 'Burkina Faso',
		'CAF'	=> 'Central African Republic',
		'CAM'	=> 'Cambodia',
		'CAN'	=> 'Canada',
		'CAY'	=> 'Cayman Islands',
		'CGO'	=> 'Congo',
		'CHA'	=> 'Chad',
		'CHI'	=> 'Chile',
		'CHN'	=> 'China',
		'CIV'	=> 'Ivory Coast',
		'CMR'	=> 'Cameroon',
		'COD'	=> 'DR Congo',
		'COK'	=> 'Cook Islands',
		'COL'	=> 'Colombia',
		'COM'	=> 'Comoros',
		'CPV'	=> 'Cape Verde',
		'CRC'	=> 'Costa Rica',
		'CRO'	=> 'Croatia',
		'CUB'	=> 'Cuba',
		'CYP'	=> 'Cyprus',
		'CZE'	=> 'Czech Republic',
		'DEN'	=> 'Denmark',
		'DJI'	=> 'Djibouti',
		'DMA'	=> 'Dominica',
		'DOM'	=> 'Dominican Republic',
		'ECU'	=> 'Ecuador',
		'EGY'	=> 'Egypt',
		'ERI'	=> 'Eritrea',
		'ESA'	=> 'El Salvador',
		'ESP'	=> 'Spain',
		'EST'	=> 'Estonia',
		'ETH'	=> 'Ethiopia',
		'FIJ'	=> 'Fiji',
		'FIN'	=> 'Finland',
		'FRA'	=> 'France',
		'FSM'	=> 'Federated States of Micronesia',
		'GAB'	=> 'Gabon',
		'GAM'	=> 'The Gambia',
		'GBR'	=> 'Great Britain',
		'GBS'	=> 'Guinea-Bissau',
		'GEO'	=> 'Georgia',
		'GEQ'	=> 'Equatorial Guinea',
		'GER'	=> 'Germany',
		'GHA'	=> 'Ghana',
		'GRE'	=> 'Greece',
		'GRN'	=> 'Grenada',
		'GUA'	=> 'Guatemala',
		'GUI'	=> 'Guinea',
		'GUM'	=> 'Guam',
		'GUY'	=> 'Guyana',
		'HAI'	=> 'Haiti',
		'HKG'	=> 'Hong Kong',
		'HON'	=> 'Honduras',
		'HUN'	=> 'Hungary',
		'INA'	=> 'Indonesia',
		'IND'	=> 'India',
		'IRI'	=> 'Iran',
		'IRL'	=> 'Ireland',
		'IRQ'	=> 'Iraq',
		'ISL'	=> 'Iceland',
		'ISR'	=> 'Israel',
		'ISV'	=> 'Virgin Islands',
		'ITA'	=> 'Italy',
		'IVB'	=> 'British Virgin Islands',
		'JAM'	=> 'Jamaica',
		'JOR'	=> 'Jordan',
		'JPN'	=> 'Japan',
		'KAZ'	=> 'Kazakhstan',
		'KEN'	=> 'Kenya',
		'KGZ'	=> 'Kyrgyzstan',
		'KIR'	=> 'Kiribati',
		'KOR'	=> 'South Korea',
		'KOS'	=> 'Kosovo',
		'KSA'	=> 'Saudi Arabia',
		'KUW'	=> 'Kuwait',
		'LAO'	=> 'Laos',
		'LAT'	=> 'Latvia',
		'LBA'	=> 'Libya',
		'LBR'	=> 'Liberia',
		'LCA'	=> 'Saint Lucia',
		'LES'	=> 'Lesotho',
		'LIB'	=> 'Lebanon',
		'LIE'	=> 'Liechtenstein',
		'LTU'	=> 'Lithuania',
		'LUX'	=> 'Luxembourg',
		'MAD'	=> 'Madagascar',
		'MAR'	=> 'Morocco',
		'MAS'	=> 'Malaysia',
		'MAW'	=> 'Malawi',
		'MDA'	=> 'Moldova',
		'MDV'	=> 'Maldives',
		'MEX'	=> 'Mexico',
		'MGL'	=> 'Mongolia',
		'MHL'	=> 'Marshall Islands',
		'MKD'	=> 'Macedonia',
		'MLI'	=> 'Mali',
		'MLT'	=> 'Malta',
		'MNE'	=> 'Montenegro',
		'MON'	=> 'Monaco',
		'MOZ'	=> 'Mozambique',
		'MRI'	=> 'Mauritius',
		'MTN'	=> 'Mauritania',
		'MYA'	=> 'Myanmar',
		'NAM'	=> 'Namibia',
		'NCA'	=> 'Nicaragua',
		'NED'	=> 'Netherlands',
		'NEP'	=> 'Nepal',
		'NGR'	=> 'Nigeria',
		'NIG'	=> 'Niger',
		'NOR'	=> 'Norway',
		'NRU'	=> 'Nauru',
		'NZL'	=> 'New Zealand',
		'OMA'	=> 'Oman',
		'PAK'	=> 'Pakistan',
		'PAN'	=> 'Panama',
		'PAR'	=> 'Paraguay',
		'PER'	=> 'Peru',
		'PHI'	=> 'Philippines',
		'PLE'	=> 'Palestine',
		'PLW'	=> 'Palau',
		'PNG'	=> 'Papua New Guinea',
		'POL'	=> 'Poland',
		'POR'	=> 'Portugal',
		'PRK'	=> 'North Korea',
		'PUR'	=> 'Puerto Rico',
		'QAT'	=> 'Qatar',
		'ROU'	=> 'Romania',
		'RSA'	=> 'South Africa',
		'RUS'	=> 'Russia',
		'RWA'	=> 'Rwanda',
		'SAM'	=> 'Samoa',
		'SEN'	=> 'Senegal',
		'SEY'	=> 'Seychelles',
		'SIN'	=> 'Singapore',
		'SKN'	=> 'Saint Kitts and Nevis',
		'SLE'	=> 'Sierra Leone',
		'SLO'	=> 'Slovenia',
		'SMR'	=> 'San Marino',
		'SOL'	=> 'Solomon Islands',
		'SOM'	=> 'Somalia',
		'SRB'	=> 'Serbia',
		'SRI'	=> 'Sri Lanka',
		'SSD'	=> 'South Sudan',
		'STP'	=> 'São Tomé and Príncipe',
		'SUD'	=> 'Sudan',
		'SUI'	=> 'Switzerland',
		'SUR'	=> 'Suriname',
		'SVK'	=> 'Slovakia',
		'SWE'	=> 'Sweden',
		'SWZ'	=> 'Swaziland',
		'SYR'	=> 'Syria',
		'TAN'	=> 'Tanzania',
		'TGA'	=> 'Tonga',
		'THA'	=> 'Thailand',
		'TJK'	=> 'Tajikistan',
		'TKM'	=> 'Turkmenistan',
		'TLS'	=> 'Timor-Leste',
		'TOG'	=> 'Togo',
		'TPE'	=> 'Chinese Taipei',
		'TTO'	=> 'Trinidad and Tobago',
		'TUN'	=> 'Tunisia',
		'TUR'	=> 'Turkey',
		'TUV'	=> 'Tuvalu',
		'UAE'	=> 'United Arab Emirates',
		'UGA'	=> 'Uganda',
		'UKR'	=> 'Ukraine',
		'URU'	=> 'Uruguay',
		'USA'	=> 'USA',
		'UZB'	=> 'Uzbekistan',
		'VAN'	=> 'Vanuatu',
		'VEN'	=> 'Venezuela',
		'VIE'	=> 'Vietnam',
		'VIN'	=> 'Saint Vincent and the Grenadines',
		'YEM'	=> 'Yemen',
		'ZAM'	=> 'Zambia',
		'ZIM'	=> 'Zimbabwe'
	);
	global $wpdb;
	global $post;
	$current_race_name	=  get_term_by('id', $current_race_ID,'stagecat' )->name;
	$short_race_name	= substr( $current_race_name, 0 , -5 );

	$race_id_query = 'SELECT pk_race_id from wp_races WHERE race_name = "' . $current_race_name . '" AND race_year ="' . $current_year . '" ';
	$race_id_result	= $wpdb->get_results( $race_id_query );
	$race_id		= ( isset( $race_id_result[0]->pk_race_id ) ? $race_id_result[0]->pk_race_id : '' );

	$stage_results_info = 'SELECT wp_riders.*, wp_stage_results.* FROM wp_stage_results INNER JOIN wp_riders WHERE wp_riders.pk_rider_id =wp_stage_results.fk_rider_id  AND fk_race_id = "' . $race_id . '" AND rider_finish_place > 0 AND rider_finish_place < 11 ORDER BY LPAD(rider_finish_place, 20, "0") ASC';

	$results_info = $wpdb->get_results( $stage_results_info );
	// echo '<pre>';
	// 	var_dump($results_info);
	// echo '</pre>';
	$latest_stage = 0;
	foreach ($results_info as $key => $value) {
			if( $value->stage_number > $latest_stage ){
				$latest_stage = $value->stage_number;
				}
			}
	$stage_args = array(
		'post_type'	=>	'stage',
		'meta_key'	=> '_vn_stages_stage_number',
		// 'meta_compare'	=> '=',
		'meta_value'	=> $latest_stage,
		'tax_query'		=> array(
			 	'operator' => 'AND',
				array(
					'taxonomy'	=> 'stagecat',
					'field'		=> 'slug',
					'terms'		=>sanitize_title( $current_race_name)
				),
				array(
					'taxonomy'	=> 'stageyear',
					'field'		=> 'slug',
					'terms'		=> $current_year
					)
				)
	);

	$stage_posts = new WP_Query ($stage_args);


	if( $stage_posts->have_posts() ) : while( $stage_posts->have_posts() ) :  $stage_posts->the_post();

		$stage_id = get_the_id();

		$stage_link = get_permalink( $stage_id );

		$stage_name = ( get_post_meta( $stage_id, '_vn_stages_stage_location', 1 ) != '' ) ? get_post_meta( $stage_id, '_vn_stages_stage_location', 1 ) : '';

		endwhile;
	endif;
	wp_reset_postdata();

	$race_args = array(
		'post_type'			=>	'races_event',
		'posts_per_page'	=>	1,
		'tax_query'			=> array(
			 	'operator' => 'AND',
				array(
					'taxonomy'	=> 'stagecat',
					'field'		=> 'slug',
					'terms'		=>sanitize_title( $current_race_name)
				),
				array(
					'taxonomy'	=> 'stageyear',
					'field'		=> 'slug',
					'terms'		=> $current_year
					)
				)
	);
	$race_posts = new WP_Query( $race_args );

	if( $race_posts->have_posts() ) : while( $race_posts->have_posts() ) :  $race_posts->the_post();

		$race_link = get_the_permalink( get_the_id() );

		endwhile;
	endif;
	wp_reset_postdata();

	if ( $latest_stage !== 0 ){

		echo '<section class="hidden-xs leader-board">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<table class="leader-board__table">
							<thead>
								<tr>
									<th colspan="6" class="leader-board__table__title">
										<span class="live">'.$short_race_name .'</span> - <span class="live">Stage ' .$latest_stage. '</span> - '.$stage_name . '
									</th>
									<th colspan="4" class="leader-board__table__options">
										<a href="'.$stage_link.'?tab=stage-report" target="_self">Stage Report</a> | <a href="'.$race_link .'" target="_self">More News</a>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>';

									foreach( $results_info as $key => $value ) {

										if( $value->stage_number == $latest_stage && $value->rider_finish_place > 0) {
											$pieces		= explode( ' ', $value->rider_name );
											$last_name	= array_pop( $pieces );
											$country	= array_search( $value->rider_country, $countries );
											echo '<td>'. $value->rider_finish_place . '. '. $last_name .' ('. $country .')</td>';

										}
									}
								echo '</tr>
								<tr>';
									$first = true;
									foreach( $results_info as $key => $value ) {

										if( $value->stage_number == $latest_stage && $value->rider_finish_place > 0 ) {
											$pieces		= explode( ' ', $value->rider_name );
											$last_name	= array_pop( $pieces );
											if( $first ) {
												$leader_time = array( explode(':', $value->rider_nice_time) ) ;
												echo '<td>' . $value->rider_nice_time . '</td>';
												$first = false;
											} else {
												if ( array( explode (':', $value->rider_time) ) == $leader_time ){
													echo '<td>+0</td>';
												}else{
													echo '<td>+' . $value->rider_time . '</td>';
												}
											}
										}
									}
								echo '</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section>';
	}
}


/**
 * get category name/slug from category object
 * returns an array that contains:
 * [1] => name, [2] => array of slugs
 */
function get_cat_array( $post_id ) {
	$catarray = array();

	/**
	 * if the article post type isn't a bike/apparel review
	 */
	if( !in_array( get_post_type( $post_id ), array( 'bike-reviews', 'apparel-reviews' ) ) ) {

		$cat_obj = get_the_category( $post_id );
		$primary_cat = new WPSEO_Primary_Term('category', $post_id);
	    $primary_cat = $primary_cat->get_primary_term();
	    $primary_obj = get_term( $primary_cat );
	    if( $primary_cat ) {
	    	$catarray['name'] = strtoupper( $primary_obj->name );
	    	$catarray['slug'] = $primary_obj->slug;
	    } else {
			// rename "Partner Connect" to "Sponsored Content"
			if( $cat_obj[0]->slug == 'partnerconnect' || $cat_obj[0]->slug == 'partner-connect' ) {

				$catarray['name'] = 'Sponsored Content';

			} else {

				/**
				 * if the category is 'news', but has other categories, set the name to one of those
				 * if it's 'news' AND ONLY 'news', then set the name to 'news'
				 */
				if( $cat_obj[0]->slug == 'news' && ( count( $cat_obj ) != 1 ) ) {
					$catarray['name'] = strtoupper( $cat_obj[1]->name );
				} else {
					$catarray['name'] = strtoupper( $cat_obj[0]->name );
				}

			}
			foreach( $cat_obj as $cat ) {
				$catarray['slug'][] = $cat->slug;
			}
		}
		} else {
			if( get_post_type( $post_id ) == 'bike-reviews' ) {
				$catarray['name'] = 'BIKE REVIEW';
				$catarray['slug'] = '';
			} elseif( get_post_type( $post_id ) == 'apparel-reviews' ) {
				$catarray['name'] = 'GEAR & APPAREL REVIEW';
				$catarray['slug'] = '';
			}
		}
		return $catarray;
}

/**
 * get post title
 * if the post is a stage or races_event custom post type, use the descriptive title (if it exists)
 *
 * otherwise, checks if a short title (metadata) was entered
 */
function vn_get_post_title( $postid ) {

	if( in_array( get_post_type(), array( 'stage', 'races_event' ) ) ) {
		$title = ( get_post_meta( $postid, 'descriptive_title', 1 ) != '' ) ? get_post_meta( $postid, 'descriptive_title', 1 ) : get_the_title( $postid );
	} else {
		$short_post_title = get_post_meta( $postid, 'short_title', 1 );
		if( $short_post_title != '' ) {
			$title = $short_post_title;
		} else {
			$title = get_the_title( $postid );
		}
	}

	/**
	 * in case the title has a quotation mark, replace it with the html entity
	 */
	$title = str_replace( '"', '&quot;', $title );

	return $title;

}


/**
 * get social sharing
 */
function get_social_sharing( $position = 'top', $type = 'normal' ) {
	$twitter			= cgi_bikes_get_option( 'twitter_link' );
	$twitter_handle		= explode( 'com/', $twitter );
	$twitter_handle[1]	= (isset( $twitter_handle[1] ) ? $twitter_handle[1] : '');

	if( $position == 'top' ) {

		if( $type != 'longform' ) {

			echo '<div id="social-bar-top" class="social-bar social-bar_location_top visible-xs visible-sm">
				<div class="social-bar__wrap">
					<div class="pw-widget" data-url="" data-title="" data-via="'. $twitter_handle[1] .'" data-layout="horizontal" data-size="32" data-counter="false">
						<a class="pw-button-facebook" data-label="Share"></a>
						<a class="pw-button-twitter" data-label="Tweet"></a>
						<a class="pw-button-email" data-label="Email"></a>
					</div>
				</div>
			</div>';

		} else {

			echo '<div id="social-bar-top" class="social-bar social-bar_location_top visible-xs">
				<div class="social-bar__wrap">
					<div class="pw-widget" data-url="" data-title="" data-via="'. $twitter_handle[1] .'" data-layout="horizontal" data-size="32" data-counter="false">
						<a class="pw-button-facebook" data-label="Share"></a>
						<a class="pw-button-twitter" data-label="Tweet"></a>
						<a class="pw-button-email" data-label="Email"></a>
					</div>
				</div>
			</div>';

		}

	} elseif( $position == 'side' ) {

		if( $type != 'longform' ) {

			echo '<div id="article-left" class="social-bar social-bar_location_left col-md-1 visible-md visible-lg">
				<section id="social-bar-left" class="social-widget__wrap">
					<div class="pw-widget" data-url="" data-title="" data-via="'. $twitter_handle[1] .'" data-layout="vertical" data-size="48" data-counter="false">
						<a class="pw-button-facebook"></a>
						<a class="pw-button-twitter"></a>
						<a class="pw-button-email"></a>
					</div>
				</section>
			</div>';

		} else {

			echo '<div id="article-left" class="social-bar social-bar_location_left col-sm-1 hidden-xs">
				<section id="social-bar-left" class="social-widget__wrap">
					<div class="pw-widget" data-url="" data-title="" data-via="'. $twitter_handle[1] .'" data-layout="vertical" data-size="48" data-counter="false">
						<a class="pw-button-facebook"></a>
						<a class="pw-button-twitter"></a>
						<a class="pw-button-email"></a>
					</div>
				</section>
			</div>';

		}
	}
}


/**
 * get breadcrumb
 */
function get_breadcrumb() {
	if( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb('<ol class="article__bread-crumb"><li>','</li></ol>');
	}
}


/**
 * get outbrain
 */
function get_outbrain() {
	$outbrain = cgi_bikes_get_option( 'outbrain_ads' );
	if( $outbrain == 'on' ) {
		echo '<section class="related-content">
			<div class="container">';
				if( !is_mobile() ) { ?>
					<div class="OUTBRAIN" data-src="<?php echo get_permalink(); ?>" data-widget-id="AR_12" data-ob-template="cgi" ></div>
					<script type="text/javascript" async="async" src="http://widgets.outbrain.com/outbrain.js"></script>
				<?php } else { ?>
					<div class="OUTBRAIN" data-src="<?php echo get_permalink(); ?>" data-widget-id="MB_8" data-ob-template="cgi" ></div>
					<script type="text/javascript" async="async" src="http://widgets.outbrain.com/outbrain.js"></script>
				<?php }
			echo '</div>
		</section>';
	}
}

/**
 * get attachment info
 */
function vn_get_attachment( $attachment_id ) {
	$attachment = get_post( $attachment_id );
	return array(
		'alt'			=> get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
		'caption'		=> $attachment->post_excerpt,
		'description'	=> $attachment->post_content,
		'href'			=> get_permalink( $attachment->ID ),
		'src'			=> $attachment->guid,
		'title'			=> $attachment->post_title
	);
}


/**
 * get datalayer
 * Define dataLayer for site analytics
 */
function get_datalayer() {
	global $post;

	$the_id = get_the_ID();

	if( is_page() && !is_front_page() || is_404() ) {
		global $page;
		$the_id = get_queried_object_id();
	} elseif( is_singular( 'post' ) || is_front_page() || is_home() || is_category() ) {
		$post_content = isset( $post->post_content ) ? $post->post_content : '';
		$multipage	= ( strpos( $post_content, '<!--nextpage-->' ) ) ? true : false;
		$the_id		= isset( $post->ID ) ? $post->ID : '';
	}
	$category		= 'category';
	$post_author = isset( $post->post_author ) ? $post->post_author : '';
	$wpAuthor		= !is_404() ? get_the_author_meta( 'user_nicename', $post_author ) : '';
	$guestAuthor	= 'none';
	$hasVideo		= ( get_post_meta( $the_id, '_youtube_id', true ) == '' && get_post_meta( $the_id, 'video_id', true ) == '' && isset( $post->post_content ) && !has_shortcode( $post->post_content, 'youtube' ) ) ? 'no' : 'yes';
	$partnerName	= get_post_meta( $the_id, 'meta-text', true ) ?: '';

	$categories		= wp_get_post_categories( $the_id, array( 'fields' => 'all' ) ) ? : array();

	if ( is_page() || is_404() || is_search() ) {
		$category = 'page';
	}
	if ( is_singular( 'post' ) ) {
		$category = 'post';
		/*
		if( $multipage ) {
			$category .= ' multipage';
		}
		*/
		$guestAuthor = get_post_meta( $the_id, 'guest_author', true ) ? : 'none';
	}
	if ( is_singular( 'bike-reviews' ) ) {
		$usedlc = 'bike-reviews';
		$category = 'custom post';
	}
	if ( is_singular( 'apparel-reviews' ) ) {
		$usedlc = 'apparel-reviews';
		$category = 'custom post';
	}
	if ( is_singular( array('races_event', 'stage', 'race_result', 'all_races') ) ) {
		$race_category		= wp_get_post_terms( get_the_ID(), 'stagecat' )[0]->slug;
		//print_r($race_category);
		$racecat_short_name	= str_replace( '-race', '', $race_category );
		$usedlc = 'race,'.$racecat_short_name;
		$category = 'custom post';
	}

	$hasGallery = 'no';
	if ( !is_404() ) {

		$cat_names		= wp_list_pluck( $categories, 'names' );

		if ( is_single() && ( in_array( 'Photos', $cat_names ) || has_shortcode( $post->post_content , 'gallery' ) ) ) {
			$hasGallery = 'yes';
			if ( $gallery = get_post_gallery( $the_id, false ) ) {
				$gallery_count = count( $gallery['src'] );
			}
		}
		/*
		if ( in_array( 'Partner Connect', $categories ) || get_post_meta( $the_id, 'meta-text', true ) != '' ) {
			$category = 'partner connect';
		}
		*/
	}

	//blue kai data layer info
	$dl_sections	= '';
	$dl_tags		= '';
	if ( $category != 'category' ) {
		$cat_dl   = wp_get_post_categories($the_id);

		if ( !empty( $categories ) ) {
			$postcats_slugs	= wp_list_pluck( $categories, 'slug' );
			$dl_sections	= implode( ',', $postcats_slugs );
		}

		if ( $category == 'custom post' ) {
			$dl_sections = $usedlc;
		}

		$posttags = get_the_tags();

		if ( !empty( $posttags ) ) {
			$posttags_slugs	= wp_list_pluck( $posttags, 'slug' );
			$dl_tags		= implode( ',', $posttags_slugs );
		}

	} elseif ( is_object( get_queried_object() ) ) {
		$dl_sections	= get_queried_object()->slug;
	}
	//End blue kai data layer
	?>

	<script type="text/javascript">
		window.dataLayer = window.dataLayer || [];
		window.dataLayer.push({
			'pageType': '<?php echo strtolower( $category ); ?>',
			'wpAuthor': '<?php echo $wpAuthor; ?>',
			'guestAuthor': '<?php echo strtolower( $guestAuthor ); ?>',
			'hasVideo': '<?php echo strtolower( $hasVideo ); ?>',
			'partnerConnectName': '<?php echo strtolower( $partnerName ); ?>',
			'category': '<?php echo strtolower($dl_sections); ?>',
			'tags': '<?php echo strtolower($dl_tags); ?>',
			'hasGallery': '<?php echo $hasGallery; ?>'
			<?php if( $hasGallery == 'yes' ) { ?>
				,'photoCount': '<?php echo $gallery_count; ?>'
			<?php } ?>
		});
	</script>

<?php }


/**
 * Get Page Navigation For Buyers/Gift Guides
 * wp_link_pages is very limited with output customization
 * use DOMDocument to handle each anchor as a node in an array
 * reference: http://stackoverflow.com/questions/10366458/php-dom-parsing-a-html-list-into-an-array
 */
function get_guide_nav( $prev_next_links ) {
	$dom = new DOMDocument;
	$dom->loadHTML( $prev_next_links );

	foreach( $dom->getElementsByTagName( 'a' ) as $node ) {
		$nav_array[] = $dom->saveHTML( $node );
	}

	$arrows = '';
	foreach( $nav_array as $nav ) {
		if( strpos( $nav, 'fa-chevron-left' ) !== false ) {
			$arrows .= str_replace( '<a href=', '<a class="product__nav_direction_prev" href=', $nav );
		} elseif( strpos( $nav, 'fa-chevron-right' ) !== false ) {
			$arrows .= str_replace( '<a href=', '<a class="product__nav_direction_next" href=', $nav );
		}
	}
	return $arrows;

}


/**
 * change post count for magazine archive page (9)
 */
function post_count_on_magazine( $query ) {
	if( !empty( $query->query['category_name'] ) && $query->query['category_name'] == 'magazine' ) {
		$query->set( 'posts_per_page', '9' ); /*set this your preferred count*/
	}
}
add_action( 'pre_get_posts', 'post_count_on_magazine' );


/**
 * get paginated links navigation
 */
function get_paginated_links( $special_query = false ) {
	if( $special_query != false ) {
		$big = 999999999; // need an unlikely integer

		$args = array(
			'before_page_number'	=> '<span>',
			'after_page_number'		=> '</span>',
			'base'					=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format'				=> '?paged=%#%',
			'current'				=> max( 1, get_query_var( 'paged' ) ),
			'total'					=> $special_query->max_num_pages,
			'prev_text'				=> '&laquo; Newer Posts',
			'next_text'				=> 'Older Posts &raquo;',
			'mid_size'				=> 3
		);
	} else {
		$args = array(
			'before_page_number'	=> '<span>',
			'after_page_number'		=> '</span>',
			'prev_text'				=> '&laquo; Newer Posts',
			'next_text'				=> 'Older Posts &raquo;',
			'mid_size'				=> 3
		);
	}

	return '<div class="page-links">'.
		paginate_links( $args ) .
	'</div>';
}


/**
 * Newsletter signup form
 */
function newsletter_signup( $pos = null ) {
	if( $pos == 'sidebar' ) { ?>

		<section class="newsletter newsletter_position_sidebar">
			<div class="newsletter__wrap">
				<h2 class="newsletter__title"><?php echo cgi_bikes_get_option( 'nl_signup_title' ); ?></h2>
				<p><?php echo cgi_bikes_get_option( 'nl_signup_blurb' ); ?></p>
				<img src="<?php echo cgi_bikes_get_option( 'nl_signup_img' ); ?>" alt="">
				<form class="newsletter__form" id="sidebar-newsletter" method="POST" action="" onsubmit="_gaq.push(['_trackEvent', 'newsletter velo', 'subscribe', 'sidebar']);">
					<?php wp_nonce_field( 'nl_subscribe_sidebar', 'nl_subscribe_sidebar_nonce', true, true ); ?>
					<input type="hidden" name="pos" value="sidebar" />
					<input type="hidden" name="mbid" value="velonews" />
					<input type="hidden" name="sub" value="general" />

					<label for="sidebar-email" class="screen-reader-text">Subscribe to the VELONEWS newsletter</label>
					<input type="text" name="sidebar-email" id="sidebar-email" placeholder="EMAIL ADDRESS" class="newsletter__input newsletter__input_name_email">
					<button type="submit" id="sidebar-submit" class="btn btn--red btn_type_submit">SUBSCRIBE</button>
					<p><span class="fa fa-lock"></span> PRIVACY PROTECTION GUARANTEED</p>

					<p class="errorMessage newsletter__error"></p>

					<div class="newsletter__loading">
						<img src="<?php echo get_bloginfo("stylesheet_directory"); ?>/images/icontact-wait-sidebar.gif">
					</div>
				</form>
			</div>
		</section>

	<?php } elseif( $pos == 'footer' ) { ?>

		<section class="footer__section newsletter newsletter_position_footer">
			<div class="newsletter__wrap">
				<h2>NEWSLETTER SIGN UP</h2>
				<p><?php echo cgi_bikes_get_option( 'nl_footer_signup_blurb' ); ?></p>
				<form class="newsletter__form" id="newsletter-footer" method="POST" action="" onsubmit="_gaq.push(['_trackEvent', 'newsletter velo', 'subscribe', 'footer']);">
					<?php wp_nonce_field( 'nl_subscribe_footer', 'nl_subscribe_footer_nonce', true, true ); ?>
					<input type="hidden" name="pos" value="footer" />
					<input type="hidden" name="mbid" value="velonews" />
					<input type="hidden" name="sub" value="general" />

					<label for="footer-email" class="screen-reader-text">Subscribe to the VELONEWS newsletter</label>
					<input type="text" name="footer-email" id="footer-email" placeholder="EMAIL ADDRESS" class="newsletter__input newsletter__input_name_email">
					<button type="submit" id="footer-submit" class="btn btn--red btn_type_submit">SIGN UP</button>

					<p class="errorMessage newsletter__error"></p>

					<div class="newsletter__loading">
						<img src="<?php echo get_bloginfo("stylesheet_directory"); ?>/images/icontact-wait-footer.gif">
					</div>
				</form>
			</div>
		</section>

	<?php }
}


/**
 * process the newsletter submission form request
 */
function newsletterprocess_request() {
	$position = ( isset( $_POST['pos'] ) ) ? $_POST['pos'] : '';

	if( $position != '' && isset( $_POST['nl_subscribe_'. $position .'_nonce'] ) ) {
		if( wp_verify_nonce( $_POST['nl_subscribe_'. $position .'_nonce'], 'nl_subscribe_'. $position ) ) {

			$confirmation_message = '<h2>THANK YOU!</h2>
				<p>You have successfully signed up to receive email updates</p>';
			$error_message = '<h2>OOPS!</h2>
				<p>Something went wrong! Please try again later</p>';

			$sanitize_email = sanitize_email( $_POST[$position . '-email'] );
			$pass_validation = is_email( $sanitize_email );

			if( $pass_validation != false ) {
				$data = array(
					'email' => $sanitize_email,
					'brand' => $_POST['mbid'],
					'lists' => $_POST['sub'],
				);
				$format = array(
					'%s',
					'%s',
					'%s',
				);
				global $wpdb;

				$table_name = 'wp_newsletters';

				$success = $wpdb->insert( $table_name, $data, $format );

				//Add to Exact Target
				addToExactTarget($data);

				if( $success ) {
					echo $confirmation_message;
				} else {
					echo $error_message;
				}
			} else {
				echo 'Validation fail.';
			}
		} else {
			echo $error_message . ' (nonce)';
		}
		exit();
	}
}
add_action('init', 'newsletterprocess_request');

/**
 * Add to Exact Target
 * This will insert the Signup into the Exact Target Data Extension
 * optin-media-newsletters
 */
function addToExactTarget($newsletterArray){
	require_once(ABSPATH . 'wp-content/includes/FuelSDK/ET_Client.php');
	$myclient = new ET_Client();
	$dataextensionrow = new ET_DataExtension_Row();
	$dataextensionrow->authStub = $myclient;
	$dataextensionrow->Name = 'optin-media-newsletters';
	$date = DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''));
	$idDate = $date->format('YmdHisu');
	$dataextensionrow->props = array("id" => $idDate, "email" => $newsletterArray['email'],
	"brand" => $newsletterArray['brand'], "lists" => $newsletterArray['lists']);

	$results = $dataextensionrow->post();
	//print_r($results);
	if($results->status != 1){
		$message = $brand."\n\n".$results->results[0]->ErrorMessage."\n\n".$results->results[0]->StatusMessage."\n\n".$results->results[0]->StatusCode;
		mail ( "webmaster@competitorgroup.com" , "Exact Target Newsletter Signup Error" , $message);
	}
}

/**
 * Get category display
 * shows "SPONSORED CONTENT" if it's checked that way, otherwise it'll show the category name
 * appears at top left of post thumbnail
 */
function get_article_category_tag( $id, $cat_array ) {
	if( get_post_meta( $id, 'meta-checkbox', 1 ) == 'on' ) {
		$tag_display = 'SPONSORED CONTENT';
	} else {
		$tag_display = $cat_array['name'];
	}
	return $tag_display;
}



/**
 * Adds the featured image of a post if there are no other images uploaded.
 * also there's a meta option to insert the featured image to the top
 */
function insert_featured_image( $postid ) {
	if( get_post_meta( $postid, 'prevent_featured_image', 1 ) != 'on' ) {
		$thumb_id		= get_post_thumbnail_id( $postid );
		$thumb_array	= wp_get_attachment_image_src( $thumb_id, 'image-gallery' );
		$img_obj		= get_post( $thumb_id );

		if( !empty( $thumb_array[0] ) ) {

			$image_meta  = wp_get_attachment_metadata( $thumb_id );
			$widthFI = ( ! empty( $image_meta['width'] ) ? $image_meta['width'] : '' );
			$heightFI = ( ! empty( $image_meta['height'] ) ? $image_meta['height'] : '' );

			if( function_exists( 'is_amp_endpoint' ) && is_amp_endpoint() ) {

				echo '<figure>
					<amp-img class="aligncenter" src="'. $thumb_array[0] .'" layout="responsive" height="'.$heightFI.'" width="'.$widthFI.'"></amp-img>
					<figcaption class="wp-caption-text gallery-caption">
						<span>'. $img_obj->post_excerpt .'</span>
					</figcaption>
				</figure>';

			} else {

				echo '<figure class="img_sidebar_align">
					<img class="aligncenter" src="'. $thumb_array[0] .'" height="'.$heightFI.'" width="'.$widthFI.'">
					<figcaption class="wp-caption-text gallery-caption">
						<span>'. $img_obj->post_excerpt .'</span>
					</figcaption>
				</figure>';

			}

		}
	}

}


/*
*
*	keep up to date information on the teams from the category name, slug and description
*/

function create_team_converter($term_id = null, $taxonomy_id = null, $taxonomy = null) {

	// make sure term_id exists
	if( !empty($term_id) ) {

		// check if term belongs to "team" taxonomy
		$term = get_term_by( 'id', $term_id, 'team' );

		if( !empty( $term ) ) {

			// get team terms
			$terms = get_terms( 'team', array( 'hide_empty' => false ) );

			if( !empty( $terms ) ) {

				//create arrays for team abbreviations and names
				$team_array = [];
				$team_names_array = [];

				// create array for team abbreviation/names from taxonomy list
				foreach ($terms as $key => $value) {
					$team_arr_abbr = strtoupper($terms[$key]->slug);
					$team_array[$team_arr_abbr] = $terms[$key]->name;
				}

				//create array for team name-> proper cases
				foreach ($terms as $key => $value ) {
					$team_long_name = strtoupper($terms[$key]->name);
					$team_names_array[$team_long_name] = $terms[$key]->description;
				}

				//get the path to the file
				$includes_path = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/includes/';
				$team_path = $includes_path . 'convert-results/teams.php';

				//create correct format for the file output
				$team_content = '<?php $team_array = ';
				$team_content .= var_export( $team_array, true );
				$team_content .= ('; $team_names_array = ');
				$team_content .= var_export($team_names_array, true );
				$team_content .=  '; ?>';

				//erase the existing content and update the file with new team information
				file_put_contents($team_path, $team_content);
			}
		}
	}
}

add_action('edit_term', 'create_team_converter', 3);


/**
 * if sponsored, display it
 * available for categories, single posts (including longform), race home
 */
function get_presented_by() {

	$sponsorship					= array();
	$sponsorship['content']			= '';
	$sponsorship['sponsorclass']	= '';
	$post_id						= get_the_ID();

	if( is_category() ) {

		$cat_obj		= get_category( get_query_var( 'cat' ) );
		$cat_id			= ( !empty( $cat_obj->term_id ) ? $cat_obj->term_id : '' );
		$logo			= get_term_meta( $cat_id, '_vn_presentedby_logo', true );
		$url			= get_term_meta( $cat_id, '_vn_presentedby_url', true );
		$sponsor_name	= get_term_meta( $cat_id, '_vn_presentedby_name', true );

	} elseif( in_array( get_post_type(), array( 'races_event', 'stage' ) ) ) {

		/**
		 * get race category,
		 * then strip off the "-race" part,
		 * then get the id of that resulting category slug
		 * use that id to get the logo and url of a presenting sponsor
		 */
		$race_category	= wp_get_post_terms( $post_id, 'stagecat' );
		$race_category	= ( !empty( $race_category[0]->slug ) ? $race_category[0]->slug : '' );

		$racecat_slug	= str_replace( '-race', '', $race_category );
		$cat_id			= get_category_by_slug( $racecat_slug );
		$cat_id			= ( !empty( $cat_id->term_id) ? $cat_id->term_id : '' );

		$logo			= get_term_meta( $cat_id, '_vn_presentedby_logo', true );
		$url			= get_term_meta( $cat_id, '_vn_presentedby_url', true );
		$sponsor_name	= get_term_meta( $cat_id, '_vn_presentedby_name', true );

	} elseif( is_single() ) {
		/**
		 * check if the post is a "sponsored content" post
		 * if it is (checked ON), then don't show the sponsorship image
		 * this manages any conflicts that might happen if a sponsor buys a category,
		 * but another sponsor writes an article within that category
		 */
		$is_sponsored_content = get_post_meta( $post_id, 'meta-checkbox', true );

		if( $is_sponsored_content != 'on' ) {

			/**
			 * check first if post is sponsored
			 */
			$logo			= get_post_meta( $post_id, '_vn_presentedby_logo', true );
			$logo_id		= get_post_meta( $post_id, '_vn_presentedby_logo_id', true );
			$logo_width		= get_post_meta( $post_id, '_vn_presentedby_width', true );
			$sponsor_name	= get_post_meta( $post_id, '_vn_presentedby_name', true );
			$url			= get_post_meta( $post_id, '_vn_presentedby_url', true );

			/**
			 * if a sponsor logo wasn't found, then check if the post has a sponsor added to it
			 * iterate through each category to see if a sponsor logo is uploaded to it
			 * break out as soon as one is found
			 * in this implementation, single post sponsor has higher precedence over the category sponsor
			 */
			if( !empty( $logo ) ) {

				$cat_obj = get_the_category();

				foreach( $cat_obj as $cat ) {

					$logo = get_term_meta( $cat->term_id, '_vn_presentedby_logo', true );

					if( !empty( $logo ) ) {

						$logo_id		= get_term_meta( $cat->term_id, '_vn_presentedby_logo_id', true );
						$logo_width		= get_term_meta( $cat->term_id, '_vn_presentedby_width', true );
						$sponsor_name	= get_term_meta( $cat->term_id, '_vn_presentedby_name', true );
						$url			= get_term_meta( $cat->term_id, '_vn_presentedby_url', true );

						break;

					}

				}

			}

		}
	}

	if( !empty( $logo ) ) {

		$sponsorship['sponsorclass'] = ' content--sponsored';

		/**
		 * slightly modify the display of the logo for amp rendering
		 * remove http:
		 * include width/height
		 */
		if ( function_exists( 'is_amp_endpoint' ) && is_amp_endpoint() ) {

			$logo = str_replace( 'http:', '', $logo );

			$sponsorship['content'] .= '<div class="sponsorship">
				<p class="sponsorship__wrap">
					<span class="sponsorship__label">Presented by: </span>';

					if( !empty( $url ) ) {
						$sponsorship['content'] .= '<a href="'. $url .'"><amp-img width="'. $logo_width .'" height="25" src="'. $logo .'" alt="'. $sponsor_name .'" class="sponsorship__logo"></amp-img></a>';
					} else {
						$sponsorship['content'] .= '<amp-img width="'. $logo_width .'" height="25" src="'. $logo .'" alt="'. $sponsor_name .'" class="sponsorship__logo"></amp-img>';
					}

				$sponsorship['content'] .= '</p>
			</div>';

		} else {

			$sponsorship['content'] .= '<div class="sponsorship">
				<p class="sponsorship__wrap">
					<span class="sponsorship__label">Presented by: </span>';

					if( !empty( $url ) ) {
						$sponsorship['content'] .= '<a href="'. $url .'"><img src="'. $logo .'" alt="'. $sponsor_name .'" class="sponsorship__logo"></a>';
					} else {
						$sponsorship['content'] .= '<img src="'. $logo .'" alt="'. $sponsor_name .'" class="sponsorship__logo">';
					}

				$sponsorship['content'] .= '</p>
			</div>';

		}

	}

	return $sponsorship;
}
/**
 * Add custom post types for all future years and child categories of gift and buyer's guides
 * You're welcome future dev
 */
function changept() {
	if(is_category() || is_single()){
		$post_query =	get_queried_object();
		$post_category = isset( $post_query ) ? $post_query : false;
		if( $post_category !== false ) {
			$guide_cat			= $post_category->slug;
			$current_cat_link 	= get_category_link( $post_category->term_id );
			$parent 			= get_category( $post_category->parent );
			$current_cat_id		= $post_category->term_id;
				//get the current year
				$this_year = date("Y");
				//set array for categorie years

				$gg_cats = [];

				//add to the array for all years including this one >=
				for ( $i = $this_year; $i >= 2017 ; $i--) {
					$gg_cats[] = "{$i}-gift-guide";
				}
				for( $i = 2018; $i <= $this_year; $i++) {
					$gg_cats[] = "{$i}-buyers-guide";
				}

				$parent_term = is_wp_error( $parent ) ?  '' : $parent->slug;
			if( ( in_category( $gg_cats )&& !is_admin() ) || ( in_array( $parent_term , $gg_cats) ) ){

			    set_query_var( 'post_type', array( 'post', 'bike-reviews', 'apparel-reviews' ) );
			}
		}
	    return;
	}
}
add_action( 'parse_query','changept' );

/**
 * Add rewrite rule for the all-races page template
 */
function vn_add_rewrite_rules() {
	add_rewrite_rule( '^all-races/([0-9]+)/?', 'index.php?pagename=all-races&raceyear=$matches[1]', 'top' );

}
add_action( 'init', 'vn_add_rewrite_rules' );


function vn_query_vars( $aVars = array() ) {
	$aVars[] = 'raceyear';
	return $aVars;
}
add_filter( 'query_vars', 'vn_query_vars' );


/**
 * set up for select box options on race categories
 */

/**
 * Gets a number of terms and displays them as options
 * @param  string       $taxonomy Taxonomy terms to retrieve. Default is category.
 * @param  string|array $args     Optional. get_terms optional arguments
 * @return array                  An array of options that matches the CMB2 options array
 */
function get_stage_terms() {
	if( taxonomy_exists( 'stagecat' ) ) {
		$terms = get_terms( array(
		    'taxonomy' => 'stagecat',
		    'hide_empty' => false,
		) );

		// Initate an empty array
		$term_options = array();
		if ( !empty( $terms ) ) {
			foreach ( $terms as $term ) {
				$term_options[ $term->term_id ] = $term->name;
			}
		} else {
			$term_options = 'Please add a category';
		}
		return $term_options;
	}
}

/**
 * function: EU Regulation cookie set up
 */
function eu_regulation() {
	$cookie_name		= 'vn_eu_regulation';
	$eu_regulation_txt	= cgi_bikes_get_option( 'eu_regulation_txt' );

	if ( !isset( $_COOKIE[$cookie_name] ) && !empty( $eu_regulation_txt ) ) { ?>

		<section class="cookie-popup-bar" id="cookie-popup-bar">
			<a href="#" class="close-cookie-bar"></a>
			<?php echo $eu_regulation_txt; ?>
		</section>

		<script>
			// Make absolutely sure to display or not display the cookie bar (there might be no recognition due to cache)
			(function() {

				// Check for both localStorage data and cookie
				var cookieStatus	= document.cookie.indexOf( '<?php echo $cookie_name; ?>' ) !== -1 ? true : false;
				var storageStatus	= localStorage.getItem( '<?php echo $cookie_name; ?>' ) ? true : false;

				if ( cookieStatus || storageStatus ) {
					var regulations_bar	= document.getElementById('cookie-popup-bar');

					regulations_bar.style.display	= 'none';
				}
			} )();
		</script>

	<?php }
}


/**
 * Function creates post duplicate as a draft and redirects then to the edit post screen
 * attrib: https://rudrastyh.com/wordpress/duplicate-post.html
 */
function vn_duplicate_post_as_draft() {
	global $wpdb;

	if ( !( isset( $_GET['post'] ) || isset( $_POST['post'] )  || ( isset($_REQUEST['action'] ) && 'vn_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
		wp_die( 'No post to duplicate has been supplied!' );
	}

	/**
	 * get the original post id
	 */
	$post_id = ( isset( $_GET['post'] ) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );

	/**
	 * and all the original post data then
	 */
	$post = get_post( $post_id );

	/**
	 * if you don't want current user to be the new post author,
	 * then change next couple of lines to this: $new_post_author = $post->post_author;
	 */
	// $current_user		= wp_get_current_user();
	$new_post_author	= $post->post_author;

	/**
	 * if post data exists, create the post duplicate
	 */
	if( isset( $post ) && $post != null ) {

		/**
		 * new post data array
		 */
		$args = array(
			'comment_status'	=> $post->comment_status,
			'ping_status'		=> $post->ping_status,
			'post_author'		=> $new_post_author,
			'post_content'		=> $post->post_content,
			'post_excerpt'		=> $post->post_excerpt,
			'post_name'			=> $post->post_name,
			'post_parent'		=> $post->post_parent,
			'post_password'		=> $post->post_password,
			'post_status'		=> 'draft',
			'post_title'		=> $post->post_title,
			'post_type'			=> $post->post_type,
			'to_ping'			=> $post->to_ping,
			'menu_order'		=> $post->menu_order
		);

		/**
		 * insert the post by wp_insert_post() function
		 */
		$new_post_id = wp_insert_post( $args );

		/**
		 * get all current post terms ad set them to the new post draft
		 */
		$taxonomies = get_object_taxonomies( $post->post_type ); // returns array of taxonomy names for post type, ex array("category", "post_tag");

		foreach( $taxonomies as $taxonomy ) {
			$post_terms = wp_get_object_terms( $post_id, $taxonomy, array( 'fields' => 'slugs' ) );
			wp_set_object_terms( $new_post_id, $post_terms, $taxonomy, false );
		}

		/**
		 * duplicate all post meta just in two SQL queries
		 */
		$post_meta_infos = $wpdb->get_results( "SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id = $post_id" );

		if( count( $post_meta_infos ) != 0 ) {
			$sql_query = "INSERT INTO $wpdb->postmeta ( post_id, meta_key, meta_value ) ";

			foreach( $post_meta_infos as $meta_info ) {
				$meta_key			= $meta_info->meta_key;
				$meta_value			= addslashes( $meta_info->meta_value );
				$sql_query_sel[]	= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}

			$sql_query.= implode( " UNION ALL ", $sql_query_sel );
			$wpdb->query( $sql_query );
		}


		/**
		 * finally, redirect to the edit post screen for the new draft
		 */
		wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
		exit;
	} else {
		wp_die( 'Post creation failed, could not find original post: ' . $post_id );
	}
}
add_action( 'admin_action_vn_duplicate_post_as_draft', 'vn_duplicate_post_as_draft' );


/**
 * Add the duplicate link to action list for post_row_actions
 */
function vn_duplicate_post_link( $actions, $post ) {

	if ( $post->post_type == 'all-races' && current_user_can( 'edit_posts' ) ) {

		$actions['duplicate'] = '<a href="admin.php?action=vn_duplicate_post_as_draft&amp;post=' . $post->ID . '" title="Duplicate this item" rel="permalink">Duplicate</a>';

	}

	return $actions;
}

add_filter( 'post_row_actions', 'vn_duplicate_post_link', 10, 2 );


/**
 * Get Meta
 *
 * Utility function used to consolidate the quering of multiple meta values
 * for the given object.
 *
 * @param int	 	$id ID of the current object.
 * @param mixed		$fields Array/string containing meta field(s) to retrieve from database.
 * @param string	$type Type of metadata request. Options: post/term/user
 * @param constant	$output pre-defined constant for return type (OBJECT/ARRAY_A)
 *
 * @return mixed	MySQL object/Associative Array containing returned post metadata.
 */
function get_meta( $id = null, $fields = array(), $type = 'post', $output = ARRAY_A ) {
	global $wpdb;

	$fields		= esc_sql( $fields );
	$values_arr	= array();
	$values_obj	= new stdClass();
	$dbtable	= $wpdb->{$type.'meta'};
	$column_id	= $type . '_id';
	$id			= $id == null ? get_the_ID() : $id ;

	$query		= "SELECT meta_key, meta_value FROM {$dbtable} WHERE {$column_id} = {$id}";

	if ( !empty( $fields ) ) {

		if ( is_array( $fields ) ) {
			$query .= " AND meta_key IN ('". implode("','", $fields) ."')";
		} else {
			$query .= " AND meta_key = '{$fields}'";
		}
	}

	$results	=  $wpdb->get_results( $query, OBJECT_K );


	foreach ( $results as $key => $result ) {
		$values_arr[$key]	= $result->meta_value;
		$values_obj->{$key}	= $result->meta_value;
	}

	if ( !is_array( $fields ) && !empty( $values_arr[$fields] ) ) {

		return $output == ARRAY_A ? $values_arr[$fields] : $values_obj[$fields];

	}

	return $output == ARRAY_A ? $values_arr : $values_obj;
}
