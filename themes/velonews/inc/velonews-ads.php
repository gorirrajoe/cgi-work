<?php

/**
 * Class holding anything we need to mod for the WP DFP Ads plugin
 */

class VeloNews_Ads {

	static $instance = false;

	public function __construct() {

		// front-end hooks
		// add_filter( 'wp_dfp_ads_filter', array( $this, '_filter_inarticle' ) );
		add_filter( 'wp_dfp_ads_filter', array( $this, '_filter_inarticle_alt' ) );
		// add_filter( 'wp_dfp_ads_filter', array( $this, '_filter_middle_archive' ) );
		add_filter( 'wp_dfp_ads_filter', array( $this, '_filter_middle_standard' ) );
		add_filter( 'wp_dfp_ads_keywords', array( $this, '_filter_keywords' ) );

	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}

	/**
	 * Unset any inarticle ad if there was not enough content for the ad to display
	 *
	 * @param  array $ads Contains all the ads that we are preparing to display
	 * @return array      $ads excluding/inlcuding inarticle ads, depending on the logic happening
	 */
	public function _filter_inarticle( $ads ) {

		if ( is_single() ) {

			global $post;

			$content		= $post->post_content;
			$post_content	= apply_filters( 'the_content', $content );
			$liveblog		= get_post_meta( get_the_ID(), '_liveblog', true );

			// if the ad call is not found inside the content, then pop the inarticle ad out of the $ads
			if ( !strpos( $post_content, 'advert_location_inline' ) || $liveblog == 'on' ) {

				foreach ( $ads as $key => $ad ) {

					foreach ( $ad['advert-slots'] as $i => $slot ) {

						if ( strpos( $slot, 'inarticle' ) )  { unset($ads[$key]); }

					} // end foreach $ad['slots']

				} // end foreach $ads

			} // end strpos

		} // end is_single()

		return $ads;

	}

	public function _filter_inarticle_alt( $ads ) {

		if ( is_single() ) {

			global $post;

			$content		= $post->post_content;
			$post_content	= apply_filters( 'the_content', $content );
			$liveblog		= get_post_meta( get_the_ID(), '_liveblog', true );

			// Get all inarticle ads (by location)
			$advert_locations	= get_terms( array(
				'taxonomy'	=> 'advert-location',
				'fields'	=> 'slugs',
			) );

			$inarticle_locations	= array_filter( $advert_locations, function( $value ) {
				if ( substr( $value, 0, strlen('inarticle') ) == 'inarticle' ) {
					return $value;
				}
			} );

			// Reverse the array so we look for the numbered inarticle ads first
			$inarticle_locations	= array_reverse( $inarticle_locations );

			foreach ( $inarticle_locations as $location ) {

				// if the ad call is not found inside the content, then pop the inarticle ad out of the $ads
				if ( !strpos( $post_content, $location ) ||  $liveblog == 'on'  ) {

					foreach ( $ads as $key => $ad ) {

						foreach ( $ad['advert-slots'] as $i => $slot ) {

							if ( strpos( $slot, $location ) !== false ) { unset($ads[$key]); }

						} // end foreach $ad['slots']

					} // end foreach $ads

				} // end strpos

			}

		} // end is_single()

		return $ads;

	}

	/**
	 * Unset the middle-archive ad if there is not enough articles to display (displays after 3 posts)
	 *
	 * @param  array $ads Contains all the ads that we are preparing to display
	 * @return array      $ads excluding/inlcuding middle ads, depending on the logic happening
	 */
	public function _filter_middle_archive( $ads ) {

		if ( is_category() ) {

			$cat_id			= get_queried_object()->cat_ID;
			$cat_meta		= Triathlete_Theme::get_meta( $cat_id, array(), 'term' );
			$archive_type	= !empty( $cat_meta['_vn_archive_type'] ) ? $cat_meta['_vn_archive_type'] : '';
			$page			= get_query_var( 'paged', 1 );

			if ( $archive_type == 'extended' && $page == 1  && empty( $_GET['categories'] ) && empty( $_GET['tags'] ) ) {

				foreach ( $ads as $key => $ad ) {

					foreach ( $ad['slots'] as $i => $slot ) {

						if ( false !== strpos( $slot, 'middle_archive' ) )  { unset( $ads[$key] ); }

					} // end foreach $ad['slots']

				} // end foreach $ads

			}

		}

		if (
			is_author()
			|| is_tag()
			|| is_archive()
			|| ( is_category() && isset( $archive_type ) && $acrhive_type != 'extended' )
			|| ( is_category() && isset( $page, $archive_type ) && $page > 1 && $archive_type == 'extended' )
		) {

			$term_obj	= get_queried_object();

			// get the author post count if we are on author archive, else use term_obj count
			$post_count	= is_author() ? count_user_posts( $term_obj->ID ) : $term_obj->count;

			if ( $post_count < 3  ) {

				foreach ( $ads as $key => $ad ) {

					foreach ( $ad['slots'] as $i => $slot ) {

						if ( false !== strpos( $slot, 'middle_archive' ) )  { unset( $ads[$key] ); }

					} // end foreach $ad['slots']

				} // end foreach $ads

			} // end if $post_count

		} // end is_

		return $ads;

	}

	/**
	 * Unset the middle standard ad if the category page is/is not an extended type
	 *
	 * @param  array $ads Contains all the ads that we are preparing to display
	 * @return array      $ads excluding/inlcuding middle ads, depending on the logic happening
	 */
	public function _filter_middle_standard( $ads ) {

		if ( is_category() ) {

			$term_obj		= get_queried_object();
			$term_ID		= $term_obj->cat_ID;
			$term_meta		= get_metadata( 'term', $term_ID );
			$archive_type	= !empty( $term_meta['_vn_archive_type'] ) ? $term_meta['_vn_archive_type'][0] : '';
			$page			= get_query_var( 'paged', 1 );
			$post_count		= $term_obj->count;

			if ( $archive_type != 'extended' || ( $archive_type == 'extended' && $page > 1 && $post_count > 3 ) ) {

				foreach ( $ads as $key => $ad ) {

					foreach ( $ad['advert-slots'] as $i => $slot ) {

						if (
							strpos( $slot, 'middle' ) !== false
							&& strpos( $slot, 'side' ) === false
							&& strpos( $slot, 'archive' ) === false
						) {
							unset( $ads[$key] );
						}

					} // end foreach $ad['slots']

				} // end foreach $ads

			} // end if $post_count

		} // end is_

		return $ads;

	}

	public function _filter_keywords( $suffix ) {

		if ( is_category() ) {

			$category = get_queried_object();

			/**
			 * Every category page should return it's parents ( if exist ), then
			 * itself.
			 */
			if ( !empty( $category->category_parent ) ) {

				// this will already have the suffix with the proper set up,
				// but we need it to have underscored instead of dashes
				// so just do that
				$suffix	= str_replace( '-', '_', $suffix );

			} else {

				// There's a few rogue categories they DID use dashes in...so make an exception for those
				// and these have to be specific to our URLs...
				if ( in_array( $category->slug, array( 'tour-de-france' ) ) ) {

					$suffix = '/'. Wp_Dfp_Ads::_sanitize_term( $category->slug );

				} else {

					$suffix = '/'. str_replace( '-', '_', Wp_Dfp_Ads::_sanitize_term( $category->slug ) );

				}

			}

		} elseif ( is_single() ) {

			global $post;

			$categories = ( $post->post_parent == '0' ? get_the_category( $post->ID ) : get_the_category( $post->post_parent ) );

			$keywords			= array();
			$author				= false;
			$sponsored_content	= false;

			// clear out the $suffix
			$suffix	= '';

			$prioritized_categories	= array(
				'bikes-and-tech',
				'news',
				'sponsored',
				'partner-connect',
				'gallery',
				'video',
				'velolife',
				'tour-of-utah',
				'tour-de-france',
				'race-results',
				'olympics',
				'home',
				'giro-ditalia',
				'gift-guide',
				'bike-reviews-rankings',
				'apparel-search',
				'analysis',
				'all-races',
			);

			// If primary category exists, make it a part of the prioritized categories
			if ( function_exists('yoast_get_primary_term_id') ) {
				$primary_cat_ID	= yoast_get_primary_term_id();

				// only do this if there was a primary category
				if ( false !== $primary_cat_ID ) {

					$primary_cat_data	= get_term( $primary_cat_ID );
					$primary_cat_slug	= $primary_cat_data->slug;

					// add if it's not already a part of the prioritized categories
					// gets lowest priority on the list
					if ( !in_array( $primary_cat_slug, $prioritized_categories ) ) {
						$prioritized_categories[]	= $primary_cat_slug;
					} else {
						// else if it does exist, throw it to the top of the prioritized list
						unset( $prioritized_categories[$primary_cat_slug] );
						$primary_cat_slug_array	= array( $primary_cat_slug );
						$prioritized_categories	= $primary_cat_slug_array + $prioritized_categories;
					}

				}

			}

			if ( in_category( $prioritized_categories ) ) {

				// sort the categories based on our AdOps provided list
				usort( $categories, function( $a, $b ) use( $prioritized_categories ) {
					// If $a category is in the prioritized categories but $b is not, $a should be bumped up
					if ( in_array( $a->slug, $prioritized_categories ) && !in_array( $b->slug, $prioritized_categories ) ) {
						return -1;
					} elseif ( !in_array( $a->slug, $prioritized_categories ) && in_array( $b->slug, $prioritized_categories ) ) {
						// else if vice versa of previous
						return 1;
					} elseif ( in_array( $a->slug, $prioritized_categories ) && in_array( $b->slug, $prioritized_categories ) ) {
						// else if both are in the prioritized categories, figure out which is higher up the list
						$a_key	= array_search( $a->slug, $prioritized_categories );
						$b_key	= array_search( $b->slug, $prioritized_categories );

						return ( $a_key < $b_key ) ? -1 : 1;
					} else {
						return 0;
					}

				} );

			}

			foreach ( $categories as $category ) {

				$parents = get_category_parents( $category->cat_ID );

				if ( !is_wp_error( $parents ) && !empty( $parents ) ) {

					foreach ( explode( '/', $parents ) as $cat ) {

						$cat = class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::_sanitize_term( $cat ) : $cat;
						$cat = str_replace( '-', '_', $cat );

						if ( !empty( $cat ) && !in_array( $category->slug, $keywords ) ) {

							$keywords[] = $category->slug;

							// sponsored needs the author in the ad call
							// and MUST be '/sponsored' even if there are other categories
							if ( 'sponsored' === $category->slug || $sponsored_content == true ) {
								$suffix 			= '/'. $category->slug;
								$author				= true;
								$sponsored_content	= true;
							} elseif ( in_array( $category->slug, array( 'tour-de-france' ) ) ) {

								$suffix .= '/'. $category->slug;

							} else {
								$cat	= $category->slug;
								$cat	= str_replace( '-', '_', $cat );
								$suffix	.= "/$cat";

							}

						}

					}

					// adding author to keywords for sponsored content
					if ( true === $author ) {

						$author_name = get_the_author_meta( 'login', $post->post_author );

						if ( $author_name != ',' && $author_name != '' ) {

							$author_name = Wp_Dfp_Ads::_sanitize_term( $author_name );

							if ( !empty( $author_name ) && !in_array( $author_name, $keywords ) ) {

								$keywords[] = $author_name;
								$suffix .= "/{$author_name}";

							}

						}

						// if we added the author, it means we were in a sponsored post
						// so breakout of the categories, no need to loop through the rest of the categories
						break;

					}

				}

			}

		}

		return $suffix;

	}

}
