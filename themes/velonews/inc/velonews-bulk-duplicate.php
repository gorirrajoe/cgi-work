<?php
	class VeloNews_Custom_Bulk_Action {

		public static $instance = false;

		public function __construct() {
			$this->_add_actions();
		}


		/**
		 * Step 1: add the custom Bulk Action to the select menus
		 */
		public function custom_bulk_admin_footer() {
			global $post_type;

			if( $post_type == 'all-races' ) { ?>
				<script type="text/javascript">
					jQuery( document ).ready( function() {
						jQuery( '<option>' ).val( 'duplicate' ).text( '<?php _e( 'Duplicate' )?>' ).appendTo( "select[name='action']" );
						jQuery( '<option>' ).val( 'duplicate' ).text( '<?php _e( 'Duplicate' )?>' ).appendTo( "select[name='action2']" );
					} );
				</script>
			<?php }
		}


		/**
		 * Step 2: handle the custom Bulk Action
		 *
		 * Based on the post http://wordpress.stackexchange.com/questions/29822/custom-bulk-action
		 */
		public function custom_bulk_action() {
			global $typenow;
			$post_type = $typenow;

			if( $post_type == 'all-races' ) {

				// get the action
				// depending on your resource type this could be WP_Users_List_Table, WP_Comments_List_Table, etc
				$wp_list_table	= _get_list_table( 'WP_Posts_List_Table' );
				$action			= $wp_list_table->current_action();

				$allowed_actions = array( "duplicate" );
				if( !in_array( $action, $allowed_actions ) ) return;

				// security check
				check_admin_referer( 'bulk-posts' );

				// make sure ids are submitted. depending on the resource type, this may be 'media' or 'ids'
				if( isset( $_REQUEST['post'] ) ) {
					$post_ids = array_map( 'intval', $_REQUEST['post'] );
				}

				if( empty( $post_ids ) ) return;

				// this is based on wp-admin/edit.php
				$sendback = remove_query_arg( array( 'duplicated', 'untrashed', 'deleted', 'ids' ), wp_get_referer() );

				if ( ! $sendback )
					$sendback = admin_url( "edit.php?post_type=$post_type" );

				$pagenum	= $wp_list_table->get_pagenum();
				$sendback	= add_query_arg( 'paged', $pagenum, $sendback );

				switch( $action ) {
					case 'duplicate':

						// if we set up user permissions/capabilities, the code might look like:
						//if ( !current_user_can( $post_type_object->cap->export_post, $post_id ) )
						//	wp_die( __( 'You are not allowed to export this post.' ) );

						$duplicated = 0;
						foreach( $post_ids as $post_id ) {

							if ( !$this->perform_dupe( $post_id ) )
								wp_die( __( 'Error duplicating post.' ) );

							$duplicated++;
						}

						$sendback = add_query_arg( array( 'duplicated' => $duplicated, 'ids' => join( ',', $post_ids ) ), $sendback );
					break;

					default: return;
				}

				$sendback = remove_query_arg( array( 'action', 'action2', 'tags_input', 'post_author', 'comment_status', 'ping_status', '_status', 'post', 'bulk_edit', 'post_view' ), $sendback );

				wp_redirect( $sendback );
				exit();
			}
		}


		/**
		 * Step 3: display an admin notice on the Posts page after exporting
		 */
		public function custom_bulk_admin_notices() {
			global $post_type, $pagenow;

			if( $pagenow == 'edit.php' && $post_type == 'all-races' && isset( $_REQUEST['duplicated'] ) && ( int ) $_REQUEST['duplicated'] ) {
				$message = sprintf( _n( 'Post duplicated.', '%s posts duplicated.', $_REQUEST['duplicated'] ), number_format_i18n( $_REQUEST['duplicated'] ) );
				echo "<div class=\"updated\"><p>{$message}</p></div>";
			}
		}

		public function perform_dupe( $post_id ) {
			global $wpdb;
			$post = get_post( $post_id );

			/**
			 * new post data array
			 */
			$args = array(
				'comment_status'	=> $post->comment_status,
				'ping_status'		=> $post->ping_status,
				'post_author'		=> $post->post_author,
				'post_content'		=> $post->post_content,
				'post_excerpt'		=> $post->post_excerpt,
				'post_name'			=> $post->post_name,
				'post_parent'		=> $post->post_parent,
				'post_password'		=> $post->post_password,
				'post_status'		=> 'draft',
				'post_title'		=> $post->post_title,
				'post_type'			=> $post->post_type,
				'to_ping'			=> $post->to_ping,
				'menu_order'		=> $post->menu_order
			);

			/**
			 * insert the post by wp_insert_post() function
			 */
			$new_post_id = wp_insert_post( $args );

			/**
			 * get all current post terms ad set them to the new post draft
			 */
			$taxonomies = get_object_taxonomies( $post->post_type ); // returns array of taxonomy names for post type, ex array("category", "post_tag");

			foreach( $taxonomies as $taxonomy ) {
				$post_terms = wp_get_object_terms( $post_id, $taxonomy, array( 'fields' => 'slugs' ) );
				wp_set_object_terms( $new_post_id, $post_terms, $taxonomy, false );
			}

			/**
			 * duplicate all post meta just in two SQL queries
			 */
			$post_meta_infos = $wpdb->get_results( "SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id = $post_id" );

			if( count( $post_meta_infos ) != 0 ) {
				$sql_query = "INSERT INTO $wpdb->postmeta ( post_id, meta_key, meta_value ) ";

				foreach( $post_meta_infos as $meta_info ) {
					$meta_key			= $meta_info->meta_key;
					$meta_value			= addslashes( $meta_info->meta_value );
					$sql_query_sel[]	= "SELECT $new_post_id, '$meta_key', '$meta_value'";
				}

				$sql_query.= implode( " UNION ALL ", $sql_query_sel );
				$wpdb->query( $sql_query );
			}

			return true;
		}


		/**
		 * Singleton
		 *
		 * Returns a single instance of the current class.
		 */
		public static function singleton() {

			if ( ! self::$instance )
				self::$instance = new self();

			return self::$instance;
		}


		/**
		 * Add Actions
		 *
		 * Defines all the WordPress actions and filters used by this class.
		 */
		protected function _add_actions() {

			// admin actions/filters
			add_action( 'admin_footer-edit.php', array( &$this, 'custom_bulk_admin_footer' ) );
			add_action( 'load-edit.php', array( &$this, 'custom_bulk_action' ) );
			add_action( 'admin_notices', array( &$this, 'custom_bulk_admin_notices' ) );

		}
	}
