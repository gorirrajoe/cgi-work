<?php

	class VELO_AMP_Content_Sanitizer extends AMP_Base_Sanitizer {
		public function sanitize() {
			$body = $this->get_body_node();

			// Replace iframe src from http: to https:
			foreach ( $body->getElementsByTagName('amp-iframe') as $iframe ) {
				// We've let the AMP plugin handle most of the iframe clean up
				// but need to freshen up even a little more
				$iframe->setAttribute(
					'layout',
					'responsive'
				);

				$iframe->removeAttribute('sizes');
			}

			foreach ( $body->getElementsByTagName('a') as $anchor_tag ) {
				$anchor_tag->removeAttribute('shape');
			}

			// Remove nowrap attributes from cells
			foreach ( $body->getElementsByTagName('td') as $cell_tag ) {
				$cell_tag->removeAttribute('nowrap');
			}

			// Remove width tag from hr elements
			foreach ( $body->getElementsByTagName('hr') as $break_tag ) {
				$break_tag->removeAttribute('width');
			}
		}
	}
