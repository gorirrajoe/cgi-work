<?php

/**
 * Custom Taxonomies | Apparel Reviews
 *
 * This taxonomy was created for the purpose of allowing the VeloNews Mega Menu
 * to include a link to the "Apparel Review" page along with the most recent
 * "apparel-reviews" posts. This taxonomy is being hidden from all users via
 * CSS as it's not being used in the traditional taxonomy context.
 *
 * Because the taxonomy is being hidden, you must take the following steps to
 * add a new term to the taxonomy. **Note:** There must be at least one term in
 * the taxonomy for the Mega Menu logic to work. It doesn't matter what the term
 * is, but one must be defined.
 *
 * 1. Go to <domain>/wp-admin/edit-tags.php?taxonomy=apparel-reviews-cat
 * 2. Create a new term
 * 3. Save term
 * 4. Go To Appearance > Menus > Apparel Review Categories > View All
 * 5. Select the term you added, and place in menu where desired
 * 6. Save the menu
 *
 * @link https://codex.wordpress.org/Function_Reference/register_taxonomy
 */
class VeloNews_Taxonomy_Apparel {

	static $instance = false;

	public function __construct() {

		$this->register_taxonomy_apparelreviews();

	}


	protected function register_taxonomy_apparelreviews() {

		$labels = array(
			'name'							=> _x( 'Apparel Review Categories', 'velonews' ),
			'singular_name'					=> _x( 'Apparel Review Category', 'velonews' ),
			'search_items'					=> _x( 'Search Apparel Review Categories', 'velonews' ),
			'popular_items'					=> _x( 'Popular Apparel Review Categories', 'velonews' ),
			'all_items'						=> _x( 'All Apparel Review Categories', 'velonews' ),
			'parent_item'					=> _x( 'Parent Apparel Review Category', 'velonews' ),
			'parent_item_colon'				=> _x( 'Parent Apparel Review Category:', 'velonews' ),
			'edit_item'						=> _x( 'Edit Apparel Review Category', 'velonews' ),
			'update_item'					=> _x( 'Update Apparel Review Category', 'velonews' ),
			'add_new_item'					=> _x( 'Add New Apparel Review Category', 'velonews' ),
			'new_item_name'					=> _x( 'New Apparel Review Category', 'velonews' ),
			'separate_items_with_commas'	=> _x( 'Separate Apparel Review Categories with commas', 'velonews' ),
			'add_or_remove_items'			=> _x( 'Add or remove Apparel Review Categories', 'velonews' ),
			'choose_from_most_used'			=> _x( 'Choose from the most used Apparel Review Categories', 'velonews' ),
			'menu_name'						=> _x( 'Apparel Review Categories', 'velonews' ),
		);

		$args = array(
			'labels'			=> $labels,
			'public'			=> true,
			'show_in_nav_menus'	=> true,
			'show_ui'			=> true,
			'show_tagcloud'		=> false,
			'show_admin_column'	=> false,
			'hierarchical'		=> true,
			'rewrite'			=> true,
			'query_var'			=> true
		);

		register_taxonomy( 'apparel-reviews-cat', array('apparel-reviews'), $args );
	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


}
