<?php
class VeloNews_Taxonomy_Stages {

	static $instance = false;

	public function __construct() {
		$this->stages_register_taxonomy();
		$this->years_register_taxonomy();
		$this->teams_register_taxonomy();
	}

	// register two taxonomies to go with the post type
	protected function stages_register_taxonomy() {
		// set up labels
		$labels = array(
			'name'              => 'Race Categories',
			'singular_name'     => 'Race Category',
			'search_items'      => 'Search Race Categories',
			'all_items'         => 'All Race Categories',
			'edit_item'         => 'Edit Race Category',
			'update_item'       => 'Update Race Category',
			'add_new_item'      => 'Add New Race Category',
			'new_item_name'     => 'New Race Category',
			'menu_name'         => 'Race Categories'
		);
		$args = array(
			'hierarchical'		=> true,
			'labels'			=> $labels,
			'query_var'			=> true,
			'show_admin_column'	=> true
		);
		// register taxonomy
		register_taxonomy( 'stagecat',
			array(
				'stage',
				'races_event',
				'race_result',
				'post'
			),
			$args
		);
	}

	protected function years_register_taxonomy() {
		// set up labels
		$labels = array(
			'name'              => 'Race Year Categories',
			'singular_name'     => 'Race Year Category',
			'search_items'      => 'Search Race Year Categories',
			'all_items'         => 'All Race Year Categories',
			'edit_item'         => 'Edit Race Year Category',
			'update_item'       => 'Update Race Year Category',
			'add_new_item'      => 'Add New Race Year Category',
			'new_item_name'     => 'New Race Year Category',
			'menu_name'         => 'Race Year Categories'
		);
		// register taxonomy
		register_taxonomy( 'stageyear', array('stage','races_event', 'race_result', 'post'), array(
			'hierarchical'		=> true,
			'labels'			=> $labels,
			'query_var'			=> true,
			'show_admin_column'	=> true
		) );
	}

	protected function teams_register_taxonomy() {
		// set up labels
		$labels = array(
			'name'              => 'Team Categories',
			'singular_name'     => 'Team Category',
			'search_items'      => 'Search Team Categories',
			'all_items'         => 'All Team Categories',
			'edit_item'         => 'Edit Team Category',
			'update_item'       => 'Update Team Category',
			'add_new_item'      => 'Add New Team Category',
			'new_item_name'     => 'New Team Category',
			'menu_name'         => 'Team Categories'
		);
		// register taxonomy
		register_taxonomy( 'team', array('post'), array(
			'hierarchical'		=> true,
			'labels'			=> $labels,
			'query_var'			=> true,
			'show_admin_column'	=> true
		) );
	}



	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}

}
