<?php

/**
 * Custom Taxonomies | Bike Reviews
 *
 * This taxonomy was created for the purpose of allowing the VeloNews Mega Menu
 * to include a link to the "Bike Review" page along with the most recent
 * "bike-reviews" posts. This taxonomy is being hidden from all users via
 * CSS as it's not being used in the traditional taxonomy context.
 *
 * Because the taxonomy is being hidden, you must take the following steps to
 * add a new term to the taxonomy. **Note:** There must be at least one term in
 * the taxonomy for the Mega Menu logic to work. It doesn't matter what the term
 * is, but one must be defined.
 *
 * 1. Go to <domain>/wp-admin/edit-tags.php?taxonomy=bike-reviews-cat
 * 2. Create a new term
 * 3. Save term
 * 4. Go To Appearance > Menus > Bike Review Categories > View All
 * 5. Select the term you added, and place in menu where desired
 * 6. Save the menu
 *
 * @link https://codex.wordpress.org/Function_Reference/register_taxonomy
 */

class VeloNews_Taxonomy_Bike {

	static $instance = false;

	public function __construct() {

		$this->register_taxonomy_bikereviews();

	}


	protected function register_taxonomy_bikereviews() {
		$labels = array(
			'name'							=> _x( 'Bike Review Categories', 'velonews' ),
			'singular_name'					=> _x( 'Bike Review Category', 'velonews' ),
			'search_items'					=> _x( 'Search Bike Review Categories', 'velonews' ),
			'popular_items'					=> _x( 'Popular Bike Review Categories', 'velonews' ),
			'all_items'						=> _x( 'All Bike Review Categories', 'velonews' ),
			'parent_item'					=> _x( 'Parent Bike Review Category', 'velonews' ),
			'parent_item_colon'				=> _x( 'Parent Bike Review Category:', 'velonews' ),
			'edit_item'						=> _x( 'Edit Bike Review Category', 'velonews' ),
			'update_item'					=> _x( 'Update Bike Review Category', 'velonews' ),
			'add_new_item'					=> _x( 'Add New Bike Review Category', 'velonews' ),
			'new_item_name'					=> _x( 'New Bike Review Category', 'velonews' ),
			'separate_items_with_commas'	=> _x( 'Separate Bike Review Categories with commas', 'velonews' ),
			'add_or_remove_items'			=> _x( 'Add or remove Bike Review Categories', 'velonews' ),
			'choose_from_most_used'			=> _x( 'Choose from the most used Bike Review Categories', 'velonews' ),
			'menu_name'						=> _x( 'Bike Review Categories', 'velonews' ),
		);

		$args = array(
			'labels'			=> $labels,
			'public'			=> true,
			'show_in_nav_menus'	=> true,
			'show_ui'			=> true,
			'show_tagcloud'		=> false,
			'show_admin_column'	=> false,
			'hierarchical'		=> true,
			'rewrite'			=> true,
			'query_var'			=> true
		);

		register_taxonomy( 'bike-reviews-cat', array( 'bike-reviews' ), $args );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


}

