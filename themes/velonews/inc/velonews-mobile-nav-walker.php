<?php

/**
 * nav walker for mobile sticky nav
 */
class MobileSticky_Walker_Nav_Menu extends Walker_Nav_Menu {
	function start_lvl( &$output, $depth = 0, $args = Array() ) {
		if( $depth != 1 ) {
			$output .= '<ul class="mobile-nav__sub-menu" aria-haspopup="true">';
		} else {
			$output .= '<ul>';
		}
	}


	function end_lvl( &$output, $depth = 0, $args = Array() ) {
		$output .= '</ul>';
	}

	function start_el( &$output, $item, $depth = 0, $args = Array(), $id = 0 ) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = $depthclass = '';

		$classes = empty( $item->classes ) ? array() : ( array ) $item->classes;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );

		if( $args->walker->has_children == 1 && $depth == 0 ) {
			$class_names = ' class="'. esc_attr( $class_names ) . $depthclass . ' mobile-nav__menu-item dropdown-nav"';
			$output .= $indent . '<li id="mobile-nav-toggle" ' . $value . $class_names .' aria-haspopup="true">';
		} elseif( $args->walker->has_children == 0 && $depth == 0 ) {
			$class_names = ' class="'. esc_attr( $class_names ) . $depthclass . ' mobile-nav__menu-item"';
			$output .= $indent . '<li id="menu-item-'. $item->ID . '" ' . $value . $class_names .' aria-haspopup="true">';
		} elseif( $args->walker->has_children == 1 && $depth == 1 ) {
			$class_names = ' class="'. esc_attr( $class_names ) . $depthclass . ' mobile-nav__sub-menu-item dropdown-nav"';
			$output .= $indent . '<li id="menu-item-'. $item->ID . '" ' . $value . $class_names .' aria-haspopup="true">';
		} elseif( $args->walker->has_children == 0 && $depth == 1 ) {
			$class_names = ' class="'. esc_attr( $class_names ) . $depthclass . ' mobile-nav__sub-menu-item"';
			$output .= $indent . '<li id="menu-item-'. $item->ID . '" ' . $value . $class_names .' aria-haspopup="true">';
		} else {
			$class_names = ' class="'. esc_attr( $class_names ) . $depthclass . '"';
			$output .= $indent . '<li id="menu-item-'. $item->ID . '" ' . $value . $class_names .' aria-haspopup="true">';
		}

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

		$prepend = '';
		$append = '';

		$item_output = $args->before;
		$item_output .= '<a'. $attributes . '>';
		if( $depth == 0 ) {
			$item_output .= '<span class="fa '. $item->description .'"></span>';
		}
		$item_output .= $args->link_before . $prepend . apply_filters( 'the_title', $item->title, $item->ID ) . $append;
		if( $args->walker->has_children == 1 && $depth == 1 ) {
			$item_output .= '<span class="fa fa-caret-down"></span>';
		}
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}
