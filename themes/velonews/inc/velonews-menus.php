<?php

class VeloNews_Menus {

	public static $instance = false;

	protected $transient_prefix = 'velonews-menu-';

	public function __construct() {
		$this->_add_actions();
	}

	/**
	 * Do WP Update Nav Menu
	 *
	 * Determines whether or not to re-generate the navigation markup when
	 * "wp_update_nav_menu" is fired.
	 *
	 * @param int $menu_id ID of the updated menu
	 * @param array $menu_data An array of menu data
	 */
	public function do_wp_update_nav_menu( $menu_id = null, $menu_data = array() ) {

		$locations = array_flip( get_nav_menu_locations() );

		if ( isset( $locations[$menu_id] ) ) {
			delete_transient( $this->transient_prefix . $locations[$menu_id] );
		}

	}

	/**
	 * Checks if there's cached menu markup based on the theme_location parameter
	 *
	 * @param  mixed	$output	Markup passed from other filters or null from the original filter
	 * @param  object	$args	Holds the menu option arguments
	 * @return mixed			Null/original passed markup, or cached menu markup
	 */
	public function return_cached_menu( $output = null, $args ) {

		if ( !empty( $args->theme_location ) && $cached_menu = get_transient( $this->transient_prefix . $args->theme_location ) ) {
			return $cached_menu;
		}

		return $output;

	}

	/**
	 * Set/save the transient/cache menu on our first/expired request for the menu
	 *
	 * @param string   $nav_menu The HTML content for the navigation menu.
	 * @param stdClass $args     An object containing wp_nav_menu() arguments.
	 */
	public function set_cache_menu( $nav_menu, $args ) {

		if ( !empty( $args ) ) {

			if ( false === get_transient( $this->transient_prefix . $args->theme_location ) ) {
				set_transient( $this->transient_prefix . $args->theme_location, $nav_menu );
			}

		}

		return $nav_menu;

	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( !self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {

		add_action( 'wp_update_nav_menu', array( $this, 'do_wp_update_nav_menu' ) );

		add_filter( 'pre_wp_nav_menu', array( $this, 'return_cached_menu' ), 10 , 2 );
		add_filter( 'wp_nav_menu', array( $this, 'set_cache_menu' ), 10 , 2 );

	}

}
