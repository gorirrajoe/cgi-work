<?php
/**
 * CMB2 Theme Options
 * @version 0.1.0
 * call cgi_bikes_get_option( 'XXX' ) to grab a variable
 */
class Cgi_Bikes_Admin {
	/**
	 * Option key, and option page slug
	 * @var string
	 */
	private $key = 'cgi_bikes_options';
	/**
	 * Options page metabox id
	 * @var string
	 */
	private $metabox_id = 'cgi_bikes_option_metabox';
	/**
	 * Options Page title
	 * @var string
	 */
	private $metabox_id_2 = 'cgi_bikes_option_metabox_2';
	/**
	 * Options Page title
	 * @var string
	 */
	private $metabox_id_3 = 'cgi_bikes_option_metabox_3';
	/**
	 * Options Page title
	 * @var string
	 */
	private $metabox_id_4 = 'cgi_bikes_option_metabox_4';
	/**
	 * Options Page title
	 * @var string
	 */
	private $metabox_id_5 = 'cgi_bikes_option_metabox_5';
	/**
	 * Options Page title
	 * @var string
	 */
	private $metabox_id_6 = 'cgi_bikes_option_metabox_6';
	/**
	 * Options Page title
	 * @var string
	 */
	private $metabox_id_7 = 'cgi_bikes_option_metabox_7';
	/**
	 * Options Page title
	 * @var string
	 */
	protected $title = '';
	/**
	 * Options Page hook
	 * @var string
	 */
	protected $options_page = '';
	/**
	 * Holds an instance of the object
	 *
	 * @var Cgi_Bikes_Admin
	 **/
	private static $instance = null;
	/**
	 * Constructor
	 * @since 0.1.0
	 */
	private function __construct() {
		// Set our title
		$this->title = __( 'Velo Options', 'cgi_bikes' );
	}
	/**
	 * Returns the running object
	 *
	 * @return Cgi_Bikes_Admin
	 **/
	public static function get_instance() {
		if( is_null( self::$instance ) ) {
			self::$instance = new Cgi_Bikes_Admin();
			self::$instance->hooks();
		}
		return self::$instance;
	}
	/**
	 * Initiate our hooks
	 * @since 0.1.0
	 */
	public function hooks() {
		add_action( 'admin_init', array( $this, 'init' ) );
		add_action( 'admin_menu', array( $this, 'add_options_page' ) );
		add_action( 'cmb2_admin_init', array( $this, 'add_header_options_page_metabox' ), 11 );
		add_action( 'cmb2_admin_init', array( $this, 'add_post_options_page_metabox' ) );
		add_action( 'cmb2_admin_init', array( $this, 'add_interstitial_options_page_metabox' ) );
		add_action( 'cmb2_admin_init', array( $this, 'add_site_options_page_metabox' ) );
		add_action( 'cmb2_admin_init', array( $this, 'add_editorial_options_page_metabox' ) );
		add_action( 'cmb2_admin_init', array( $this, 'add_analytics_options_page_metabox' ) );
		add_action( 'cmb2_admin_init', array( $this, 'add_amp_options_page_metabox' ) );

	}
	/**
	 * Register our setting to WP
	 * @since  0.1.0
	 */
	public function init() {
		register_setting( $this->key, $this->key );
	}
	/**
	 * Add menu options page
	 * @since 0.1.0
	 */
	public function add_options_page() {
		$this->options_page = add_menu_page( $this->title, $this->title, 'manage_options', $this->key, array( $this, 'admin_page_display' ) );
		// Include CMB CSS in the head to avoid FOUC
		add_action( "admin_print_styles-{$this->options_page}", array( 'CMB2_hookup', 'enqueue_cmb_css' ) );
		// Include postbox so metaboxes are collapsable
		add_action( 'admin_print_scripts-'.$this->options_page, 'cgi_enqueue_scripts' );
		add_action( 'admin_footer-'.$this->options_page,'cgi_options_print_script_in_footer' );
		function cgi_enqueue_scripts( $suffix ){
			wp_enqueue_script( 'postbox' );
		}
		function cgi_options_print_script_in_footer(){
		?>
			<script>jQuery(document).ready(function(){ postboxes.add_postbox_toggles(pagenow); });</script>
		<?php
		}
	}


	/**
	 * Admin page markup. Mostly handled by CMB2
	 * @since  0.1.0
	 */
	public function admin_page_display() { ?>
		<!-- manually create Tabs for options page -->
		<div class="wrap cmb2-options-page <?php echo $this->key; ?>">
			<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
			<div id="poststuff">
				<div id="cmb2-nav">
					<div class="nav-tab-wrapper">
						<a class="nav-tab" href="#cgi_bikes_option_metabox">Header/Footer Options</a>
						<a class="nav-tab" href="#cgi_bikes_option_metabox_2">Post Options</a>
						<a class="nav-tab" href="#cgi_bikes_option_metabox_6">Interstitial Options</a>
						<a class="nav-tab" href="#cgi_bikes_option_metabox_3">Social Options</a>
						<a class="nav-tab" href="#cgi_bikes_option_metabox_4">Newsletter/iContact Options</a>
						<a class="nav-tab" href="#cgi_bikes_option_metabox_5">Analytics Options</a>
						<a class="nav-tab" href="#cgi_bikes_option_metabox_7">AMP Options</a>
					</div>
				</div>
				<div class="meta-box-sortables ui-sortable">
					<div class="postbox" id="<?php echo ($this->metabox_id); ?>" style="display: block;">
						<button type="button" class="handlediv button-link" aria-expanded="true">
							<span class="toggle-indicator" aria-hidden="true"></span>
						</button>
						<h3 class="hndle">Header/Footer Options</h3>
						<div class="inside">
							<?php cmb2_metabox_form( $this->metabox_id, $this->key ); ?>
						</div>
					</div>
					<div class="postbox" id="<?php echo ($this->metabox_id_2); ?>" style="display: block;">
						<button type="button" class="handlediv button-link" aria-expanded="true">
							<span class="toggle-indicator" aria-hidden="true"></span>
						</button>
						<h3 class="hndle">Post Category and Feature Options</h3>
						<div class="inside">
							<?php cmb2_metabox_form( $this->metabox_id_2, $this->key ); ?>
						</div>
					</div>
					<div class="postbox" id="<?php echo ($this->metabox_id_6); ?>" style="display: block;">
						<button type="button" class="handlediv button-link" aria-expanded="true">
							<span class="toggle-indicator" aria-hidden="true"></span>
						</button>
						<h3 class="hndle">Interstitial Options</h3>
						<div class="inside">
							<?php cmb2_metabox_form( $this->metabox_id_6, $this->key ); ?>
						</div>
					</div>
					<div class="postbox" id="<?php echo ($this->metabox_id_3); ?>" style="display: block;">
						<button type="button" class="handlediv button-link" aria-expanded="true">
							<span class="toggle-indicator" aria-hidden="true"></span>
						</button>
						<h3 class="hndle">Social Options</h3>
						<div class="inside">
							<?php cmb2_metabox_form( $this->metabox_id_3, $this->key ); ?>
						</div>
					</div>
					<div class="postbox" id="<?php echo ($this->metabox_id_4); ?>" style="display: block;">
						<button type="button" class="handlediv button-link" aria-expanded="true">
							<span class="toggle-indicator" aria-hidden="true"></span>
						</button>
						<h3 class="hndle">Newsletter/iContact Options</h3>
						<div class="inside">
							<?php cmb2_metabox_form( $this->metabox_id_4, $this->key ); ?>
						</div>
					</div>
					<div class="postbox" id="<?php echo ($this->metabox_id_5); ?>" style="display: block;">
						<button type="button" class="handlediv button-link" aria-expanded="true">
							<span class="toggle-indicator" aria-hidden="true"></span>
						</button>
						<h3 class="hndle">Analytics Options</h3>
						<div class="inside">
							<?php cmb2_metabox_form( $this->metabox_id_5, $this->key ); ?>
						</div>
					</div>
					<div class="postbox" id="<?php echo ($this->metabox_id_7); ?>" style="display: block;">
						<button type="button" class="handlediv button-link" aria-expanded="true">
							<span class="toggle-indicator" aria-hidden="true"></span>
						</button>
						<h3 class="hndle">AMP Options</h3>
						<div class="inside">
							<?php cmb2_metabox_form( $this->metabox_id_7, $this->key ); ?>
						</div>
					</div>
				</div><!-- END .meta-box-sortables.ui-sortable -->
			</div>
		</div>
	<?php }


	function add_header_options_page_metabox() {
		// hook in our save notices
		add_action( "cmb2_save_options-page_fields_{$this->metabox_id}", array( $this, 'settings_notices' ), 10, 2 );
		$cmb = new_cmb2_box( array(
			'id'         => $this->metabox_id,
			'hookup'     => false,
			'cmb_styles' => false,
			'title'		 => 'Header/Footer Options',
			'show_on'    => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( $this->key, )
			),
		) );
		$cmb->add_field( array(
			'name' => __( 'Logo', 'cgi_bikes' ),
			'desc' => __( 'Upload the Logo. Must be 135x25 pixels. SVG is preferred. Leave empty for the default logo.', 'cgi_bikes' ),
			'id'  => 'site_logo',
			'type'     => 'file',
		) );
		$cmb->add_field( array(
			'name' => __( 'Display Alert Ribbon', 'cgi_bikes' ),
			'desc' => __( 'Check to Display Single Line Stage Headline', 'cgi_bikes' ),
			'id'   => 'ticker_on',
			'type' => 'checkbox',
			'default' => '',
		) );
		$cmb->add_field( array(
			'name' => __( 'Alert Ribbon Text', 'cgi_bikes' ),
			'desc' => __( 'Single line of text for Ticker Ribbon', 'cgi_bikes' ),
			'id'   => 'ticker_text',
			'type' => 'text',
			'default' => '',
		) );
		$cmb->add_field( array(
			'name'    => __( 'Alert Ribbon Read More Link', 'cgi_bikes' ),
			'desc'    => __( 'Link to article', 'cgi_bikes' ),
			'id'      => 'ticker_link',
			'type'    => 'text_url',
			'default' => '',
		) );
		$cmb->add_field( array(
			'name' => __( 'Race Stage Leaders', 'cgi_bikes' ),
			'desc' => __( 'Check to Display Race Stage Top 10', 'cgi_bikes' ),
			'id'   => 'leaders_on',
			'type' => 'checkbox',
			'default' => '',
		) );
		$cmb->add_field( array(
			'name' => __( 'Current Race', 'cgi_bikes' ),
			'desc' => __( 'Choose the current race', 'cgi_bikes' ),
			'id'   => 'leaders_race',
			'type' => 'select',
			'options' => get_stage_terms(), // Tells CMB2 which taxonomies should have these fields
		) );
		$cmb->add_field( array(
			'name'    => __( 'Subscribe Link', 'cgi_bikes' ),
			'desc'    => __( 'Footer Subscribe Link', 'cgi_bikes' ),
			'id'      => 'subscribe_link',
			'type'    => 'text_url',
		) );
		$cmb->add_field( array(
			'id'				=> 'eu_regulation_txt',
			'name'				=> __( 'EU Regulations Text', 'triathelete' ),
			'desc'				=> __( 'Text to display about privacy policy.', 'cgi_bikes' ),
			'type'				=> 'textarea',
			'sanitization_cb'	=> 'stripslashes_deep',
			'attributes'		=> array(
				'rows'		=> 20,
			)
		) );
	}


	function add_post_options_page_metabox() {
		// hook in our save notices
		add_action( "cmb2_save_options-page_fields_{$this->metabox_id_2}", array( $this, 'settings_notices' ), 10, 2 );
		$cmb = new_cmb2_box( array(
			'id'         => $this->metabox_id_2,
			'hookup'     => false,
			'cmb_styles' => false,
			'title'		 => 'Post Options',
			'show_on'    => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( $this->key, )
			),
		) );
		$cmb->add_field( array(
			'name' => __( 'HOMEPAGE - Featured 5 Articles', 'cgi_bikes' ),
			'desc' => __( 'Enter a list of 5 post ID\'s, separated by commas', 'cgi_bikes' ),
			'id'   => 'home_featured_5',
			'type' => 'text',
		) );
		$cmb->add_field( array(
			'name' => __( 'GLOBAL Top Right Post', 'cgi_bikes' ),
			'desc' => __( 'ID for post above 300X600 ad', 'cgi_bikes' ),
			'id'   => 'ad_post_id',
			'type' => 'text',
			'default' => '',
		) );
		$cmb->add_field( array(
			'name' => __( 'GLOBAL 4 Across Top Title', 'cgi_bikes' ),
			'desc' => __( 'Title for 4 recent posts', 'cgi_bikes' ),
			'id'   => 'home_4_title',
			'type' => 'text',
			'default' => '',
		) );
		$cmb->add_field( array(
			'name' => __( 'GLOBAL 4 Across Top Category', 'cgi_bikes' ),
			'desc' => __( 'Enter the category ID for 4 across', 'cgi_bikes' ),
			'id' => 'posts_home_4_category',
			'type' => 'text',
		) );
		$cmb->add_field( array(
			'name' => __( '2 Featured Articles', 'cgi_bikes' ),
			'desc' => __( '2 Post IDs separated by a comma', 'cgi_bikes' ),
			'id'   => 'featured_articles',
			'type' => 'text',
			'default' => '',
		) );
		$cmb->add_field( array(
			'name' => __( 'GLOBAL 5 Across Title', 'cgi_bikes' ),
			'desc' => __( 'Title for 5 recent posts', 'cgi_bikes' ),
			'id'   => 'site_5_title',
			'type' => 'text',
			'default' => '',
		) );
		$cmb->add_field( array(
			'name' => __( 'GLOBAL 5 Across Category', 'cgi_bikes' ),
			'desc' => __( 'Enter the category id for 5 across', 'cgi_bikes' ),
			'id'   => 'site_5_category',
			'type'     => 'text',
		) );
		$cmb->add_field( array(
			'name' => __( 'GLOBAL 2x2 Title', 'cgi_bikes' ),
			'desc' => __( 'Title for 2x2 recent posts', 'cgi_bikes' ),
			'id' => 'home_2by2_title',
			'type' => 'text',
			'default' => '',
		) );
		$cmb->add_field( array(
			'name' => __( 'GLOBAL 2x2 Category', 'cgi_bikes' ),
			'desc' => __( 'Enter the category ID for 2x2', 'cgi_bikes' ),
			'id' => 'posts_home_2by2_category',
			'type' => 'text',
		) );
		$cmb->add_field( array(
			'name' => __( 'GLOBAL 4 Across Bottom Title', 'cgi_bikes' ),
			'desc' => __( 'Title for bottom 4 recent posts', 'cgi_bikes' ),
			'id'   => 'site_4bottom_title',
			'type' => 'text',
			'default' => '',
		) );
		$cmb->add_field( array(
			'name' => __( 'GLOBAL 4 Across Bottom Category', 'cgi_bikes' ),
			'desc' => __( 'Enter the category id for bottom 4 across', 'cgi_bikes' ),
			'id'   => 'site_4bottom_category',
			'type'     => 'text',
		) );
		$cmb->add_field( array(
			'name' => 'Buy Now Sponsor Logos',
			'type' => 'title',
			'id'   => 'buynow_title'
		) );

		$brands_array = array(
			'amazon'		=> 'Amazon',
			'artscyclery'	=> 'ArtsCyclery',
			'cocyclist'		=> 'Colorado Cyclist',
			'chainreaction'	=> 'Chain Reaction',
			'wiggle'		=> 'Wiggle'
		);
		foreach( $brands_array as $key => $value ) {
			// file
			$cmb->add_field( array(
				'name'		=> 'Buy Now: '. $value,
				'desc'		=> 'Upload an image or enter an URL.',
				'id'		=> 'buynow_'. $key,
				'type'		=> 'file',
				// Optional:
				'options'	=> array(
					'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
				),
			) );

		}

	}


	function add_interstitial_options_page_metabox() {
		// hook in our save notices
		add_action( "cmb2_save_options-page_fields_{$this->metabox_id_6}", array( $this, 'settings_notices' ), 10, 2 );
		$cmb = new_cmb2_box( array(
			'id'         => $this->metabox_id_6,
			'hookup'     => false,
			'cmb_styles' => false,
			'title'		 => 'Interstitial Options',
			'show_on'    => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( $this->key, )
			),
		) );
		$cmb->add_field( array(
			'name'    => 'Interstitial Ad',
			'desc'    => 'If checked, an interstitial ad will be displayed when the first link is clicked. Uncheck to disable.',
			'id'      => 'interstitial_ad_on',
			'type'    => 'checkbox',
		) );
		$cmb->add_field( array(
			'name'    => 'Interstitial Ad Category',
			'desc'    => 'Enter a category for the interstitial ad.',
			'id'      => 'interstitial_ad_cat',
			'type'    => 'text',
		) );
		$cmb->add_field( array(
			'name'    => 'Interstitial Ad Exclude Category',
			'desc'    => 'Enter a category for the interstitial ad to be excluded, will only work if Interstitial Ad field is empty.',
			'id'      => 'interstitial_ad_excl_cat',
			'type'    => 'text',
		) );
		$cmb->add_field( array(
			'name'			=> 'Interstitial Ad Cookie Expiration time',
			'desc'			=> 'Enter an amount in <strong>MINUTES</strong> that this cookie should expire in.',
			'id'			=> 'interstitial_exp_time',
			'type'			=> 'text_small',
			'default'		=> '30', // 30 minutes
			'attributes'	=> array(
				'type'	=> 'number',
				'min'	=> '0',
				'step'	=> '.1'
			),
		) );
		$cmb->add_field( array(
			'name'    => 'Interstitial Mobile Ad',
			'desc'    => 'If checked, an interstitial ad will be displayed when the first link is clicked. Uncheck to disable.',
			'id'      => 'interstitial_mobile_on',
			'type'    => 'checkbox',
		) );
		$cmb->add_field( array(
			'name'    => 'Interstitial Mobile Ad Category',
			'desc'    => 'Enter a category for the interstitial ad.',
			'id'      => 'interstitial_mobile_cat',
			'type'    => 'text',
		) );
		$cmb->add_field( array(
			'name'    => 'Interstitial Mobile Exclude Category',
			'desc'    => 'Enter a category for the interstitial mobile ad to be excluded, will only work if Interstitial Mobile Ad field is empty.',
			'id'      => 'interstitial_mobile_excl_cat',
			'type'    => 'text',
		) );
		$cmb->add_field( array(
			'name'			=> 'Interstitial Mobile Cookie Expiration time',
			'desc'			=> 'Enter an amount in <strong>MINUTES</strong> that this cookie should expire in.',
			'id'			=> 'interstitial_mobile_exp_time',
			'type'			=> 'text_small',
			'default'		=> '30', // 30 minutes
			'attributes'	=> array(
				'type'	=> 'number',
				'min'	=> '0',
				'step'	=> '.1'
			),
		) );
		$cmb->add_field( array(
			'name'    => 'Facebook Interstitial Ad',
			'desc'    => 'If checked, a Facebook Like Box interstitial ad will be displayed after a minute on the site, rotating with the magazine subscription and newsletter subscription. Uncheck to disable.',
			'id'      => 'facebook_interstitial_on',
			'type'    => 'checkbox',
		) );
		$cmb->add_field( array(
			'name'    => 'Facebook/Magazine Interstitial Ad Exclude Category',
			'desc'    => 'Enter categories where the Facebook Like Box interstitial ad and/or Magazine subscription interstitial will not display.',
			'id'      => 'facebook_interstitial_exclude',
			'type'    => 'text',
		) );
		$cmb->add_field( array(
			'name'			=> 'Facebook Interstitial Cookie Expiration time',
			'desc'			=> 'Enter an amount in <strong>MINUTES</strong> that this cookie should expire in.',
			'id'			=> 'facebook_interstitial_exp_time',
			'type'			=> 'text_small',
			'default'		=> '10080', // one week in minutes
			'attributes'	=> array(
				'type'	=> 'number',
				'min'	=> '0',
				'step'	=> '.1'
			),
		) );
		/*$cmb->add_field( array(
			'name'    => 'Magazine Interstitial Ad',
			'desc'    => 'If checked, a Magazine subscription interstitial ad will be displayed after a minute on the site, rotating with the Facebook Interstitial and Newsletter interstitial. Uncheck to disable.',
			'id'      => 'magazine_interstitial_on',
			'type'    => 'checkbox',
		) );
		$cmb->add_field( array(
			'name'    => 'Magazine Interstitial Image',
			'desc'    => 'Max image size is 700 x 200. Either enter a full URL to an image or upload/choose an image to/from the media library. Use the insert into post button in the Media Library to place the image link here.',
			'id'      => 'magazine_interstitial_image',
			'type'    => 'file',
		) );
		$cmb->add_field( array(
			'name'    => 'Magazine Interstitial Link',
			'desc'    => 'Enter a link for the magazine interstitial.',
			'id'      => 'magazine_interstitial_link',
			'type'    => 'text_url',
		) );
		$cmb->add_field( array(
			'name'			=> 'Magazine Interstitial Cookie Expiration time',
			'desc'			=> 'Enter an amount in <strong>MINUTES</strong> that this cookie should expire in.',
			'id'			=> 'magazine_interstitial_exp_time',
			'type'			=> 'text_small',
			'default'		=> '10080', // one week in minutes
			'attributes'	=> array(
				'type'	=> 'number',
				'min'	=> '0',
				'step'	=> '.1'
			),
		) );*/
		/*$cmb->add_field( array(
			'name'    => 'Newsletter Interstitial Ad',
			'desc'    => 'If checked, a newsletter subscription interstitial ad will be displayed after a minute on the site, rotating with the Facebook Interstitial and Magazine interstitial. Uncheck to disable.',
			'id'      => 'newsletter_interstitial_on',
			'type'    => 'checkbox',
		) );
		$cmb->add_field( array(
			'name'			=> 'Newsletter Interstitial Cookie Expiration time',
			'desc'			=> 'Enter an amount in <strong>MINUTES</strong> that this cookie should expire in.',
			'id'			=> 'newsletter_interstitial_exp_time',
			'type'			=> 'text_small',
			'default'		=> '0', // 0 means session
			'attributes'	=> array(
				'type'	=> 'number',
				'min'	=> '0',
				'step'	=> '.1'
			),
		) );*/
		$cmb->add_field( array(
			'name'    => 'Pre-Roll',
			'desc'    => 'If checked, pre-roll ads will be displayed on videos. Uncheck to disable.)',
			'id'      => 'preroll_on',
			'type'    => 'checkbox',
		) );
	}


	function add_site_options_page_metabox() {
		// hook in our save notices
		add_action( "cmb2_save_options-page_fields_{$this->metabox_id_3}", array( $this, 'settings_notices' ), 10, 2 );
		$cmb = new_cmb2_box( array(
			'id'         => $this->metabox_id_3,
			'hookup'     => false,
			'cmb_styles' => false,
			'title'		 => 'Social Options',
			'show_on'    => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( $this->key, )
			),
		) );
		$cmb->add_field( array(
			'name' => __( 'Facebook', 'cgi_bikes' ),
			'desc' => __( 'Site Facebook Page', 'cgi_bikes' ),
			'id'   => 'facebook_link',
			'type'     => 'text',
		) );
		$cmb->add_field( array(
			'name' => __( 'Twitter', 'cgi_bikes' ),
			'desc' => __( 'Site Twitter Handle', 'cgi_bikes' ),
			'id'   => 'twitter_link',
			'type'     => 'text',
		) );
		$cmb->add_field( array(
			'name' => __( 'Instagram', 'cgi_bikes' ),
			'desc' => __( 'Enter the full URL for the Instagram account for this site', 'cgi_bikes' ),
			'id'   => 'instagram_link',
			'type'     => 'text_url',
		) );
		$cmb->add_field( array(
			'name' => __( 'YouTube', 'cgi_bikes' ),
			'desc' => __( 'YouTube Profile Link', 'cgi_bikes' ),
			'id'   => 'youtube_link',
			'type'     => 'text_url',
		) );
		$cmb->add_field( array(
			'name' => __( 'LiveBlog - Tumblr URL', 'cgi_bikes' ),
			'desc' => __( 'Enter Tumblr SUBDOMAIN URL of site for liveblog. Ex: cgittest', 'cgi_bikes' ),
			'id'   => 'tumblr_liveblog_subdomain',
			'type'     => 'text',
		) );
		$cmb->add_field( array(
			'name' => __( 'LiveBlog - Tumblr Consumer Key', 'cgi_bikes' ),
			'desc' => __( 'Enter Tumblr Consumer API key for liveblog.', 'cgi_bikes' ),
			'id'   => 'tumblr_liveblog_API_key',
			'type'     => 'text',
		) );
		$cmb->add_field( array(
			'name' => __( 'Outbrain', 'cgi_bikes' ),
			'desc' => __( 'Check to Display Outbrain ads', 'cgi_bikes' ),
			'id'   => 'outbrain_ads',
			'type'     => 'checkbox',
		) );
	}


	function add_editorial_options_page_metabox() {
		// hook in our save notices
		add_action( "cmb2_save_options-page_fields_{$this->metabox_id_4}", array( $this, 'settings_notices' ), 10, 2 );
		$cmb = new_cmb2_box( array(
			'id'         => $this->metabox_id_4,
			'hookup'     => false,
			'cmb_styles' => false,
			'title'		 => 'Site Options',
			'show_on'    => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( $this->key, )
			),
		) );
		// Set our CMB2 fields
		$cmb->add_field( array(
			'name' => __( 'Newsletter Sponsor Image', 'cgi_bikes' ),
			'desc' => __( 'Must be 175x40 in size', 'cgi_bikes' ),
			'id'   => 'newsletter_sponsor_image',
			'type'     => 'file',
		) );
		$cmb->add_field( array(
			'name' => __( 'Newsletter Sponsor Link', 'cgi_bikes' ),
			'desc' => __( 'Enter full URL to use for the Newsletter Sponsor Link.', 'cgi_bikes' ),
			'id'   => 'newsletter_sponsor_link',
			'type'     => 'text_url',
		) );
		$cmb->add_field( array(
			'name' => __( 'Newsletter Posts', 'cgi_bikes' ),
			'desc' => __( 'Enter the 6 IDs (comma delimited) of the posts you wish to be shown on the newsletter', 'cgi_bikes' ),
			'id'   => 'newsletter_posts_IDs',
			'type'     => 'text',
		) );

		$cmb->add_field( array(
			'name'    => 'Gallery Last Slide',
			'desc'    => 'Upload Last Slide Image',
			'id'      => 'editorial_gallery_last_slide_image',
			'type'    => 'file',
		) );
		$cmb->add_field( array(
			'name'    => 'Gallery Last Slide URL',
			'desc'    => 'Enter full URL to use for the last slide image of the gallery.',
			'id'      => 'editorial_gallery_last_slide_url',
			'type'    => 'text_url',
		) );

		$cmb->add_field( array(
			'name' => __( 'Partner Showcase Newsletter Posts', 'cgi_bikes' ),
			'desc' => __( 'Enter the 6 IDs (comma delimited) of the posts you wish to be shown on the Competitor\'s Best Newsletter', 'cgi_bikes' ),
			'id'   => 'partner_showcase_posts',
			'type'     => 'text',
		) );
		$cmb->add_field( array(
			'name' => __( 'Partner Showcase Newsletter Logo Image', 'cgi_bikes' ),
			'desc'  => __('Must be 210x75 in size'),
			'id'   => 'partner_showcase_logo',
			'type'     => 'file',
		) );
		$cmb->add_field( array(
			'name' => __( 'Partner Showcase Newsletter Logo Link', 'cgi_bikes' ),
			'desc'  => __('Link for newsletter partner logo'),
			'id'   => 'partner_logo_link',
			'type'     => 'text_url',
		) );
		$cmb->add_field( array(
			'name' => __( 'Sidebar Newsletter Signup - Title', 'cgi_bikes' ),
			'id'   => 'nl_signup_title',
			'type'     => 'text',
		) );
		$cmb->add_field( array(
			'name' => __( 'Sidebar Newsletter Signup - Blurb', 'cgi_bikes' ),
			'id'   => 'nl_signup_blurb',
			'type'     => 'text',
		) );
		$cmb->add_field( array(
				'name'    => 'Sidebar Newsletter Signup - Image',
				'id'      => 'nl_signup_img',
				'type'    => 'file',
		) );
		$cmb->add_field( array(
			'name' => __( 'Footer Newsletter Signup - Blurb', 'cgi_bikes' ),
			'id'   => 'nl_footer_signup_blurb',
			'type'     => 'text',
		) );
	}


	function add_analytics_options_page_metabox() {
		// hook in our save notices
		add_action( "cmb2_save_options-page_fields_{$this->metabox_id_5}", array( $this, 'settings_notices' ), 10, 2 );
		$cmb = new_cmb2_box( array(
			'id'         => $this->metabox_id_5,
			'hookup'     => false,
			'cmb_styles' => false,
			'title'		 => 'Analytics Options',
			'show_on'    => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( $this->key, )
			),
		) );
		// Set our CMB2 fields
		$cmb->add_field( array(
			'name' => __( 'Facebook App ID', 'cgi_bikes' ),
			'desc' => __( 'Site Facebook Page', 'cgi_bikes' ),
			'id'   => 'facebook_app_ID',
			'type'     => 'text',
		) );
		$cmb->add_field( array(
			'name' => __( 'Google Analytics ID', 'cgi_bikes' ),
			'desc' => __( 'GA ID', 'cgi_bikes' ),
			'id'   => 'google_analytics_ID',
			'type'     => 'text',
		) );
		$cmb->add_field( array(
			'name' => __( 'After &lt;head&gt; Tag', 'cgi_bikes' ),
			'desc' => __( 'Code to be placed immediately after opening <head> tag', 'cgi_bikes' ),
			'id' => 'in_head_section',
			'type' => 'textarea_code',
		) );
		$cmb->add_field( array(
			'name' => __( 'Before &lt;/head&gt; Tag', 'cgi_bikes' ),
			'desc' => __( 'Code to be placed immediately before closing </head> tag', 'cgi_bikes' ),
			'id' => 'before_closing_head',
			'type' => 'textarea_code',
		) );
		$cmb->add_field( array(
			'name' => __( 'After &lt;body&gt; Tag', 'cgi_bikes' ),
			'desc' => __( 'Code to be placed directly after opening <body> tag', 'cgi_bikes' ),
			'id' => 'after_opening_body',
			'type' => 'textarea_code',
		) );
		$cmb->add_field( array(
			'name' => __( 'Before &lt;/body&gt; Tag', 'cgi_bikes' ),
			'desc' => __( 'Code to be placed directly before closing </body> tag', 'cgi_bikes' ),
			'id' => 'before_closing_body',
			'type' => 'textarea_code',
		) );
		$cmb->add_field( array(
			'name' => __( 'Social Media Publisher Key', 'cgi_bikes' ),
			'desc' => __( 'Enter publisher key for social media sharing icons.', 'cgi_bikes' ),
			'id' => 'social_media_publisher',
			'type' => 'text',
		) );
		$cmb->add_field( array(
			'name' => __( 'Bing Search App ID', 'cgi_bikes' ),
			'desc' => __( 'Bing Search App ID', 'cgi_bikes' ),
			'id' => 'bing_search_id',
			'type' => 'text',
		) );
		$cmb->add_field( array(
			'name' => __( 'Bing Web Search Subscription Key', 'cgi_bikes' ),
			'desc' => __( 'bing api v5', 'cgi_bikes' ),
			'id' => 'bing_web_search_id_v5',
			'type' => 'text',
		) );
		$cmb->add_field( array(
			'name' => __( 'Bing Image Search Subscription Key', 'cgi_bikes' ),
			'desc' => __( 'bing api v5', 'cgi_bikes' ),
			'id' => 'bing_img_search_id_v5',
			'type' => 'text',
		) );
		$cmb->add_field( array(
			'id'		=> 'search_type',
			'name'		=> __( 'Search Type', 'cgi_bikes' ),
			'desc'		=> __( 'Determines which type of search to use.', 'cgi_bikes' ),
			'type'		=> 'radio_inline',
			'options'	=> array(
				'wordpress'	=> __( 'WordPress Default Search', 'cgi_bikes' ),
				'bing'		=> __( 'Bing Search', 'cgi_bikes' ),
				'algolia'	=> __( 'Algolia Search', 'cgi_bikes' ),
			),
			'default'	=> 'wordpress',
		) );
	}

	function add_amp_options_page_metabox() {
		// hook in our save notices
		add_action( "cmb2_save_options-page_fields_{$this->metabox_id_7}", array( $this, 'settings_notices' ), 10, 2 );
		$cmb = new_cmb2_box( array(
			'id'         => $this->metabox_id_7,
			'hookup'     => false,
			'cmb_styles' => false,
			'title'		 => 'AMP Options',
			'show_on'    => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( $this->key, )
			),
		) );
		$cmb->add_field( array(
			'name' => __( 'AMP - Exclude MultiPage Posts', 'cgi_bikes' ),
			'desc' => __( 'Exclude multipage posts from AMP', 'cgi_bikes' ),
			'id'   => 'amp_exclude_multipage_posts',
			'type' => 'checkbox',
		) );
		$cmb->add_field( array(
			'name' => __( 'AMP - Exclude Categories', 'cgi_bikes' ),
			'desc' => __( 'Enter a comma separated list of category ID\'s that will not be AMP-ified', 'cgi_bikes' ),
			'id'   => 'amp_exclude_cats',
			'type' => 'text',
		) );
	}


	/**
	 * Register settings notices for display
	 *
	 * @since  0.1.0
	 * @param  int   $object_id Option key
	 * @param  array $updated   Array of updated fields
	 * @return void
	 */
	public function settings_notices( $object_id, $updated ) {
		if ( $object_id !== $this->key || empty( $updated ) ) {
			return;
		}
		add_settings_error( $this->key . '-notices', '', __( 'Settings updated.', 'cgi_bikes' ), 'updated' );
		settings_errors( $this->key . '-notices' );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if (!self::$instance)
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Public getter method for retrieving protected/private variables
	 * @since  0.1.0
	 * @param  string  $field Field to retrieve
	 * @return mixed          Field value or exception is thrown
	 */
	public function __get( $field ) {
		// Allowed fields to retrieve
		if ( in_array( $field, array( 'key', 'metabox_id', 'title', 'options_page' ), true ) ) {
			return $this->{$field};
		}
		throw new Exception( 'Invalid property: ' . $field );
	}


}





/**
 * Helper function to get/return the Cgi_Bikes_Admin object
 * @since  0.1.0
 * @return Cgi_Bikes_Admin object
 */
function cgi_bikes_admin() {
	return Cgi_Bikes_Admin::get_instance();
}


/**
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 * @param  string  $key Options array key
 * @return mixed        Option value
 */
function cgi_bikes_get_option( $key = '' ) {

	$option = array();

	if( function_exists( 'cmb2_get_option' ) && function_exists( 'cgi_bikes_admin' ) ) {
		$admin = cgi_bikes_admin();
		$admin_key = ( property_exists( $admin, 'key' ) ? $admin->key : '' );

		$option = cmb2_get_option( $admin_key , $key );
	}

	return $option;
}


// Get it started
cgi_bikes_admin();
