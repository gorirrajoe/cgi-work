<?php

class VeloNews_MegaMenu_Walker extends Walker_Nav_Menu {

	private $depth = 0;

	private $menu_id = 0;

	private $is_mega_menu = false;

	private $is_nested = false;

	private $mega_menus = array();

	public function __construct() {

		/**
		 * Queries the database for the Megamenu transient as soon as this class
		 * is initiated and saves the return value to "$this->mega_menus".
		 */
		if( class_exists( 'VeloNews_MegaMenu' ) && false === get_transient( 'velonews-menu-primary' ) ) {
			$this->mega_menus = VeloNews_MegaMenu::singleton()->get_megamenu_markup();
		}
	}

	/**
	 * Starts the list before the elements are added.
	 *
	 * @see Walker::start_lvl()
	 *
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ) {

		if( 0 === $depth ) {

			// if current item is mega menu, add additional markup
			if( $this->is_mega_menu ) {

				$output .= '<div class="mega-menu__container">' ."\n";
					$output .= '<div class="mega-menu__row">' ."\n";
						$output .= '<ul class="main-nav__sub-menu" role="menu" aria-hidden="true" data-depth='. $depth .'>' ."\n";

			} else {

				$output .= '<ul class="main-nav__sub-menu" role="menu" aria-hidden="true">' ."\n";

			}

		}

	}

	/**
	 * Ends the list of after the elements are added.
	 *
	 * @see Walker::end_lvl()
	 *
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 */
	public function end_lvl( &$output, $depth = 0, $args = Array() ) {

		if( $this->is_nested && 0 !== $depth ) {
			return;
		}

		// if current item is mega menu, add additional markup
		if( $this->is_mega_menu ) {

					$output .= '</ul> <!-- .main-nav__sub-menu -->' ."\n";

					if( isset( $this->mega_menus[$this->menu_id] ) ) {

						foreach( $this->mega_menus[$this->menu_id] as $menu ) {
							$output .= $menu;
						}

					}

				$output .= '</div> <!-- .mega-menu__row -->'."\n";

			$output .= '</div> <!-- .mega-menu__container -->'."\n";

		} else {

			$output .= '</ul> <!-- .main-nav__sub-menu -->'."\n";

		}
	}

	/**
	 * Start the element output.
	 *
	 * @see Walker::start_el()
	 *
	 * @since 3.0.0
	 * @since 4.4.0 'nav_menu_item_args' filter was added.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item   Menu item data object.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 * @param int    $id     Current item ID.
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;

		$indent = ( isset( $depth ) ? str_repeat( "\t", $depth ) : '' );

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		/**
		 * Add depth-dependant classes
		 */
		if( $depth == 0 ) {

			$this->depth = 0;

			$classes[] = ( isset( $args->walker->has_children ) && $args->walker->has_children == 1 ? 'main-nav__menu-item' : 'main-nav__menu-item' );

			if( isset( $item->ID ) && in_array( 'mega-menu', $classes ) ) {

				$this->is_mega_menu = true;
				$this->menu_id = $item->ID;

			} else {

				$this->is_mega_menu = false;

			}

		} elseif( $depth == 1 ) {

			if( 0 == $this->depth ) {

				$classes = array_merge( $classes, array( 'main-nav__sub-menu-item', 'active' ) );
				$this->depth = 1;

			} else {

				$classes[] = 'main-nav__sub-menu-item';

			}

			$this->is_mega_menu = ( isset( $item->menu_item_parent ) && $item->menu_item_parent == $this->menu_id );

		} else {

			$this->is_nested = true;
			return;

		}

		/**
		 * Filter the arguments for a single nav menu item.
		 *
		 * @since 4.4.0
		 *
		 * @param array  $args  An array of arguments.
		 * @param object $item  Menu item data object.
		 * @param int    $depth Depth of menu item. Used for padding.
		 */
		$args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );

		/**
		 * Filter the CSS class(es) applied to a menu item's list item element.
		 *
		 * @since 3.0.0
		 * @since 4.1.0 The `$depth` parameter was added.
		 *
		 * @param array  $classes The CSS classes that are applied to the menu item's `<li>` element.
		 * @param object $item    The current menu item.
		 * @param array  $args    An array of {@see wp_nav_menu()} arguments.
		 * @param int    $depth   Depth of menu item. Used for padding.
		 */
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
		$class_names = ( isset( $class_names ) ? ' class="' . esc_attr( $class_names ) . '"' : '' );

		/**
		 * Filter the ID applied to a menu item's list item element.
		 *
		 * @since 3.0.1
		 * @since 4.1.0 The `$depth` parameter was added.
		 *
		 * @param string $menu_id The ID that is applied to the menu item's `<li>` element.
		 * @param object $item    The current menu item.
		 * @param array  $args    An array of {@see wp_nav_menu()} arguments.
		 * @param int    $depth   Depth of menu item. Used for padding.
		 */
		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
		$id = ( isset( $id ) ? ' id="' . esc_attr( $id ) . '"' : '' );

		$output .= '<li' . $id . $class_names .' role="menuitem" aria-haspopup="true">';

		$output .= "\n";

		$atts = array();
		$atts['title']	= ( ! empty( $item->attr_title ) ?  esc_attr( $item->attr_title ) : '' );
		$atts['target']	= ( ! empty( $item->target )     ?  esc_attr( $item->target )     : '' );
		$atts['rel']	= ( ! empty( $item->xfn )        ?  esc_attr( $item->xfn )        : '' );
		$atts['href']	= ( ! empty( $item->url )        ?  esc_url( $item->url )         : '' );

		/**
		 * If the current item is a 'Race Category' (aka stagecat) then update
		 * the URL so that the menu item points to the most recent race homepage.
		 */
		if( isset( $item->type ) && isset( $item->object ) ) {

			if( 'taxonomy' == $item->type ) {

				switch( $item->object ) {
					case 'apparel-reviews-cat':
						$atts['href'] = $this->_get_review_homepage_url( $item, $item->url );
						break;
					case 'bike-reviews-cat':
						$atts['href'] = $this->_get_review_homepage_url( $item, $item->url );
						break;
					case 'stagecat':
						$atts['href'] = $this->_get_race_homepage_url( $item, $item->url );
						break;
				}

			}

		}

		//  Add data-target attribute to mega-menu nav items.
		if( $depth > 0 ) {

			if( isset($item->menu_item_parent) && $item->menu_item_parent == $this->menu_id ) {
				$atts['data-target'] = '#mn-' . $item->ID;
			}

		}

		/**
		 * Filter the HTML attributes applied to a menu item's anchor element.
		 *
		 * @since 3.6.0
		 * @since 4.1.0 The `$depth` parameter was added.
		 *
		 * @param array $atts {
		 *     The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
		 *
		 *     @type string $title  Title attribute.
		 *     @type string $target Target attribute.
		 *     @type string $rel    The rel attribute.
		 *     @type string $href   The href attribute.
		 * }
		 * @param object $item  The current menu item.
		 * @param array  $args  An array of {@see wp_nav_menu()} arguments.
		 * @param int    $depth Depth of menu item. Used for padding.
		 */
		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

		$attributes = '';

		foreach( $atts as $attr => $value ) {

			if( ! empty( $value ) ) {
				$attributes .= sprintf(' %s="%s"', $attr, $value );
			}

		}

		/** This filter is documented in wp-includes/post-template.php */
		$title = apply_filters( 'the_title', $item->title, $item->ID );

		/**
		 * Filter a menu item's title.
		 *
		 * @since 4.4.0
		 *
		 * @param string $title The menu item's title.
		 * @param object $item  The current menu item.
		 * @param array  $args  An array of {@see wp_nav_menu()} arguments.
		 * @param int    $depth Depth of menu item. Used for padding.
		 */
		$title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

		$item_output =  ( isset( $args->before ) ? $args->before : '');
			$item_output .= '<a'. $attributes .'>';
				$item_output .= ( isset( $args->link_before ) ? $args->link_before : '');
					$item_output .= $title;
				$item_output .=  ( isset( $args->link_after ) ? $args->link_after : '');
			$item_output .= '</a>';
		$item_output .= ( isset( $args->after ) ? $args->after : '');

		$item_output .= "\n";

		/**
		 * Filter a menu item's starting output.
		 *
		 * The menu item's starting output only includes `$args->before`, the opening `<a>`,
		 * the menu item's title, the closing `</a>`, and `$args->after`. Currently, there is
		 * no filter for modifying the opening and closing `<li>` for a menu item.
		 *
		 * @since 3.0.0
		 *
		 * @param string $item_output The menu item's starting HTML output.
		 * @param object $item        Menu item data object.
		 * @param int    $depth       Depth of menu item. Used for padding.
		 * @param array  $args        An array of {@see wp_nav_menu()} arguments.
		 */
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	/**
	 * Ends the element output, if needed.
	 *
	 * @see Walker::end_el()
	 *
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item   Page data object. Not used.
	 * @param int    $depth  Depth of page. Not Used.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 */
	public function end_el( &$output, $item, $depth = 0, $args = array() ) {

		if( 1 >= $depth ) {

			$output .= '</li> <!-- .menu-item -->' ."\n";

		}

	}

	/**
	 * Get Review Homepage URL
	 *
	 * If the given item is a "Apparel Review" or "Bike Review" taxonomy, then
	 * query the database for the related apparel/bike review page.
	 *
	 * @param object $item    The current menu item.
	 * @param string $default The default URL for the menu item.
	 *
	 * @return string Permalink to use for menu item.
	 */
	private function _get_review_homepage_url( $item = null, $permalink = null ) {

		$args =
			array(
				'post_type'					=> 'page',
				'pagename'					=> '',
				'posts_per_page'			=> 1,
				'no_found_rows'				=> true,
				'update_post_meta_cache'	=> false,
			);

		if( isset( $item->object ) ) {

			switch( $item->object ) {
				case 'apparel-reviews-cat':
					$args['pagename'] = 'apparel-search';
					break;
				case 'bike-reviews-cat':
					$args['pagename'] = 'bike-reviews-rankings';
					break;
			}

		}

		$query = new WP_Query( $args );

		/**
		 * If there is a matching page, update $permalink otherwise
		 * use the default permalink.
		 */
		if ( $query->have_posts() ) {

			$query->the_post();

			$permalink = get_the_permalink();

		}

		/* Restore original Post Data */
		wp_reset_postdata();

		return $permalink;
	}

	/**
	 * Get All Races URL
	 *
	 * If the given item is the "All Races" category, then
	 * query the database for the "All Races" page.
	 *
	 * @param object $item    The current menu item.
	 * @param string $default The default URL for the menu item.
	 *
	 * @return string Permalink to use for menu item.
	 */
	private function _get_all_races_url( $item = null, $permalink = null ) {

		$args =
			array(
				'post_type'					=> 'page',
				'pagename'					=> 'all-races',
				'posts_per_page'			=> 1,
				'no_found_rows'				=> true,
				'update_post_meta_cache'	=> false,
			);

		$query = new WP_Query( $args );

		/**
		 * If there is a matching page, update $permalink otherwise
		 * use the default permalink.
		 */
		if ( $query->have_posts() ) {

			$query->the_post();

			$permalink = get_the_permalink();

		}

		/* Restore original Post Data */
		wp_reset_postdata();

		return $permalink;
	}

	/**
	 * Get Race Homepage URL
	 *
	 * If the given item is a "Race Category" (aka stagecat) then query the
	 * database for the current or previous "races_event" post.
	 *
	 * @param object $item    The current menu item.
	 * @param string $default The default URL for the menu item.
	 *
	 * @return string Permalink to use for menu item.
	 */
	private function _get_race_homepage_url( $item = null, $permalink = null ) {

		$current_year = date('Y');

		$args =
			array(
				'post_type'					=> 'races_event',
				'posts_per_page'			=> 1,
				'no_found_rows'				=> true,
				'update_post_meta_cache'	=> false,
				'tax_query'					=> array(
					'relation' => 'AND',
					array(
						'taxonomy'	=> 'stagecat',
						'field'		=> 'id',
						'terms'		=> ( isset( $item->object_id ) ? $item->object_id : '' )
					),
					array(
						'taxonomy'	=> 'stageyear',
						'field'		=> 'slug',
						'terms'		=> $current_year
					)
				)
			);

		$query = new WP_Query( $args );

		/**
		 * If there is a post for the current year, update $permalink otherwise
		 * query the database for a post last year's "races_event" post. If that
		 * exists, update $permalink, otherwise use the default permalink.
		 */
		if ( $query->have_posts() ) {

			$query->the_post();

			$permalink = get_the_permalink();

		} else {

			$args['tax_query'][1]['terms'] = ( $current_year - 1 );

			$query = new WP_Query( $args );

			if ( $query->have_posts() ) {

				$query->the_post();

				$permalink = get_the_permalink();

			}

		}

		/* Restore original Post Data */
		wp_reset_postdata();

		return $permalink;
	}
}
