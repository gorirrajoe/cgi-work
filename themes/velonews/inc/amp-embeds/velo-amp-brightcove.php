<?php

	class Velo_AMP_Brightcove extends AMP_Base_Embed_Handler {
		public function register_embed() {
			add_shortcode( 'brightcove', array( $this, 'brightcove_embed' ) );
		}

		public function unregister_embed() {
			remove_shortcode( 'brightcove' );
		}

		public function get_scripts() {
			if ( !$this->did_convert_elements ) {
				return array();
			}

			return array( 'amp-brightcove' => 'https://cdn.ampproject.org/v0/amp-brightcove-0.1.js' );
		}

		public function brightcove_embed( $atts = array() ) {
			$a = shortcode_atts(
				array(
					'id' => ''
				),
				$atts
			);

			if ( empty( $a['id'] ) ) {
				return;
			}

			// If we've made it this far then we will be displaying the shortcode
			// Record this
			$this->did_convert_elements = true;

			return sprintf(
					'<amp-brightcove
						data-account="3655502813001"
						data-player="r1oC9M1S"
						data-video-id="%s"
						layout="responsive"
						width="480"
						height="270">
					</amp-brightcove>',
					$a['id']
				);
		}
	}

?>
