<?php

	class Velo_AMP_Twitter extends AMP_Base_Embed_Handler {
		const URL_PATTERN = '#http(s|):\/\/twitter\.com(\/\#\!\/|\/)([a-zA-Z0-9_]{1,20})\/status(es)*\/(\d+)#i';

		public function register_embed() {
			add_shortcode( 'twitter', array( $this, 'twitter_embed' ) );
		}

		public function unregister_embed() {
			remove_shortcode( 'twitter' );
		}

		public function get_scripts() {
			if ( ! $this->did_convert_elements ) {
				return array();
			}

			return array( 'amp-twitter' => 'https://cdn.ampproject.org/v0/amp-twitter-0.1.js' );
		}

		public function twitter_embed( $atts = array() ) {
			$a = shortcode_atts(
				array(
					'url' => '',
					'align' => 'center'
				),
				$atts
			);

			// Make sure there is a URL of tweeet to display
			if ( empty( $a['url'] ) ) {
				return;
			}

			// Grab the tweet ID
			if ( is_numeric( $a['url'] ) ) {
				$id = $a['url'];
			} else {
				preg_match( self::URL_PATTERN, $a['url'], $matches );
				if ( isset( $matches[5] ) && is_numeric( $matches[5] ) ) {
					$id = $matches[5];
				}

				if ( empty( $id ) ) {
					return '';
				}
			}

			// If we've made it this far then we will be displaying the shortcode
			// Record this
			$this->did_convert_elements = true;

			return sprintf(
				'<amp-twitter width="486" height="657" layout="responsive" data-tweetid="%s" data-align="%s"></amp-twitter>',
				$id,
				$a['align']
			);

		}
	}

?>
