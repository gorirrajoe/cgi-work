<?php

	class Velo_AMP_Caption extends AMP_Base_Embed_Handler {

		public function register_embed() {
			add_shortcode( 'wp_caption', array( $this, 'img_caption_shortcode' ) );
			add_shortcode( 'caption', array( $this, 'img_caption_shortcode' ) );
		}

		public function unregister_embed() {
			remove_shortcode( 'wp_caption' );
			remove_shortcode( 'caption' );
		}

		/*
		 * This embed code is a moded version from the wp-includes/media.php file
		 */
		public function img_caption_shortcode( $attr = array(), $content = null ) {

			if ( ! isset( $attr['caption'] ) ) {
				if ( preg_match( '#((?:<a [^>]+>\s*)?<img [^>]+>(?:\s*</a>)?)(.*)#is', $content, $matches ) ) {
					$content = $matches[1];
					$attr['caption'] = trim( $matches[2] );

				}
			} elseif ( strpos( $attr['caption'], '<' ) !== false ) {
				$attr['caption'] = wp_kses( $attr['caption'], 'post' );
			}

			$output = apply_filters( 'img_caption_shortcode', '', $attr, $content );
			if ( $output != '' )
				return $output;

			$atts = shortcode_atts( array(
				'id'	  => '',
				'align'	  => 'alignnone',
				'width'	  => '',
				'caption' => '',
				'class'   => '',
			), $attr, 'caption' );

			$atts['width'] = (int) $atts['width'];
			if ( $atts['width'] < 1 || empty( $atts['caption'] ) )
				return $content;

			if ( ! empty( $atts['id'] ) )
				$atts['id'] = 'id="' . esc_attr( sanitize_html_class( $atts['id'] ) ) . '" ';

			$class = trim( 'wp-caption ' . $atts['align'] . ' ' . $atts['class'] );

			$html5 = current_theme_supports( 'html5', 'caption' );
			// HTML5 captions never added the extra 10px to the image width
			$width = $html5 ? $atts['width'] : ( 10 + $atts['width'] );

			$caption_width = apply_filters( 'img_caption_shortcode_width', $width, $atts, $content );

			$html = '';
			if ( $html5 ) {
				$html = '<figure ' . $atts['id'] . 'class="' . esc_attr( $class ) . '">'
				. do_shortcode( $content ) . '<figcaption class="wp-caption-text">' . $atts['caption'] . '</figcaption></figure>';
			} else {
				$html = '<div ' . $atts['id'] . 'class="' . esc_attr( $class ) . '">'
				. do_shortcode( $content ) . '<p class="wp-caption-text">' . $atts['caption'] . '</p></div>';
			}

			// replace "img" tags with "amp-img" here so that it takes on the proper classes
			$html = preg_replace( '/<img/', '<amp-img layout="responsive"', $html );

			return $html;

		}
	}
