<?php
/**
 * Class holding anything Search related
 */

class VeloNews_Search {

	static $instance = false;

	public function __construct() {

		add_action( 'template_redirect', array( $this, 'search_url_rewrite' ) );

		add_action( 'wp', array( $this, 'search_redirect' ) );

		// Algolia Search filters
		add_filter( 'algolia_searchable_post_shared_attributes', array( $this, 'unset_attributes' ), 10, 2 );
		add_filter( 'algolia_searchable_posts_index_settings', array( $this, 'remove_settings' ) );
		add_filter( 'algolia_get_post_images', array( $this, 'remove_image_sizes' ) );
		add_filter( 'algolia_should_index_post', array( $this, 'should_index_post' ), 10, 2 );
		add_filter( 'algolia_should_index_searchable_post', array( $this, 'should_index_post' ), 10, 2 );
		add_filter( 'algolia_post_parser', array( $this, 'change_content_parser' ) );
		add_filter( 'algolia_indexing_batch_size', function() {
			return 200;
		} );

		define( 'ALGOLIA_SPLIT_POSTS', false );

	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}

	/**
	 * Unset any inarticle ad if there was not enough content for the ad to display
	 *
	 * @param  array $ads Contains all the ads that we are preparing to display
	 * @return array      $ads excluding/inlcuding inarticle ads, depending on the logic happening
	 */


	/**
	 * Rewrite the template for the search so we can use WP Search/Bing/Algolia
	 */
	public function search_url_rewrite() {

		$search_type	= function_exists('cgi_bikes_get_option') ? cgi_bikes_get_option( 'search_type' ) : 'wordpress';

		if ( ( is_search() && $search_type != 'algolia' ) || ( $search_type == 'bing' && isset( $_REQUEST['q'] ) ) ) {

			global $wp_query;

			if ( !$wp_query->is_search ) {
				$wp_query->is_search = 1;
			}

			include( get_stylesheet_directory() . '/page-templates/site-search.php' );
			exit();

		}

	}

	/**
	 * Redirect posts that have the search query in the URL
	 * Google keeps crawling old URLs when we had a bug
	 */
	public function search_redirect() {
		global $wp_query;

		$search_type	= function_exists('cgi_bikes_get_option') ? cgi_bikes_get_option( 'search_type' ) : 'wordpress';

		if ( !is_admin() && ( ( $search_type != 'algolia' && isset( $_REQUEST['s'] ) ) || ( $search_type == 'bing' && isset( $_REQUEST['q'] ) ) ) && !is_front_page() ) {
			$site_url	= get_site_url();
			$query_string	= $_SERVER['QUERY_STRING'];

			wp_redirect( $site_url .'?'. $query_string , 301 );
			exit;
		}

	}

	public function unset_attributes( $shared_attributes, WP_Post $post ) {

		unset( $shared_attributes['comment_count'] );
		unset( $shared_attributes['menu_order'] );
		unset( $shared_attributes['taxonomies_hierarchical'] );
		unset( $shared_attributes['is_sticky'] );
		unset( $shared_attributes['post_author']['user_url'] );
		unset( $shared_attributes['taxonomies']['post_format'] );
		unset( $shared_attributes['taxonomies']['stagecat'] );
		unset( $shared_attributes['taxonomies']['stageyear'] );
		unset( $shared_attributes['taxonomies']['team'] );

		return $shared_attributes;

	}

	public function remove_settings( $settings ) {

		// Values to remove
		// Leaving some as array in case we ever want to manipulate further
		$settings_to_remove	= array(
			'attributesToIndex'		=> array(
				'unordered(taxonomies)'
			),
			'customRanking'			=> array(
				'desc(is_sticky)'
			),
			'attributesForFaceting'	=> array(
				'taxonomies',
				'taxonomies_hierarchical',
				'post_author.display_name'
			)
		);

		foreach ( $settings_to_remove as $setting => $setting_values ) {

			foreach ( $setting_values as $value ) {

				$key_to_remove	= array_search( $value, $settings[$setting] );

				unset( $settings[$setting][$key_to_remove] );

				// Reset the values for the array
				$settings[$setting]	= array_values( $settings[$setting] );

			}

		}

		// Add some settings
		$settings['attributesToIndex'][]	= 'unordered(post_excerpt)';
		$settings['attributesToSnippet'][]	= 'post_excerpt:30';

		return $settings;

	}

	public function remove_image_sizes( $images ) {

		// Remove all image sizes except the size we need
		// To keep the algolia object size low
		if ( !empty( $images ) && isset( $images['newsletter-top'] ) ) {

			$select_image_sizes	= array('newsletter-top');

			$images	= array_intersect_key( $images, array_flip( $select_image_sizes ) );

		}

		return $images;

	}

	public function should_index_post( $should_index, WP_Post $post ) {

		if ( false === $should_index )
			return $should_index;

		if ( $post->post_type !== 'post' )
			return false;

		return $should_index;

	}

	public function change_content_parser( Algolia\DOMParser $parser ) {
		// Custom selectors.
		$parser->setAttributeSelectors( array(
			'title1'		=> 'h1#unused',
			'title2'		=> 'h2#unused',
			'title3'		=> 'h3#unused',
			'title4'		=> 'h4#unused',
			'title5'		=> 'h5#unused',
			'title6'		=> 'h6#unused',
			'content'		=> 'h1, h2, h3, h4, h5, h6, p, ul, ol, dl, table',
			'post_excerpt'	=> 'p',
			'post_title'	=> 'p',
		) );
		// Custom exlusion rules.
		$parser->setExcludeSelectors( array(
			'pre', 'script', 'style'
		) );

		return $parser;
	}

}
