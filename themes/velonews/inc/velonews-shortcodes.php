<?php
/**
 * VeloNews Shortcodes
 *
 * Adds custom shortcodes to the WordPress API.
 *
 * NOTICE
 *
 * The Facebook Instant Articles plugin requires the following rules be added to
 * the custom ruleset. Instant Articles > Publishing Settings >
 * Custom transformer rules.
 *
 * <code>
 * {
 *   "rules": [
 *     {
 *       "class": "PassThroughRule",
 *       "selector": "figure.op-interactive"
 *     },
 *     {
 *       "class": "PassThroughRule",
 *       "selector": "figure.op-slideshow"
 *     },
 *     {
 *       "class": "PassThroughRule",
 *       "selector": "figure.op-ad"
 *     },
 *     {
 *       "class": "SocialEmbedRule",
 *       "selector": "iframe",
 *       "properties": {
 *         "socialembed.url": {
 *           "type": "string",
 *           "selector": "iframe",
 *           "attribute": "src"
 *         },
 *         "socialembed.height": {
 *           "type": "int",
 *           "selector": "iframe",
 *           "attribute": "height"
 *         },
 *         "socialembed.width": {
 *           "type": "int",
 *           "selector": "iframe",
 *           "attribute": "width"
 *         },
 *         "socialembed.iframe": {
 *           "type": "children",
 *           "selector": "iframe"
 *         },
 *         "socialembed.caption": {
 *           "type": "element",
 *           "selector": "figcaption"
 *         }
 *       }
 *     }
 *   ]
 * }
 * </code>
 */
class VeloNews_Shortcodes {

	public static $instance = false;

	public function __construct() {
		$this->_add_actions();
	}

	/**
	 * Facebook Embed
	 *
	 * Embeds a the given facebook video in the current post.
	 *
	 * Example:
	 * [facebook url="https://www.facebook.com/VeloNewsMagazine/videos/10154150108373656/" /]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function facebook_embed( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'url'	=> '',
				'type'	=> 'video'
			),
			$atts
		);

		if ( empty( $a['url'] ) ) {
			return;
		}

		if ( defined( 'INSTANT_ARTICLES_SLUG' ) && is_feed( INSTANT_ARTICLES_SLUG ) ) {
			if( $a['type'] == 'video' ) {
				$output = sprintf(
					'<figure class="op-interactive">
						<iframe>
							<script>(function(d, s, id) {
								var js, fjs = d.getElementsByTagName(s)[0];
								if (d.getElementById(id)) return;
								js = d.createElement(s); js.id = id;
								js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&amp;version=v2.5";
								fjs.parentNode.insertBefore(js, fjs);
							}(document, "script", "facebook-jssdk"));</script>
							<div id="fb-root"></div>
							<div class="fb-video" data-href="%s" data-allowfullscreen="true"></div>
						</iframe>
					</figure>',
					$a['url']
				);
			} elseif( $a['type'] == 'post' ) {
				$output = sprintf(
					'<figure class="op-interactive">
						<iframe>
							<script>(function(d, s, id) {
								var js, fjs = d.getElementsByTagName(s)[0];
								if (d.getElementById(id)) return;
								js = d.createElement(s); js.id = id;
								js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&amp;version=v2.5";
								fjs.parentNode.insertBefore(js, fjs);
							}(document, "script", "facebook-jssdk"));</script>
							<div id="fb-root"></div>
							<div class="fb-post" data-href="%s"></div>
						</iframe>
					</figure>',
					$a['url']
				);
			}

		} else {
			if( $a['type'] == 'video' ) {
				$output = sprintf(
					'<div id="fb-root"></div>
					<script>(function(d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) return;
						js = d.createElement(s); js.id = id;
						js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&amp;version=v2.5";
						fjs.parentNode.insertBefore(js, fjs);
					}(document, "script", "facebook-jssdk"));</script>
					<div class="fb-video" data-href="%s" data-allowfullscreen="true"></div>',
					$a['url']
				);
			} elseif( $a['type'] == 'post' ) {
				$encoded_url = urlencode( $a['url'] );
				$output = sprintf(
					'<iframe src="https://www.facebook.com/plugins/post.php?href=%s&width=500" width="500" height="754" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>',
					$encoded_url
				);
			}

		}

		return $output;
	}

	/**
	 * Bootstrap Row Embed
	 *
	 * Embeds a Bootstrap row in the current post.
	 *
	 * Example:
	 * [grid]Code is poetry. Silence is golden.[/grid]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function bootstrap_row_embed( $atts = array(), $content = null ) {

		$content = do_shortcode( trim( $content, '<br />' ) );
		$content = preg_replace( '/<\/div><br\ \/>/', '</div>', $content );

		return sprintf('<div class="row">%s</div>', $content );
	}

	/**
	 * Bootstrap Column Embed
	 *
	 * Embeds a Bootstrap column in the current post.
	 *
	 * Example:
	 * [grid]
	 *     [column count="2"]This is text wrapped in a 6-column div.[/column]
	 *     [column count="2"]This is text wrapped in a 6-column div.[/column]
	 * [grid]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function bootstrap_column_embed( $atts = array(), $content = null ) {

		$a = shortcode_atts(
			array(
				'count' => ''
			),
			$atts
		);

		$columns = intval( $a['count'] );

		// if count value isn't set, ignore shortcode
		if ( $columns < 1 || empty( $content ) ) {
			return $content;
		}

		switch ( $columns ) {
			case 2:
				$class = 'col-xs-12 col-md-6 grid-exempt';
				break;
			case 3:
				$class = 'col-xs-12 col-md-4 grid-exempt';
				break;
			case 4:
				$class = 'col-xs-6 col-md-3 grid-exempt';
				break;
			default:
				$class = 'col-xs-12 col-md-6 grid-exempt';
				break;
		}

		$content = trim( wpautop( wptexturize( $content ) ) );

		return sprintf('<div class="%s">%s</div>', $class, $content );
	}

	/**
	 * WordPress Gallery Embed
	 *
	 * Overwrites the default WordPress gallery shortcode.
	 *
	 * Example:
	 * [gallery ids="" include="" exclude="" order="" orderby="" /]
	 *
	 * @link https://codex.wordpress.org/Gallery_Shortcode
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function gallery_embed( $attr = array(), $content = null ) {

		global $post;

		// check $post exists
		if ( empty( $post ) ) {
			return;
		}

		static $index = 0;
		$index++;

		if ( ! empty( $attr['ids'] ) ) {

			// 'ids' is explicitly ordered, unless you specify otherwise.
			if ( empty( $attr['orderby'] ) ) {
				$attr['orderby'] = 'post__in';
			}

			$attr['include'] = $attr['ids'];

		}

		$output = apply_filters( 'post_gallery', '', $attr, $content );

		if ( ! empty( $output ) ) {
			return $output;
		}

		$size = ( ( is_page_template( 'page-templates/longform.php' ) || in_category( 'feature' ) ) ? 'longform-image-gallery' : 'image-gallery');

		$html5 = current_theme_supports( 'html5', 'gallery' );

		$atts = shortcode_atts(
			array(
				'order'			=> 'ASC',
				'orderby'		=> 'menu_order ID',
				'id'			=> ( $post ? $post->ID : 0 ),
				'itemtag'		=> ( $html5 ? 'figure'     : 'dl' ),
				'icontag'		=> ( $html5 ? 'div'        : 'dt' ),
				'captiontag'	=> ( $html5 ? 'figcaption' : 'dd' ),
				'columns'		=> 1,
				//'size'		=> $size,
				'include'		=> '',
				'exclude'		=> '',
				'link'			=> '',
				'autoplay'		=> ''
			),
			$attr,
			'gallery'
		);

		$id = intval( $atts['id'] );

		if ( ! empty( $atts['include'] ) ) {

			$attachments = array();

			$params = array(
				'post__in'			=> explode( ',', $atts['include'] ),
				'post_status'		=> 'inherit',
				'post_type'			=> 'attachment',
				'post_mime_type'	=> 'image',
				'order'				=> $atts['order'],
				'orderby'			=> $atts['orderby'],
				'posts_per_page'	=> -1
			);

			$_attachments = new WP_Query( $params );

			if ( $_attachments->have_posts() ) : while ( $_attachments->have_posts() ) : $_attachments->the_post();

				$attachments[$post->ID] = $post;

			endwhile; endif;

			wp_reset_postdata();

		} elseif ( ! empty( $atts['exclude'] ) ) {

			$params =
				array(
					'post_parent'		=> $id,
					'exclude'			=> $atts['exclude'],
					'post_status'		=> 'inherit',
					'post_type'			=> 'attachment',
					'post_mime_type'	=> 'image',
					'order'				=> $atts['order'],
					'orderby'			=> $atts['orderby']
				);

			$attachments = get_children( $params );

		} else {

			$params =
				array(
					'post_parent'		=> $id,
					'post_status'		=> 'inherit',
					'post_type'			=> 'attachment',
					'post_mime_type'	=> 'image',
					'order'				=> $atts['order'],
					'orderby'			=> $atts['orderby']
				);

			$attachments = get_children( $params );

		}

		if ( empty( $attachments ) ) {
			return '';
		}

		if ( is_feed() && ( ! defined( 'INSTANT_ARTICLES_SLUG' ) || ! is_feed( INSTANT_ARTICLES_SLUG ) ) ) {

			$output = "\n";

			foreach ( $attachments as $att_id => $attachment ) {
				$output .= wp_get_attachment_link( $att_id, $size, true ) . "\n";
			}

			return $output;

		}

		$valid_tags = wp_kses_allowed_html( 'post' );

		$item_tag = tag_escape( $atts['itemtag'] );
		$caption_tag = tag_escape( $atts['captiontag'] );
		$icon_tag = tag_escape( $atts['icontag'] );

		if ( ! isset( $valid_tags[ $item_tag ] ) ) {
			$item_tag = 'dl';
		}

		if ( ! isset( $valid_tags[ $caption_tag ] ) ) {
			$caption_tag = 'dd';
		}

		if ( ! isset( $valid_tags[ $icon_tag ] ) ) {
			$icon_tag = 'dt';
		}

		$columns = intval( $atts['columns'] );
		$item_width = ( $columns > 0 ? floor(100/$columns) : 100 );
		$float = is_rtl() ? 'right' : 'left';

		$selector = "gallery-{$index}";

		$gallery_style = '';

		$size_class = sanitize_html_class( $size );

		$carousel = ( 'stage' === get_post_type($post->ID) ? 'race-carousel' : 'generic-carousel');
		$gallery_div = sprintf('<div id="%s" class="gallery galleryid-%s owl-carousel %s">', $selector, $id, $carousel );

		$output = apply_filters( 'gallery_style', $gallery_style . $gallery_div );

			foreach ( $attachments as $id => $attachment ) {
				$excerpt = trim( $attachment->post_excerpt );

				$attr = ( $excerpt ? array( 'aria-describedby' => "{$selector}-{$id}" ) : '' );

				// add fancybox support
				$image_output = wp_get_attachment_link( $id, $size, false, false, false, $attr );
				$image_output = str_replace( 'href', 'class="fancybox" data-fancybox="'. $selector .'" data-caption=\''. wptexturize($excerpt) .'\' href', $image_output );

				// set up lazy load on gallery images
				$image_output = str_replace( 'attachment-image-gallery', 'attachment-image-gallery owl-lazy', $image_output );
				$image_output = str_replace( 'attachment-longform-image-gallery', 'attachment-longform-image-gallery owl-lazy', $image_output );
				$image_output = str_replace( 'src', 'data-src', $image_output );

				// clean up the image caption

				if ( $caption_tag && ( ! empty( $attachment->post_excerpt ) || ! empty( $attachment->post_title ) ) ) {
					$caption_copy = ( ! empty( $attachment->post_title ) ? "<h3>{$attachment->post_title}</h3>" : '' );
					$caption_copy .= '<span>'. wptexturize($excerpt) .'</span>';

					$image_output .= sprintf(
						'<%1$s class="wp-caption-text gallery-caption" id="%2$s">%3$s</%1$s>',
						$caption_tag,
						$selector .'-'. $id,
						$caption_copy
					);

				}

				$output .= sprintf(
					'<%1$s class="gallery-item">%2$s</%1$s>',
					$item_tag,
					$image_output
				);

				if ( ! $html5 && $columns > 0 && ++$i % $columns == 0 ) {
					$output .= '<br style="clear: both" />';
				}

			}

			/**
			 * show last slide ad if it exists in the admin options
			 */
			if( ! is_page_template( 'page-templates/longform.php' ) ) {

				$last_slide = cgi_bikes_get_option( 'editorial_gallery_last_slide_image' );

				if( $last_slide != '' ) {

					$output .= sprintf(
						'<figure class="gallery-item gallery-ad-last-slide">
							<a href="%s" target="_blank">
								<img data-src="%s" class="owl-lazy">
							</a>
						</figure>',
						cgi_bikes_get_option( 'editorial_gallery_last_slide_url' ),
						$last_slide
					);

				}

			}

			if ( ! $html5 && $columns > 0 && $i % $columns !== 0 ) {
				$output .= "<br style='clear: both' />";
			}

		$output .= "</div>\n";

		return $output;
	}

	/**
	 * Instagram Embed
	 *
	 * Adds the given instagram photo to the curent post.
	 *
	 * Example:
	 * [instagram url="https://www.instagram.com/p/BEW239AmoHh/" hidecaption="{true|false}" align="{left|center|right}"]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function instagram_embed( $atts = array(), $content = null ) {

		$a = shortcode_atts(
			array(
				'url' => '',
				'hidecaption' => '',
				'align' => ''
			),
			$atts
		);

		if ( empty( $a['url'] ) ) {
			return;
		}

		$align = ( $a['align'] == '' ? '' : 'align'. $a['align'] );

		if ( defined( 'INSTANT_ARTICLES_SLUG' ) && is_feed( INSTANT_ARTICLES_SLUG ) ) {

			$hidecaption = ( $a['hidecaption'] == 'true' ? 'true' : '' );

			$json		= file_get_contents( 'https://api.instagram.com/oembed?url='. $a['url'] .'&hidecaption='. $hidecaption  );
			$response	= ( ! empty($json) ? json_decode( $json ) : '' );

			return sprintf(
				'<figure class="op-interactive">
					<iframe>%s</iframe>
				</figure>',
				$response->html
			);

		} else {

			$hidecaption = ( $a['hidecaption'] == 'true' ? 'true' : '' );

			$json		= file_get_contents( 'https://api.instagram.com/oembed?url='. $a['url'] .'&hidecaption='. $hidecaption  );
			$response	= ( ! empty($json) ? json_decode( $json ) : '' );

			return sprintf(
				'<div class="instagram_embed %s">%s</div>',
				$align,
				$response->html
			);

		}

	}

	/**
	 * Embed Newsletter
	 *
	 * Adds the newsletter subscribe form to a post/page.
	 *
	 * Example:
	 * [newsletter heading="Sample Heading" subheading="Sample Sub-Heading"]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function newsletter_embed( $atts = array(), $content = null ) {

		if( ! function_exists( 'cgi_bikes_get_option') ) {
			return $content;
		}

		$a = shortcode_atts(
			array(
				'heading'		=> cgi_bikes_get_option( 'nl_signup_title' ),
				'subheading'	=> cgi_bikes_get_option( 'nl_signup_blurb' ),
			),
			$atts
		);

		$output = '<div class="newsletter newsletter_position_inline">';

			$output .= '<div class="newsletter__wrap">';

				$output .= sprintf( '<h3>%s</h3>', $a['heading'] );
				$output .= sprintf( '<p>%s</p>', $a['subheading'] );

				$output .= '<form class="newsletter__form" id="newsletter-inarticle" method="POST" action="" onsubmit="_gaq.push([\'_trackEvent\', \'newsletter velo\', \'subscribe\', \'inarticle\']);">';

					$output .= wp_nonce_field( 'nl_subscribe_inarticle', 'nl_subscribe_inarticle_nonce', true, false );
					$output .= '<input type="hidden" name="pos" value="inarticle">';
					$output .= '<input type="hidden" name="mbid" value="velonews">';
					$output .= '<input type="hidden" name="sub" value="general">';

					$output .= '<label for="inarticle-email" class="screen-reader-text">Subscribe to the VELONEWS newsletter</label>';
					$output .= '<input type="text" name="inarticle-email" id="inarticle-email" placeholder="EMAIL ADDRESS" class="newsletter__input newsletter__input_name_email">';
					$output .= '<button type="submit" id="inarticle-submit" class="btn btn--red btn_type_submit">SIGN UP</button>';

					$output .= '<p class="errorMessage newsletter__error"></p>';

					$output .= '<div class="newsletter__loading">';
						$output .= sprintf( '<img src="%s/images/icontact-wait-sidebar.gif">', get_bloginfo('stylesheet_directory') );
					$output .= '</div>';

				$output .= '</form>';

			$output .= '</div>';

		$output .= '</div>';

		return $output;
	}

	/**
	 * Pull-quote Embed
	 *
	 * Adds blockquote tags around the given content.
	 *
	 * Example:
	 * [pullquote attrib="John Doe" align="{left|center|right}"]Code is poetry. Silence is golden.[/pullquote]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function pullquote_embed( $atts = array(), $content = null ) {

		$a = shortcode_atts(
			array(
				'align'		=> '',
				'attrib'	=> ''
			),
			$atts
		);

		$align = ( ! empty( $a['align'] ) ? $a['align'] : 'center' );
		if( $a['attrib'] != '' ) {
			$attrib = '<footer>&ndash; '. $a['attrib'] .'</footer>';
		} else {
			$attrib = '';
		}

		return sprintf(
			'<blockquote class="pullquote align%s"><span>%s</span>%s</blockquote>',
			$align,
			do_shortcode( $content ),
			$attrib
		);

	}

	/**
	 * Related Articles Embed
	 *
	 * Adds a set of related articles to the current post.
	 *
	 * Example:
	 * [related title="Sample Title" tag="Sample Tag" align="{left|center|right}"]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function related_articles_embed( $atts = array(), $content = null ) {

		$post_id	= get_the_ID();
		$post_tags	= get_the_tags( $post_id );
		$tags_array	= array();
		$tag_string	= '';

		if( !empty( $post_tags ) ) {

			foreach( $post_tags as $tag ) {
				$tags_array[]	.= $tag->slug;
			}

			$tag_string = implode(',', $tags_array);

		}

		$a = shortcode_atts(
			array(
				'title'		=> 'Related Coverage',
				'tag'		=> $tag_string,
				'align'		=> 'none',
				'post-ids'	=> '',
				'style'		=> 0,
				'location'	=> ''
			),
			$atts
		);

		$align = ( !empty( $a['align'] ) ? 'align'. $a['align'] : '' );

		// define arguments that queries have in common
		$args = array(
			'posts_per_page'			=> 4,
			'post__not_in'				=> array( $post_id ),
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false,
			'ignore_sticky_posts'		=> true,
			'has_password'				=> false
		);

		if ( $a['location'] == 'article-footer' ) {

			// When the widget is placed in the article-footer, it displays articles based on the post's categories
			$cat_IDs	=  wp_get_post_categories( $post_id, array( 'fields' => 'ids' ) );

			// check if AMP article with the AMP plugin's method
			$is_amp_endpoint = function_exists( 'is_amp_endpoint' ) ? is_amp_endpoint() : false;

			if ( !empty( $cat_IDs ) ) {

				$args['category__in']	= $cat_IDs;
				$args['post_type']		= 'any';

				$related_query	= new WP_Query( $args );

				if ( $related_query->have_posts() ):
					ob_start(); ?>

					<section class="content__section">
						<div class="container">
						<header class="col-xs-12 content__section__header">
							<h2><?php echo $a['title']; ?></h2>
						</header>

						<?php while( $related_query->have_posts() ) : $related_query->the_post();

							$post_ID	= get_the_ID();
							$the_title	= get_the_title();
							$cat_array	= get_cat_array( $post_ID );

							// standard classes we are adding to the post
							$post_classes	= array( 'article', 'article_type_latest', 'col-xs-12', 'col-sm-3' );

							// hide last 2 on larger sizes screens:
							if ( $related_query->current_post > 1 ) { $post_classes[] = 'hidden-xs'; }

							// create ad fallback class if first post
							$ad_fallback_class	= $related_query->current_post == 0 ? ' ad_fallback' : '';

							if ( is_array( $cat_array['slug'] ) ) {
								foreach ( $cat_array['slug'] as $slug ) {
									$post_classes[] = 'article_category_' . $slug;
								}
							} else {
								$post_classes[] = 'article_category_' . $cat_array['slug'];
							}

							// build image tag
							if ( $is_amp_endpoint ) {
								$thumb		= wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );
								$thumbnail	= '<amp-img layout="responsive" src="'. $thumb[0] .'" width="'. $thumb[1] .'" height="'. $thumb[2] .'" alt="'. $the_title .'"></amp-img> ';
							} else {
								$thumbnail	= get_the_post_thumbnail_url( $post_ID, 'featured-thumb-lg' );
								$thumbnail	= '<img data-original="' .$thumbnail .'" alt="'. $the_title .'" class="article__thumbnail lazyload">';
							} ?>

							<article <?php post_class( $post_classes ); ?>>

								<?php if ( $related_query->current_post == 0 && !$is_amp_endpoint ) {
									echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('content-sponsored') : '';
								} ?>

								<a href="<?php the_permalink(); ?>" target="_self" class="article__permalink<?php echo $ad_fallback_class; ?>" title="<?php echo $the_title; ?>">
									<div class="article__thumbnail__wrap">
										<?php echo $thumbnail; ?>
										<div class="article__thumbnail__overlay"></div>
									</div>
									<div class="article__category"><?php echo get_article_category_tag( $post_ID, $cat_array ); ?></div>
									<p class="article__title"><?php echo $the_title; ?></p>
								</a>
							</article>

						<?php endwhile; ?>
						</div>
					</section>
					<?php
					$widget	= ob_get_clean();

				endif;

				wp_reset_postdata();

				return $widget;

			}

		} elseif ( !empty( $a['tag'] ) ) {

			$widget = '';

			// add the tag(s) to the arguments
			$args['tag']	= $a['tag'];

			// use IDs instead of by tag if defined
			if ( !empty( $a['post-ids'] ) ) {

				unset( $args['tag'] );
				unset( $args['post__not_in'] );

				$args['post__in'] = explode( ',', $a['post-ids'] );
			}

			$related_query = new WP_Query( $args );

			// Save our data into an array we can loop over several times instead of making all the calls repeatedly
			$posts_array = array();

			if ( $related_query->have_posts() ) {
				while( $related_query->have_posts() ) {
					$related_query->the_post();

					// so we can keep add to array
					$loop_count	= $related_query->current_post;

					$posts_array[$loop_count]['title']		= get_the_title();
					$posts_array[$loop_count]['permalink']	= get_permalink();
					$posts_array[$loop_count]['excerpt']	= get_the_excerpt();

					if ( has_post_thumbnail() ) {

						$image = '<figure class="article__thumbnail recommended-content__thumbnail">' .
							get_the_post_thumbnail() .'
						</figure>';

					} else {
						$image = '';
					}

					$posts_array[$loop_count]['image'] = $image;

				}

				wp_reset_postdata();

			}

			// Make sure the array of posts is not empty
			if ( !empty( $posts_array ) ) {

				// Original widget
				$style = $a['style'] == 0 ? ' style="display: block;"' : ' style="display: none;"';

				$widget = sprintf('<aside class="recommended-content %s"%s>', $align, $style );

					$widget .= '<div class="recommended-content__wrap">';

						$widget .= sprintf('<h3 class="recommended-content__title">%s</h3>', $a['title']);

						$widget .= '<ul class="recommended-content__menu">';

							foreach( $posts_array as $post ) {

								$widget .= sprintf( '<li><a href="%s">%s</a></li>', $post['permalink'], $post['title'] );

							}

						$widget .= '</ul>';

					$widget .= '</div>';

				$widget .= '</aside>';


				/*// Widget alt version 1 for A/B Testing
				$style	= $a['style'] == 1 ? ' style="display: block;"' : ' style="display: none;"';

				$widget .= sprintf( '<aside class="recommended-content alt-1 %s"%s>', $align, $style );

					$widget .= '<div class="recommended-content__wrap">';

						$widget .= sprintf( '<h3 class="recommended-content__title">%s</h3>', $a['title'] );

						$widget .= '<ul class="recommended-content__menu">';

							foreach ( $posts_array as $post ) {

								$widget .= sprintf( '<li class="recommended-content__menu-item"><a href="%s">%s</a></li>', $post['permalink'], $post['title'] );

							}

						$widget .= '</ul>';

					$widget .= '</div>';

				$widget .= '</aside>';*/


				// Widget alt version 1
				$style	= $a['style'] == 1 ? ' style="display: block;"' : ' style="display: none;"';

				$widget .= sprintf( '<aside class="recommended-content alt-2 %s"%s>', $align, $style );

					$widget .= '<div class="recommended-content__wrap">';

						$widget .= sprintf('<h3 class="recommended-content__title">%s</h3>', $a['title']);

						$widget .= '<ul class="recommended-content__menu">';

							foreach( $posts_array as $post ) {

								$widget .='<li class="recommended-content__menu-item">';

									$widget .= sprintf( '<a href="%s">', $post['permalink'] );

										$widget .= $post['image'];

										$widget .= '<div class="recommended-content__details">';
											$widget .= sprintf( '<h3 class="recommended-content__headline">%s</h3>', $post['title'] );
											$widget .= sprintf( '<p class="recommended-content__excerpt">%s</p>', $post['excerpt'] );
										$widget .= '</div>';

									$widget .= '</a>';

								$widget .= '</li>';

								break; // we only need the first post so we break out ASAP

							}

						$widget .= '</ul>';

					$widget .= '</div>';

				$widget .= '</aside>';

			}

			return $widget;
		}

		return $content;

	}

	/**
	 * Embed Twitter
	 *
	 * Adds the given tweet to the current post.
	 *
	 * Example:
	 * [twitter url="https://twitter.com/velonews/status/724731422385164288" align="{left|center|right}"]
	 */
	public function twitter_embed( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'url' => '',
				'align' => 'center'
			),
			$atts
		);

		if ( empty( $a['url'] ) ) {
			return;
		}

		if ( defined( 'INSTANT_ARTICLES_SLUG' ) && is_feed( INSTANT_ARTICLES_SLUG ) ) {

			$align = ( ! empty( $a['align'] ) ? $a['align'] : '' );

			$json		= file_get_contents( 'https://api.twitter.com/1/statuses/oembed.json?url=' . $a['url'] .'&align='. $align );
			$response	= json_decode( $json );

			return sprintf(
				'<figure class="op-interactive"><iframe>%s</iframe></figure>',
				$response->html
			);

		} else {

			$align = ( ! empty( $a['align'] ) ? $a['align'] : '' );
			$divalign = ( ! empty( $align ) ? "twitter_{$align}" : '' );

			$json		= file_get_contents( 'https://api.twitter.com/1/statuses/oembed.json?url=' . $a['url'] .'&align='. $align );
			$response	= json_decode( $json );

			return sprintf(
				'<div class="twitter_embed %s">
					%s
				</div>',
				$divalign,
				$response->html
			);

		}

	}

	/**
	 * YouTube Embed
	 *
	 * Embeds the given YouTube video in the current post.
	 *
	 * Example:
	 * [youtube id="GjUXUN-F9Pg" /]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function youtube_embed( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'id' => ''
			),
			$atts
		);

		if ( empty( $a['id'] ) ) {
			return;
		}

		return sprintf(
			'<div class="video-container">
				<iframe width="1280" height="720" src="https://www.youtube.com/embed/%s?rel=0" frameborder="0" allowfullscreen></iframe>
			</div>',
			$a['id']
		);

	}

	/**
	 * Visual Composer Image
	 *
	 * Embeds the given image ID into the current post.
	 *
	 * Example:
	 * [vc_single_image border_color="grey" img_link_target="_self" img_size="full" image="389121"]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function vc_image( $atts = array() ) {
		$a = shortcode_atts(
			array(
				'image' => '',
				'img_link_target' => '_self',
				'img_size' => 'full',
			),
			$atts
		);

		if ( empty( $a['image'] ) ) {
			return;
		}

		$content	= apply_filters( 'the_content', wp_get_attachment_image( $a['image'], $a['img_size'] ) );

		return $content;
	}

	/**
	 * Visual Composer Grid Gallery for Longform posts
	 *
	 * Embeds a Bootstrap column in the current post.
	 *
	 * Example:
	 * [grid]
	 *     [column count="2"]This is text wrapped in a 6-column div.[/column]
	 *     [column count="2"]This is text wrapped in a 6-column div.[/column]
	 * [grid]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function vc_images_grid( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'ids' 				=> '',
				'slides_per_view'	=> ''
			),
			$atts
		);

		$columns	= intval( $a['slides_per_view'] );
		$image_ids	= explode( ',', $a['ids'] );
		$images		= '';
		$content	= '';

		// if count value isn't set or there are no image ids, ignore shortcode. do not pass GO, do not collect $200
		if ( $columns < 1 || empty($image_ids) ) {
			return;
		}


		switch ( $columns ) {
			case 2:
				$class = 'col-xs-12 col-md-6 grid-exempt';
				break;
			case 3:
				$class = 'col-xs-12 col-md-4 grid-exempt';
				break;
			case 4:
				$class = 'col-xs-6 col-md-3 grid-exempt';
				break;
			default:
				$class = 'col-xs-12 col-md-6 grid-exempt';
				break;
		}

		foreach( $image_ids as $image_id ){
			$images	.= '<div class="'. $class .'">'. wp_get_attachment_image( $image_id, 'full' ) .'</div>';
		}

		return sprintf('<div class="row">%s</div>', $images );
	}

	/**
	 * Visual Composer Dummy Shortcode
	 *
	 * Dummy shortcode to replace the VC shortcodes with their contents.
	 *
	 * Examples:
	 * [vc_column_text]Wrapped Text[/vc_column_text]
	 * [vc_column]Wrapped Text[/vc_column]
	 * [vc_row]Wrapped Text[/vc_row]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function vc_plain_return( $atts = array(), $content = null ) {

		if ( empty( $content ) ) {
			return;
		}

		$content = do_shortcode($content);

		$content = apply_filters( 'the_content', $content );

		return $content;
	}


	/**
	 * Brightcove Embed
	 *
	 * Embeds the given Brightcove video in the current post.
	 *
	 * Example:
	 * [brightcove id="4933566086001" /]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function brightcove_embed( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'id' => ''
			),
			$atts
		);

		if ( empty( $a['id'] ) ) {
			return;
		}

		$adKw = class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::singleton()->generate_dfp_keywords() : '';

		ob_start(); ?>

			<div class="video-container">
				<div style="display: block; position: relative; max-width: 100%;">
					<div style="padding-top: 56.25%;">
						<video id="myVideo" data-video-id="<?php echo trim( $a['id'] ); ?>" data-account="3655502813001" data-player="r1oC9M1S" data-embed="default" class="video-js" controls style="width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;"></video>
					</div>
				</div>

				<script src="//players.brightcove.net/3655502813001/r1oC9M1S_default/index.min.js"></script>
				<?php if( cgi_bikes_get_option( 'preroll_on' ) == 'on' ): ?>
					<link href="//players.brightcove.net/videojs-ima3/2/videojs.ima3.min.css" rel="stylesheet">
					<script src="//players.brightcove.net/videojs-ima3/2/videojs.ima3.min.js"></script>
					<script type="text/javascript">
						var myPlayer = videojs('myVideo');
						myPlayer.ready(function(){
						    myPlayer=this;

						    myPlayer.ima3({
						        serverUrl:"https://pubads.g.doubleclick.net/gampad/ads?sz=720x480&iu=<?php echo $adKw; ?>&ciu_szs&impl=s&gdfp_req=1&env=vp&output=xml_vast2&unviewed_position_start=1&url=[referrer_url]&correlator=[timestamp]",
						        timeout:5000,
						        prerolltimeout:1000,
						        requestMode: 'onload',
						        debug:true,
						        adTechOrder:["html5", "flash"],
						        vpaidMode:"ENABLED"
						    });
						    myPlayer.one('loadedmetadata', function(){
						        // get a reference to the player
						        myPlayer = this;

						        myPlayer.on("ima3-ad-error",function(event){
						            console.log("ima3-ad-error: " + event);
						        });

						        myPlayer.on("adserror", function(event){
						            console.log("adsError Fired: " + event);
						        });
						     });
						});
					</script>
				<?php endif;?>
			</div>

		<?php return ob_get_clean();

	}


	/**
	 * Vimeo Embed
	 *
	 * Embeds the given Vimeo video in the current post.
	 *
	 * Example:
	 * [vimeo id="110962890" /]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function vimeo_embed( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'id' => ''
			),
			$atts
		);

		if ( empty( $a['id'] ) ) {
			return;
		}

		return sprintf(
			'<div class="video-container">
				<iframe src="https://player.vimeo.com/video/%s" width="1280" height="720" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>',
			$a['id']
		);

	}


	/**
	 * GIFS.com Embed
	 *
	 * Embeds the given gif image in the current post.
	 *
	 * Example:
	 * [gifs id="v2645r" /]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function gifs_embed( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'url'	=> '',
				'file'	=> ''
			),
			$atts
		);

		if ( empty( $a['url'] ) ) {
			return;
		}

		if ( defined( 'INSTANT_ARTICLES_SLUG' ) && is_feed( INSTANT_ARTICLES_SLUG ) ) {

			return sprintf(
				'<figure class="op-interactive">
					<iframe src="%s" frameborder="0" scrolling="no" width="480" height="270" style="-webkit-backface-visibility: hidden;-webkit-transform: scale(1);" ></iframe>
				</figure>',
				$a['url']
			);

		} else {

			return sprintf(
				'<div class="video-container">
					<iframe src="%s" frameborder="0" scrolling="no" width="480" height="270" style="-webkit-backface-visibility: hidden;-webkit-transform: scale(1);" ></iframe>
				</div>',
				$a['url']
			);

		}
	}


	/**
	 * Gift Guide/Buyer's Guide - Buy Now shortcode
	 *
	 * Embeds the Buy Now module in the current post.
	 * Mainly used in gift/buyer's guides
	 * store logos can be maintained in the theme options
	 *
	 * Example:
	 * [buy-now-btn storeoption="store_url" [storeoption=...] [text="Header Text"] /]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function buynowguide_function( $atts ) {
		$a = shortcode_atts(array(
			'text'			=> '',
			'amazon'		=> '',
			'artscyclery'	=> '',
			'cocyclist'		=> '',
			'chainreaction'	=> '',
			'wiggle'		=> ''
		), $atts);

		$header_txt = ( $a['text'] != '' ) ? $a['text'] : 'Buy Now From';

		$return_string = '<div class="buynow_module">
			<h3>'. $header_txt .':</h3>
			<ul>';
				if( $a['amazon'] != '' ) {
					$return_string .= '<li><a onclick="_gaq.push([\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $a['amazon'] .'\'])" href="'. $a['amazon'] .'" target="_blank"><img src="'. cgi_bikes_get_option( 'buynow_amazon' ) .'" alt="" /></a></li>';
				}
				if( $a['artscyclery'] != '' ) {
					$return_string .= '<li><a onclick="_gaq.push([\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $a['artscyclery'] .'\'])" href="'. $a['artscyclery'] .'" target="_blank"><img src="'. cgi_bikes_get_option( 'buynow_artscyclery' ) .'" alt="" /></a></li>';
				}
				if( $a['cocyclist'] != '' ) {
					$return_string .= '<li><a onclick="_gaq.push([\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $a['cocyclist'] .'\'])" href="'. $a['cocyclist'] .'" target="_blank"><img src="'. cgi_bikes_get_option( 'buynow_cocyclist' ) .'" alt="" /></a></li>';
				}
				if( $a['chainreaction'] != '' ) {
					$return_string .= '<li><a onclick="_gaq.push([\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $a['chainreaction'] .'\'])" href="'. $a['chainreaction'] .'" target="_blank"><img src="'. cgi_bikes_get_option( 'buynow_chainreaction' ) .'" alt="" /></a></li>';
				}
				if( $a['wiggle'] != '' ) {
					$return_string .= '<li><a onclick="_gaq.push([\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $a['wiggle'] .'\'])" href="'. $a['wiggle'] .'" target="_blank"><img src="'. cgi_bikes_get_option( 'buynow_wiggle' ) .'" alt="" /></a></li>';
				}
			$return_string .= '</ul>
		</div>';

		return $return_string;
	}


	// public function buysim_function( $atts ) {
	// 	$gg_options		= GiftGuide_Admin::get_guide_option( '2016', 'all' );

	// 	$a = shortcode_atts( array(
	// 		'text'			=> '',
	// 		'amazon'		=> '',
	// 		'artscyclery'	=> '',
	// 		'cocyclist'		=> '',
	// 		'chainreaction'	=> '',
	// 		'wiggle'		=> ''
	// 	), $atts );

	// 	$return_string = '<div class="buynow_module">
	// 		<h3>Buy Similar From:</h3>
	// 		<ul>';
	// 			if( $a['amazon'] != '' ) {
	// 				$return_string .= '<li><a onclick="_gaq.push([\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $a['amazon'] .'\'])" href="'. $a['amazon'] .'" target="_blank"><img src="'. $gg_options['buynow_2016_amazon'] .'" alt="" /></a></li>';
	// 			}
	// 			if( $a['artscyclery'] != '' ) {
	// 				$return_string .= '<li><a onclick="_gaq.push([\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $a['artscyclery'] .'\'])" href="'. $a['artscyclery'] .'" target="_blank"><img src="'. $gg_options['buynow_2016_artscyclery'] .'" alt="" /></a></li>';
	// 			}
	// 			if( $a['cocyclist'] != '' ) {
	// 				$return_string .= '<li><a onclick="_gaq.push([\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $a['cocyclist'] .'\'])" href="'. $a['cocyclist'] .'" target="_blank"><img src="'. $gg_options['buynow_cocyclist'] .'" alt="" /></a></li>';
	// 			}
	// 			if( $a['chainreaction'] != '' ) {
	// 				$return_string .= '<li><a onclick="_gaq.push([\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $a['chainreaction'] .'\'])" href="'. $a['chainreaction'] .'" target="_blank"><img src="'. $gg_options['buynow_chainreaction'] .'" alt="" /></a></li>';
	// 			}
	// 			if( $a['wiggle'] != '' ) {
	// 				$return_string .= '<li><a onclick="_gaq.push([\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $a['wiggle'] .'\'])" href="'. $a['wiggle'] .'" target="_blank"><img src="'. $gg_options['buynow_wiggle'] .'" alt="" /></a></li>';
	// 			}
	// 		$return_string .= '</ul>
	// 	</div>';

	// 	return $return_string;
	// }


	/**
	 * VeloPress Book Embed
	 *
	 * Adds VeloPress book embed to content.
	 *
	 * Example:
	 * [velopress align="{left|right}" {cta="Buy Now!"}]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function velopress_embed( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'align'	=> '',
				'cta'	=> '',
				'title'	=> ''
			),
			$atts
		);

		$book_id = ( get_post_meta( get_the_ID(), '_vn_velopress_book', 1 ) != '' ) ? get_post_meta( get_the_ID(), '_vn_velopress_book', 1 ) : '';

		/**
		 * if no book meta option was chosen, don't return anything
		 */
		if( $book_id != '' ) {

			$align	= ( !empty( $a['align'] ) ? 'align' . $a['align'] : 'aligncenter' );
			$cta	= ( !empty( $a['cta'] ) ? $a['cta'] : 'Buy Now' );
			$title	= ( !empty( $a['title'] ) ? $a['title'] : 'Read the whole book');

			$book_json	= file_get_contents( 'http://www.velopress.com/wp-json/velopress/book/?id=' . $book_id );

			$book_obj	= json_decode( $book_json );

			if( $book_obj->status == 'success' ) {

				return sprintf(
					'<aside class="velopress-book %s">

						<div class="velopress-book__header">
							<h4 class="velopress-book__header__module_title">%s</h4>
						</div>

						<div class="velopress-book__content">
							<figure class="velopress-book__content__cover">
								<a href="%s" target="_blank"><img src="%s"></a>
							</figure>
							<p class="velopress-book__content__cta"><a href="%s" target="_blank">%s</a></p>
							<h3 class="velopress-book__content__title"><a href="%s" target="_blank">%s</a></h3>
							<span class="velopress-book__content__subtitle">%s</span>
							<p class="velopress-book__content__author">%s</p>
							<p class="velopress-book__content__desc">%s</p>
						</div>

					</aside>',
					$align,
					$title,
					$book_obj->book_url,
					$book_obj->cover_url,
					$book_obj->book_url,
					$cta,
					$book_obj->book_url,
					$book_obj->title,
					$book_obj->sub_title,
					$book_obj->author,
					$book_obj->description

				);

			} else {
				// failure
				return sprintf(
					'<aside class="velopress-book %s">

						<div class="velopress-book__header">
							<h4 class="velopress-book__header__module_title">%s</h4>
						</div>

						<div class="velopress-book__content">
							<figure class="velopress-book__content__cover">
								<a href="%s" target="_blank"><img src="%s"></a>
							</figure>
							<p class="velopress-book__content__cta"><a href="%s" target="_blank">%s</a></p>
							<h3 class="velopress-book__content__title"><a href="%s" target="_blank">%s</a></h3>
							<div class="velopress-book__content__desc">%s</div>
						</div>

					</aside>',
					$align,
					$title,
					$book_obj->book_url,
					$book_obj->cover_url,
					$book_obj->book_url,
					$cta,
					$book_obj->book_url,
					$book_obj->title,
					$book_obj->description
				);
			}

		} else {

			return false;

		}

	}


	/**
	 * Buy Now shortcode
	 *
	 * Embeds the Buy Now button (link) in the current post.
	 *
	 * Example:
	 * [buy-now text="Buy This!" url="http://..." /]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function buynow_function( $atts ) {
		$a = shortcode_atts(array(
			'text'	=> '',
			'url'	=> ''
		), $atts);

		if ( empty( $a['url'] ) || empty( $a['text'] ) ) {
			return;
		}

		return sprintf(
			'<p><a class="btn--red btn--buynow" href="%s" target="_blank">%s</a></p>',
			$a['url'],
			$a['text']
		);

	}


	/**
	 * Table Shortcode - Wrapper
	 *
	 * When you REALLY need to show tabular data in a website
	 * Increments a static count so the page can keep track
	 * of multiple tables on the same page
	 *
	 * Table has a nice mobile view!
	 *
	 * Example:
	 * [table]...[/table]
	 */
	public function table_wrapper( $atts, $content = null ) {
		static $tablecount = 0;
		$tablecount++;

		return sprintf(
			'<table data-tablecount="'. $tablecount .'" class="tabular__data" cellspacing="0" cellpadding="0" border="0">%s</table>',
			do_shortcode( $content )
		);
	}


	/**
	 * Table Shortcode - Row
	 *
	 * Use the 'type="header"' option to surround the row with '<thead></thead>' tags
	 *
	 * Example:
	 * [tr type="header"]...[/tr]
	 */
	public function table_row( $atts, $content = null ) {
		$a = shortcode_atts(array(
			'type'	=> '',
		), $atts);

		if( $a['type'] == 'header' ) {
			return sprintf(
				'<thead><tr>%s</tr></thead>',
				do_shortcode( $content )
			);
		} else {
			return sprintf(
				'<tr>%s</tr>',
				do_shortcode( $content )
			);
		}


	}


	/**
	 * Table Shortcode - Data
	 *
	 * Use the 'type="header"' option to use '<th></th>' tags
	 * instead of the default '<td></td>' ones
	 *
	 * Example: [td type="header"]...[/td]
	 */
	public function table_data( $atts, $content = null ) {
		$a = shortcode_atts(array(
			'type'	=> '',
		), $atts);

		if( $a['type'] == 'header' ) {
			return sprintf(
				'<th>%s</th>',
				do_shortcode( $content )
			);
		} else {
			return sprintf(
				'<td>%s</td>',
				do_shortcode( $content )
			);
		}
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self();

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {

		// register "facebook" shortcode
		add_shortcode( 'facebook', array( $this, 'facebook_embed' ) );

		// override default gallery shortcode
		remove_shortcode( 'gallery' );
		add_shortcode( 'gallery',  array( $this, 'gallery_embed' ) );

		// register "grid" shortcode
		add_shortcode( 'grid', array( $this, 'bootstrap_row_embed' ) );
		add_shortcode( 'column', array( $this, 'bootstrap_column_embed' ) );

		// register "instagram" shortcode
		add_shortcode( 'instagram', array( $this, 'instagram_embed' ) );

		// register "newsletter" shortcode
		add_shortcode( 'newsletter', array( $this, 'newsletter_embed' ) );

		// register "pullquote" shortcode
		add_shortcode( 'pullquote', array( $this, 'pullquote_embed' ) );

		// register "related" shortcode
		add_shortcode( 'related',  array( $this, 'related_articles_embed' ) );

		// register "twitter" shortcode
		add_shortcode( 'twitter', array( $this, 'twitter_embed' ) );

		// register "youtube" shortcode
		add_shortcode( 'youtube', array( $this, 'youtube_embed' ) );

		// register "vimeo" shortcode
		add_shortcode( 'vimeo', array( $this, 'vimeo_embed' ) );

		// register "brightcove" shortcode
		add_shortcode( 'brightcove', array( $this, 'brightcove_embed' ) );

		// register "gifs" shortcode
		add_shortcode( 'gifs', array( $this, 'gifs_embed' ) );

		// register Visual Composer replacement shortcodes
		add_shortcode( 'vc_images_carousel', array( $this, 'vc_images_grid' ) );
		add_shortcode( 'vc_single_image', array( $this, 'vc_image' ) );
		add_shortcode( 'vc_column_text', array( $this, 'vc_plain_return' ) );
		add_shortcode( 'vc_column', array( $this, 'vc_plain_return' ) );
		add_shortcode( 'vc_row', array( $this, 'vc_plain_return' ) );

		// register "velopress" shortcode
		add_shortcode( 'velopress', array( $this, 'velopress_embed' ) );

		// register buy now buttons for buyers/gift guides
		add_shortcode( 'buy-now-guide-btn', array( $this, 'buynowguide_function' ) );

		add_shortcode( 'buy-now', array( $this, 'buynow_function' ) );

		// register table shortcodes
		add_shortcode( 'table', array( $this, 'table_wrapper' ) );
		add_shortcode( 'tr', array( $this, 'table_row' ) );
		// add_shortcode( 'th', array( $this, 'table_heading' ) );
		add_shortcode( 'td', array( $this, 'table_data' ) );
	}
}

