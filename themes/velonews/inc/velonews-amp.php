<?php

class VeloNews_AMP {

	static $instance = false;

	public function __construct() {
		$this->_add_actions();
	}

	public function parse_request( $is_parse, $wp, $extra_query_vars ) {

		/**
		 * is_amp_endpoint sucks for multipage so checking the URL
		 * It sucks because as noted in the helper function:
		 * "will always return `false` if called before the `parse_query` hook."
		 */
		$endpoint = AMP_QUERY_VAR;

		$url_parts   = explode( '?', $_SERVER['REQUEST_URI'] );
		$query_parts = explode( '/', $url_parts[0] );

		$is_amp = in_array( $endpoint, $query_parts );

		if ( $is_amp ) {
			$is_parse = false;
			$this->_parse_request( $wp, $extra_query_vars );
		}

		return $is_parse;
	}



	/**
	 * Ampify Post
	 *
	 * Determines whether the current post should be "Ampified" by checking the
	 * post's categories against the blacklist stored in the database.
	 *
	 * Note: This logic was specifically designed for the Automattic AMP plugin.
	 */
	public function ampify_post() {

		// Cannot use `get_queried_object` before canonical redirect; see https://core.trac.wordpress.org/ticket/35344
		global $post;

		$is_amp_endpoint = function_exists( 'is_amp_endpoint' ) ? is_amp_endpoint() : false;

		// check for endpoint on multipage and others with custom code
		$endpoint		= AMP_QUERY_VAR;

		$url_parts		= explode( '?', $_SERVER['REQUEST_URI'] );
		$query_parts	= explode( '/', $url_parts[0] );

		$is_amp_custom = in_array( $endpoint, $query_parts );

		if ( !is_singular() || is_feed() ) {
			return;
		}

		// Check if this is a multipage post and if it should be excluded
		if ( false !== strpos( $post->post_content, '<!--nextpage-->' ) ) {

			$exclude_multipage	= cgi_bikes_get_option( 'amp_exclude_multipage_posts' );

			if ( ( $is_amp_custom || $is_amp_endpoint ) && !empty( $exclude_multipage ) ) {
				wp_safe_redirect( get_permalink( $post->ID ) );
				exit;
			}

		}

		// Check if we should exclude the post based on theme category excludes
		$exclude_cats	= cgi_bikes_get_option( 'amp_exclude_cats' );

		if ( !empty( $exclude_cats ) ) {

			$blacklist	= explode( ',', $exclude_cats );

			if ( ( $is_amp_custom || $is_amp_endpoint ) && in_array( $blacklist ) ) {
				wp_safe_redirect( get_permalink( $post->ID ) );
				exit;
			}

		}

		$supports = post_supports_amp( $post );

		if ( !$supports ) {
			if ( $is_amp_custom || $is_amp_endpoint ) {
				wp_safe_redirect( get_permalink( $post->ID ) );
				exit;
			}
			return;
		}

		if ( $is_amp_custom || $is_amp_endpoint ) {
			remove_filter( 'template_redirect', 'redirect_canonical' );
			amp_render();
		} else {
			amp_add_frontend_actions();
		}

	}

	/**
	 * Middle man action function for adding filter to content only on AMP posts
	 */
	public function do_amp_content_filter() {

		// Do all the needed DOM manipulation first
		add_filter( 'the_content', array( $this, 'do_amp_post_content' ) );
		add_filter( 'the_content', array( $this, 'do_amp_multipage_content' ), -1 );

	}

	/**
	 * Do AMP Post Content
	 *
	 * Filters the content of AMP posts prior to the page being rendered.
	 *
	 * @param  string $content Post content
	 *
	 * @return string Sanitized post content.
	 */
	public function do_amp_post_content( $content = null ) {

		// Strip signature Post Snippets
		$content = preg_replace('/\[sig:(.+?)\]/', '', $content);

		// Remove empty paragraphs wrapped around specific shortcodes
		$shortcodes = array( 'gallery' );

		foreach ( $shortcodes as $shortcode ) {

			$array = array(
				'<p>['. $shortcode		=> '['.$shortcode,
				'<p>[/'. $shortcode		=> '[/'.$shortcode,
				$shortcode .']</p>'		=> $shortcode .']',
				$shortcode .']<br />'	=> $shortcode .']'
			);

			$content = strtr( $content, $array );

		}

		return $content;
	}

	public function do_amp_multipage_content( $content ) {

		global $page;
		$page	= $page ? $page : 1;

		if ( false !== strpos( $content, '<!--nextpage-->' ) ) {
			$content	= str_replace( "\n<!--nextpage-->\n", '<!--nextpage-->', $content );
			$content	= str_replace( "\n<!--nextpage-->", '<!--nextpage-->', $content );
			$content	= str_replace( "<!--nextpage-->\n", '<!--nextpage-->', $content );

			// Ignore nextpage at the beginning of the content.
			if ( 0 === strpos( $content, '<!--nextpage-->' ) ) {
				$content	= substr( $content, 15 );
			}

			$pages		= explode( '<!--nextpage-->', $content );
			$content	= $pages[ $page - 1 ];
			add_filter( 'wp_link_pages_link', array( $this, '_amp_link_pages' ) );

		}

		return $content;

	}

	/**
	 * Do AMP Post Template CSS
	 *
	 *	Adds our AMP-specific styles to the HEAD of our AMP template.
	 *
	 * @param  [type] $amp_template
	 *
	 * @return string Contents of the 'amp.min.css' file
	 */
	public function do_amp_post_template_css( $amp_template = null ) {

		$source = get_template_directory() .'/css/amp.min.css';

		if ( file_exists($source) ) {
			$contents	= file_get_contents( $source );

			// strip the source map, causes bugs
			$contents	= preg_replace( '/\/\*(.)+\ *\//', '', $contents );

			echo $contents;
		}

	}

	/**
	 * Add scripts to the header of the AMP post
	 */
	public function add_loose_scripts() {
		global $post;

		// Have to add the YouYube and Brightcove scripts as soon as possible (legacy code)
		$post_meta	= get_post_meta( $post->ID );

		$yt_vid			= !empty( $post_meta['_youtube_id'] ) ? $post_meta['_youtube_id'] : '';
		$brightcove_id	= !empty( $post_meta['video_id'] ) ? $post_meta['video_id'] : '';

		if ( $yt_vid != '' ) {
			echo '<script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>'."\n";
		}

		if ( $brightcove_id != '' ) {
			echo '<script async custom-element="amp-brightcove" src="https://cdn.ampproject.org/v0/amp-brightcove-0.1.js"></script>'."\n";
		}

		// add the social sharing script
		echo '<script async custom-element="amp-social-share" src="https://cdn.ampproject.org/v0/amp-social-share-0.1.js"></script>'."\n";

	}

	/**
	 * Add page number to amphtml rel if multipage
	 *
	 * @param  string	$amp_url	Contains the original AMP URL
	 * @param  integer	$post_ID	Post ID Number
	 *
	 * @return string				Contains original or altered AMP URL
	 */
	public function do_multipage_amphtml_rel( $amp_url, $post_ID ) {

		global $multipage;
		global $page;

		if ( $multipage && $page !== 1 ) {
			$amp_url	= preg_replace( '/amp$/', $page. '/amp', $amp_url );
		}

		return $amp_url;

	}

	/**
	 * Add page number to canonical URL if multipage
	 * @param array		$data Array with AMP data
	 * @param object	$post     WP Post Object
	 *
	 * @return array	Original array or array with link changed
	 */
	public function canonical_url_page_number( $data, $post ) {

		global $page;

		if ( !empty( $page ) && $page > 1  ) {
			$data['canonical_url']	= $data['canonical_url'] .'/'. $page;
		}

		return $data;

	}

	/**
	 * Handle the external fonts
	 * @param array		$data Array with AMP data
	 *
	 * @return array	(Un)altered array with AMP data
	 */
	public function handle_fonts( $data ) {

		// AMP plugin adds merriweather font and we don't need it. unset it
		unset( $data['font_urls']['merriweather'] );

		$data['font_urls']['googlefonts']	= 'https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700|Open+Sans:300,400,600,400italic|Crimson+Text';
		$data['font_urls']['fontawesome']	= 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css';

		return $data;

	}

	/**
	 * Add page number to schema URL if multipage
	 * @param array		$metadata Array with AMP metadata
	 * @param object	$post     WP Post Object
	 *
	 * @return array	Original array or array with link changed
	 */
	public function schema_multipage_number( $metadata, $post ) {

		global $page;

		if ( !empty( $page ) && $page > 1  ) {
			$metadata['mainEntityOfPage']	= $metadata['mainEntityOfPage'] .'/'. $page;
		}

		return $metadata;

	}

	/**
	 * Add default image for posts without featured image nor any inline images
	 * -galleries do not apply since it is a shortcode
	 * @param array		$metadata Array with AMP metadata
	 * @param object	$post     WP Post Object
	 *
	 * @return array	Original array or array with default image array
	 */
	public function add_default_image( $metadata, $post ) {

		// if there is no featured image, use default in theme settings
		if ( empty($metadata['image']) ) {

			if ( function_exists('cgi_bikes_get_option') && cgi_bikes_get_option( 'site_logo' ) != '' ) {
				$sitelogoid		= cgi_bikes_get_option( 'site_logo_id' );
				$thumb_array	= wp_get_attachment_image_src( $sitelogoid, 'large' );
				$url			= $thumb_array[0];
				$width			= $thumb_array[1];
				$height			= $thumb_array[2];

			} else {
				$sitelogo	= get_bloginfo( 'stylesheet_directory' ) . '/images/velonews-logo.svg';
				$xml		= simplexml_load_file( $sitelogo );
				$attr		= $xml->attributes();
				$url		= $sitelogo;
				$width		= floatval($attr->width);
				$height		= floatval($attr->height);
			}

			$metadata['image']	= array(
				'@type'		=> 'ImageObject',
				'url'		=> $url,
				'width'		=> $width,
				'height'	=> $height
			);

		}

		return $metadata;
	}

	/**
	 * Do AMP Post Template File
	 *
	 * Defines which template to use for displaying AMP pages.
	 *
	 * @param  string $file
	 * @param  string $template_type
	 * @param  WP_POST $post
	 *
	 * @return string Path of template file to use when displaying AMP pages.
	 */
	public function do_amp_post_template_file( $file = null, $template_type = null, $post = null ) {

		if ( 'single' === $template_type ) {
			$file = dirname( dirname( __FILE__ ) ) . '/single-amp.php';
		}

		return $file;
	}

	/**
	 * Adding analytics code to AMP
	 *
	 * @param	array	$analytics List of analytics to attach to AMP posts.
	 *
	 * @return	array	Array with analytics to embed as an associative array
	 */
	public function add_custom_analytics( $analytics ) {

		if ( !is_array( $analytics ) ) {
			$analytics = array();
		}

		$analytics_id	= cgi_bikes_get_option( 'google_analytics_ID' );

		if ( $analytics_id != '' ) {

			$analytics['googleanalytics'] = array(
				'type'			=> 'googleanalytics',
				'attributes'	=> array(
				),
				'config_data'	=> array(
					'vars'			=> array(
						'account'		=> "$analytics_id"
					),
					'triggers'		=> array(
						'trackPageview' => array(
							'on'		=> 'visible',
							'request'	=> 'pageview',
						),
					),
				),
			);

		}

		return $analytics;

	}

	/**
	 * Amp Content Embed Filter
	 *
	 * Removes the social media embed handlers provided by the "AMP" plugin so that
	 * this plugin can use it's own embed logic.
	 *
	 * @param array $handlers List of embed handlers to be loaded by the "AMP" plugin.
	 */
	public function amp_content_embed_handlers( $handlers = array() ) {

		// remove the facebook shortcode handler
		unset( $handlers['AMP_Facebook_Embed_Handler'] );

		// remove the gallery shortcode handler
		unset( $handlers['AMP_Gallery_Embed_Handler'] );

		// remove the instagram shortcode handler
		unset( $handlers['AMP_Instagram_Embed_Handler'] );

		// remove the twitter shortcode handler
		unset( $handlers['AMP_Twitter_Embed_Handler'] );

		// remove the yotuube shortcode handler
		unset( $handlers['AMP_YouTube_Embed_Handler'] );

		return $handlers;
	}

	public function embeds( $embed_handler_classes, $post ) {

		// require class file from plugin
		require_once( AMP__DIR__ . '/includes/embeds/class-amp-base-embed-handler.php' );

		// require all our individiual class files (trying to keep this file short and the classes easier to find)
		require_once( 'amp-embeds/velo-amp-youtube.php' );
		require_once( 'amp-embeds/velo-amp-vimeo.php' );
		require_once( 'amp-embeds/velo-amp-brightcove.php' );
		require_once( 'amp-embeds/velo-amp-facebook.php' );
		require_once( 'amp-embeds/velo-amp-instagram.php' );
		require_once( 'amp-embeds/velo-amp-twitter.php' );
		require_once( 'amp-embeds/velo-amp-caption.php' );
		require_once( 'amp-embeds/velo-amp-gallery.php' );

		$embed_handler_classes['Velo_AMP_Youtube']		= array();
		$embed_handler_classes['Velo_AMP_Vimeo']		= array();
		$embed_handler_classes['Velo_AMP_Brightcove']	= array();
		$embed_handler_classes['Velo_AMP_Facebook']		= array();
		$embed_handler_classes['Velo_AMP_Instagram']	= array();
		$embed_handler_classes['Velo_AMP_Twitter']		= array();
		$embed_handler_classes['Velo_AMP_Caption']		= array();
		$embed_handler_classes['Velo_AMP_Gallery']		= array();

		return $embed_handler_classes;

	}

	public function sanitizers( $sanitizer_classes, $post ) {

		// Since we are in an AMP filter,
		// lets remove our WP DFP ADs inarticle ad
		add_filter( 'wp_dfp_ads_inarticle', '__return_false' );

		// require class files from plugin taht sanitizers will require
		require_once( AMP__DIR__ .'/includes/sanitizers/class-amp-base-sanitizer.php' );
		require_once( AMP__DIR__ .'/includes/utils/class-amp-dom-utils.php' );
		require_once( AMP__DIR__ .'/includes/utils/class-amp-html-utils.php' );
		require_once( AMP__DIR__ .'/includes/class-amp-content.php' );

		require_once( 'amp-sanitizers/velo-amp-ad-inject.php' );
		require_once( 'amp-sanitizers/velo-amp-content-sanitizer.php' );

		$sanitizer_classes['VELO_AMP_Ad_Injection_Sanitizer']	= array();
		$sanitizer_classes['VELO_AMP_Content_Sanitizer']		= array();

		return $sanitizer_classes;

	}

	/**
	 * Amp inarticle ad
	 *
	 */
	public function amp_inarticle( $ad_text, $inarticle_ad ){

		$is_amp = ( function_exists( 'is_amp_endpoint' ) && is_amp_endpoint() ? true : false );

		if ( class_exists('Wp_Dfp_Ads') && true === $is_amp ) {

			$adKw =  Wp_Dfp_Ads::singleton()->generate_dfp_keywords();

			// wrap the ad in the correct markup
			$ad_text = '<div class="advert advert_xs_300x250 advert_location_inline">
				<div class="advert__wrap">
					<amp-ad width="300" height="250" type="doubleclick" data-slot="'.$adKw.'"></amp-ad>
				</div>
			</div>';

		}

		return $ad_text;

	}



	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( !self::$instance )
			self::$instance = new self();

		return self::$instance;
	}

	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	private function _add_actions() {
		// Remove plugin action, we're treating it ourselves
		remove_action( 'wp', 'amp_maybe_add_actions' );

		// custom parse request instead of WP's to accommodate for multipage posts
		add_action( 'do_parse_request', array( $this, 'parse_request' ), 10, 3 );

		add_action( 'wp', array( $this, 'ampify_post' ) );

		add_action( 'pre_amp_render_post', array( $this, 'do_amp_content_filter' ) );
		add_action( 'amp_post_template_css', array($this, 'do_amp_post_template_css') );
		add_action( 'amp_post_template_head', array( $this, 'add_loose_scripts' ) );

		add_filter( 'amp_get_permalink', array( $this, 'do_multipage_amphtml_rel' ), 10, 2 );
		add_filter( 'amp_post_template_data', array( $this, 'canonical_url_page_number' ), 10, 2 );
		add_filter( 'amp_post_template_data', array( $this, 'handle_fonts' ) );
		add_filter( 'amp_post_template_metadata', array( $this, 'schema_multipage_number' ), 10, 2 );
		add_filter( 'amp_post_template_metadata', array( $this, 'add_default_image' ), 10, 2 );

		add_filter( 'amp_post_template_file', array($this, 'do_amp_post_template_file'), 10, 3 );
		add_filter( 'amp_post_template_analytics', array( $this, 'add_custom_analytics' ) );

		add_filter( 'amp_content_embed_handlers', array( $this, 'amp_content_embed_handlers' ) );
		if ( defined( 'AMP__DIR__' ) && file_exists( AMP__DIR__ . '/includes/embeds/class-amp-base-embed-handler.php' ) ) {

			add_filter( 'amp_content_embed_handlers', array( $this, 'embeds' ), 10, 2 );
			add_filter( 'amp_content_sanitizers', array( $this, 'sanitizers' ), 10, 2 );

		}

	}

	/**
	 * Public function to generate the amp-social-share markup
	 *
	 * @return	string	Full markup of the social icons
	 */
	public static function get_social_sharing() {

		$facebook_app_ID = cgi_bikes_get_option( 'facebook_app_ID' );

		ob_start(); ?>
		<nav class="article__social-sharing" >
			<amp-social-share type="facebook" data-param-app_id="<?php echo $facebook_app_ID; ?>" width="83" height="32">Share</amp-social-share>
			<amp-social-share type="twitter" width="83" height="32">Tweet</amp-social-share>
			<amp-social-share type="email" width="80" height="32">Email</amp-social-share>
		</nav>
		<?php
		return ob_get_clean();

	}

	/**
	 * Remaking the WP parse_request call so it applies AMP endpoint since the beginning
	 */
	protected function _parse_request( $wp, $extra_query_vars ) {
		global $wp_rewrite;

		$wp->query_vars       = array();
		$post_type_query_vars = array();

		$amp_endpoint = AMP_QUERY_VAR;
		$wp->query_vars[ $amp_endpoint ] = '';

		if ( is_array( $extra_query_vars ) ) {
			$wp->extra_query_vars = &$extra_query_vars;
		} elseif ( ! empty( $extra_query_vars ) ) {
			parse_str( $extra_query_vars, $wp->extra_query_vars );
		}
		// Process PATH_INFO, REQUEST_URI, and 404 for permalinks.

		// Fetch the rewrite rules.
		$rewrite = $wp_rewrite->wp_rewrite_rules();

		if ( ! empty( $rewrite ) ) {
			// If we match a rewrite rule, this will be cleared.
			$error             = '404';
			$wp->did_permalink = true;

			$pathinfo = isset( $_SERVER['PATH_INFO'] ) ? $_SERVER['PATH_INFO'] : '';
			list( $pathinfo ) = explode( '?', $pathinfo );
			$pathinfo = str_replace( "%", "%25", $pathinfo );

			list( $req_uri ) = explode( '?', $_SERVER['REQUEST_URI'] );
			$self            = $_SERVER['PHP_SELF'];
			$home_path       = trim( parse_url( home_url(), PHP_URL_PATH ), '/' );
			$home_path_regex = sprintf( '|^%s|i', preg_quote( $home_path, '|' ) );

			// Trim path info from the end and the leading home path from the
			// front. For path info requests, this leaves us with the requesting
			// filename, if any. For 404 requests, this leaves us with the
			// requested permalink.
			$req_uri  = str_replace( $pathinfo, '', $req_uri );
			$req_uri  = trim( $req_uri, '/' );
			$req_uri  = preg_replace( $home_path_regex, '', $req_uri );
			$req_uri  = trim( $req_uri, '/' );
			$pathinfo = trim( $pathinfo, '/' );
			$pathinfo = preg_replace( $home_path_regex, '', $pathinfo );
			$pathinfo = trim( $pathinfo, '/' );
			$self     = trim( $self, '/' );
			$self     = preg_replace( $home_path_regex, '', $self );
			$self     = trim( $self, '/' );

			// The requested permalink is in $pathinfo for path info requests and
			//  $req_uri for other requests.
			if ( ! empty( $pathinfo ) && ! preg_match( '|^.*' . $wp_rewrite->index . '$|', $pathinfo ) ) {
				$requested_path = $pathinfo;
			} else {
				// If the request uri is the index, blank it out so that we don't try to match it against a rule.
				if ( $req_uri == $wp_rewrite->index ) {
					$req_uri = '';
				}
				$requested_path = $req_uri;
			}
			$requested_file = $req_uri;

			$wp->request = $requested_path;

			// Look for matches.
			$endpoint      = sprintf( '/\/%s(\/)?$/', $amp_endpoint );
			$request_match = ( $requested_path == $amp_endpoint )
				? $requested_path : preg_replace( $endpoint, '', $requested_path );

			if ( empty( $request_match ) ) {
				// An empty request could only match against ^$ regex
				if ( isset( $rewrite['$'] ) ) {
					$wp->matched_rule = '$';
					$query            = $rewrite['$'];
					$matches          = array( '' );
				}
			} else {
				foreach ( (array) $rewrite as $match => $query ) {
					// If the requested file is the anchor of the match, prepend it to the path info.
					if ( ! empty( $requested_file ) && strpos( $match, $requested_file ) === 0 && $requested_file != $requested_path ) {
						$request_match = $requested_file . '/' . $requested_path;
					}

					if ( preg_match( "#^$match#", $request_match, $matches ) ||
					     preg_match( "#^$match#", urldecode( $request_match ), $matches )
					) {

						if ( $wp_rewrite->use_verbose_page_rules && preg_match( '/pagename=\$matches\[([0-9]+)\]/', $query, $varmatch ) ) {
							// This is a verbose page match, let's check to be sure about it.
							$page = get_page_by_path( $matches[ $varmatch[1] ] );
							if ( ! $page ) {
								continue;
							}

							$post_status_obj = get_post_status_object( $page->post_status );
							if ( ! $post_status_obj->public && ! $post_status_obj->protected
							     && ! $post_status_obj->private && $post_status_obj->exclude_from_search
							) {
								continue;
							}
						}

						// Got a match.
						$wp->matched_rule = $match;
						break;
					}
				}
			}

			if ( isset( $wp->matched_rule ) ) {
				// Trim the query of everything up to the '?'.
				$query = preg_replace( "!^.+\?!", '', $query );

				// Substitute the substring matches into the query.
				$query = addslashes( WP_MatchesMapRegex::apply( $query, $matches ) );

				$wp->matched_query = $query;

				// Parse the query.
				parse_str( $query, $perma_query_vars );

				// If we're processing a 404 request, clear the error var since we found something.
				if ( '404' == $error ) {
					unset( $error, $_GET['error'] );
				}
			}

			// If req_uri is empty or if it is a request for ourself, unset error.
			if ( empty( $requested_path ) || $requested_file == $self || strpos( $_SERVER['PHP_SELF'], 'wp-admin/' ) !== false ) {
				unset( $error, $_GET['error'] );

				if ( isset( $perma_query_vars ) && strpos( $_SERVER['PHP_SELF'], 'wp-admin/' ) !== false ) {
					unset( $perma_query_vars );
				}

				$wp->did_permalink = false;
			}
		}

		/**
		 * Filters the query variables whitelist before processing.
		 *
		 * Allows (publicly allowed) query vars to be added, removed, or changed prior
		 * to executing the query. Needed to allow custom rewrite rules using your own arguments
		 * to work, or any other custom query variables you want to be publicly available.
		 *
		 * @since 1.5.0
		 *
		 * @param array $public_query_vars The array of whitelisted query variables.
		 */
		$wp->public_query_vars = apply_filters( 'query_vars', $wp->public_query_vars );

		foreach ( get_post_types( array(), 'objects' ) as $post_type => $t ) {
			if ( is_post_type_viewable( $t ) && $t->query_var ) {
				$post_type_query_vars[ $t->query_var ] = $post_type;
			}
		}

		foreach ( $wp->public_query_vars as $wpvar ) {
			if ( isset( $wp->extra_query_vars[ $wpvar ] ) ) {
				$wp->query_vars[ $wpvar ] = $wp->extra_query_vars[ $wpvar ];
			} elseif ( isset( $_POST[ $wpvar ] ) ) {
				$wp->query_vars[ $wpvar ] = $_POST[ $wpvar ];
			} elseif ( isset( $_GET[ $wpvar ] ) ) {
				$wp->query_vars[ $wpvar ] = $_GET[ $wpvar ];
			} elseif ( isset( $perma_query_vars[ $wpvar ] ) ) {
				$wp->query_vars[ $wpvar ] = $perma_query_vars[ $wpvar ];
			}

			if ( ! empty( $wp->query_vars[ $wpvar ] ) ) {
				if ( ! is_array( $wp->query_vars[ $wpvar ] ) ) {
					$wp->query_vars[ $wpvar ] = (string) $wp->query_vars[ $wpvar ];
				} else {
					foreach ( $wp->query_vars[ $wpvar ] as $vkey => $v ) {
						if ( ! is_object( $v ) ) {
							$wp->query_vars[ $wpvar ][ $vkey ] = (string) $v;
						}
					}
				}

				if ( isset( $post_type_query_vars[ $wpvar ] ) ) {
					$wp->query_vars['post_type'] = $post_type_query_vars[ $wpvar ];
					$wp->query_vars['name']      = $wp->query_vars[ $wpvar ];
				}
			}
		}

		// Convert urldecoded spaces back into +
		foreach ( get_taxonomies( array(), 'objects' ) as $taxonomy => $t ) {
			if ( $t->query_var && isset( $wp->query_vars[ $t->query_var ] ) ) {
				$wp->query_vars[ $t->query_var ] = str_replace( ' ', '+', $wp->query_vars[ $t->query_var ] );
			}
		}

		// Don't allow non-publicly queryable taxonomies to be queried from the front end.
		if ( ! is_admin() ) {
			foreach ( get_taxonomies( array( 'publicly_queryable' => false ), 'objects' ) as $taxonomy => $t ) {
				/*
				 * Disallow when set to the 'taxonomy' query var.
				 * Non-publicly queryable taxonomies cannot register custom query vars. See register_taxonomy().
				 */
				if ( isset( $wp->query_vars['taxonomy'] ) && $taxonomy === $wp->query_vars['taxonomy'] ) {
					unset( $wp->query_vars['taxonomy'], $wp->query_vars['term'] );
				}
			}
		}

		// Limit publicly queried post_types to those that are publicly_queryable
		if ( isset( $wp->query_vars['post_type'] ) ) {
			$queryable_post_types = get_post_types( array( 'publicly_queryable' => true ) );
			if ( ! is_array( $wp->query_vars['post_type'] ) ) {
				if ( ! in_array( $wp->query_vars['post_type'], $queryable_post_types ) ) {
					unset( $wp->query_vars['post_type'] );
				}
			} else {
				$wp->query_vars['post_type'] = array_intersect( $wp->query_vars['post_type'], $queryable_post_types );
			}
		}

		// Resolve conflicts between posts with numeric slugs and date archive queries.
		$wp->query_vars = wp_resolve_numeric_slug_conflicts( $wp->query_vars );

		foreach ( (array) $wp->private_query_vars as $var ) {
			if ( isset( $wp->extra_query_vars[ $var ] ) ) {
				$wp->query_vars[ $var ] = $wp->extra_query_vars[ $var ];
			}
		}

		if ( isset( $error ) ) {
			$wp->query_vars['error'] = $error;
		}

		/**
		 * Filters the array of parsed query variables.
		 *
		 * @since 2.1.0
		 *
		 * @param array $query_vars The array of requested query variables.
		 */
		$wp->query_vars = apply_filters( 'request', $wp->query_vars );

		/**
		 * Fires once all query variables for the current request have been parsed.
		 *
		 * @since 2.1.0
		 *
		 * @param WP &$wp Current WordPress environment instance (passed by reference).
		 */
		do_action_ref_array( 'parse_request', array( &$wp ) );
	}

	public function _amp_link_pages( $link ) {

		// The Regular Expression filter
		$reg_exUrl = "#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#";
		// Check if there is a url
		if ( preg_match( $reg_exUrl, $link, $url ) ) {
			$amp_link = str_replace( $url[0], $this->_get_amphtml_link( $url[0] ), $link );

			return $amp_link;
		}

		return $link;

	}

	public function _get_amphtml_link( $link, $id = '' ) {
		$id		= $id ? $id : url_to_postid( $link );

		$link	= trailingslashit( $link );

		return user_trailingslashit( $link . AMP_QUERY_VAR );
	}

}


