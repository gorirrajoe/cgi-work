<?php

use Facebook\InstantArticles\Client\Client;
use Facebook\Facebook;


class VeloNews_Facebook_Instant_Articles {

	static $instance = false;

	public function __construct() {
		$this->_add_actions();
	}

	/**
	 * Do Pre Get Posts
	 *
	 * Refines the Instant Articles Feed query to just what we decide it should
	 * contain.
	 *
	 * @param WP_Query $query
	 */
	public function do_pre_get_posts( $query = null ) {

		/**
		 * Note: using this logic instead of "is_feed()" because "the-prologue"
		 * (aka newsletter) breaks because no get_header() or get_footer() calls
		 * are made (maybe).
		 */
		if ( defined( 'INSTANT_ARTICLES_SLUG' ) && isset($query->query_vars['feed']) && $query->query_vars['feed'] === INSTANT_ARTICLES_SLUG ) {
			$query->set('post_type', array( 'post' ) );
		}
	}

	/**
	 * Handles removing the filter/shortcodes for all media
	 *
	 * @param  string $content The original content
	 */
	public function fbia_media_shortcodes( $content ) {

		remove_filter( 'the_content', 'wpautop' );

		remove_shortcode( 'youtube' );
		remove_shortcode( 'vimeo' );
		remove_shortcode( 'brightcove' );
		remove_shortcode( 'gallery' );

		add_shortcode( 'youtube', array( $this, 'youtube_embed' ) );
		add_shortcode( 'vimeo', array( $this, 'vimeo_embed' ) );
		add_shortcode( 'brightcove', array( $this, 'brightcove_embed' ) );
		add_shortcode( 'gallery', array( $this, 'gallery_embed' ) );

	}

	public function youtube_embed( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'id' => ''
			),
			$atts
		);

		if ( empty( $a['id'] ) ) {
			return;
		}

		// no white space in the markup because the FBIA plugin adds wpautop as the last step of converting the content
		return sprintf(
			'<figure class="op-interactive"><iframe width="1280" height="720" src="https://www.youtube.com/embed/%s?rel=0" frameborder="0" allowfullscreen></iframe></figure>',
			$a['id']
		);

	}

	public function vimeo_embed( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'id' => ''
			),
			$atts
		);

		if ( empty( $a['id'] ) ) {
			return;
		}

		// no white space in the markup because the FBIA plugin adds wpautop as the last step of converting the content
		return sprintf(
			'<figure class="op-interactive"><iframe src="https://player.vimeo.com/video/%s" width="1280" height="720" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></figure>',
			$a['id']
		);

	}

	public function brightcove_embed( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'id' => ''
			),
			$atts
		);

		if ( empty( $a['id'] ) ) {
			return;
		}

		// no white space in the markup because the FBIA plugin adds wpautop as the last step of converting the content
		return sprintf(
			'<figure class="op-interactive"><iframe src="//players.brightcove.net/3655502813001/default_default/index.html?videoId=%1$d" width="1280" height="720" allowfullscreen></iframe></figure>',
			$a['id']
		);

	}

	public function gallery_embed( $attr = array() ) {

		global $post;

		// check $post exists
		if ( empty( $post ) ) {
			return;
		}

		static $index = 0;
		$index++;

		if ( ! empty( $attr['ids'] ) ) {

			// 'ids' is explicitly ordered, unless you specify otherwise.
			if ( empty( $attr['orderby'] ) ) {
				$attr['orderby'] = 'post__in';
			}

			$attr['include'] = $attr['ids'];

		}

		$content	= get_the_content();

		$output = apply_filters( 'post_gallery', '', $attr, $content );

		if ( ! empty( $output ) ) {
			return $output;
		}

		$size = 'image-gallery';

		$html5 = current_theme_supports( 'html5', 'gallery' );

		$atts = shortcode_atts(
			array(
				'order'			=> 'ASC',
				'orderby'		=> 'menu_order ID',
				'id'			=> ( $post ? $post->ID : 0 ),
				'itemtag'		=> ( $html5 ? 'figure'     : 'dl' ),
				'icontag'		=> ( $html5 ? 'div'        : 'dt' ),
				'captiontag'	=> ( $html5 ? 'figcaption' : 'dd' ),
				'columns'		=> 1,
				//'size'		=> $size,
				'include'		=> '',
				'exclude'		=> '',
				'link'			=> '',
				'autoplay'		=> ''
			),
			$attr,
			'gallery'
		);

		$id = intval( $atts['id'] );

		if ( ! empty( $atts['include'] ) ) {

			$attachments = array();

			$params = array(
				'post__in'			=> explode( ',', $atts['include'] ),
				'post_status'		=> 'inherit',
				'post_type'			=> 'attachment',
				'post_mime_type'	=> 'image',
				'order'				=> $atts['order'],
				'orderby'			=> $atts['orderby'],
				'posts_per_page'	=> -1
			);

			$_attachments = new WP_Query( $params );

			if ( $_attachments->have_posts() ) : while ( $_attachments->have_posts() ) : $_attachments->the_post();

				$attachments[$post->ID] = $post;

			endwhile; endif;

			wp_reset_postdata();

		} elseif ( ! empty( $atts['exclude'] ) ) {

			$params =
				array(
					'post_parent'		=> $id,
					'exclude'			=> $atts['exclude'],
					'post_status'		=> 'inherit',
					'post_type'			=> 'attachment',
					'post_mime_type'	=> 'image',
					'order'				=> $atts['order'],
					'orderby'			=> $atts['orderby']
				);

			$attachments = get_children( $params );

		} else {

			$params =
				array(
					'post_parent'		=> $id,
					'post_status'		=> 'inherit',
					'post_type'			=> 'attachment',
					'post_mime_type'	=> 'image',
					'order'				=> $atts['order'],
					'orderby'			=> $atts['orderby']
				);

			$attachments = get_children( $params );

		}

		if ( empty( $attachments ) ) {
			return '';
		}

		$valid_tags = wp_kses_allowed_html( 'post' );

		$item_tag = tag_escape( $atts['itemtag'] );
		$caption_tag = tag_escape( $atts['captiontag'] );
		$icon_tag = tag_escape( $atts['icontag'] );

		if ( ! isset( $valid_tags[ $item_tag ] ) ) {
			$item_tag = 'dl';
		}

		if ( ! isset( $valid_tags[ $caption_tag ] ) ) {
			$caption_tag = 'dd';
		}

		if ( ! isset( $valid_tags[ $icon_tag ] ) ) {
			$icon_tag = 'dt';
		}

		$columns = intval( $atts['columns'] );
		$item_width = ( $columns > 0 ? floor(100/$columns) : 100 );
		$float = is_rtl() ? 'right' : 'left';

		$selector = "gallery-{$index}";

		$gallery_style = '';

		$size_class = sanitize_html_class( $size );

		$output = '<div class="gallery">'.

			$output .= '<figure class="op-slideshow">';

				foreach ( $attachments as $id => $attachment ) {

					$excerpt = trim( $attachment->post_excerpt );

					$attr = ( $excerpt ) ? array( 'aria-describedby' => "$selector-$id" ) : '';
					$image = wp_get_attachment_link( $id, $size, false, false, false, $attr );

					$excerpt = wptexturize( $excerpt );

					if ( $caption_tag && ! empty( $excerpt ) ) {
						$image .= "<figcaption>{$excerpt}</figcaption>";
					}

					$output .= "<figure>{$image}</figure>";

				}

			$output .= '</figure>';

		$output .= '</div>';

		return $output;

	}

	/**
	 * Modify the authors in the FBIA to show the guest author if filled in metadata
	 * @param  array	$authors	contains the article's author and it's info. (ID, display_name, etc)
	 * @param  integer	$post_id	the article's post ID number
	 * @return object				object containing the author's info
	 */
	public function mod_authors( $authors, $post_id ) {

		$guest_author = get_post_meta( $post_id, 'guest_author', true );

		if ( $guest_author != '' ) {

			unset( $authors );

			$author_id			= get_post_field( 'post_author', $post_id );
			$custom_author_url	= '';
			$custom_author_desc	= '';

			$author = new stdClass;
			$author->ID				= $author_id;
			$author->display_name	= $guest_author;
			$author->user_url		= $custom_author_url;
			$author->bio			= $custom_author_desc;

			$authors[]	= $author;

		}

		return $authors;
	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if( ! self::$instance )
			self::$instance = new self();

		return self::$instance;
	}

	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	private function _add_actions() {
		add_action( 'pre_get_posts', array( $this, 'do_pre_get_posts' ) );
		add_action( 'instant_articles_before_transform_post' , array( $this, 'fbia_media_shortcodes' ), 5, 2 );

		add_filter( 'instant_articles_authors', array( $this, 'mod_authors' ), 10, 2 );
	}
}


if( class_exists( 'Instant_Articles_Publisher' ) ) {

	/**
	 * VeloNews Instant Articles Publisher
	 *
	 * This class overrides the original "Instant_Articles_Publisher" class because
	 * the original 'submit_article' method doesn't push updates to the facebook API
	 * and we want to temporarily disallow custom post types from being submitted.
	 *
	 * This class can be removed if a future version of the "fb-instant-articles"
	 * plugin adds additonal functionality to handles the use-cases mentioned above.
	 */
	class VeloNews_Instant_Articles_Publisher extends Instant_Articles_Publisher {

		public static function init() {
			remove_action( 'save_post', array( 'Instant_Articles_Publisher', 'submit_article' ) );
			add_action( 'save_post', array( 'VeloNews_Instant_Articles_Publisher', 'submit_article' ), 10, 2 );
		}

		public static function submit_article( $post_id, $post ) {

			// Don't process if this is just an autosave.
			if ( wp_is_post_autosave( $post ) ) {
				return;
			}

			// Don't process if this post is not published
			if ( 'publish' !== $post->post_status ) {
				return;
			}

			// Don't process if this post is not a "wordpress post"
			if ( 'post' !== $post->post_type ) {
				return;
			}

			// Transform the post to an Instant Article.
			$adapter = new Instant_Articles_Post( $post );

			$article = $adapter->to_instant_article();

			// Skip empty articles or articles missing title.
			// This is important because the save_post action is also triggered by bulk updates, but in this case
			// WordPress does not load the content field from DB for performance reasons. In this case, articles
			// will be empty here, despite of them actually having content.
			if ( count($article->getChildren()) === 0 || ! $article->getHeader() || ! $article->getHeader()->getTitle() ) {
				return;
			}

			// Instantiate an API client.
			try {
				$fb_app_settings = Instant_Articles_Option_FB_App::get_option_decoded();
				$fb_page_settings = Instant_Articles_Option_FB_Page::get_option_decoded();
				$publishing_settings = Instant_Articles_Option_Publishing::get_option_decoded();

				$dev_mode = isset( $publishing_settings['dev_mode'] )
					? ( $publishing_settings['dev_mode'] ? true : false )
					: false;

				if ( isset( $fb_app_settings['app_id'] )
					&& isset( $fb_app_settings['app_secret'] )
					&& isset( $fb_page_settings['page_access_token'] )
					&& isset( $fb_page_settings['page_id'] ) ) {

					$client = Client::create(
						$fb_app_settings['app_id'],
						$fb_app_settings['app_secret'],
						$fb_page_settings['page_access_token'],
						$fb_page_settings['page_id'],
						$dev_mode
					);

					if ( $dev_mode ) {
						$take_live = false;
					} else {
						// Any publish status other than 'publish' means draft for the Instant Article.
						$take_live = true;
					}

					try {
						// Import the article.
						$client->importArticle( $article, $take_live );
					} catch ( Exception $e ) {
						// Try without taking live for pages not yet reviewed.
						$client->importArticle( $article, false );
					}
				}
			} catch ( Exception $e ) {

				Logger::getLogger( 'instantarticles-wp-plugin' )->error(
					'Unable to submit article.',
					$e->getTraceAsString()
				);
			}
		}
	}

	VeloNews_Instant_Articles_Publisher::init();
}
