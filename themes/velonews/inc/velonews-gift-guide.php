<?php
/**
 * CMB2 Theme Options
 * @version 0.1.0
 */
class GiftGuide_Admin {

	/**
 	 * Option key, and option page slug
 	 * @var string
 	 */
	private $key = 'giftguide_options';

	/**
 	 * Options page metabox id
 	 * @var string
 	 */
	private $metabox_id = 'giftguide_option_metabox';

	/**
	 * Options Page title
	 * @var string
	 */
	protected $title = '';

	/**
	 * Options Page hook
	 * @var string
	 */
	protected $options_page = '';

	protected $suboptions_pages = array();

	/**
	 * Holds an instance of the object
	 *
	 * @var GiftGuide_Admin
	 **/
	private static $instance = null;


	/**
	 * Holds array with the slugs
	 *
	 * @var Array
	 **/
	protected $category_slugs = '';

	/**
	 * Holds the page/subpage currently on
	 *
	 * @var string
	 **/
	protected $page = '';


	/**
	 * Constructor
	 * @since 0.1.0
	 */
	private function __construct() {
		// Set our title
		$this->title = __( 'Gift Guide Options', 'giftguide' );

		$this->generate_options_array();
		$this->_add_actions();
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if(! self::$instance)
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Options Array Generator
	 *
	 * Adds years to Gift Guide.
	 */
	public function generate_options_array() {

		//get the current year
		$current_year = date("Y");
		//set up non empty array with the first year
		global $gg_years;
		$gg_years = array(  );

		//add to the array for all years including this one <=
		for ( $i = $current_year; $i > 2014 ; $i--) {

			$gg_years[] .= $i;

		}
		$options_array	= array();
		$the_slugs		= array();
		$key			= $this->key;

		foreach( $gg_years as $year ){
			$options_array[$key .'-'. $year]	= array(
				'title'	=> $year .' Gift Guide',
				'slug'	=> $key .'-'. $year,
				'year'	=> "$year"
			);
			$the_slugs[]	= $year . '-gift-guide';
		}

		 // 2015 gift guide didn't have a grid layout -- don't add
		// $the_slugs[]	= 'velonews-gift-guide';

		$this->option_groups	= $options_array;
		$this->category_slugs	= $the_slugs;

	}


	/**
	 * Register our setting to WP
	 * @since  0.1.0
	 */
	public function init() {
		register_setting( $this->key, $this->key );
	}

	/**
	 * Add menu options page
	 * @since 0.1.0
	 */
	public function add_options_page() {
		$this->options_page	= add_menu_page( $this->title, $this->title, 'manage_options', $this->key, array( $this, 'admin_page_display' ) );

		foreach( $this->option_groups as $option_group ) {
			$this->suboptions_pages[]	= add_submenu_page( $this->key, $option_group['title'], $option_group['year'] ,'edit_posts', $option_group['slug'], array( $this, 'admin_page_display' ) );
		}

		// remove top level submenu which holds no data (no year)
		remove_submenu_page( $this->key, $this->key );

		// Include CMB CSS in the head to avoid FOUC
		add_action( "admin_print_styles-{$this->options_page}", array( 'CMB2_hookup', 'enqueue_cmb_css' ) );

		foreach( $this->suboptions_pages as $suboptions_page){
			add_action( "admin_print_styles-{$suboptions_page}", array( 'CMB2_hookup', 'enqueue_cmb_css' ) );
		}
	}


	/**
	 * Register CMB2 Options
	 *
	 * Controller method for registering all CMB2 options.
	 */
	public function register_cmb2_options() {

		// add years here calling function for fields
		$this->_register_2016_options();
		$this->_register_2015_options();
		$this->_register_years();
	}

/**
	 * Add the options metabox to the array of metaboxes
	 * @since  0.1.0
	 */
	function _register_years() {
		global $gg_years;
		foreach ($gg_years as $key => $value) {

			//create options for all the years
			if( $value > 2016 ) {

					$cmb = new_cmb2_box( array(
						'id'         => $this->metabox_id . '-'.$value,
						'hookup'     => false,
						'cmb_styles' => false,
						'show_on'    => array(
							// These are important, don't remove
							'key'   => 'options-page',
							'value' => array( $this->key . $value, )
						),
					) );
					//wysiwyg
					$cmb->add_field( array(
						'name'		=> 'Intro',
						'id'		=> '_bg_' . $value . '_intro',
						'type'		=> 'wysiwyg',
						'desc'		=> 'Intro for guide overview page'
						));
					//text
					$cmb->add_field( array(
						'id'		=> '_bg_' . $value . '_author_ID',
						'type'		=> 'text',
						'desc'		=> 'Id for author to show',
						'name'		=> 'Author ID'
					) );

					//group for category listings
					$category_listing = $cmb->add_field( array(
						'id'                => '_bg_' . $value,
			            'type'              => 'group',
			            'description'       => 'Set up for Categories',
			            'options'           => array(
			                'group_title'   => 'Category {#}',
			                'add_button'    => 'Add Another Category',
			                'remove_button' => 'Remove Category',
			                'sortable'      => true
			            ) ) );
					// file
					$cmb->add_group_field( $category_listing, array(
							'name'    => 'Image',
							'desc'    => 'Upload an image 300X300',
							'id'      => '_bg_' . $value . '_image',
							'type'    => 'file',
							// Optional:
							'options' => array(
									'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
							),
					) );
					// text
					$cmb->add_group_field( $category_listing, array(
							'name' => 'Category',
							'id'   => '_bg_' . $value . '_cat_id',
							'type' => 'text',
							'desc' => 'Id for category'
					) );
					$cmb->add_group_field( $category_listing, array(
							'name' => 'Link Text',
							'id'   => '_bg_' . $value . '_text',
							'type' => 'text',
							'desc' => 'Category Name'
					) );


				}
			}
		}

	/**
	 * Admin page markup. Mostly handled by CMB2
	 * @since  0.1.0
	 */
	public function admin_page_display() {

		$this->page		= isset( $_GET['page'] ) ? $_GET['page']: '';
		$current_option	= array();

		// check which page we are on
		foreach( $this->option_groups as $option_group ){

			if( in_array( $this->page, $option_group ) ){
				$current_option	= $option_group;
				break;
			}

		} ?>
		<div class="wrap cmb2-options-page <?php echo $current_option['slug']; ?>">
			<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
			<?php cmb2_metabox_form( $this->metabox_id . '-' . $current_option['year'], $current_option['slug'] ); ?>
		</div>
		<?php
	}


	/**
	 * Register settings notices for display
	 *
	 * @param  int   $object_id Option key
	 * @param  array $updated   Array of updated fields
	 * @return void
	 */
	public function settings_notices( $object_id, $updated = array() ) {

		$this->page		= isset( $_GET['page'] ) ? $_GET['page']: '';

		if ( $object_id !== $this->page || empty( $updated ) ) {
			return;
		}

		add_settings_error( $this->page . '-notices', '', __( 'Settings updated.', 'velonews' ), 'updated' );
		settings_errors( $this->page . '-notices' );

	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {

		add_action( 'admin_init', array( $this, 'init' ) );
		add_action( 'admin_menu', array( $this, 'add_options_page' ) );
		add_action( 'cmb2_admin_init', array( $this, 'register_cmb2_options' ) );
		add_action( 'template_include', array( $this, 'gift_guide_template' ) );
		add_action( 'admin_init', array( $this, '_register_years' ) );
		// add_action( 'wp_enqueue_scripts', array( $this, 'gift_guide_scripts' ) );

		foreach( $this->option_groups as $option_group ) {
			add_action( "cmb2_save_options-page_fields_{$this->metabox_id}-{$option_group['year']}", array( $this, 'settings_notices' ), 10, 2 );
		}

	}


	/**
	 * Add the options metabox to the array of metaboxes
	 * @since  0.1.0
	 */

	/**
	 * 2016
	 */
	function _register_2016_options() {

		$prefix	= '_gg_2016_';

		$cmb = new_cmb2_box( array(
			'id'         => $this->metabox_id . '-2016',
			'hookup'     => false,
			'cmb_styles' => false,
			'show_on'    => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( $this->key . '-2016', )
			),
		) );

		// Set our CMB2 fields
		// file
		$cmb->add_field( array(
			'name'		=> 'Banner Image',
			'desc'		=> 'Upload an image or enter an URL.',
			'id'		=> $prefix . 'banner_image',
			'type'		=> 'file',
			// Optional:
			'options'	=> array(
					'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
			),
		) );
		for( $i = 1; $i < 9; $i++ ) {
			// file
			$cmb->add_field( array(
				'name'		=> 'Image '. $i,
				'desc'		=> 'Upload an image or enter an URL.',
				'id'		=> $prefix . 'image_' . $i,
				'type'		=> 'file',
				// Optional:
				'options'	=> array(
						'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
				),
			) );
			// text_url
			$cmb->add_field( array(
				'name'	=> 'Link '. $i,
				'id'	=> $prefix . 'link_' . $i,
				'type'	=> 'text_url',
			) );
			// file
			$cmb->add_field( array(
				'name'		=> 'Gear Image '. $i,
				'desc'		=> 'Upload an image or enter an URL.',
				'id'		=> $prefix . 'more_gifts_image_' . $i,
				'type'		=> 'file',
				// Optional:
				'options'	=> array(
						'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
				),
			) );
		}


	}


	/**
	 * 2015
	 */
	function _register_2015_options() {

		$cmb = new_cmb2_box( array(
			'id'         => $this->metabox_id . '-2015',
			'hookup'     => false,
			'cmb_styles' => false,
			'show_on'    => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( $this->key . '-2015', )
			),
		) );

		// Set our CMB2 fields
		// file
		$cmb->add_field( array(
			'name'		=> 'Banner Image',
			'desc'		=> 'Upload an image or enter an URL.',
			'id'		=> 'gift_guide_banner_image',
			'type'		=> 'file',
			// Optional:
			'options'	=> array(
					'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
			),
		) );
		for( $i = 1; $i < 8; $i++ ) {
			// file
			$cmb->add_field( array(
				'name'		=> 'Image '. $i,
				'desc'		=> 'Upload an image or enter an URL.',
				'id'		=> 'gift_guide_image_'. $i,
				'type'		=> 'file',
				// Optional:
				'options'	=> array(
						'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
				),
			) );
			// text_url
			$cmb->add_field( array(
				'name'	=> 'Link '. $i,
				'id'	=> 'gift_guide_link_'. $i,
				'type'	=> 'text_url',
			) );
			// file
			$cmb->add_field( array(
				'name'		=> 'Gear Image '. $i,
				'desc'		=> 'Upload an image or enter an URL.',
				'id'		=> 'more_gifts_image_'. $i,
				'type'		=> 'file',
				// Optional:
				'options'	=> array(
						'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
				),
			) );
		}
	}


	public function gift_guide_template( $original_template ) {

		 // 2014 had no year attached to the category so have to add the original category slug as an OR
		if ( is_category( $this->category_slugs ) ) {
			return STYLESHEETPATH . '/category-gift-guide.php';
		} else {
			return $original_template;
		}

	}


	/**
	 * Get Theme Option
	 *
	 * Utility method that returns the given theme option (if exists).
	 *
	 * @param string $option Options array key
	 *
	 * @return mixed Options array or specific field
	 */
	public static function get_guide_option( $year = null,  $option = null ) {
		$self = self::singleton();
		$value = null;

		if( function_exists( 'cmb2_get_option' ) ) {

			$value = cmb2_get_option( $self->key . '-' . $year, $option );

		} else {

			$options = get_option( $self->key . '-' . $year );

			if( !empty( $options ) && isset( $options[$option] ) ) {
				$value = $options[$option];
			}

		}

		return $value;
	}

}
function _register_gg_menus(){
		//get the current year
		$current_year = date("Y");
		//set up non empty array with the first year
		$gg_years = array();

		//add to the array for all years including this one <=
		for ( $i = $current_year; $i > 2016 ; $i--) {

			$gg_years[] .= $i;

		}

		foreach ($gg_years as $key => $value) {
			//create menus for each year
			register_nav_menus( array(
					"{$value}-gift-guide" => esc_html__("{$value} Gift Guide")
					)
			);
		}
	}
	add_action( 'after_setup_theme', '_register_gg_menus');
