<?php

class VeloNews_Theme_Setup {

	public static $instance = false;

	public static $is_phone = null;

	public static $is_mobile = null;

	public static $is_tablet = null;

	public static $used_posts = array();

	public function __construct() {
		$this->_add_hooks();
	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( !self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	public static function do_after_theme_setup() {

		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on velonews, use a find and replace
		 * to change 'velonews' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'velonews', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the docum gent head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 150, 150 );

		// This theme uses wp_nav_menu() in some locations.
		register_nav_menus( array(
			'primary'			=> esc_html__( 'Primary', 'velonews' ),
			'footer-network'	=> esc_html__( 'Footer Network', 'velonews' ),
			'footer-sitemap'	=> esc_html__( 'Footer Sitemap', 'velonews' ),
			//'mobile_top'		=> esc_html__( 'Mobile Top menu', 'velonews' ),
			'mobile_footer'		=> esc_html__( 'Mobile Footer menu', 'velonews' ),
		 ) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		 ) );

		/*
		 * Registering image sizes
		 */
		/*
		add_image_size( 'marquee-carousel', 728, 410, true );
		add_image_size( 'marquee-carousel-mobile', 480, 270, true );
		add_image_size( 'small-marquee', 580, 435, true );
		add_image_size( 'gallery-carousel', 200, 200, true );
		add_image_size( 'featured-thumb', 300, 200, true );
		add_image_size( 'legacy-list', 660, 495, false );
		add_image_size( 'thumbnail', 150, 150, true );
		add_image_size( 'author-avatar', 120, 120, true );
		*/
		add_image_size( 'newsletter-top', 400, 225, true );
		add_image_size( 'newsletter-splash', 620, 400, true );
		add_image_size( 'newsletter-image', 300, 200, true );
		add_image_size( 'fb-thumb', 600, 313, false );  // based off facebook's image size of 470x245
		add_image_size( 'featured-thumb-lg', 560, 315, true );
		add_image_size( 'featured-gallery', 590, 360, true );
		add_image_size( 'bike-search', 400, 400, true );
		add_image_size( 'meganav-thumb', 200, 150, true );
		add_image_size( 'image-gallery', 800, 600, false );
		add_image_size( 'longform-image-gallery', 1170, 658, false );
		add_image_size( 'square-buyers-guide-product', 300, 300, true );

		/**
		* Register widget area.
		*
		* @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
		*/
		register_sidebar( array(
			'id'			=> 'right-short',
			'name'			=> __( 'Right Short Sidebar', 'text_domain' ),
			'description'	=> 'Short Popular posts and ad for review overview pages',
			'before_widget'	=> '',
			'after_widget'	=> '',
			'before_title'	=> '',
			'after_title'	=> ''
		 ) );

		register_sidebar( array(
			'id'			=> 'left',
			'name'			=> __( 'Left Sidebar', 'text_domain' ),
			'description'	=> 'Place hot-topics widget here',
			'before_widget'	=> '',
			'after_widget'	=> '',
			'before_title'	=> '',
			'after_title'	=> ''
		 ) );

		register_sidebar( array( 'name' => 'Hot Stories - In Page',
			'id'			=> 'inpage-hotstories',
			'description'	=> 'Place hot-topics widget here',
			'before_widget'	=> '',
			'after_widget'	=> '',
		 ) );

		/**
		 * prevent wp_site_icon() from adding favicons to the wp_head
		 * we already have a function that handles that: vn_display_favicon
		 * must add an image to the customizer so that AMP can work properly
		 */
		remove_action( 'wp_head', 'wp_site_icon', 99 );

	}

	/**
	 * Removes responsive images from Feeds
	 * srcset was causing issues with validation
	 */
	public function no_responsive_image_feeds() {
		add_filter( 'max_srcset_image_width', function() {
			return 1;
		} );
	}

	/**
	 * Adding Google fonts asynchronously because it blocks page loading if loaded old fashion way
	 */
	public function add_fonts_asynchronously() { ?>

		<script>
			WebFontConfig = {
				classes: false,
				events: false,
				google: {
					families: ['Open+Sans+Condensed:300,700', 'Open+Sans:300,400,600,400italic', 'Crimson+Text']
				},
				custom: {
					families: ['FontAwesome'],
					urls: ['https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css']
				}
			};

			(function(d) {
				var wf = document.createElement('script');
				wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
					'://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js';
				wf.type = 'text/javascript';
				wf.async = 'true';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(wf, s);
			})(document);
		</script>

		<?php
	}

	/**
	 * Excerpt lengths
	 */
	public function trim_excerpt( $full_excerpt ) {
		$full_excerpt = strip_tags( $full_excerpt );

		if ( strlen( $full_excerpt ) > 140 ) {

			$short_excerpt	= substr( $full_excerpt, 0, 140 );
			$words			= explode( ' ', $short_excerpt );
			$trim_excerpt	= -( strlen( $words[count( $words ) - 1] ) );

			if ( $trim_excerpt < 0 ) {
				return substr( $short_excerpt, 0, $trim_excerpt );
			} else {
				return $short_excerpt;
			}

		} else {
			return $full_excerpt;
		}
	}

	/**
	 * extend the get_the_archive_title function
	 */
	public function extend_get_the_archive_title( $title ) {


		if ( is_category() ) {
			$title = single_cat_title( '', false );
		} elseif ( is_tag() ) {
			$title = single_tag_title( '', false );
		} elseif ( is_author() ) {
			$title = '<span class="vcard">' . get_the_author() . '</span>' ;
		}

		$page = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

		return 'All ' . $title . ': Page ' . $page;

	}

	/**
	 * Filters the content to remove any extra paragraph or break tags
	 * caused by shortcodes.
	 *
	 * @since 1.0.0
	 *
	 * @param string $content  String of HTML content.
	 * @return string $content Amended string of HTML content.
	 */
	public function shortcode_empty_paragraph_fix( $content ) {

		$array = array(
			'<p>['    => '[',
			']</p>'   => ']',
			// ']<br />' => ']'
		);

		return strtr( $content, $array );

	}

	/**
	 * give the editors the ability for rich media/longform pieces to be POSTS, not PAGES
	 */
	public function longform_callback( $original_template ) {

		$longform_cats = array( 'longform', 'feature' );
		if ( ( is_single() || is_page() ) && !is_category( $longform_cats ) && in_category( $longform_cats ) ) {
			return get_stylesheet_directory() . '/page-templates/longform.php';
		} else {
			return $original_template;
		}

	}

	/**
	 * Add our custom query var to the site
	 */
	public function add_article_query_var( $query_vars ) {
		$query_vars[] = 'article';

		return $query_vars;
	}

	/**
	 * Add the rewrite point only to the root URL of the site
	 * we are not calling flush_rewrite_rules() here because it would be called on every page load.
	 * instead flush rewrite rules manually by saving the permalinks page
	 */
	public function add_article_rewrite() {
		add_rewrite_endpoint( 'article', EP_ROOT );
	}

	/**
	 * Check if a request is being made for the 'article' endpoint
	 * If it is, then handle appropriately
	 */
	public function handle_article_redirect() {

		// if our variable is NOT in the query, break out of here. BAIL!
		if ( false === $article_request = get_query_var( 'article', false ) ) {
			return;
		}

		if ( !empty( $article_request ) && get_post_status( $article_request ) == 'publish' ) {

			$post_url	= get_permalink( $article_request );

			wp_redirect( $post_url , 301 );
			exit;

		} else {
			// else do a 404 if the article request is empty or not published
			global $wp_query;

			$wp_query->set_404();
			status_header( 404 );
			nocache_headers();

			include( get_query_template( '404' ) );
			exit;

		}

	}

	/**
	 * Check if a request is being made for the OOOOOLD article format before WP.
	 * ex: {site_url}/race/mtn/articles/123456.0.html
	 * If it is, then handle appropriately
	 */
	public function handle_html_redirect() {

		$request_uri	= $_SERVER['REQUEST_URI'];

		preg_match( '/[0-9]+\.0\.html$/', $request_uri, $matches );

		if ( !empty( $matches[0] ) ) {

			$post_ID	= str_replace( '.0.html', '', $matches[0] );

			if ( get_post_status( $post_ID ) == 'publish' ) {

				$post_url	= get_permalink( $post_ID );

				wp_redirect( $post_url , 301 );
				exit;

			}


		}

	}

	/**
	 * allow editor to upload svg files
	 */
	public function custom_upload_mimes ( $existing_mimes = array() ) {
		$existing_mimes['svg'] = 'mime/type';
		return $existing_mimes;
	}

	/**
	 * Removes some input fields from the user profile screen
	 */
	public function remove_user_fields( $contactmethods ) {
		unset( $contactmethods['googleplus'] );
		unset( $contactmethods['twitter'] );
		unset( $contactmethods['facebook'] );

		return $contactmethods;
	}

	/**
	 * add "featured" column to post admin screen to see the option at a glance
	 */
	public function featured_columns_head( $defaults ) {
		$defaults['featured'] = 'Featured?';
		return $defaults;
	}


	public function featured_columns_content( $column_name, $post_ID ) {
		if ( $column_name == 'featured' ) {
			if ( get_post_meta( $post_ID, '_featured_post', 1 ) == 'featured' ) {
				echo 'featured';
			} else {
				echo '-- not featured --';
			}
		}
	}

	/**
	 * Filter the Media list table columns to add a File Size column.
	 *
	 * @param	array	$columns	Existing array of columns.
	 *
	 * @return	array				Array with columns including filesize.
	 */
	public function add_filesize_column( $columns ) {
		$columns['filesize'] = 'File Size';

		return $columns;
	}

	/**
	 * Handle File Size column display
	 *
	 * @param	string	$column_name	Name of the custom column.
	 * @param	int		$post_ID		Current Attachment ID.
	 */
	public function manage_filesize_column( $column_name, $post_ID ) {
		// if this is not our column, bail. we don't want it interfering other places
		if ( 'filesize' !== $column_name ) {
			return;
		}

		$bytes = filesize( get_attached_file( $post_ID ) );

		echo size_format( $bytes, 2 );
	}

	public function sort_filesize_column( $sortable_columns ) {

		$sortable_columns['filesize']	= 'filesize';

		return $sortable_columns;

	}

	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this theme.
	 */
	protected function _add_hooks() {

		add_action( 'rss2_head', array( $this, 'no_responsive_image_feeds' ) );
		add_action( 'atom_head', array( $this, 'no_responsive_image_feeds' ) );
		add_action( 'rss_head', array( $this, 'no_responsive_image_feeds' ) );

		add_action( 'wp_head', array( $this, 'add_fonts_asynchronously' ), 0 );

		// front-end filters
		add_filter( 'get_the_excerpt', array( $this, 'trim_excerpt' ) );
		add_filter( 'get_the_archive_title', array( $this, 'extend_get_the_archive_title' ) );
		add_filter( 'the_content', array( $this, 'shortcode_empty_paragraph_fix' ) );
		add_filter( 'template_include', array( $this, 'longform_callback' ) );

		add_filter( 'query_vars', array( $this, 'add_article_query_var') );
		add_action( 'init', array( $this, 'add_article_rewrite' ) );
		add_action( 'template_redirect', array( $this, 'handle_article_redirect' ) );
		add_action( 'wp', array( $this, 'handle_html_redirect' ) );

		// admin area filters and actions
		add_filter( 'upload_mimes', array( $this, 'custom_upload_mimes' ) );
		add_filter( 'user_contactmethods', array( $this, 'remove_user_fields' ), 10, 1 );
		add_filter( 'manage_posts_columns', array( $this, 'featured_columns_head' ) );
		add_action( 'manage_posts_custom_column', array( $this, 'featured_columns_content' ), 10, 2 );

		add_filter( 'manage_media_columns', array( $this, 'add_filesize_column' ) );
		add_filter( 'manage_upload_sortable_columns', array( $this, 'sort_filesize_column' ) );
		add_action( 'manage_media_custom_column', array( $this, 'manage_filesize_column' ), 10, 2 );

		// remove nasty features of WP
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'admin_print_styles', 'print_emoji_styles' );

	}

}
