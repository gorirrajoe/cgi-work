<?php
/**
 * Custom Metaboxes | Latest & Popular Page Templates
 */
class VeloNews_Metadata_LatestPopular {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function vn_latest_popular_metaboxes() {

			// Start with an underscore to hide fields from custom fields list
			$prefix = '_vn_';

			/**
			 * Initiate the metabox
			 */
			$cmb = new_cmb2_box( array(
				'id'            => 'latest-popular',
				'title'         => __( 'Latest & Popular Page Options', 'vn' ),
				'object_types'  => array( 'page' ), // Post type
				'show_on'       => array( 'key' => 'page-template', 'value' => 'page-templates/latest-and-popular-template.php' ),
				'context'       => 'normal',
				'priority'      => 'high',
				'show_names'    => true // Show field names on the left
			) );

			// radio_inline
			$cmb->add_field( array(
					'name' => 'Latest or Popular?',
					'id'   => $prefix . 'latest_or_popular',
					'type' => 'radio_inline',
					'options' => array(
							'latest'	=> 'Latest',
							'popular'	=> 'Popular',
					),
			) );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'vn_latest_popular_metaboxes' ) );
	}

}
