<?php
/**
* Custom Metaboxes | Category Metaboxes
*/
class VeloNews_Metadata_Category {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}
	public function vn_category_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_vn_';

		/**
		* Initiate the metabox
		*/
		$cmb = new_cmb2_box( array(
			'id'               => 'category-meta',
			'object_types'     => array( 'term',),
			'taxonomies'       => array( 'category', 'stagecat'),
			'show_names'       => true, // Show field names on the left
			'new_term_section' => false,
			'fields'           => array(
				'extra_info'      => array(
					'name'           => 'Extra Customization',
					'desc'           => '',
					'id'             => 'extra_info',
					'type'           => 'title',
					'on_front'       => false,
				)
			)
		) );

		$cmb->add_field( array(
			'name'        => 'Archive Type?',
			'id'          => $prefix . 'archive_type',
			'type'        => 'radio_inline',
			'default'     => 'standard',
			'desc'        => 'standard: blogroll, extended: modular, multistage: extended + race data',
			'options'     => array(
				'standard'   => __( 'Standard', 'cmb2' ),
				'extended'   => __( 'Extended', 'cmb2' ),
				'multistage' => __( 'Multistage', 'cmb2' ),
			),
		) );

		// text_small
		$cmb->add_field( array(
			'name'       => 'Race Year',
			'desc'       => 'enter race year',
			'id'         => $prefix . 'race-year',
			'type'       => 'text_small',
			'show_on_cb' => 'vn_show_for_multistage'
		) );

		// file
		$cmb->add_field( array(
			'name'    => 'Presented by Sponsor - Image',
			'desc'    => 'Upload an image or enter an URL. max height of 25px',
			'id'      => $prefix . 'presentedby_logo',
			'type'    => 'file',
			'text'    => array(
				'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
			),
		) );
		// text
		$cmb->add_field( array(
			'name'    => 'Presented by Sponsor - Image Width',
			'desc'    => 'for AMP purposes, enter image width here (height of 25px is assumed)',
			'id'      => $prefix . 'presentedby_width',
			'type'    => 'text',
		) );
		// text
		$cmb->add_field( array(
			'name'    => 'Presented by Sponsor - Name',
			'desc'    => 'enter the name for the sponsor. used for the alt tag',
			'id'      => $prefix . 'presentedby_name',
			'type'    => 'text',
		) );
		// text_url
		$cmb->add_field( array(
			'name' => 'Presented by Sponsor - URL',
			'desc' => 'Link to brand/company landing page',
			'id'   => $prefix . 'presentedby_url',
			'type' => 'text_url',
		) );

		// text
		$cmb->add_field( array(
			'name'    => 'Top Right Featured Post',
			'desc'    => 'enter one post id',
			'id'      => $prefix . 'top-right-featured',
			'type'    => 'text',
		) );

		// checkbox
		$cmb->add_field( array(
			'name' => 'Enable 4 Across (Top)?',
			'desc' => 'check ON to enable',
			'id'   => $prefix . 'four-across-top-enable',
			'type' => 'checkbox'
		) );

		// text
		$cmb->add_field( array(
			'name'    => '4 Across (Top) - Title',
			'desc'    => 'enter title',
			'id'      => $prefix . 'four-across-top-title',
			'type'    => 'text',
		) );

		// text
		$cmb->add_field( array(
			'name'    => '4 Across (Top) - Category',
			'desc'    => 'enter one supplemental category id',
			'id'      => $prefix . 'four-across-top',
			'type'    => 'text',
		) );

		// checkbox
		$cmb->add_field( array(
			'name' => 'Enable 2 Across?',
			'desc' => 'check ON to enable',
			'id'   => $prefix . 'two-across-enable',
			'type' => 'checkbox'
		) );

		// text
		$cmb->add_field( array(
			'name'    => '2 Across - Category',
			'desc'    => 'enter two post id\'s',
			'id'      => $prefix . 'two-across',
			'type'    => 'text',
		) );

		// checkbox
		$cmb->add_field( array(
			'name' => 'Enable 5 Across?',
			'desc' => 'check ON to enable',
			'id'   => $prefix . 'five-across-enable',
			'type' => 'checkbox'
		) );

		// text
		$cmb->add_field( array(
			'name'    => '5 Across - Title',
			'desc'    => 'enter title',
			'id'      => $prefix . 'five-across-title',
			'type'    => 'text',
		) );

		// text
		$cmb->add_field( array(
			'name'    => '5 Across - Category',
			'desc'    => 'enter one supplemental category id',
			'id'      => $prefix . 'five-across',
			'type'    => 'text',
		) );

		// checkbox
		$cmb->add_field( array(
			'name' => 'Enable 2x2?',
			'desc' => 'check ON to enable',
			'id'   => $prefix . 'two-by-two-enable',
			'type' => 'checkbox'
		) );

		// text
		$cmb->add_field( array(
			'name'    => '2x2 - Title',
			'desc'    => 'enter title',
			'id'      => $prefix . 'two-by-two-title',
			'type'    => 'text',
		) );

		// text
		$cmb->add_field( array(
			'name'    => '2x2',
			'desc'    => 'enter one supplemental category id',
			'id'      => $prefix . 'two-by-two',
			'type'    => 'text',
		) );

		// checkbox
		$cmb->add_field( array(
			'name' => 'Enable 4 Across (Bottom)?',
			'desc' => 'check ON to enable',
			'id'   => $prefix . 'four-across-bottom-enable',
			'type' => 'checkbox'
		) );

		// text
		$cmb->add_field( array(
			'name' => '4 Across (Bottom) - Title',
			'desc' => 'enter title',
			'id'   => $prefix . 'four-across-bottom-title',
			'type' => 'text',
		) );

		// text
		$cmb->add_field( array(
			'name' => '4 Across (Bottom)',
			'desc' => 'enter one supplemental category id',
			'id'   => $prefix . 'four-across-bottom',
			'type' => 'text',
		) );

	}


	public function vn_show_for_multistage( $field ) {
		if( get_term_meta( $field->object_id, '_vn_archive_type', 1 ) == 'multistage' ) {
			return true;
		}
	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'vn_category_metaboxes' ) );

	}
}
