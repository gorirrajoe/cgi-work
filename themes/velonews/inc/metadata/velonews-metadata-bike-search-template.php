<?php
/**
 * Custom Metaboxes | Bike Search Template
 */
class VeloNews_Metadata_BikeSearch {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function vn_bike_search_template_metaboxes() {

			// Start with an underscore to hide fields from custom fields list
			$prefix = '_vn_';

			/**
			 * Initiate the metabox
			 */
			$cmb = new_cmb2_box( array(
				'id'			=> 'bike-search',
				'title'			=> __( 'Bike Search Page Options', 'vn' ),
				'object_types'	=> array( 'page' ), // Post type
				'show_on'		=> array( 'key' => 'page-template', 'value' => array(
					'page-templates/bike-search.php'
				) ),
				'context'		=> 'normal',
				'priority'		=> 'high',
				'show_names'	=> true // Show field names on the left
			) );

			$cmb->add_field( array(
				'name'		=> 'Intro Title',
				'id'		=> $prefix . 'intro_title',
				'type'		=> 'text'
			) );

			$cmb->add_field( array(
				'name'		=> 'Featured Bike Review',
				'desc'		=> 'enter post ID',
				'id'		=> $prefix . 'featured_bike_id',
				'type'		=> 'text_small'
			) );

			$group_field_id = $cmb->add_field( array(
				'id'			=> $prefix . 'bike_type',
				'type'			=> 'group',
				'description'	=> __( 'Generates Bike Types', 'cmb2' ),
				'options'		=> array(
					'group_title'	=> __( 'Bike Type {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
					'add_button'	=> __( 'Add Another Bike Type', 'cmb2' ),
					'remove_button'	=> __( 'Remove Bike Type', 'cmb2' ),
					'sortable'		=> true, // beta
			    ),
			) );

			$cmb->add_group_field( $group_field_id, array(
				'name'	=> 'Bike Type Background Image',
				'id'	=> 'image',
				'type'	=> 'file',
			) );

			$cmb->add_group_field( $group_field_id, array(
				'name'				=> 'Bike Type',
				'id'				=> 'bike_type',
				'type'				=> 'radio',
				'show_option_none'	=> true,
				'options'			=> array(
					'aero-road::Aero Road'		=> __( 'Aero Road', 'cmb2' ),
					'all-around::All Around'	=> __( 'All Around', 'cmb2' ),
					'cyclocross::Cyclocross'	=> __( 'Cyclocross', 'cmb2' ),
					'endurance::Endurance'		=> __( 'Endurance', 'cmb2' ),
					'gravel::Gravel'			=> __( 'Gravel', 'cmb2' ),
					'mountain::Mountain'		=> __( 'Mountain', 'cmb2' ),
			    ),
			) );




	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'vn_bike_search_template_metaboxes' ) );
	}
}
