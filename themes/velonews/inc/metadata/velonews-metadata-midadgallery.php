<?php
/**
 * Custom Metaboxes | Mid-Ad Gallery
 */
class VeloNews_Metadata_MidAdGallery {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}

	public function vn_midadgallery_metaboxes() {

	    // Start with an underscore to hide fields from custom fields list
	    $prefix = '_cgi_';

	    /**
	     * Initiate the metabox
	     */
	    $cmb = new_cmb2_box( array(
	      'id'            => 'mid-ad-gallery',
	      'title'         => __( 'Mid-Ad Gallery Options', 'vn' ),
	      'object_types'  => array( 'post' ), // Post type
	      'context'       => 'normal',
	      'priority'      => 'high',
	      'show_names'    => true // Show field names on the left
	    ) );

	    // checkbox
	    $cmb->add_field( array(
	        'name' => 'Mid-Ad Gallery?',
	        'desc' => 'check ON if this post is a mid-ad gallery',
	        'id'   => $prefix . 'mid_ad',
	        'type' => 'checkbox'
	    ) );
	    // text_url
	    $cmb->add_field( array(
	        'name' => __( 'Header URL', 'vn' ),
	        'id'   => $prefix . 'mid_ad_header_url',
	        'type' => 'text_url',
	    ) );
	    // text_url
	    $cmb->add_field( array(
	        'name' => __( 'Header Image URL', 'vn' ),
	        'id'   => $prefix . 'mid_ad_header_img',
	        'type' => 'text_url',
	    ) );
	    // text_url
	    $cmb->add_field( array(
	        'name' => __( 'Header URL - MOBILE', 'vn' ),
	        'id'   => $prefix . 'mid_ad_header_mobile_url',
	        'type' => 'text_url',
	    ) );
	    // text_url
	    $cmb->add_field( array(
	        'name' => __( 'Header Image URL - MOBILE', 'vn' ),
	        'id'   => $prefix . 'mid_ad_header_mobile_img',
	        'type' => 'text_url',
	    ) );
	    // text_url
	    $cmb->add_field( array(
	        'name' => __( 'Mid Ad URL', 'vn' ),
	        'id'   => $prefix . 'mid_ad_url',
	        'type' => 'text_url',
	    ) );
	    // text_url
	    $cmb->add_field( array(
	        'name' => __( 'Mid Ad Image URL', 'vn' ),
	        'id'   => $prefix . 'mid_ad_img',
	        'type' => 'text_url',
	    ) );
	    // text_url
	    $cmb->add_field( array(
	        'name' => __( 'Mid Ad URL 2', 'vn' ),
	        'id'   => $prefix . 'mid_ad_url2',
	        'type' => 'text_url',
	    ) );
	    // text_url
	    $cmb->add_field( array(
	        'name' => __( 'Mid Ad Image URL 2', 'vn' ),
	        'id'   => $prefix . 'mid_ad_img2',
	        'type' => 'text_url',
	    ) );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'vn_midadgallery_metaboxes' ) );
	}
}

