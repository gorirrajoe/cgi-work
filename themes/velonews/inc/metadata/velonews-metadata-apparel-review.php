<?php
/**
 * Custom Metaboxes | Apparel Reviews
 */
class VeloNews_Metadata_ApparelReviews {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function vn_apparel_review_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_vn_';

		/**
		 * Initiate the metabox
		 */
		$cmb_apparel = new_cmb2_box( array(
			'id'           => 'apparel_review',
			'title'        => __( 'Apparel Reviews', 'vn' ),
			'object_types' => array( 'apparel-reviews', ), // Post type
			'context'      => 'advanced',
			'priority'     => 'high',
			'show_names'   => true, // Show field names on the left
			'closed'       => false,
		) );


		// radio_inline
		$cmb_apparel->add_field( array(
			'name'          => 'Featured?',
			'desc'          => 'same function as stickies (home page marquee)',
			'id'            => '_featured_post',
			'type'          => 'radio_inline',
			'default'       => 'not_featured',
			'options'       => array(
				'featured'     => 'Featured',
				'not_featured' => 'Not Featured',
			),
		) );
		// select
		$cmb_apparel->add_field( array(
			'name'                  => 'Type',
			'desc'                  => 'Select type of apparel',
			'id'                    => $prefix . 'apparel_type',
			'type'                  => 'select',
			'show_option_none'      => '-- choose one --',
			'options'               => array(
				'helmet::Helmets'		=> __( 'Helmet', 'cmb2' ),
				'jersey::Jerseys'		=> __( 'Jersey', 'cmb2' ),
				'shoe::Shoes'			=> __( 'Shoe', 'cmb2' ),
				'bib::Bibs'				=> __( 'Bib', 'cmb2' ),
				'outerwear::Outerwear'	=> __( 'Outerwear', 'cmb2' ),
				'tool::Tools'			=> __( 'Tool', 'cmb2' ),
				'wheels::Wheels'		=> __( 'Wheels', 'cmb2' ),
				'misc::Miscellaneous'	=> __( 'Miscellaneous', 'cmb2' ),
			),
			'column'		=> array(
				'name'		=> 'Type'
			)
		) );

		// radio_inline
		$cmb_apparel->add_field( array(
			'name'    => 'Gender',
			'id'      => $prefix . 'apparel_gender',
			'type'    => 'radio_inline',
			'default' => 'uni',
			'options' => array(
					'u'     => __( 'Uni', 'cmb2' ),
					'm'     => __( 'Male', 'cmb2' ),
					'f'     => __( 'Female', 'cmb2' ),
			),
		) );

		// text
		$specs1 = $cmb_apparel->add_field( array(
			'name' => 'Brand',
			'id'   => $prefix . 'apparel_brand',
			'type' => 'text',
			'column'		=> array(
				'name'		=> 'Brand'
			)
		) );

		// text
		$specs2 = $cmb_apparel->add_field( array(
			'name'    => 'MSRP',
			'default' => '',
			'id'      => $prefix . 'apparel_msrp',
			'type'    => 'text',
			'desc'    => 'MSRP for apparel. do not include $ sign nor commas',
		) );

		// text
		$specs5 = $cmb_apparel->add_field( array(
			'name'    => 'MSRP Notes',
			'default' => '',
			'id'      => $prefix . 'apparel_msrp_notes',
			'type'    => 'text',
			'desc'    => 'MSRP notes for apparel.',
		) );

		// text
		$specs3 = $cmb_apparel->add_field( array(
			'name'    => 'Helmet Type',
			'default' => '',
			'id'      => $prefix . 'helmet_type',
			'type'    => 'text',
			'desc'    => 'only if this is a helmet',
		) );

		// text
		$specs4 = $cmb_apparel->add_field( array(
			'name'    => 'Weight',
			'default' => '',
			'id'      => $prefix . 'weight',
			'type'    => 'text',
		) );

		// text
		$specs6 = $cmb_apparel->add_field( array(
			'name'    => 'Size',
			'default' => '',
			'id'      => $prefix . 'size',
			'type'    => 'text',
		) );
		// text
		$rating = $cmb_apparel->add_field( array(
			'name'    => 'Rating',
			'default' => '0',
			'id'      => $prefix . 'rating',
			'desc'    => 'Out of 10',
			'type'    => 'text',
		) );

		// group
		$store_links = $cmb_apparel->add_field( array(
			'name'           => 'Stores',
			'id'             => $prefix . 'stores',
			'type'           => 'group',
			'options'        => array(
				'group_title'   => __( 'Store {#}', 'cmb' ),
				'add_button'    => __( 'Add Another Store', 'cmb' ),
				'remove_button' => __( 'Remove Store', 'cmb' ),
				'sortable'      => true, // beta
			)
		));

		$cmb_apparel->add_group_field( $store_links, array(
			'name' => 'Store Name',
			'id'   => 'store_name',
			'type' => 'text',
		) );

		$cmb_apparel->add_group_field( $store_links, array(
			'name' => 'Store URL',
			'id'   => 'store_url',
			'type' => 'text_url',
		) );


		// file list
		$cmb_apparel->add_field( array(
			'name' => 'Gallery',
			'id'   => $prefix . 'apparel_gallery',
			'type' => 'file_list',
			'preview_size' => array( 100, 100 ),
			'options' => array(
				'add_upload_files_text' => 'Add or Upload Files', // default: "Add or Upload Files"
				'remove_image_text' => 'Remove Image', // default: "Remove Image"
				'file_text' => 'File:', // default: "File:"
				'file_download_text' => 'Download', // default: "Download"
				'remove_text' => 'Remove', // default: "Remove"
			),
		) );

		// wizzy
		$cmb_apparel->add_field( array(
				'name'    => 'Review',
				'desc'    => 'Apparel review write up',
				'id'      => $prefix . 'apparel_review',
				'type'    => 'wysiwyg',
		) );

				// guide author id
		$cmb_apparel->add_field( array(
			'name' => 'Display Author',
			'id'   => $prefix . 'review_author',
			'type' => 'checkbox',

		) );
		if( ! is_admin() || ! class_exists( '\Cmb2Grid\Grid\Cmb2Grid' ) ) {
			return;
		}

		$cmb2Grid = new \Cmb2Grid\Grid\Cmb2Grid($cmb_apparel);

		$row = $cmb2Grid->addRow();

		$row->addColumns( array(
			$specs1,
			$specs2,
			$specs5,
		));

		$row = $cmb2Grid->addRow();

		$row->addColumns( array(
			$specs3,
			$specs4,
			$specs6,
		));

	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'vn_apparel_review_metaboxes' ) );
	}
}
