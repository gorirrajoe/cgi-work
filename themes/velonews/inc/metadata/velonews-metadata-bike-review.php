<?php
/**
 * Custom Metaboxes | Bike Reviews
 */
class VeloNews_Metadata_BikeReviews {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}
	public function vn_bike_review_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_vn_bike_review_';

		/**
		 * Initiate the metabox
		 */
		$cmb_bikes = new_cmb2_box( array(
			'id'           => 'bike_review',
			'title'        => __( 'Bike Reviews', 'vn' ),
			'object_types' => array( 'bike-reviews', ), // Post type
			'context'      => 'normal',
			'priority'     => 'high',
			'show_names'   => true, // Show field names on the left
			'closed'       => false,
		) );


		// radio_inline
		$cmb_bikes->add_field( array(
			'name'          => 'Featured?',
			'desc'          => 'same function as stickies (home page marquee)',
			'id'            => '_featured_post',
			'type'          => 'radio_inline',
			'default'       => 'not_featured',
			'options'       => array(
				'featured'     => 'Featured',
				'not_featured' => 'Not Featured',
			),

		) );
		// Bike Category
		// select option
		$cmb_bikes->add_field( array(
			'name'                    => 'Type',
			'desc'                    => 'Select type of bike',
			'id'                      => $prefix . 'bike_type',
			'type'                    => 'select',
			'show_option_none'        => '-- choose one --',
			'default'                 => 'All Around',
			'options'                 => array(
				'all-around::All Around' => __( 'All Around', 'cmb2' ),
				'aero-road::Aero Road'   => __( 'Aero Road', 'cmb2' ),
				'endurance::Endurance'   => __( 'Endurance', 'cmb2' ),
				'gravel::Gravel'         => __( 'Gravel', 'cmb2' ),
				'time-trial::Time Trial' => __( 'TT', 'cmb2' ),
				'cyclocross::Cyclocross' => __( 'Cyclocross', 'cmb2' ),
				'mountain::Mountain'     => __( 'Mountain', 'cmb2' ),
				// 'womens::Women\'s' => __( "Women's", 'cmb2' ),
			),
		) );

		// text
		$specs6 = $cmb_bikes->add_field( array(
			'name' => 'Brand',
			'id'   => $prefix . 'bike_brand',
			'type' => 'text',
			'column'		=> array(
				'name'		=> 'Brand'
			)
		) );

		// radio_inline
		$specs5 = $cmb_bikes->add_field( array(
			'name'    => 'Gender',
			'id'      => $prefix . 'apparel_gender',
			'type'    => 'radio_inline',
			'default' => 'u',
			'options' => array(
					'u'     => __( 'Uni', 'cmb2' ),
					'm'     => __( 'Male', 'cmb2' ),
					'f'     => __( 'Female', 'cmb2' ),
			),
		) );

		// text
		$specs1 = $cmb_bikes->add_field( array(
			'name'    => 'MSRP',
			'default' => '',
			'id'      => $prefix . 'bike_msrp',
			'type'    => 'text',
			'desc'    => 'MSRP for bike. do not include $ sign nor any commas',
		) );

		$specs4 = $cmb_bikes->add_field( array(
			'name'    => 'MSRP Notes',
			'default' => '',
			'id'      => $prefix . 'bike_msrp_notes',
			'type'    => 'text',
		) );

		$specs2 = $cmb_bikes->add_field( array(
			'name' => 'Size',
			'id'   => $prefix . 'bike_size',
			'type' => 'text',
		) );

		$specs3 = $cmb_bikes->add_field( array(
			'name' => 'Weight',
			'id'   => $prefix . 'bike_weight',
			'type' => 'text',
		) );

		// Scoring fields
		// title
		$cmb_bikes->add_field( array(
			'name' => 'SCORES',
			'type' => 'title',
			'id'   => 'scores_title'
		) );
		// text
		$scoring1 = $cmb_bikes->add_field( array(
			'name' => 'LAB Score',
			'id'   => $prefix . 'bike_lab_score',
			'type' => 'text',
			'desc' => '(Road) - Out of 20',
		) );

		// text
		$scoring2 = $cmb_bikes->add_field( array(
			'name' => 'BUILD Score',
			'id'   => $prefix . 'bike_build_score',
			'type' => 'text',
			'desc' => 'Out of 15 or 20',
		) );

		// text
		$scoring3 = $cmb_bikes->add_field( array(
			'name' => 'COMFORT Score',
			'id'   => $prefix . 'bike_comfort_score',
			'type' => 'text',
			'desc' => 'Out of 15 or 20',
		) );

		// text
		$scoring4 = $cmb_bikes->add_field( array(
			'name' => 'VALUE Score',
			'id'   => $prefix . 'bike_value_score',
			'type' => 'text',
			'desc' => 'Out of 15',
		) );

		// text
		$scoring5 = $cmb_bikes->add_field( array(
			'name' => 'HANDLING Score',
			'id'   => $prefix . 'bike_handling_score',
			'type' => 'text',
			'desc' => 'Out of 15 (25 as of 6/1/2017)',
		) );

		// text
		$scoring6 = $cmb_bikes->add_field( array(
			'name' => 'PEDALING RESPONSE Score',
			'id'   => $prefix . 'bike_pedaling_score',
			'type' => 'text',
			'desc' => 'Out of 15 (25 as of 6/1/2017)',
		) );

		// text
		$scoring7 = $cmb_bikes->add_field( array(
			'name' => 'AESTHETICS Score',
			'id'   => $prefix . 'bike_aesthetics_score',
			'type' => 'text',
			'desc' => 'Out of 5',
		) );

		// text
		$scoring8 = $cmb_bikes->add_field( array(
			'name' => 'ASCENDING Score',
			'id'   => $prefix . 'bike_ascending_score',
			'type' => 'text',
			'desc' => '(Mountain) - Out of 15',
		) );

		// text
		$scoring9 = $cmb_bikes->add_field( array(
			'name' => 'DESCENDING Score',
			'id'   => $prefix . 'bike_descending_score',
			'type' => 'text',
			'desc' => '(Mountain) - Out of 15',
		) );

		// text
		$scoring10 = $cmb_bikes->add_field( array(
			'name' => 'VERSATILITY Score',
			'id'   => $prefix . 'bike_versatility_score',
			'type' => 'text',
			'desc' => '(Gravel) - Out of 15',
		) );

		// text
		$cmb_bikes->add_field( array(
			'name' => 'Lab Notes',
			'id'   => $prefix . 'lab_notes',
			'type' => 'text',
		) );


		// Pros & Cons
		// group
		$bike_pros = $cmb_bikes->add_field( array(
			'name'           => 'Bike Pros',
			'id'             => $prefix . 'bike_pros',
			'type'           => 'group',
			'options'        => array(
				'group_title'   => __( 'Pro {#}', 'cmb' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add Another Pro', 'cmb' ),
				'remove_button' => __( 'Remove Pro', 'cmb' ),
				'sortable'      => true, // beta
			),
		) );

		$cmb_bikes->add_group_field( $bike_pros, array(
			'name' => 'Bike Pro',
			'id'   => 'bike_pro',
			'type' => 'text',
		) );

		$bike_cons = $cmb_bikes->add_field( array(
			'name'           => 'Bike Cons',
			'id'             => $prefix . 'bike_cons',
			'type'           => 'group',
			'options'        => array(
				'group_title'   => __( 'Con {#}', 'cmb' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add Another Con', 'cmb' ),
				'remove_button' => __( 'Remove Con', 'cmb' ),
				'sortable'      => true, // beta
			),
		) );

		$cmb_bikes->add_group_field( $bike_cons, array(
			'name' => 'Bike Con',
			'id'   => 'bike_con',
			'type' => 'text',
		) );

		// Purchase links
		// group
		$store_links = $cmb_bikes->add_field( array(
			'name'           => 'Stores',
			'id'             => $prefix . 'stores',
			'type'           => 'group',
			'options'        => array(
				'group_title'   => __( 'Store {#}', 'cmb' ),
				'add_button'    => __( 'Add Another Store', 'cmb' ),
				'remove_button' => __( 'Remove Store', 'cmb' ),
				'sortable'      => true, // beta
			)
		));

		$cmb_bikes->add_group_field( $store_links, array(
			'name' => 'Store Name',
			'id'   => 'store_name',
			'type' => 'text',
		) );

		$cmb_bikes->add_group_field( $store_links, array(
			'name' => 'Store URL',
			'id'   => 'store_url',
			'type' => 'text_url',
		) );


		// Bike Review Information
		/* text
		$cmb_bikes->add_field( array(
			'name'    => 'Bike Name',
			'desc'    => '',
			'id'      => $prefix . 'bike_name_txt',
			'type'    => 'text',
			'options' => array(),
		) );
		*/

		// text
		$cmb_bikes->add_field( array(
			'name'    => 'Bike Review Title',
			'desc'    => '',
			'id'      => $prefix . 'bike_about_txt',
			'type'    => 'text',
			'options' => array(),
		) );

		// Gallery
		// file list
		$cmb_bikes->add_field( array(
			'name'                   => 'Gallery',
			'id'                     => $prefix . 'bike_gallery',
			'type'                   => 'file_list',
			'desc'                   => 'add 2 or more images to create a gallery',
			'preview_size'           => array( 100, 100 ),
			'options'                => array(
				'add_upload_files_text' => 'Add or Upload Files', // default: "Add or Upload Files"
				'remove_image_text'     => 'Remove Image', // default: "Remove Image"
				'file_text'             => 'File:', // default: "File:"
				'file_download_text'    => 'Download', // default: "Download"
				'remove_text'           => 'Remove', // default: "Remove"
			),
		) );

		// wizzy
		$cmb_bikes->add_field( array(
			'name' => 'Review',
			'desc' => 'Bike review write up',
			'id'   => $prefix . 'bike_review',
			'type' => 'wysiwyg',
		) );

		// guide author id
		$cmb_bikes->add_field( array(
			'name' => 'Display Author',
			'id'   => $prefix . 'review_author',
			'type' => 'checkbox',

		) );

		// CUSTOM: overall_score
		$cmb_bikes->add_field( array(
			'name' => '',
			'id'   => $prefix . 'bike_overall',
			'type' => 'overall_score',
			'column'		=> array(
				'name'		=> 'Overall Score'
			)
		) );

		// CUSTOM: database_transfer
		$cmb_bikes->add_field( array(
			'name' => '',
			'id'   => $prefix . 'bikedb',
			'type' => 'database_transfer',
		) );



		if( ! is_admin() || ! class_exists( '\Cmb2Grid\Grid\Cmb2Grid' ) ) {
			return;
		}

		$cmb2Grid = new \Cmb2Grid\Grid\Cmb2Grid($cmb_bikes);

		$row = $cmb2Grid->addRow();

		$row->addColumns( array(
			$specs6,
			$specs5,
		));

		$row = $cmb2Grid->addRow();

		$row->addColumns( array(
			$specs1,
			$specs4,
		));

		$row = $cmb2Grid->addRow();

		$row->addColumns( array(
			$specs2,
			$specs3,
		));

		$row = $cmb2Grid->addRow();

		$row->addColumns( array(
			$scoring1,
			$scoring2,
			$scoring3,
			$scoring4,
		));

		$row = $cmb2Grid->addRow();

		$row->addColumns( array(
			$scoring5,
			$scoring6,
			$scoring7,
			$scoring8,
		));

		$row = $cmb2Grid->addRow();

		$row->addColumns( array(
			$scoring9,
			$scoring10,
		));

	}


	public function cmb2_render_callback_for_overall_score( $field, $escaped_value, $object_id, $object_type, $field_type_object ) {
		// echo $field_type_object->input( array( 'type' => 'text' ) );
	}



	public function cmb2_sanitize_overall_score_callback( $override_value, $value ) {
		$overall = $_POST['_vn_bike_review_bike_lab_score'] + $_POST['_vn_bike_review_bike_build_score'] + $_POST['_vn_bike_review_bike_comfort_score'] + $_POST['_vn_bike_review_bike_value_score'] + $_POST['_vn_bike_review_bike_handling_score'] + $_POST['_vn_bike_review_bike_pedaling_score'] + $_POST['_vn_bike_review_bike_aesthetics_score'] + $_POST['_vn_bike_review_bike_ascending_score'] + $_POST['_vn_bike_review_bike_descending_score'] + $_POST['_vn_bike_review_bike_versatility_score'];

		return $overall;
	}


	public function cmb2_render_callback_for_database_transfer( $field, $escaped_value, $object_id, $object_type, $field_type_object ) {
		// echo $field_type_object->input( array( 'type' => 'text' ) );
	}


	public function cmb2_sanitize_database_transfer_callback( $override_value, $value, $object_id ) {
		global $wpdb;
		$score = $_POST['_vn_bike_review_bike_lab_score'] + $_POST['_vn_bike_review_bike_build_score'] + $_POST['_vn_bike_review_bike_comfort_score'] + $_POST['_vn_bike_review_bike_value_score'] + $_POST['_vn_bike_review_bike_handling_score'] + $_POST['_vn_bike_review_bike_pedaling_score'] + $_POST['_vn_bike_review_bike_aesthetics_score'] + $_POST['_vn_bike_review_bike_ascending_score'] + $_POST['_vn_bike_review_bike_descending_score'] + $_POST['_vn_bike_review_bike_versatility_score'];

		$type			= $_POST['_vn_bike_review_bike_type'];
		$type_pieces	= explode( '::', $type );
		$type_key		= $type_pieces[0];
		$postid			= get_the_ID();

		$wpdb->replace(
			'wp_bikes',
			array(
				'post_id'		=> $object_id,
				'overall_score'	=> $score,
				'type'			=> $type_key
			),
			array(
				'%d',
				'%f',
				'%s'
			)
		);
		return;
	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}

	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {

		//lets try to keep this clean and add each metabox individually
		add_action( 'cmb2_admin_init', array( $this, 'vn_bike_review_metaboxes' ) );
		add_action( 'cmb2_render_overall_score', array( $this, 'cmb2_render_callback_for_overall_score' ), 10, 5 );
		add_filter( 'cmb2_sanitize_overall_score', array( $this, 'cmb2_sanitize_overall_score_callback' ), 10, 2 );
		add_action( 'cmb2_render_database_transfer', array( $this, 'cmb2_render_callback_for_database_transfer' ), 10, 5 );
		add_filter( 'cmb2_sanitize_database_transfer', array( $this, 'cmb2_sanitize_database_transfer_callback' ), 10, 3 );

	}
}
