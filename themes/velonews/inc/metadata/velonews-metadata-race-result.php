<?php
/**
 * Custom Metaboxes | Race Results
 * Single post for importing results file
 */
class VeloNews_Metadata_Race_Results {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}

	public function vn_race_results_metaboxes() {

			// Start with an underscore to hide fields from custom fields list
			$prefix = '_vn_';

			/**
			 * Initiate the metabox
			 */
			$cmb_results = new_cmb2_box( array(
				'id' => 'race-results',
				'title' => __( 'Race Results', 'vn' ),
				'object_types' => array( 'race_result', ), // Post type
				'context' => 'normal',
				'priority' => 'high',
				'show_names' => true, // Show field names on the left
				'closed' => false,
			) );

			// text
			$cmb_results->add_field( array(
					'name' => 'Stage Number',
					'desc'	=> 'Stage number',
					'id' => $prefix . 'stages_stage_number',
					'type' => 'text',
			) );

			// text
			$cmb_results->add_field( array(
				'name' => 'Position',
				'desc' => 'Racer\'s finishing position',
				'id' => $prefix . 'finish_place',
				'type' => 'text',
			) );

			// text
			$cmb_results->add_field( array(
				'name' => 'Overall Position',
				'desc' => 'Racer\'s overall position',
				'id' => $prefix . 'overall_position',
				'type' => 'text',
			) );

			// text
			$cmb_results->add_field( array(
				'name' => 'Finish Time',
				'desc' => 'Racer\'s finishing time',
				'id' => $prefix . 'finish_time',
				'type' => 'text',
			) );


			// text
			$cmb_results->add_field( array(
					'name' => 'Full Name',
					'id' => $prefix . 'racer_name_full',
					'type' => 'text',
			) );

			// text
			$cmb_results->add_field( array(
					'name' => 'Country Code',
					'id' => $prefix . 'racer_country',
					'type' => 'text',
			) );

			// text
			$cmb_results->add_field( array(
					'name'    => 'Team',
					'id'      => $prefix . 'race_team',
					'type'    => 'text',
			) );

			//text
			$cmb_results->add_field( array(
					'name'    => 'Team Nationality',
					'id'      => $prefix . 'race_team_nationality',
					'type'    => 'text',
			) );

					// text
			$cmb_results->add_field( array(
					'name' => 'GC Time',
					'desc'	=> 'GC time for this stage',
					'id' => $prefix . 'racer_gc_time',
					'type' => 'text',
			) );

			$cmb_results->add_field( array(
					'name' => 'Overall GC Time',
					'desc'	=> 'overall time if available',
					'id' => $prefix . 'racer_overall_time',
					'type' => 'text',
			) );

			$cmb_results->add_field( array(
					'name' => 'Sprint Points',
					'desc'	=> 'Sprint points for this stage if earned',
					'id' => $prefix . 'racer_sprint_points',
					'type' => 'text',
			) );

			$cmb_results->add_field( array(
					'name' => 'Total Sprint Points',
					'desc'	=> 'Total Sprint points',
					'id' => $prefix . 'racer_sprint_total',
					'type' => 'text',
			) );

			$cmb_results->add_field( array(
					'name' => 'Climber Points',
					'desc'	=> 'Climber points for this stage if earned',
					'id' => $prefix . 'racer_climber_points',
					'type' => 'text',
			) );

			$cmb_results->add_field( array(
					'name' => 'Total Climber Points',
					'desc'	=> 'Total Climber points',
					'id' => $prefix . 'racer_climber_total',
					'type' => 'text',
			) );

			$cmb_results->add_field( array(
					'name' => 'Youth Time',
					'desc'	=> 'Youth time for this stage if qualified',
					'id' => $prefix . 'racer_youth_time',
					'type' => 'text',
			) );

			$cmb_results->add_field( array(
					'name' => 'Youth Time',
					'desc'	=> 'Youth time for this stage if qualified',
					'id' => $prefix . 'youth_overall',
					'type' => 'text',
			) );

			$cmb_results->add_field( array(
					'name' => 'Team Time',
					'desc'	=> 'Team time for this stage',
					'id' => $prefix . 'team_time',
					'type' => 'text',
			) );

			$cmb_results->add_field( array(
					'name' => 'Team Position',
					'desc'	=> 'Team finish place',
					'id' => $prefix . 'team_finish',
					'type' => 'text',
			) );

			if(!is_admin()){
				return;
			}
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}

	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {

		//lets try to keep this clean and add each metabox individually
		add_action('cmb2_admin_init', array( $this, 'vn_race_results_metaboxes') );

	}

}
