<?php
/**
 * Custom Metaboxes | Race Stages
 */


class VeloNews_Metadata_Stages {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	//Stage Overview
	public function add_stages_metaboxes() {
		// Start with an underscore to hide fields from custom fields list
		$prefix = '_vn_stages_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'            => 'stage-description',
			'title'         => __( 'Stage Information', 'vn' ),
			'object_types'  => array( 'stage', ), // Post type
			'context'       => 'normal',
			'priority'      => 'high',
			'show_names'    => true // Show field names on the left
		) );

		// select option
		$cmb->add_field( array(
			'name'	=> 'Stage Icon',
			'desc'	=> 'Choose the icon',
			'id'	=> $prefix . 'stage_icon',
			'type'	=> 'select',
				'show_option_none'	=> false,
				'default'			=> 'Hill',
				'options'			=> array(
					'hill'			=> __( 'Rolling (Hills)', 'cmb2' ),
					'time-trial'	=> __( 'Time Trial', 'cmb2' ),
					'flat'			=> __( 'Flat', 'cmb2' ),
					'peaks'			=> __( 'Mountain (Peaks)', 'cmb2' ),
				),
		) );
		//checkbox
		$cmb->add_field( array(
			'name'    => 'Team Time Trial?',
			'id'      => $prefix . 'stage_ttt',
			'type'    => 'checkbox',
			'desc'	  => 'Check for Team Time Trial Stages',
			'options_cb' => 'get_updated_stages',
		) );
		//text
		$cmb->add_field( array(
			'name'    => 'Location',
			'id'      => $prefix . 'stage_location',
			'type'    => 'text',
			'options_cb' => 'get_updated_stages',
		) );
		// text
		$cmb->add_field( array(
			'name'    => 'Distance',
			'id'      => $prefix . 'stage_distance',
			'type'    => 'text',
		) );
		// text
		$cmb->add_field( array(
			'name'    => 'Stage Number',
			'id'      => $prefix . 'stage_number',
			'type'    => 'text',
		) );
		$cmb->add_field( array(
			'name'	=>	'Stage Detail Title',
			'id'	=>	$prefix . 'detail_title',
			'type'	=>	'text',
			'desc'	=>	'Title for stage detail tab'
		) );
		// wizzy
		$cmb->add_field( array(
			'name'    => 'Stage Description',
			'id'      => $prefix . 'about_txt',
			'type'    => 'wysiwyg',
		) );
		// date picker
		$cmb->add_field( array(
			'name'    => 'Stage Date',
			'id'      => $prefix . 'date',
			'time_format' => 'M-d-Y',
			'type'    => 'text_date',
		) );
	}


	//Stage Results
	public function vn_stages_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_vn_stages_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'            => 'stage',
			'title'         => __( 'Stage Results', 'vn' ),
			'object_types'  => array( 'stage', ), // Post type
			'context'       => 'normal',
			'priority'      => 'high',
			'show_names'    => true // Show field names on the left
		) );
		// text
		$cmb->add_field( array(
			'name'	=> 'Descriptive Title',
			'id'	=> 'descriptive_title',
			'desc'	=> 'type a title that will show up in the hot stories widget and in the modularized areas',
			'type'	=> 'text',
		) );	// text
		$cmb->add_field( array(
			'name'    => 'Results title',
			'desc'    => 'Title for results tab',
			'id'      => $prefix . 'stage_results_title',
			'type'    => 'text',
		) );
		// wizzy
		$cmb->add_field( array(
			'name'    => 'Results write up',
			'desc'    => 'Tell us what happened',
			'id'      => $prefix . 'stage_results',
			'type'    => 'wysiwyg',
		) );

		// Gallery
		// wizzy
		$cmb->add_field( array(
			'name'    => 'Gallery',
			'desc'    => 'Use the gallery shortcode',
			'id'      => $prefix . 'stage_photos',
			'type'    => 'wysiwyg',
		) );

	}



	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {

		//lets try to keep this clean and add each metabox individually
		add_action( 'cmb2_admin_init', array( $this, 'add_stages_metaboxes' ) );
		add_action( 'cmb2_admin_init', array( $this, 'vn_stages_metaboxes' ) );


	}
}

