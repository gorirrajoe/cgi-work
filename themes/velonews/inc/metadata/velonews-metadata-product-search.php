<?php
/**
 * Custom Metaboxes | Product Search Template
 */
class VeloNews_Metadata_ProductSearch {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function vn_bike_search_metaboxes() {

			// Start with an underscore to hide fields from custom fields list
			$prefix = '_vn_';

			/**
			 * Initiate the metabox
			 */
			$cmb = new_cmb2_box( array(
				'id'            => 'product-search',
				'title'         => __( 'Product Search Options', 'vn' ),
				'object_types'  => array( 'page' ), // Post type
				'show_on'       => array( 'key' => 'page-template', 'value' => array(
					'page-templates/apparel-search.php'
				) ),
				'context'       => 'normal',
				'priority'      => 'high',
				'show_names'    => true // Show field names on the left
			) );

			// text_small
			$cmb->add_field( array(
					'name'    => '2 (Featured) Across Category',
					'desc'    => 'Enter the category ID for 2 across (featured), which will appear under the 3-column product grid',
					'id'      => $prefix . '2special_category',
					'type'    => 'text_small'
			) );
			// text
			$cmb->add_field( array(
					'name'    => '4 Across Title',
					'desc'    => 'Title for 4 recent posts (top)',
					'id'      => $prefix . '4top_title',
					'type'    => 'text',
			) );
			// text_small
			$cmb->add_field( array(
					'name'    => '4 Across Category',
					'desc'    => 'Enter the category ID for 4 across (top)',
					'id'      => $prefix . '4top_category',
					'type'    => 'text_small'
			) );
			// text_small
			$cmb->add_field( array(
					'name'    => '2 Featured Articles',
					'desc'    => '2 Post IDs separated by a comma',
					'id'      => $prefix . 'featured_articles',
					'type'    => 'text_small'
			) );
			// text
			$cmb->add_field( array(
					'name'    => '5 Across Title',
					'desc'    => 'Title for 4 recent posts (top)',
					'id'      => $prefix . '5_title',
					'type'    => 'text',
			) );
			// text_small
			$cmb->add_field( array(
					'name'    => '5 Across Category',
					'desc'    => 'Enter the category id for 5 across',
					'id'      => $prefix . '5_category',
					'type'    => 'text_small'
			) );
			// text
			$cmb->add_field( array(
					'name'    => '2x2 Title',
					'desc'    => 'Title for 2x2 recent posts',
					'id'      => $prefix . '2by2_title',
					'type'    => 'text',
			) );
			// text_small
			$cmb->add_field( array(
					'name'    => '2x2 Category',
					'desc'    => 'Enter the category ID for 2x2',
					'id'      => $prefix . '2by2_category',
					'type'    => 'text_small'
			) );
			// text
			$cmb->add_field( array(
					'name'    => '4 Across Bottom Title',
					'desc'    => 'Title for bottom 4 recent posts',
					'id'      => $prefix . '4bottom_title',
					'type'    => 'text',
			) );
			// text_small
			$cmb->add_field( array(
					'name'    => '4 Across Bottom Category',
					'desc'    => 'Enter the category id for bottom 4 across',
					'id'      => $prefix . '4bottom_category',
					'type'    => 'text_small'
			) );


	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'vn_bike_search_metaboxes' ) );
	}
}
