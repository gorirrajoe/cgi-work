<?php
/**
* Custom Metaboxes | FAQ Page
*/
class VeloNews_Metadata_FAQ {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function vn_faq_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_vn_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'            => 'faqpage',
			'title'         => __( 'FAQ Page Options', 'vn' ),
			'object_types'  => array( 'page' ), // Post type
			'context'       => 'normal',
			'priority'      => 'high',
			'show_on'       => array( 'key' => 'page-template', 'value' => 'page-templates/faq-template.php' ),
			'show_names'    => true // Show field names on the left
		) );

		// group
		$mag_questions = $cmb->add_field( array(
				'id'          => $prefix . 'mag_questions',
				'type'        => 'group',
				'description' => __( 'Magazine Questions', 'cmb2' ),
				// 'repeatable'  => false, // use false if you want non-repeatable group
				'options'     => array(
						'group_title'   => __( 'Magazine Question {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
						'add_button'    => __( 'Add Another Magazine Question', 'cmb2' ),
						'remove_button' => __( 'Remove Magazine Question', 'cmb2' ),
						'sortable'      => true, // beta
						// 'closed'     => true, // true to have the groups closed by default
				),
		) );
			$cmb->add_group_field( $mag_questions, array(
					'name' => 'Question',
					'id'   => 'question',
					'type' => 'text',
			) );
			$cmb->add_group_field( $mag_questions, array(
					'name'    => 'Answer',
					'id'      => 'answer',
					'type'    => 'textarea_small',
			) );
		// group
		$web_questions = $cmb->add_field( array(
				'id'          => $prefix . 'web_questions',
				'type'        => 'group',
				'description' => __( 'Website Questions', 'cmb2' ),
				// 'repeatable'  => false, // use false if you want non-repeatable group
				'options'     => array(
						'group_title'   => __( 'Website Question {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
						'add_button'    => __( 'Add Another Website Question', 'cmb2' ),
						'remove_button' => __( 'Remove Website Question', 'cmb2' ),
						'sortable'      => true, // beta
						// 'closed'     => true, // true to have the groups closed by default
				),
		) );
			$cmb->add_group_field( $web_questions, array(
					'name' => 'Question',
					'id'   => 'question',
					'type' => 'text',
			) );
			$cmb->add_group_field( $web_questions, array(
					'name'    => 'Answer',
					'id'      => 'answer',
					'type'    => 'textarea_small',
			) );
		// group
		$contact_questions = $cmb->add_field( array(
				'id'          => $prefix . 'contact_questions',
				'type'        => 'group',
				'description' => __( 'Contact Questions', 'cmb2' ),
				// 'repeatable'  => false, // use false if you want non-repeatable group
				'options'     => array(
						'group_title'   => __( 'Contact Question {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
						'add_button'    => __( 'Add Another Contact Question', 'cmb2' ),
						'remove_button' => __( 'Remove Contact Question', 'cmb2' ),
						'sortable'      => true, // beta
						// 'closed'     => true, // true to have the groups closed by default
				),
		) );
			$cmb->add_group_field( $contact_questions, array(
					'name' => 'Question',
					'id'   => 'question',
					'type' => 'text',
			) );
			$cmb->add_group_field( $contact_questions, array(
					'name'    => 'Answer',
					'id'      => 'answer',
					'type'    => 'textarea_small',
			) );
		// group
		$contribute_questions = $cmb->add_field( array(
				'id'          => $prefix . 'contribute_questions',
				'type'        => 'group',
				'description' => __( 'Contribute Questions', 'cmb2' ),
				// 'repeatable'  => false, // use false if you want non-repeatable group
				'options'     => array(
						'group_title'   => __( 'Contribute Question {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
						'add_button'    => __( 'Add Another Contribute  Question', 'cmb2' ),
						'remove_button' => __( 'Remove Contribute Question', 'cmb2' ),
						'sortable'      => true, // beta
						// 'closed'     => true, // true to have the groups closed by default
				),
		) );
			$cmb->add_group_field( $contribute_questions, array(
					'name' => 'Question',
					'id'   => 'question',
					'type' => 'text',
			) );
			$cmb->add_group_field( $contribute_questions, array(
					'name'    => 'Answer',
					'id'      => 'answer',
					'type'    => 'textarea_small',
			) );
		// group
		$ad_questions = $cmb->add_field( array(
				'id'          => $prefix . 'ad_questions',
				'type'        => 'group',
				'description' => __( 'Advertising Questions', 'cmb2' ),
				// 'repeatable'  => false, // use false if you want non-repeatable group
				'options'     => array(
						'group_title'   => __( 'Advertising Question {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
						'add_button'    => __( 'Add Another Advertising Question', 'cmb2' ),
						'remove_button' => __( 'Remove Advertising Question', 'cmb2' ),
						'sortable'      => true, // beta
						// 'closed'     => true, // true to have the groups closed by default
				),
		) );
			$cmb->add_group_field( $ad_questions, array(
					'name' => 'Question',
					'id'   => 'question',
					'type' => 'text',
			) );
			$cmb->add_group_field( $ad_questions, array(
					'name'    => 'Answer',
					'id'      => 'answer',
					'type'    => 'textarea_small',
			) );
		// group
		$job_questions = $cmb->add_field( array(
				'id'          => $prefix . 'job_questions',
				'type'        => 'group',
				'description' => __( 'Job Questions', 'cmb2' ),
				// 'repeatable'  => false, // use false if you want non-repeatable group
				'options'     => array(
						'group_title'   => __( 'Job Question {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
						'add_button'    => __( 'Add Another Job Question', 'cmb2' ),
						'remove_button' => __( 'Remove Job Question', 'cmb2' ),
						'sortable'      => true, // beta
						// 'closed'     => true, // true to have the groups closed by default
				),
		) );
			$cmb->add_group_field( $job_questions, array(
					'name' => 'Question',
					'id'   => 'question',
					'type' => 'text',
			) );
			$cmb->add_group_field( $job_questions, array(
					'name'    => 'Answer',
					'id'      => 'answer',
					'type'    => 'textarea_small',
			) );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'vn_faq_metaboxes' ) );
	}
}

