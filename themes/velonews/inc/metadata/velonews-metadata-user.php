<?php
/**
 * Custom Metaboxes | User
 */
class VeloNews_Metadata_User {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function vn_user_metaboxes() {

	    // Start with an underscore to hide fields from custom fields list
	    $prefix = '_vn_';

	    /**
	     * Initiate the metabox
	     */
	    $cmb_user = new_cmb2_box( array(
	      'id'               => $prefix . 'edit',
	      'title'            => __( 'User Profile Metabox', 'cmb2' ),
	      'object_types'     => array( 'user' ), // Tells CMB2 to use user_meta vs post_meta
	      'show_names'       => true,
	      'new_user_section' => 'add-new-user', // where form will show on new user page. 'add-existing-user' is only other valid option.
	    ) );
	    // title
	    $cmb_user->add_field( array(
	      'name'      => 'Extra Info',
	      'id'        => $prefix . 'extra_info',
	      'type'      => 'title',
	      'desc'      => 'enter full url. will affect author page',
	      'on_front'  => false,
	    ) );
	    // text_url
	    $cmb_user->add_field( array(
	        'name' => 'Twitter',
	        'id'   => $prefix . 'twitter',
	        'type' => 'text_url',
	    ) );
	    // text_url
	    $cmb_user->add_field( array(
	        'name' => 'Facebook',
	        'id'   => $prefix . 'facebook',
	        'type' => 'text_url',
	    ) );
	    // text_url
	    $cmb_user->add_field( array(
	        'name' => 'Instagram',
	        'id'   => $prefix . 'instagram',
	        'type' => 'text_url',
	    ) );
	    // text_url
	    $cmb_user->add_field( array(
	        'name' => 'Pinterest',
	        'id'   => $prefix . 'pinterest',
	        'type' => 'text_url',
	    ) );
	    // text_url
	    $cmb_user->add_field( array(
	        'name' => 'Google+',
	        'id'   => $prefix . 'google_plus',
	        'type' => 'text_url',
	    ) );
	    // file
	    $cmb_user->add_field( array(
	        'name'    => 'Avatar',
	        'desc'    => 'Upload an image or enter an URL.',
	        'id'      => $prefix . 'avatar',
	        'type'    => 'file',
	        // Optional:
	        'options' => array(
	            'url' => false, // Hide the text input for the url
	            'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
	        ),
	    ) );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'vn_user_metaboxes' ) );
	}
}

