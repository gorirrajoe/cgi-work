<?php
/**
* Custom Metaboxes | Single Posts
*/
class VeloNews_Metadata_Single {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function vn_single_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_vn_';

		/**
		* Initiate the metabox
		*/
		$cmb = new_cmb2_box( array(
			'id'           => 'single-posts',
			'title'        => __( 'Single Post Options', 'vn' ),
			'object_types' => array( 'post' ), // Post type
			'context'      => 'normal',
			'priority'     => 'high',
			'show_names'   => true // Show field names on the left
		) );

		// radio_inline
		$cmb->add_field( array(
			'name'          => 'Featured?',
			'desc'          => 'same function as stickies (home page marquee)',
			'id'            => '_featured_post',
			'type'          => 'radio_inline',
			'default'       => 'not_featured',
			'options'       => array(
				'featured'     => 'Featured',
				'not_featured' => 'Not Featured',
			),
		) );

		// text
		$cmb->add_field( array(
			'name' => 'Short Title',
			'desc' => 'used on home page',
			'id'   => 'short_title',
			'type' => 'text',
		) );

		// text
		$cmb->add_field( array(
			'name' => 'Guest Author',
			'desc' => 'overrides post author display',
			'id'   => 'guest_author',
			'type' => 'text',
		) );

		// file
		$cmb->add_field( array(
			'name'    => 'Presented by Sponsor - Image',
			'desc'    => 'Upload an image or enter an URL. max height of 25px',
			'id'      => $prefix . 'presentedby_logo',
			'type'    => 'file',
			'text'    => array(
				'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
			),
		) );
		// text
		$cmb->add_field( array(
			'name'    => 'Presented by Sponsor - Image Width',
			'desc'    => 'for AMP purposes, enter image width here (height of 25px is assumed)',
			'id'      => $prefix . 'presentedby_width',
			'type'    => 'text',
		) );
		// text
		$cmb->add_field( array(
			'name'    => 'Presented by Sponsor - Name',
			'desc'    => 'enter the name for the sponsor. used for the alt tag',
			'id'      => $prefix . 'presentedby_name',
			'type'    => 'text',
		) );
		$cmb->add_field( array(
			'name' => 'Presented by Sponsor - URL',
			'desc' => 'Link to brand/company landing page',
			'id'   => $prefix . 'presentedby_url',
			'type' => 'text_url',
		) );

		// checkbox
		$cmb->add_field( array(
			'name' => 'Prevent Featured Image on top?',
			'desc' => 'check ON to prevent featured image from displaying on top of post.',
			'id'   => 'prevent_featured_image',
			'type' => 'checkbox'
		) );
		// checkbox
		$cmb->add_field( array(
			'name' => 'Legacy List',
			'desc' => 'check ON to enable Legacy List',
			'id'   => 'legacy_list_status',
			'type' => 'checkbox'
		) );

		// checkbox
		$liveblog1 = $cmb->add_field( array(
			'name' => 'Liveblog?',
			'desc' => 'check ON if this is a liveblog post',
			'id'   => '_liveblog',
			'type' => 'checkbox'
		) );

		// text
		$liveblog2 = $cmb->add_field( array(
			'name' => 'Liveblog Tag',
			'desc' => 'enter tag to use for liveblog',
			'id'   => '_liveblog_tag',
			'type' => 'text',
		) );


		// checkbox
		$sponsored1 = $cmb->add_field( array(
			'name' => 'Sponsored Content?',
			'desc' => 'check ON if post is sponsored content',
			'id'   => 'meta-checkbox',
			'type' => 'checkbox'
		) );
		$sponsored2 = $cmb->add_field( array(
			'name'    => 'Sponsored Content Name',
			'desc'    => 'Enter Sponsored Content Name',
			'id'      => 'meta-text',
			'type'    => 'text',
		) );


		$cmb->add_field( array(
			'name' => 'Show In Article Ad at bottom?',
			'desc' => 'check ON to show in article ad at bottom of post',
			'id'   => '_inarticle_at_bottom',
			'type' => 'checkbox'
		) );

		$cmb->add_field( array(
			'name' => 'Disable Newsletter Subscribe Widget (Sidebar)?',
			'desc' => 'check ON to disable the newsletter subscribe widget that appears in the sidebar',
			'id'   => '_diasble_newsletter_subscribe',
			'type' => 'checkbox'
		) );
		$cmb->add_field( array(
			'name' => 'Enable Author Bio at bottom of article?',
			'desc' => 'check ON to enable the author bio that appears at the bottom of this article',
			'id'   => '_enable_author_bio',
			'type' => 'checkbox'
		) );
		// text_small
		$legacy1 = $cmb->add_field( array(
			'name'    => 'YouTube (legacy)',
			'desc'    => 'this is a legacy field and does not need to be modified',
			'id'      => '_youtube_id',
			'type'    => 'text_small',

		) );
		// text_small
		$legacy2 = $cmb->add_field( array(
			'name'    => 'Brightcove (legacy)',
			'desc'    => 'this is a legacy field and does not need to be modified',
			'id'      => 'video_id',
			'type'    => 'text_small',

		) );

		// select
		$cmb->add_field( array(
			'name'				=> 'VeloPress Books',
			'desc'				=> 'must use the [velopress] shortcode with this selection',
			'id'				=> $prefix . 'velopress_book',
			'type'				=> 'select',
			'show_option_none'	=> true,
			'options_cb'		=> 'vp_book_options'
		) );


		// grids
		if( ! is_admin() || ! class_exists( '\Cmb2Grid\Grid\Cmb2Grid' ) ) {
			return;
		}

		$cmb2Grid = new \Cmb2Grid\Grid\Cmb2Grid($cmb);

		$row = $cmb2Grid->addRow();

		$row->addColumns( array(
			$legacy1,
			$legacy2,
		));

		$row = $cmb2Grid->addRow();

		$row->addColumns( array(
			$sponsored1,
			$sponsored2,
		));

		$row = $cmb2Grid->addRow();

		$row->addColumns( array(
			$liveblog1,
			$liveblog2,
		));


		/**
		 * get list of books from velopress
		 * @param  object  $field  field object
		 * @return array           array of books + their id's
		 */
		function vp_book_options( $field ) {

			$json		= file_get_contents( 'http://www.velopress.com/wp-json/velopress/books' );
			$data		= json_decode( $json, true );


			/**
			 * since key/values are opposite of what we want, flip em!
			 */
			$data_flip	= array_flip( $data );

			return $data_flip;

		}

	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'vn_single_metaboxes' ) );
	}
}
