<?php
/**
 * Custom Metaboxes | Longform Page Templates
 */
class VeloNews_Metadata_Longform {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function vn_longform_metaboxes() {

			// Start with an underscore to hide fields from custom fields list
			$prefix = '_vn_';

			/**
			 * Initiate the metabox
			 */
			$cmb = new_cmb2_box( array(
				'id'            => 'longform',
				'title'         => __( 'Longform Post Options', 'vn' ),
				'object_types'  => array( 'page' ), // Post type
				'show_on'       => array( 'key' => 'page-template', 'value' => 'page-templates/longform.php' ),
				'context'       => 'normal',
				'priority'      => 'high',
				'show_names'    => true // Show field names on the left
			) );

			$cmb->add_field( array(
					'name'    => 'Alternative Header Image',
					'desc'    => 'upload an image if you don\'t want to use the post thumbnail',
					'id'      => $prefix . 'alt_header_img',
					'type'    => 'file',
					'text'    => array(
							'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
					),
			) );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'vn_longform_metaboxes' ) );
	}
}

