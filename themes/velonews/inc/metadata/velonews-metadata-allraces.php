<?php
/**
* Custom Metaboxes | All Races
*/
class VeloNews_Metadata_AllRaces {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}
	public function vn_allraces_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_vn_';

		/**
		* Initiate the metabox
		*/
		$cmb = new_cmb2_box( array(
			'id'           => 'allraces-template',
			'title'        => __( 'All Races Template Options', 'vn' ),
			'object_types' => array( 'all-races' ), // Post type
			'context'      => 'normal',
			'priority'     => 'high',
			'show_names'   => true // Show field names on the left
		) );


		// text_date_timestamp
		$cmb->add_field( array(
			'name'		=> 'Date Start',
			'id'		=> $prefix . 'race_date_start',
			'type'		=> 'text_date_timestamp',
			'desc'		=> 'mm/dd/yyyy',
			'column'	=> true
		) );

		// text_date_timestamp
		$cmb->add_field( array(
			'name' => 'Date End',
			'id'   => $prefix . 'race_date_end',
			'type' => 'text_date_timestamp',
			'desc' => 'mm/dd/yyyy'
		) );

		// text_url
		$cmb->add_field( array(
			'name' => __( 'Content URL', 'cmb2' ),
			'id'   => $prefix . 'race_url',
			'type' => 'text_url',
		) );

		// select
		$cmb->add_field( array(
			'name'				=> 'Type',
			'desc'				=> 'Select an option',
			'id'				=> $prefix . 'race_type',
			'type'				=> 'select',
			'show_option_none'	=> true,
			'default'			=> 'custom',
			'options'			=> array(
				'mens_road'		=> __( 'Mens Road', 'cmb2' ),
				'womens_road'	=> __( 'Womens Road', 'cmb2' ),
				'cyclocross'	=> __( 'Cyclocross', 'cmb2' ),
			),
			'column'			=> true
		) );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {

		//lets try to keep this clean and add each metabox individually
		add_action( 'cmb2_admin_init', array( $this, 'vn_allraces_metaboxes' ) );
	}
}
