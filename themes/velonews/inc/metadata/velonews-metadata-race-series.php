<?php
/**
* Custom Metaboxes | Race Series
* Single post for importing results file
*/

class VeloNews_Metadata_Race_Series {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}

	public function vn_race_series_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_vn_race_';

		/**
		* Initiate the metabox
		*/
		$cmb = new_cmb2_box( array(
			'id'           => 'race-event-information',
			'title'        => __( 'Race Events', 'vn' ),
			'object_types' => array( 'races_event', ), // Post type
			'context'      => 'normal',
			'priority'     => 'high',
			'show_names'   => true, // Show field names on the left
			'closed'       => false,
		) );
		// text
		$cmb->add_field( array(
			'name'	=> 'Descriptive Title',
			'id'	=> 'descriptive_title',
			'desc'	=> 'type a title that will show up in the hot stories widget and in the modularized areas',
			'type'	=> 'text',
		) );
		$cmb->add_field( array(
			'name' => 'Has Stages',
			'desc' => 'Check for multistage races',
			'id'   => $prefix . 'multistage',
			'type' => 'checkbox'
		) );


		// wizzy
		$cmb->add_field( array(
			'name' => 'Overview',
			'desc' => 'Course Description and Overview',
			'id'   => $prefix . 'description',
			'type' => 'wysiwyg',
		) );

		// wizzy
		$cmb->add_field( array(
			'name' => 'Start List',
			'desc' => 'Start List for the Race',
			'id'   => $prefix . 'start_list',
			'type' => 'wysiwyg',
		) );

		// wizzy
		$cmb->add_field( array(
			'name' => 'Race Report / Results',
			'id'   => $prefix . 'report',
			'type' => 'wysiwyg',
			'desc' => 'Use for single day races'
		) );


	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}

	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {

		//lets try to keep this clean and add each metabox individually
		add_action('cmb2_admin_init', array( $this, 'vn_race_series_metaboxes') );

	}
}
