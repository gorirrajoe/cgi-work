<?php
/**
 * Custom Metaboxes | Legacy List
 */
class VeloNews_Metadata_LegacyList {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function vn_legacylist_metaboxes() {

	    /**
	     * Initiate the metabox
	     */
	    $cmb = new_cmb2_box( array(
	      'id'            => 'legacy-list',
	      'title'         => __( 'Legacy Lists Options', 'vn' ),
	      'object_types'  => array( 'post' ), // Post type
	      'show_on_cb'    => 'cmb_show_when_checked', // function should return a bool value
	      'context'       => 'normal',
	      'priority'      => 'high',
	      'show_names'    => true // Show field names on the left
	    ) );
			// file
			$cmb->add_field( array(
					'name'    => 'Franchise Image',
					'desc'    => 'Upload an image or enter an URL.',
					'id'      => '_franchise_image',
					'type'    => 'file',
					// Optional:
					'options' => array(
							'url' => false, // Hide the text input for the url
							'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
					),
			) );
			// file_list
			$cmb->add_field( array(
					'name' => 'Slide Items',
					'desc' => '',
					'id'   => '_slide_items',
					'type' => 'file_list',
					'preview_size' => array( 150, 150 ),
			) );
			$cmb->add_field( array(
					'name'    => 'Mid-Slider - Ad Image',
					'desc'    => 'Upload an image or enter an URL.',
					'id'      => '_mid_slider_ad_img',
					'type'    => 'file',
					// Optional:
					'options' => array(
							'url' => false, // Hide the text input for the url
							'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
					),
			) );
			// text_url
			$cmb->add_field( array(
					'name'    => 'Mid-Slider Ad Link: ',
					'desc'    => 'Enter URL you want the ad image to link to.',
					'id'      => '_mid_slider_ad_link',
					'type'    => 'text_url',
			) );
			// file
			$cmb->add_field( array(
					'name'    => 'End of Slider - Ad Image',
					'desc'    => 'Upload an image or enter an URL. This will show up in the middle of the list',
					'id'      => '_end_of_slider_ad_img',
					'type'    => 'file',
					// Optional:
					'options' => array(
							'url' => false, // Hide the text input for the url
							'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
					),
			) );
			// text_url
			$cmb->add_field( array(
					'name'    => 'End of Slider Ad Link: ',
					'desc'    => 'Enter URL you want the last ad image to link to.',
					'id'      => '_end_of_slider_ad_url',
					'type'    => 'text_url',
			) );

		/**
		 * Only display a metabox if "legacy list" is checked
		 * @param  object $cmb CMB2 object
		 * @return bool        True/false whether to show the metabox
		 */
		function cmb_show_when_checked( $cmb ) {
			$status = get_post_meta( $cmb->object_id(), 'legacy_list_status', 1 );

			// Only show if status is 'external'
			return 'on' === $status;
		}
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'vn_legacylist_metaboxes' ) );
	}
}
