<?php
/**
 * Custom Metaboxes | Gift Guide Single Post Template
 */
class VeloNews_Metadata_GiftGuide_Single {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function vn_giftguide_single_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_vn_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'			=> 'giftguide-single',
			'title'			=> __( 'Gift Guide Options', 'vn' ),
			'object_types'	=> array( 'post', 'apparel-reviews', 'bike-reviews' ), // Post type
			'context'		=> 'normal',
			'priority'		=> 'high',
			'show_names'	=> true // Show field names on the left
		) );

		$cmb->add_field( array(
			'id'	=> $prefix . 'gg_author_id',
			'name'	=> 'Gift Guide - Reviewer ID',
			'desc'	=> 'enter id of author to show on page',
			'type'	=> 'text',
		) );

		// select
		$cmb->add_field( array(
			'name'		=> 'Gift Guide - Product Rating',
			'id'		=> $prefix . 'gg_product_rating',
			'type'		=> 'select',
			'options'	=> array(
				'none'	=> '-off-',
				'0'		=> '0 stars',
				'0.5'	=> '0.5 star',
				'1'		=> '1 star',
				'1.5'	=> '1.5 stars',
				'2'		=> '2 stars',
				'2.5'	=> '2.5 stars',
				'3'		=> '3 stars',
				'3.5'	=> '3.5 stars',
				'4'		=> '4 stars',
				'4.5'	=> '4.5 stars',
				'5'		=> '5 stars',
			),
		) );

		$cmb->add_field( array(
			'id'	=> $prefix . 'gg_buy_url',
			'name'	=> 'Gift Guide - Buy URL',
			'desc'	=> 'enter url where reader can purchase product',
			'type'	=> 'text_url',
		) );

	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'vn_giftguide_single_metaboxes' ) );
	}
}

