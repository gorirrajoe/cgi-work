<?php
/**
 * Custom Metaboxes | Gift Guide Page Template
 */
class VeloNews_Metadata_GiftGuide {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function vn_giftguide_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_vn_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'			=> 'giftguide-template',
			'title'			=> __( 'Gift Guide Options', 'vn' ),
			'object_types'	=> array( 'page' ), // Post type
			'show_on'		=> array( 'key' => 'page-template', 'value' => 'page-templates/gift-guide.php' ),
			'context'		=> 'normal',
			'priority'		=> 'high',
			'show_names'	=> true // Show field names on the left
		) );

		// $cmb->add_field( array(
		// 	'name'				=> 'Gift Guide Category',
		// 	'desc'				=> 'choose the active gift guide category',
		// 	'id'				=> $prefix . 'gift_guide_cat',
		// 	'taxonomy'			=> 'category', //Enter Taxonomy Slug
		// 	'type'				=> 'taxonomy_radio',
		// 	'sanitization_cb'	=> 'sanitize_text_field'
		// ) );
		$cmb->add_field( array(
		    'name'    => 'Gift Guide Category',
		    'desc'    => 'type in the name for the active gift guide category',
		    'id'      => $prefix . 'gift_guide_cat',
		    'type'    => 'text',
		) );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'vn_giftguide_metaboxes' ) );
	}
}

