<?php

class VeloNews_MegaMenu {

	static $instance = false;

	private $transient = 'velonews-menu-primary';

	protected $location	= 'primary';

	public function __construct() {
		$this->_add_actions();
	}

	/**
	 * Do Save Post
	 *
	 * Determines whether or not to re-generate the navigation markup when
	 * "publish_post" is fired.
	 *
	 * @param int $post_id ID of the updated post
	 * @param WP_Post $post WP_Post object containing the current post
	 */
	public function do_save_post( $post_id = null, $post = null, $update = null ) {

		$post_types =
			array(
				'apparel-reviews',
				'bike-reviews',
				'post'
			);

		// if this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
			return;
		}

		// make sure the post_type is "post"
		if ( isset( $_POST['post_type'] ) && !in_array( $_POST['post_type'], $post_types )  ) {
			return;
		}

		// make sure the post_status is "publish"
		if ( isset( $_POST['post_status'] ) && 'publish' != $_POST['post_status'] ) {
			return;
		}

		/**
		 * Instead of regenerating the nav menu each time a post is updated,
		 * assert that the post has a category that would put it in the nav menu
		 * before regenerating the nav.
		 */
		if ( isset( $_POST['post_type'] ) && 'post' === $_POST['post_type'] ) {

			// get the post categories
			$post_categories = get_the_category( $post_id );
			$cats = array();

			if ( ! empty( $post_categories ) ) {

				foreach ( $post_categories as $post_category ) {
					$cats[] = $post_category->term_id;
				}

			}

			// get the navigation menu categories
			$nav_categories = $this->_get_megamenu_categories();

			// loop through navigation menu categories
			if ( !empty( $cats ) && !empty( $nav_categories ) ) {

				foreach ( $nav_categories as $nav_category ) {

					foreach ( $nav_category as $category ) {

						/**
						 * If the current nav category matches any of the post
						 * categories, re-generate the navigation markup.
						 */
						if ( isset( $category->term_id ) && in_array( $category->term_id, $cats ) ) {

							delete_transient( $this->transient );
							break;

						}

					}

				}

			}

		} else {

			/**
			 * For non-post post types (e.g. "bike-reviews") regenerate the nav
			 * menu immediately if the above criteria are met.
			 */
			delete_transient( $this->transient );

		}

	}

	/**
	 * Generate Nav Markup
	 *
	 * Generates the required mega-menu navigation markup based on the contents
	 * of the "primary" menu.
	 *
	 * @param array $categories Array containing category objects pertaining to the menu.
	 */
	public function generate_nav_markup( $menus = array() ) {

		if ( ! empty( $menus ) ) {

			$posts = array();

			foreach ( $menus as $categories ) {

				foreach ( $categories as $key => $category ) {

					if ( isset( $category->term_id ) ) {

						$posts[$category->term_id] = $this->_get_latest_posts( $category );

					} else {

						$posts[$key] = $this->_get_specific_posts( $category );

					}

				}

			}

			return $this->_generate_megamenu_markup( $menus, $posts );

		}

	}

	/**
	 * Get MegaMenu Markup
	 *
	 *	Queries the database for the megamenu transient. If the transient doesn't
	 *	exist, or is empty, it attempts to trigger the regeneration process once
	 *	and returns the value of the transient.
	 *
	 * @return mixed Array containing all generated megamenus. False/empty if unsuccessful.
	 */
	public function get_megamenu_markup() {

		$categories = $this->_get_megamenu_categories();
		$markup 	= $this->generate_nav_markup( $categories );

		return $markup;
	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( !self::$instance )
			self::$instance = new self();

		return self::$instance;
	}

	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	private function _add_actions() {

		add_action( 'save_post', array( $this, 'do_save_post' ), 10, 3 );

	}

	/**
	 * Get Menu Category
	 *
	 * Queries the database for the category associated with the given menu item.
	 *
	 * @param WP_Post $post Post object containing menu item details
	 *
	 * @return mixed If successfuly, a WP_Term object containing the associated category; else false.
	 */
	private function _get_menu_category( $post = null ) {

		if ( isset( $post->object ) ) {

			// if ( 'page' == $post->object && false !== strpos( $post->url, 'all-races' ) ) {
			// 	$term = get_term_by( 'slug', 'all-races', 'category' );
			// } else {
				$term = get_term_by( 'id', $post->object_id, $post->object );
			// }

			// if the item's taxonomy is "Race Category" find the matching "category"
			if ( 'stagecat' == $post->object ) {

				$slug = ( isset( $term->slug ) ? str_replace( '-race', '', $term->slug ) : '' );

				return get_term_by( 'slug', $slug, 'category' );

			}

			return $term;
		}

		return false;
	}

	/**
	 * Get MegaMenu Categories
	 *
	 * Searches the "primary" navigation menu for all menu items that belong to
	 * a mega-menu and returns them as an array.
	 *
	 * if the item is a page OR if the item is a category AND has a "has-children" class,
	 * then it has custom children
	 *
	 * otherwise, it's a standard category and grab the most recent 4 posts from that cat
	 *
	 * @return array Array containing all mega-menu categories.
	 */
	private function _get_megamenu_categories() {

		$locations = get_nav_menu_locations();
		$categories = array();

		if ( isset( $locations[$this->location] ) ) {

			$items = wp_get_nav_menu_items( $locations[$this->location] );

			if ( ! empty( $items ) ) {

				$parents = array();

				foreach ( $items as $i => $item ) {

					if ( ! empty( $item->classes ) && in_array( 'mega-menu', $item->classes ) ) {

						$parents[] = $item->ID;

					} else if ( ! empty( $item->menu_item_parent ) && in_array( $item->menu_item_parent, $parents ) ) {

						if ( $item->object == 'page' || ( $item->object == 'category' && in_array( 'has-custom-children', $item->classes ) ) ) {
							$category = $this->_get_menu_children( $items, $i, $item->ID );

						} else {

							// get the related category object
							$category = $this->_get_menu_category($item);

						}

						if ( ! empty( $category ) ) {
							$categories[$item->menu_item_parent][$item->ID] = $category;
						}
					}

				}

			}

		}

		return $categories;
	}

	/**
	 * Get Latest Posts
	 *
	 *	Retrieves the latest four posts that are tagged with the given category.
	 *
	 * @param WP_Post $category WP_Post object containing category information.
	 *
	 * @return array Array of posts beloning to the given category.
	 */
	private function _get_latest_posts( $category = null ) {

		$posts = array();

		if ( ! empty( $category ) ) {

			$args =
				array(
					'post_type'					=> 'post',
					'post_status'				=> 'publish',
					'posts_per_page'			=> 4,
					'order'						=> 'DESC',
					'orderby'					=> 'post_date',
					'ignore_sticky_posts'		=> true,
					'no_found_rows'				=> true,
					'update_post_meta_cache'	=> false,
					'tax_query'					=> array(
						array(
							'taxonomy'	=> ( isset( $category->taxonomy ) ? $category->taxonomy : '' ),
							'field'		=> 'term_id',
							'terms'		=> ( isset( $category->term_id ) ? $category->term_id : '' ),
						)
					)
				);

			/**
			 * "Apparel Review" and "Bike Review" categories refer to their
			 * respective post types, so update the query accordingly.
			 *
			 * The "All Races" category refers to the "races_event" post type,
			 * so update that accordingly.
			 */
			if ( isset( $category->taxonomy ) ) {

				// if ( 'apparel-reviews-cat' == $category->taxonomy ) {

				// 	$args['post_type'] = 'apparel-reviews';
				// 	$args['tax_query'] = array();

				// } elseif ( 'bike-reviews-cat' == $category->taxonomy ) {

				// 	$args['post_type'] = 'bike-reviews';
				// 	$args['tax_query'] = array();

				// } elseif ( 'category' == $category->taxonomy && 'all-races' == $category->slug ) {

				// 	$args['post_type'] = 'races_event';
				// 	$args['tax_query'] = array();

				// }

			}

			$query = new WP_Query( $args );

			// The Loop
			if ( $query->have_posts() ) {

				while( $query->have_posts() ) {

					$query->the_post();

					$post_id = get_the_ID();


					if ( has_post_thumbnail( $post_id ) ) {

						$thumbnail_id	= get_post_thumbnail_id( $post_id );
						$thumbnail		= wp_get_attachment_image_src( $thumbnail_id, 'meganav-thumb' );
						$alt_text		= get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true );

					} else {

						$thumbnail_id	= '';
						$thumbnail		= '';
						$alt_text		= '';

					}

					$posts[] =
						array(
							'ID'		=> $post_id,
							'title'		=> get_the_title(),
							'permalink'	=> get_the_permalink(),
							'thumbnail'	=> ( isset( $thumbnail[0] ) ? $thumbnail[0] : '' ),
							'alt_text'	=> ( isset( $alt_text ) ? $alt_text : '' ),
						);

				}

			}

			wp_reset_postdata();

		}

		return $posts;
	}

	/**
	 * Generate Mega Menu Markup
	 *
	 * Loops through the given categories and posts and builds the necessary
	 * markup to generate the mega-menu.
	 *
	 * @param array $menus Array containing category objects pertaining to the menu.
	 * @param array  $posts Array containing posts belonging to the mega-menu.
	 *
	 * @return array Array containing fully-generated mega-menus.
	 */
	private function _generate_megamenu_markup( $menus = array(), $posts = array() ) {

		$navs = array();

		if ( ! empty( $posts ) ) {

			foreach ( $menus as $parent_id => $categories ) {

				foreach ( $categories as $menu_id => $category ) {

					$html = sprintf(
						'<ul id="mn-%s" class="mega-menu__menu" role="menu" aria-hidden="true">',
						$menu_id
					);

					$item_id = ( isset( $category->term_id ) ) ? $category->term_id : $menu_id;

					foreach ( $posts[$item_id] as $post ) {

						$html .= sprintf(
							'<li id="menu-item-%s" class="mega-menu__menu-item">',
							$post['ID']
						);

							$html .= sprintf( '<a href="%s">', $post['permalink'] ) ."\n";

								if ( ! empty( $post['thumbnail'] ) ) {

									$html .= sprintf(
										'<figure><img data-original="%s" alt="%s" class="lazyload"></figure>',
										$post['thumbnail'],
										$post['alt_text']
									);

								}

								$html .= sprintf( '<p>%s</p>', $post['title'] );

							$html .= '</a>' ."\n";

						$html .= '</li> <!-- .mega-menu__menu-item -->' ."\n";

					}

					$html .= '</ul> <!-- .mega-menu__menu -->' ."\n";

					$navs[$parent_id][] = $html;

				}

			}

		}

		return $navs;
	}

	/**
	 * Get Menu Children
	 *
	 * Does a forward lookup on the given menu object to determine which items
	 * have a parent/child relationship.
	 *
	 * @param  array $items Array containing menu items
	 * @param  int $index Current position inside the menu object
	 * @param  int $parent_id MySQL ID of parent element
	 *
	 * @return array Array containing the children of the current menu item.
	 */
	private function _get_menu_children( $items = array(), $index = null, $parent_id = null ) {

		$children	= array();
		$next		= $index + 1;

		for( $i = 0; $i < 4; $i++ ) {

			if ( isset( $items[$next] ) && ( $parent_id == $items[$next]->menu_item_parent ) ) {

				$children[] = $items[$next];

			}

			$next++;
		}

		return $children;
	}

	/**
	 * Get Specific Posts
	 *
	 * @param  array  $menu_items Array containing menu object to query against.
	 *
	 * @return array Array containing the retrieved WordPress Post objects.
	 */
	private function _get_specific_posts( $menu_items = array() ) {

		$post_ids	= array();
		$posts		= array();

		foreach ( $menu_items as $post ) {
			$post_ids[] = $post->object_id;
		}

		$args =
			array(
				'post__in'					=> $post_ids,
				'post_type'					=> 'any',
				'post_status'				=> 'publish',
				'posts_per_page'			=> 4,
				'orderby'					=> 'post__in',
				'ignore_sticky_posts'		=> true,
				'no_found_rows'				=> true,
				'update_post_meta_cache'	=> false
			);

		$query = new WP_Query( $args );

		// The Loop
		if ( $query->have_posts() ) {

			while( $query->have_posts() ) {

				$query->the_post();

				$post_id = get_the_ID();

				if ( has_post_thumbnail( $post_id ) ) {

					$thumbnail_id = get_post_thumbnail_id( $post_id );
					$thumbnail = wp_get_attachment_image_src( $thumbnail_id, 'meganav-thumb' );
					$alt_text = get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true );

				}

				if ( get_post_type() == 'all-races' ) {

					$allraces_link = get_post_meta( $post_id, '_vn_race_url', 1 );
					$posts[] =
						array(
							'ID'		=> $post_id,
							'title'		=> get_the_title(),
							'permalink'	=> $allraces_link,
							'thumbnail'	=> ( isset( $thumbnail[0] ) ? $thumbnail[0] : '' ),
							'alt_text'	=> ( isset( $alt_text ) ? $alt_text : '' ),
						);

				} else {

					$posts[] =
						array(
							'ID'		=> $post_id,
							'title'		=> get_the_title(),
							'permalink'	=> get_the_permalink(),
							'thumbnail'	=> ( isset( $thumbnail[0] ) ? $thumbnail[0] : '' ),
							'alt_text'	=> ( isset( $alt_text ) ? $alt_text : '' ),
						);
				}

			}

		} else {

			$posts = array();

		}

		wp_reset_postdata();

		return $posts;
	}

}
