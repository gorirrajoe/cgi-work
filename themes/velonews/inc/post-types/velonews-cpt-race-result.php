<?php
/**
 * Custom Post Types | Race Results
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 * @link http://themergency.com/generators/wordpress-custom-post-types/
 */

class VeloNews_CPT_Race_Result {

	static $instance = false;

	public function __construct() {

		$this->register_cpt_race_result();

	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}

	protected function register_cpt_race_result() {

	    $labels = array(
			'name'					=> _x( 'Race Result', 'race_result' ),
			'singular_name'			=> _x( 'Race Result', 'race_result' ),
			'add_new'				=> _x( 'Add New', 'race_result' ),
			'add_new_item'			=> _x( 'Add New Race Result', 'race_result' ),
			'edit_item'				=> _x( 'Edit Race Result', 'race_result' ),
			'new_item'				=> _x( 'New Race Result', 'race_result' ),
			'view_item'				=> _x( 'View Race Result', 'race_result' ),
			'search_items'			=> _x( 'Search Race Result', 'race_result' ),
			'not_found'				=> _x( 'No Race Result found', 'race_result' ),
			'not_found_in_trash'	=> _x( 'No Race Result found in Trash', 'race_result' ),
			'parent_item_colon'		=> _x( 'Parent Race Result:', 'race_result' ),
			'menu_name'				=> _x( 'Race Result', 'race_result' ),
	    );

	    $args = array(
			'labels'				=> $labels,
			'hierarchical'			=> true,
			'supports'				=> array( 'title', 'excerpt', 'category' ),
			'taxonomies'			=> array( 'race_result', 'post_tag' ),
			'public'				=> true,
			'show_ui'				=> true,
			'show_in_menu'			=> true,
			'menu_icon'				=> 'dashicons-nametag',
			'show_in_nav_menus'		=> false,
			'publicly_queryable'	=> true,
			'exclude_from_search'	=> true,
			'has_archive'			=> true,
			'query_var'				=> true,
			'can_export'			=> true,
			'rewrite'				=> true,
			'capability_type'		=> 'post'
	    );

	    register_post_type( 'race_result', $args );
	}

}
