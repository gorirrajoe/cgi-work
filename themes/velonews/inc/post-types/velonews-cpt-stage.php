<?php
/**
 * Custom Post Types | Race Stages
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 * @link http://themergency.com/generators/wordpress-custom-post-types/
 */

class VeloNews_CPT_Stage {

	static $instance	= false;

	public function __construct() {

		$this->register_cpt_stage();

	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	protected function register_cpt_stage() {

	    $labels = array(
	        'name'               => _x( 'Stages', 'stage' ),
	        'singular_name'      => _x( 'Stage', 'stage' ),
	        'add_new'            => _x( 'Add New', 'stage' ),
	        'add_new_item'       => _x( 'Add New Stage', 'stage' ),
	        'edit_item'          => _x( 'Edit Stage', 'stage' ),
	        'new_item'           => _x( 'New Stage', 'stage' ),
	        'view_item'          => _x( 'View Stage', 'stage' ),
	        'search_items'       => _x( 'Search Stages', 'stage' ),
	        'not_found'          => _x( 'No Stages found', 'stage' ),
	        'not_found_in_trash' => _x( 'No Stages found in Trash', 'stage' ),
	        'parent_item_colon'  => _x( 'Parent Race:', 'stage' ),
	        'menu_name'          => _x( 'Stages', 'stage' ),
	    );

	    $args = array(
	        'labels'               => $labels,
	        'hierarchical'         => false,
	        'supports'             => array( 'title', 'thumbnail', 'category', 'author'),
	        'taxonomies'           => array( 'stages', 'category'  ),
	        'public'               => true,
	        'show_ui'              => true,
	        'show_in_menu'         => true,
	        'menu_icon'            => 'dashicons-list-view',
	        'show_in_nav_menus'    => false,
	        'publicly_queryable'   => true,
	        'exclude_from_search'  => false,
	        'has_archive'          => false,
	        'query_var'            => true,
	        'can_export'           => true,
	        'rewrite'              => array('slug'=>'stages'),
	        'capability_type'      => 'post',
	        // 'register_meta_box_cb' => 'add_stages_metaboxes'
	    );

	    register_post_type( 'stage', $args );
	}

}

