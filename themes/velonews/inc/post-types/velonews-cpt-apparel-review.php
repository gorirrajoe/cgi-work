<?php
/**
 * Custom Post Types | Apparel Review
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 * @link http://themergency.com/generators/wordpress-custom-post-types/
 */
class VeloNews_CPT_Apparel_Review {

	static $instance	= false;

	public function __construct() {

		$this->vn_setup_apparel_review_cpt();

	}


	protected function vn_setup_apparel_review_cpt() {
		$labels = array(
			'name'               => _x( 'Apparel Review', 'post type general name' ),
			'singular_name'      => _x( 'Apparel Review', 'post type singular name' ),
			'add_new'            => _x( 'Add New', 'Apparel Review' ),
			'add_new_item'       => __( 'Add Apparel Review' ),
			'edit_item'          => __( 'Edit Apparel Review' ),
			'new_item'           => __( 'New Apparel Review' ),
			'all_items'          => __( 'All Apparel Reviews' ),
			'view_item'          => __( 'View Apparel Review' ),
			'search_items'       => __( 'Search Apparel Reviews' ),
			'not_found'          => __( 'No Apparel Reviews Found' ),
			'not_found_in_trash' => __( 'No Apparel Reviews Found in the Trash' ),
			'parent_item_colon'  => '',
			'menu_name'          => _x( 'Apparel Reviews', 'admin menu' )
		);
		$args = array(
			'labels'			=> $labels,
			'description'		=> 'Apparel Reviews',
			'public'			=> true,
			'menu_position'		=> 25,
			'supports'			=> array( 'author', 'title', 'thumbnail', 'category', 'excerpt' ),
			'has_archive'		=> true,
			'menu_icon'			=> 'dashicons-products',
			'show_in_nav_menus'	=> true,
			'query_var'			=> true,
			'taxonomies'		=> array( 'category', 'post_tag' ),
			'can_export'		=> true,
			'rewrite'			=> false,
			'capability_type'	=> 'post',
		);
		register_post_type( 'apparel-reviews', $args );


		/**
		 * Rewrite URL's for bike/apparel reviews so they read as close to regular posts as possible
		 */
		// add to our plugin init function
		global $wp_rewrite;
		$apparel_structure = '/apparel-review/%year%/%monthnum%/%apparel-reviews%_%post_id%';
		$wp_rewrite->add_rewrite_tag( "%apparel-reviews%", '([^/]+)', "apparel-reviews=" );
		$wp_rewrite->add_permastruct( 'apparel-reviews', $apparel_structure, false );


		// Add filter to plugin init function
		add_filter('post_type_link', 'apparelreviews_permalink', 10, 3);
		// Adapted from get_permalink function in wp-includes/link-template.php
		function apparelreviews_permalink( $permalink, $post_id, $leavename ) {
			$post = get_post( $post_id );
			$rewritecode = array(
				'%year%',
				'%monthnum%',
				'%day%',
				'%hour%',
				'%minute%',
				'%second%',
				$leavename? '' : '%postname%',
				'%post_id%',
				'%category%',
				'%author%',
				$leavename? '' : '%pagename%',
			);

			if ( '' != $permalink && !in_array( $post->post_status, array( 'draft', 'pending', 'auto-draft' ) ) ) {
				$unixtime = strtotime( $post->post_date );

				$category = '';
				if ( strpos( $permalink, '%category%' ) !== false ) {
					$cats = get_the_category( $post->ID );
					if ( $cats ) {
						usort( $cats, '_usort_terms_by_ID' ); // order by ID
						$category = $cats[0]->slug;
						if ( $parent = $cats[0]->parent )
							$category = get_category_parents( $parent, false, '/', true ) . $category;
					}
					// show default category in permalinks, without
					// having to assign it explicitly
					if ( empty( $category ) ) {
						$default_category = get_category( get_option( 'default_category' ) );
						$category = is_wp_error( $default_category ) ? '' : $default_category->slug;
					}
				}

				$author = '';
				if ( strpos( $permalink, '%author%' ) !== false ) {
					$authordata = get_userdata( $post->post_author );
					$author = $authordata->user_nicename;
				}

				$date = explode( " ",date( 'Y m d H i s', $unixtime ) );
				$rewritereplace =
				array(
					$date[0],
					$date[1],
					$date[2],
					$date[3],
					$date[4],
					$date[5],
					$post->post_name,
					$post->ID,
					$category,
					$author,
					$post->post_name,
				);
				$permalink = str_replace( $rewritecode, $rewritereplace, $permalink );
			} else { // if they're not using the fancy permalink option
			}
			return $permalink;
		}

	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}

}
