<?php
/**
 * Custom Post Types | Race Series
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 * @link http://themergency.com/generators/wordpress-custom-post-types/
 */

class VeloNews_CPT_Race_Series {

	static $instance	= false;

	public function __construct() {

		$this->register_cpt_race_series_event();

	}

	protected function register_cpt_race_series_event() {

	    $labels = array(
	        'name'               => _x( 'Races', 'races_event' ),
	        'singular_name'      => _x( 'Races', 'races_event' ),
	        'add_new'            => _x( 'Add New', 'races_event' ),
	        'add_new_item'       => _x( 'Add New Races', 'races_event' ),
	        'edit_item'          => _x( 'Edit Races', 'races_event' ),
	        'new_item'           => _x( 'New Races', 'races_event' ),
	        'view_item'          => _x( 'View Races', 'races_event' ),
	        'search_items'       => _x( 'Search Races', 'races_event' ),
	        'not_found'          => _x( 'No Races found', 'races_event' ),
	        'not_found_in_trash' => _x( 'No Races found in Trash', 'races_event' ),
	        'parent_item_colon'  => _x( 'Parent Races:', 'races_event' ),
	        'menu_name'          => _x( 'Races', 'races_event' ),
	    );

	    $args = array(
	        'labels'              => $labels,
	        'hierarchical'        => true,
	        'supports'            => array( 'title', 'excerpt', 'thumbnail', 'category', 'author' ),
	        'taxonomies'          => array( 'races_event', 'category'  ),
	        'public'              => true,
	        'show_ui'             => true,
	        'show_in_menu'        => true,
	        'menu_icon'           => 'dashicons-universal-access-alt',
	        'show_in_nav_menus'   => false,
	        'publicly_queryable'  => true,
	        'exclude_from_search' => false,
	        'has_archive'         => true,
	        'query_var'           => true,
	        'can_export'          => true,
	        'rewrite'             => true,
	        'capability_type'     => 'post'
	    );

	    register_post_type( 'races_event', $args );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}

}
