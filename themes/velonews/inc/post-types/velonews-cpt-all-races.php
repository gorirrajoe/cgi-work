<?php
/**
 * Custom Post Types | All Races
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 * @link http://themergency.com/generators/wordpress-custom-post-types/
 */

class VeloNews_CPT_All_Races {

	static $instance	= false;

	public function __construct() {

		$this->_register_cpt_allraces();

	}

	/**
	 * Register Meta Boxes
	 *
	 * Defines all the meta boxes with CMB2 used by this custom post type.
	 *
	 */
	protected function _register_cpt_allraces() {

		$labels = array(
			'name'					=> __( 'All Races', 'all-races' ),
			'singular_name'			=> __( 'Race', 'all-races' ),
			'add_new'				=> __( 'Add New Race', 'all-races' ),
			'add_new_item'			=> __( 'Add Race', 'all-races' ),
			'edit_item'				=> __( 'Edit Race', 'all-races' ),
			'new_item'				=> __( 'New Race', 'all-races' ),
			'view_item'				=> __( 'View Race', 'all-races' ),
			'search_items'			=> __( 'Search Races', 'all-races' ),
			'not_found'				=> __( 'No Races Found', 'all-races' ),
			'not_found_in_trash'	=> __( 'No Races Found in the Trash', 'all-races' ),
			'parent_item_colon'		=> __( 'Parent Race:', 'all-races' ),
			'menu_name'				=> __( 'All Races Template', 'all-races' ),
		);

		$args = array(
			'labels'				=> $labels,
			'supports'				=> array( 'thumbnail', 'title', 'revisions' ),
			'public'				=> true,
			// 'show_ui'				=> true,
			// 'show_in_menu'			=> true,
			'menu_icon'				=> 'dashicons-admin-site',
			'publicly_queryable'	=> true,
			// 'exclude_from_search'	=> true,
			'menu_position'			=> 25,
			'has_archive'			=> true,
			// 'show_in_nav_menus'		=> true,
			'query_var'				=> true,
			// 'taxonomies'			=> array( 'category' ),
			'can_export'			=> true,
			'rewrite'				=> false,
			'capability_type'		=> 'post',
		);

		register_post_type( 'all-races', $args );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


}
