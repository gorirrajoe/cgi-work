<?php
	/**
	 * Template Name: Instant Search for Algolia
	 */

	get_header();

	if ( isset( $_GET['s'] ) ) {
		$formvalue		= htmlentities( stripslashes( strip_tags( $_GET['s'] ) ), ENT_HTML5, 'UTF-8' );
		$search_text	= htmlentities( stripslashes( strip_tags( $_GET['s'] ) ), ENT_QUOTES | ENT_HTML5, 'UTF-8' );
		$title			= '<span class="search-results__label">Search Results For:</span>
						   <span class="search-results__term">' . $search_text . '</span>';
	} else {
		$formvalue		= '';
		$search_text	= '';
		$title			= '<span class="search-results__label">Search Results</span>';
	}

	// if there is no featured image, use default in theme settings
	// $default_image_id	= Triathlete_Theme_Settings::get_theme_option( '_tri_default_post_image_id' );
	// $default_thumb_array	= wp_get_attachment_image_src( $default_image_id );
?>

<section id="content" class="content archive template--site-search">
	<div class="container content__container">
		<header class="row">
			<div class="col-xs-12">
				<?php
					/**
					 * breadcrumb
					 */
					get_breadcrumb();
				?>
			</div>
			<div class="col-xs-12">
				<h1 class="content__title">Search <?php echo bloginfo( 'name' ); ?>:</h1>
			</div>
		</header>


		<div class="row">

			<section class="col-xs-12 col-sm-7 col-md-8 site-search">

				<div id="ais-wrapper">
					<main id="ais-main">
						<div id="algolia-search-box">
							<div id="algolia-stats"></div>
							<svg class="search-icon" width="25" height="25" viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg"><path d="M24.828 31.657a16.76 16.76 0 0 1-7.992 2.015C7.538 33.672 0 26.134 0 16.836 0 7.538 7.538 0 16.836 0c9.298 0 16.836 7.538 16.836 16.836 0 3.22-.905 6.23-2.475 8.79.288.18.56.395.81.645l5.985 5.986A4.54 4.54 0 0 1 38 38.673a4.535 4.535 0 0 1-6.417-.007l-5.986-5.986a4.545 4.545 0 0 1-.77-1.023zm-7.992-4.046c5.95 0 10.775-4.823 10.775-10.774 0-5.95-4.823-10.775-10.774-10.775-5.95 0-10.775 4.825-10.775 10.776 0 5.95 4.825 10.775 10.776 10.775z" fill-rule="evenodd"></path></svg>
						</div>
						<div id="algolia-hits"></div>
						<footer class="content__footer">
							<div id="algolia-pagination" class="content__pagination"></div>
						</footer>
					</main>
				</div>

				<script type="text/html" id="tmpl-instantsearch-hit">

					<article id="post-{{{ data.post_id }}}" <?php post_class( array( 'article', 'article_type_archive-result' ) ); ?>>

						<# if ( data.images['newsletter-top'] ) { #>
						<div class="article__thumbnail__wrap">
							<a href="{{ data.permalink }}" title="{{ data.post_title }}">
								<img src="{{ data.images['newsletter-top'].url }}" alt="{{ data.post_title }}" title="{{ data.post_title }}" itemprop="image" class="article__thumbnail" />
							</a>
						</div>
						<# } #>

						<div class="article__body__wrap">
							<h3 class="article__title" itemprop="name headline">
								<a href="{{ data.permalink }}" class="article__permalink">{{{ data._highlightResult.post_title.value }}}</a>
							</h3>
							<p class="article__header__byline">
								<# if ( data.post_author ) { #>
								<span class="article__header__author">By <a href="<?php echo site_url('/author'); ?>/{{{ data.post_author.user_login }}}"><strong>{{{ data.post_author.display_name }}}</strong></a></span>
								<# } #>
								<span class="article__header__published">Published <strong>{{{ data.post_date_formatted }}}</time></strong></span>
							</p>
							<p class="article__excerpt">
								<#

								var excerpt_display = '';

								if ( data._highlightResult.post_excerpt && data._highlightResult.post_excerpt.matchedWords.length > 0 ) {
									excerpt_display	= data._highlightResult.post_excerpt.value;
								} else if( data._highlightResult.content && data._highlightResult.content.matchedWords.length > 0 ) {
									excerpt_display	= data._snippetResult.content.value;
								} else {
									excerpt_display	= data._snippetResult.post_excerpt.value;
								}

								#>
								{{{ excerpt_display }}}
							</p>
							<p class="article__meta">
								<# if ( data.taxonomies.category ) {

									for ( var i = 0; i < data.taxonomies.category.length; i++ ) { #>
									<span class="article_category_{{{ data.taxonomies.category[i].toLowerCase() }}}"><span class="article__category">{{{ data.taxonomies.category[i] }}}</span></span>
									<# }

								} #>
							</p>
						</div>

					</article>
				</script>

				<script type="text/javascript">
					jQuery(function() {
						if ( jQuery('#algolia-search-box').length > 0 ) {

							if ( algolia.indices.searchable_posts === undefined && jQuery('.admin-bar').length > 0 ) {
								alert('It looks like you haven\'t indexed the searchable posts index. Please head to the Indexing page of the Algolia Search plugin and index it.');
							}

							/* Instantiate instantsearch.js */
							var search = instantsearch({
								appId: algolia.application_id,
								apiKey: algolia.search_api_key,
								indexName: algolia.indices.searchable_posts.name,
								urlSync: {
									mapping: {'q': 's'},
									trackedParameters: ['query']
								},
								searchParameters: {
									facetingAfterDistinct: true
								},
								searchFunction: function(helper) {
									// don't run a query if the search is less than three characters long (want to save queries with algolia)
									if ( helper.state.query.length < 3 ) {
										return;
									}
									/* helper does a setPage(0) on almost every method call */
									/* see https://github.com/algolia/algoliasearch-helper-js/blob/7d9917135d4192bfbba1827fd9fbcfef61b8dd69/src/algoliasearch.helper.js#L645 */
									/* and https://github.com/algolia/algoliasearch-helper-js/issues/121 */
									var savedPage = helper.state.page;
									if ( search.helper.state.query === '' ) {
										search.helper.setQueryParameter('distinct', false);
										search.helper.setQueryParameter('filters', 'record_index=0');
									} else {
										search.helper.setQueryParameter('distinct', true);
										search.helper.setQueryParameter('filters', '');
									}
									search.helper.setPage(savedPage);
									helper.search();
								}
							});

							/* Search box widget */
							search.addWidget(
								instantsearch.widgets.searchBox({
									container: '#algolia-search-box',
									placeholder: 'Search for...',
									wrapInput: false
								})
							);

							/* Stats widget */
							search.addWidget(
								instantsearch.widgets.stats({
									container: '#algolia-stats'
								})
							);

							/* Hits widget */
							search.addWidget(
								instantsearch.widgets.hits({
									container: '#algolia-hits',
									hitsPerPage: 10,
									templates: {
										empty: 'No results were found for "<strong>{{query}}</strong>".',
										item: wp.template('instantsearch-hit')
									}
								})
							);

							/* Pagination widget */
							search.addWidget(
								instantsearch.widgets.pagination({
									cssClasses: {
										item: 'page-numbers',
										root: 'page-links'
									},
									container: '#algolia-pagination',
									labels: {
										next: 'Older Posts &raquo;',
										previous: '&laquo; Newer Posts'
									},
									showFirstLast: false
								})
							);

							/* Start */
							search.start();

							jQuery('#algolia-search-box input').attr('type', 'search').select();
						}
					});
				</script>

			</section>

			<?php get_sidebar( 'right' ); ?>


		</div>

	</div>
</section>

<?php
	/**
	 * ad
	 */
?>
<section class="advert advert_xs_300x250 advert_sm_728x90 advert_location_bottom ">
	<div class="advert__wrap">
		<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom' ) : ''; ?>
	</div>
</section>


<?php get_footer(); ?>
