<?php
/**
 * The template for displaying stage single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package velonews
 */

$sponsorship = get_presented_by();

get_header(); ?>

<section id="content" class="content archive template--single-race<?php echo $sponsorship['sponsorclass']; ?>">

	<div class="container content__container">

		<?php while ( have_posts() ) : the_post();

			// Get the parent race ID and title
			$stage_cat      = wp_get_post_terms( $post->ID, 'stagecat' );
			$stage_cat      = ( isset( $stage_cat[0]->slug ) ? $stage_cat[0]->slug : '' );
			$stage_year     = wp_get_post_terms( $post->ID, 'stageyear' );
			$stage_year     = ( isset( $stage_year[0]->slug ) ? $stage_year[0]->slug : '' );
			$stage_cat_name = wp_get_post_terms( $post->ID, 'stagecat' );
			$stage_cat_name = ( isset( $stage_cat_name[0]->name ) ? $stage_cat_name[0]->name : '' );

			// get the post meta
			$meta = get_post_meta($post->ID);

			$stage_overview       = ( isset($meta['_vn_stages_about_txt'] ) ? $meta['_vn_stages_about_txt'][0] : '' );
			$stage_overview       = ( isset( $meta['_vn_stages_about_txt'] ) ? $meta['_vn_stages_about_txt'][0] : '' );
			$stage_overview_title = ( isset( $meta['_vn_stages_detail_title'] ) ? $meta['_vn_stages_detail_title'][0] : '' );
			$stage_number         = ( isset( $meta['_vn_stages_stage_number'] ) ? $meta['_vn_stages_stage_number'][0] : '' );
			$stage_location       = ( isset( $meta['_vn_stages_stage_location'] ) ? $meta['_vn_stages_stage_location'][0] : '' );
			$stage_results_title  = ( isset( $meta['_vn_stages_stage_results_title'] ) ? $meta['_vn_stages_stage_results_title'][0] : '' );
			$stage_results        = ( isset( $meta['_vn_stages_stage_results'] ) ? $meta['_vn_stages_stage_results'][0] : '' );
			$stage_date_original  = ( isset( $meta['_vn_stages_date'] ) ? $meta['_vn_stages_date'][0] : '' );
			$stage_gallery        = ( !empty( $meta['_vn_stages_stage_photos'] ) ? $meta['_vn_stages_stage_photos'][0] : 'Check back after the race.' );
			//$ttt 				  = ( $meta['_vn_stages_stage_ttt'][0] ) == "on" ? true : false;


			// stage results
			$race_id = $wpdb->get_results( 'SELECT pk_race_id from wp_races WHERE race_name = "' . $stage_cat_name . '" AND race_year ="' . $stage_year . '" ' );
			$race_id_num = ( isset( $race_id[0]->pk_race_id ) ? $race_id[0]->pk_race_id : '' );
			$stage_results_info = 'SELECT wp_riders.*, wp_stage_results.* FROM wp_stage_results INNER JOIN wp_riders WHERE wp_riders.pk_rider_id = wp_stage_results.fk_rider_id  AND fk_race_id = "' . $race_id_num . '" AND stage_number ="' . $stage_number . '" ORDER BY LPAD(rider_finish_place, 20, "0") ASC';
			$results_stage_times = $wpdb->get_results( $stage_results_info );

			// $ttt_query = 'SELECT wp_team.*, wp_stage_results.* FROM wp_stage_results INNER JOIN wp_team WHERE wp_team.pk_team_id = wp_stage_results.fk_team_id  AND fk_race_id = "' . $race_id_num . '" AND stage_number ="' . $stage_number . '" ORDER BY LPAD(team_stage_finish, 20, "0") ASC';
			// $ttt_results = $wpdb->get_results( $ttt_query );

			$stage_gc_info = 'SELECT s.*, r.* FROM wp_stage_results r, wp_riders s WHERE s.pk_rider_id = r.fk_rider_id  AND r.fk_race_id = "'.$race_id_num.'" AND r.stage_number ="'.$stage_number.'" ORDER BY CASE WHEN r.overall_position >= 1 THEN 0 ELSE 1 END, r.overall_position';
			$results_gc_times = $wpdb->get_results( $stage_gc_info );

			$sprint_query = 'SELECT wp_riders.*, wp_stage_results.* FROM wp_stage_results INNER JOIN wp_riders WHERE wp_riders.pk_rider_id = wp_stage_results.fk_rider_id  AND fk_race_id = "' . $race_id_num . '" AND stage_number ="' . $stage_number . '" ORDER BY LPAD( rider_sprint_total, 20,"0") DESC';
			$sprint_results = $wpdb->get_results( $sprint_query );

			$mountain_query = 'SELECT wp_riders.*, wp_stage_results.* FROM wp_stage_results INNER JOIN wp_riders WHERE wp_riders.pk_rider_id = wp_stage_results.fk_rider_id  AND fk_race_id = "' . $race_id_num . '" AND stage_number ="' . $stage_number . '" ORDER BY LPAD( rider_climber_total, 20, "0") DESC';
			$mountain_results = $wpdb->get_results( $mountain_query );

			wp_reset_postdata();

			// create previous and next posts for stages
			$postlist_args = array(
				'post_type'					=> 'stage',
				'posts_per_page'			=> -1,
				'orderby'					=> 'meta_value_num',
				'meta_key'					=> '_vn_stages_stage_number',
				'order'						=> 'ASC',
				'no_found_rows'				=> true,
				'update_post_meta_cache'	=> false,
				'fields'					=> 'ids',
				'tax_query'					=> array(
					array(
						'taxonomy'	=> 'stagecat',
						'field'		=> 'slug',
						'terms'		=> $stage_cat
					),
					array(
						'taxonomy'	=> 'stageyear',
						'field'		=> 'slug',
						'terms'		=> $stage_year
					)
				)
			);
			$postlist = new WP_Query( $postlist_args );

			// get ids of posts retrieved from get_posts
			$ids = $postlist->posts;

			// get and echo previous and next post in the same taxonomy
			$thisindex	= array_search( $post->ID, $ids );
			$previd		= ( isset( $ids[$thisindex-1] ) ? $ids[$thisindex-1] : '' );
			$nextid		= ( isset( $ids[$thisindex+1] ) ? $ids[$thisindex+1] : '' );

			$parent_categories = '"' . $stage_year . ',' . $stage_cat . '"';
			$stage_date = !empty( $stage_date_original ) ? date("Y-m-d", strtotime( $stage_date_original[0] ) ) : '' ;
			$parent_cat_slug = str_replace( '-race', '', $stage_cat );
			$parent_cat = get_category_by_slug( $parent_cat_slug );
			$parent_cat_name = $parent_cat->name;
			$parent_cat_url = get_category_link( $parent_cat );
			$secondary_cat_slug = $stage_year . '-' . $parent_cat_slug;
			if( term_exists( $secondary_cat_slug, 'category' ) ) {
				$secondary_cat = get_category_by_slug( $secondary_cat_slug );
				$secondary_cat_name = $secondary_cat->name;
				$secondary_cat_url = get_category_link( $secondary_cat );
			}


			// fetch overall race post ID
			$parent_post = new WP_Query( array(
				'post_type'					=>'races_event',
				'posts_per_page'			=> 1,
				'no_found_rows'				=> true,
				'update_post_meta_cache'	=> false,
				'tax_query'					=> array(
					array(
						'taxonomy'	=> 'stagecat',
						'field'		=> 'slug',
						'terms'		=> $stage_cat
					),
					array(
						'taxonomy'	=> 'stageyear',
						'field'		=> 'slug',
						'terms'		=> $stage_year
					),
				)
			) );

			$parent_post_id	= ( isset( $parent_post->posts[0]->ID ) ? $parent_post->posts[0]->ID : '' );
			$parent_title	= $parent_post->posts[0]->post_title != '' ? $parent_post->posts[0]->post_title : get_the_title();
			$active_tab		= ( isset( $_GET['tab'] ) ? $_GET['tab'] : '' );

			if( ! empty( $_GET['tab'] ) ) {
				$active_tab = $_GET['tab'];
			} else if( !empty( $meta['_vn_stages_stage_results'] ) ) {
				$active_tab = 'stage-report';
			} else {
				$active_tab = 'stage-details';
			}

			echo '
				<header class="row">
					<div class="col-xs-12">
						<ol class="article__bread-crumb">
							<li><a href="' . site_url('/') . '">Home</a></li>
							 »
							<li><a href="' . $parent_cat_url . '">' . $parent_cat_name . '</a></li>
							 » ';
							if( term_exists( $secondary_cat_slug, 'category' ) ){
								echo '<li><a href="' . $secondary_cat_url . '">' . $secondary_cat_name . '</a></li>
								 » ';
							}

						echo'<li>'. get_the_title() . '</li>
						</ol>
					</div>
					<div class="col-xs-12">
						<h1 class="content__title">' . $parent_title . '</h1>
						' . $sponsorship['content'] . '
					</div>
				</header>


				<div class="row">
					<section class="col-xs-12 col-sm-7 col-md-8 race">
						<article class="article">
							<header class="article__header">
								<h2 class="article__header__title">STAGE ' . $stage_number . ': ' . $stage_location .'</h2>
							</header>

							<div class="race__nav__wrap">
								<ul id="article-nav" class="nav nav-pills race__nav" role="tablist">
									<li class="' . ( 'stage-details' == $active_tab ? 'active' : '' )  . '" role="presentation">
										<a href="#stage-details" id="stage-details-toggle" aria-controls="stage-details" role="tab" data-toggle="tab">STAGE DETAILS</a>
									</li>
									<li class="' . ( 'stage-report' == $active_tab ? 'active' : '' )  . '" role="presentation">
										<a href="#stage-report" id="stage-report-toggle" aria-controls="stage-report" role="tab" data-toggle="tab">STAGE REPORT / RESULTS</a>
									</li>
									<li class="' . ( 'stage-photos' == $active_tab ? 'active' : '' )  . '" role="presentation">
										<a href="#stage-photos" id="stage-photos-toggle" aria-controls="stage-photos" role="tab" data-toggle="tab">STAGE PHOTOS</a>
									</li>
								</ul>

								<nav class="race__pagination">';
									if ( !empty( $previd ) ) {
					   					echo '<a rel="prev" href="' . get_permalink( $previd ) . '" target="_self">« Previous Stage</a>' ;
					   				}
				   					if ( !empty($previd) && !empty( $nextid )) {
				   						echo '|';
				   					}
					   				if ( !empty( $nextid ) ) {
					   					echo '<a rel="next" href="' . get_permalink( $nextid ) . '" target="_self">Next Stage »</a>';
					   				}
						   		echo '</nav>
							</div>';

							// tab content
							// stage overview
							echo '<div class="tab-content">

								<div id="stage-details" class="tab-pane race__pane ' . ( 'stage-details' == $active_tab ? 'active' : '' ) . '" role="tabpanel">

									<h2>' . $stage_overview_title . '</h2>
									' . apply_filters( 'the_content', $stage_overview ) . '

								</div>

								<div id="stage-report" class="tab-pane race__pane ' . ( 'stage-report' == $active_tab ? 'active' : '' ) . '" role="tabpanel">

									<h3 class="race__heading">' . $stage_results_title . '</h3>
									' . apply_filters('the_content', $stage_results );


									//ad widget before table, after contents
									?>

									<div class="advert advert_xs_300x250 advert_md_970x250 advert_location_inline">
										<div class="advert__wrap">
											<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'middle-archive' ) : ''; ?>
										</div>
									</div><!-- ad unit -->

									<div class="race-table">
										<div class="race-table__wrap">
											<ul id="race-table-nav" class="nav nav-pills race-table__nav" role="tablist">
												<?php //if( $ttt ){
													//if team time trial, change tab name
													//?>
													<!-- <li role="presentation" class="active">
													<a href="#stage" aria-controls="stage" role="tab" data-toggle="tab">TTT</a> -->
												</li>
													<?php // } else{
														//otherwise, just show stage
														?>
												<li role="presentation" class="active">
													<a href="#stage" aria-controls="stage" role="tab" data-toggle="tab">STAGE</a>
												</li>
												<?php //}?>
												<li role="presentation">
													<a href="#gc" aria-controls="gc" role="tab" data-toggle="tab">GC</a>
												</li>
												<li role="presentation">
													<a href="#points" aria-controls="points" role="tab" data-toggle="tab">SPRINT</a>
												</li>
												<li role="presentation">
													<a href="#mountains" aria-controls="mountains" role="tab" data-toggle="tab">MOUNTAIN</a>
												</li>
												<li role="presentation">
													<a href="#youth" aria-controls="youth" role="tab" data-toggle="tab">YOUTH</a>
												</li>
												<li role="presentation">
													<a href="#teams" aria-controls="teams" role="tab" data-toggle="tab">TEAMS</a>
												</li>
											</ul>
											<div class="tab-content race-table__content"></div>

											<?php
											/**
											 * stage tab
											 */
											echo '<div id="stage" class="tab-pane race-table__pane active" role="tabpanel">
												<table class="race-table__table">
													<thead>
														<tr>
															<th>RANK</th>';
															// if( $ttt ){
															// 	//no rider name or country for ttt
															// 	echo '<th>TEAM</th>';
															// }
															// 	else {
																echo'<th>RIDER (COUNTRY) TEAM</th>';
															// }
													echo'	<th>TIME</th>
														</tr>
													</thead>
													<tbody>';

														$check_back = '<tr><td colspan="3">Results will be published once race is underway.</td></tr>';
														// if( $ttt ){
															//for team time trial, show team results
															// if( !empty( $ttt_results ) ) {
															// 	foreach ($ttt_results as $key => $value) {
															// // 		//first place show time as entered
															// 		if( $value->team_stage_finish == 1 ){
															// 			$time_to_show = $value->team_stage_time;
															// 		} elseif( $value->team_stage_finish < 1 ) {
															// 			//dns dq etc, show that
															// 			$time_to_show = $value->team_stage_time;
															// 		} else {
															// 			//otherwise show the time with the + off the lead
															// 			$time_to_show = '+'. $value->team_stage_time;
															// 		// }

															// 		echo '<tr>
															// 			<td>' . $value->team_stage_finish . '</td>
															// 			<td>' . $value->rider_team . '</td>
															// 			<td>' . $time_to_show . '</td>
															// 		</tr>';
															// 	}
															//  // } else {
															// 	echo $check_back;
															// }
													//	}else{
															if( !empty( $results_stage_times ) ) {
																foreach ($results_stage_times as $key => $value) {

																	if ($value->rider_finish_place == 1 ) {
																		//winner shows the full time
																		$time_to_show = $value->rider_time;
																	} elseif ( $value->rider_finish_place < 1){
																		//dnf dns dq etc show that
																		$time_to_show = $value->rider_time;
																	} else {
																		//show the gap time with + in front
																		$time_to_show = '+'. $value->rider_time;
																	}

																	echo '<tr>
																		<td>' . $value->rider_finish_place . '</td>
																		<td>' . $value->rider_name . ' (' . $value->rider_country . ') ' . $value->rider_team . '</td>
																		<td>' . $time_to_show . '</td>
																	</tr>';

																}
															} else {
																echo $check_back;
															// }
														}
													echo '</tbody>
												</table>
											</div>';


											/**
											 * gc tab
											 */
											echo '<div id="gc" class="tab-pane race-table__pane" role="tabpanel">
												<table class="race-table__table">
													<thead>
														<tr>
															<th>RANK</th>
															<th>RIDER (COUNTRY) TEAM</th>
															<th>OVERALL TIME</th>
														</tr>
													</thead>
													<tbody>';

														$check_back = '<tr><td colspan="4">Results will be published once race is underway.</td></tr>';

														if( !empty($results_gc_times[0]->overall_position) ) {


															foreach ($results_gc_times as $key => $value) {
																if($value->rider_time == '') {
																	//leave this guy out
																} else {

																	if ($value->overall_position == 1 ) {
																		$time_to_show = $value->overall_time;
																	} else {
																		$time_to_show = '+' . $value->rider_time;
																	}


																	if ($value->overall_position >= 1) {
																		$finish_position = $value->overall_position;
																	} else {
																		$finish_position= $value->rider_finish_place;
																	}
																	echo '<tr>
																		<td>' . $finish_position . '</td>
																		<td>' . $value->rider_name . ' (' . $value->rider_country . ') ' . $value->rider_team . '</td>
																		<td>' . $value->overall_time . '</td>
																	</tr>';
																}
															}

														} else {
															echo $check_back;
														}
													echo '</tbody>
												</table>
											</div>';


											/**
											 * sprint tab
											 */
											echo '<div id="points" class="tab-pane race-table__pane" role="tabpanel">
												<table class="race-table__table">
													<thead>
														<tr>
															<th>RANK</th>
															<th>RIDER</th>
															<th>STAGE POINTS</th>
															<th>OVERALL POINTS</th>
														</tr>
													</thead>
													<tbody>';

														$has_sprint_points = false;

														if( !empty( $sprint_results ) ) {
															$sprint_rank = 1;
															foreach ($sprint_results as $key => $value) {
																if ( !empty( $value->rider_sprint_total ) ) {
																	echo '<tr>
																		<td>' . $sprint_rank . '</td>
																		<td>' . $value->rider_name . '</td>
																		<td>' . $value->rider_sprint_points . '</td>
																		<td>' . $value->rider_sprint_total . '</td>
																	</tr>';

																	$has_sprint_points = true;
																	$sprint_rank++;
																}
															}
															if ( $has_sprint_points == false ){
																echo '<tr><td colspan="4">Not Applicable</td></tr>';}
														} else {
															echo $check_back;
														}
													echo '</tbody>
												</table>
											</div>';


											/**
											 * mountain tab
											 */
											echo '<div id="mountains" class="tab-pane race-table__pane" role="tabpanel">
												<table class="race-table__table">
													<thead>
														<tr>
															<th>RANK</th>
															<th>RIDER</th>
															<th>STAGE POINTS</th>
															<th>OVERALL POINTS</th>
														</tr>
													</thead>
													<tbody>';

														$has_mountain_points = false;

														if( !empty( $mountain_results ) ) {
															$mountain_rank = 1;

															foreach ( $mountain_results as $key => $value ) {
																if ( !empty( $value->rider_climber_total ) ) {
																	echo '<tr>
																		<td>' . $mountain_rank . '</td>
																		<td>' . $value->rider_name . '</td>
																		<td>' . $value->rider_mountain_points . '</td>
																		<td>' . $value->rider_climber_total . '</td>
																	</tr>';

																	$has_mountain_points === true;
																	$mountain_rank++;
																}
															}

														} else if ( $has_mountain_points == false ) {
															echo '<tr><td colspan="4">Not Applicable</td></tr>';
														} else {
															echo $check_back;
														}
													echo '</tbody>
												</table>
											</div>';


											/**
											 * youth tab
											 */
											echo '<div id="youth" class="tab-pane race-table__pane" role="tabpanel">
												<table class="race-table__table">
													<thead>
														<tr>
															<th>RANK</th>
															<th>RIDER</th>
															<th>STAGE TIME</th>
															<th>OVERALL TIME</th>
														</tr>
													</thead>
													<tbody>';

														$has_youth_time = false;

														if( !empty( $results_gc_times ) ) {
															$i = 1;
															foreach ( $results_gc_times as $key => $value ) {
																// var_dump($value);
																if( $value->youth_overall_time != NULL ) {
																	//check for individual time
																	if( $value->rider_finish_place == 1 ) {
																		//if rider is first for the stage, show clock time
																		$youth_show_time = ( $value->rider_youth_time != NULL ) ? $value->rider_youth_time : $value->rider_nice_time;
																	} else {
																		//otherwise use + to show gap time
																		$youth_show_time = ( $value->rider_youth_time != NULL ) ? $value->rider_youth_time : '+' . $value->rider_time;
																	}
																		//has individual times
																	if ( $i == 1 ) {
																		$has_youth_time = true;
																		echo '<tr>
																			<td>' . $i . '</td>
																			<td>' . $value->rider_name . '</td>
																			<td>' . $youth_show_time . '</td>
																			<td>' . $value->youth_overall_time . '</td>
																		</tr>';
																		$i++;
																	} else {
																		echo '<tr>
																			<td>' . $i . '</td>
																			<td>' . $value->rider_name . '</td>
																			<td>' . $youth_show_time . '</td>
																			<td>' . $value->youth_overall_time . '</td>
																		</tr>';
																		$i++;
																	}
																}
															}
															if ( $has_youth_time == false ) {
																echo '<tr><td colspan="4">Not Applicable</td><td></td><td></td></tr>';
															}
														} else {
															echo $check_back;
														}
													echo '</tbody>
												</table>

											</div>';


											/**
											 * teams tab
											 */
											echo '<div id="teams" class="tab-pane race-table__pane" role="tabpanel">

												<table class="race-table__table">
													<thead>
														<tr>
															<th>POSITION</th>
															<th>TEAM</th>
															<th>TIME</th>

														</tr>
													</thead>
													<tbody>';

														if( !empty( $results_gc_times ) ) {

															$stage_teams	= [];
															$results		= [];
															$full_team_list	= [];

															foreach ( $results_gc_times as $key => $value ) {

																if ( !empty( $value->rider_team  ) && ( $value->team_time !== '' ) && ( $value->team_time !== NULL ) ) {

																	if ( in_array( $value->rider_team , $stage_teams ) ){
																	} else {	//create unique list of team names
																		array_push( $stage_teams, $value->rider_team  );
																	}

																} else {

																}

															}

															foreach ($results_gc_times as $key => $value) {

																foreach ($stage_teams as $skey => $svalue) {

																	if( ( $svalue == $value->rider_team ) ){
																		array_push( $results, $key );//get one key for each team name from results

																		unset( $stage_teams[$skey] );//remove team name from for each loop
																	}

																}

															}

															foreach ( $results as $key => $value ) {

																//create array for team table using keys and results info
																array_push( $full_team_list, array( strtoupper( $results_gc_times[$value]->rider_team ), $results_gc_times[$value]->team_time, $results_gc_times[$value]->team_rank ) );

															}

															function cmpRanks( $a, $b ) {
																  return ( $a[2] - $b[2] );
															}

															usort( $full_team_list, 'cmpRanks' );

															foreach ( $full_team_list as $key => $value ) {
																if( $value[2] != NULL ){
																	echo '<tr>
																		<td>' . $value[2] . '</td>
																		<td>' . $value[0] . '</td>';
																		if ( $value[2]== '1') {
																			echo '<td>' . $value[1] . '</td>';
																		} else {
																			echo '<td>+' . $value[1] . '</td>';
																		}
																	echo '</tr>';

																}

															}
														} else {
															echo $check_back;
														}
													echo '</tbody>
												</table>

											</div>
										</div>
									</div>
								</div>


								<div id="stage-photos" class="tab-pane race__pane ' . ( 'stage-photos' == $active_tab ? 'active' : '' ) . '" role="tabpanel">
									' . apply_filters( 'the_content', $stage_gallery ) . '
								</div>';

								echo '<nav class="race__pagination">';
									if ( !empty( $previd ) ) {
					   					echo '<a rel="prev" href="' . get_permalink( $previd ) . '" target="_self">« Previous Stage</a>';
					   				}
				   					if ( !empty( $previd ) && !empty( $nextid )){
				   						echo '|';
				   					}
					   				if ( !empty( $nextid ) ) {
					   					echo '<a rel="next" href="' . get_permalink( $nextid ) . '" target="_self">Next Stage »</a>';
					   				}
						   		echo '</nav>
						   	</div>
						</article>
					</section>';

		endwhile; // End of the loop.
		get_sidebar( 'right' ); ?>
		</div>
	</div>
</section>

<?php echo get_outbrain(); ?>

<?php get_footer();
