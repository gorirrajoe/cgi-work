<?php get_header(); ?>

	<section id="content" class="content template--all-races">

		<div class="container content__container">

			<?php while ( have_posts() ) : the_post(); ?>

				<header class="row">

					<div class="col-xs-12">
						<h1 class="content__title">Races</h1>
					</div>

				</header>

				<div class="row">

					<article class="col-xs-12 col-sm-7 col-md-8 article">

						<header class="article__header">
							<h2 class="article__header__title">2016 Races</h2>
						</header>

						<section class="article__body race-table">

							<div class="race-table__wrap">

								<ul id="race-table-nav" class="nav nav-pills race-table__nav" role="tablist">
									<li role="presentation" class="active">
										<a href="#mens-road" aria-controls="mens-road" role="tab" data-toggle="tab" aria-expanded="true">MENS ROAD</a>
									</li>
									<li role="presentation" class="">
										<a href="#womens-road" aria-controls="womens-road" role="tab" data-toggle="tab" aria-expanded="false">WOMENS ROAD</a>
									</li>
									<li role="presentation" class="">
										<a href="#cyclocross" aria-controls="cyclocross" role="tab" data-toggle="tab" aria-expanded="false">CYCLOCROSS</a>
									</li>
								</ul>

								<?php
									$mens_road = array();
									$womens_road = array();
									$cyclocross = array();

									$args =
										array(
											'post_type' => 'races_event',
											'posts_per_page' => 200,
											'post_status' => 'publish',
											'orderby' => 'date',
											'order' => 'DESC',
											'no_found_rows' => true,
											'update_post_meta_cache' => false,
											'tax_query' => array(
												'relation' => 'AND',
												array(
													'taxonomy' => 'stagecat',
													'field'    => 'slug',
													'terms'    => array( 'mens-road', 'womens-road', 'cyclocross' ),
													'operator' => 'IN'
												),
												array(
													'taxonomy' => 'stageyear',
													'field'    => 'slug',
													'terms'    => date('Y'),
													'operator' => '='
												)
											)
										);

									$query = new WP_Query($args);

									if( $query->have_posts() ) {

										while( $query->have_posts() ) {

											$query->the_post();

											$race = array(
												'date' => get_the_date(),
												'title' => get_the_title(),
												'permalink' => get_permalink()
											);

											$terms = wp_get_post_terms( $post->ID, 'stagecat', array( 'fields' => 'slugs' ) );

											if( in_array('mens-road', $terms) ) {
												$mens_road[] = $race;
											} else if( in_array('womens-road', $terms) ) {
												$womens_road[] = $race;
											} else if( in_array('cyclocross', $terms) ) {
												$cyclocross[] = $race;
											}

										}

									}

									wp_reset_postdata();
								?>

								<div class="tab-content race-table__content">

									<div id="mens-road" class="tab-pane race-table__pane active" role="tabpanel">
										<table class="race-table__table">
											<thead>
												<tr>
													<th>DATE</th>
													<th>RACE</th>
													<th>CONTENT</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach( $mens_road as $race ): ?>
													<tr>
														<td><?php echo $race['date'] ?></td>
														<td><?php echo $race['title'] ?></td>
														<td><a href="<?php echo $race['permalink'] ?>" target="_self"><strong>News &amp; Reports</strong></a></td>
													</tr>
												<?php endforeach; ?>
											</tbody>
										</table>
									</div>

									<div id="womens-road" class="tab-pane race-table__pane" role="tabpanel">
										<table class="race-table__table">
											<thead>
												<tr>
													<th>DATE</th>
													<th>RACE</th>
													<th>CONTENT</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach( $womens_road as $race ): ?>
													<tr>
														<td><?php echo $race['date'] ?></td>
														<td><?php echo $race['title'] ?></td>
														<td><a href="<?php echo $race['permalink'] ?>" target="_self"><strong>News &amp; Reports</strong></a></td>
													</tr>
												<?php endforeach; ?>
											</tbody>
										</table>
									</div>

									<div id="cyclocross" class="tab-pane race-table__pane" role="tabpanel">
										<table class="race-table__table">
											<thead>
												<tr>
													<th>DATE</th>
													<th>RACE</th>
													<th>CONTENT</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach( $cyclocross as $race ): ?>
													<tr>
														<td><?php echo $race['date'] ?></td>
														<td><?php echo $race['title'] ?></td>
														<td><a href="<?php echo $race['permalink'] ?>" target="_self"><strong>News &amp; Reports</strong></a></td>
													</tr>
												<?php endforeach; ?>
											</tbody>
										</table>
									</div>

								</div>

							</div>

						</section>

					</article>

				<?php endwhile; // End of the loop. ?>

				<?php get_sidebar( 'right' ) ?>

			</div>

		</div>

	</section>

	<section class="advert advert_xs_300x250 advert_sm_728x90 advert_location_bottom ">

		<div class="advert__wrap">
			<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom' ) : ''; ?>
		</div>

	</section>

<?php get_footer(); ?>
