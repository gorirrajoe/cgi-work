<?php
	/**
	 * The template for displaying all Gift Guide category pages.
	 */
	get_header();
?>

<section id="content" class="content">

	<?php
		/**
		 * note: 2015 only had a blogroll setup so no special category template
		 */

		//get the current year
		$this_year = date("Y");
		//set up non empty array with the first year
		$gg_cat_years = array();

		//add to the array for all years including this one <=
		for ( $i = $this_year; $i >= 2017 ; $i--) {

			$gg_cat_years[] .= $i;
		}
		if( is_category( '2016-gift-guide' ) ) {
			get_template_part( 'template-parts/content', '2016-gift-guide-cat' );
		}
		foreach ($gg_cat_years as $key => $value) {
			if (is_category( $value . '-gift-guide' ) ) {
				get_template_part( 'template-parts/content', 'final-gift-guide-cat' );
			}
		}
				?>

</section>

<?php get_footer(); ?>
