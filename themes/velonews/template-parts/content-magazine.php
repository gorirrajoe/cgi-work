<?php
/**
 * Template part for displaying standard single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package velonews
 */
?>


<div class="col-xs-4">
	<p class="article__magazine-archive__item">
		<a href="<?php echo get_permalink(); ?>" title="Preview the <?php the_title(); ?>">
			<?php if ( has_post_thumbnail() ) {
				the_post_thumbnail( 'medium' );
			} ?>
			<br>
			<?php the_title(); ?>
		</a>
	</p>
</div>

