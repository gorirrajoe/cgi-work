	<?php
/**
 * Template part for displaying posts with old video embeds
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package velonews
 */
$sponsorship = get_presented_by();
?>


<article id="article" <?php post_class( array( 'col-xs-12', 'col-sm-7', 'col-md-8', 'article' ) ); ?>>

	<?php
		/**
		 * breadcrumb
		 */
		get_breadcrumb();
	?>

	<header class="article__header">

		<?php the_title( '<h1 class="article__header__title">', '</h1>' );

		if ( 'post' === get_post_type() ) {
			velonews_posted_on();
		} ?>
	</header>

	<?php

		echo $sponsorship['content'];

	?>

	<section class="article__body row">

		<?php
			/**
			 * social sharing
			 */
			get_social_sharing( 'side' );

			echo '<div id="article-right" class="col-xs-12 col-md-11">';

				get_social_sharing( 'top' );

				$yt_vid			= get_post_meta( get_the_ID(), '_youtube_id', true ) ? get_post_meta( get_the_ID(), '_youtube_id', true ) : '' ;
				$brightcove_id	= get_post_meta( get_the_ID(), 'video_id', true ) ? get_post_meta( get_the_ID(), 'video_id', true ) : '';

				if( $yt_vid != '' ) {
					if ( defined( 'INSTANT_ARTICLES_SLUG' ) && is_feed( INSTANT_ARTICLES_SLUG ) ) {

						echo sprintf(
							'<figure class="op-interactive">
								<iframe width="1280" height="720" src="https://www.youtube.com/embed/%s?rel=0" frameborder="0" allowfullscreen></iframe>
							</figure>',
							$yt_vid
						);

					} else {

						echo sprintf(
							'<div class="video-container">
								<iframe width="1280" height="720" src="https://www.youtube.com/embed/%s?rel=0" frameborder="0" allowfullscreen></iframe>
							</div>',
							$yt_vid
						);

					}

				} elseif( $brightcove_id != '' ) {
					if ( defined( 'INSTANT_ARTICLES_SLUG' ) && is_feed( INSTANT_ARTICLES_SLUG ) ) {

						echo sprintf(
							'<figure class="op-interactive">
								<iframe src="//players.brightcove.net/3655502813001/default_default/index.html?videoId=%1$d" width="1280" height="720" allowfullscreen></iframe>
							</figure>',
							$a['id']
						);

					} else {

						$adKw = class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::singleton()->generate_dfp_keywords() : ''; ?>

						<div class="video-container">
							<div style="display: block; position: relative; max-width: 100%;">
								<div style="padding-top: 56.25%;">
									<video id="myVideo"
									data-video-id="<?php echo trim( $brightcove_id ); ?>"
									data-account="3655502813001"
									data-player="r1oC9M1S"
									data-embed="default"
									class="video-js"
									controls style="width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;"></video>
								</div>
							</div>

							<script src="//players.brightcove.net/3655502813001/r1oC9M1S_default/index.min.js"></script>
							<?php if( cgi_bikes_get_option( 'preroll_on' ) == 'on' ): ?>
								<link href="//players.brightcove.net/videojs-ima3/2/videojs.ima3.min.css" rel="stylesheet">
								<script src="//players.brightcove.net/videojs-ima3/2/videojs.ima3.min.js"></script>
								<script type="text/javascript">
									var myPlayer = videojs('myVideo');
									myPlayer.ready(function(){
									    myPlayer=this;

									    myPlayer.ima3({
									        serverUrl:"https://pubads.g.doubleclick.net/gampad/ads?sz=720x480&iu=<?php echo $adKw; ?>&ciu_szs&impl=s&gdfp_req=1&env=vp&output=xml_vast2&unviewed_position_start=1&url=[referrer_url]&correlator=[timestamp]",
									        timeout:5000,
									        prerolltimeout:1000,
									        requestMode: 'onload',
									        debug:true,
									        adTechOrder:["html5", "flash"],
									        vpaidMode:"ENABLED"
									    });
									    myPlayer.one('loadedmetadata', function(){
									        // get a reference to the player
									        myPlayer = this;

									        myPlayer.on("ima3-ad-error",function(event){
									            console.log("ima3-ad-error: " + event);
									        });

									        myPlayer.on("adserror", function(event){
									            console.log("adsError Fired: " + event);
									        });
									     });
									});
								</script>
							<?php endif;?>
						</div>
					<?php }
				}

				the_content();

			echo '</div>';

		?>
	</section>


	<?php
		/**
		 * get author bio
		 */
		echo get_author_info();
	?>

</article><!-- #article-->
