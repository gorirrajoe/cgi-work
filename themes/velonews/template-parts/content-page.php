<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package velonews
 */
?>


<article id="article" <?php post_class( array( 'col-xs-12', 'col-sm-7', 'col-md-8', 'article' ) ); ?>>

	<?php
		/**
		 * breadcrumb
		 */
		get_breadcrumb();
	?>

	<header class="article__header">
		<?php the_title( '<h1 class="article__header__title">', '</h1>' ); ?>
	</header>


	<section class="article__body row">
		<?php
			/**
			 * social sharing
			 */
			get_social_sharing( 'side' );

			echo '<div id="article-right" class="col-xs-12 col-md-11">';

				get_social_sharing( 'top' );

				$content_string = apply_filters( 'the_content', get_the_content() );

				echo $content_string;

			echo '</div>';
		?>
	</section>

</article><!-- #article -->
