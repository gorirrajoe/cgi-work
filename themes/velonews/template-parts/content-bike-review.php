<?php
	/**
	 * Template part for displaying bike reviews.
	 *
	 * @package velonews
	 */

	$prefix			= '_vn_bike_review_';
	$post_classes	= array( 'col-xs-12', 'col-sm-7', 'col-md-8', 'article', 'article_type_bike-review' );

	/**
	 * set metadata to variables
	 */
	$bike_meta = get_meta( get_the_ID() );
	// print_r( $bike_meta );

	$type			= array_key_exists( $prefix . 'bike_type', $bike_meta ) && $bike_meta[$prefix . 'bike_type'] != '' ? explode( '::', $bike_meta[$prefix . 'bike_type'] ) : '';
	$type_key		= ( isset( $type[0] ) ? $type[0] : '' );
	$type_name		= ( isset( $type[1] ) ? $type[1] : '' );

	$msrp			= array_key_exists( $prefix . 'bike_msrp', $bike_meta ) && $bike_meta[$prefix . 'bike_msrp'] != '' ? number_format( $bike_meta[$prefix . 'bike_msrp'], 2 ) : '';
	$msrp_notes		= array_key_exists( $prefix . 'bike_msrp_notes', $bike_meta ) && $bike_meta[$prefix . 'bike_msrp_notes'] != '' ? $bike_meta[$prefix . 'bike_msrp_notes'] : '';

	$size			= array_key_exists( $prefix . 'bike_size', $bike_meta ) && $bike_meta[$prefix . 'bike_size'] != '' ? $bike_meta[$prefix . 'bike_size'] : '';
	$weight			= array_key_exists( $prefix . 'bike_weight', $bike_meta ) && $bike_meta[$prefix . 'bike_weight'] != '' ? $bike_meta[$prefix . 'bike_weight'] : '';
	$score			= array_key_exists( $prefix . 'bike_overall', $bike_meta ) && $bike_meta[$prefix . 'bike_overall'] != '' ? $bike_meta[$prefix . 'bike_overall'] : '';

	$rank			= get_bike_standing( get_the_ID(), $type_key );

	$pros			= array_key_exists( $prefix . 'bike_pros', $bike_meta ) && $bike_meta[$prefix . 'bike_pros'] != '' ? unserialize( $bike_meta[$prefix . 'bike_pros'] ) : '';
	$cons			= array_key_exists( $prefix . 'bike_cons', $bike_meta ) && $bike_meta[$prefix . 'bike_cons'] != '' ? unserialize( $bike_meta[$prefix . 'bike_cons'] ) : '';
	$stores			= array_key_exists( $prefix . 'stores', $bike_meta ) && $bike_meta[$prefix . 'stores'] != '' ? unserialize( $bike_meta[$prefix . 'stores'] ) : '';

	$cats		= get_the_category();
	$cat_array	= [];
	$bg_author	= array_key_exists( $prefix . 'review_author', $bike_meta ) && $bike_meta[$prefix . 'review_author'] != '' ? true : false;
	$in_guide	= false;

	//check if in gift guide or buyers guide after 2017, set in_guide true
	foreach( $cats as $key => $value ) {
		if( strpos( $value->slug, 'guide' ) != false ) {
			//get the year for the guide
			preg_match_all( '!\d+!', $value->slug, $guide_year );

			if( is_array( $guide_year ) ) {
				$guide_year  = $guide_year[0][0];
			}

			//start with 2017 gift guide and 2018 buyers guide
			if( ( $guide_year > 2016 && strpos( $value->slug, 'gift' ) != false ) || ( $guide_year > 2017 && strpos( $value->slug, 'buy' ) != false ) ) {

				$in_guide					= true;
				$cat_array[$value->slug]	= $value->name;
				$guide_slug					= current( array_keys( $cat_array ) );
				$guide_name					= $cat_array[$guide_slug];
				$guide_link					=  get_category_link( get_category_by_slug( $guide_slug ) );

			}
		}
	}


	if( $in_guide ) { ?>

		<div class="template__bg-final">
			<section id="guides">
				<div class="col-xs-12 bg__final_title">
					<h1><a href="<?php echo( $guide_link );?>"><?php echo $guide_name; ?></a></h1>
				</div>
				<div class="guide__menu">

					<?php
						wp_nav_menu( array(
							'theme_location' => $guide_slug,
							'menu_class' => 'bg__final_top_menu'
						) );

					?>

				</div>
			</section>
		</div>

	<?php }
?>


<article id="article" <?php post_class( $post_classes ); ?>>
	<header class="article__header">

		<ol class="article__bread-crumb">
			<li>
				<a href="<?php echo site_url() ?>" target="_self">Home</a>
			</li>
			<?php if( $in_guide ) {
				echo '&raquo;
					<li>
					<a href="'.site_url() . '/category/' . $guide_slug . '" target="_self">' . $guide_name .'</a>
					</li>' ;
			}?>
			&raquo;
			<li>
				<a href="<?php echo site_url() ?>/bike-reviews-rankings?resultspage=1&type[]=<?php echo $type_key ?>&price_range[]=all&gender[]=all&brand[]=all&product_search=" target="_self">
					<?php echo $type_name ?>
				</a>
			</li>
			&raquo;
			<li>
				<?php the_title(); ?>
			</li>
		</ol>

		<h1 class="article__header__title"><?php echo get_the_title(); ?></h1>

		<?php velonews_posted_on(); ?>

		<p class="article__header__rank">#<?php echo $rank; ?> in <a href="<?php echo site_url( '/bike-reviews-rankings?resultspage=1&type[]='. $type_key .'&price_range[]=all&brand[]=all&product_search=' ); ?>" target="_self"><?php echo $type_name; ?></a></p>

		<ul class="article__header__score">

			<li>SCORE <strong><?php echo $score; ?>/100</strong></li>

			<?php
				if( $size ) {
					echo "<li>SIZE <strong>{$size}</strong></li>";
				}

				if( $weight ) {
					echo "<li>WEIGHT <strong>{$weight}</strong></li>";
				}

				if( $msrp ) {
					echo "<li>MSRP <strong>$". $msrp ."</strong> {$msrp_notes}</li>";
				}
			?>

		</ul>

	</header>


	<section class="article__body article__review row">
		<?php
			/**
			 * social sharing
			 */
			get_social_sharing( 'side' );

			echo '<div id="article-right" class="col-xs-12 col-md-11">';

				get_social_sharing( 'top' );


				/**
				 * owl carousel gallery and ratings row
				 */

				$gallery_imgs	= array_key_exists( $prefix . 'bike_gallery', $bike_meta ) && $bike_meta[$prefix . 'bike_gallery'] != '' ? unserialize( $bike_meta[$prefix . 'bike_gallery'] ) : '';
				$html			= '';

				if( count( $gallery_imgs ) > 1 ) {

					$html = '<div id="gallery-1" class="gallery owl-carousel generic-carousel">';

						foreach( $gallery_imgs as $imgid => $imgurl ) {

							$img		= wp_get_attachment_image_src( $imgid, 'image-gallery' );
							$img_obj	= get_post( $imgid );

							$html .= '<figure class="gallery-item">';
								$html .= sprintf( '<a data-fancybox="gallery-1" href="%1$s" data-caption="%1$s"><img class="owl-lazy" data-src="%1$s"></a>', $img[0], $img[0], $img_obj->post_excerpt );
								$html .= sprintf( '<figcaption class="wp-caption-text gallery-caption">%1$s</figcaption>', $img_obj->post_excerpt );
							$html .= '</figure>';
						}

					$html .= '</div>';

				} else {

					$imgid	= get_post_thumbnail_id( get_the_ID() );
					$img	= wp_get_attachment_image_src( $imgid, 'image-gallery' );

					if( $img != '' ) {

						$img_obj	= get_post( $imgid );
						$img_large	= wp_get_attachment_image_src( $imgid, 'full' );

						$html = '<figure>';
							$html .= sprintf( '<a href="%s" data-fancybox data-caption="%s"><img alt="%s" src="%s"></a>', $img_large[0], $img_obj->post_excerpt, $img_obj->post_excerpt, $img[0] );
							$html .= sprintf( '<figcaption class="wp-caption-text gallery-caption">%s</figcaption>', $img_obj->post_excerpt );
						$html .= '</figure>';

					}

				}

				echo $html;

				/**
				 * ratings
				 */
				get_ratings( $type_key, $score );


				/**
				 * pros & cons & stores
				 */
				$pros	= ( $pros != '' ) ? array_filter( $pros ) : '';
				$cons	= ( $cons != '' ) ? array_filter( $cons ) : '';
				$stores	= ( $stores != '' ) ? array_filter( $stores ) : '';

				if( !empty( $pros ) || !empty( $cons ) || !empty( $stores ) ) { ?>

					<div class="row article__review__overview">

						<div class="col-xs-12">

							<?php
								if ( !empty( $pros ) || !empty( $cons ) ) {

									echo '<h2>PROS &amp; CONS</h2>

									<ul class="article__review__pros-and-cons">';

										if ( !empty( $pros ) ) {

											foreach( $pros as $key => $entry ) {
												echo '<li>+ '. $entry['bike_pro'] .'</li>';
											}

										}

										if( !empty( $pros ) && !empty( $cons ) ) {
											echo '<li class="article__review__pros-and-cons__spacer">&nbsp;</li>';
										}

										if ( !empty( $cons ) ) {

											foreach( $cons as $key => $entry ) {
												echo '<li>- '. $entry['bike_con'] .'</li>';
											}

										}

									echo '</ul>';
								}

								if( !empty( $stores[0] ) ) {

									echo '<h2>WHERE TO BUY</h2>

									<p class="article__review__msrp"><strong>MSRP:</strong> $'. $msrp . ' ' . $msrp_notes .'</p>

									<ul class="article__review__vendors">';

										foreach( $stores as $key => $entry ) {
											echo '<li><a href="'. $entry['store_url'] .'" target="_blank" class="btn btn--red">'. $entry['store_name'] .'</a></li>';
										}

									echo '</ul>';

								}
							?>
						</div>

					</div>

				<?php }
			?>

			<div class="entry-content">
				<?php
					$bike_about_hdr = array_key_exists( $prefix . 'bike_about_txt', $bike_meta ) && $bike_meta[$prefix . 'bike_about_txt'] != '' ? $bike_meta[$prefix . 'bike_about_txt'] : '';

					if( $bike_about_hdr != '' ) {
						echo '<h3>'. $bike_about_hdr .'</h3>';
					}

					$bike_review_content = array_key_exists( $prefix . 'bike_review', $bike_meta ) && $bike_meta[$prefix . 'bike_review'] != '' ? apply_filters( 'the_content', $bike_meta[$prefix . 'bike_review'] ) : '';

					echo $bike_review_content;


					/**
					 * gift guide stuffs
					 */
					if( $in_guide ) {
						$reviewer_id	= get_post_meta( get_the_ID(), '_vn_gg_author_id', 1 ) != '' ? get_post_meta( get_the_ID(), '_vn_gg_author_id', 1 ) : '';
						$rating			= get_post_meta( get_the_ID(), '_vn_gg_product_rating', 1 ) != 'none' ? get_post_meta( get_the_ID(), '_vn_gg_product_rating', 1 ) : '';


						if( $reviewer_id != '' || $rating != '' ) {
							echo '<div class="row">';
						}


						if( $reviewer_id != '' ) {

							$author_meta	= get_user_meta( $reviewer_id );
							$author_name	= get_the_author_meta( 'display_name', $reviewer_id );

							$reviewer_output = '<div class="col-md-6">
								<div class="reviewer_meta">';

									$template_directory		= get_template_directory_uri();
									$author_image			= get_template_directory() .'/images/authors/'. $reviewer_id .'.jpg';
									$author_image_display	= $template_directory .'/images/authors/'. $reviewer_id .'.jpg';

									if ( file_exists( $author_image ) ) {
										$reviewer_output .= '<img class="reviewer_meta__image" src="'. $author_image_display .'" alt="'. $author_name .'">';
									} else {
										$reviewer_output .= get_avatar( $reviewer_id, 120, '', $author_name, array( 'class' => 'reviewer_meta__image' ) );
									}

									$reviewer_output .= '<p class="reviewer_meta__name"><strong>Reviewed by:</strong><br>' .
									$author_name . '</p>
									<p class="reviewer_meta__bio">'. $author_meta['description'][0] .'</p>

								</div>
							</div>';

							echo $reviewer_output;

						}


						echo '<div class="col-md-6">';

							if( $rating != '' ) {

								$stars = '<div class="stars_rating">';

									$total		= 5;
									$floor		= floor( $rating );
									$ceiling	= ceil( $rating );
									$remaining	= $total - $ceiling;

									if( strpos( $rating, '.' ) !== false ) {

										for( $i = 0; $i < $floor; $i++ ) {
											$stars .= '<span class="fa fa-star" aria-hidden="true"></span>';
										}
										$stars .= '<span class="fa fa-star-half-o"></span>';

									} else {

										for( $i = 0; $i < $rating; $i++ ) {
											$stars .= '<span class="fa fa-star"></span>';
										}

									}

									for( $x = 0; $x < $remaining; $x++ ) {
										$stars .= '<span class="fa fa-star-o"></span>';
									}

								$stars .= '</div>';

								echo $stars;

							}


							$buy_url = get_post_meta( get_the_ID(), '_vn_gg_buy_url', 1 ) != '' ? get_post_meta( get_the_ID(), '_vn_gg_buy_url', 1 ) : '';

							if( $buy_url != '' ) {

								echo '<a class="btn--red" href="'. $buy_url .'" onClick="_gaq.push( [\'_trackEvent\', \'Triathlete Gift Guide\', \'Buy Now Button\', \''. $buy_url .'\'] );" target="_blank">Buy Now</a>';

							}

						echo '</div>';


						if( $reviewer_id != '' || $rating != '' ) {
							echo '</div>';
						}
					}
				?>
			</div>

		</div>

	</section>


	<?php

		/**
		 * get author bio for buyer/gift guide
		 */
		if( $bg_author !== false ) {
			$authorurl		= get_author_posts_url( get_the_author_meta( 'ID' ) );
			$displayname	= get_the_author_meta( 'display_name' );
			$gravatar		= get_avatar( get_the_author_meta( 'ID' ), 120, '', $displayname );
			$avatarurl		= get_the_author_meta( '_vn_avatar' );
			$avatarid		= get_attachment_id_from_src( $avatarurl );
			$avatarsized	= wp_get_attachment_image_src( $avatarid, 'thumbnail' );
			$bio			= apply_filters( 'the_content', get_the_author_meta( 'description' ) );
			$twitter		= get_the_author_meta( '_vn_twitter' );
			$facebook		= get_the_author_meta( '_vn_facebook' );
			$instagram		= get_the_author_meta( '_vn_instagram' );
			$pinterest		= get_the_author_meta( '_vn_pinterest' );
			$googleplus		= get_the_author_meta( '_vn_google_plus' );


			if( $avatarsized[0] ) {
				$author_avatar = '<img src="'. $avatarsized[0] .'">';
			} else {
				$author_avatar = $gravatar;
			}

			// $author_desc 	= get_the_author_meta('description', $bg_author );
			// $author_img		= get_avatar( $bg_author );
			// $author_name 	= get_the_author_meta('display_name', $bg_author);

			$authorbio_div = '<footer class="article__footer">
				<div class="row article__author">
					<div class="col-xs-3 article__author__thumbnail">
						<a href="'. $authorurl .'">'. $author_avatar .'</a>
					</div>';

					$authorbio_div .= '<div class="col-xs-9 article__author__bio">
						<h3>By <a href="'. $authorurl .'">'. $displayname .'</a></h3>

						'. $bio .'

						<ul class="article__author__social">';
							if( $twitter ) {
								$authorbio_div .= '<li>
									<a href="'. $twitter .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Twitter</span><span class="fa fa-twitter"></span></a>
								</li>';
							}
							if( $facebook ) {
								$authorbio_div .= '<li>
									<a href="'. $facebook .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Facebook</span>
									<span class="fa fa-facebook"></span></a>
								</li>';
							}
							if( $instagram ) {
								$authorbio_div .= '<li>
									<a href="'. $instagram .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Instagram</span>
									<span class="fa fa-instagram"></span></a>
								</li>';
							}
							if( $pinterest ) {
								$authorbio_div .= '<li>
									<a href="'. $pinterest .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Instagram</span>
									<span class="fa fa-pinterest-p"></span></a>
								</li>';
							}
							if( $googleplus ) {
								$authorbio_div .= '<li>
									<a href="'. $googleplus .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Instagram</span>
									<span class="fa fa-google-plus"></span></a>
								</li>';
							}
						$authorbio_div .= '</ul>
					</div>
				</div>
			</footer>';

			echo $authorbio_div;

		} else {

			/**
			 * get author bio
			 */
			echo get_author_info();
		}

	?>

</article>


<?php
	/**
	 * get bike ratings
	 */
	function get_ratings( $type_key, $overallscore ) {

		/**
		 * array housing key::display name and the max score of that category
		 * need diff max scores based on type of bike
		 */
		switch( $type_key ) {

			case 'mountain':
				$scores_array =
					array(
						'build::Build'					=> '20',
						'value::Value'					=> '15',
						'handling::Handling'			=> '15',
						'pedaling::Pedaling Response'	=> '15',
						'ascending::Ascending'			=> '15',
						'descending::Descending'		=> '15',
						'aesthetics::Aesthetics'		=> '5',
					);
				break;

			case 'gravel':
				$scores_array =
					array(
						'build::Build'					=> '15',
						'comfort::Comfort'				=> '20',
						'value::Value'					=> '15',
						'handling::Handling'			=> '15',
						'pedaling::Pedaling Response'	=> '15',
						'versatility::Versatility'		=> '15',
						'aesthetics::Aesthetics'		=> '5',
					);
				break;

			default:
				/**
				 * wf: 207042
				 * bike reviews after june 1, 2017 will no longer have the lab spec
				 * therefore, increase handling and pedaling by 10 each
				 * previous reviews should remain untouched
				 */
				if( get_the_date( 'U' ) > strtotime( 'June 1, 2017' ) ) {

					$scores_array =
						array(
							'build::Build'					=> '15',
							'comfort::Comfort'				=> '15',
							'value::Value'					=> '15',
							'handling::Handling'			=> '25',
							'pedaling::Pedaling Response'	=> '25',
							'ascending::Ascending'			=> '15',
							'descending::Descending'		=> '15',
							'versatility::Versatility'		=> '15',
							'aesthetics::Aesthetics'		=> '5',
						);

				} else {

					$scores_array =
						array(
							'lab::Lab'						=> '20',
							'build::Build'					=> '15',
							'comfort::Comfort'				=> '15',
							'value::Value'					=> '15',
							'handling::Handling'			=> '15',
							'pedaling::Pedaling Response'	=> '15',
							'ascending::Ascending'			=> '15',
							'descending::Descending'		=> '15',
							'versatility::Versatility'		=> '15',
							'aesthetics::Aesthetics'		=> '5',
						);

				}

				break;
		}

		$prefix				= '_vn_bike_review_';
		$indiv_scores_array	= array();

		echo '<div id="ratings-chart" class="row ratings-chart">
			<div class="col-xs-12">
				<div class="ratings-chart__rating ratings-chart__rating_name_overall">
					<div class="ratings-chart__rating__label">SCORE <strong>'. $overallscore .'/100</strong></div>
					<div class="ratings-chart__rating__wrap">';

						$post_id = get_the_ID();

						foreach( $scores_array as $key => $value ) {

							$scorekey		= explode( '::', $key );
							$score			= get_post_meta( $post_id, $prefix . 'bike_'. $scorekey[0] .'_score', 1 );
							$score_percent	= ( ( $score / $value ) * 100 );

							if( $score || $score == '0' ) {

								echo '<div class="ratings-chart__rating__bar ratings-chart__rating_name_'. $scorekey[0] .'" data-percent="'. $score .'%"></div>';

								$indiv_scores = '<div class="ratings-chart__rating ratings-chart__rating_name_'. $scorekey[0] .'">
									<div class="ratings-chart__rating__label">
										'. strtoupper( $scorekey[1] ) .' <strong>'. $score .'/'. $value .'</strong>';

										if( $scorekey[0] == 'lab' && ( $labnotes = get_post_meta( $post_id, $prefix . 'lab_notes', 1 ) ) ) {
											$indiv_scores .= '<span>('. $labnotes .')</span>';
										}

									$indiv_scores .= '</div>
									<div class="ratings-chart__rating__wrap ratings-chart__rating_max_'. $value .'">
										<div class="ratings-chart__rating__bar" data-percent="'. $score_percent .'%"></div>
									</div>
								</div>';

								$indiv_scores_array[] = $indiv_scores;

							}
						}

						echo '<ul class="ratings-chart__rating__markers">
							<li>10</li>
							<li>20</li>
							<li>30</li>
							<li>40</li>
							<li>50</li>
							<li>60</li>
							<li>70</li>
							<li>80</li>
							<li>90</li>
							<li>100</li>
						</ul>

					</div>
				</div>
			</div>';

			$scores_count		= count( $indiv_scores_array );
			$scores_count_half	= ceil( $scores_count / 2 );

			echo '<div class="col-xs-12 col-sm-6">';

				for( $i = 0; $i < $scores_count; $i++ ) {

					if( $i == $scores_count_half ) {
						echo '</div><div class="col-xs-6">';
					}

					echo $indiv_scores_array[$i];

				}

			echo '</div>
		</div>';

	}
