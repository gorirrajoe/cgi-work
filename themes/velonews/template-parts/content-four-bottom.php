<?php
/**
 * Template part for displaying four posts across
 *
 * sitewide, bottom of page
 *
 * @package velonews
 */
global $post_id_array;


/**
 * re: posts_per_page
 * add 27 to the query just in case there are duplicates
 * from the top 5, featured 1 (top right), top 4, featured 2,
 * top 5, 6 from the 2x2 (+2 responsive)
 */
if( is_category() && ( $four_across_bottom = get_term_meta( $cat_id, '_vn_four-across-bottom', 1 ) ) != '' ) {
	if( false === ( $four_bottom_query = get_transient( $cat_nicename . '_four_bottom_query' ) ) ) {
		$args = array(
			'post_status'				=> 'publish',
			'post_type'					=> 'any',
			'posts_per_page'			=> 31,
			'category__and'				=> array( $cat_id, $four_across_bottom ),
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false

		);
		$four_bottom_query = new WP_Query( $args );
		set_transient( $cat_nicename . '_four_bottom_query', $four_bottom_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}
	$title		= get_term_meta( $cat_id, '_vn_four-across-bottom-title', 1 );
	$cat_url	= get_category_link( $four_across_bottom );


} elseif ( 'races_event' == get_post_type() ) {
	$four_across_bottom = get_term_meta( $cat_id, '_vn_four-across-bottom', 1 ) ?  get_term_meta( $cat_id, '_vn_four-across-bottom', 1 ) : cgi_bikes_get_option( 'site_4bottom_category' );

	if( false === ( $four_bottom_query = get_transient( $cat_short_name . '_four_bottom_query' ) ) ) {
		$args = array(
			'posts_per_page'			=> 31,
			'post_type'					=> 'any',
			'category__and'				=> array( $cat_id, $four_across_bottom ),
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false

		);
		$four_bottom_query = new WP_Query( $args );
		set_transient( $cat_short_name . '_four_bottom_query', $four_bottom_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}

	$title = get_term_meta( $cat_id, '_vn_four-across-bottom-title', 1 ) ? get_term_meta( $cat_id, '_vn_four-across-bottom-title', 1 ) : cgi_bikes_get_option( 'site_4bottom_title' );

	$cat_id_result = get_term_meta( $cat_id, '_vn_four-across-bottom', 1) ? get_term_meta( $cat_id, '_vn_four-across-bottom', 1 ) : cgi_bikes_get_option('site_4bottom_category') ;
	$cat_url  = get_category_link( $cat_id_result );

} elseif( is_page_template() && ( $four_across_bottom = get_post_meta( get_the_ID(), '_vn_4bottom_category', 1 ) ) ) {
	$page_template_pieces = explode( '.', basename( get_page_template() ) );

	if( false === ( $four_bottom_query = get_transient( 'template_'. $page_template_pieces[0].'_four_bottom_query' ) ) ) {
		$args = array(
			'post_type'					=> 'any',
			'post_status'				=> 'publish',
			'posts_per_page'			=> 31,
			'cat'						=> $four_across_bottom,
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false

		);
		$four_bottom_query = new WP_Query( $args );
		set_transient( 'template_'. $page_template_pieces[0].'_four_bottom_query', $four_bottom_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}
	$title = get_post_meta( get_the_ID(), '_vn_4bottom_title', 1 );
	$cat_url	= get_category_link( $four_across_bottom );


} else {
	$global_four_bottom = cgi_bikes_get_option( 'site_4bottom_category' );

	if( false === ( $four_bottom_query = get_transient( 'global_four_bottom_query' ) ) ) {
		$args = array(
			'post_status'				=> 'publish',
			'post_type'					=> 'any',
			'posts_per_page'			=> 31,
			'cat'						=> $global_four_bottom,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false,
			'ignore_sticky_posts'		=> true
		);
		$four_bottom_query = new WP_Query( $args );
		set_transient( 'global_four_bottom_query', $four_bottom_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}
	$title		= cgi_bikes_get_option( 'site_4bottom_title' );
	$cat_url	= get_category_link( $global_four_bottom );

}


$count = 1;
if ( $four_bottom_query->have_posts() ) {
	echo '<section class="row content__section">
		<header class="col-xs-12 content__section__header">
			<h2>'. strtoupper( $title ) .'</h2>
			<a href="'. $cat_url .'" target="_self" class="content__section__more">VIEW ALL <span class="fa fa-caret-right"></span></a>
		</header>';

		while( $four_bottom_query->have_posts() ) {
			$four_bottom_query->the_post();

			if( !in_array( get_the_ID(), $post_id_array ) && $count < 5 ) {

				$cat_array			= get_cat_array( get_the_ID() );
				$thumbnail			= get_the_post_thumbnail_url( get_the_ID(), 'featured-thumb-lg' );
				$post_id_array[]	= get_the_ID();

				$article_classes	= '';

				if( is_array( $cat_array['slug'] ) ) {
					foreach( $cat_array['slug'] as $slug ) {
						$article_classes .= ' article_category_' . $slug;
					}
				} else {
					$article_classes .= ' article_category_' . $cat_array['slug'];
				}

				echo '<article class="col-xs-12 col-sm-3 article article_type_latest '. $article_classes .'">
					<a href="'. get_permalink() .'" target="_self" class="article__permalink" title="'. get_the_title() .'">
						<div class="article__thumbnail__wrap">
							<img data-original="'. $thumbnail .'" alt="" class="article__thumbnail lazyload">
							<div class="article__thumbnail__overlay"></div>
						</div>
						<div class="article__category">'. get_article_category_tag( get_the_ID(), $cat_array ) .'</div>
						<p class="article__title">'. get_the_title() .'</p>
					</a>
				</article>';

				$count++;
			}
		}
		wp_reset_postdata();

	echo '</section>';
}

?>
