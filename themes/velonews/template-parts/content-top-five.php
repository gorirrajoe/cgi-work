<?php
/**
 * Template part for displaying 5 featured posts
 *
 * homepage, center column
 *
 * @package velonews
 */
global $post_id_array;


if ( is_home() || is_front_page() ) {
	$home_featured_5 = explode( ',', cgi_bikes_get_option( 'home_featured_5' ) );
	$home_featured_5 = array_filter( $home_featured_5 );

	if ( !empty( $home_featured_5 ) ) {
		$args = array(
			'ignore_sticky_posts'		=> true,
			'post_status'				=> 'publish',
			'posts_per_page'			=> 5,
			'post_type'					=> 'any',
			'post__in'					=> $home_featured_5,
			'orderby'					=> 'post__in',
			'has_password'				=> false,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false
		);
	} else {
		$args = array(
			'ignore_sticky_posts'		=> true,
			'post_status'				=> 'publish',
			'post_type'					=> 'any',
			'posts_per_page'			=> 5,
			'meta_key'					=> '_featured_post',
			'meta_value'				=> 'featured',
			'has_password'				=> false,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false
		);
	}

	if ( false === ( $top5_articles = get_transient( 'homepage_top5_articles' ) ) ) {
		$top5_articles = new WP_Query( $args );
		set_transient( 'homepage_top5_articles', $top5_articles, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}
} else {
	if ( false === ( $top5_articles = get_transient( $cat_nicename . '_top5_query' ) ) ) {
		$args = array(
			'ignore_sticky_posts'		=> true,
			'post_status'				=> 'publish',
			'posts_per_page'			=> 5,
			'post_type'					=> 'any',
			'has_password'				=> false,
			'cat'						=> $cat_id,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false

		);
		$top5_articles = new WP_Query( $args );
		set_transient( $cat_nicename . '_top5_query', $top5_articles, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}
}

if ( $top5_articles->have_posts() ) {
	echo '<section class="content__section content__flex_position_second">';
		while( $top5_articles->have_posts() ) : $top5_articles->the_post();

			$post_ID			= get_the_ID();
			$post_id_array[]	= $post_ID;
			$article_classes	= $top5_articles->current_post == 0 ? ' article_type_latest--featured' : ' col-sm-6';

			$thumbnail	= get_the_post_thumbnail( $post_ID, 'featured-thumb-lg', array( 'class' => 'article__thumbnail' ) );
			$title		= vn_get_post_title( $post_ID );
			$cat_array	= get_cat_array( $post_ID );

			if ( is_array( $cat_array['slug'] ) ) {
				foreach ( $cat_array['slug'] as $slug ) {
					$article_classes .= ' article_category_' . $slug;
				}
			} else {
				$article_classes .= ' article_category_' . $cat_array['slug'];
			}

			// turn thumbnails into lazyload images if in the last 3 posts, only on mobile
			if ( $top5_articles->current_post > 1 && is_mobile() ) {
				$thumbnail_src	= get_the_post_thumbnail_url( $post_ID, 'featured-thumb-lg' );
				$thumbnail	= '<img data-original="'. $thumbnail_src .'" alt="" class="article__thumbnail lazyload">';
			}

			if ( $top5_articles->current_post == 3 ) {

				echo '<article class="col-xs-12 article article_type_latest' . $article_classes .'">';
					echo '<div class="ad_fallback">
						<a href="'. get_permalink() .'" target="_self" class="article__permalink" title="'. get_the_title() .'">
							<div class="article__thumbnail__wrap">
								'. $thumbnail .'
								<div class="article__thumbnail__overlay"></div>
							</div>
							<div class="article__category">'. get_article_category_tag( $post_ID, $cat_array ) .'</div>
							<p class="article__title">'. $title .'</p>
						</a>
					</div>';
					echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('content-sponsored') : '';
				echo '</article>';

			} else {

				echo '
					<article class="col-xs-12 article article_type_latest' . $article_classes .'">
						<a href="'. get_permalink() .'" target="_self" class="article__permalink" title="'. get_the_title() .'">
							<div class="article__thumbnail__wrap">
								'. $thumbnail .'
								<div class="article__thumbnail__overlay"></div>
							</div>
							<div class="article__category">'. get_article_category_tag( $post_ID, $cat_array ) .'</div>
							<p class="article__title">'. $title .'</p>
						</a>
					</article>
				';

			}

		endwhile;

		if( is_home() || is_front_page() ) {
			echo '<div class="col-xs-12">
				<a href="'. site_url( '/all/' ) .'" target="_self" class="content__section__more">VIEW MORE NEWS <span class="fa fa-caret-right"></span></a>
			</div>';
		} else {
			echo '<div class="col-xs-12">
				<a href="'. $cat_url .'/page/2" target="_self" class="content__section__more">VIEW MORE '. strtoupper( $cat_obj->name ) .' <span class="fa fa-caret-right"></span></a>
			</div>';
		}
		wp_reset_postdata();

	echo '</section>';
}
?>
