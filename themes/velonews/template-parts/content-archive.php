<?php
/**
 * Template part for displaying standard single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package velonews
 */
?>


<article id="post-<?php the_ID(); ?>" <?php post_class( array( 'article', 'article_type_archive-result' ) ); ?>>
	<?php
		if( has_post_thumbnail() ) {
			$thumb_id	= get_post_thumbnail_id();
			$thumbnail	= wp_get_attachment_image_src( $thumb_id, 'newsletter-top' );

			echo '<div class="article__thumbnail__wrap">';
				echo '<a href="'. get_permalink() .'" target="_self">';
					echo '<img src="'. $thumbnail[0] .'" class="article__thumbnail">';
				echo '</a>';
			echo '</div>';
		}
	?>
	<div class="article__body__wrap">
		<h3 class="article__title">
			<a href="<?php echo get_permalink(); ?>" target="_self" class="article__permalink"><?php the_title(); ?></a>
		</h3>

		<?php velonews_posted_on(); ?>

		<p class="article__excerpt">
			<?php if( in_array( get_post_type(), array( 'post', 'races_event', 'bike-reviews', 'apparel-reviews' ) ) ) {
				echo get_the_excerpt();
			} elseif( 'stage' == get_post_type() ) {
				echo get_post_meta( get_the_ID(), '_vn_stages_stage_results_title', 1 );
			} ?>

		</p>
		<p class="article__meta">
			<?php
				$categories	= get_the_category();
				$separator	= ' ';
				$output		= '';

				if ( !empty( $categories ) ) {
					foreach( $categories as $category ) {
						$output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '" class="article_category_'. $category->category_nicename .'"><span class="article__category">' . esc_html( $category->name ) . '</span></a>' . $separator;
					}
					echo trim( $output, $separator );
				}
			?>
		</p>
	</div>
</article><!-- #post-## -->
