<?php
	/**
	 * Template part for displaying standard single posts.
	 *
	 * @link https://codex.wordpress.org/Template_Hierarchy
	 *
	 * @package velonews
	 */
	$sponsorship = get_presented_by();


	$cats		= get_the_category();
	$cat_array	= [];
	$in_guide	= false;

	//check if in gift guide or buyers guide after 2017, set in_guide true
	foreach( $cats as $key => $value ) {
		if( strpos( $value->slug, 'guide' ) != false ) {

			//get the year for the guide
			preg_match_all( '!\d+!', $value->slug, $guide_year );
			$guide_year = $guide_year[0][0];

			//start with 2017 gift guide and 2018 buyers guide
			if( ( $guide_year > 2016 && strpos($value->slug, 'gift') != false ) || ( $guide_year > 2017 && strpos( $value->slug, 'buy' ) != false ) ) {

				$in_guide					= true;
				$cat_array[$value->slug]	= $value->name;
				$guide_slug					= current( array_keys( $cat_array ) );
				$guide_name					= $cat_array[$guide_slug];
				$guide_link					= get_category_link( get_category_by_slug( $guide_slug ) );

			}
		}
	}

	if( $in_guide ) { ?>

		<div class="template__bg-final">
			<section id="guides">
				<div class="col-xs-12 bg__final_title">
					<h1><a href="<?php echo( $guide_link );?>"><?php echo $guide_name; ?></a></h1>
				</div>
				<div class="guide__menu">
					<?php
						wp_nav_menu( array(
							'theme_location'	=> $guide_slug,
							'menu_class'		=> 'bg__final_top_menu'
						) );
					?>
				</div>
			</section>
		</div>
	<?php }
?>


<article id="article" <?php post_class( array( 'col-xs-12', 'col-sm-7', 'col-md-8', 'article' ) ); ?>>

	<?php
		/**
		 * breadcrumb
		 */
		get_breadcrumb();
	?>

	<header class="article__header">

		<?php the_title( '<h1 class="article__header__title">', '</h1>' );

		if ( 'post' === get_post_type() ) {
			velonews_posted_on();
		} ?>
	</header>


	<?php
		echo $sponsorship['content'];

	?>


	<section class="article__body row">
		<?php
			/**
			 * social sharing
			 */
			get_social_sharing( 'side' );

			echo '<div id="article-right" class="col-xs-12 col-md-11">';

				get_social_sharing( 'top' );

				insert_featured_image( get_the_ID() );

				the_content();


				/**
				 * gift guide stuffs
				 */
				if( $in_guide ) {
					$reviewer_id	= get_post_meta( get_the_ID(), '_vn_gg_author_id', 1 ) != '' ? get_post_meta( get_the_ID(), '_vn_gg_author_id', 1 ) : '';
					$rating			= get_post_meta( get_the_ID(), '_vn_gg_product_rating', 1 ) != 'none' ? get_post_meta( get_the_ID(), '_vn_gg_product_rating', 1 ) : '';


					if( $reviewer_id != '' || $rating != '' ) {
						echo '<div class="row">';
					}


					if( $reviewer_id != '' ) {

						$author_meta	= get_user_meta( $reviewer_id );
						$author_name	= get_the_author_meta( 'display_name', $reviewer_id );

						$reviewer_output = '<div class="col-md-6">
							<div class="reviewer_meta">';

								$template_directory		= get_template_directory_uri();
								$author_image			= get_template_directory() .'/images/authors/'. $reviewer_id .'.jpg';
								$author_image_display	= $template_directory .'/images/authors/'. $reviewer_id .'.jpg';

								if ( file_exists( $author_image ) ) {
									$reviewer_output .= '<img class="reviewer_meta__image" src="'. $author_image_display .'" alt="'. $author_name .'">';
								} else {
									$reviewer_output .= get_avatar( $reviewer_id, 120, '', $author_name, array( 'class' => 'reviewer_meta__image' ) );
								}

								$reviewer_output .= '<p class="reviewer_meta__name"><strong>Reviewed by:</strong><br>' .
								$author_name . '</p>
								<p class="reviewer_meta__bio">'. $author_meta['description'][0] .'</p>

							</div>
						</div>';

						echo $reviewer_output;

					}


					echo '<div class="col-md-6">';

						if( $rating != '' ) {

							$stars = '<div class="stars_rating">';

								$total		= 5;
								$floor		= floor( $rating );
								$ceiling	= ceil( $rating );
								$remaining	= $total - $ceiling;

								if( strpos( $rating, '.' ) !== false ) {

									for( $i = 0; $i < $floor; $i++ ) {
										$stars .= '<span class="fa fa-star" aria-hidden="true"></span>';
									}
									$stars .= '<span class="fa fa-star-half-o"></span>';

								} else {

									for( $i = 0; $i < $rating; $i++ ) {
										$stars .= '<span class="fa fa-star"></span>';
									}

								}

								for( $x = 0; $x < $remaining; $x++ ) {
									$stars .= '<span class="fa fa-star-o"></span>';
								}

							$stars .= '</div>';

							echo $stars;

						}


						$buy_url = get_post_meta( get_the_ID(), '_vn_gg_buy_url', 1 ) != '' ? get_post_meta( get_the_ID(), '_vn_gg_buy_url', 1 ) : '';

						if( $buy_url != '' ) {

							echo '<a class="btn--red" href="'. $buy_url .'" onClick="_gaq.push( [\'_trackEvent\', \'Triathlete Gift Guide\', \'Buy Now Button\', \''. $buy_url .'\'] );" target="_blank">Buy Now</a>';

						}

					echo '</div>';


					if( $reviewer_id != '' || $rating != '' ) {
						echo '</div>';
					}
				}

			echo '</div>';
		?>
	</section>


	<?php
		/**
		 * get author bio
		 */
		echo get_author_info();
	?>

</article><!-- #article -->

