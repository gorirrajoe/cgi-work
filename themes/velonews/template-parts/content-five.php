<?php
/**
 * Template part for displaying five posts across the site
 *
 * @package velonews
 */
global $post_id_array;


/**
 * re: posts_per_page
 * add 12 to the query just in case there are duplicates
 * from the top 5, featured 1 (top right), top 4, and featured 2
 */
if( is_category() && ( $five_across = get_term_meta( $cat_id, '_vn_five-across', 1 ) ) != '' ) {
	/**
	 * get category number from cat options, or global options if not entered
	 */
	if( false === ( $five_recent_query = get_transient( $cat_nicename . '_5across_query' ) ) ) {
		$args = array(
			'posts_per_page'			=> 17,
			'category__and'				=> array( $cat_id, $five_across ),
			'post_status'				=> 'publish',
			'ignore_sticky_posts'		=> true,
			'post_type'					=> 'any',
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false,

		);
		$five_recent_query = new WP_Query( $args );
		set_transient( $cat_nicename . '_5across_query', $five_recent_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query category part 1 -->';
	} else {
		echo '<!-- cache -->';
	}
	$title		= get_term_meta( $cat_id, '_vn_five-across-title', 1 );
	$cat_url	= get_category_link( $five_across );


} elseif ( 'races_event' == get_post_type() ) {
	$five_across_category = get_term_meta( $cat_id, '_vn_five-across', 1 ) ?  get_term_meta( $cat_id, '_vn_five-across', 1 ) : cgi_bikes_get_option( 'sitewide_5_category' );

	if( false === ( $five_recent_query = get_transient( $cat_short_name . '_five_recent_query' ) ) ) {
		$args = array(
			'posts_per_page'			=> 17,
			'category__and'				=> array( $cat_id, $five_across_category ),
			'post_type'					=> 'any',
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false,


		);
		$five_recent_query = new WP_Query( $args );
		set_transient( $cat_short_name . '_five_recent_query', $five_recent_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query category part 2 -->';
	} else {
		echo '<!-- cache -->';
	}

	$title = get_term_meta( $cat_id, '_vn_five-across-title', 1 ) ? get_term_meta( $cat_id, '_vn_five-across-title', 1 ) :  cgi_bikes_get_option( 'sitewide_5_category' );

	$cat_id_result = get_term_meta( $cat_id, '_vn_five-across', 1) ? get_term_meta( $cat_id, '_vn_five-across', 1 ) : $five_across_category ;
	$cat_url = get_category_link( $cat_id_result );

} elseif( is_page_template() && ( $five_across = get_post_meta( get_the_ID(), '_vn_5_category', 1 ) ) ) {
	$page_template_pieces = explode( '.', basename( get_page_template() ) );

	if( false === ( $five_recent_query = get_transient( 'template_'. $page_template_pieces[0].'_5across_query' ) ) ) {
		$args = array(
			'posts_per_page'			=> 17,
			'post_type'					=> 'any',
			'cat'						=> $five_across,
			'post_status'				=> 'publish',
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false
		);
		$five_recent_query = new WP_Query( $args );
		set_transient( 'template_'. $page_template_pieces[0].'_5across_query', $five_recent_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query part 3 -->';
	} else {
		echo '<!-- cache -->';
	}
	$title = get_post_meta( get_the_ID(), '_vn_5_title', 1 );
	$cat_url	= get_category_link( $five_across );


} else {
	$five_across_category = cgi_bikes_get_option( 'site_5_category' );

	if( false === ( $five_recent_query = get_transient( 'sitewide_five_query' ) ) ) {
		$five_recent_query = new WP_Query(
			array(
				'posts_per_page'			=> 17,
				'post_type'					=> 'any',
				'cat'						=> $five_across_category,
				'post_status'				=> 'publish',
				'ignore_sticky_posts'		=> true,
				'no_found_rows'				=> true,
				'update_post_meta_cache'	=> false,
			)
		);
		set_transient( 'sitewide_five_query', $five_recent_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query part 4-->';
	} else {
		echo '<!-- cache -->';
	}
	$title		= cgi_bikes_get_option( 'site_5_title' );
	$cat_url	= get_category_link( $five_across_category );
}


$count = 1;
if( $five_recent_query->have_posts() ) {
	echo '<section class="row content__section content__section_cols_five">
		<div class="col-xs-12 content__section__header content__section__header">
			<h2>'. strtoupper( $title ) .'</h2>
			<a href="'. $cat_url .'" target="_self" class="content__section__more">VIEW ALL <span class="fa fa-caret-right"></span></a>
		</div>';

		while( $five_recent_query->have_posts() ) {
			$five_recent_query->the_post();

			if( !in_array( get_the_ID(), $post_id_array ) && $count < 6 ) {

				$thumbnail			= get_the_post_thumbnail_url( get_the_ID(), 'featured-thumb-lg' );
				$title				= vn_get_post_title( get_the_ID() );
				$cat_array			= get_cat_array( get_the_ID() );
				$post_id_array[]	= get_the_ID();

				$article_classes	= '';
				if( is_array( $cat_array['slug'] ) ) {
					foreach( $cat_array['slug'] as $slug ) {
						$article_classes .= ' article_category_' . $slug;
					}
				} else {
					$article_classes .= ' article_category_' . $cat_array['slug'];
				}

				echo '
					<article class="col-xs-12 col-sm-3 article article_type_latest '. $article_classes .'">
						<a href="'. get_permalink() .'" target="_self" class="article__permalink" title="'. $title .'">
							<div class="article__thumbnail__wrap">
								<img data-original="'. $thumbnail .'" alt="" class="article__thumbnail lazyload">
								<div class="article__thumbnail__overlay"></div>
							</div>
							<div class="article__category">'. get_article_category_tag( get_the_ID(), $cat_array ) .'</div>
							<p class="article__title">'. $title .'</p>
						</a>
					</article>
				';

				$count++;
			}
		}
		wp_reset_postdata();

	echo '</section>';

}

?>
