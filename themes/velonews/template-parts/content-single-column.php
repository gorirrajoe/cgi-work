<?php
/**
 * Template part for displaying standard single column pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package velonews
 */
	$prefix = '_vn_';

?>


<article id="article" <?php post_class( array( 'col-xs-12', 'article' ) ); ?>>

	<?php
		/**
		 * breadcrumb
		 */
		get_breadcrumb();
	?>

	<header class="article__header">

		<?php the_title( '<h1 class="article__header__title">', '</h1>' ); ?>

	</header>


	<section class="article__body row">
		<?php
			/**
			 * social sharing
			 */
			get_social_sharing( 'side' );

			echo '<div id="article-right" class="col-xs-12 col-md-11">';

				get_social_sharing( 'top' );

				the_content();

			echo '</div>';
		?>
	</section>


</article><!-- #article -->

