<?php
/**
 * Template part for displaying 4 posts in 2x2 format.
 *
 * @package velonews
 */
global $post_id_array;

/**
 * re: posts_per_page
 * add 17 to the query just in case there are duplicates
 * from the top 5, featured 1 (top right), top 4, featured 2, and top 5
 *
 * note that you're technically pulling 6 articles (responsive tablet)
 */
if( is_category() && ( $two_by_two = get_term_meta( $cat_id, '_vn_two-by-two', 1 ) ) != '' ) {
	if( false === ( $two_by_two_query = get_transient( $cat_nicename . '_twobytwo_query' ) ) ) {
		$args = array(
			'post_status'				=> 'publish',
			'post_type'					=> 'any',
			'posts_per_page'			=> 23,
			'category__and'				=> array( $cat_id, $two_by_two ),
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false
		);
		$two_by_two_query = new WP_Query( $args );
		set_transient( $cat_nicename . '_twobytwo_query', $two_by_two_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}
	$title		= get_term_meta( $cat_id, '_vn_two-by-two-title', 1 );
	$cat_url	= get_category_link( $two_by_two );


} elseif ( 'races_event' == get_post_type() ) {

	$two_by_two = get_term_meta( $cat_id, '_vn_two-by-two', 1 ) ?  get_term_meta( $cat_id, '_vn_two-by-two', 1 ) : cgi_bikes_get_option( 'posts_home_2by2_category' );

	if( false === ( $two_by_two_query = get_transient( $cat_short_name . '_twobytwo_query' ) ) ) {
		$args = array(
			'category__and'				=> array( $cat_id, $two_by_two ),
			'post_type'					=> 'any',
			'posts_per_page'			=> 23,
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false,
			'post_status'				=> 'publish',
			'ignore_sticky_posts'		=> true,
		);
		$two_by_two_query = new WP_Query( $args );
		set_transient( $cat_short_name . '_twobytwo_query', $two_by_two_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}
	$title = get_term_meta( $cat_id, '_vn_two-by-two-title', 1 ) ? get_term_meta( $cat_id, '_vn_two-by-two-title', 1 ) : get_post_meta( get_the_ID(), '_vn_2by2_title', 1 ) ;
	$cat_url = get_category_link( $two_by_two );

} elseif( is_page_template() && ( $two_by_two = get_post_meta( get_the_ID(), '_vn_2by2_category', 1 ) ) != '' ) {
	$page_template_pieces = explode( '.', basename( get_page_template() ) );

	if( false === ( $two_by_two_query = get_transient( 'template_'. $page_template_pieces[0].'_twobytwo_query' ) ) ) {
		$args = array(
			'posts_per_page'			=> 23,
			'post_status'				=> 'publish',
			'cat'						=> $two_by_two,
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false
		);
		$two_by_two_query = new WP_Query( $args );
		set_transient( 'template_'. $page_template_pieces[0].'_twobytwo_query', $two_by_two_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}
	$title = get_post_meta( get_the_ID(), '_vn_2by2_title', 1 );
	$cat_url	= get_category_link( $two_by_two );


} else {
	$global_twobytwo = cgi_bikes_get_option( 'posts_home_2by2_category' );

	if( false === ( $two_by_two_query = get_transient( 'global_twobytwo_query' ) ) ) {
		$args = array(
			'posts_per_page'			=> 23,
			'post_type'					=> 'any',
			'post_status'				=> 'publish',
			'cat'						=> $global_twobytwo,
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false

		);
		$two_by_two_query = new WP_Query( $args );
		set_transient( 'global_twobytwo_query', $two_by_two_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}
	$title		= cgi_bikes_get_option( 'home_2by2_title' );
	$cat_url	= get_category_link( $global_twobytwo );

}


$count = 1;
if ( $two_by_two_query->have_posts() ) {
	echo '<section class="row content__flex">

		<section class="content__section content__flex_position_fourth">
			<header class="col-xs-12 content__section__header content__section__header">
				<h2>'. strtoupper( $title ) .'</h2>
				<a href="'. $cat_url .'" target="_self" class="content__section__more">VIEW ALL <span class="fa fa-caret-right"></span></a>
			</header>';

			while( $two_by_two_query->have_posts() ) {
				$two_by_two_query->the_post();

				if( !in_array( get_the_ID(), $post_id_array ) && $count < 7 ) {

					$title				= vn_get_post_title( get_the_ID() );
					$thumbnail			= get_the_post_thumbnail_url( get_the_ID(), 'featured-thumb-lg' );
					$cat_array			= get_cat_array( get_the_ID() );
					$post_id_array[]	= get_the_ID();

					$article_classes	= '';

					if( is_array( $cat_array['slug'] ) ) {
						foreach( $cat_array['slug'] as $slug ) {
							$article_classes .= ' article_category_' . $slug;
						}
					} else {
						$article_classes .= ' article_category_' . $cat_array['slug'];
					}

					echo '<article class="col-xs-12 col-sm-6 article article_type_latest'. $article_classes .'">
						<a href="'. get_permalink() .'" target="_self" class="article__permalink" title="'. $title .'">
							<div class="article__thumbnail__wrap">
								<img data-original="'. $thumbnail .'" alt="" class="article__thumbnail lazyload">
								<div class="article__thumbnail__overlay"></div>
							</div>
							<div class="article__category">'. get_article_category_tag( get_the_ID(), $cat_array ) .'</div>
							<p class="article__title">'. $title .'</p>
						</a>
					</article>';

					$count++;
				}
			}
			wp_reset_postdata();

		echo '</section>';


			/**
			 * ad
			 */
		?>
		<section class="content__section content__flex_position_fifth">
			<section class="col-xs-12 hidden-xs advert advert_sm_300x600">
				<div class="advert__wrap">
					<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom-side' ) : '';?>
				</div>
			</section>
		</section>

	</section>

<?php }

?>

