<?php
	/**
	 * Template part for displaying multipage single posts.
	 */
	global $post;
	global $page;

	$post_ID	= $post->ID;
?>

<article id="post-<?php echo $post_ID; ?>" <?php post_class( array( 'col-xs-12', 'article' ) ); ?>>

	<header class="article__header">

		<?php
			the_title( '<h1 class="article__header__title">', '</h1>' );

			velonews_posted_on();
		?>
	</header>

	<?php
		$sponsorship = get_presented_by();
		echo $sponsorship['content'];
	?>

	<section class="article__body">

		<?php
			echo VeloNews_AMP::get_social_sharing();

			insert_featured_image( get_the_ID() );

			// Create table of contents of post
			preg_match_all( '/<!--pagetitle:(.*?)-->/', $post->post_content, $titles, PREG_PATTERN_ORDER );

			$page_titles = ( isset( $titles[1] ) ? $titles[1] : array() );

			if ( !empty( $page_titles ) ) : ?>

				<aside class="recommended-content alignleft">
					<div class="recommended-content__wrap">
						<h3 class="recommended-content__title">Table of Contents</h3>
							<ul class="recommended-content__menu">

							<?php foreach ( $page_titles as $i => $page_title ) {

								$i++;

								echo '<li>';

									if ( $page == $i ) {
										echo $page_title;
									} elseif ( 1 === $i ) {
										echo '<a href="'. get_permalink() .'/'. AMP_QUERY_VAR .'">'. $page_title .'</a>';
									} else {
										echo '<a href="'. get_permalink() .'/'. $i .'/'. AMP_QUERY_VAR .'">'. $page_title .'</a>';
									}

								echo '</li>';

							} ?>

						</ul>
					</div>
				</aside>

				<?php

			endif;

			echo $this->get( 'post_amp_content' );

			wp_link_pages( array(
					'before'      => '<div class="page-links">' . esc_html__( 'Pages:', 'velonews' ),
					'after'       => '</div>',
					'link_before' => '<span class="page-link-number" aria-label="Go To Page %">',
					'link_after'  => '</span>'
			) );

			echo VeloNews_AMP::get_social_sharing();
		?>
	</section>

</article><!-- #post-## -->
