<?php
	/**
	 * The template for displaying 2015 buyer's guide landing page
	 *
	 * @package velonews
	 */

	$this_category	= get_category(get_query_var('cat'), false);
	$bg_options		= BuyersGuide_Admin::get_guide_option( '2015', 'all' );

	get_header();
?>


<section id="content" class="content">
	<div class="vbg_container">

		<div class="vbg_banner">
			<a href="<?php echo site_url( '/category/' . $this_category->slug ); ?>"><img src="<?php echo $bg_options['buyers_guide_2015_banner_image']; ?>" /></a>
		</div>

		<div class="vbg_twothirds">

			<div class="vbg_full">
				<?php // 1
					$parsed_url_1		= parse_url( $bg_options[ 'buyers_guide_2015_link_1'] );
					$google_tracking_1	= '';

					if( $bg_options[ 'buyers_guide_2015_link_1'] != '' && strpos( home_url(), $parsed_url_1['host'] ) === FALSE  ) {
						$google_tracking_1 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options[ 'buyers_guide_2015_link_1'].'\') )"';
					}
				?>
				<a href="<?php echo $bg_options['buyers_guide_2015_link_1']; ?>" <?php echo $google_tracking_1; ?>>
					<img src="<?php echo $bg_options['buyers_guide_2015_image_1']; ?>">
				</a>
			</div>

			<div class="vbg_row">
				<div class="vbg_onethird">
					<div class="vbg_row">

						<div class="vbg_onesixth">
							<?php // 2
								$parsed_url_2		= parse_url( $bg_options[ 'buyers_guide_2015_link_2'] );
								$google_tracking_2	= '';

								if( $bg_options[ 'buyers_guide_2015_link_2'] != '' && strpos( home_url(), $parsed_url_2['host'] ) === FALSE  ) {
									$google_tracking_2 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_2'].'\') )"';
								}
							?>
							<a href="<?php echo $bg_options['buyers_guide_2015_link_2']; ?>" <?php echo $google_tracking_2; ?>>
								<img src="<?php echo $bg_options['buyers_guide_2015_image_2']; ?>">
							</a>
						</div>

						<div class="vbg_onesixth">
							<?php // 3
								$parsed_url_3		= parse_url( $bg_options['buyers_guide_2015_link_3'] );
								$google_tracking_3	= '';

								if( $bg_options['buyers_guide_2015_link_3'] != '' && strpos( home_url(), $parsed_url_3['host'] ) === FALSE  ) {
									$google_tracking_3 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_3'].'\') )"';
								}
							?>
							<a href="<?php echo $bg_options['buyers_guide_2015_link_3']; ?>" <?php echo $google_tracking_3; ?>>
								<img src="<?php echo $bg_options['buyers_guide_2015_image_3']; ?>">
							</a>
						</div>

					</div>

					<div class="vbg_full">
						<?php // 4
							$parsed_url_4		= parse_url( $bg_options['buyers_guide_2015_link_4'] );
							$google_tracking_4	= '';

							if( $bg_options['buyers_guide_2015_link_4'] != '' && strpos( home_url(), $parsed_url_4['host'] ) === FALSE  ) {
								$google_tracking_4 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_4'].'\') )"';
							}
						?>
						<a href="<?php echo $bg_options['buyers_guide_2015_link_4']; ?>" <?php echo $google_tracking_4; ?>>
							<img src="<?php echo $bg_options['buyers_guide_2015_image_4']; ?>">
						</a>
					</div>

					<div class="vbg_row">

						<div class="vbg_onesixth">
							<?php // 5
								$parsed_url_5		= parse_url( $bg_options['buyers_guide_2015_link_5'] );
								$google_tracking_5	= '';

								if( $bg_options['buyers_guide_2015_link_5'] != '' && strpos( home_url(), $parsed_url_5['host'] ) === FALSE  ) {
									$google_tracking_5 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_5'].'\') )"';
								}
							?>
							<a href="<?php echo $bg_options['buyers_guide_2015_link_5']; ?>" <?php echo $google_tracking_5; ?>>
								<img src="<?php echo $bg_options['buyers_guide_2015_image_5']; ?>">
							</a>
						</div>

						<div class="vbg_onesixth">
							<?php // 6
								$parsed_url_6		= parse_url( $bg_options['buyers_guide_2015_link_6'] );
								$google_tracking_6	= '';

								if( $bg_options['buyers_guide_2015_link_6'] != '' && strpos( home_url(), $parsed_url_6['host'] ) === FALSE  ) {
									$google_tracking_6 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_6'].'\') )"';
								}
							?>
							<a href="<?php echo $bg_options['buyers_guide_2015_link_6']; ?>" <?php echo $google_tracking_6; ?>>
								<img src="<?php echo $bg_options['buyers_guide_2015_image_6']; ?>">
							</a>
						</div>

					</div>

					<div class="vbg_full">
						<?php // 7
							$parsed_url_7		= parse_url( $bg_options['buyers_guide_2015_link_7'] );
							$google_tracking_7	= '';

							if( $bg_options['buyers_guide_2015_link_7'] != '' && strpos( home_url(), $parsed_url_7['host'] ) === FALSE  ) {
								$google_tracking_7 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_7'].'\') )"';
							}
						?>
						<a href="<?php echo $bg_options['buyers_guide_2015_link_7']; ?>" <?php echo $google_tracking_7; ?>>
							<img src="<?php echo $bg_options['buyers_guide_2015_image_7']; ?>">
						</a>
					</div>

					<div class="vbg_row">

						<div class="vbg_onesixth vbg_module">
							<?php // 8
								$parsed_url_8		= parse_url( $bg_options['buyers_guide_2015_link_8'] );
								$google_tracking_8	= '';

								if( $bg_options['buyers_guide_2015_link_8'] != '' && strpos( home_url(), $parsed_url_8['host'] ) === FALSE  ) {
									$google_tracking_8 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_8'].'\') )"';
								}
							?>
							<a href="<?php echo $bg_options['buyers_guide_2015_link_8']; ?>" <?php echo $google_tracking_8; ?>>
								<img src="<?php echo $bg_options['buyers_guide_2015_image_8']; ?>">
							</a>
						</div>

						<div class="vbg_onesixth vbg_module">
							<?php // 9
								$parsed_url_9		= parse_url( $bg_options['buyers_guide_2015_link_9'] );
								$google_tracking_9	= '';

								if( $bg_options['buyers_guide_2015_link_9'] != '' && strpos( home_url(), $parsed_url_9['host'] ) === FALSE  ) {
									$google_tracking_9 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_9'].'\') )"';
								}
							?>
							<a href="<?php echo $bg_options['buyers_guide_2015_link_9']; ?>" <?php echo $google_tracking_9; ?>>
								<img src="<?php echo $bg_options['buyers_guide_2015_image_9']; ?>">
							</a>
						</div>

					</div>

					<div class="vbg_full">
						<?php // 10
							$parsed_url_10		= parse_url( $bg_options['buyers_guide_2015_link_10'] );
							$google_tracking_10	= '';

							if( $bg_options['buyers_guide_2015_link_10'] != '' && strpos( home_url(), $parsed_url_10['host'] ) === FALSE  ) {
								$google_tracking_10 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_10'].'\') )"';
							}
						?>
						<a href="<?php echo $bg_options['buyers_guide_2015_link_10']; ?>" <?php echo $google_tracking_10; ?>>
							<img src="<?php echo $bg_options['buyers_guide_2015_image_10']; ?>">
						</a>
					</div>

				</div>
				<div class="vbg_onethird">

					<div class="vbg_full">
						<?php // 11
							$parsed_url_11		= parse_url( $bg_options['buyers_guide_2015_link_11'] );
							$google_tracking_11	= '';

							if( $bg_options['buyers_guide_2015_link_11'] != '' && strpos( home_url(), $parsed_url_11['host'] ) === FALSE  ) {
								$google_tracking_11 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_11'].'\') )"';
							}
						?>
						<a href="<?php echo $bg_options['buyers_guide_2015_link_11']; ?>" <?php echo $google_tracking_11; ?>>
							<img src="<?php echo $bg_options['buyers_guide_2015_image_11']; ?>">
						</a>
					</div>

					<div class="vbg_row">

						<div class="vbg_onesixth">
							<?php // 12
								$parsed_url_12		= parse_url( $bg_options['buyers_guide_2015_link_12'] );
								$google_tracking_12	= '';

								if( $bg_options['buyers_guide_2015_link_12'] != '' && strpos( home_url(), $parsed_url_12['host'] ) === FALSE  ) {
									$google_tracking_12 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_12'].'\') )"';
								}
							?>
							<a href="<?php echo $bg_options['buyers_guide_2015_link_12']; ?>" <?php echo $google_tracking_12; ?>>
								<img src="<?php echo $bg_options['buyers_guide_2015_image_12']; ?>">
							</a>
						</div>

						<div class="vbg_onesixth vbg_nomargin">

							<div class="vbg_full">
								<?php // 13
									$parsed_url_13		= parse_url( $bg_options['buyers_guide_2015_link_13'] );
									$google_tracking_13	= '';

									if( $bg_options['buyers_guide_2015_link_13'] != '' && strpos( home_url(), $parsed_url_13['host'] ) === FALSE  ) {
										$google_tracking_13 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_13'].'\') )"';
									}
								?>
								<a href="<?php echo $bg_options['buyers_guide_2015_link_13']; ?>" <?php echo $google_tracking_13; ?>>
									<img src="<?php echo $bg_options['buyers_guide_2015_image_13']; ?>">
								</a>
							</div>

							<div class="vbg_full">
								<?php // 14
									$parsed_url_14		= parse_url( $bg_options['buyers_guide_2015_link_14'] );
									$google_tracking_14	= '';

									if( $bg_options['buyers_guide_2015_link_14'] != '' && strpos( home_url(), $parsed_url_14['host'] ) === FALSE  ) {
										$google_tracking_14 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_14'].'\') )"';
									}
								?>
								<a href="<?php echo $bg_options['buyers_guide_2015_link_14']; ?>" <?php echo $google_tracking_14; ?>>
									<img src="<?php echo $bg_options['buyers_guide_2015_image_14']; ?>">
								</a>
							</div>

						</div>
					</div>

					<div class="vbg_full">
						<?php // 15
							$parsed_url_15		= parse_url( $bg_options['buyers_guide_2015_link_15'] );
							$google_tracking_15	= '';

							if( $bg_options['buyers_guide_2015_link_15'] != '' && strpos( home_url(), $parsed_url_15['host'] ) === FALSE  ) {
								$google_tracking_15 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_15'].'\') )"';
							}
						?>
						<a href="<?php echo $bg_options['buyers_guide_2015_link_15']; ?>" <?php echo $google_tracking_15; ?>>
							<img src="<?php echo $bg_options['buyers_guide_2015_image_15']; ?>">
						</a>
					</div>

					<div class="vbg_full">
						<?php // 16
							$parsed_url_16		= parse_url( $bg_options['buyers_guide_2015_link_16'] );
							$google_tracking_16	= '';

							if( $bg_options['buyers_guide_2015_link_16'] != '' && strpos( home_url(), $parsed_url_16['host'] ) === FALSE  ) {
								$google_tracking_16 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_16'].'\') )"';
							}
						?>
						<a href="<?php echo $bg_options['buyers_guide_2015_link_16']; ?>" <?php echo $google_tracking_16; ?>>
							<img src="<?php echo $bg_options['buyers_guide_2015_image_16']; ?>">
						</a>
					</div>

					<div class="vbg_row">

						<div class="vbg_onesixth">
							<?php // 17
								$parsed_url_17		= parse_url( $bg_options['buyers_guide_2015_link_17'] );
								$google_tracking_17	= '';

								if( $bg_options['buyers_guide_2015_link_17'] != '' && strpos( home_url(), $parsed_url_17['host'] ) === FALSE  ) {
									$google_tracking_17 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_17'].'\') )"';
								}
							?>
							<a href="<?php echo $bg_options['buyers_guide_2015_link_17']; ?>" <?php echo $google_tracking_17; ?>>
								<img src="<?php echo $bg_options['buyers_guide_2015_image_17']; ?>">
							</a>
						</div>

						<div class="vbg_onesixth">
							<?php // 18
								$parsed_url_18		= parse_url( $bg_options['buyers_guide_2015_link_18'] );
								$google_tracking_18	= '';

								if( $bg_options['buyers_guide_2015_link_18'] != '' && strpos( home_url(), $parsed_url_18['host'] ) === FALSE  ) {
									$google_tracking_18 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_18'].'\') )"';
								}
							?>
							<a href="<?php echo $bg_options['buyers_guide_2015_link_18']; ?>" <?php echo $google_tracking_18; ?>>
								<img src="<?php echo $bg_options['buyers_guide_2015_image_18']; ?>">
							</a>
						</div>

					</div>
				</div>
			</div>
		</div>


		<div class="vbg_lastthird">

			<div class="vbg_ad">
				<h4>Advertisement</h4>
				<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'side-middle' ) : ''; ?>
			</div>

			<div class="vbg_row">
				<div class="vbg_onethird">

					<div class="vbg_full">
						<?php // 19
							$parsed_url_19		= parse_url( $bg_options['buyers_guide_2015_link_19'] );
							$google_tracking_19	= '';

							if( $bg_options['buyers_guide_2015_link_19'] != '' && strpos( home_url(), $parsed_url_19['host'] ) === FALSE  ) {
								$google_tracking_19 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_19'].'\') )"';
							}
						?>
						<a href="<?php echo $bg_options['buyers_guide_2015_link_19']; ?>" <?php echo $google_tracking_19; ?>>
							<img src="<?php echo $bg_options['buyers_guide_2015_image_19']; ?>">
						</a>
					</div>

					<div class="vbg_row">

						<div class="vbg_onesixth">
							<?php // 20
								$parsed_url_20		= parse_url( $bg_options['buyers_guide_2015_link_20'] );
								$google_tracking_20	= '';

								if( $bg_options['buyers_guide_2015_link_20'] != '' && strpos( home_url(), $parsed_url_20['host'] ) === FALSE  ) {
									$google_tracking_20 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_20'].'\') )"';
								}
							?>
							<a href="<?php echo $bg_options['buyers_guide_2015_link_20']; ?>" <?php echo $google_tracking_20; ?>>
								<img src="<?php echo $bg_options['buyers_guide_2015_image_20']; ?>">
							</a>
						</div>

						<div class="vbg_onesixth">
							<?php // 21
								$parsed_url_21		= parse_url( $bg_options['buyers_guide_2015_link_21'] );
								$google_tracking_21	= '';

								if( $bg_options['buyers_guide_2015_link_21'] != '' && strpos( home_url(), $parsed_url_21['host'] ) === FALSE  ) {
									$google_tracking_21 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_21'].'\') )"';
								}
							?>
							<a href="<?php echo $bg_options['buyers_guide_2015_link_21']; ?>" <?php echo $google_tracking_21; ?>>
								<img src="<?php echo $bg_options['buyers_guide_2015_image_21']; ?>">
							</a>
						</div>

					</div>

					<div class="vbg_full">
						<?php // 22
							$parsed_url_22		= parse_url( $bg_options['buyers_guide_2015_link_22'] );
							$google_tracking_22	= '';

							if( $bg_options['buyers_guide_2015_link_22'] != '' && strpos( home_url(), $parsed_url_22['host'] ) === FALSE  ) {
								$google_tracking_22 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_22'].'\') )"';
							}
						?>
						<a href="<?php echo $bg_options['buyers_guide_2015_link_22']; ?>" <?php echo $google_tracking_22; ?>>
							<img src="<?php echo $bg_options['buyers_guide_2015_image_22']; ?>">
						</a>
					</div>

				</div>
				<div class="vbg_onethird">

					<div class="vbg_onesixth vbg_module">
						<?php // 23
							$parsed_url_23		= parse_url( $bg_options['buyers_guide_2015_link_23'] );
							$google_tracking_23	= '';

							if( $bg_options['buyers_guide_2015_link_23'] != '' && strpos( home_url(), $parsed_url_23['host'] ) === FALSE  ) {
								$google_tracking_23 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_23'].'\') )"';
							}
						?>
						<a href="<?php echo $bg_options['buyers_guide_2015_link_23']; ?>" <?php echo $google_tracking_23; ?>>
							<img src="<?php echo $bg_options['buyers_guide_2015_image_23']; ?>">
						</a>
					</div>

					<div class="vbg_onesixth vbg_module vbg_nomargin">

						<div class="vbg_full">
							<?php // 24
								$parsed_url_24		= parse_url( $bg_options['buyers_guide_2015_link_24'] );
								$google_tracking_24	= '';

								if( $bg_options['buyers_guide_2015_link_24'] != '' && strpos( home_url(), $parsed_url_24['host'] ) === FALSE  ) {
									$google_tracking_24 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_24'].'\') )"';
								}
							?>
							<a href="<?php echo $bg_options['buyers_guide_2015_link_24']; ?>" <?php echo $google_tracking_24; ?>>
								<img src="<?php echo $bg_options['buyers_guide_2015_image_24']; ?>">
							</a>
						</div>

						<div class="vbg_full">
							<?php // 25
								$parsed_url_25		= parse_url( $bg_options['buyers_guide_2015_link_25'] );
								$google_tracking_25	= '';

								if( $bg_options['buyers_guide_2015_link_25'] != '' && strpos( home_url(), $parsed_url_25['host'] ) === FALSE  ) {
									$google_tracking_25 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_25'].'\') )"';
								}
							?>
							<a href="<?php echo $bg_options['buyers_guide_2015_link_25']; ?>" <?php echo $google_tracking_25; ?>>
								<img src="<?php echo $bg_options['buyers_guide_2015_image_25']; ?>">
							</a>
						</div>
					</div>

					<div class="vbg_full">
						<?php // 26
							$parsed_url_26		= parse_url( $bg_options['buyers_guide_2015_link_26'] );
							$google_tracking_26	= '';

							if( $bg_options['buyers_guide_2015_link_26'] != '' && strpos( home_url(), $parsed_url_26['host'] ) === FALSE  ) {
								$google_tracking_26 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2015_link_26'].'\') )"';
							}
						?>
						<a href="<?php echo $bg_options['buyers_guide_2015_link_26']; ?>" <?php echo $google_tracking_26; ?>>
							<img src="<?php echo $bg_options['buyers_guide_2015_image_26']; ?>">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<?php get_footer();
