<?php
/**
 * Template part for displaying standard archive posts for products.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package velonews
 */
?>


<article id="post-<?php the_ID(); ?>" <?php post_class( array( 'article', 'article_type_archive-result' ) ); ?>>
	<?php
		if( has_post_thumbnail() ) {
			$thumb_id	= get_post_thumbnail_id();
			$thumbnail	= wp_get_attachment_image_src( $thumb_id, 'meganav-thumb' );

			echo '<a href="'. get_permalink() .'" target="_self"><img src="'. $thumbnail[0] .'" class="visible-md visible-lg article__thumbnail"></a>';
		}
	?>
	<div class="article__body__wrap">
		<h3 class="article__title">
			<a href="<?php echo get_permalink(); ?>" target="_self" class="article__permalink"><?php the_title(); ?></a>
		</h3>

		<?php
			if( 'apparel-reviews' === get_post_type() ) {
				$review = get_post_meta( get_the_ID(), '_vn_apparel_review', 1 );
			} elseif( 'bike-reviews' === get_post_type() ) {
				$review = get_post_meta( get_the_ID(), '_vn_bike_review_bike_review', 1 );
			}

			if( isset( $review ) && $review != '' ) {
				$review_140 = substr( $review, 0, ( 140 - 3 ) );
				$review_140 = preg_replace( '/ [^ ]*$/', ' ...', $review_140 );
			} else {
				$review_140 = '';
			}
		?>

		<p class="article__excerpt"><?php echo $review_140; ?></p>
		<p class="article__meta">
			<?php
				$categories	= get_the_category();
				$separator	= ' ';
				$output		= '';

				if ( !empty( $categories ) ) {
					foreach( $categories as $category ) {
						$output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '" class="article_category_'. $category->category_nicename .'"><span class="article__category">' . esc_html( $category->name ) . '</span></a>' . $separator;
					}
					echo trim( $output, $separator );
				}
			?>
		</p>
	</div>
</article><!-- #post-## -->
