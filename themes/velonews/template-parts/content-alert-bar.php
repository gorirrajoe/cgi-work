<?php
/**
 * Template part for displaying alert bar
 *
 * will display if "display alert ribbon" is checked ON in options page
 *
 * @package velonews
 */


	echo '<section class="breaking-news">
		<div class="container breaking-news__container">
			<p>'. cgi_bikes_get_option( 'ticker_text' ) .' <a href="'. cgi_bikes_get_option( 'ticker_link' ) .'">Read More &raquo;</a></p>
		</div>
	</section>';
?>