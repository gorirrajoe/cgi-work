<?php
/**
 * Template part for displaying apparel reviews.
 *
 * @package velonews
 */

	$prefix			= '_vn_';
	$post_classes	= array( 'col-xs-12', 'col-sm-7', 'col-md-8', 'article', 'article_type_bike-review' );

	/**
	 * set metadata to variables
	 */
	$msrp		= number_format( get_post_meta( get_the_ID(), $prefix . 'apparel_msrp', 1 ), 2 );
	$msrp_note	= get_post_meta( get_the_ID(), $prefix . 'apparel_msrp_notes', 1 );
	$weight		= get_post_meta( get_the_ID(), $prefix . 'weight', 1 );
	$size		= get_post_meta( get_the_ID(), $prefix . 'size', 1 );
	$stores		= get_post_meta( get_the_ID(), $prefix . 'stores', 1 );
	$rating		= get_post_meta( get_the_ID(), $prefix . 'rating', 1 );
	$gender		= get_post_meta( get_the_ID(), $prefix . 'apparel_gender', 1 );
	$type		= get_post_meta( get_the_ID(), $prefix . 'apparel_type', 1 );
	$bg_author	= get_post_meta( get_the_ID(), $prefix . 'review_author',1) != '' ? true : false;

	$cats		= get_the_category();
	$cat_array	= [];
	$in_guide	= false;

	//check if in gift guide or buyers guide after 2017, set in_guide true
	foreach( $cats as $key => $value ) {
		if( strpos( $value->slug, 'guide' ) != false ) {

			//get the year for the guide
			preg_match_all( '!\d+!', $value->slug, $guide_year );
			$guide_year = $guide_year[0][0];

			//start with 2017 gift guide and 2018 buyers guide
			if( ( $guide_year > 2016 && strpos($value->slug, 'gift') != false ) || ( $guide_year > 2017 && strpos( $value->slug, 'buy' ) != false ) ) {

				$in_guide					= true;
				$cat_array[$value->slug]	= $value->name;
				$guide_slug					= current( array_keys( $cat_array ) );
				$guide_name					= $cat_array[$guide_slug];
				$guide_link					= get_category_link( get_category_by_slug( $guide_slug ) );

			}
		}
	}

	if( $in_guide ) { ?>

		<div class="template__bg-final">
			<section id="guides">
				<div class="col-xs-12 bg__final_title">
					<h1><a href="<?php echo( $guide_link );?>"><?php echo $guide_name; ?></a></h1>
				</div>
				<div class="guide__menu">

					<?php
						wp_nav_menu( array(
							'theme_location'	=> $guide_slug,
							'menu_class'		=> 'bg__final_top_menu'
						) );
					?>

				</div>
			</section>
		</div>
	<?php }
?>

<article id="article" <?php post_class( $post_classes ); ?>>

	<?php
		/**
		 * breadcrumb
		 */
	?>
	<ol class="article__bread-crumb">
		<li><a href="<?php echo site_url() ?>" target="_self">Home</a></li>

		<?php if( $in_guide ) {
			echo '&raquo;
				<li>
				<a href="'.site_url() . '/category/' . $guide_slug . '" target="_self">' . $guide_name .'</a>
				</li>' ;
		}?>
		&raquo;
		<li><a href="<?php echo site_url( '/apparel-search/' ); ?>">Gear &amp; Apparel Reviews</a></li>
		&raquo;
		<li><?php the_title(); ?></li>
	</ol>

	<header class="article__header">
		<h1 class="article__header__title"><?php echo get_the_title(); ?></h1>

		<?php velonews_posted_on(); ?>

		<ul class="article__header__score">
			<?php if( $rating != '' ) { ?>
				<li>
					RATING <strong><?php echo $rating; ?>/10</strong>
				</li>
			<?php }

			/**
			 * don't show gender if the product is a wheel
			 */
			if( strpos( $type, 'wheels' ) ) {
				if( $gender == 'm' ) {
					$gender_display = 'Men';
				} elseif( $gender == 'f' ) {
					$gender_display = 'Women';
				} else {
					$gender_display = 'Unisex';
				}
				echo '<li>GENDER <strong>'. $gender_display .'</strong></li>';
			}

			if( $msrp != '' ) { ?>
				<li>
					PRICE <strong>$<?php echo $msrp; ?></strong>
					<?php if( $msrp_note != '' ) {
						echo '('. $msrp_note .')';
					} ?>
				</li>
			<?php }

			if( $weight ) { ?>
				<li>WEIGHT <strong><?php echo $weight; ?></strong></li>
			<?php }

			if( $size ) { ?>
				<li>SIZE <strong><?php echo $size; ?></strong></li>
			<?php } ?>
		</ul>
	</header>


	<section class="article__body article__review row">
		<?php
			/**
			 * social sharing
			 */
			get_social_sharing( 'side' );
		?>

		<div id="article-right" class="col-xs-12 col-md-11">

			<?php
				get_social_sharing( 'top' );

				/**
				 * owl carousel gallery and ratings row
				 */
				$gallery_imgs	= get_post_meta( get_the_ID(), $prefix . 'apparel_gallery', 1 );
				$imgid			= get_post_thumbnail_id( get_the_ID() );
				$img			= wp_get_attachment_image_src( $imgid, 'image-gallery' );
				$img_obj		= get_post( $imgid );
				$img_large		= wp_get_attachment_image_src( $imgid, 'full' );

				if( count( $gallery_imgs ) > 1 ) { ?>
					<div id="gallery-1" class="gallery owl-carousel generic-carousel">
						<?php
							foreach( $gallery_imgs as $imgid => $imgurl ) {
								$img		= wp_get_attachment_image_src( $imgid, 'image-gallery' );
								$img_obj	= get_post( $imgid );
								echo '<figure class="gallery-item">
									<a data-fancybox="gallery-1" data-caption="'. $img_obj->post_excerpt .'" href="'. $img[0] .'"><img class="owl-lazy" data-src="'. $img[0] .'"></a>
									<figcaption class="wp-caption-text gallery-caption">'. $img_obj->post_excerpt .'</figcaption>
								</figure>';
							}
						?>
					</div>
				<?php } else {
					/**
					 * show landscape-sized image here
					 */
					if( $img[1] > $img[2] ) {
						echo '<figure>
							<a href="'. $img_large[0] .'" data-fancybox data-caption="'. $img_obj->post_excerpt .'"><img alt="'. $img_obj->post_excerpt .'" src="'. $img[0] .'"></a>
							<figcaption class="wp-caption-text gallery-caption">'. $img_obj->post_excerpt .'</figcaption>
						</figure>';
					}
				}


				/**
				 * stores
				 */
				$stores = ( $stores != '' ) ? array_filter( $stores ) : '';

				if( !empty( $stores ) ) { ?>

					<div class="row article__review__overview">
						<div class="col-xs-12">
							<?php
								echo '<h2>WHERE TO BUY</h2>
								<p class="article__review__msrp"><strong>MSRP:</strong> $'. $msrp .'</p>
								<ul class="article__review__vendors">';
									foreach( $stores as $key => $entry ) {
										echo '<li><a href="'. $entry['store_url'] .'" target="_blank" class="btn btn--red">'. $entry['store_name'] .'</a></li>';
									}
								echo '</ul>';
							?>
						</div>

					</div>

				<?php }

				echo '<div class="entry-content">';
					/**
					 * if image is portrait, use a diff image and align it left
					 */
					if( $img[1] < $img[2] ) {
						$img = wp_get_attachment_image_src( $imgid, 'large' );
						echo '<figure class="alignleft">
							<a href="'. $img_large[0] .'" data-fancybox data-caption="'. $img_obj->post_excerpt .'"><img alt="'. $img_obj->post_excerpt .'" src="'. $img[0] .'"></a>
							<figcaption class="wp-caption-text gallery-caption">'. $img_obj->post_excerpt .'</figcaption>
						</figure>';
					}

					echo apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'apparel_review', 1 ) );

					/**
					 * gift guide stuffs
					 */
					if( $in_guide ) {
						$reviewer_id	= get_post_meta( get_the_ID(), '_vn_gg_author_id', 1 ) != '' ? get_post_meta( get_the_ID(), '_vn_gg_author_id', 1 ) : '';
						$rating			= get_post_meta( get_the_ID(), '_vn_gg_product_rating', 1 ) != 'none' ? get_post_meta( get_the_ID(), '_vn_gg_product_rating', 1 ) : '';


						if( $reviewer_id != '' || $rating != '' ) {
							echo '<div class="row">';
						}


						if( $reviewer_id != '' ) {

							$author_meta	= get_user_meta( $reviewer_id );
							$author_name	= get_the_author_meta( 'display_name', $reviewer_id );

							$reviewer_output = '<div class="col-md-6">
								<div class="reviewer_meta">';

									$template_directory		= get_template_directory_uri();
									$author_image			= get_template_directory() .'/images/authors/'. $reviewer_id .'.jpg';
									$author_image_display	= $template_directory .'/images/authors/'. $reviewer_id .'.jpg';

									if ( file_exists( $author_image ) ) {
										$reviewer_output .= '<img class="reviewer_meta__image" src="'. $author_image_display .'" alt="'. $author_name .'">';
									} else {
										$reviewer_output .= get_avatar( $reviewer_id, 120, '', $author_name, array( 'class' => 'reviewer_meta__image' ) );
									}

									$reviewer_output .= '<p class="reviewer_meta__name"><strong>Reviewed by:</strong><br>' .
									$author_name . '</p>
									<p class="reviewer_meta__bio">'. $author_meta['description'][0] .'</p>

								</div>
							</div>';

							echo $reviewer_output;

						}


						echo '<div class="col-md-6">';

							if( $rating != '' ) {

								$stars = '<div class="stars_rating">';

									$total		= 5;
									$floor		= floor( $rating );
									$ceiling	= ceil( $rating );
									$remaining	= $total - $ceiling;

									if( strpos( $rating, '.' ) !== false ) {

										for( $i = 0; $i < $floor; $i++ ) {
											$stars .= '<span class="fa fa-star" aria-hidden="true"></span>';
										}
										$stars .= '<span class="fa fa-star-half-o"></span>';

									} else {

										for( $i = 0; $i < $rating; $i++ ) {
											$stars .= '<span class="fa fa-star"></span>';
										}

									}

									for( $x = 0; $x < $remaining; $x++ ) {
										$stars .= '<span class="fa fa-star-o"></span>';
									}

								$stars .= '</div>';

								echo $stars;

							}


							$buy_url = get_post_meta( get_the_ID(), '_vn_gg_buy_url', 1 ) != '' ? get_post_meta( get_the_ID(), '_vn_gg_buy_url', 1 ) : '';

							if( $buy_url != '' ) {

								echo '<a class="btn--red" href="'. $buy_url .'" onClick="_gaq.push( [\'_trackEvent\', \'Triathlete Gift Guide\', \'Buy Now Button\', \''. $buy_url .'\'] );" target="_blank">Buy Now</a>';

							}

						echo '</div>';


						if( $reviewer_id != '' || $rating != '' ) {
							echo '</div>';
						}
					}

				echo '</div>';
			?>

		</div>
	</section>


	<?php
		if( $in_guide ) {
			/**
			 * get author bio for buyer/gift guide
			 */
			if( $bg_author !== false ) {
				$authorurl		= get_author_posts_url( get_the_author_meta( 'ID' ) );
				$displayname	= get_the_author_meta( 'display_name' );
				$gravatar		= get_avatar( get_the_author_meta( 'ID' ), 120, '', $displayname );
				$avatarurl		= get_the_author_meta( '_vn_avatar' );
				$avatarid		= get_attachment_id_from_src( $avatarurl );
				$avatarsized	= wp_get_attachment_image_src( $avatarid, 'thumbnail' );
				$bio			= apply_filters( 'the_content', get_the_author_meta( 'description' ) );
				$twitter		= get_the_author_meta( '_vn_twitter' );
				$facebook		= get_the_author_meta( '_vn_facebook' );
				$instagram		= get_the_author_meta( '_vn_instagram' );
				$pinterest		= get_the_author_meta( '_vn_pinterest' );
				$googleplus		= get_the_author_meta( '_vn_google_plus' );


				if( $avatarsized[0] ) {
					$author_avatar = '<img src="'. $avatarsized[0] .'">';
				} else {
					$author_avatar = $gravatar;
				}

				$authorbio_div = '<footer class="article__footer">
					<div class="row article__author">
						<div class="col-xs-3 article__author__thumbnail">
							<a href="'. $authorurl .'">'. $author_avatar .'</a>
						</div>

						<div class="col-xs-9 article__author__bio">
							<h3>By <a href="'. $authorurl .'">'. $displayname .'</a></h3>'.

							$bio .'

							<ul class="article__author__social">';
								if( $twitter ) {
									$authorbio_div .= '<li>
										<a href="'. $twitter .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Twitter</span><span class="fa fa-twitter"></span></a>
									</li>';
								}
								if( $facebook ) {
									$authorbio_div .= '<li>
										<a href="'. $facebook .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Facebook</span>
										<span class="fa fa-facebook"></span></a>
									</li>';
								}
								if( $instagram ) {
									$authorbio_div .= '<li>
										<a href="'. $instagram .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Instagram</span>
										<span class="fa fa-instagram"></span></a>
									</li>';
								}
								if( $pinterest ) {
									$authorbio_div .= '<li>
										<a href="'. $pinterest .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Instagram</span>
										<span class="fa fa-pinterest-p"></span></a>
									</li>';
								}
								if( $googleplus ) {
									$authorbio_div .= '<li>
										<a href="'. $googleplus .'" target="_blank"><span class="screen-reader-text">Follow '. $displayname .' on Instagram</span>
										<span class="fa fa-google-plus"></span></a>
									</li>';
								}
							$authorbio_div .= '</ul>
						</div>
					</div>
				</footer>';

				echo $authorbio_div;

			}
		} else {
			/**
			 * get author bio
			 */
			echo get_author_info();
		}
	?>
</article>
