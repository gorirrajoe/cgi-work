<?php
/**
 * Template part for displaying 2 large thumbnailed posts.
 *
 * set $featured_IDs in page calling this template-part file
 *
 * @package velonews
 */
global $post_id_array;


if( is_category() && ( $two_across = get_term_meta( $cat_id, '_vn_two-across', 1 ) ) != '' ) {
	if( false === ( $two_query = get_transient( $cat_nicename . '_two_query' ) ) ) {
		$featured_array = explode( ',', $two_across );
		$args = array(
			'orderby'					=> 'post__in',
			'post_status'				=> 'publish',
			'post__in'					=> $featured_array,
			'post_type'					=> 'any',
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false
		);
		$two_query = new WP_Query( $args );
		set_transient( $cat_nicename . '_two_query', $two_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}
} elseif ( 'races_event' == get_post_type() ) {
	$two_across = wp_get_post_terms( $cat_id, '_vn_two-across', 1 ) ?  get_term_meta( $cat_id, '_vn_two-across', 1 ) : cgi_bikes_get_option( 'featured_articles' );

	if( false === ( $two_query = get_transient( $cat_short_name . '_vn_two_query' ) ) ) {
		$featured_array = explode( ',', $two_across );
		$args = array(
			'orderby'					=> 'post__in',
			'post__in'					=> $featured_array,
			'post_type'					=> 'any',
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false
		);
		$two_query = new WP_Query( $args );
		set_transient( $cat_short_name . '_vn_two_query', $two_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}

} elseif( is_page_template() && ( $two_across = get_post_meta( get_the_ID(), '_vn_featured_articles', 1 ) ) ) {
	$page_template_pieces = explode( '.', basename( get_page_template() ) );
	$featured_array = explode( ',', $two_across );

	if( false === ( $two_query = get_transient( 'template_'. $page_template_pieces[0].'_two_query' ) ) ) {
		$args = array(
			'orderby'					=> 'post__in',
			'post_status'				=> 'publish',
			'post__in'					=> $featured_array,
			'ignore_sticky_posts'		=> true,
			'post_type'					=> 'any',
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false
		);
		$two_query = new WP_Query( $args );
		set_transient( 'template_'. $page_template_pieces[0].'_two_query', $two_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}

} else {
	// use global velo options
	$global_two_across = cgi_bikes_get_option( 'featured_articles' );

	if( false === ( $two_query = get_transient( 'global_two_query' ) ) ) {
		$featured_array = explode( ',', $global_two_across );
		$args = array(
			'orderby'					=> 'post__in',
			'post__in'					=> $featured_array,
			'ignore_sticky_posts'		=> true,
			'post_type'					=> 'any',
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false
		);
		$two_query = new WP_Query( $args );
		set_transient( 'global_two_query', $two_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}
}


if( $two_query->have_posts() ) {
	echo '<section class="row content__section">';
		while( $two_query->have_posts() ) {
			$two_query->the_post();

			$post_id_array[] = get_the_ID();

			$cat_array	= get_cat_array( get_the_ID() );
			$thumbnail	= get_the_post_thumbnail_url( get_the_ID(), 'featured-thumb-lg' );

			$title		= vn_get_post_title( get_the_ID() );
			//$post_id_array[]	= get_the_ID();

			$article_classes	= '';
			if( is_array( $cat_array['slug'] ) ) {
				foreach( $cat_array['slug'] as $slug ) {
					$article_classes .= ' article_category_' . $slug;
				}
			} else {
				$article_classes .= ' article_category_' . $cat_array['slug'];
			}

			echo '<article class="col-xs-12 col-sm-6 article article_type_sponsored article_category_sponsored' . $article_classes . '">
				<a href="'. get_the_permalink() .'" target="_self" class="article__permalink" title="'. $title .'">
					<img data-original="'. $thumbnail .'" alt="" class="article__thumbnail lazyload">
					<div class="article__img-filter"></div>
					<div class="article__category">'. get_article_category_tag( get_the_ID(), $cat_array ) .'</div>
					<p class="article__title">'. $title .'</p>
				</a>
			</article>';
		}
		wp_reset_postdata();
	echo '</section>';
}


?>


