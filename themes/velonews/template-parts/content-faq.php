<?php
/**
 * Template part for displaying standard single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package velonews
 */
	$prefix = '_vn_';

?>


<article id="article" <?php post_class( array( 'col-xs-12', 'col-sm-7', 'col-md-8', 'article' ) ); ?>>

	<?php
		/**
		 * breadcrumb
		 */
		get_breadcrumb();
	?>

	<header class="article__header">

		<?php the_title( '<h1 class="article__header__title">', '</h1>' ); ?>

	</header>


	<section class="article__body row">
		<?php
			/**
			 * social sharing
			 */
			get_social_sharing( 'side' );

			echo '<div id="article-right" class="col-xs-12 col-md-11">';

				get_social_sharing( 'top' );

				$content_string = apply_filters( 'the_content', get_the_content() );
				echo $content_string;

				/**
				 * cmb2 stuff
				 */

				$mag_questions = get_post_meta( get_the_ID(), $prefix . 'mag_questions', 1 );
				if( !empty( $mag_questions ) ) {
					echo '<h2>Magazine Questions</h2>
					<ul class="faq__list">';
						foreach( $mag_questions as $key => $entry ) {
							echo '<li>
								<a href="#faq-mag'. $key .'" data-toggle="collapse" aria-expanded="false" aria-controls="faq-mag'. $key .'">'. $entry['question'] .'</a>
								<div class="collapse" id="faq-mag'. $key .'" data-toggle="collapse" aria-expanded="false" aria-controls="faq-mag'. $key .'">
									<div class="well">
										'. apply_filters( 'the_content', $entry['answer'] ) .'
									</div>
								</div>
							</li>';
						}
					echo '</ul>';
				}


				$web_questions = get_post_meta( get_the_ID(), $prefix . 'web_questions', 1 );
				if( !empty( $web_questions ) ) {
					echo '<h2>Website Feedback?</h2>
					<ul class="faq__list">';
						foreach( $web_questions as $key => $entry ) {
							echo '<li>
								<a href="#faq-web'. $key .'" data-toggle="collapse" aria-expanded="false" aria-controls="faq-web'. $key .'">'. $entry['question'] .'</a>
								<div class="collapse" id="faq-web'. $key .'" data-toggle="collapse" aria-expanded="false" aria-controls="faq-web'. $key .'">
									<div class="well">
										'. apply_filters( 'the_content', $entry['answer'] ) .'
									</div>
								</div>
							</li>';
						}
					echo '</ul>';
				}


				$contact_questions = get_post_meta( get_the_ID(), $prefix . 'contact_questions', 1 );
				if( !empty( $contact_questions ) ) {
					echo '<h2>Contacting Us?</h2>
					<ul class="faq__list">';
						foreach( $contact_questions as $key => $entry ) {
							echo '<li>
								<a href="#faq-contact'. $key .'" data-toggle="collapse" aria-expanded="false" aria-controls="faq-contact'. $key .'">'. $entry['question'] .'</a>
								<div class="collapse" id="faq-contact'. $key .'" data-toggle="collapse" aria-expanded="false" aria-controls="faq-contact'. $key .'">
									<div class="well">
										'. apply_filters( 'the_content', $entry['answer'] ) .'
									</div>
								</div>
							</li>';
						}
					echo '</ul>';
				}


				$contribute_questions = get_post_meta( get_the_ID(), $prefix . 'contribute_questions', 1 );
				if( !empty( $contribute_questions ) ) {
					echo '<h2>Contributing to VeloNews</h2>
					<ul class="faq__list">';
						foreach( $contribute_questions as $key => $entry ) {
							echo '<li>
								<a href="#faq-contribute'. $key .'" data-toggle="collapse" aria-expanded="false" aria-controls="faq-contribute'. $key .'">'. $entry['question'] .'</a>
								<div class="collapse" id="faq-contribute'. $key .'" data-toggle="collapse" aria-expanded="false" aria-controls="faq-contribute'. $key .'">
									<div class="well">
										'. apply_filters( 'the_content', $entry['answer'] ) .'
									</div>
								</div>
							</li>';
						}
					echo '</ul>';
				}


				$ad_questions = get_post_meta( get_the_ID(), $prefix . 'ad_questions', 1 );
				if( !empty( $ad_questions ) ) {
					echo '<h2>Advertising</h2>
					<ul class="faq__list">';
						foreach( $ad_questions as $key => $entry ) {
							echo '<li>
								<a href="#faq-ad'. $key .'" data-toggle="collapse" aria-expanded="false" aria-controls="faq-ad'. $key .'">'. $entry['question'] .'</a>
								<div class="collapse" id="faq-ad'. $key .'" data-toggle="collapse" aria-expanded="false" aria-controls="faq-ad'. $key .'">
									<div class="well">
										'. apply_filters( 'the_content', $entry['answer'] ) .'
									</div>
								</div>
							</li>';
						}
					echo '</ul>';
				}


				$job_questions = get_post_meta( get_the_ID(), $prefix . 'job_questions', 1 );
				if( !empty( $job_questions ) ) {
					echo '<h2>Jobs and Internships</h2>
					<ul class="faq__list">';
						foreach( $job_questions as $key => $entry ) {
							echo '<li>
								<a href="#faq-jobs'. $key .'" data-toggle="collapse" aria-expanded="false" aria-controls="faq-jobs'. $key .'">'. $entry['question'] .'</a>
								<div class="collapse" id="faq-jobs'. $key .'" data-toggle="collapse" aria-expanded="false" aria-controls="faq-jobs'. $key .'">
									<div class="well">
										'. apply_filters( 'the_content', $entry['answer'] ) .'
									</div>
								</div>
							</li>';
						}
					echo '</ul>';
				}

			echo '</div>';
		?>
	</section>


</article><!-- #article-->

