<?php
/**
 * Template part for displaying race without stages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package velonews
 */

	//Get the category for year and race name
	$post_id = get_the_ID();

	$race_cat = wp_get_post_terms( $post_id, 'stagecat');
	$race_cat = ( isset( $race_cat[0]->term_id ) ? $race_cat[0]->term_id : '' );

	$race_year = wp_get_post_terms( $post_id, 'stageyear');
	$race_year = ( isset( $race_year[0]->term_id ) ? $race_year[0]->term_id : '' );

	$race_overview = wpautop( get_post_meta( $post_id, '_vn_race_description', true ) );
	$race_start_list = wpautop( get_post_meta( $post_id, '_vn_race_start_list', true ) );
	$race_report = wpautop( get_post_meta( $post_id, '_vn_race_report', true ) );
?>

	<div class="row">

		<section class="col-xs-12 col-sm-7 col-md-8 race">

			<article class="article">

				<div class="race__nav__wrap race__nav__wrap--with-border">

					<ul id="race-single-nav" class="nav nav-pills race__nav" role="tablist">
						<li role="presentation" class="active">
							<a href="#race-details" aria-controls="race-details" role="tab" data-toggle="tab">RACE DETAILS</a>
						</li>
						<li role="presentation">
							<a href="#race-report" aria-controls="race-report" role="tab" data-toggle="tab">RACE REPORT / RESULTS</a>
						</li>
					</ul>

				</div>

				<div class="tab-content">

					<div id="race-details" class="tab-pane race-single__pane active" role="tabpanel">

						<h3>Race Overview</h3>
						<?php echo $race_overview ?>

						<h3>Start List</h3>
						<?php echo $race_start_list ?>

					</div>

					<div id="race-report" class="tab-pane race-single__pane" role="tabpanel">
						<?php echo $race_report ?>
					</div>

				</div>

			</article>

		</section>

		<?php get_sidebar( 'right' ); ?>

	</div>
