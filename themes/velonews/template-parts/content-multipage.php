<?php
/**
 * Template part for displaying standard single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package velonews
 */
global $multipage;
$sponsorship = get_presented_by();
?>


<article id="article" <?php post_class( array( 'col-xs-12', 'col-sm-7', 'col-md-8', 'article' ) ); ?>>

	<?php
		/**
		 * breadcrumb
		 */
		get_breadcrumb();
	?>

	<header class="article__header">

		<?php the_title( '<h1 class="article__header__title">', '</h1>' );

		if ( 'post' === get_post_type() ) {
			velonews_posted_on();
		} ?>
	</header>

	<?php
		echo $sponsorship['content'];
	?>

	<section class="article__body row">
		<?php
			/**
			 * social sharing
			 */
			get_social_sharing( 'side' );

			echo '<div id="article-right" class="col-xs-12 col-md-11">';

				get_social_sharing( 'top' );

				insert_featured_image( get_the_ID() );

				$content_string = apply_filters( 'the_content', get_the_content() );

				global $posts;
				$pagetitlestring = '/<!--pagetitle:(.*?)-->/';
				$content = $posts[0]->post_content;
				preg_match_all( $pagetitlestring, $content, $titlesarray, PREG_PATTERN_ORDER );

				$pagetitles = $titlesarray[1];
				$i = 1;
				echo '<aside class="recommended-content alignleft">
					<div class="recommended-content__wrap">
						<h3 class="recommended-content__title">Table of Contents</h3>
						<ul class="recommended-content__menu">';

							foreach( $pagetitles as $pagetitle ) {
								echo '<li><a href="'. get_permalink() . '/' . $i .'" target="_self">'. $pagetitle .'</a></li>';
								$i++;
							}

						echo '</ul>
					</div>
				</aside>'
				. $content_string;


				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'velonews' ),
					'after'  => '</div>',
					'link_before' => '<span class="page-link-number" aria-label="Go To Page %">',
					'link_after' => '</span>'
				) );

			echo '</div>';

		?>
	</section>


	<?php
		/**
		 * get author bio
		 */
		echo get_author_info();
	?>

</article><!-- #article -->

