<?php
/**
 * Template part for displaying race table
 *
 * category, center column
 *
 * @package velonews
 */

global $post_id_array;

$original_cat_id 	= $cat_id; //get the current cat_id since it will be changed for race info
$post_id_array		= array();
$current_cat 		= $original_cat = get_queried_object()->slug;
$sponsorship		= get_presented_by();
$race_year 			= get_term_meta( $cat_id, '_vn_race-year', 1 ) ? get_term_meta( $cat_id, '_vn_race-year', 1 ) : date('Y') - 1;//set default to last year
$results_after_race =  'Results will be published once race is underway.';


//get all race categories and get the related category name
$race_stagecats = get_terms( array('taxonomy'=>'stagecat') );
$race_categories = [];
foreach ($race_stagecats as $key => $value) {
	array_push($race_categories, str_replace( '-race', '', $value->slug ) );
}

//check for current category as archive of race category
if( !in_array( $current_cat, $race_categories ) ) { //if not in the slug array
	foreach ($race_categories as $key => $value) {
		if( strpos($current_cat, $value )  ) {
			$current_cat = $value;//set the value to the slug for the race name
		}
	}
}

//setup race name and
$cat_id				= get_category_by_slug( $current_cat )->term_id;
$race_name 			= get_the_category_by_id( $cat_id ) . ' Race';
$stagecat_category  = $current_cat . '-race';

// query db for the race id
	$race_id = 'SELECT pk_race_id from wp_races WHERE race_name = "' . $race_name . '" AND race_year ="' . $race_year . '" ';
	$race_id_result = $wpdb->get_results( $race_id );

	if( !empty( $race_id_result ) ) {
		$race_id_num = $race_id_result[0]->pk_race_id;
	}
	// Get the stages for the race id
	if( isset( $race_id_num ) ) {
		$stage_results_info = 'SELECT wp_riders.*, wp_stage_results.* FROM wp_stage_results INNER JOIN wp_riders WHERE wp_riders.pk_rider_id = wp_stage_results.fk_rider_id  AND fk_race_id = "' . $race_id_num . '"';
		$results_info = $wpdb->get_results( $stage_results_info );
	}

/**
 * race table
 */

if( false === ( $race_query = get_transient( 'race_query' . $race_name . '_' . $race_year ) ) ) {
	$race_args = array(
		'post_type'			=> 'races_event',
		'posts_per_page'	=> 1,
		'tax_query'			=> array(
			'relation'		=> 'AND',
			array(
				'taxonomy'	=>'stagecat',
				'field'		=> 'slug',
				'terms'		=> $stagecat_category,
			 ),
			array(
				'taxonomy'	=>'stageyear',
				'field'		=> 'slug',
				'terms'		=> $race_year
			 ),
		 )
	 );
	$race_query = new WP_Query( $race_args );

	set_transient( 'race_query' . $race_name . '_' . $race_year, $race_query, 15 * MINUTE_IN_SECONDS );
}


//get the post ID for the race_result post
$race_posts = $race_query->posts;//get the posts in the race category
$race_post_id = !empty( $race_posts ) ? $race_posts[0]->ID : '';//if there is a result, get the id for the race_results post
$race_overview		= get_post_meta( $race_post_id, '_vn_race_description', 1 );
$race_start_list	= get_post_meta( $race_post_id, '_vn_race_start_list', 1 );
$race_tech_feed		= get_post_meta( $race_post_id, '_vn_race_tech', 1 );
// $ttt 				= get_term_meta( $race_post_id, '_vn_stages_stage_ttt', 1);
// var_dump( $ttt );

?>

<section class="content__section content__flex_position_second">
<?php
	//5 top articles for race categories
	if( false === ( $category5_articles = get_transient( 'category_'. $original_cat .'_top5_articles' ) ) ) {
		// set or get transient category for 5 articles related to the race
		$args = array(
			'ignore_sticky_posts'	=> true,
			'post_status'			=> 'publish',
			'posts_per_page'		=> 5,
			'has_password'			=> false,
			'category_name'			=> $original_cat, // posts that have the category for the race
			'offset'				=> 10

		 );
		$category5_articles = new WP_Query( $args );
		set_transient( 'category_'. $original_cat .'_top5_articles', $category5_articles, 1 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}

	if( $category5_articles->have_posts() ) {
		echo '<section class="col-xs-12 owl-carousel generic-carousel">';
			// fill the slides with related articles
			while( $category5_articles->have_posts() ) {
				$category5_articles->the_post();
				$the_id 			= get_the_ID();
				$thumb_id			= get_post_thumbnail_id();
				$thumbnail			= wp_get_attachment_image_src( $thumb_id, 'featured-thumb-lg' );
				$cat_array			= get_cat_array( $the_id );
				$post_id_array[]	= $the_id;

				$article_classes	= '';
				if( is_array( $cat_array['slug'] ) ) {
					foreach( $cat_array['slug'] as $slug ) {
						$article_classes .= ' article_category_' . $slug;
					}
				} else {
					$article_classes .= ' article_category_' . $cat_array['slug'];
				}

				echo '
					<article class="item article article_type_latest article_type_latest--featured' . $article_classes .'">
						<a href="'. get_permalink() .'" target="_self" class="article__permalink" title="'. get_the_title() .'">
							<div class="article__thumbnail__wrap">
								<img data-src="'. $thumbnail[0] .'" alt="" class="owl-lazy article__thumbnail">
								<div class="article__thumbnail__overlay"></div>
							</div>
							<div class="article__category">'. get_article_category_tag( get_the_ID(), $cat_array ) .'</div>
							<p class="article__title">'. get_the_title() .'</p>
						</a>
					</article>
				';
			}
		echo '</section>';
	}
	wp_reset_postdata();

	//racetable for category page
	if( $race_query->have_posts() ) :
		while( $race_query->have_posts() ) :
			$race_query->the_post(); ?>

			<section class="col-xs-12 race-table">
				<div class="race-table__wrap">
					<ul id="race-table-nav" class="nav nav-pills race-table__nav" role="tablist">
						<li role="presentation" class="active">
							<a href="#overview" aria-controls="overview" role="tab" data-toggle="tab" aria-expanded="true">OVERVIEW</a>
						</li>
						<li role="presentation" class="">
							<a href="#stages" aria-controls="stages" role="tab" data-toggle="tab" aria-expanded="false">STAGES</a>
						</li>
						<li role="presentation" class="">
							<a href="#gc-top-ten" aria-controls="gc-top-ten" role="tab" data-toggle="tab" aria-expanded="false">CURRENT GC</a>
						</li>
						<li role="presentation" class="">
							<a href="#start-list" aria-controls="start-list" role="tab" data-toggle="tab" aria-expanded="false">START LIST</a>
						</li>
						<li role="presentation" class="">
							<a href="#tech-feed" aria-controls="tech-feed" role="tab" data-toggle="tab" aria-expanded="false">TECH FEED</a>
						</li>
					</ul>
					<div class="tab-content race-table__content">

						<?php
							/**
							 * overview tab
							 */
						?>
						<div id="overview" class="tab-pane race-table__pane active" role="tabpanel">
							<div class="race-table__pane__wrap">
								<?php if( !empty( $race_overview ) ) {
									echo apply_filters( 'the_content', $race_overview );
								} else {
										echo '<p>'. $results_after_race . '</p>';
								} ?>
							</div>
						</div>


						<?php
							/**
							 * stages tab
							 */
						?>
						<div id="stages" class="tab-pane race-table__pane" role="tabpanel">

							<?php
								// wp_reset_postdata();

								// get stages based on race category and race year category sorted by stage number
								$stage_args = array(
									'post_type'			=> 'stage',
									'posts_per_page'	=> -1,
									'orderby'			=> 'meta_value_num',
									'meta_key'			=> '_vn_stages_stage_number',
									'order'				=> 'ASC',
									'tax_query'			=> array(
										'operator'		=> 'AND',
										array(
											'taxonomy'	=> 'stagecat',
											'field'		=> 'slug',
											'terms'		=> $stagecat_category
										 ),
										array(
											'taxonomy'	=> 'stageyear',
											'field'		=> 'slug',
											'terms'		=> $race_year
										 )
									 )
								 );

								$stage_posts = new WP_Query( $stage_args );


								if( $stage_posts->have_posts() ) {
									echo '<table class="race-table__table">
										<thead>
											<tr>
												<th>STAGE</th>
												<th>DATE</th>
												<th>LOCATION</th>
												<th>DISTANCE</th>
												<th>REPORT</th>
											</tr>
										</thead>
										<tbody>';
											while( $stage_posts->have_posts() ) {
												$stage_posts->the_post();

												$stage_number		= get_post_meta( get_the_ID(), '_vn_stages_stage_number', 1 ) ;
												$stage_location		= get_post_meta( get_the_ID(), '_vn_stages_stage_location', 1 ) ;
												$stage_icon			= get_post_meta( get_the_ID(), '_vn_stages_stage_icon', 1 ) ;
												$stage_date			= get_post_meta( get_the_ID(), '_vn_stages_date', 1 ) ;
												$stage_distance		= get_post_meta( get_the_ID(), '_vn_stages_stage_distance', 1 ) ;
												$stage_gallery		= get_post_meta( get_the_id(), '_vn_stages_stage_photos', 1 );
												$stage_date_time	= date( 'm/d/Y', strtotime( $stage_date ) );

												// get the right icon
												switch( $stage_icon ) {
													case 'hill':
														$stageclass = 'hill';
													break;
													case 'time_trial':
														$stageclass = 'time-trial';
													break;
													case 'flat':
														$stageclass = 'flat';
													break;
													case 'peaks':
														$stageclass = 'peaks">';
													break;
													default:
														$stageclass = 'hill';
													break;
												}

												$text_link = '';

												if( !empty( $stage_gallery ) ) {
													$text_link = 'REPORT / PHOTOS';
												} else {
													$text_link = 'DETAILS';
												}

												echo '<tr>
													<td>
														<span class="race-table__table__stage">'. $stage_number .'</span>
														<span class="icon icon_name_'. $stage_icon .'"></span>
													</td>
													<td>'. $stage_date_time .'</td>
													<td>'. $stage_location .'</td>
													<td>'. $stage_distance .'</td>
													<td><a href="'. get_the_permalink( get_the_ID() ) .'" target="_self">'. $text_link .'</a></td>
												</tr>';
											}
										echo '</tbody>
									</table>';
								} else {
									echo '<div class="race-table__pane__wrap">
										<p>'. $results_after_race .'</p>
									</div>';
								}

								wp_reset_postdata();
							?>

						</div><!-- end stages -->


						<?php
							/**
							 * current gc
							 */
						?>
						<div id="gc-top-ten" class="tab-pane race-table__pane" role="tabpanel">
							<?php
								// check for results in each stage. if results change the latest stage to that one
								$latest_stage = 0;
								if( !empty( $results_info ) ) {
									foreach ( $results_info as $key => $value ) {

										if( $value->stage_number > $latest_stage ) {
											$latest_stage = $value->stage_number;
										}
									}

									// changed from top 10 to overall results ordered by overall_position in results import
									// Updating query to move DNF to the end
									$gc_top_results_info = 'SELECT s.*, r.* FROM wp_stage_results r, wp_riders s WHERE s.pk_rider_id = r.fk_rider_id  AND r.fk_race_id = "' . $race_id_num . '" AND r.stage_number ='.$latest_stage.' ORDER BY CASE WHEN r.overall_position >= 1 THEN 0 ELSE 1 END, r.overall_position';
									$gc_results_info = $wpdb->get_results( $gc_top_results_info );

								}

								if( !empty( $gc_results_info )  ) {
									echo '<table class="race-table__table">
										<thead>
											<tr>
												<th>RANK</th>
												<th>RIDER( TEAM )</th>
												<th>OVERALL TIME</th>
											</tr>
										</thead>
										<tbody>';

											foreach ( $gc_results_info as $key => $value ) {
												echo '<tr>
													<td>' . $value->overall_position . '</td>
													<td>' . $value->rider_name . ' ( '.$value->rider_team.' )</td>
													<td>' . $value->overall_time . '</td>
												</tr>';
											}
										echo'</tbody>
									</table>';
								} else {
									echo '<div class="race-table__pane__wrap">
										<p>'. $results_after_race .'</p>
									</div>';
								}
							?>
						</div> <!-- end gc top 10 -->


						<?php
							/**
							 * start list
							 */
						?>
						<div id="start-list" class="tab-pane race-table__pane" role="tabpanel">
							<div class="race-table__pane__wrap">
								<?php
									if( !empty( $race_start_list ) ) {
										echo apply_filters( 'the_content', $race_start_list );
									} else {
										echo $results_after_race;
									}

								?>
							</div>
						</div><!-- end start list -->


						<?php
							/**
							 * tech feed
							 */
						?>
						<div id="tech-feed" class="tab-pane race-table__pane" role="tabpanel">
							<?php
								$tech_args = array(
									'posts_per_page'	=> 10,
									'category_name'		=> 'bikes-and-tech',
									'tax_query'			=> array(
										'operator'		=> 'AND',
										array(
											'taxonomy'	=> 'stagecat',
											'field'		=> 'slug',
											'terms'		=> $stagecat_category
										),
										array(
											'taxonomy'	=> 'stageyear',
											'field'		=> 'slug',
											'terms'		=> $race_year
										)
									)
								 );

								$tech_posts = new WP_Query( $tech_args );

								if( $tech_posts->have_posts() ) :
									echo '<div class="race-table__pane__wrap">
										<ul class="news-feed__feed">';

											while ( $tech_posts->have_posts() ) :
												$tech_posts->the_post();

												echo '<li><a href="' . get_the_permalink() .'">' . get_the_title() . '</a></li>';

											endwhile;
										echo '</ul>
									</div><!--end racetable_pane_wrap-->';
								else:
									echo '<div class="race-table__pane__wrap">
										<p>Check back for full coverage once the race is underway.</p>
									</div>';
								endif;

								wp_reset_postdata();
								$cat_id =  $original_cat_id; //set the category id back to the original now that the race parts done
							?>
						</div>
					</div>
				</div> <!-- end race-table__wrap -->
			</section><!-- end racetable section -->

		<?php endwhile;
	endif;
	?>
</section>
