<?php
/**
 * Template part for displaying Legacy Lists.
 *
 * @package velonews
 */
?>


<article id="article" <?php post_class( array( 'col-xs-12', 'col-sm-7', 'col-md-8', 'article' ) ); ?>>
	<header class="article__header">
		<div class="article__header__banner">
			<?php echo '<figure><img src="'. get_post_meta( get_the_ID(), '_franchise_image', 1 ) .'"></figure>'; ?>
		</div>
		<?php the_title( '<h1 class="article__header__title">', '</h1>' );
		if ( 'post' === get_post_type() ) {
			velonews_posted_on();
		} ?>
	</header>


	<section class="article__body row">
		<?php
			/**
			 * social sharing
			 */
			get_social_sharing( 'side' );

			echo '<div id="article-right" class="col-xs-12 col-md-11">';

				get_social_sharing( 'top' );

				$slides = get_post_meta( get_the_ID(), '_slide_items', 1 );

				if( $slides != '' ) {

					$main_slider = '<div id="list-carousel" class="owl-carousel list-carousel">';
					$thumbnail_slider = '<div id="thumbs-carousel" class="owl-carousel thumbs-carousel">';

					$slidecount = count( $slides );
					$slide_halfway = ceil( $slidecount / 2 );
					$i = 1;

					foreach( $slides as $attachment_id => $attachment_url ) {

						/**
						 * thumbnail slider
						 */
						$thumbnail_slider .= '<div>
							'. wp_get_attachment_image( $attachment_id, 'thumbnail' );

							if( $i != 1 ) {
								$thumbnail_slider .= '<div class="slide_number">'. $slidecount .'</div>';
							}
						$thumbnail_slider .= '</div>';

						if( $i == $slide_halfway ) {
							$thumbnail_slider .= '<div></div>';
						}


						/**
						 * main slider
						 */
						$slide_obj = vn_get_attachment( $attachment_id );

						$main_slider .= '<div>
							<img class="owl-lazy" data-src="'. $slide_obj['src'] .'">
							<h2>'. $slide_obj['title'] .'</h2>
							'. apply_filters( 'the_content', $slide_obj['caption'] ) .'
						</div>';


						if( $i == $slide_halfway ) {

							$midad_obj = vn_get_attachment( get_post_meta( get_the_ID(), '_mid_slider_ad_img_id', 1 ) );
							$main_slider .= '<div>
								<a href="'. get_post_meta( get_the_ID(), '_mid_slider_ad_link', 1 ) .'" target="_blank"><img class="owl-lazy" data-src="'. get_post_meta( get_the_ID(), '_mid_slider_ad_img', 1 ) .'"></a>
								<h2>'. $midad_obj['title'] .'</h2>
								'. apply_filters( 'the_content', $midad_obj['caption'] ) .'
							</div>';

						}

						$i++;
						$slidecount--;

					}

					/**
					 * add last slide ad
					 */
					$thumbnail_slider .= '<div></div>';

					$lastad_obj = vn_get_attachment( get_post_meta( get_the_ID(), '_end_of_slider_ad_img_id', 1 ) );
					$main_slider .= '<div>
						<a href="'. get_post_meta( get_the_ID(), '_end_of_slider_ad_url', 1 ) .'" target="_blank"><img class="owl-lazy" data-src="'. get_post_meta( get_the_ID(), '_end_of_slider_ad_img', 1 ) .'"></a>
						<h2>'. $lastad_obj['title'] .'</h2>
						'. apply_filters( 'the_content', $lastad_obj['caption'] ) .'
					</div>';

					// close off each div
					$thumbnail_slider .= '</div>';
					$main_slider .= '</div>';


					/**
					 * display thumbnail slider
					 */
					echo $thumbnail_slider;


					/**
					 * display main slider
					 */
					echo $main_slider;


					echo '<style>
						.owl-item-'.$slide_halfway.' {
							display:none;
						}
					</style>';

				}

			echo '</div>';

		?>
	</section>


	<?php
		/**
		 * get author bio
		 */
		echo get_author_info();
	?>

</article><!-- #article -->

