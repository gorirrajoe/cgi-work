<?php
	$cats = get_the_category();

	foreach( $cats as $key => $value ) {
		if( strpos( $value->slug, 'guide' ) != false ) {
			//get the year for the guide
			preg_match_all( '!\d+!', $value->slug, $guide_year );
			$guide_year = $guide_year[0][0];

			//start with 2017 gift guide and 2018 buyers guide
			if( ( $guide_year > 2016 && strpos( $value->slug, 'gift' ) != false ) || ( $guide_year > 2017 && strpos( $value->slug, 'buy' ) != false ) ) {

				$in_guide					= true;
				$cat_array[$value->slug]	= $value->name;
				$guide_slug					= current( array_keys( $cat_array ) );
				$guide_name					= $cat_array[$guide_slug];
				$guide_link					= get_category_link( get_category_by_slug( $guide_slug ) );

			}
		}
	}

	$is_amp_endpoint	= function_exists( 'is_amp_endpoint' ) ? is_amp_endpoint() : false;
	$post_id			= get_the_id();
	$cat_IDs			= wp_get_post_categories( $post_id, array( 'fields' => 'ids' ) );

	$related_args = array(
		'posts_per_page'			=> 4,
		'post__not_in'				=> array( $post_id ),
		'no_found_rows'				=> true,
		'update_post_meta_cache'	=> false,
		'ignore_sticky_posts'		=> true,
		'has_password'				=> false,
		'category__in'				=> $cat_IDs,
		'post_type'					=> 'any',
	);

	$related_query = new WP_Query( $related_args );

	if ( $related_query->have_posts() ): ?>

		<section class="content__section">
			<div class="container">
				<header class="col-xs-12 content__section__header">
					<h2>More Bikes &amp; Tech News</h2>
				</header>

				<?php while( $related_query->have_posts() ) : $related_query->the_post();
					$id = get_the_id();

					//apparel items
					$ap_rating		= get_post_meta( $id, '_vn_rating', 1 );
					$ap_msrp		= get_post_meta( $id, '_vn_apparel_msrp', 1 ) ;

					//bike review items
					$bike_rating	= get_post_meta( $id, '_vn_bike_review_bike_overall', 1);
					$bike_msrp		= get_post_meta( $id, '_vn_bike_review_bike_msrp', 1 ) ;
					$overall_score	= 100;

					if( get_post_type( $id ) == "apparel-reviews"){
						$rating			= $ap_rating;
						$msrp			= $ap_msrp;
						$overall_score	= 100;
					} else {
						$rating	= $bike_rating;
						$msrp	= $bike_msrp;
					}

					$post_ID	= get_the_ID();
					$the_title	= get_the_title();
					$cat_array	= get_cat_array( $post_ID );

					// standard classes we are adding to the post
					$post_classes	= array( 'article', 'guide_related', 'col-xs-12', 'col-sm-3' );

					// hide last 2 on larger sizes screens:
					if ( $related_query->current_post > 1 ) { $post_classes[] = 'hidden-xs'; }

					// create ad fallback class if first post
					$ad_fallback_class	= $related_query->current_post == 0 ? ' ad_fallback' : '';

					if ( is_array( $cat_array['slug'] ) ) {
						foreach ( $cat_array['slug'] as $slug ) {
							$post_classes[] = 'article_category_' . $slug;
						}
					} else {
						$post_classes[] = 'article_category_' . $cat_array['slug'];
					}

					// build image tag
					if ( $is_amp_endpoint ) {
						$thumb		= wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );
						$thumbnail	= '<amp-img layout="responsive" src="'. $thumb[0] .'" width="'. $thumb[1] .'" height="'. $thumb[2] .'" alt="'. $the_title .'"></amp-img> ';
					} else {
						$thumbnail	= get_the_post_thumbnail_url( $post_ID, 'featured-thumb-lg' );
						$thumbnail	= '<img data-original="' .$thumbnail .'" alt="'. $the_title .'" class="article__thumbnail lazyload">';
					} ?>

					<article <?php post_class( $post_classes ); ?>>

						<?php if ( $related_query->current_post == 0 && !$is_amp_endpoint ) {
							echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('content-sponsored') : '';
						} ?>

						<a href="<?php the_permalink(); ?>" target="_self" class="article__permalink<?php echo $ad_fallback_class; ?>" title="<?php echo $the_title; ?>">
							<div class="article__thumbnail__wrap">
								<?php echo $thumbnail; ?>
							</div>
							<?php
								echo '<h2 class="bg_single_link">' .  get_the_title() . '</h2>';

								if( $rating != '' ) {
									echo '<p class="bg_score" >Overall Score <strong> '. $rating .'/' . $overall_score . '</strong></p>';
								}
								if( $msrp !== '' ) {
									echo '<p class="bg_price">$'.  $msrp . '</p>' ;
								}
							?>
						</a>
					</article>

				<?php endwhile; ?>
			</div>
		</section>

	<?php endif;

	wp_reset_postdata();
?>

<section class="content__section">
	<div class="container">
		<div class="col-xs-12">
			<div class="row template__bg-final">
				<section class="bg__final_bottom_menu">
					<h2>More Categories</h2>
					<?php
						wp_nav_menu( array(
							'theme_location'	=> $guide_slug,
							'menu_class'		=> 'bg__final_bottom_menu'
						) );

					?>

				</section>
			</div>
		</div>
	</div>
</section>
