<?php
	/**
	 * Template part for displaying standard single posts on AMP.
	 */
	$post_ID	= get_the_ID();
?>
<article id="post-<?php echo $post_ID; ?>" <?php post_class( array( 'col-xs-12', 'article' ) ); ?>>

	<header class="article__header">

		<?php the_title( '<h1 class="article__header__title">', '</h1>' );

		if ( 'post' === get_post_type() ) {
			velonews_posted_on();
		} ?>
	</header>

	<?php
		$sponsorship = get_presented_by();
		echo $sponsorship['content'];
	?>

	<section class="article__body">
		<?php
			echo VeloNews_AMP::get_social_sharing();

			insert_featured_image( $post_ID );

			// the_content();
			echo $this->get( 'post_amp_content' );

			echo VeloNews_AMP::get_social_sharing();
		?>
	</section>

	<?php
		/**
		 * get author bio
		 */
		echo get_author_info();
	?>

</article><!-- #post-## -->
