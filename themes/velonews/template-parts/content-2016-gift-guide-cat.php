<?php
	/**
	 * The template for displaying 2016 gift guide landing page
	 *
	 * @package velonews
	 */

	$this_category	= get_category( get_query_var( 'cat' ), false );

	/**
	 * check if current page is a category
	 * since we have the gift guide page template now
	 */
	if( is_wp_error( $this_category ) ) {
		$cat_flag = 0;
	} else {
		$cat_flag = 1;
	}

	$gg_options = GiftGuide_Admin::get_guide_option( '2016', 'all' );

?>


<div class="vbg_container">

	<?php if( array_key_exists( '_gg_2016_banner_image', $gg_options ) ) {
		$banner_url = ( $cat_flag == 1 ) ? site_url( '/category/' . $this_category->slug ) : site_url( 'gift-guide' );

		echo '<div class="vbg_banner">
			<a href="'. $banner_url .'"><img src="'. $gg_options['_gg_2016_banner_image'] .'" /></a>
		</div>';
	} ?>


	<div class="vbg_twothirds">

		<div class="vbg_full">

			<?php // 1
				if( array_key_exists( '_gg_2016_link_1', $gg_options ) ) {

					$parsed_url_1		= parse_url( $gg_options['_gg_2016_link_1'] );
					$google_tracking_1	= '';

					if( strpos( home_url(), $parsed_url_1['host'] ) === FALSE  ) {
						$google_tracking_1 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Gift Guide\', \'velonews\', \''. $gg_options['_gg_2016_link_1'] .'\') )"';
					}

					echo '<a href="'. $gg_options['_gg_2016_link_1'] .'" '. $google_tracking_1 .'>
						<img src="'. $gg_options['_gg_2016_image_1'] .'">
					</a>';

				}
			?>

		</div>


		<div class="vbg_row">

			<div class="vbg_onethird">

				<div class="vbg_onesixth vbg_module vbg_nomargin">

					<?php // 2
						if( array_key_exists( '_gg_2016_link_2', $gg_options ) ) {

							$parsed_url_2		= parse_url( $gg_options['_gg_2016_link_2'] );
							$google_tracking_2	= '';

							if( strpos( home_url(), $parsed_url_2['host'] ) === FALSE  ) {
								$google_tracking_2 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Gift Guide\', \'velonews\', \''. $gg_options['_gg_2016_link_2'] .'\') )"';
							}
							echo '<div class="vbg_full"><a href="'. $gg_options['_gg_2016_link_2'] .'" '. $google_tracking_2 .'>
								<img src="'. $gg_options['_gg_2016_image_2'] .'">
							</a></div>';

						}

						// 3
						if( array_key_exists( '_gg_2016_link_3', $gg_options ) ) {

							$parsed_url_3		= parse_url( $gg_options['_gg_2016_link_3'] );
							$google_tracking_3	= '';

							if( strpos( home_url(), $parsed_url_3['host'] ) === FALSE  ) {
								$google_tracking_3 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Gift Guide\', \'velonews\', \''. $gg_options['_gg_2016_link_3'] .'\') )"';
							}
							echo '<div class="vbg_full"><a href="'. $gg_options['_gg_2016_link_3'] .'" '. $google_tracking_2 .'>
								<img src="'. $gg_options['_gg_2016_image_3'] .'">
							</a></div>';

						}
					?>

				</div>

				<div class="vbg_onesixth vbg_module">

					<?php // 4
						if( array_key_exists( '_gg_2016_link_4', $gg_options ) ) {

							$parsed_url_4		= parse_url( $gg_options['_gg_2016_link_4'] );
							$google_tracking_4	= '';

							if( strpos( home_url(), $parsed_url_4['host'] ) === FALSE  ) {
								$google_tracking_4 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Gift Guide\', \'velonews\', \''. $gg_options['_gg_2016_link_4'] .'\') )"';
							}
							echo '<a href="'. $gg_options['_gg_2016_link_4'] .'" '. $google_tracking_4 .'>
								<img src="'. $gg_options['_gg_2016_image_4'] .'">
							</a>';

						}
					?>

				</div>

			</div>

			<div class="vbg_onethird">

				<div class="vbg_full">

					<?php // 5
						if( array_key_exists( '_gg_2016_link_5', $gg_options ) ) {

							$parsed_url_5		= parse_url( $gg_options['_gg_2016_link_5'] );
							$google_tracking_5	= '';

							if( strpos( home_url(), $parsed_url_5['host'] ) === FALSE  ) {
								$google_tracking_5 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Gift Guide\', \'velonews\', \''. $gg_options['_gg_2016_link_5'] .'\') )"';
							}
							echo '<a href="'. $gg_options['_gg_2016_link_5'] .'" '. $google_tracking_5 .'>
								<img src="'. $gg_options['_gg_2016_image_5'] .'">
							</a>';

						}
					?>

				</div>

				<div class="vbg_full">

					<?php // 6
						if( array_key_exists( '_gg_2016_link_6', $gg_options ) ) {

							$parsed_url_6		= parse_url( $gg_options['_gg_2016_link_6'] );
							$google_tracking_6	= '';

							if( strpos( home_url(), $parsed_url_6['host'] ) === FALSE  ) {
								$google_tracking_6 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Gift Guide\', \'velonews\', \''. $gg_options['_gg_2016_link_6'].'\') )"';
							}
							echo '<a href="'. $gg_options['_gg_2016_link_6'] .'" '. $google_tracking_6 .'>
								<img src="'. $gg_options['_gg_2016_image_6'] .'">
							</a>';
						}
					?>

				</div>

			</div>

		</div>
	</div>


	<div class="vbg_lastthird">
		<div class="vbg_ad">

			<h4>Advertisement</h4>
			<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'side-middle' ) : ''; ?>

		</div>

		<div class="vbg_row">

			<div class="vbg_onesixth vbg_module">

				<?php // 7
					if( array_key_exists( '_gg_2016_link_7', $gg_options ) ) {

						$parsed_url_7		= parse_url( $gg_options['_gg_2016_link_7'] );
						$google_tracking_7	= '';

						if( strpos( home_url(), $parsed_url_7['host'] ) === FALSE  ) {
							$google_tracking_7 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Gift Guide\', \'velonews\', \''. $gg_options['_gg_2016_link_7'].'\') )"';
						}
						echo '<a href="'. $gg_options['_gg_2016_link_7'] .'" '. $google_tracking_7 .'>
							<img src="'. $gg_options['_gg_2016_image_7'] .'">
						</a>';

					}
				?>

			</div>


			<div class="vbg_onesixth vbg_module">

				<?php // 8
					if( array_key_exists( '_gg_2016_link_8', $gg_options ) ) {

						$parsed_url_8		= parse_url( $gg_options['_gg_2016_link_8'] );
						$google_tracking_8	= '';

						if( strpos( home_url(), $parsed_url_8['host'] ) === FALSE  ) {
							$google_tracking_8 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Gift Guide\', \'velonews\', \''. $gg_options['_gg_2016_link_8'].'\') )"';
						}
						echo '<a href="'. $gg_options['_gg_2016_link_8'] .'" '. $google_tracking_8 .'>
							<img src="'. $gg_options['_gg_2016_image_8'] .'">
						</a>';

					}
				?>

			</div>

		</div>



	</div>
</div>

