<?php
/**
 * Template part for displaying two posts (responsive)
 *
 * @package velonews
 */


if( is_category() && ( $four_across = get_term_meta( $cat_id, '_vn_four-across-top', 1 ) ) != '' ) {

	if( false === ( $two_responsive_query = get_transient( $cat_nicename .'_tworesponsive_query' ) ) ) {
		$args = array(
			'post_status'				=> 'publish',
			'post_type'					=> 'any',
			'posts_per_page'			=> '2',
			'category__and'				=> array( $cat_id, $four_across ),
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false
		);
		$two_responsive_query = new WP_Query( $args );
		set_transient( $cat_nicename .'_tworesponsive_query', $two_responsive_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}
	$title		= get_term_meta( $cat_id, '_vn_four-across-top-title', 1 );
	$cat_url	= get_category_link( $four_across );

} else {
	// use global velo options
	$global_four_across = cgi_bikes_get_option( 'posts_home_4_category' );

	if( false === ( $two_responsive_query = get_transient( 'global_tworesponsive_query' ) ) ) {
		$args = array(
			'post_status'				=> 'publish',
			'post_type'					=> 'any',
			'posts_per_page'			=> '2',
			'cat'						=> $global_four_across,
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false
		);
		$two_responsive_query = new WP_Query( $args );
		set_transient( 'global_tworesponsive_query', $two_responsive_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}
	$title		= cgi_bikes_get_option( 'home_4_title' );
	$cat_url	= get_category_link( $global_four_across );

}


if ( $two_responsive_query->have_posts() ) {
	echo '<section class="visible-sm photo-galleries">
		<div class="col-xs-12 content__section__header">
			<h2>'. strtoupper( $title ) .'</h2>
			<a href="'. $cat_url .'" target="_self" class="content__section__more">VIEW ALL <span class="fa fa-caret-right"></span></a>
		</div>';

		while( $two_responsive_query->have_posts() ) {
			$two_responsive_query->the_post();

			$cat_array	= get_cat_array( get_the_ID() );
			$thumbnail	= get_the_post_thumbnail_url( get_the_ID(), 'featured-thumb-lg' );

			$article_classes	= '';
			if( is_array( $cat_array['slug'] ) ) {
				foreach( $cat_array['slug'] as $slug ) {
					$article_classes .= ' article_category_' . $slug;
				}
			} else {
				$article_classes .= ' article_category_' . $cat_array['slug'];
			}

			echo '<article class="col-xs-12 col-sm-6 article article_location_homepage article_type_latest' . $article_classes .'">
				<a href="'. get_permalink() .'" target="_self" class="article__permalink" title="'. get_the_title() .'">
					<div class="article__thumbnail__wrap">
						<img data-original="'. $thumbnail .'" alt="" class="article__thumbnail lazyload">
						<div class="article__thumbnail__overlay"></div>
					</div>
					<div class="article__category">'. get_article_category_tag( get_the_ID(), $cat_array ) .'</div>
					<p class="article__title">'. get_the_title() .'</p>
				</a>
			</article>';
		}
		wp_reset_postdata();
	echo '</section>';
}

?>
