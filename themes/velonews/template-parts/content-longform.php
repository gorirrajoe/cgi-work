<?php
/**
 * Template part for displaying standard single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package velonews
 */


$sponsorship = get_presented_by();
?>


<article id="article" <?php post_class( array( 'article_type_long-form', 'article' ) ); ?>>
	<?php
		if( ( $alt_header_img = get_post_meta( get_the_ID(), '_vn_alt_header_img', 1 ) ) != '' ) {
			$header_img = $alt_header_img;
		} else {
			$image_array = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'longform-image-gallery' );
			$header_img = $image_array[0];
		}
	?>
	<header class="col-xs-12 article__header" style="background-image:url( '<?php echo $header_img; ?>' );">
		<div class="article__header__filter"></div>
		<div class="article__header__wrap">
			<?php
				the_title( '<h1 class="article__header__title">', '</h1>' );
				velonews_posted_on();
			?>
		</div>
		<?php echo $sponsorship['content']; ?>
	</header>


	<div class="row article__content">
		<?php
			/**
			 * social sharing
			 */
			get_social_sharing( 'side', 'longform' );
		?>

		<div id="article-right" class="col-xs-12 col-sm-11">

			<?php get_social_sharing( 'top', 'longform' ); ?>

			<section class="article__body">
				<?php
					the_content();
				?>
			</section>

		</div>
	</div>


	<?php
		/**
		 * get author bio
		 */
		echo get_author_info();
	?>

</article><!-- #article -->
