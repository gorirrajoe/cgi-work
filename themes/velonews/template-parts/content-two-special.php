<?php
/**
 * Template part for displaying 2 large thumbnailed posts.
 *
 * set $featured_IDs in page calling this template-part file
 *
 * @package velonews
 */



if( is_page_template() && ( $twospecial_across = get_post_meta( get_the_ID(), '_vn_2special_category', 1 ) ) ) {
	$page_template_pieces = explode( '.', basename( get_page_template() ) );

	if( false === ( $twospecial_query = get_transient( 'template_'. $page_template_pieces[0].'_twospecial_query' ) ) ) {
		$args = array( 
			'cat'						=> $twospecial_across,
			'post_type'					=> 'any',
			'meta_key'					=> '_featured_post',
			'meta_value'				=> 'featured',
			'ignore_sticky_posts'		=> true,
			'post_status'				=> 'publish',
			'posts_per_page'			=> '2',
			'no_found_rows'				=> false,
			'update_post_meta_cache'	=> false
		);
		$twospecial_query = new WP_Query( $args );
		set_transient( 'template_'. $page_template_pieces[0].'_twospecial_query', $twospecial_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}

}

if( $twospecial_query->have_posts() ) {
	echo '<section class="featured-content">';
		while( $twospecial_query->have_posts() ) {
			$twospecial_query->the_post();

			$cat_array	= get_cat_array( get_the_ID() );
			$thumbnail	= get_the_post_thumbnail_url( get_the_ID(), 'featured-thumb-lg' );

			$title		= vn_get_post_title( get_the_ID() );
			//$post_id_array[]	= get_the_ID();

			$article_classes	= '';
			if( is_array( $cat_array['slug'] ) ) {
				foreach( $cat_array['slug'] as $slug ) {
					$article_classes .= ' article_category_' . $slug;
				}
			} else {
				$article_classes .= ' article_category_' . $cat_array['slug'];
			}

			echo '<article class="col-xs-12 col-sm-6 article article_type_latest' . $article_classes . '">
				<a href="'. get_the_permalink() .'" target="_self" class="article__permalink" title="'. $title .'">
					<img data-original="'. $thumbnail .'" alt="" class="article__thumbnail lazyload">
					<div class="article__img-filter"></div>
					<div class="article__category">'. get_article_category_tag( get_the_ID(), $cat_array ) .'</div>
					<p class="article__title">'. $title .'</p>
				</a>
			</article>';

		}
		wp_reset_postdata();
	echo '</section>';
}


?>


