<?php
	/**
	 * Template part for displaying buyer's guide items.
	 *
	 * @link https://codex.wordpress.org/Template_Hierarchy
	 *
	 * @package velonews
	 */

	global $posts, $page;
	$bg_options = BuyersGuide_Admin::get_guide_option( '2015', 'all' );

?>


			<article id="article" <?php post_class( array( 'article', 'article_type_product' ) ); ?>>

				<div class="article__banner">
					<a href="<?php echo site_url( '/category/2015-buyers-guide' ); ?>"><img src="<?php echo $bg_options['buyers_guide_2015_banner_image']; ?>" /></a>
				</div>

				<?php
					/**
					 * breadcrumb
					 */
					get_breadcrumb();
				?>

				<header class="article__header">
					<?php
						the_title( '<h1 class="article__header__title">', '</h1>' );
						velonews_posted_on();
					?>
				</header>


				<section class="article__body product">
					<div class="row">
						<?php
							/**
							 * social sharing
							 */
							get_social_sharing( 'side' );

							echo '<div id="article-right" class="col-xs-12 col-md-11">';

								get_social_sharing( 'top' );

								echo '<nav class="product__nav">';
									/**
									 * page navigation
									 */
									$prev_next_links = wp_link_pages( array(
										'next_or_number'	=> 'next',
										'previouspagelink'	=> '<span class="fa fa-chevron-left"></span><span class="product__nav__link-text">Previous</span>',
										'nextpagelink'		=> '<span class="product__nav__link-text">Next</span><span class="fa fa-chevron-right"></span>',
										'echo'				=> 0
									) );

									echo get_guide_nav( $prev_next_links ) . '
								</nav>';

								$attachments = get_children( array(
									'post_parent'		=> get_the_ID(),
									'post_status'		=> 'inherit',
									'post_type'			=> 'attachment',
									'post_mime_type'	=> 'image',
									'orderby'			=> 'menu_order',
									'order'				=> 'ASC'
								) );

								$count				= 1;
								$attachment_count	= count( $attachments );

								foreach( $attachments as $attachment ) {

									$attachment_id = $attachment->ID;

									if( $page == $count ) {
										$attachment_id = $attachment->ID;
										break;
									}

									$count++;
								}

								if( is_mobile() ) {
									$image_array = wp_get_attachment_image_src( $attachment_id, 'medium' );
								} else {
									$image_array = wp_get_attachment_image_src( $attachment_id, 'featured-gallery' );
								}

								$thumbnail_image	= get_post( $attachment_id );
								$thumbnail_title	= $thumbnail_image->post_title != '' ? 'title="'. $thumbnail_image->post_title .'"' : '';
								$thumbnail_caption	= $thumbnail_image->post_excerpt != '' ? '<figcaption class="wp-caption-text">'. $thumbnail_image->post_excerpt .'</figcaption>' : '';

								echo '<div class="col-xs-12 col-sm-6">';

									printf(
										'<figure><img %s src="'. $image_array[0] .'" />%s</figure>',
										$thumbnail_title,
										$thumbnail_caption
									);

								echo '</div>

								<div class="col-xs-12 col-sm-6">';
									$pagetitlestring	= '/<!--pagetitle:(.*?)-->/';
									$content			= $posts[0]->post_content;
									preg_match_all( $pagetitlestring, $content, $titlesarray, PREG_PATTERN_ORDER );

									$pagetitles = $titlesarray[1];

									echo '<h2 class="product__title">'. $pagetitles[$page - 1] .'</h2>' .
									apply_filters( 'the_content', get_the_content() );
								echo '</div>
							</div>';
						?>
					</div>


					<?php
						echo '<div class="row">
							<div class="col-xs-12">
								<div id="product-carousel" class="owl-carousel product-carousel">';

									$page_count = 1;

									foreach( $attachments as $attachment ) {

										if( $page_count != $attachment_count ) {  // show all attachments but last one ( featured img )
											$attachment_id		= $attachment->ID;
											$image_array_thumb	= wp_get_attachment_image_src( $attachment_id, 'square-buyers-guide-product' );
											echo '
												<div class="owl-carousel__item__wrap">
													<a href="'.get_permalink().'/'.$page_count.'" rel="nofollow">
														<img src="'.$image_array_thumb[0].'"/>
														'. $pagetitles[$page_count - 1] .'
													</a>
												</div>';
										}

										$page_count++;
									}

								echo '</div>
							</div>
						</div>';
					?>
				</section>
			</article><!-- #article -->
		</div>
	</div>
</section>


<section class="related-content">
	<div class="container">

		<header class="row related-content__header">
			<div class="col-xs-12">
				<h2 class="related-content__title">MORE GEAR</h2>
			</div>
		</header>

		<div class="row related-content__body">
			<div class="col-xs-12">
				<div id="related-carousel" class="owl-carousel product-carousel product-carousel_type_related">

					<?php
						$gear_count	= 1;
						$gear_array	= array();

						for( $i = 0; $i < 87; $i++ ) { // since it chooses a random number, padding it 3x

							if( $gear_count > 29 ) {  // 28 types in the buyers guide, stop after it has all of them displaying
								break;
							} else {

								$random = rand( 1, 28 );

								if( !in_array( $random, $gear_array ) ) {

									$theme_option_image = 'more_gear_2015_image_' . $random;

									if( array_key_exists( $theme_option_image, $bg_options ) && $bg_options[$theme_option_image] != '' ) {

										$theme_option_link = 'buyers_guide_2015_link_' . $random;

										echo '<div class="owl-carousel__item__wrap">
											<a href="'. $bg_options[$theme_option_link] .'"><img src="'. $bg_options[$theme_option_image] .
											'"/></a>
										</div>';

									}

									$gear_array[] = $random;
									$gear_count++;
								}
							}
						}

					?>
				</div>
			</div>
		</div>
	</div>
</section>


