<?php
/**
 * Template part for displaying one post
 *
 * sitewide, top right of page
 *
 * @package velonews
 */
global $post_id_array;

if( is_category() && ( $one_featured = get_term_meta( $cat_id, '_vn_top-right-featured', 1 ) ) != '' ) {
	if( false === ( $one_featured_query = get_transient( $cat_nicename . '_one_query' ) ) ) {
		$args = array(
			'p'							=> $one_featured,
			'post_status'				=> 'publish',
			'post_type'					=> 'any',
			'posts_per_page'			=> 1,
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> false,
			'update_post_meta_cache'	=> false
		);
		$one_featured_query = new WP_Query( $args );
		set_transient( $cat_nicename . '_one_query', $one_featured_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}
} elseif ( 'races_event' == get_post_type() ) {
	$one_featured =  ( get_term_meta( $cat_id, '_vn_top-right-featured', 1 ) !== '' ) ? get_term_meta( $cat_id, '_vn_top-right-featured', 1 ) : cgi_bikes_get_option( 'ad_post_id' ) ;

	if( false === ( $one_featured_query = get_transient( $cat_short_name . '_one_query' ) ) ) {
		$args = array(
			'p'							=> $one_featured,
			'posts_per_page'			=> 1,
			'post_type'					=> 'any',
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> false,
			'update_post_meta_cache'	=> false
		);
		$one_featured_query = new WP_Query( $args );
		set_transient( $cat_short_name . '_one_query', $one_featured_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query content-one races_event -->';
	} else {
		echo '<!-- cache -->';
	}
} else {
	$global_one_featured = cgi_bikes_get_option( 'ad_post_id' );

	if( false === ( $one_featured_query = get_transient( 'global_one_query' ) ) ) {
		$args = array(
			'p'							=> $global_one_featured,
			'post_type'					=> 'any',
			'post_status'				=> 'publish',
			'posts_per_page'			=> 1,
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> false,
			'update_post_meta_cache'	=> false

		);
		$one_featured_query = new WP_Query( $args );
		set_transient( 'global_one_query', $one_featured_query, 15 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cache -->';
	}
}


if( $one_featured_query->have_posts() ) {
	while( $one_featured_query->have_posts() ) {
		$one_featured_query->the_post();

		$post_id			= get_the_ID();
		$title				= vn_get_post_title( $post_id );
		$thumbnail			= get_the_post_thumbnail( $post_id, 'featured-thumb-lg', array( 'class' => 'article__thumbnail' ) );
		$title				= vn_get_post_title( $post_id );
		$cat_array			= get_cat_array( $post_id );
		$post_id_array[]	= $post_id;

		$article_classes	= '';

		if( is_array( $cat_array['slug'] ) ) {
			foreach( $cat_array['slug'] as $slug ) {
				$article_classes .= ' article_category_' . $slug;
			}
		} else {
			$article_classes .= ' article_category_' . $cat_array['slug'];
		}

		echo '<article class="col-xs-12 article article_type_latest article_type_latest--featured' . $article_classes .'">
			<a href="'. get_permalink() .'" target="_self" class="article__permalink" title="'. $title .'">
				<div class="article__thumbnail__wrap">
					'. $thumbnail .'
					<div class="article__thumbnail__overlay"></div>
				</div>
				<div class="article__category">'. get_article_category_tag( get_the_ID(), $cat_array ) .'</div>
				<p class="article__title">'. $title .'</p>
			</a>
		</article>';
	}
	wp_reset_postdata();
}

?>
