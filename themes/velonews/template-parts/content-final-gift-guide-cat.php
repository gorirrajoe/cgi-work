<?php
	/**
	 * The template for displaying gift guide landing page 2017 and later
	 *
	 * @package velonews
	 */

	$post_category		= get_queried_object();
	$guide_name			= $post_category->name;
	$guide_cat			= $post_category->slug;
	$current_cat_link	= get_category_link( $post_category->term_id );
	$parent				= get_category( $post_category->parent );
	$current_cat_id		= $post_category->term_id;

	$type_name			= ( isset( $type[1] ) ? $type[1] : '' );

	if( !is_wp_error( $parent ) &&( strpos( $parent->slug, 'guide' ) !== false ) ) {
		//if parent category is the guide category, set the guide category to parent
		$guide_cat	= $parent->slug;
		$guide_name	= $parent->name;

	}

	$guide_link = get_category_link( get_category_by_slug( $guide_cat ) );

	//extract the current year from the category
	preg_match_all('!\d+!', $guide_cat, $this_year);
	$this_year		= implode('', $this_year[0]);
	$menu_location	= "{$this_year}-buyers-guide";

	$bg_options		= BuyersGuide_Admin::get_guide_option( $this_year, 'all' );

	if( strpos($guide_cat, 'gift') ){
		$bg_options		= GiftGuide_Admin::get_guide_option($this_year, 'all');
		$menu_location	= "{$this_year}-gift-guide";
	}

	$category_contents	= $bg_options['_bg_' . $this_year];
	$bg_author			= isset($bg_options["_bg_{$this_year}_author_ID"]) ? $bg_options["_bg_{$this_year}_author_ID"] : false ;
	$count				= 0;


	//pick a template for category or guide
	if( $guide_cat !== $post_category->slug ){

		echo '<section class="content template__bg-final category">';
	} else {
		echo '<section class="content template__bg-final">';
	}
	//main container
	echo '<div class="container">';
?>
<section class="content archive template__bg-final">
	<div class="container">

		<div class="row">
			<?php
				echo '<div class="col-xs-12 bg__final_title">
					<h1><a href="'. $guide_link  .'">' . $guide_name . '</a></h1>
				</div>';
			?>
		</div>

	</div>

	<div class="container top_menu">
		<?php
			wp_nav_menu( array(
				'theme_location'	=> $menu_location,
				'menu_class'		=> 'bg__final_top_menu'
			) );
		?>

	</div>


	<div class="container content__container">
		<?php
			get_social_sharing( 'top' );

			if( $guide_cat !== $post_category->slug ) { ?>
				<div class="row">
					<div class="col-xs-12 bg__breadcrumb">
						<?php
							/**
							 * breadcrumb
							 */
							get_breadcrumb();
						?>
					</div>
				</div>

				<div class="row">
					<section class="col-sm-7 col-md-8">
						<div class="row bg__grid">
							<?php
								if(have_posts() ) {

									while( have_posts() ){
										the_post();
										$id = get_the_id();

										//apparel items
										$ap_rating	= get_post_meta( $id, '_vn_rating', 1 );
										$ap_msrp	= get_post_meta( $id, '_vn_apparel_msrp', 1 ) ;

										//bike review items
										$bike_rating	= get_post_meta( $id, '_vn_bike_review_bike_overall', 1);
										$bike_msrp		= get_post_meta( $id, '_vn_bike_review_bike_msrp', 1 ) ;
										$overall_score	= 100;

										if( get_post_type( $id ) == "apparel-reviews" ) {
											$rating			=  $ap_rating;
											$msrp			= $ap_msrp;
											$overall_score	= 10;
										} else {
											$rating = $bike_rating;
											$msrp 	= $bike_msrp;
										}

										echo '<div class="col-md-4 col-sm-6">
											<a href="'. get_permalink( $id ) .'">'. get_the_post_thumbnail( $id, array( 320, 250) ) .'</a>
											<h2 class="bg_single_link"><a href="'. get_permalink( $id ) .'">' .  get_the_title() . '</a></h2>';

											if( $rating != '' ) {
												echo '<p class="bg_score" >Overall Score <strong> '. $rating .'/' . $overall_score . '</strong></p>';
											}

											if( $msrp !== ''){
												echo '<p class="bg_price">$'.  $msrp . '</p>' ;
											}

									 	echo '</div>';
									}
								}
							?>
						</div>
					</section>

					<?php get_sidebar( 'right' ); ?>
				</div>

				<?php
					echo get_paginated_links();
				?>

				<div class="row">
					<section class="bg__final_bottom_menu">
						<h2>More Categories</h2>
						<?php
							wp_nav_menu( array(
								'theme_location'	=> $menu_location,
								'menu_class'		=> 'bg__final_bottom_menu'
							) );
						?>
					</section>
				</div>

			<?php } else {
				//bg overview setup
				$intro			= $bg_options["_bg_{$this_year}_intro"];
				$author_desc	= get_the_author_meta('description', $bg_author );
				$author_img		= get_avatar( $bg_author );
				$author_name	= get_the_author_meta('display_name', $bg_author); ?>

				<div class="row">
					<article class="col-xs-12 col-sm-7 col-md-8">

						<?php if($bg_author !== false ) { ?>
							<div class="row">
								<div class="col-xs-12">
									<?php
										echo '<p class="bg__final_intro">'. $intro . '</p>
										<div class="bg__final_author col-xs-12 col-sm-8">
											<div id="img">'
												. $author_img . '
											</div>
											<div id="desc">
												<h3>Reviews by:</h3>
												<h4>' . $author_name . '</h4>
												<p>' .  $author_desc . '</p>
											</div>'.//end desc
										'</div>';
									?>
								</div>
							</div>
						<?php } //end author if block


						echo '<div class="row">';

							$total = count($category_contents);

							foreach ($category_contents as $key => $value) {

								$link				= get_category_link( $value['_bg_' . $this_year . '_cat_id']);
								$img_url			= $value['_bg_' . $this_year . '_image'];
								$text				= $value['_bg_' . $this_year . '_text'];
								$google_tracking	= '';

								if( $count == 4 ) { ?>
											</div>

										</article>

										<section class="hidden-xs col-sm-5 col-md-4 sidebar">
											<section class="advert advert_sm_300x600 advert_location_side">
												<div class="advert__wrap">
													<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'side-middle' ) : ''; ?>
												</div>
											</section>
										</section>
									</div>

									<article>
										<div class="row">
											<div class="col-md-4 article col-sm-6">
												<a href="<?php echo $link;?>" class="article__permalink"<?php //echo $google_tracking != '' ? $google_tracking : '';?> >
													<img src="<?php echo $img_url;?>">
												</a>
												<p class="article__title"><a href="<?php echo $link;?>" class="article__permalink"><?php echo $text; ?></a></p>
											</div>

								<?php } elseif( $count > 4 ) { ?>

									<div class="col-md-4 article col-sm-6">
										<a href="<?php echo $link;?>" class="article__permalink"<?php //echo $google_tracking != '' ? $google_tracking : '';?> >
											<img src="<?php echo $img_url;?>">
										</a>
										<p class="article__title"><a href="<?php echo $link;?>" class="article__permalink"><?php echo $text; ?></a></p>
									</div>

								<?php } else {

									$parsed_url = parse_url( $link );

									if( strpos( home_url(), $parsed_url['host'] ) === FALSE ) {
										//set google tracking for external links
										$google_tracking = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $link .'\') )"';
									} ?>

									<div class="col-xs-12 article col-sm-6">
										<a href="<?php echo $link;?>" class="article__permalink"<?php //echo $google_tracking != '' ? $google_tracking : '';?> >
											<img src="<?php echo $img_url;?>">
										</a>
										<p class="article__title"><a href="<?php echo $link;?>" class="article__permalink"><?php echo $text; ?></a></p>
									</div>

								<?php }

								$count ++;//increment the counter

							} //end loop

						echo '</div>
					</article>
				</div>';

			}
		?>
	</div>


	<section class="advert advert_xs_300x250 advert_sm_728x90 advert_location_bottom ">
		<div class="advert__wrap">
			<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom' ) : ''; ?>
		</div>
	</section>


</section>
</div>

</section>

<?php get_footer();
