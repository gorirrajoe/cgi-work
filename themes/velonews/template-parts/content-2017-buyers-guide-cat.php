<?php
	/**
	 * The template for displaying 2017 gift guide landing page
	 *
	 * @package velonews
	 */

	$this_category		= get_category( get_query_var( 'cat' ), false );
	$bg_options			= BuyersGuide_Admin::get_guide_option( '2017', 'all' );
	$category_contents	= $bg_options['_bg_2017'];
	get_header();

	$count = 0;
?>
<div class="content content__container">
	<div class="vbg_container vbg">
		<div class="row">
		<?php if( array_key_exists( 'buyers_guide_2017_banner_image', $bg_options ) ) {
			echo '<div class="col-xs-12">
				<a href="'. site_url( '/category/' . $this_category->slug ) .'"><img src="'. $bg_options['buyers_guide_2017_banner_image'] .'" /></a>
			</div>';
		} ?>

		</div>

		<div class="row">

				<?php
					$total = count($category_contents);

					foreach ($category_contents as $key => $value) {

						//get the post link
						$link = $value['buyers_guide_2017_link'];
						$img_url = $value['buyers_guide_2017_image'];
						$text = $value['buyers_guide_2017_text'];

						$google_tracking = '';?>
						<div class="col-md-4 col-sm-6">
						<?php
						if(count( $value ) > 0 ) {
							if( $count == 2 ) {?>
								<div class="advert__wrap">
									<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'side-middle' ) : ''; ?>
								</div>
							</div>
							<div class="col-md-4 col-sm-6">
								<a href="<?php echo $link;?>" <?php echo $google_tracking != '' ? $google_tracking : '';?> class="article__permalink">
									<img src="<?php echo $img_url;?>">
								</a>
								<a href="<?php echo $link;?>" class="article__permalink">
									<p class="article__title"><?php echo $text; ?></p>
								</a>
							</div>
							<?php
						} else {
								$parsed_url = parse_url( $link );
								if( strpos( home_url(), $parsed_url['host'] ) === FALSE ) {
									//set google tracking for external links
									$google_tracking = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $link .'\') )"';
								}

								?>
									<a href="<?php echo $link;?>" class="article__permalink"<?php //echo $google_tracking != '' ? $google_tracking : '';?> >
										<img src="<?php echo $img_url;?>">
									</a>
									<a href="<?php echo $link;?>" class="article__permalink"><p class="article__title"><?php echo $text; ?></p>
									</a>
								</div>
								<?php
								}
							}
							$count ++;//increment the counter
						}//end loop?>
					</div><!--  end  -->
			</div> <!-- end row -->
		</div>


		<section class="advert advert_xs_300x250 advert_sm_728x90 advert_location_bottom ">
			<div class="advert__wrap">
				<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom' ) : ''; ?>
			</div>
		</section>
</div>
<?php get_footer();
