<?php
	/**
	 * Template part for displaying buyer's guide items.
	 *
	 * @link https://codex.wordpress.org/Template_Hierarchy
	 *
	 * @package velonews
	 */

	global $posts, $page;
	$bg_options = BuyersGuide_Admin::get_guide_option( '2016', 'all' );

?>

<article id="article" <?php post_class( array( 'article', 'article_type_product' ) ); ?>>
	<div class="article__banner">
		<a href="<?php echo site_url( '/category/2016-buyers-guide' ); ?>"><img src="<?php echo $bg_options['buyers_guide_2016_banner_image']; ?>" /></a>
	</div>

	<?php
		/**
		 * breadcrumb
		 */
		get_breadcrumb();
	?>

	<header class="article__header">
		<?php
			the_title( '<h1 class="article__header__title">', '</h1>' );
			velonews_posted_on();
		?>
	</header>


	<section class="article__body product">
		<div class="row">

			<?php
				/**
				 * social sharing
				 */
				get_social_sharing( 'side' );

				echo '<div id="article-right" class="col-xs-12 col-md-11">';

					get_social_sharing( 'top' );

					$attachments = get_children( array(
						'post_parent'		=> get_the_ID(),
						'post_status'		=> 'inherit',
						'post_type'			=> 'attachment',
						'post_mime_type'	=> 'image',
						'orderby'			=> 'menu_order',
						'order'				=> 'ASC'
					) );

					$count				= 1;
					$attachment_count	= count( $attachments );

					foreach( $attachments as $attachment ) {

						$attachment_id = $attachment->ID;

						if( $page == $count ) {
							$attachment_id = $attachment->ID;
							break;
						}

						$count++;

					}

					if( is_mobile() ) {
						$image_array = wp_get_attachment_image_src( $attachment_id, 'medium' );
					} else {
						$image_array = wp_get_attachment_image_src( $attachment_id, 'featured-gallery' );
					}

					$thumbnail_image	= get_post( $attachment_id );
					$thumbnail_title	= $thumbnail_image->post_title != '' ? 'title="'. $thumbnail_image->post_title .'"' : '';
					$thumbnail_caption	= $thumbnail_image->post_excerpt != '' ? '<figcaption class="wp-caption-text">'. $thumbnail_image->post_excerpt .'</figcaption>' : '';

					echo '<div class="col-xs-12 col-sm-6">';

						printf(
							'<figure><img %s src="'. $image_array[0] .'" />%s</figure>',
							$thumbnail_title,
							$thumbnail_caption
						);

					echo '</div>

					<div class="col-xs-12 col-sm-6">';
						$pagetitlestring	= '/<!--pagetitle:(.*?)-->/';
						$content			= $posts[0]->post_content;
						preg_match_all( $pagetitlestring, $content, $titlesarray, PREG_PATTERN_ORDER );

						$pagetitles = $titlesarray[1];

						echo apply_filters( 'the_content', get_the_content() ) . '
					</div>
				</div>';
			?>
		</div>

	</section>
</article><!-- #article -->

