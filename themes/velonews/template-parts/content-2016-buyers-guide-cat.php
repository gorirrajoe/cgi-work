<?php
	/**
	 * The template for displaying 2016 gift guide landing page
	 *
	 * @package velonews
	 */

	$this_category	= get_category( get_query_var( 'cat' ), false );
	$bg_options		= BuyersGuide_Admin::get_guide_option( '2016', 'all' );

	get_header();
?>

<section id="content" class="content">
	<div class="vbg_container">

		<?php if( array_key_exists( 'buyers_guide_2016_banner_image', $bg_options ) ) {
			echo '<div class="vbg_banner">
				<a href="'. site_url( '/category/' . $this_category->slug ) .'"><img src="'. $bg_options['buyers_guide_2016_banner_image'] .'" /></a>
			</div>';
		} ?>

		<div class="vbg_twothirds">

			<div class="vbg_full">

				<?php // 1
					if( array_key_exists( 'buyers_guide_2016_link_1', $bg_options ) ) {
						$parsed_url_1		= parse_url( $bg_options['buyers_guide_2016_link_1'] );
						$google_tracking_1	= '';

						if( strpos( home_url(), $parsed_url_1['host'] ) === FALSE  ) {
							$google_tracking_1 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_1'].'\') )"';
						}

						echo '<a href="'. $bg_options['buyers_guide_2016_link_1'] .'" '. $google_tracking_1 .'>
							<img src="'. $bg_options['buyers_guide_2016_image_1'] .'">
						</a>';
					}
				?>

			</div>

			<div class="vbg_row">

				<div class="vbg_onethird">

					<div class="vbg_row">
						<div class="vbg_onesixth">

							<?php // 2
								if( array_key_exists( 'buyers_guide_2016_link_2', $bg_options ) ) {

									$parsed_url_2		= parse_url( $bg_options['buyers_guide_2016_link_2'] );
									$google_tracking_2	= '';

									if( strpos( home_url(), $parsed_url_2['host'] ) === FALSE  ) {
										$google_tracking_2 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_2'] .'\') )"';
									}

									echo '<a href="'. $bg_options['buyers_guide_2016_link_2'] .'" '. $google_tracking_2 .'>
										<img src="'. $bg_options['buyers_guide_2016_image_2'] .'">
									</a>';
								}
							?>

						</div>

						<div class="vbg_onesixth">

							<?php // 3
								if( array_key_exists( 'buyers_guide_2016_link_3', $bg_options ) ) {

									$parsed_url_3		= parse_url( $bg_options['buyers_guide_2016_link_3'] );
									$google_tracking_3	= '';

									if(  strpos( home_url(), $parsed_url_3['host'] ) === FALSE  ) {
										$google_tracking_3 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_3'] .'\') )"';
									}

									echo '<a href="'. $bg_options['buyers_guide_2016_link_3'] .'" '. $google_tracking_3 .'>
										<img src="'. $bg_options['buyers_guide_2016_image_3'] .'">
									</a>';
								}
							?>

						</div>

					</div>

					<div class="vbg_full">
						<?php // 4
							if( array_key_exists( 'buyers_guide_2016_link_4', $bg_options ) ) {
								$parsed_url_4		= parse_url( $bg_options['buyers_guide_2016_link_4'] );
								$google_tracking_4	= '';

								if( strpos( home_url(), $parsed_url_4['host'] ) === FALSE  ) {
									$google_tracking_4 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_4'] .'\') )"';
								}
								echo '<a href="'. $bg_options['buyers_guide_2016_link_4'] .'" '. $google_tracking_4 .'>
									<img src="'. $bg_options['buyers_guide_2016_image_4'] .'">
								</a>';
							}

						?>

					</div>

					<div class="vbg_row">

						<div class="vbg_onesixth">

							<?php // 5
								if( array_key_exists( 'buyers_guide_2016_link_5', $bg_options ) ) {
									$parsed_url_5		= parse_url( $bg_options['buyers_guide_2016_link_5'] );
									$google_tracking_5	= '';

									if( strpos( home_url(), $parsed_url_5['host'] ) === FALSE  ) {
										$google_tracking_5 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_5'].'\') )"';
									}
									echo '<a href="'. $bg_options['buyers_guide_2016_link_5'] .'" '. $google_tracking_5 .'>
										<img src="'. $bg_options['buyers_guide_2016_image_5'] .'">
									</a>';
								}

							?>

						</div>

						<div class="vbg_onesixth">
							<?php // 6
								if( array_key_exists( 'buyers_guide_2016_link_6', $bg_options ) ) {
									$parsed_url_6		= parse_url( $bg_options['buyers_guide_2016_link_6'] );
									$google_tracking_6	= '';

									if( strpos( home_url(), $parsed_url_6['host'] ) === FALSE  ) {
										$google_tracking_6 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_6'].'\') )"';
									}

									echo '<a href="'. $bg_options['buyers_guide_2016_link_6'] .'" '. $google_tracking_6 .'>
										<img src="'. $bg_options['buyers_guide_2016_image_6'] .'">
									</a>';
								}
							?>

						</div>

					</div>

					<div class="vbg_full">

						<?php // 7
							if( array_key_exists( 'buyers_guide_2016_link_7', $bg_options ) ) {
								$parsed_url_7		= parse_url( $bg_options['buyers_guide_2016_link_7'] );
								$google_tracking_7	= '';

								if( strpos( home_url(), $parsed_url_7['host'] ) === FALSE  ) {
									$google_tracking_7 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_7'].'\') )"';
								}
								echo '<a href="'. $bg_options['buyers_guide_2016_link_7'] .'" '. $google_tracking_7 .'>
									<img src="'. $bg_options['buyers_guide_2016_image_7'] .'">
								</a>';
							}

						?>

					</div>

					<div class="vbg_row">

						<div class="vbg_onesixth vbg_module">

							<?php // 8
								if( array_key_exists( 'buyers_guide_2016_link_8', $bg_options ) ) {
									$parsed_url_8		= parse_url( $bg_options['buyers_guide_2016_link_8'] );
									$google_tracking_8	= '';

									if( strpos( home_url(), $parsed_url_8['host'] ) === FALSE  ) {
										$google_tracking_8 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_8'].'\') )"';
									}
									echo '<a href="'. $bg_options['buyers_guide_2016_link_8'] .'" '. $google_tracking_8 .'>
										<img src="'. $bg_options['buyers_guide_2016_image_8'] .'">
									</a>';
								}
							?>

						</div>

						<div class="vbg_onesixth vbg_module">

							<?php // 9
								if( array_key_exists( 'buyers_guide_2016_link_9', $bg_options ) ) {
									$parsed_url_9		= parse_url( $bg_options['buyers_guide_2016_link_9'] );
									$google_tracking_9	= '';

									if( strpos( home_url(), $parsed_url_9['host'] ) === FALSE  ) {
										$google_tracking_9 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_9'].'\') )"';
									}
									echo '<a href="'. $bg_options['buyers_guide_2016_link_9'] .'" '. $google_tracking_9 .'>
										<img src="'. $bg_options['buyers_guide_2016_image_9'] .'">
									</a>';
								}
							?>

						</div>

					</div>

					<div class="vbg_full">

						<?php // 10
							if( array_key_exists( 'buyers_guide_2016_link_10', $bg_options ) ) {
								$parsed_url_10		= parse_url( $bg_options['buyers_guide_2016_link_10'] );
								$google_tracking_10	= '';

								if( strpos( home_url(), $parsed_url_10['host'] ) === FALSE  ) {
									$google_tracking_10 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_10'].'\') )"';
								}
								echo '<a href="'. $bg_options['buyers_guide_2016_link_10'] .'" '. $google_tracking_10 .'>
									<img src="'. $bg_options['buyers_guide_2016_image_10'] .'">
								</a>';
							}

						?>

					</div>

				</div>

				<div class="vbg_onethird">

					<div class="vbg_full">

						<?php // 11
							if( array_key_exists( 'buyers_guide_2016_link_11', $bg_options ) ) {
								$parsed_url_11		= parse_url( $bg_options['buyers_guide_2016_link_11'] );
								$google_tracking_11	= '';

								if( strpos( home_url(), $parsed_url_11['host'] ) === FALSE  ) {
									$google_tracking_11 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_11'].'\') )"';
								}
								echo '<a href="'. $bg_options['buyers_guide_2016_link_11'] .'" '. $google_tracking_11 .'>
									<img src="'. $bg_options['buyers_guide_2016_image_11'] .'">
								</a>';
							}

						?>

					</div>

					<div class="vbg_row">

						<div class="vbg_onesixth">

							<?php // 12
								if( array_key_exists( 'buyers_guide_2016_link_12', $bg_options ) ) {
									$parsed_url_12		= parse_url( $bg_options['buyers_guide_2016_link_12'] );
									$google_tracking_12	= '';

									if( strpos( home_url(), $parsed_url_12['host'] ) === FALSE  ) {
										$google_tracking_12 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_12'].'\') )"';
									}
									echo '<a href="'. $bg_options['buyers_guide_2016_link_12'] .'" '. $google_tracking_2 .'>
										<img src="'. $bg_options['buyers_guide_2016_image_12'] .'">
									</a>';
								}

							?>

						</div>

						<div class="vbg_onesixth vbg_nomargin">

							<div class="vbg_full">

								<?php // 13
									if( array_key_exists( 'buyers_guide_2016_link_13', $bg_options ) ) {
										$parsed_url_13		= parse_url( $bg_options['buyers_guide_2016_link_13'] );
										$google_tracking_13	= '';

										if( strpos( home_url(), $parsed_url_13['host'] ) === FALSE  ) {
											$google_tracking_13 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_13'].'\') )"';
										}
										echo '<a href="'. $bg_options['buyers_guide_2016_link_13'] .'" '. $google_tracking_13 .'>
											<img src="'. $bg_options['buyers_guide_2016_image_13'] .'">
										</a>';
									}

								?>

							</div>

							<div class="vbg_full">

								<?php // 14
									if( array_key_exists( 'buyers_guide_2016_link_14', $bg_options ) ) {
										$parsed_url_14		= parse_url( $bg_options['buyers_guide_2016_link_14'] );
										$google_tracking_14	= '';

										if( strpos( home_url(), $parsed_url_14['host'] ) === FALSE  ) {
											$google_tracking_14 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_14'].'\') )"';
										}

										echo '<a href="'. $bg_options['buyers_guide_2016_link_14'] .'" '. $google_tracking_14 .'>
											<img src="'. $bg_options['buyers_guide_2016_image_14'] .'">
										</a>';
									}

								?>

							</div>
						</div>
					</div>

					<div class="vbg_full">

						<?php // 15
							if( array_key_exists( 'buyers_guide_2016_link_15', $bg_options ) ) {
								$parsed_url_15		= parse_url( $bg_options['buyers_guide_2016_link_15'] );
								$google_tracking_15	= '';

								if( strpos( home_url(), $parsed_url_15['host'] ) === FALSE  ) {
									$google_tracking_15 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_15'].'\') )"';
								}
								echo '<a href="'. $bg_options['buyers_guide_2016_link_15'] .'" '. $google_tracking_15 .'>
									<img src="'. $bg_options['buyers_guide_2016_image_15'] .'">
								</a>';
							}

						?>

					</div>
					<div class="vbg_full">

						<?php // 16
							if( array_key_exists( 'buyers_guide_2016_link_16', $bg_options ) ) {
								$parsed_url_16		= parse_url( $bg_options['buyers_guide_2016_link_16'] );
								$google_tracking_16	= '';

								if( strpos( home_url(), $parsed_url_16['host'] ) === FALSE  ) {
									$google_tracking_16 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_16'].'\') )"';
								}
								echo '<a href="'. $bg_options['buyers_guide_2016_link_16'] .'" '. $google_tracking_16 .'>
									<img src="'. $bg_options['buyers_guide_2016_image_16'] .'">
								</a>';
							}

						?>

					</div>
					<div class="vbg_row">
						<div class="vbg_onesixth">

							<?php // 17
								if( array_key_exists( 'buyers_guide_2016_link_17', $bg_options) ) {
									$parsed_url_17		= parse_url( $bg_options['buyers_guide_2016_link_17'] );
									$google_tracking_17	= '';

									if( strpos( home_url(), $parsed_url_17['host'] ) === FALSE  ) {
										$google_tracking_17 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_17'].'\') )"';
									}
									echo '<a href="'. $bg_options['buyers_guide_2016_link_17'] .'" '. $google_tracking_17 .'>
										<img src="'. $bg_options['buyers_guide_2016_image_17'] .'">
									</a>';
								}
							?>

						</div>
						<div class="vbg_onesixth">
							<?php // 18
								if( array_key_exists( 'buyers_guide_2016_link_18', $bg_options ) ) {
									$parsed_url_18		= parse_url( $bg_options['buyers_guide_2016_link_18'] );
									$google_tracking_18	= '';

									if( strpos( home_url(), $parsed_url_18['host'] ) === FALSE  ) {
										$google_tracking_18 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_18'].'\') )"';
									}
									echo '<a href="'. $bg_options['buyers_guide_2016_link_18'] .'" '. $google_tracking_18 .'>
										<img src="'. $bg_options['buyers_guide_2016_image_18'] .'">
									</a>';
								}

							?>

						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="vbg_lastthird">

			<div class="vbg_ad">
				<h4>Advertisement</h4>
				<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'side-middle' ) : ''; ?>
			</div>

			<div class="vbg_row">
				<div class="vbg_onethird">
					<div class="vbg_full">

						<?php // 19
							if( array_key_exists( 'buyers_guide_2016_link_19', $bg_options ) ) {
								$parsed_url_19		= parse_url( $bg_options['buyers_guide_2016_link_19'] );
								$google_tracking_19	= '';

								if( strpos( home_url(), $parsed_url_19['host'] ) === FALSE  ) {
									$google_tracking_19 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_19'].'\') )"';
								}
								echo '<a href="'. $bg_options['buyers_guide_2016_link_19'] .'" '. $google_tracking_19 .'>
									<img src="'. $bg_options['buyers_guide_2016_image_19'] .'">
								</a>';
							}
						?>
					</div>

					<div class="vbg_row">
						<div class="vbg_onesixth">

							<?php // 20
								if( array_key_exists( 'buyers_guide_2016_link_20', $bg_options ) ) {
									$parsed_url_20		= parse_url( $bg_options['buyers_guide_2016_link_20'] );
									$google_tracking_20	= '';

									if( strpos( home_url(), $parsed_url_20['host'] ) === FALSE  ) {
										$google_tracking_20 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_20'].'\') )"';
									}
									echo '<a href="'. $bg_options['buyers_guide_2016_link_20'] .'" '. $google_tracking_20 .'>
										<img src="'. $bg_options['buyers_guide_2016_image_20'] .'">
									</a>';
								}
							?>
						</div>
						<div class="vbg_onesixth">

							<?php // 21
								if( array_key_exists( 'buyers_guide_2016_link_21', $bg_options ) ) {
									$parsed_url_21		= parse_url( $bg_options['buyers_guide_2016_link_21'] );
									$google_tracking_21	= '';

									if( strpos( home_url(), $parsed_url_21['host'] ) === FALSE  ) {
										$google_tracking_21 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_21'].'\') )"';
									}
									echo '<a href="'. $bg_options['buyers_guide_2016_link_21'] .'" '. $google_tracking_21 .'>
										<img src="'. $bg_options['buyers_guide_2016_image_21'] .'">
									</a>';
								}
							?>
						</div>
					</div>
					<div class="vbg_full">

						<?php // 22
							if( array_key_exists( 'buyers_guide_2016_link_22', $bg_options ) ) {
								$parsed_url_22		= parse_url( $bg_options['buyers_guide_2016_link_22'] );
								$google_tracking_22	= '';

								if( strpos( home_url(), $parsed_url_22['host'] ) === FALSE  ) {
									$google_tracking_22 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_22'].'\') )"';
								}
								echo '<a href="'. $bg_options['buyers_guide_2016_link_22'] .'" '. $google_tracking_22 .'>
									<img src="'. $bg_options['buyers_guide_2016_image_22'] .'">
								</a>';
							}
						?>
					</div>
				</div>
				<div class="vbg_onethird">
					<div class="vbg_onesixth vbg_module">

						<?php // 23
							if( array_key_exists( 'buyers_guide_2016_link_23', $bg_options ) ) {
								$parsed_url_23		= parse_url( $bg_options['buyers_guide_2016_link_23'] );
								$google_tracking_23	= '';

								if( strpos( home_url(), $parsed_url_23['host'] ) === FALSE  ) {
									$google_tracking_23 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_23'].'\') )"';
								}
								echo '<a href="'. $bg_options['buyers_guide_2016_link_23'] .'" '. $google_tracking_23 .'>
									<img src="'. $bg_options['buyers_guide_2016_image_23'] .'">
								</a>';
							}

						?>
					</div>
					<div class="vbg_onesixth vbg_module vbg_nomargin">
						<div class="vbg_full">

							<?php // 24
								if( array_key_exists( 'buyers_guide_2016_link_24', $bg_options ) ) {
									$parsed_url_24		= parse_url( $bg_options['buyers_guide_2016_link_24'] );
									$google_tracking_24	= '';

									if( strpos( home_url(), $parsed_url_24['host'] ) === FALSE  ) {
										$google_tracking_24 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_24'].'\') )"';
									}
									echo '<a href="'. $bg_options['buyers_guide_2016_link_24'] .'" '. $google_tracking_24 .'>
										<img src="'. $bg_options['buyers_guide_2016_link_24'] .'">
									</a>';
								}

							?>
						</div>
						<div class="vbg_full">

							<?php // 25
								if( array_key_exists( 'buyers_guide_2016_link_25', $bg_options ) ) {
									$parsed_url_25		= parse_url( $bg_options['buyers_guide_2016_link_25'] );
									$google_tracking_25	= '';

									if( strpos( home_url(), $parsed_url_25['host'] ) === FALSE  ) {
										$google_tracking_25 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_25'].'\') )"';
									}
									echo '<a href="'. $bg_options['buyers_guide_2016_link_25'] .'" '. $google_tracking_25 .'>
										<img src="'. $bg_options['buyers_guide_2016_image_25'] .'">
									</a>';
								}

							?>
						</div>
					</div>
					<div class="vbg_full">
						<?php // 26
							if( array_key_exists( 'buyers_guide_2016_link_26', $bg_options ) ) {
								$parsed_url_26		= parse_url( $bg_options['buyers_guide_2016_link_26'] );
								$google_tracking_26	= '';

								if( strpos( home_url(), $parsed_url_26['host'] ) === FALSE  ) {
									$google_tracking_26 = 'onclick="_gaq.push( [\'_trackEvent\', \'VeloNews Buyer\'s Guide\', \'velonews\', \''. $bg_options['buyers_guide_2016_link_26'].'\') )"';
								}
								echo '<a href="'. $bg_options['buyers_guide_2016_link_26'] .'" '. $google_tracking_26 .'>
									<img src="'. $bg_options['buyers_guide_2016_image_26'] .'">
								</a>';
							}

						?>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>


<?php get_footer();
