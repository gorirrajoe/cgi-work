<?php
/**
 * Template part for displaying liveblog posts.
 *
 * @package velonews
 */

?>

<section id="article" <?php post_class( array( 'col-xs-12', 'col-sm-7', 'col-md-8', 'article' ) ); ?>>

	<?php
		/**
		 * breadcrumb
		 */
		get_breadcrumb();
	?>

	<header class="article__header">

		<?php the_title( '<h1 class="article__header__title">', '</h1>' );

		if ( 'post' === get_post_type() ) {
			velonews_posted_on();
		} ?>
	</header>


	<section class="article__body live-blog row">

		<?php
			/**
			 * social sharing
			 */
			get_social_sharing( 'side' );
		?>

		<div id="article-right" class="col-xs-12 col-md-11">

			<?php get_social_sharing( 'top' ); ?>

			<article class="live-blog__article">
				<?php
					// $content_string		= apply_filters( 'the_content', get_the_content() );

					$liveblog_tumblr_api	= cgi_bikes_get_option( 'tumblr_liveblog_API_key' );
					$liveblog_tumblr_url	= cgi_bikes_get_option( 'tumblr_liveblog_subdomain' );
					$liveblog_tag			= get_post_meta( get_the_ID(), '_liveblog_tag', 1 );

					the_content();
				?>
			</article>

			<div class="live-blog-container"></div>
			<div class="liveblog_nav"></div>

			<script type="text/javascript">
				var velonews = {
					'liveblog': true,
					'adSize': '<?php echo ( ! is_mobile() && ! is_tablet() && is_active_sidebar( 'ia-728x90' ) ? '728x90' : '320x250' ); ?>',
					'tumblrURL': '<?php echo $liveblog_tumblr_url; ?>',
					'tumblrAPI': '<?php echo $liveblog_tumblr_api; ?>',
					'tumblrTag': '<?php echo $liveblog_tag; ?>'
				};
			</script>

		</div>



	</section>
</section><!-- #article -->
