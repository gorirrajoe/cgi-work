/**
 * Calculate Scrollbar Width
 *
 * Calculate the scrollbar width (if exists).
 *
 * @return int Width of scrollbars
 */
function calcScrollbarWidth() {
	var inner, outer, w1, w2;

	inner = document.createElement('p');
		inner.style.width = "100%";
		inner.style.height = "200px";

	outer = document.createElement('div');
		outer.style.position = "absolute";
		outer.style.top = "0px";
		outer.style.left = "0px";
		outer.style.visibility = "hidden";
		outer.style.width = "200px";
		outer.style.height = "150px";
		outer.style.overflow = "hidden";
		outer.appendChild(inner);

	document.body.appendChild(outer);

	w1 = inner.offsetWidth;
	outer.style.overflow = 'scroll';

	w2 = inner.offsetWidth;

	if(w1 == w2) {
		w2 = outer.clientWidth;
	}

	document.body.removeChild(outer);

	return (w1 - w2);
}


jQuery(document).ready(function($) {
	var $lazyLoad = $('img.lazyload'),
		$fancybox = $('.fancybox'),
		$collapse = $('.collapse'),
		$newsFeed = $('#news-feed-nav');
		$tabularData = $( '.tabular__data' ),
		$privacyBar = $('#cookie-popup-bar');


	/**
	 * Ad Block Enabled
	 *
	 * Fired if/when the Ad Blocker detection script detects an ad blocker.
	 */
	function adBlockEnabled() {
		$('#leaderboard .ad-blocker').show();

		$('.alert__dismiss__link').on('click', function() {
			$(this).parents('.alert').fadeOut();
		});

		dataLayer.push({ 'adBlocker': 'true' });
	}

	/**
	 * Ad Block Disabled
	 *
	 * Fired if the Ad Blocker detection script doesn't detect an ad blocker.
	 */
	function adBlockDisabled() {
		dataLayer.push({ 'adBlocker': 'false' });
	}

	if(typeof dataLayer !== 'undefined') {

		if(typeof blockAdBlock === 'undefined') {
			adBlockEnabled();
		} else {
			blockAdBlock.on(true, adBlockEnabled);
			blockAdBlock.on(false, adBlockDisabled);
		}

	}

	/**
	 * LazyLoad
	 *
	 * Initializes the lazyLoad module on any image containing the
	 * '.lazyload' class.
	 */
	if ($lazyLoad.length > 0) {
		$lazyLoad.lazyload({
			effect: 'fadeIn',
		});
	}

	/**
	 * Fancybox
	 *
	 * Initializes the fancybox module on any element containing the
	 * '.fancybox' class.
	 */
	if ($fancybox.length > 0) {

		$fancybox.fancybox({
			caption: function( instance, item ) {
				var caption = $(this).data('caption') || '';

				if ( item.type === 'image' && caption == '' ) {

					caption = $( 'img', this ).attr( 'alt' );

				}

				return caption;
			}
		});

	}

	/**
	 * Bootstap collapse
	 *
	 * Initializes Bootstrap collapse on any element containing the '.collapse'
	 * class.
	 */
	if ($collapse.length > 0) {

		$('.collapse').collapse({
			toggle: false
		}).on('click', function(e) {
			var $target = $(e.target);

			/*
			 * if target doesn't have "collapse" class link,
			 * prevent event bubbling
			 */
			if (!$target.hasClass('collapse')) {
				e.stopImmediatePropagation();
			}
		});

	}

	/**
	 * News Feed Tab Listener
	 *
	 * Initializes the bootstrap tab functionality for the news feed module.
	 */
	if ($newsFeed.length > 0) {

		$newsFeed.click('a', function(e) {
			e.preventDefault();
			$(this).tab('show');
		})

	}

	/**
	 * remove styling that wordpress adds to images with captions
	 */
	$(".wp-caption").removeAttr('style');


	/**
	 * get table headings
	 * used in conjunction with the [table] shortcode
	 * if used properly (with headers), the table will look spiffy
	 * in smaller browser sizes
	 */
	if( $tabularData.length > 0 ) {

		var obj = {};

		$( '.tabular__data' ).each( function () {
			var headings	= [];
			$tableCount		= $( this ).data( 'tablecount' );
			$tableHeadings	= $( this ).find( 'th' );
			$tableData		= $( this ).find( 'td' );

			$tableHeadings.each( function () {

				headings.push( $( this ).text() );

			});

			obj[$tableCount] = headings;
			var i = 0;

			$tableData.each( function() {

				$( this ).attr( 'data-title', headings[i] );
				i++;

				if( i > ( headings.length - 1 ) ) {
					i = 0;
				}

			});
		});
	}

	/**
	 * cookie bar/privacy notice
	 */
	if( $privacyBar.length > 0 ) {

		$('.close-cookie-bar').on('click', function(e){
			e.preventDefault();

			var objectName  = 'vn_eu_regulation';

			var d		= new Date();
			var days	= 180;
			var time	= d.getTime() + ( days * 24 * 60 * 60 * 1000 );

			// if browser supports localStorage use it (IE8<)
			if ( typeof( Storage ) !== 'undefined' && !localStorage.getItem( objectName ) ) {

				// iOS 8.3+ Safari Private Browsing mode throws a quota exceeded JS error with localStorage.setItem
				try {
				    var regulationObject   = { timestamp: time };
				    localStorage.setItem( objectName, JSON.stringify( regulationObject ) );
				} catch ( error ) {
				    // Do nothing with localStore if iOS 8.3+ Safari Private Browsing mode, we are still using the cookie.
				}

			}

			// Set the cookie regardless of support
			d.setTime( time );
			document.cookie = objectName + '=1; expires=' + d.toUTCString();

			$('.cookie-popup-bar').slideUp();
		});

	} else {

		var objectName  = 'vn_eu_regulation';

		// Check localStorage for our data
		if ( typeof( Storage ) !== 'undefined' && localStorage.getItem( objectName ) ) {

			// Delete if the date is too old (we want a friendly reminder every 180 days)
			var regulationObject	= JSON.parse( localStorage.getItem( objectName ) ),
				dateTime			= regulationObject.timestamp,
				now					= new Date().getTime().toString();

			if ( now > dateTime) {
				localStorage.removeItem( objectName );
			}

		}

	}

});
