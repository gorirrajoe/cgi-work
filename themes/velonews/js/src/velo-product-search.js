jQuery(document).ready(function($) {
	var $productSearchForm = $('#product-search-multifacet'),
		$productSearchFormToggle = $('.product-search__form-toggle'),
		$productSearchFilterToggle = $('.product-search__filter-toggle'),
		$showHiddenToggle = $('.show-hidden-toggle');

	// Hide the "see more" link
	$('.show-hidden-toggle').on('click', function () {
		$(this).parent('.product-search__more').hide();
	})

	/**
	 * Product Search Form Event Handler
	 *
	 * Adds some UX improvements to the product search form.
	 */
	if($productSearchForm.length > 0) {
		var $productSearchSubmit = $productSearchForm.find('input[type="submit"]');

		$productSearchForm.on('click', 'input', function(e) {
			var $input = $(e.target),
				$value = $input.val(),
				$fieldset = $input.parents('fieldset');

			if('all' !== $value) {

				// Uncheck the "All" filter
				$fieldset.find('input[value="all"]').prop('checked', false);

				// If nothing is checked, check the "All" option
				if(false === $(this).is(':checked')) {

					if( 0 == $fieldset.find('input:checked').length) {

						$fieldset.find('input[value="all"]').prop('checked', true);

					}

				}

			} else {

				/**
				 * Don't allow the user to deselect the "All" filter, rather
				 * force them to select any other filer to disable "All".
				 */
				if(false === $(this).is(':checked')) {
					return false;
				}

				// Uncheck everything but the "All" filter
				$fieldset.find('input[value!="all"]').prop('checked', false);

			}

		});

	}

	/**
	 * Product Search Event Listeners
	 *
	 * Event listeners for the Product Search page(s).
	 */
	if($productSearchFormToggle.length > 0) {
		var resized = false;

		/**
		 * Product Search Form Toggle
		 *
		 * Toggles the display of the search form on mobile devices.
		 */
		$productSearchFormToggle.on('click', function(e) {
			var $content = $('#content'),
				$form = $('#product-search-form'),
				offset = ($content.height() - $content.find('.content__title').parent().height()) + ($content.next('.advert').height());

			e.preventDefault();

			if($form.is(':visible')) {

				$form.toggle('slide', {direction: 'left'}, 500);

			} else {

				$form.css('height', offset).toggle('slide', {direction: 'left'}, 500);

			}

		});

		/**
		 * Product Search Filter Toggle
		 *
		 * Toggles the display of the specified filter options.
		 */
		$productSearchFilterToggle.on('click', function(e) {
			var target = $(this).attr('href'),
				$arrow = $(this).find('.fa');

			e.preventDefault();

			$(target).toggle();

			if($arrow.hasClass('fa-caret-down')) {

				$arrow.removeClass('fa-caret-down').addClass('fa-caret-up');

			} else {

				$arrow.removeClass('fa-caret-up').addClass('fa-caret-down');

			}
		})

		/**
		 * Search Form Visibility Logic
		 *
		 * On devices where the user can change the browser width such that the
		 * layout alternates between mobile and desktop views, the search form's
		 * height and visibility need to be adjusted accordingly.
		 */
		$(window).resize(function() {
			var windowWidth = $(window).width(),
				$form = $('#product-search-form');

			if(windowWidth > 990 && false === $form.is(':visible')) {

				$form.css('height', '').show();

			} else if(windowWidth <= 990 && true === $form.is(':visible')) {

				$form.hide();

			}

		});
	}


	if( $showHiddenToggle.length > 0 ) {

		$showHiddenToggle.on( 'click', function( e ) {
			$( '.product-search__form__option__hidden' ).removeClass( 'product-search__form__option__hidden' );
		});

	}
});
