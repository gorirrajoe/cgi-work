jQuery(document).ready(function($) {

	/**
	 * Validate submission
	 *
	 * Handles the email validation and error handling process.
	 *
	 * @param $form
	 * @param $email
	 * @param $submit
	 */
	function validate_submission($form, $email, $submit) {
		var error, regex;

		$email.attr('readonly', true);
		$submit.attr('disabled', true);

		/**
		 * Validation
		 */
		error = new Boolean(false);
		regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

		/**
		 * fail: if email field is empty OR if it's the default value OR if validation doesn't pass
		 */
		if (
			$($email).val() == '' ||
			$($email).val() == $($email)[0].defaultValue ||
			false === regex.test($($email).val())
		) {

			$email.addClass("validation-error");
			$form.find(".newsletter__error").html("There was an error with your email address.").show();
			$email.removeAttr("readonly");

		} else {

			$email.removeClass("validation-error");

			$email.hide();
			$submit.hide();

			$form.find('.newsletter__loading').show();

			/* Ajax submission code here */
			$.post(
				"./",
				$form.serialize() + "&ajax=true",
				function(data) {
					 $form.parent('.newsletter__wrap').html(data);
				}
			);

		}

		return false;
	}

	/**
	 * Sidebar Newsletter Submit Handler
	 */
	$('.newsletter__form').submit(function(e) {
		var $input = $(this).find('.newsletter__input_name_email'),
			$submit = $(this).find('.btn_type_submit');

		e.preventDefault();

		return validate_submission($(this), $input, $submit);
	});

	/**
	 * Sidebar Newsletter Focus/BlurHandler
	 */
	$('.newsletter__input_name_email').focus(function() {
		var $form = $(this).parents('form');

		$(this).removeClass('validation-error');

		$form.find('.btn_type_submit').removeAttr('disabled');
		$form.find('.newsletter__error').html('');
	});

});
