jQuery(document).ready(function($) {
	var $genericCarousel = $('.generic-carousel'),
		$thumbsCarousel = $("#thumbs-carousel"),
		$listCarousel = $("#list-carousel"),
		$productCarousel = $("#product-carousel"),
		$relatedCarousel = $("#related-carousel"),
		$stageReportToggle = $('#stage-report-toggle'),
		vn = (window.velonews !== undefined ? window.velonews : {}),
		owlInitialized = false,
		owlInTransition = false,
		owlSlideDuration = 300;

	/**
	 * Align Owl Carousel
	 *
	 * Places the previous, next, and dots nav items in the correct location by
	 * calculating the height of the first image in the carousel.
	 */
	function alignOwlCarousel($carousel) {
		var $thumbnail = $carousel.find('.active img').first(),
			$prev = $carousel.find('.owl-prev'),
			$next = $carousel.find('.owl-next'),
			$dots = $carousel.find('.owl-dots'),
			$ad = $carousel.find('.gallery-ad-last-slide'),
			arrowOffset = (Math.floor($thumbnail.height() - $prev.height()) / 2),
			dotsOffset = Math.floor($thumbnail.height() - $dots.height());

		$prev.css('top', arrowOffset);
		$next.css('top', arrowOffset);
		$dots.css('top', dotsOffset);

		if($ad.length > 0) {
			$ad.css('height', $thumbnail.height());
			$ad.find('img').css('max-height', $thumbnail.height());
		}
	};

	/**
	 * Align Thumbnail Carousel
	 *
	 * Places the previous, next, and dots nav items in the correct location by
	 * calculating the height of the carousel stage.
	 */
	function alignThumbnailCarousel($carousel) {
		var $stage = $carousel.find('.owl-stage-outer'),
			$prev = $carousel.find('.owl-prev'),
			$next = $carousel.find('.owl-next'),
			arrowOffset = Math.floor(($stage.height() - $prev.height()) / 2);

		$prev.css('top', arrowOffset);
		$next.css('top', arrowOffset);
	}

	/**
	 * Sync List Carousels
	 *
	 * Triggers the corresponding carousel to jump to the defined location so
	 * that the "thumbsCarousel" and "listCarousel" are set to the same index.
	 */
	function syncListCarousels($target, index) {

		if(false === owlInTransition) {

			owlInTransition = true;

			$thumbsCarousel.find('.current').removeClass('current');
			$thumbsCarousel.find('.owl-item:nth-child(' + (index + 1) + ')').addClass('current');

			$target.trigger('to.owl.carousel', [index, owlSlideDuration, true]);

			owlInTransition = false;
		}
	}

	/**
	 * Init Generic Owl Carousels
	 *
	 * Encapsulates the genericCarousel logic inside a function so that it can
	 * be called programatically from other enclosures.
	 */
	function initGenericCarousel($carousel, align) {

		$carousel
			.owlCarousel({
				items: 1,
				loop: true,
				nav: true,
				navText: ['<span class="fa fa-caret-left"></span>','<span class="fa fa-caret-right"></span>'],
				dots: true,
				lazyLoad: true,
				responsiveClass: true,
				navSpeed: 800,
				dotsSpeed: 800,
				dragEndSpeed: 800,
				onLoadedLazy: function() {

					if(true === align || false === owlInitialized) {

						owlInitialized = true;

						// center arrows with carousel
						return alignOwlCarousel($carousel);
					}

				},
				onResized: function() {

					// center arrows with carousel
					return alignOwlCarousel($carousel);

				}
			})
			.on('changed.owl.carousel', function (e) {

				// refresh page ads
				if(typeof(googletag) !== 'undefined' && typeof(googletag.pubads) == 'function' ) {
					googletag.pubads().refresh();
				}

			});

	}

	/**
	 * Generic Owl Carousels
	 *
	 * Initializes the owl carousel module on any element containing the
	 * "generic-carousel" class.
	 */
	if($genericCarousel.length > 0 ) {
		initGenericCarousel($genericCarousel);
	}

	/**
	 * Thumbnail Owl Carousels
	 *
	 * Initializes the owl carousel module on any element whose ID is
	 * "thumbs-carousel".
	 *
	 * Note: The "thumbs-carousel" is used in conjunction with the "list-carousel".
	 */
	if($thumbsCarousel.length > 0) {

		$thumbsCarousel
			.owlCarousel({
				center: true,
				nav: true,
				navText: ['<span class="fa fa-caret-left"></span>','<span class="fa fa-caret-right"></span>'],
				dots: false,
				responsiveClass: true,
			    responsive: {
					0: {
						items: 3
					},
					1024: {
						items: 5
					}
				},
				onInitialized: function() {
					var current = ($(this)[0]._current + 1);

					// add "current" class to the current active item
					$thumbsCarousel.find('.owl-item:nth-child(' + current + ')').addClass('current');

					// center arrows with carousel
					return alignThumbnailCarousel($thumbsCarousel);
				},
				onResized: function() {

					// center arrows with carousel
					return alignThumbnailCarousel($thumbsCarousel);
				}
			})
			.on('changed.owl.carousel', function (e) {

				// synchronize thumbnail and list carousels
				syncListCarousels($listCarousel, e.item.index);

			})
			.on('click', '.owl-item', function () {

				// jump to given index
				$thumbsCarousel.trigger('to.owl.carousel', [$(this).index(), owlSlideDuration, true]);

			});

	}

	/**
	 * List Owl Carousels
	 *
	 * Initializes the owl carousel module on any element whose ID is
	 * "list-carousel".
	 */
	if($listCarousel.length > 0) {

		$listCarousel
			.owlCarousel({
				items: 1,
				nav: true,
				navText: ['<span class="fa fa-caret-left"></span>','<span class="fa fa-caret-right"></span>'],
				dots: false,
				mouseDrag: false,
				touchDrag: false,
				lazyLoad: true,
				onLoadedLazy: function() {

					if(false === owlInitialized) {

						owlInitialized = true;

						// center arrows with carousel
						return alignOwlCarousel($listCarousel);
					}

				},
				onResized: function() {

					// center arrows with carousel
					return alignOwlCarousel($listCarousel);

				}
			})
			.on('changed.owl.carousel', function (e) {

				// synchronize thumbnail and list carousels
				syncListCarousels($thumbsCarousel, e.item.index);

				// refresh page ads
				if(typeof(googletag) !== 'undefined' && typeof(googletag.pubads) == 'function') {
					googletag.pubads().refresh();
				}

			});

	}

	/**
	 * Product Owl Carousel
	 *
	 * Initializes the owl carousel module on any element whose ID is
	 * "product-carousel".
	 */
	if($productCarousel.length > 0) {

		$productCarousel
			.owlCarousel({
				nav: true,
				navText: ['<span class="fa fa-caret-left"></span>','<span class="fa fa-caret-right"></span>'],
				dots: false,
				responsiveClass: true,
			    responsive: {
					0: {
						items: 3
					},
					1024: {
						items: 5
					}
				},
				onInitialized: function() {

					// center arrows with carousel
					return alignThumbnailCarousel($productCarousel);
				},
				onResized: function() {

					// center arrows with carousel
					return alignThumbnailCarousel($productCarousel);
				}
			});
	}

	/**
	 * Related Owl Carousel
	 *
	 * Initializes the owl carousel module on any element whose ID is
	 * "related-carousel".
	 */
	if($relatedCarousel.length > 0) {

		$relatedCarousel
			.owlCarousel({
				nav: true,
				navText: ['<span class="fa fa-caret-left"></span>','<span class="fa fa-caret-right"></span>'],
				dots: false,
				responsiveClass: true,
				responsive: {
					0: {
						items: 1
					},
					768: {
						items: 3
					}
				},
				onInitialized: function() {

					// center arrows with carousel
					return alignThumbnailCarousel($relatedCarousel);
				},
				onResized: function() {

					// center arrows with carousel
					return alignThumbnailCarousel($relatedCarousel);
				}
			});
	}

	/**
	 * Race Single Carousels
	 *
	 * When you try to initialize an OwlCarousel within a hidden element
	 * (e.g. boostrap tab), things go badly because hidden elements have no
	 * width, so Owl does not know how much space it has to work with. Therefore
	 * the work-around is to wait until a tab is shown to initialize the carousel,
	 * then destroy the carousel each time the tab is hidden.
	 *
	 * @ref http://stackoverflow.com/questions/28347479/owl-carousel-and-bootstrap-tab-on-a-page
	 */
	if($stageReportToggle.length > 0) {
		var $raceCarousel = $('.race__pane.active .race-carousel'),
			loaded = [];

		if($raceCarousel.length > 0) {

			// add active carousel id to "loaded" array
			loaded.push($raceCarousel.attr('id'));

			// initalize the active carousel
			initGenericCarousel($raceCarousel);

		}

		// hook into bootstrap tab event hook
		$('#article-nav a').on('shown.bs.tab', function () {
			var $raceCarousel = $('.race__pane.active .race-carousel');

			if($raceCarousel.length > 0 && loaded.indexOf($raceCarousel.attr('id')) < 0) {

				// add active carousel id to "loaded" array
				loaded.push($raceCarousel.attr('id'));

				// initalize the active carousel
				initGenericCarousel($raceCarousel, true);

			}

		});

	}

	/**
	 * Generic Carousel Event Listener
	 *
	 * Custom event listner that allows the liveblog template to call the Generic
	 * Carousel logic from outside of this closure.
	 */
	if(true === vn.liveblog) {

		$(window).on('load.genericCarousel', function() {
			initGenericCarousel($('.generic-carousel'));
		});

	}
});
