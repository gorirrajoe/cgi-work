jQuery(document).ready(function($) {

	/**
	 * Add User Acceptabce Testing Markup
	 *
	 * Adds the necssary UAT markup and styling to the page.
	 */
	function addUatMarkup() {
		var html, modalHeight, modalOffset;

		modalHeight = $(document).height() + 'px';
		modalOffset = Math.floor($(window).height() * .2) + 'px';


		$('head').append('<link rel="stylesheet" href="/wp-content/themes/velonews/css/uat.min.css" type="text/css" media="all">');

		html = '<!-- start UAT -->';

			html += '<div id="uat-stop" class="uat uat_section_btn">';
				html += '<button id="uat-stop-button" class="uat__btn">';
					html += 'END TEST';
				html += '</button>';
			html += '</div>';

			html += '<div id="uat-overlay" class="uat uat_section_overlay" style="height:' + modalHeight + ';">';
				html += '<div class="uat__modal" style="top:' + modalOffset + '">';
					html += '<h1>Test Complete</h1>';
					html += '<p>Thank you for completing this test. This window will close itself momentarily.</p>';
				html += '</div>';
			html += '</div>';

		html += '<!-- end UAT -->';

		$('body').append(html);
	}

	// add markup to page on load
	addUatMarkup();

	/**
	 * End Test Event Listener
	 *
	 * Listens for a "click" event on the end test button and then fades in
	 * the "Thank You" modal.
	 * @param  {[type]} '#uat-stop-button' [description]
	 * @return {[type]}                    [description]
	 */
	$('#uat-stop-button').on('click', function(e) {
		var endTest, $overlay = $('#uat-overlay');

		endTest = confirm('Would you like to end this test?');

		if(true === endTest) {

			$overlay.fadeIn('fast');

			$('html, body').animate({
				scrollTop : 0
			},
			800,
			function() {
				$overlay.find('.uat__modal').fadeIn();
			});

		} else {

			e.stopImmediatePropagation();

		}

	});

});
