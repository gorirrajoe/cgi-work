/**
 * Adds accessibility parameters to the main navigation menu and enables TAB key
 * navigation support for dropdown menus.
 *
 * **Note:** The order of events is extremely important to set the ARIA
 * attributes properly, so do not re-order things unless you know what you are
 * doing.
 */
jQuery(document).ready(function($) {
	var $mainNav = $('#main-nav'),
		$megaMenus = $mainNav.find('.mega-menu'),
		$menus = $mainNav.find('.main-nav__menu-item > a'),
		$subMenus = $mainNav.find('.main-nav__sub-menu-item > a'),
		$articleNav = $('#article-nav'),
		$socialNav = $('#social-bar-top'),
		loadedMenus = [];

	/**
	 * Inititalize Mega Menus
	 *
	 * Adds the "active" class to the first '.mega-menu__menu' item in each
	 * Mega Menu. This ensures that when the user hovers over a mega menu they
	 * actually see a menu.
	 */
	function initMegaMenus() {

		$megaMenus.each(function() {
			$(this).find('.mega-menu__menu').first().addClass('active');
		});

	}

	/**
	 * Hide Open Menus
	 *
	 * Hides previously opened menus that were opened using the tab event
	 * listener.
	 */
	function hideMenu() {
		var $dropdown = $mainNav.find('.main-nav__sub-menu.active'),
			$megaMenu = $mainNav.find('.mega-menu__container.active');

		// check if any dropdown items are currently active
		if($dropdown.length > 0) {

			$dropdown.removeClass('active').attr('aria-hidden', 'true')
				.find('a').attr('tabIndex', -1);

		}

		// check if any mega menu items are currently active
		if($megaMenu.length > 0) {
			hideMegaMenu($megaMenu);
		}

	}

	/**
	 * Show Menu
	 *
	 * Displays the sub-menu of the currently selected menu item.
	 */
	function showMenu($menu, hover) {

		$menu.attr('aria-hidden', 'false')
			.find('a')
				.attr('tabIndex', 0);

		if(false === hover) {
			$menu.addClass('active');
		}

	}

	/**
	 * Show Mega Menu
	 *
	 * Displays the mega-menu of the currently selected menu item.
	 */
	function showMegaMenu($menu, $container, hover) {
		var target = $menu.find('.active > a').data('target');

		$menu.attr('aria-hidden', 'false')
			.find('a')
				.attr('tabIndex', 0);

		if(false === hover) {
			$container.addClass('active');
		}

		$container.find('.mega-menu__menu')
			.attr('aria-hidden', 'true');

		$container.find('.mega-menu__menu.active')
			.attr('aria-hidden', 'false');

		if(loadedMenus.indexOf(target) < 0) {

			$(target).find('img').lazyload();

			loadedMenus.push(target);
		}
	}

	/**
	 * Hide Mega Menu
	 *
	 * Hides any open mega-menus on blur.
	 */
	function hideMegaMenu($megaMenu) {

		$megaMenu.removeClass('active')
			.find('.main-nav__sub-menu').attr('aria-hidden', 'true')
				.find('a').attr('tabIndex', -1);

		$megaMenu.find('.mega-menu__menu').attr('aria-hidden', 'true');

	}

	/**
	 * Mobile Footer Sub-Navigation Listener
	 *
	 * Listens for 'click' actions resulting from elements tagged with the
	 * '.dropdown-nav' class. If a click results from a '.dropdown-nav'
	 * element, the corresponding dropdown is displayed.
	 */
	$('.mobile-nav__sub-menu').on('click', '.dropdown-nav', function(e) {
		var $target = $(e.target),
			$parent = $target.parent();

		$(this).toggleClass('active');

		if($target.is('a') && '#' != $target.attr('href') && !$parent.hasClass('dropdown-nav')) {
			window.location.href = $target.attr('href');
			$('#mobile-nav-toggle').removeClass('active');
		}

		return false;

	});

	/**
	 * Mobile Footer Navigation Listener
	 *
	 * Listens for 'click' actions resulting from elements tagged with the
	 * '.dropdown-nav' class. If a click results from a '.dropdown-nav'
	 * element, the corresponding dropdown is displayed.
	 */
	$('#mobile-nav-toggle').on('click', 'a', function(e) {
		var $parent = $(this).parent(),
			$target = $(e.target);

		if( $parent.hasClass('mobile-nav__menu-item') ) {
			e.preventDefault();
			$parent.toggleClass('active');
		}

	});

	/**
	 * Add Event Listners only if a mega menu exists.
	 */
	if($megaMenus.length > 0) {

		// add "active" class to mega menus where necessary
		initMegaMenus();

		$megaMenus.hover(
			function() {
				// mouseenter: do nothing
			},
			function() {
				// mouseout: reset aria attributes
				hideMegaMenu($(this));
			}
		);

		/**
		 * Mega Menu Sub-Navigation Listener
		 *
		 * Listens for 'mouseover' or 'hover' actions fired by a mega-menu sub-nav
		 * element and assigns the current 'li' the active class.
		 */
		$('.mega-menu .main-nav__sub-menu-item').hover(function() {
			var target = $(this).find('a').data('target');

			$(this).parents('.main-nav__sub-menu').find('.main-nav__sub-menu-item').removeClass('active');
			$(this).addClass('active');

			$(target).parent().find('.mega-menu__menu').removeClass('active').attr('aria-hidden', 'true');
			$(target).addClass('active').attr('aria-hidden', 'false');

			if(loadedMenus.indexOf(target) < 0) {

				$(target).find('img').lazyload();

				loadedMenus.push(target);
			}

		});

	}

	/**
	 * Add Event Listners only if main navigation exists. This is sort of a given,
	 * but you never know, and it only adds a few milliseconds to page load.
	 */
	if($mainNav.length > 0) {

		/**
		 * Add/remove ARIA markup on hover event
		 */
		$menus.hover(function(e) {

			hideMenu();

			if(!$(e.target).parent().hasClass('mega-menu')) {

				showMenu($(this).next('.main-nav__sub-menu'), true);

			} else {

				$container = $(this).next('.mega-menu__container');
				showMegaMenu($container.find('.main-nav__sub-menu'), $container, true);

			}

		});

		/**
		 * Add/remove ARIA markup AND show/hide menu on tab (aka "focus") event
		 */
		$menus.focus(function(e) {
			var $container;

			hideMenu();

			if(!$(e.target).parent().hasClass('mega-menu')) {

				showMenu($(this).next('.main-nav__sub-menu'), false);

			} else {

				$container = $(this).next('.mega-menu__container');
				showMegaMenu($container.find('.main-nav__sub-menu'), $container, false);

			}

		});

		/**
		 * Trigger "mouseenter" event for mega-menu when using tab key.
		 */
		$mainNav.find('.mega-menu .main-nav__sub-menu-item > a').focus(function() {
			$(this).trigger('mouseenter');
		});

		/**
		 * When tabbing through the site make sure we don't leave any menus open.
		 * This could get expensive, but since we're doing a length check above,
		 * it's not so bad. There might be a better way though?
		 */
		$('a').focus(function(e) {

			if($(e.target).parents('.main-nav__menu-item').length < 1) {
				hideMenu();
			}

		});

		/**
		 * Bind arrow keys to main navigation
		 */
		$menus.keydown(function(e) {
			var $parent = $(this).parent('li');

			switch(e.keyCode) {

				// left arrow (go to next top-level item)
				case 37:
					e.preventDefault();

					// Check if we're at the begining of menu
					if($parent.prev('li').length == 0) {
						$mainNav.find('.main-nav__menu-item').last().find('a').first().focus();
					} else {
						$parent.prev('li').find('a').first().focus();
					}

					break;

				// right arrow (go to next top-level item)
				case 39:
					e.preventDefault();

					// Check if we're at the end of menu
					if($parent.next('li').length == 0) {
						$mainNav.find('.main-nav__menu-item').first().find('a').first().focus();
					} else {
						$parent.next('li').find('a').first().focus();
					}

					break;
			}

		});

		/**
		 * Bind arrow keys to sub-navigation
		 */
		$subMenus.keydown(function(e) {
			var $parent = $(this).parents('.main-nav__menu-item');

			switch(e.keyCode) {

				// escape (close menu and go to next top-level item)
				case 27:
					e.preventDefault();

					hideMenu();
					$parent.next().find('a').first().focus();

					break;

				// left arrow (go to next top-level item)
				case 37:
					e.preventDefault();

					// Check if we're at the begining of menu
					if($parent.prev('li').length == 0) {
						$mainNav.find('.main-nav__menu-item').last().find('a').first().focus();
					} else {
						$parent.prev('li').find('a').first().focus();
					}

					break;

				// right arrow (go to next top-level item)
				case 39:
					e.preventDefault();

					// Check if we're at the end of menu
					if($parent.next('li').length == 0) {
						$mainNav.find('.main-nav__menu-item').first().find('a').first().focus();
					} else {
						$parent.next('li').find('a').first().focus();
					}

					break;
			}

		});

	}

	/**
	 * Add Event Listners only if an article nav exists.
	 */
	if($articleNav.length > 0) {
		var $activeTab = $articleNav.find('.active');

		// assert that browser has pushState available and there is no "active" tab
		if( 'undefined' != window.history.pushState && 0 >= $activeTab.length ) {
			var activeHash = '', activeTab = '';

			activeHash = $articleNav.find('li:first-child a').attr('href');
			activeTab = '?tab=' + activeHash.replace(/#/, '');

			// update the "hash"
			history.replaceState(null, null, activeTab);

			// add the "active" class to the appropriate li
			$articleNav.find('a[href="'+ activeHash +'"]').parent().addClass('active');

		}

		/**
		 * Article Nav Event Listener
		 *
		 * When a tab is clicked, update the "location hash"
		 */
		$articleNav.on('click', 'a', function() {

			if( 'undefined' != window.history.pushState ) {
				var activeTab = '?tab=' + $(this).attr('href').replace(/#/, '');

				history.replaceState(null, null, activeTab);
			}

		});

	}


	/**
	 * Menu actions for buyers guides
	 *
	 *
	 */

	$bgMenu = $( '.bg__final_top_menu' );
	$('.bg__final_top_menu .dropdown-nav a').attr('aria-haspopup', true);
	$bgMenu.prepend('<li class="dropdown-nav" id="bg-mobile-nav-toggle"><a href="#" id="mobile-nav-title">Categories</a></li>');
	$bgMenu.children('li').addClass('top-level');

	$(window).on('load resize', function(){
		//Mobile menu toggles and clicks
		if( $(window).width() < 660 ){

			$('#bg-mobile-nav-toggle').off('click').on('click', function(e) {
				var $parent = $(this).parent(),
					$target = $(e.target);
					e.preventDefault();
					$parent.children('li').not('#bg-mobile-nav-toggle').not('.sub-menu li').toggle(300);
			});
			$('.bg__final_top_menu .sub-menu').hide();
		} else {
			$('.bg__final_top_menu .sub-menu').removeAttr('style');
			$('.bg__final_top_menu li.top-level').removeAttr('style');
		}


		$(".bg__final_top_menu .menu-item-has-children").children('a').off('click').on('click', function(e){
			e.preventDefault();
			$(this).toggleClass('active');
		    $(this).parent().children("ul").toggle();

		});


	});

	// if( $(window).width() >=661 ){
	// 	$('.bg__final_top_menu .sub-menu').removeAttr('style');
	// 	$('.bg__final_top_menu li.top-level').removeAttr('style');
	// } else {
	// 	$('.bg__final_top_menu .sub-menu').hide();
	// }

	//truncate menu if more than 9 elements
	var $lis = $('ul.bg__final_top_menu > li'),
		$guide_link = $('.bg__final_title a').attr('href');

		//check if there are more than 5 elements
	if($lis.length > 9) {
	    //if so remove them and replace with a more link
	    $lis.slice(8).wrapAll('<li class="extras top-level dropdown-nav menu-item-has-children">').wrapAll('<ul class="sub-menu">');
	    $('.extras').prepend('<a href="' + $guide_link + '">More</a>');
	}

	//Joey's version
	//  var $submenuMobileLabel = $('.gg-submenu__label'),
	//         $submenu = $('.gg-submenu');

	//     if( $submenuMobileLabel.length ) {

	//         $submenuMobileLabel.on( 'click', function() {
	//             $submenu.slideToggle();
	//         });

	//     }

	//     $('.gg-submenu .has-sub-menu > a').click( function( e ) {
	//         e.preventDefault();
	//         $( this ).parentsUntil( 'ul' ).siblings().children().next( '.sub-menu' ).slideUp();
	//         $( this ).next().slideToggle();

	//     });

	// });


	// $( window ).click( function( e ) {

	//     $container = $( '.gg-submenu' );
	//     $toggle = $( '.gg-submenu .has-sub-menu a' );

	//     if( !$container.is( e.target ) && $container.has( e.target ).length === 0 ) {
	//         $toggle.siblings( '.sub-menu' ).slideUp();
	//     }

	// });

	// alert('test');
	// if($socialNav.length > 0) {
	// 	$(window).scroll(function() {
	// 		if (window.pageYOffset > 50) {

	// 			$socialNav.css({
	// 				'position': 'fixed',
	// 				'top': '0',
	// 				'width': '100%',
	// 				'z-index': '9999'
	// 			});

	// 		} else {

	// 			$socialNav.css('position', '');

	// 		}
	// 	});
	// }
});
