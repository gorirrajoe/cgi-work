jQuery(document).ready(function($) {
	var $ratingsChart = $('#ratings-chart'),
		$ratingsBar = $('.ratings-chart__rating__bar'),
		ratingsChartInitialized = false;

	/**
	 * Animate Ratings Chart
	 *
	 * Creates a simple loading animation for the ratings bar based on the given
	 * scores.
	 *
	 * @param (int) delay Number of milliseconds to delay execution
	 */
	function animateRatingsChart(delay) {

		if(false !== ratingsChartInitialized) {
			return;
		}

		$ratingsBar.delay(delay).each(function() {
			$(this).animate({
				width: $(this).attr('data-percent')
			}, 2000);
		});

		ratingsChartInitialized = true;
	}

	/**
	 * Ratings Bar
	 *
	 * Initializes the ratings bar module on any element containing the
	 * '.ratings-chart__rating__bar' class.
	 */
	if($ratingsBar.length > 0 && false === ratingsChartInitialized) {

		/**
		 * If the chart is visible, run the animation, if not, add scroll event
		 * listener to trigger animation later.
		 */
		if($ratingsBar.offset().top <= $(window).height()) {

			animateRatingsChart(300);

		} else {

			$(window).scroll(function() {
				var top_of_element = $ratingsChart.offset().top,
					bottom_of_element = (top_of_element + $ratingsChart.outerHeight()),
					bottom_of_screen = ($(window).scrollTop() + $(window).height());

				// Delay animation until the ratings chart comes into focus
				if((bottom_of_screen > top_of_element) && (bottom_of_screen < bottom_of_element)) {
					animateRatingsChart(300);
				}

			});

		}

	}
});
