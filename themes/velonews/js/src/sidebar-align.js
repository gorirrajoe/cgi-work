/**
 * Sidebar - align with featured image
 */
jQuery(document).ready(function($) {
	var $article_body			= $( '.article__body ' ),
		$sidebar				= $( '.sidebar' ),
		sidebar_margin_offset	= 0,
		windowWidth				= $(window).width() +calcScrollbarWidth(),
		style					= '';


	/**
	 * Note: wait until "load" event so element heights are set.
	 */
	$(window).on('load', function() {

		if( $article_body.length && $sidebar.length && windowWidth >= 992 ) {

			sidebar_margin_offset = $article_body.offset().top - $sidebar.offset().top;

			$( '.sidebar' ).css( 'margin-top', sidebar_margin_offset );

		}

	});

});
