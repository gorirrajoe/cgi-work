/**
 * Logic for displaying the "sticky" social sharing icons on the site.
 */
jQuery(document).ready(function($) {
	var $header = $('#header'),
		$adminBar = $('#wpadminbar'),
		$articleHeader = $('.article__header'),
		$article = $('#article'),
		$articleLeft = $('#article-left'),
		$articleRight = $('#article-right'),
		$topBar = $('#social-bar-top'),
		$leftBar = $('#social-bar-left'),
		topBoundry = 0,
		bottomBoundry = 0,
		paddingLeft = 0,
		positionTop = 0,
		positionBottom = 0,
		widgetWidth = 0,
		windowWidth = 0,
		adminBarSticky = false,
		activeWidget = '',
		fixed = false,
		fixedBottom = false,
		longForm = false;

	/**
	 * Init
	 *
	 * Constructor method which calls all the utility methods used to
	 * calculate the relevant element dimensions.
	 */
	function init() {

		windowWidth = ( $(window).width() + calcScrollbarWidth() );

		longForm = $('#content').hasClass('template--long-form');

		if( windowWidth <= 767 ) {

			activeWidget = 'mobile';

		} else if( windowWidth <= 991 && !longForm ) {

			activeWidget = 'tablet';

			generateMobileWidgetStyles();

		} else {

			activeWidget = 'desktop';

			$articleLeft.css( 'height', $articleRight.outerHeight() );

		}


		if( windowWidth >= 601 && $adminBar.length ) {

			adminBarSticky = true;

		}

		topBoundry = calcTopBoundry();
		bottomBoundry = calcBottomBoundry();

		positionTop = calcPositionTop();
		// positionBottom = calcPositionBottom();

	}


	/**
	 * Calculate Top Boundry
	 *
	 * "topBoundry" defines the point at which the social widget should
	 * become "fixed" when scrolling down, or "relative" when scrolling up.
	 *
	 * Note: This is the highest we want the widget to go on the page.
	 *
	 * @return int Position of the "top" scroll boundry.
	 */
	function calcTopBoundry() {

		if( 'mobile' === activeWidget ) {

			if( adminBarSticky == true ) {

				return $articleRight.offset().top - $adminBar.outerHeight();

			} else {

				return $articleRight.offset().top;

			}

		} else {

			return $articleRight.offset().top - $header.height() - $adminBar.height() - 15;

		}

	}

	/**
	 * Calculate Bottom Boundry
	 *
	 * "bottomBoundry" defines the point at which the social widget
	 * should become "absolute" when scrolling down, or "fixed" when
	 * scrolling up.
	 *
	 * Note: This is the lowest we want the widget to go on the page.
	 *
	 * @return int Position of the "bottom" scroll boundry.
	 */
	function calcBottomBoundry() {

		if('desktop' === activeWidget ) {

			if( adminBarSticky == true ) {

				return $articleRight.outerHeight() + $articleRight.offset().top - ( $header.height() + $adminBar.height() + 165 );

			} else {

				return $articleRight.outerHeight() + $articleRight.offset().top - ( $header.height() + 165 );

			}

		}

	}

	/**
	 * Calculate Position Top
	 *
	 * "positionTop" defines the y-coordinate at which the social widget
	 * should remained fixed at while the widget is "fixed".
	 *
	 * @return int Position of the widget while "fixed".
	 */
	function calcPositionTop() {
		return ($header.outerHeight() + $adminBar.height() + 15);
	}


	/**
	 * Generate Mobile Widget Styles
	 *
	 * Adds dynamically generated styles to the HEAD.
	 */
	function generateMobileWidgetStyles() {
		var style = '';

		style += '@media screen and (min-width:768px) {';
			style += '.footer__copyright {';
				style += 'padding-bottom: '+ ( $topBar.outerHeight() + 30 ) + 'px';
			style += '}';
		style += '}';

		if($('#velonews-sb-css').length < 1) {

			$('<style id="velonews-sb-css">'+ style +'</style>').appendTo('head');

		} else {

			$('#velonews-sb-css').html(style);

		}

	}

	/**
	 * Position Mobile Widget
	 *
	 * Controls the positioning of the social widget for screens that
	 * are <= 991px wide.
	 *
	 * @param int position Current position of the scrollbar
	 */
	function positionMobileWidget(position) {

		$articleRight.removeClass('content-left--fixed');
		$leftBar.removeClass('social-bar-left--fixed');

		if(true === fixed) {

			if( true === fixedBottom && position <= topBoundry ) {

				$topBar.css({
					'bottom': '',
					'position': '',
					'left': '',
					'top': '',
					'padding': '',
					'box-shadow': ''
				});

				fixedBottom = false;

			}

			// top-boundry logic
			if( position <= topBoundry) {

				$topBar.removeClass('social-bar--fixed');
				$articleRight.removeClass('article__body--fixed');

				$leftBar.css({
					'position': 'relative',
					'top': '0',
					'bottom': ''
				});

				fixed = false;

			}

		} else {

			// top-boundry logic
			if(position >= topBoundry ) {

				$topBar.addClass('social-bar--fixed');
				$articleRight.addClass('article__body--fixed');

				$leftBar.css({
					'position': 'fixed',
					'top': positionTop,
					'bottom': ''
				});

				fixed = true;

			}
		}

	}

	/**
	 * Position Desktop Widget
	 *
	 * Controls the positioning of the social widget for screens that
	 * are >= 992px wide.
	 *
	 * @param int position Current position of the scrollbar
	 */
	function positionDesktopWidget(position) {

		$articleRight.removeClass('article__body--fixed');

		if(true === fixed) {

			// bottom boundry logic
			if(false === fixedBottom && position >= bottomBoundry) {

				$leftBar.css({
					'bottom': 0,
					'position': 'absolute',
					'top': ''
				});

				fixedBottom = true;

			} else if(true === fixedBottom && position <= bottomBoundry) {

				$leftBar.css({
					'position': 'fixed',
					'top': positionTop,
					'bottom': ''
				});

				fixedBottom = false;

			}

			// top-boundry logic
			if( position <= topBoundry ) {

				$leftBar.removeClass('social-bar-left--fixed');
				$articleRight.removeClass('content-left--fixed');
				$topBar.removeClass('social-bar--fixed');

				$leftBar.css({
					'position': 'relative',
					'top': '0'
				});

				fixed = false;

			}

		} else {

			// top-boundry logic
			if( position >= topBoundry ) {

				$leftBar.addClass('social-bar-left--fixed');
				$articleRight.addClass('content-left--fixed');
				$topBar.addClass('social-bar--fixed');

				$leftBar.css({
					'position': 'fixed',
					'top': positionTop
				});

				fixed = true;

			}

		}

	}

	/**
	 * Note: wait until "load" event so element heights are set.
	 */
	$(window).on('load', function() {

		if( $topBar.length ) {

			init();

			// bind window events logic
			$(window).scroll(function() {

				if('desktop' === activeWidget) {

					positionDesktopWidget( $(this).scrollTop() );

				} else {

					positionMobileWidget( $(this).scrollTop() );

				}

			}).resize(function() {

				var newWidth = ( $( window ).width() + calcScrollbarWidth() );

				// don't call init unless screen width has changed
				if( 0 === windowWidth || newWidth !== windowWidth ) {

					init();

				}

			});

		}

	});

});
