jQuery(document).ready(function($) {
	var currentpage = '',
		x = 0;

	/**
	 * Do Lazy Load
	 *
	 * Fires the necessary lazyload logic so that the images on the live blog
	 * render after they are added to the DOM.
	 */
	function doLazyLoad() {

		$('img.lazyload').lazyload({
			effect: 'fadeIn',
			event: 'initLazyLoad',
			failure_limit: 10
		})
		.trigger('initLazyLoad');

		$(window).trigger('load.genericCarousel');

		loaded = true;
	}

	/**
	 * Poll Tumblr
	 *
	 * Queries the tumblr API on a continuous basis to retrieve the latest data.
	 */
	function pollTumblr() {
		var vn = (window.velonews !== undefined ? window.velonews : {});

		$.ajax({
			url: 'http://api.tumblr.com/v2/blog/'+ vn.tumblrURL +'.tumblr.com/posts?api_key='+ vn.tumblrAPI,
			dataType: 'jsonp',
			data: {
				'tag': vn.tumblrTag,
				'offset': x, 'limit': 50
			},
			success: function( posts ) {
				var postings = posts.response.posts,
					text = '',
					result = 0;

				for ( var i in postings ) {
					var p = postings[i],
						taglength = p.tags.length,
						author = '',
						authorfound = 0,
						title = '',
						titlefound = 0,
						posted = new Date( p.timestamp * 1000 ),
						postedDate = posted.toLocaleDateString();
						postedTime = posted.toLocaleTimeString(),
						advert = '',

					text += '<article class="live-blog__article">';

						for( var x = 0; x < taglength; x++ ) {

							if( p.tags[x].search( /author/i ) != -1 ) {

								author = p.tags[x].replace( 'author:', '' );
								authorfound = 1;

							} else if( ( p.tags[x].search( /author/i ) == -1 && ( authorfound != 1 ) ) ) {

								author = 'Guest Poster';
								authorfound = 0;

							}

							if( p.tags[x].search( /title/i ) != -1 ) {

								title = p.tags[x].replace( 'title:', '' );
								titlefound = 1;

							} else if( ( p.tags[x].search( /title/i ) == -1 && ( titlefound != 1 ) ) ) {

								title = '';
								titlefound = 0;

							}

						}

						if( p.type == 'photo' ) {

							text += '<!-- photo --><h2>' + title + '</h2>';

							if( p.photos.length > 1 ) {

								text += '<div class="owl-carousel generic-carousel">';

									for( var x in p.photos ) {
										text += '<figure>';
											text += '<a href="' + p.photos[x].original_size.url + '" data-fancybox="gallery-' + i + '">';
												text += '<img class="owl-lazy" data-src="' + p.photos[x].original_size.url + '" alt="">';
											text += '</a>';
										text += '</figure>';
									}

								text += '</div>';

							} else {

								text += '<figure><a href="' + p.photos[0].original_size.url + '" data-fancybox><img data-original="' + p.photos[0].original_size.url +'" class="lazyload" alt=""></a></figure>';

							}

							text += '<p>' + p.caption +'</p>';

						} else if( p.type == 'text' ) {

							text += '<!-- text --><h2>' + p.title + '</h2>' + p.body;

						} else if( p.type == 'video' ) {

							if( p.video_type == 'youtube' ) {

								text += '<!-- youtube video --><h2>' + title + '</h2>';
								text += '<div class="video-container">' + p.player[2].embed_code + '</div>' + p.caption;

							} else {

								text += '<!-- video --><h2>' + title + '</h2>';
								text += '<div class="video-container">';
									text += '<iframe width="700" height="394" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" data-height="394" data-width="700" data-can-resize="" data-can-gutter="" scrolling="no" class="embed_iframe tumblr_video_iframe" style="display: block; background-color: transparent; overflow: hidden; width: 100%; height: 394px;" src="https://www.tumblr.com/video/' + vn.tumblrURL + '/' +  p.id + '/700/"></iframe>';
								text += '</div>';

							}

						} else if( p.type == 'quote' ) {

							text += '<!-- quote --><h2>' + title + '</h2>';
							text += '<blockquote>';
								text += '<p>' + p.text + '</p>';
								text += '<cite>- ' + p.source + '</cite>';
							text += '</blockquote>';

						} else if( p.type == 'link' ) {
							var excerpt = '',
								link_image = '';

							if( p.excerpt ) {
								excerpt = '<p>' + p.excerpt + '</p>';
							}

							if( p.link_image ) {

								link_image = '<figure>';
									link_image += '<a href="' + p.url + '" target="_blank">';
										link_image += '<img src="' + p.link_image + '" alt="">';
									link_image += '</a>';
								link_image += '</figure>';

							}

							text += '<!-- link --><h2>' + title + '</h2>';
							text +=  link_image;
							text += '<h2>';
								text += '<a href="' + p.url + '" target="_blank">' + p.title + ' &gt;</a>';
							text +='</h2>';
							text += excerpt;
							text += p.description;

						}

						text += '<div class="live-blog__article__timestamp">Posted by <strong>' + author + '</strong> on ' + postedDate +' @ '+ postedTime +'</div>';

					text += '</article>';

					// add inline ad after 3rd item (note: "i" is zero-based)
					if( i == '2' ) {

						// 320x250 up to 1200px wide
						advert = ( '728x90' === vn.adSize ? 'div-728_90_inarticle' : 'div-300_250_inarticle' );

						text += '<section class="advert advert_xs_300x250 advert_md_728x90 advert_location_inline">';
							text += '<div class="advert__wrap">';
								text += '<div id="' + advert + '">';
									text += '<script type="text/javascript">googletag.cmd.push(function(){googletag.display("' + advert + '");});</script>';
								text += '</div>';
							text += '</div>';
						text += '</section>';

					}

				}

				$('div.live-blog-container').html(text);

				result = ( posts.response.total_posts / 50 );

				if( result <= 1 ) {

					$( '.liveblog_nav' ).remove();

				} else {
					var nav = '';

					$( '.liveblog_nav_pages' ).nextAll().remove();

					if( currentpage != 1 ) {

						nav += '<div class="navright">';
							nav += '<a href="?tumblrpage='+ ( +currentpage - 1 ) +'">Newer Posts &raquo;</a>';
						nav += '</div>';

					}

					if( currentpage != Math.ceil( result ) ) {

						nav += '<div class="navleft">';
							nav += '<a href="?tumblrpage='+ ( +currentpage + 1 ) +'">&laquo; Older Posts</a>';
						nav += '</div>';

					}

				}

				$('.liveblog_nav').html(nav);

			},
			complete: function() {

				// refresh page ads
				if(typeof(googletag) !== 'undefined') {
					googletag.pubads().refresh();
				}

				// fire lazyload actions
				doLazyLoad();

				// After completion of request, time to redo it after 5 minutes (in milliseconds)
				setTimeout( pollTumblr, 120000 );

			}

		});
	}

	if ( window.location.search.indexOf( 'tumblrpage=' ) > -1 ) {
		var url = $( location ).attr( 'href' );
			url = url.split( '=' );

		x = url[1] * 50 - 50;
		currentpage = url[1];

	} else {

		currentpage = 1;

	}

	// call pollTumblr function on page load
	pollTumblr();

});
