/**
 * Copyright (C) 2008 Brightcove, Inc.  All Rights Reserved.  No
 * use, copying or distribution of this work may be made except in
 * accordance with a valid license agreement from Brightcove, Inc.
 * This notice must be included on all copies, modifications and
 * derivatives of this work.
 *  
 * Brightcove, Inc MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT
 * THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR
 * NON-INFRINGEMENT. BRIGHTCOVE SHALL NOT BE LIABLE FOR ANY DAMAGES SUFFERED
 * BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS
 * SOFTWARE OR ITS DERIVATIVES.
 * 
 *
 * "Brightcove" is a trademark of Brightcove, Inc.
 * 
 * @description JS Example on how to play external Ads
 **/

    var player;
    var adModule;
    var adkeyword;

    function setAdKeyword(keytxt){
    //alert(keytxt);
    adkeyword = keytxt;
    }


    // called when template loads, we use this to store a reference to the player and modules
    // and add event listeners for external Ads

    function onTemplateLoaded(experienceID) {

     //alert(experienceID);

        // 'experienceID' is equivalent to the html object id (ie: "myExperience")
        player= brightcove.getExperience(experienceID);
    // Get a reference to the Ad Module API
    adModule = player.getModule(APIModules.ADVERTISING);
    //Enabled External Ads
    adModule.enableExternalAds(true);

   //pre/mid ad assign start
   var adPolicy = new Object();
   adPolicy.prerollAds = true;
   adPolicy.midrollAds = true;
   adPolicy.postrollAds = true;


   Preadkeyword = adkeyword + ",preroll";
   Midadkeyword = adkeyword + ",midroll";
   Postadkeyword = adkeyword + ",postroll";

   Preadkeyword = encodeURIComponent(Preadkeyword);
   Midadkeyword = encodeURIComponent(Midadkeyword);
   Postadkeyword = encodeURIComponent(Postadkeyword);
   
   //alert("pre : " + Preadkeyword);
   //alert("mid : " + Midadkeyword);

   adPolicy.prerollAdKeys ="&kw=" + Preadkeyword;   
   adPolicy.midrollAdKeys ="&kw=" + Midadkeyword;  
   adPolicy.postrollAdKeys ="&kw=" + Postadkeyword;


   //alert("keyword :" + adkeyword);
   adModule.setAdPolicy(adPolicy);
   //pre/mid ad assign end

        
    // Add a callback function for the external Ads    event
    adModule.addEventListener(BCAdvertisingEvent.EXTERNAL_AD, onExternalAd);
    adModule.addEventListener(BCAdvertisingEvent.AD_COMPLETE, onAdComplete);    
 
    //alert('yyy');

    }

    function onExternalAd(evt) {
        // In this specific example, evt.ad contains the synchedBanner XML            
       //alert("it goes!!");
       //alert(evt.ad);
       // Create an XML doc with externalAd XML sent by the player
       
         var adXML = getXMLDoc(evt.ad);
        
         // alert(adXML);
        //Extract specific XML nodes for a video Ad
         var videoAd = getVideoAd(adXML);

         //var bannerAd = getBannerImg(adXML);     
        //Extract specific XML nodes for a Banner
        // var bannerAd = getCollapsedBanner(adXML);
        
        // Render a video Ad using the Adverstising module

         adModule.showAd(videoAd); 
        
        //alert(videoAd.bannerImage);
        //alert(videoAd.bannerImage.trim().length);

        // Add an external banner to the HTML page using JS

         var externalBanner = document.getElementById("externalBanner");
         if (videoAd.bannerImage.trim().length != 0) {
         //var externalBannerHTML = "";
         //else
         var externalBannerHTML = '<img src="' + videoAd.bannerImage  + '" width="1" height="1" />'; 
        
        //writes out the regular anchor/tag to the externalBanner div
        externalBanner.innerHTML = externalBannerHTML;
        }


    }

    function onAdComplete(evt) {
        // Remove the banner on Ad Complete    
        var externalBanner = document.getElementById("externalBanner");        
        //externalBanner.innerHTML ="";
    }
    

     String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
     }


    // Parses a synchedBanner ad XML and creates a video Ad 
    // Change accordingly to the XML that you are trafficking as external ad.
    function getVideoAd(adXML){
        var videoAd = new Object();       
        videoAd.type = "videoAd";
        
        //alert("haha:" + adXML.getElementsById("text"));
        //var nodeItems = adXML.firstChild.childNodes.length;
        //var currentNode = adXML.firstChild.firstChild;
        //alert(adXML.getElementsByTagName("text")[0].firstChild.nodeValue);
        //alert('*' + adXML.getElementsByTagName("text")[0].firstChild.nodeValue + '*')

        if (adXML.getElementsByTagName("text")[0].firstChild.nodeValue == "defaultad" ) {

	//alert("it exist!!");
	videoAd.videoURL = "";
	videoAd.bannerImage = "";
 	videoAd.videoClickURL= "";

        } else {

        var adhtml = adXML.getElementsByTagName("html")[0];
        //var htmllist = /(.*)\[advideo\](.*)\[adimg\](.*)/g.exec(adhtml.firstChild.nodeValue);        
        //alert("html : " + adhtml.firstChild.nodeValue);
        //alert('*' + htmllist[2] + '*');      
        //alert('*' + htmllist[3] + '*'); 
        //alert("here :" + adXML.getElementsByTagName("advideo")[0].firstChild.nodeValue);          
        var adhtmlstr = adhtml.firstChild.nodeValue;
        //var adRE = new RegExp("[AND]", "g")
 
        //if (adhtmlstr.match(adRE) == null ){ 
	if (adhtmlstr.indexOf("[AND]") == -1 ){
        videoAd.videoURL = adhtmlstr.trim();
        videoAd.bannerImage = "";
        }
        else {
        var htmllist=adhtmlstr.split("[AND]");
        //alert(">" + htmllist[0].trim() + "<");
        //alert(">" + htmllist[1].trim() + "<");
        videoAd.videoURL = htmllist[0].trim();
        videoAd.bannerImage = htmllist[1].trim();     
        }

        var adurl = adXML.getElementsByTagName("click")[0];
        //alert("url : " + adurl.firstChild.nodeValue);
        videoAd.videoClickURL = adurl.firstChild.nodeValue;
 
        //alert('length:' + nodeItems);
        //alert('current node : ' + currentNode.type);
        //alert("here 1: " + adXML.getElementByName("text"));
        //ttext = adXML.getElementsByTagName("text");
        //alert("the text : " + ttext.nodeValue);         

        }

        
        return videoAd;
    }    

    // Parses a synchedBanner ad XML and returns an object with the collapse banner URL and click URL
    // Change accordingly to the XML format that you traffic as external ad

    function getXMLDoc(pXML){
    
        var adXML;
        if (window.ActiveXObject) {
            //parses the XML for IE browsers
            adXML = new ActiveXObject("Microsoft.XMLDOM");
            adXML.async = false;
            adXML.loadXML(pXML);
        }
        else //parses the XML for Mozilla browsers
            if (window.XMLHttpRequest) {
                adXML = (new DOMParser()).parseFromString(pXML, "text/xml"); 
            }
    return adXML;        
    }

