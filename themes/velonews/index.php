<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package velonews
 */

get_header(); ?>

<section id="content" class="content archive template--blogroll">
	<div class="container content__container">
		<header class="row">
			<div class="col-xs-12">
				<?php
					/**
					 * breadcrumb
					 */
					get_breadcrumb();
				?>
			</div>


			<div class="col-xs-12">
				<?php
					$page = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

					echo '<h1 class="content__title">All Recent Articles: Page '. $page .'</h1>';
					the_archive_description( '<div class="content__description">', '</div>' );
				?>
			</div>
		</header>


		<div class="row">
			<section class="col-xs-12 col-sm-7 col-md-8 archive">
				<?php
					if ( have_posts() ) {
						$count = 1;

						while ( have_posts() ) {
							the_post();

							get_template_part( 'template-parts/content', 'archive' );

							if( $count == 3 ) { ?>

								<div class="advert advert_xs_300x250 advert_md_728x90 advert_location_inline">
									<div class="advert__wrap">
										<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'middle-archive' ) : ''; ?>
									</div>
								</div><!-- ad unit -->

								<?php
							}
							$count++;
						}

						echo get_paginated_links();

					} else {

						get_template_part( 'template-parts/content', 'none' );

					}
				?>
			</section>
			<?php get_sidebar( 'right' ); ?>
		</div>
	</div>
</section>


<?php
	/**
	 * ad
	 */
?>
<section class="advert advert_xs_300x250 advert_sm_728x90 advert_location_bottom ">
	<div class="advert__wrap">
		<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom' ) : ''; ?>
	</div>
</section>

<?php get_footer();
