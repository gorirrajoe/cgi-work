<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package velonews
 */
$search_type		= function_exists('cgi_bikes_get_option') ? cgi_bikes_get_option( 'search_type' ) : 'wordpress';
$search_name_value	= $search_type == 'bing' ? 'q' : 's';
?>

		<footer class="footer">
			<div class="container footer__container">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-3">
						<section class="footer__section">
							<div class="footer__logo"></div>

							<form role="search" action="<?php echo site_url( '/' ); ?>" method="GET" class="clearfix footer__search-form">
								<label for="footer-search-field" class="screen-reader-text">Search Velo News</label>
								<input type="text" name="<?php echo $search_name_value ?>" id="footer-search-field" placeholder="SEARCH" class="footer__search-field">
								<span class="fa fa-search footer__search-icon"></span>
							</form>
						</section>

						<section class="footer__section">
							<h2>GET THE MAGAZINE</h2>
							<p>Get VeloNews at Your Door!</p>
							<a href="<?php echo cgi_bikes_get_option( 'subscribe_link' ); ?>" class="btn btn--red btn_type_magazine">SUBSCRIBE</a>
						</section>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3">
						<section class="footer__section">
							<?php
								/**
								 * social link variables
								 */
								$twitter	= cgi_bikes_get_option( 'twitter_link' );
								$facebook	= cgi_bikes_get_option( 'facebook_link' );
								$instagram	= cgi_bikes_get_option( 'instagram_link' );
								$youtube	= cgi_bikes_get_option( 'youtube_link' );
							?>
							<h2>CONNECT</h2>
							<p>Get Social with VeloNews</p>
							<ul class="clearfix footer__social-menu">
								<li><a href="<?php echo $twitter; ?>" target="_self" class="fa fa-twitter"><span class="screen-reader-text">VeloNews on Twitter</span></a></li>
								<li><a href="<?php echo $facebook; ?>" target="_self" class="fa fa-facebook"><span class="screen-reader-text">VeloNews on facebook</span></a></li>
								<li><a href="<?php echo $instagram; ?>" target="_self" class="fa fa-instagram"><span class="screen-reader-text">VeloNews on Instagram</span></a></li>
								<li><a href="<?php echo $youtube; ?>" target="_self" class="fa fa-youtube-play"><span class="screen-reader-text">VeloNews on YouTube</span></a></li>
							</ul>
						</section>
						<section class="footer__section">
							<?php newsletter_signup( 'footer' ); ?>
						</section>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3">
						<section class="footer__section">
							<h2>OUR NETWORK</h2>
							<?php
								if( has_nav_menu( 'footer-network' ) ) {
									$args = array(
										'menu'				=> 'footer-network',
										'fallback_cb'		=> false,
										'theme_location'	=> 'footer-network'
									);
									wp_nav_menu( $args );
								}
							?>
						</section>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3">
						<section class="footer__section">
							<h2>SITEMAP</h2>
							<?php
								if( has_nav_menu( 'footer-sitemap' ) ) {
									$args = array(
										'menu'				=> 'footer-sitemap',
										'fallback_cb'		=> false,
										'theme_location'	=> 'footer-sitemap'
									);
									wp_nav_menu( $args );
								}
							?>
						</section>
					</div>
				</div>
			</div>
			<div class="footer__copyright">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 ">
							<p>&copy; <?php echo date( 'Y' );?> POCKET OUTDOOR MEDIA, LLC. ALL RIGHTS RESERVED.</p>
						</div>
					</div>
				</div>
			</div>
		</footer>


		<nav id="mobile-footer-nav" class="container mobile-nav">
			<div class="row">
				<div class="col-xs-12">

					<?php
						/**
						 * mobile sticky footer nav
						 */
						if( has_nav_menu( 'mobile_footer' ) ) {
							$id = get_the_ID();
							$args = array(
								'menu'				=> 'footer-mobile',
								'menu_class'		=> 'mobile-nav__menu',
								'container'			=> '',
								'fallback_cb'		=> false,
								'theme_location'	=> 'mobile_footer',
								'echo'				=> false,
								'walker'			=> new MobileSticky_Walker_Nav_Menu()
							);
							$footernav = wp_nav_menu( $args );
							echo $footernav;
						}
					?>

				</div>
			</div>
		</nav>


		<?php
			get_facebook();

			wp_footer();

			// code to add before closing body tag
			$before_closing_body = cgi_bikes_get_option( 'before_closing_body' );
			if( $before_closing_body AND !( empty( $before_closing_body ) ) ) {
				echo $before_closing_body;
			}
		?>

	</body>
</html>
