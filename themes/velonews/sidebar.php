<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package velonews
 */

?>

<aside id="secondary" class="widget-area" role="complementary">
	<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('left')) : else : endif; ?>
</aside><!-- #secondary -->
