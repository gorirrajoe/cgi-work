<?php
	/* Template Name: Search Page */

	get_header();

	$page	= !empty( $_REQUEST['searchpage'] ) ? htmlentities( $_REQUEST['searchpage'] ) : 1;

	$search_type	= function_exists('cgi_bikes_get_option') ? cgi_bikes_get_option( 'search_type' ) : 'wordpress';

	$search_text = '';
	// $page = 1;
	$offset = 0;
	$type = 'web';
	$results_per_page_web = 10;
	$results_per_page_image = 18;

	if ( isset( $_GET['searchpage'] ) ) {
		$type= $_GET['type'];

		if ( $type == "web" ) {
			$offset = ( ( $page - 1 ) * $results_per_page_web );
		} elseif ( $type == "image" ) {
			$offset = ( ( $page - 1 ) * $results_per_page_image );
		}
	}

	if ( isset( $_GET['q'] ) && $search_type == 'bing' ) {
		$formvalue		= htmlentities( stripslashes( strip_tags( $_REQUEST['q'] ) ), ENT_HTML5, 'UTF-8' );
		$search_text	= htmlentities( stripslashes( strip_tags( $_GET['q'] ) ), ENT_QUOTES | ENT_HTML5, 'UTF-8' );
	} elseif ( isset( $_GET['s'] ) ) {
		$formvalue		= htmlentities( stripslashes( strip_tags( $_GET['s'] ) ), ENT_HTML5, 'UTF-8' );
		$search_text	= htmlentities( stripslashes( strip_tags( $_GET['s'] ) ), ENT_QUOTES | ENT_HTML5, 'UTF-8' );
	} else {
		$formvalue		= '';
		$search_text	= '';
	}
?>

<section id="content" class="content archive template--site-search">
	<div class="container content__container">
		<header class="row">
			<div class="col-xs-12">
				<?php
					/**
					 * breadcrumb
					 */
					get_breadcrumb();
				?>
			</div>
			<div class="col-xs-12">
				<h1 class="content__title">Search <?php echo bloginfo( 'name' ); ?>: Page <?php echo $page; ?></h1>
			</div>
		</header>


		<div class="row">
			<section class="col-xs-12 col-sm-7 col-md-8 site-search">

				<?php if ( $search_type == 'bing' ) {

						echo get_search_form();

						if ( isset( $_REQUEST['searchpage'] ) ) {
							$type= $_REQUEST['type'];

							if( $type == "web" ) {
								$offset = ( ( $page - 1 ) * $results_per_page_web );
							} elseif( $type == "image" ) {
								$offset = ( ( $page - 1 ) * $results_per_page_image );
							}
						}
						if ( isset( $_REQUEST['q'] ) ) {
							$search_text = htmlentities( stripslashes( strip_tags( $_REQUEST['q'] ) ), ENT_QUOTES | ENT_HTML5, 'UTF-8' );
						}


						if ( $search_text != '' ) {
							/**
							 * bing search
							 */
							$accountKeyWeb	= cgi_bikes_get_option( 'bing_web_search_id_v5' );
							$accountKeyImg	= cgi_bikes_get_option( 'bing_img_search_id_v5' );

							$ServiceRootURL	=  'https://api.cognitive.microsoft.com/bing/v5.0/';
							$WebSearchURL	= $ServiceRootURL . 'search?count='. $results_per_page_web .'&offset='. $offset .'&q=';
							$ImageSearchURL	= $ServiceRootURL . 'images/search?count='. $results_per_page_image .'&offset='. $offset .'&q=';

							$url = site_url();
							$url = str_replace( ".lan", ".com", $url );
							$url = str_replace( ".loc", ".com", $url );
							$url = str_replace( 'http://velonews.', 'http://www.velonews.', $url );


							/**
							 * cyclist directory in search
							 * removing for now since content hasn't been added for awhile
							 *
								$search_query = new WP_Query();
								$search_posts = $search_query->query( 's='.$search_text .'&post_type=cyclist' );
								$cycling_team_query = new WP_Query();
								$cycling_team_posts = $cycling_team_query->query( 's='.$search_text .'&post_type=team' );

								if( $search_query->have_posts() ) {
									echo '<div class="posts">
										<h2>Cyclist Directory</h2>
										<ul>';
											while( $search_query->have_posts() ) : $search_query->the_post();
												$title = get_the_title();
												$thumb = get_the_post_thumbnail( get_the_ID(), 'thumbnail' );
												$birthday = simple_fields_get_post_value( get_the_ID(), 'Birthdate', true );
												$hometown = simple_fields_get_post_value( get_the_ID(), 'Hometown', true );
												$height = simple_fields_get_post_value( get_the_ID(), 'Height', true );
												$weight = simple_fields_get_post_value( get_the_ID(), 'Weight', true );
												$link = get_permalink();
												$post_classes = post_class( 'post' );

												echo '<li '. $post_classes.'>
													<a href="' . $link . '">' . $thumb . '</a>
													<h3><a href="' . $link . '">' . $title . '</a></h3>
													<ul>
														<li><strong>Birthdate: </strong>' . $birthday . '</li>
														<li><strong>Hometown: </strong>' . $hometown . '</li>
														<li><strong>Height: </strong>' . $height . '</li>
														<li><strong>Weight: </strong>' . $weight . '</li>
													</ul>
												</li>';
											endwhile;
										echo '</ul>
									</div>';

									$search_query = null;
									wp_reset_postdata();
									echo '<span id="page-nav">
										<a class="more-first" href="../cyclist/">Browse Cyclist Directory</a>
									</span>';
								}

								if( $cycling_team_query->have_posts() ) {
									echo '<div class="posts">
										<h2>Cycling Team Directory</h2>
										<ul>';
											while( $cycling_team_query->have_posts() ) : $cycling_team_query->the_post();
												$title = get_the_title();
												$thumb = get_the_post_thumbnail( get_the_ID(), 'thumbnail' );
												$sponsor = simple_fields_get_post_value( get_the_ID(), 'Sponsor', true );
												$manager = simple_fields_get_post_value( get_the_ID(), 'Manager', true );
												$nationality = simple_fields_get_post_value( get_the_ID(), 'Team Nationality', true );
												$link = get_permalink();
												$post_classes = post_class( 'post' );
												echo '<li '. $post_classes .'>
													<a href="' . $link . '">' . $thumb . '</a>
													<h3><a href="' . $link . '">' . $title . '</a></h3>
													<ul>
														<li><strong>Sponsor: </strong>' . $sponsor . '</li>
														<li><strong>Manager: </strong>' . $manager . '</li>
														<li><strong>Nationality: </strong>' . $nationality . '</li>
													</ul>
												</li>';
											endwhile;
										echo '</ul>
									</div>';

									$cycling_team_query = null;
									wp_reset_postdata();

									echo '<span id="page-nav">';
										echo '<a class="more-first" href="../team/">Browse Cycling Team Directory</a>';
									echo '</span>';
								}
							*/

							if ( $type == "web" ) {

								$context = stream_context_create( array(
									'http' => array(
										'method'	=> 'GET',
										'header'	=> 'Ocp-Apim-Subscription-Key: ' . $accountKeyWeb
									)
								) );

								echo '<nav class="site-search__nav">
									<span class="site-search__nav__item active">Article Results</span> |
									<a href="'. site_url( '/?type=image&searchpage=1&q='. $search_text ) .'" target="_self" class="site-search__nav__item">Image Results</a>
								</nav>';

								$request	= $WebSearchURL . urlencode( $search_text . ' site:' . $url );
								$request	= str_replace( '%26period%3B', '%2E', $request );
								$request	= str_replace( '%26apos%3B', '%27', $request );
								$request	= str_replace( '%26rsquo%3B', '%92', $request );
								$response	= file_get_contents( $request, 0, $context );
								$jsonobj	= json_decode( $response );


								if( array_key_exists( 'webPages', $jsonobj ) && $jsonobj->webPages->totalEstimatedMatches > 0 ) {

									$valueObj = $jsonobj->webPages->value;

									foreach( $valueObj as $value ) {

										echo '<article class="article article_type_search-result article_type_search-result--article">
											<h3 class="article__title">
												<a href="'. $value->url .'" target="_self" class="article__permalink">'. $value->name .'</a>
											</h3>
											<p>'. stripslashes( $value->snippet ) .'</p>
										</article>';

									}
								} else {
									echo '<article class="article article_type_search-result article_type_search-result--article">
										<h3 class="article__title">No web results found.</h3>
									</article>';
								}

							} else {

								$context = stream_context_create( array(
									'http' => array(
										'method'	=> 'GET',
										'header'	=> 'Ocp-Apim-Subscription-Key: ' . $accountKeyImg
									)
								) );

								$count = 1;

								echo '<nav class="site-search__nav">
									<a href="'. site_url( '/?type=web&searchpage=1&q='. $search_text ) .'" target="_self" class="site-search__nav__item">Article Results</a> |
									<span class="site-search__nav__item active">Image Results</span>
								</nav>';


								$request	= $ImageSearchURL . urlencode( $search_text . ' site:' . $url );
								$request	= str_replace( '%26period%3B', '%2E', $request );
								$request	= str_replace( '%26apos%3B', '%27', $request );
								$request	= str_replace( '%26rsquo%3B', '%92', $request );
								$response	= file_get_contents( $request, 0, $context );
								$jsonobj	= json_decode( $response );

								if( array_key_exists( 'totalEstimatedMatches', $jsonobj ) && $jsonobj->totalEstimatedMatches > 0 ) {

									$valueObj = $jsonobj->value;

									echo '<div class="row">';

											foreach( $valueObj as $value ) {

												echo '<article class="col-xs-12 col-sm-4 article article_type_search-result article_type_search-result--image">
													<a href="'. $value->hostPageUrl .'" target="_self" class="article__permalink" title="'. $value->name .'">
														<div class="article__thumbnail__wrap">
															<img src="'. $value->thumbnailUrl .'" alt="" class="article__thumbnail">
														</div>
														<h3 class="article__title">'. $value->name .'</h3>
													</a>
												</article>';

												if( ( $count % 3 ) == 0 ) {
													echo '</div>
													<div class="row">';
												}

												$count++;
											}

									echo '</div>';

								} else {

									echo '<article class="article article_type_search-result article_type_search-result--article">
										<h3 class="article__title">No image results found.</h3>
									</article>';

								}

							}

							echo '<div class="page-links">';

								if( $page > 1 ) {

									$previous_page = $page - 1;
									echo '<a class="prev page-numbers" href="'. site_url('?type='. $type .'&searchpage=' . $previous_page . '&q=' . $search_text) .'">&laquo; Newer Posts</a>';

								}

								if( $type == 'web' ) {

									$page_results_count = ( array_key_exists( 'webPages', $jsonobj ) ) ? count( $jsonobj->webPages->value ) : 0;

								} else {

									$page_results_count = count( $jsonobj->value );

								}

								if( $page_results_count == ${'results_per_page_' . $type} ) {

									$next_page = $page + 1;
									echo '<a class="next page-numbers" href="'. site_url( '?type='. $type .'&searchpage=' . $next_page . '&q=' . $search_text ) .'">Older Posts &raquo;</a>';

								}

							echo '</div>';

						}

					} else {
						// if not bing, do WP regular search
						if ( have_posts() ) :

							get_search_form();

							while ( have_posts() ) : the_post();

							get_template_part( 'template-parts/content', 'archive' );

							endwhile;

							echo '<div class="page-links">';

								$pagination_args	= array(
									'before_page_number'	=> '<span>',
									'after_page_number'		=> '</span>',
									'prev_text'				=> '&laquo; Newer Posts',
									'next_text'				=> 'Older Posts &raquo;'
								);

								echo paginate_links( $pagination_args );

							echo '</div>';

						else:

							get_template_part( 'template-parts/content', 'none' );

						endif;
					}

				?>

			</section>

			<?php get_sidebar( 'right' ); ?>

		</div>

	</div>
</section>

<?php
	/**
	 * ad
	 */
?>
<section class="advert advert_xs_300x250 advert_sm_728x90 advert_location_bottom ">
	<div class="advert__wrap">
		<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom' ) : ''; ?>
	</div>
</section>


<?php get_footer(); ?>
