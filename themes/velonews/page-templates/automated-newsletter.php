<?php
/* Template Name: Automated Newsletter */

	// Title and logo
	$newsletter_title = get_the_title();
	$newsletter_sponsor_image = cgi_bikes_get_option( 'newsletter_sponsor_image' );
	$newsletter_sponsor_link = cgi_bikes_get_option( 'newsletter_sponsor_link' );

	$brand_color = '#c01100';
	$campaign_var = 'newsletter_prologue';
	$logo = get_bloginfo( 'template_url' ). '/images/newsletter/velonews-logo.png';
	$magazines = get_bloginfo( 'template_url' ). '/images/newsletter/velo-mags.jpg';

	// Social Icons + Links
	$facebook_check = cgi_bikes_get_option( 'facebook_link' );
	if ( $facebook_check != '' ) {
		$facebook = '<a href="'. $facebook_check . '" style="text-decoration: none;color: '. $brand_color .';"><img src="' . get_bloginfo( 'template_url' ). '/images/newsletter/ico-facebook-blk.jpg"/></a>';
	}

	$twitter_check = cgi_bikes_get_option( 'twitter_link' );
	if ( $twitter_check != '' ) {
		$twitter = '<a href="'. $twitter_check .'" style="text-decoration: none;color: '. $brand_color .';"><img src="' . get_bloginfo( 'template_url' ). '/images/newsletter/ico-twitter-blk.jpg"/></a>';
	}

	$instagram_check = cgi_bikes_get_option( 'instagram_link' );
	if ( $instagram_check != '' ) {
		$instagram = '<a href="'. $instagram_check .'" style="text-decoration: none;color: '. $brand_color .';"><img src="' . get_bloginfo( 'template_url' ). '/images/newsletter/ico-instagram-blk.jpg"/></a>';
	}

	$twitter_share = get_bloginfo( 'template_url' ) . '/images/newsletter/twitter-inverse.png';
	$facebook_share = get_bloginfo( 'template_url' ) . '/images/newsletter/facebook-inverse.png';


	// Setup ad codes
	$ad_code_newsletter_name = str_replace(' ', '-', $newsletter_title) . '-Newsletter';

	//new calls
	/*$leaderboard_ad_link = "%%=Concat('http://pubads.g.doubleclick.net/gampad/jump?co=1&iu=/8221/" . $ad_code_newsletter_name . "&sz=615x76&c=',@rand1)=%%";
	$threeby250_ad1_link = "%%=Concat('http://pubads.g.doubleclick.net/gampad/jump?co=1&iu=/8221/" . $ad_code_newsletter_name . "&sz=300x250&tile=1&c=',@rand2)=%%";
	$threeby250_ad2_link = "%%=Concat('http://pubads.g.doubleclick.net/gampad/jump?co=1&iu=/8221/" . $ad_code_newsletter_name . "&sz=300x250&tile=2&c=',@rand3)=%%";

	$leaderboard_ad = '<a href="' . $leaderboard_ad_link .'"><img src="http://pubads.g.doubleclick.net/gampad/ad?co=1&iu=/8221/'.$ad_code_newsletter_name.'&sz=615x76&c=%%=v(@rand1)=%%" width="615" height="76" /></a>';
	$threeby250_ad = '<a href="' . $threeby250_ad1_link .'"><img src="http://pubads.g.doubleclick.net/gampad/ad?co=1&iu=/8221/'.$ad_code_newsletter_name.'&sz=300x250&tile=1&c=%%=v(@rand2)=%%" width="300" height="250" /></a>';
	$threeby250_ad2 = '<a href="' . $threeby250_ad2_link .'"><img src="http://pubads.g.doubleclick.net/gampad/ad?co=1&iu=/8221/'.$ad_code_newsletter_name.'&sz=300x250&tile=2&c=%%=v(@rand3)=%%" width="300" height="250" /></a>';*/

	$threeby100_ad1 = '';
	$threeby100_ad2 = '';

	//new calls
	$leaderboard_ad = '<table border="0" cellpadding="0" cellspacing="0"  align="center" style="margin-left:auto; margin-right:auto;max-width:620px;"><tr><td colspan="2"><a href="http://li.velonews.com/click?s=201389&layout=marquee&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.velonews.com/imp?s=201389&layout=marquee&e=%%emailaddr%%&p=%%jobid%%" border="0" style="display: block; width:100%; height:auto;" width="620" /></a></td></tr><tr style="display:block; height:1px; line-height:1px;"><td><img src="http://li.velonews.com/imp?s=201390&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /></td><td><img src="http://li.velonews.com/imp?s=201391&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /></td></tr><tr><td align="left"><a href="http://li.velonews.com/click?s=201392&sz=116x15&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.velonews.com/imp?s=201392&sz=116x15&e=%%emailaddr%%&p=%%jobid%%" border="0"/></a></td><td align="right"><a href="http://li.velonews.com/click?s=201393&sz=69x15&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.velonews.com/imp?s=201393&sz=69x15&e=%%emailaddr%%&p=%%jobid%%" border="0"/></a></td></tr></table>';
	$threeby250_ad = '<table border="0" cellpadding="0" cellspacing="0" align="center"><tr><td colspan="2"><a style="display: block; width: 300px; height: 250px;" href="http://li.velonews.com/click?s=201383&sz=300x250&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.velonews.com/imp?s=201383&sz=300x250&e=%%emailaddr%%&p=%%jobid%%" border="0" width="300" height="250"/></a></td></tr><tr style="display:block; height:1px; line-height:1px;"><td><img src="http://li.velonews.com/imp?s=201384&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /></td><td><img src="http://li.velonews.com/imp?s=201385&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /></td></tr><tr><td align="left"><a href="http://li.velonews.com/click?s=201117&sz=116x15&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.velonews.com/imp?s=201117&sz=116x15&e=%%emailaddr%%&p=%%jobid%%" border="0"/></a></td><td align="right"><a href="http://li.velonews.com/click?s=201118&sz=69x15&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.velonews.com/imp?s=201118&sz=69x15&e=%%emailaddr%%&p=%%jobid%%" border="0"/></a></td></tr></table>';
	$threeby250_ad2 = '<table border="0" cellpadding="0" cellspacing="0" align="center"><tr><td colspan="2"><a style="display: block; width: 300px; height: 250px;" href="http://li.velonews.com/click?s=201386&sz=300x250&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.velonews.com/imp?s=201386&sz=300x250&e=%%emailaddr%%&p=%%jobid%%" border="0" width="300" height="250"/></a></td></tr><tr style="display:block; height:1px; line-height:1px;"><td><img src="http://li.velonews.com/imp?s=201387&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /></td><td><img src="http://li.velonews.com/imp?s=201388&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /></td></tr><tr><td align="left"><a href="http://li.velonews.com/click?s=201117&sz=116x15&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.velonews.com/imp?s=201117&sz=116x15&e=%%emailaddr%%&p=%%jobid%%" border="0"/></a></td><td align="right"><a href="http://li.velonews.com/click?s=201118&sz=69x15&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.velonews.com/imp?s=201118&sz=69x15&e=%%emailaddr%%&p=%%jobid%%" border="0"/></a></td></tr></table>';
	$leaderboard_ad_bottom = '<table border="0" cellpadding="0" cellspacing="0"  align="center" style="margin-left:auto; margin-right:auto;max-width:620px;"><tr><td colspan="2"><a href="http://li.velonews.com/click?s=201394&layout=marquee&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.velonews.com/imp?s=201394&layout=marquee&e=%%emailaddr%%&p=%%jobid%%" border="0" style="display: block; width:100%; height:auto;" width="620" /></a></td></tr><tr style="display:block; height:1px; line-height:1px;"><td><img src="http://li.velonews.com/imp?s=201395&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /></td><td><img src="http://li.velonews.com/imp?s=201396&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /></td></tr><tr><td align="left"><a href="http://li.velonews.com/click?s=201397&sz=116x15&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.velonews.com/imp?s=201397&sz=116x15&e=%%emailaddr%%&p=%%jobid%%" border="0"/></a></td><td align="right"><a href="http://li.velonews.com/click?s=201398&sz=69x15&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.velonews.com/imp?s=201398&sz=69x15&e=%%emailaddr%%&p=%%jobid%%" border="0"/></a></td></tr></table>';
	$native_ad = '<table align="center" border="0" cellpadding="0" cellspacing="0" id="li-container" style="width:600px; margin:0 auto; font-size:0; line-height:0; border-collapse: collapse; background-color:#FFFFFF;"> <tr> <td class="adcontent" style="overflow:hidden; width:50%; padding:5px;"> <img src="https://s3-eu-west-1.amazonaws.com/avari-production-content-blocks-images/sponsored_content.png" width="217" height="22" border="0" style="display:inline-block;"> </td> <td class="adchoices" style="overflow:hidden; width:50%; vertical-align:bottom; text-align:right; padding-bottom:5px; padding-right:5px;"> <a href="http://li.velonews.com/click?s=201118&sz=69x15&li={LIST_ID}&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.velonews.com/imp?s=201118&sz=69x15&li={LIST_ID}&e=%%emailaddr%%&p=%%jobid%%" border="0"/></a> </td> </tr> <tr> <td colspan="2" align="center" width="100%" style="width:100%; text-align:center; font-size:0; line-height:0; vertical-align: top;"> <!--[if (gte mso 9)|(IE)]> <table width="600" align="center" cellpadding="0" cellspacing="0" border="0"> <tr> <td> <![endif]--> <!--[if (gte mso 9)|(IE)]> <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"> <tr><td width="150" valign="top"> <![endif]--> <div style="width:149px; display:inline-block; text-align:center; border:0; vertical-align:top; margin-bottom: 10px;" class="fullw"> <a href="http://li.velonews.com/click?s=249837&layout=recommendation_widget&li={LIST_ID}&e=%%emailaddr%%&p=%%jobid%%" target="_blank"><img src="http://li.velonews.com/imp?s=249837&layout=recommendation_widget&li={LIST_ID}&e=%%emailaddr%%&p=%%jobid%%" width="142" class="orgw"></a> <img src="http://li.velonews.com/imp?s=249838&sz=1x1&li={LIST_ID}&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /><img src="http://li.velonews.com/imp?s=249839&sz=1x1&li={LIST_ID}&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /> </div> <!--[if (gte mso 9)|(IE)]> </td><td width="150" align="center" valign="top"> <![endif]--> <div style="width:149px; display:inline-block; text-align:center; border:0; vertical-align:top; margin-bottom: 10px;" class="fullw"> <a href="http://li.velonews.com/click?s=249840&layout=recommendation_widget&li={LIST_ID}&e=%%emailaddr%%&p=%%jobid%%" target="_blank"><img src="http://li.velonews.com/imp?s=249840&layout=recommendation_widget&li={LIST_ID}&e=%%emailaddr%%&p=%%jobid%%" width="142" class="orgw"></a> <img src="http://li.velonews.com/imp?s=249841&sz=1x1&li={LIST_ID}&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /><img src="http://li.velonews.com/imp?s=249842&sz=1x1&li={LIST_ID}&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /> </div> <!--[if (gte mso 9)|(IE)]> </td><td width="150" align="center" valign="top"> <![endif]--> <div style="width:149px; display:inline-block; text-align:center; border:0; vertical-align:top; margin-bottom: 10px;" class="fullw"> <a href="http://li.velonews.com/click?s=249843&layout=recommendation_widget&li={LIST_ID}&e=%%emailaddr%%&p=%%jobid%%" target="_blank"><img src="http://li.velonews.com/imp?s=249843&layout=recommendation_widget&li={LIST_ID}&e=%%emailaddr%%&p=%%jobid%%" width="142" class="orgw"></a> <img src="http://li.velonews.com/imp?s=249844&sz=1x1&li={LIST_ID}&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /><img src="http://li.velonews.com/imp?s=249845&sz=1x1&li={LIST_ID}&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /> </div> <!--[if (gte mso 9)|(IE)]> </td><td width="150" valign="top"> <![endif]--> <div style="width:149px; display:inline-block; text-align:center; border:0; vertical-align:top; margin-bottom: 10px;" class="fullw"> <a href="http://li.velonews.com/click?s=249846&layout=recommendation_widget&li={LIST_ID}&e=%%emailaddr%%&p=%%jobid%%" target="_blank"><img src="http://li.velonews.com/imp?s=249846&layout=recommendation_widget&li={LIST_ID}&e=%%emailaddr%%&p=%%jobid%%" width="142" class="orgw"></a> <img src="http://li.velonews.com/imp?s=249847&sz=1x1&li={LIST_ID}&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /><img src="http://li.velonews.com/imp?s=249848&sz=1x1&li={LIST_ID}&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /> </div> <!--[if (gte mso 9)|(IE)]> </td></tr> </table> <![endif]--> <!--[if (gte mso 9)|(IE)]> </td> </tr> </table> <![endif]--> </td> </tr> </table>';
	$safe_rtb = '<table cellpadding="0" cellspacing="0" border="0" width="40" height="6"><tbody><tr><td><img src="http://li.velonews.com/imp?s=124521700&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.velonews.com/imp?s=124521701&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.velonews.com/imp?s=124521702&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.velonews.com/imp?s=124521703&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.velonews.com/imp?s=124521704&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.velonews.com/imp?s=124521705&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.velonews.com/imp?s=124521706&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.velonews.com/imp?s=124521707&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.velonews.com/imp?s=124521708&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.velonews.com/imp?s=124521709&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.velonews.com/imp?s=124521710&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.velonews.com/imp?s=124521711&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.velonews.com/imp?s=124521712&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.velonews.com/imp?s=124521713&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.velonews.com/imp?s=124521714&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.velonews.com/imp?s=124521715&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.velonews.com/imp?s=124521716&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.velonews.com/imp?s=124521717&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.velonews.com/imp?s=124521718&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.velonews.com/imp?s=124521719&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td></tr></tbody></table>';


	// Post ID's to keep from duplicates showing up
	$post_id_array = array();

	/* this array requires 5 post ids, a query will run for
	each to populate the newsletter */
	$newsletter_posts = '';
	if( cgi_bikes_get_option( 'newsletter_posts_IDs' ) ) {
		$newsletter_posts = explode( ',' , cgi_bikes_get_option( 'newsletter_posts_IDs' ) );
		// print_r( $newsletter_posts);
	}

	$sticky = get_option( 'sticky_posts' );

	// Recent stories for sidebar
	$args = array(
		'post_type'			=> 'post',
		'post__not_in'		=> $sticky,
		'posts_per_page'	=> 12,
	);

	$newsletter_latest_posts = new WP_Query( $args );


	$count = 0;
	$latest_news = '<table cellspacing="0" border="0" cellpadding="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">';

		while( $newsletter_latest_posts->have_posts() ) : $newsletter_latest_posts->the_post();
			if( !in_array( get_the_ID(), $newsletter_posts ) && $count < 5 ) {
				$post_id_array[] = get_the_ID();
				$title = get_the_title();
				$link = wp_get_shortlink();

				$latest_news .= '<tr>
					<td height="20" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; padding-top: 5px;">
						<img src="' . get_bloginfo( 'template_url' ) . '/images/newsletter/bullet.gif" alt="" style="border: 0;" width="15" height="15" />
					</td>
					<td class="list" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; padding-top: 5px;">
						<a href = "'. $link .'" style="color: '. $brand_color .'; font-family: Arial; font-size: 13px; text-decoration: none;">' . $title . '</a>
					</td>
				</tr>';
				$count++;
			}
		endwhile;

	$latest_news .= "</table>";

	wp_reset_postdata();


	// Get 5 Stories in order which gives editors control over the main stories in Newsletter
	$stories = '';
	for( $count = 0; $count < 6; $count++ ) {
		$args = array(
			'post_type'	=> 'any',
			'p' => $newsletter_posts[$count]
		);
		$the_stories = new WP_Query($args);

		if( $the_stories->have_posts() ) :
			while( $the_stories->have_posts() ) : $the_stories->the_post();
				if( !in_array( get_the_ID(), $post_id_array ) ) {
					$post_id_array[] = get_the_ID();
					$title_link = '<a href="'. wp_get_shortlink() .'" title="Read full story" style="font-family: Arial, sans-serif; font-size:21px; line-height:24px; color:#333; margin:0; font-weight:bold; text-decoration:none;">' . get_the_title() . '</a>';
					$excerpt = get_the_excerpt();

					if( $count == 0 ) {
						$image_array = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'newsletter-splash' );
						$image_link = '<a href="'. wp_get_shortlink() .'" title="Read full story" style="text-decoration: none; color: '.$brand_color.';"><img src="' . $image_array[0] . '" class="max-size" width="620" height="400" /></a>';

									$stories .= '
										<tr>
											<td width="100%" class="text-center" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">' . $image_link . '</td>
										</tr>
										<tr>
											<td width="100%" bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
												<table width="100%" align="center" cellpadding="15" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
													<tr>
														<td width="100%" style="font-family:Arial, sans-serif;font-size:15px;line-height:20px;color:#555; border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
															' . $title_link . '
															<br>
															<span>' . $excerpt . '</span>
															<br /><br />
															<a href="'.wp_get_shortlink().'" class="button" style="color:'.$brand_color.';font-family: Arial, sans-serif;font-size:13px;line-height:19px;">Find out more</a>
														</td>
													</tr>
												</table>
												<table width="180" align="right" cellpadding="15" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
													<tr>
														<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><span>Share:</span></td>
														<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><atarget="_blank" href="http://twitter.com/intent/tweet?original_referer=&text='.get_the_title().'&url='.wp_get_shortlink().'"><img src="'.$twitter_share.'" width="32" height="32"></a></td>
														<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><a href="https://www.facebook.com/sharer/sharer.php?u='.wp_get_shortlink().'" target="_blank"><img src="'.$facebook_share.'" width="32" height="32"></a></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td width="100%" height="20" bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br /></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>';
					} elseif( $count < 5 ) {
						if ($count == 1) {
							$image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'newsletter-image');
							$image_link = '<a href="'. wp_get_shortlink() .'" title="Read full story" style="text-decoration: none; color: '.$brand_color.';"><img src="' . $image_array[0] . '" class="max-size" width="300" height="200" /></a>';

							$stories .= '
								<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="font-family:Arial, sans-serif;font-size:15px;line-height:20px;color:#555;">
									<tr>
										<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
											<table width="620" cellpadding="0" cellspacing="0" border="0" align="center" class="content_wrap" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
												<tr>
													<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
														<!-- content 2 -->
														<table class="full_width" width="300" align="left" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
															<tr>
																<td class="text-center" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
																	'.$image_link.'
																</td>
															</tr>
															<tr>
																<td bgcolor="#eaeaea" width="100%" valign="top" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
																	<table width="300" cellpadding="15" cellspacing="0" border="0" class="full_width" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
																		<tr>
																			<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; font-family:Arial, sans-serif;font-size:15px;line-height:20px;color:#555;">
																				'.$title_link.'
																				<br />
																				<span>'.$excerpt.'</span>
																				<br /><br />
																				<a href="'.wp_get_shortlink().'" class="button" style="color:'.$brand_color.';font-family: Arial, sans-serif;font-size:13px;line-height:19px;">Find out more</a>
																			</td>
																		</tr>
																	</table>
																	<table width="200" align="right" cellpadding="15" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
																		<tr>
																			<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><span>Share:</span></td>
																			<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><a target="_blank" href="http://twitter.com/intent/tweet?original_referer=&text='.get_the_title().'&url='.wp_get_shortlink().'"><img src="'.$twitter_share.'" width="32" height="32"></a></td>
																			<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><a href="https://www.facebook.com/sharer/sharer.php?u='.wp_get_shortlink().'" target="_blank"><img src="'.$facebook_share.'" width="32" height="32"></a></td>
																		</tr>
																	</table>
																</td>
															</tr>
															<tr>
																<td width="100%" height="20" bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br /></td>
															</tr>
														</table>
														<!-- end content 2 -->
														<!-- ad & bullets -->
														<table class="full_width" width="300" align="right" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
															<tr>
																<td width="100%" class="text-center" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
																	'. $threeby250_ad .'
																</td>
															</tr>
															<tr>
																<td bgcolor="#ffffff" width="100%" valign="top" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
																	<table width="100%" cellpadding="15" cellspacing="0" border="0" class="full_width" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
																		<tr>
																			<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; font-family:Arial, sans-serif;font-size:15px;line-height:20px;color:#555;">
																				<b>ALSO IN THIS ISSUE:</b><br />
																				'. $latest_news .'
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
														<!-- end ad & bullets -->
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
											<table width="100%" cellpadding="0" cellspacing="0" border="0" class="hide" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
												<tr>
													<td width="100%" height="10" bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br /></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							';
						} elseif( $count == 2 ) {
							$image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'newsletter-image');
							$image_link = '<a href="'. wp_get_shortlink() .'" title="Read full story" style="text-decoration: none; color: '.$brand_color.';"><img src="' . $image_array[0] . '" class="max-size" width="300" height="200" /></a>';

							$stories .= '
								<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="font-family:Arial, sans-serif;font-size:15px;line-height:20px;color:#555;">
									<tr>
										<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
											<table width="620" cellpadding="0" cellspacing="0" border="0" align="center" class="content_wrap" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
												<tr>
													<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
														<!--Content 3 & ads-->
														<!-- content 3 -->
														<table class="full_width" width="300" align="left" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
															<tr>
																<td class="text-center" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
																	'. $image_link .'
																</td>
															</tr>
															<tr>
																<td bgcolor="#eaeaea" width="100%" valign="top" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
																	<table width="300" cellpadding="15" cellspacing="0" border="0" class="full_width" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
																		<tr>
																			<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; font-family:Arial, sans-serif;font-size:15px;line-height:20px;color:#555;">
																				'.$title_link.'
																				<br />
																				<span>'.$excerpt.'</span>
																				<br /><br />
																				<a href="'.wp_get_shortlink().'" class="button" style="color:'.$brand_color.';font-family: Arial, sans-serif;font-size:13px;line-height:19px;">Find out more</a>
																			</td>
																		</tr>
																	</table>
																	<table width="200" align="right" cellpadding="15" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
																		<tr>
																			<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><span>Share:</span></td>
																			<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><a target="_blank" href="http://twitter.com/intent/tweet?original_referer=&text='.get_the_title().'&url='.wp_get_shortlink().'"><img src="'.$twitter_share.'" width="32" height="32"></a></td>
																			<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><a href="https://www.facebook.com/sharer/sharer.php?u='.wp_get_shortlink().'" target="_blank"><img src="'.$facebook_share.'" width="32" height="32"></a></td>
																		</tr>
																	</table>
																</td>
															</tr>
															<tr>
																<td width="100%" height="20" bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br /></td>
															</tr>
														</table>
														<!-- end content 3 -->
														<!-- ads -->
														<table class="full_width" width="300" align="right" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
															<tr>
																<td width="100%" class="text-center" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
																	'. $threeby250_ad2 .'
																</td>
															</tr>
															<tr>
																<td width="100%" height="20" bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br /></td>
															</tr>
														</table>
														<!-- end ads -->
														<!--end Content 3 & ads -->
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							';
								} elseif( $count == 3 || $count == 4 ) {
									$image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'newsletter-image');
									$image_link = '<a href="'. wp_get_shortlink() .'" title="Read full story" style="text-decoration: none; color: '.$brand_color.';"><img src="' . $image_array[0] . '" class="max-size" width="300" height="200" /></a>';
									if( $count == 3 ) {
										$stories .= '
											<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="font-family:Arial, sans-serif;font-size:15px;line-height:20px;color:#555;">
												<tr>
													<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
														<table width="620" cellpadding="0" cellspacing="0" border="0" align="center" class="content_wrap" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
															<tr>
																<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
																	<!--Content 4 & 5-->
																	<!-- content 4 -->
																	<table class="full_width" width="300" align="left" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
																		<tr>
																			<td width="100%" class="text-center" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
																				'. $image_link .'
																			</td>
																		</tr>
																		<tr>
																			<td bgcolor="#eaeaea" width="100%" valign="top" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
																				<table width="300" cellpadding="15" cellspacing="0" border="0" class="full_width" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
																					<tr>
																						<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; font-family:Arial, sans-serif;font-size:15px;line-height:20px;color:#555;">
																							'. $title_link .'
																							<br />
																							<span>'. $excerpt .'</span>
																							<br /><br />
																							<a href="'.wp_get_shortlink().'" class="button" style="color:'. $brand_color .';font-family: Arial, sans-serif;font-size:13px;line-height:19px;">Find out more</a>
																						</td>
																					</tr>
																				</table>
																				<table width="200" align="right" cellpadding="15" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
																					<tr>
																						<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><span>Share:</span></td>
																						<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><a target="_blank" href="http://twitter.com/intent/tweet?original_referer=&text='.get_the_title().'&url='.wp_get_shortlink().'"><img src="'. $twitter_share .'" width="32" height="32"></a></td>
																						<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><a href="https://www.facebook.com/sharer/sharer.php?u='.wp_get_shortlink().'" target="_blank"><img src="'. $facebook_share .'" width="32" height="32"></a></td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="100%" height="20" bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br /></td>
																		</tr>
																	</table>
																	<!-- end content 4 -->
																';

									} elseif( $count == 4 ) {
										$stories .= '
											<!-- content 5 -->
											<table class="full_width" width="300" align="right" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
												<tr>
													<td width="100%" class="text-center" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
														'.$image_link.'
													</td>
												</tr>
												<tr>
													<td bgcolor="#eaeaea" width="100%" valign="top" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
														<table width="300" cellpadding="15" cellspacing="0" border="0" class="full_width" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
															<tr>
																<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; font-family:Arial, sans-serif;font-size:15px;line-height:20px;color:#555;">
																	'. $title_link .'
																	<br />
																	<span>'. $excerpt .'</span>
																	<br /><br />
																	<a href="'.wp_get_shortlink().'" class="button" style="color:'. $brand_color .';font-family: Arial, sans-serif;font-size:13px;line-height:19px;">Find out more</a>
																</td>
															</tr>
														</table>
														<table width="200" align="right" cellpadding="15" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
															<tr>
																<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><span>Share:</span></td>
																<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><a target="_blank" href="http://twitter.com/intent/tweet?original_referer=&text='.get_the_title().'&url='.wp_get_shortlink().'"><img src="'. $twitter_share .'" width="32" height="32"></a></td>
																<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><a href="https://www.facebook.com/sharer/sharer.php?u='.wp_get_shortlink().'" target="_blank"><img src="'. $facebook_share .'" width="32" height="32"></a></td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
											<!-- end content 5 -->
											<!--end Content 4 & 5-->
										</td>
									</tr>
									<tr>
										<td width="100%" height="20" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br /></td>
									</tr>
								</table>
              ';
							}
						}
					}
				}
			endwhile;
		else:
			$stories .= '<tr><td>Sorry, no posts match that category</td></tr>';
		endif;
	}
	$stories .= '</table>';
	wp_reset_postdata();

?><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<title><?php echo $newsletter_title; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<style>
			body {
				width:100%!important;
				-webkit-text-size-adjust:100%;
				-ms-text-size-adjust:100%;
				margin:0;
				padding:0;
			}
			.ExternalClass {
				width: 100%;
			}
			span {
				font-family:Arial, sans-serif;
				font-size:16px;
				line-height:20px;
				color:#555;
			}
			table, td {
				border-collapse:collapse;
				mso-table-lspace:0pt;
				mso-table-rspace:0pt;
			}
			img {
				border:0;
				line-height:100%;
				outline:none;
				text-decoration:none;
			}
			img.max-size {
				height:auto!important;
			}
			img#header-logo {
				margin-right: 0px;
			}
			img#sponsor-logo {
				padding: 2px 0px;
				margin: 2px 0px;
			}
			@media only screen and (max-width: 480px) {
				body,table,td,p,a,li,blockquote{
					-webkit-text-size-adjust:none !important;
				}
			}
			@media only screen and (max-width: 480px) {
				table[class=content_wrap] {
					width: 94%!important;
				}
			}
			@media only screen and (max-width: 480px) {
				table[class=full_width] {
					width: 100%!important;
				}
			}
			@media only screen and (max-width: 480px) {
				img[class=max-size] {
					width: 100%!important;
					height:auto;
				}
			}
			@media only screen and (max-width: 480px) {
				img[class=hide] {
					display: none!important;
				}
			}
			@media only screen and (max-width: 480px) {
				td[class=hide] {
					display: none!important;
				}
			}
			@media only screen and (max-width: 480px) {
				table[class=show] {
					display:inline!important;
				}
			}
			@media only screen and (max-width: 480px) {
				img[class=show] {
					display:inline!important;
				}
			}
			@media only screen and (max-width: 480px) {
				td[class=text-center] {
					text-align: center!important;
				}
			}
			@media only screen and (max-width: 480px) {
				td[class=text-left] {
					text-align: left!important;
				}
			}
			@media only screen and (max-width: 480px) {
				a[class=button] {
					border-radius:2px;
					background-color:<?php echo $brand_color; ?>;
					color:#fff!important;
					padding: 5px;
					text-align:center;
					display:block;
					text-decoration: none;
					text-transform: uppercase;
					margin: 0 0 10px 0;
				}
			}
			@media only screen and (max-width: 599px) {
				table[id=li-container]{ width: 100% !important; }
				div[class=fullw]{ width: 50% !important; }
				img[class=orgw]{ width: 95% !important; }
				td[class=adcontent]{ padding: 5px 0 5px 10px !important; }
				td[class=adchoices]{ padding: 0 10px 5px 0 !important; }
			}
		</style>
	</head>

	<body bgcolor="#ffffff" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="-webkit-font-smoothing:antialiased;width:100% !important;background-color:#ffffff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;-webkit-text-size-adjust:none;">
		<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="font-family:Arial, sans-serif;font-size:15px;line-height:20px;color:#555;">
			<tr>
				<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
					<table width="620" cellpadding="0" cellspacing="0" border="0" align="center" class="content_wrap" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
						<tr>
							<td width="100%" bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
								<?php if( !isset($_GET['header']) ) { ?>
									<!--new header start -->
									<table width="620" cellpadding="0" cellspacing="0" border="0" align="center" class="content_wrap" style="border-collapse: collapse;">
										<tbody>
											<tr>
												<td width="100%" height="30" style="border-collapse: collapse;">
													<table class="full_width" width="620" align="left" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
														<tbody>
															<tr>
																<td width="206" align="center" class="text-center" style="border-collapse: collapse;">
																	<a href="<?php echo get_option('home'); ?>"><img id="header-logo" src="<?php echo $logo;?>" title="<?php echo get_bloginfo('name'); ?> Newsletter" alt="<?php echo get_bloginfo('name'); ?> Newsletter" border="0" /></a>

																	<?php if( isset( $newsletter_sponsor_image ) AND $newsletter_sponsor_image != '' ) {
																		if( isset( $newsletter_sponsor_link ) AND $newsletter_sponsor_link != '' ) {
																			echo "<a href='".$newsletter_sponsor_link."' target='_blank'>";
																		}
																		echo "<img id='sponsor-logo' src='". $newsletter_sponsor_image ."' width='175' height='40' border='0' />";
																		if( isset( $newsletter_sponsor_link ) AND $newsletter_sponsor_link != '' ) {
																			echo "</a>";
																		}
																	} ?>

																</td>

																<td width="207" align="center" style="border-collapse: collapse;">
																	<a href="<?php echo get_option( 'home' ); ?>" target="_blank"><img src="<?php echo $magazines; ?>"></a>
																</td>

																<td width="206" align="right" style="border-collapse: collapse;">
																	<?php echo $facebook; ?> <?php echo $twitter; ?> <?php echo $instagram; ?>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
									<!-- new header end -->
								<?php } ?>
							</td>
						</tr>
						<tr>
							<td style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
								<table width="100%" cellpadding="0" cellspacing="0" border="0" class="hide" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
									<tr>
										<td width="100%" height="10" bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br /></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
								<table width="100%" cellpadding="0" cellspacing="0" border="0" class="hide" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
									<tr>
										<!--ad-->
										<td bgcolor="#ffffff" width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
											<?php echo $leaderboard_ad; ?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<!-- spacer -->
						<tr>
							<td width="100%" height="10" bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br /></td>
						</tr>
						<!-- / header -->
						<?php echo $stories; ?>
						<tr>
							<td bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
								<table width="100%" cellpadding="0" cellspacing="0" border="0" class="hide" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
									<tr>
										<!--ad-->
										<td bgcolor="#ffffff" width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
											<?php echo $leaderboard_ad_bottom; ?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
								<table width="100%" cellpadding="0" cellspacing="0" border="0" class="hide" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
									<tr>
										<!--ad-->
										<td bgcolor="#ffffff" width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
											<?php echo $native_ad; ?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<?php echo $safe_rtb; ?>
	</body>
</html>
