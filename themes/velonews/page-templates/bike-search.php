<?php
	/* Template Name: Bike Search Page */

	get_header();
	global $wpdb;

	// query setup
	$page				= 0;
	$resultspage		= 1;
	$bike_search		= '';
	$query				= '';
	$bike_type_sql		= '';
	$bike_brand_sql		= '';
	$gender_sql			= '';
	$price_range_sql	= '';
	$bike_search_sql	= '';

	$bike_type			= array();
	$price_range		= array();
	$gender				= array();
	$bike_brand			= array();

	if( isset( $_REQUEST['resultspage'] ) ) {
		if( !isset( $_REQUEST['resultspage'] ) ) {
			$resultspage = 1;
		} else {
			$resultspage = $_REQUEST['resultspage'];
		}
		$bike_type		= $_REQUEST['type'];
		$price_range	= $_REQUEST['price_range'];
		$gender			= !empty( $_REQUEST['gender'] ) ? $_REQUEST['gender'] : array('all');
		$bike_brand		= $_REQUEST['brand'];
		$bike_search	= sanitize_text_field( $_REQUEST['product_search'] );
	}

?>

<section id="content" class="content template--product-search">
	<div class="container content__container">

		<header class="row">
			<div class="col-xs-12">
				<h1 class="content__title"><?php the_title(); ?></h1>
				<ol class="article__bread-crumb">
					<li><a href="<?php echo site_url() ?>" target="_self">Home</a></li>

					<?php
						if( !isset( $_REQUEST['resultspage'] ) ) {
							printf( ' &raquo; <li>%s</li>', get_the_title() );
						} else {
							printf( ' &raquo; <li><a href="%s">%s</a></li>', get_permalink(), get_the_title() );
						}

						if( isset( $_GET['type'] ) ) {

							if( 1 == count($_GET['type']) ) {
								printf(
									' &raquo; <li><a href="%s?resultspage=1&type[]=%s&price_range[]=all&gender[]=all&brand[]=all&product_search=">%s</a></li>',
									get_permalink(),
									$_GET['type'][0],
									ucwords($_GET['type'][0]) . ('all' === $_GET['type'][0] ? ' Types' : ''),
									$_GET['type'][0]
								);
							}

							echo ' &raquo; <li>Search Results</li>';
						}
					?>

				</ol>
			</div>
		</header>


		<section class="row content__flex">

			<section class="content__section content__flex_position_first product-search">
				<?php
					/**
					 * bike model text search
					 */
				?>
				<div class="product-search__wrap" id="product-search-form">

					<h3 class="product-search__heading product-search__heading_name_brand">VeloNews Bike Finder</h3>

					<form action="<?php echo get_permalink(); ?>" method="GET" name="text_product_search" id="product-search-text" class="product-search__form product-search__form_name_text">
						<input type="hidden" value="1" name="resultspage">
						<input type="hidden" value="all" name="type[]">
						<input type="hidden" value="all" name="price_range[]">
						<input type="hidden" value="all" name="gender[]">
						<input type="hidden" value="all" name="brand[]">

						<fieldset class="product-search__form__fieldset product-search__form__fieldset_name_search">
							<div class="product-search__form__option">
								<label for="product-search-search" class="screen-reader-text">Search Bikes</label>
								<input type="text" name="product_search" id="product-search-search" value="<?php echo $bike_search; ?>" placeholder="Search brands" class="product-search__form__input">
								<button class="btn btn--red"><span class="fa fa-search"></span></button>
							</div>
							<a href="#" target="_self" class="product-search__form-toggle"><span class="fa fa-times"></span></a>
						</fieldset>
					</form>


					<h3 class="product-search__heading product-search__heading_name_filter">Search By Filter</h3>

					<?php
						/**
						 * bike multi-option search
						 */
					?>
					<form action="<?php echo get_permalink(); ?>" method="GET" name="multifacet_product_search" id="product-search-multifacet" class="product-search__form product-search__form_name_multifacet">
						<input type="hidden" value="1" name="resultspage">

						<?php
							/**
							 * bike type
							 * all-around, aero-road, endurance, gravel, time-trial, cyclocross, mountain
							 * these types will appear as bikes are entered in the admin
							 */
							$query = 'SELECT DISTINCT r.meta_value FROM wp_postmeta r, wp_posts s WHERE r.meta_key = "_vn_bike_review_bike_type" AND s.ID = r.post_id AND s.post_status = "publish" order by r.meta_value';
							$entries_type = $wpdb->get_results( $query );
						?>

						<fieldset class="product-search__form__fieldset">

							<legend class="product-search__form__legend">
								<a href="#product-search-type-filters" target="_self" title="Toggle Type Filters" class="product-search__filter-toggle">
									TYPE <span class="fa fa-caret-down"></span>
								</a>
							</legend>

							<div id="product-search-type-filters" class="product-search__form__group">
								<div class="product-search__form__option">
									<input <?php echo ( !isset( $bike_type[0] ) || $bike_type[0] == 'all' ) ? 'checked' : ''; ?> type="checkbox" name="type[]" id="type-all" value="all" class="product-search__form__checkbox">
									<label for="type-all" class="product-search__form__label">All</label>
								</div>

								<?php
									foreach( $entries_type as $entry_type ) {
										$type_pieces	= explode( '::', $entry_type->meta_value );
										$type_key		= $type_pieces[0];
										$type_name		= $type_pieces[1];
										$checked		= in_array( $type_key, $bike_type ) ? 'checked' : '';

										echo '<div class="product-search__form__option">
											<input '. $checked .' type="checkbox" name="type[]" id="type-'. $type_key .'" value="'. $type_key .'" class="product-search__form__checkbox">
											<label for="type-'. $type_key .'" class="product-search__form__label">'. $type_name .'</label>
										</div>';
									}
								?>

							</div>
						</fieldset>


						<?php
							/**
							 * price
							 * under $3,000 | $3,000 - $6,999 | $7,000+
							 */
						?>
						<fieldset class="product-search__form__fieldset">

							<legend class="product-search__form__legend">
								<a href="#product-search-price-filters" target="_self" title="Toggle Price Filters" class="product-search__filter-toggle">
									PRICE <span class="fa fa-caret-down"></span>
								</a>
							</legend>

							<div id="product-search-price-filters" class="product-search__form__group">
								<div class="product-search__form__option">
									<input <?php echo ( !isset( $price_range[0] ) || $price_range[0] == 'all' ) ? 'checked' : ''; ?> type="checkbox" name="price_range[]" id="price-all" value="all" class="product-search__form__checkbox">
									<label for="price-all" class="product-search__form__label">All</label>
								</div>

								<?php
									$bike_price_under_3000 = in_array( "under_3000", $price_range ) ? 'checked' : '';
									$bike_price_three_to_seven_thousand = in_array( "three_to_seven_thousand", $price_range ) ? 'checked' : '';
									$bike_price_over_7000 = in_array( "over_7000", $price_range ) ? 'checked' : '';
								?>

								<div class="product-search__form__option">
									<input <?php echo $bike_price_under_3000; ?> type="checkbox" name="price_range[]" id="price-under-3000" value="under_3000" class="product-search__form__checkbox">
									<label for="price-under-3000" class="product-search__form__label">Under $3,000</label>
								</div>
								<div class="product-search__form__option">
									<input <?php echo $bike_price_three_to_seven_thousand; ?> type="checkbox" name="price_range[]" id="price-3000-6999" value="three_to_seven_thousand" class="product-search__form__checkbox">
									<label for="price-3000-6999" class="product-search__form__label">$3,000 - $6,999</label>
								</div>
								<div class="product-search__form__option">
									<input <?php echo $bike_price_over_7000; ?> type="checkbox" name="price_range[]" id="price-7000-and-up" value="over_7000" class="product-search__form__checkbox">
									<label for="price-7000-and-up" class="product-search__form__label">$7,000 &amp; Up</label>
								</div>
							</div>
						</fieldset>


						<?php
							/**
							 * gender
							 */
						?>
						<fieldset class="product-search__form__fieldset">

							<legend class="product-search__form__legend">
								<a href="#product-search-brand-filters" target="_self" title="Toggle Gender Filters" class="product-search__filter-toggle">
									GENDER <span class="fa fa-caret-down"></span>
								</a>
							</legend>

							<div id="product-search-brand-filters" class="product-search__form__group">
								<div class="product-search__form__option">
									<input <?php echo ( !isset( $gender[0] ) || $gender[0] == 'all' ) ? 'checked' : ''; ?> type="checkbox" name="gender[]" id="gender-all" value="all" class="product-search__form__checkbox">
									<label for="gender-all" class="product-search__form__label">All</label>
								</div>

								<?php
									$men_checked	= in_array( 'm', $gender ) ? 'checked' : '';
									$women_checked	= in_array( 'f', $gender ) ? 'checked' : '';

									echo '<div class="product-search__form__option">
										<input '. $men_checked .' type="checkbox" name="gender[]" id="gender-men" value="m" class="product-search__form__checkbox">
										<label for="gender-men" class="product-search__form__label">Men</label>
									</div>
									<div class="product-search__form__option">
										<input '. $women_checked .' type="checkbox" name="gender[]" id="gender-women" value="f" class="product-search__form__checkbox">
										<label for="gender-women" class="product-search__form__label">Women</label>
									</div>';
								?>

							</div>
						</fieldset>


						<?php
							/**
							 * bike brand
							 * this list will populate as more bikes are added
							 */
							$query			= 'SELECT DISTINCT r.meta_value FROM wp_postmeta r, wp_posts s WHERE r.meta_key = "_vn_bike_review_bike_brand" AND s.ID = r.post_id AND s.post_status = "publish" AND s.post_date >= DATE_SUB(NOW(), INTERVAL 36 MONTH)  ORDER BY r.meta_value';
							$entries_brand	= $wpdb->get_results( $query );
						?>
						<fieldset class="product-search__form__fieldset">

							<legend class="product-search__form__legend">
								<a href="#product-search-brand-filters" target="_self" title="Toggle Brand Filters" class="product-search__filter-toggle">
									BRAND <span class="fa fa-caret-down"></span>
								</a>
							</legend>

							<div id="product-search-brand-filters" class="product-search__form__group">
								<div class="product-search__form__option">
									<input <?php echo ( !isset( $bike_brand[0] ) || $bike_brand[0] == 'all' ) ? 'checked' : ''; ?> type="checkbox" name="brand[]" id="brand-all" value="all" class="product-search__form__checkbox">
									<label for="brand-all" class="product-search__form__label">All</label>
								</div>

								<?php
									$brand_count = 1;

									foreach( $entries_brand as $entry_brand ) {
										$dashed_name	= str_replace( ' ', '-', $entry_brand->meta_value );
										$checked		= in_array( $dashed_name, $bike_brand ) ? 'checked' : '';

										/**
										 * show only 4 brands + "all", hide the rest under a "see all brands" toggle link
										 */
										// if( $brand_count == 5 ) {
										// 	echo '<div id="product-search__brand__hidden" class="collapse">';
										// }
										if( $brand_count > 4 ) {
											$option_hide_class = ' product-search__form__option__hidden';
										} else {
											$option_hide_class = '';
										}

										echo '<div class="product-search__form__option'. $option_hide_class .'">
											<input '. $checked .' type="checkbox" name="brand[]" id="brand-'. $dashed_name .'" value="'. $dashed_name .'" class="product-search__form__checkbox">
											<label for="brand-'. $dashed_name .'" class="product-search__form__label">'. $entry_brand->meta_value .'</label>
										</div>';

										$brand_count++;
									}

									if( $brand_count > 4 ) {
										// echo '</div>
										echo '<div class="product-search__more"><a href="javascript:;" class="show-hidden-toggle">See All Brands</a></div>';
									}
								?>
							</div>
						</fieldset>


						<fieldset class="product-search__form__fieldset product-search__form__fieldset_name_submit">
							<input type="hidden" value="" name="product_search">
							<input type="submit" value="Filter Results" class="btn btn--red">
						</fieldset>

					</form>
				</div>
			</section>


			<section class="content__section content__flex_position_second">

				<?php
					/**
					 * a search was made
					 */
					if( isset( $_REQUEST['resultspage'] ) ) {

						/**
						 * set up the main query
						 * multiply score by 100 just in case a bike gets a perfect 100 score
						 * only search for bikes that were added within the past 36 months
						 */
						$query = 'SELECT r.ID, t.meta_value, t.meta_value * 100 AS Overall FROM wp_posts r, wp_postmeta t WHERE r.post_type = "bike-reviews" AND r.post_status = "publish" AND r.post_date >= DATE_SUB(NOW(), INTERVAL 36 MONTH) ';


						/**
						 * type filter was modified
						 * get post_id's for as many types selected
						 */
						if( $bike_type[0] != 'all' ) {

							$bike_type_or		= '';
							$bike_type_count	= count( $bike_type );

							for( $i = 0; $i < $bike_type_count; $i++ ) {
								$bike_type_or .= 'meta_value LIKE "' . $bike_type[$i] . '%"';
								if( $i != ( $bike_type_count -1 ) ) {
									$bike_type_or .= ' OR ';
								}
							}

							$bike_type_sql = 'SELECT post_id FROM wp_postmeta WHERE meta_key = "_vn_bike_review_bike_type" AND ('. $bike_type_or .')';
						}


						/**
						 * price filter was modified
						 * get post_id's for as many price ranges selected
						 */
						if( $price_range[0] != 'all' ) {

							$price_range_or		= '';
							$price_range_count	= count( $price_range );

							for( $i = 0; $i < $price_range_count; $i++ ) {
								if( "under_3000" == $price_range[$i] ) {
									$price_range_compare = 'meta_value <= 3000';
								}
								if( "three_to_seven_thousand" == $price_range[$i] ) {
									$price_range_compare = 'meta_value > 3001 AND meta_value < 7000';
								}
								if( "over_7000" == $price_range[$i] ) {
									$price_range_compare = 'meta_value >= 7000';
								}
								$price_range_or .= $price_range_compare;
								if( $i != ( $price_range_count -1 ) ) {
									$price_range_or .= ' OR ';
								}
							}

							$price_range_sql = 'SELECT post_id FROM wp_postmeta WHERE meta_key ="_vn_bike_review_bike_msrp" AND ('. $price_range_or .')';
						}


						/**
						 * gender filter was modified
						 * get post_id's for as many gender types selected
						 */
						if( $gender[0] != 'all' ) {

							$gender_value = sprintf( "'%s'", $gender[0] );

							// if both male and female are chosen
							if( in_array( 'm', $gender ) && in_array( 'f', $gender ) ) {
								$gender_value = "'m', 'f', 'u'";
							}

							/**
							 * if only male chosen, include unisex
							 * if only female chosen, only find bikes marked for women
							 * if both are chosen, find bikes marked for men, women, and unisex
							 */
							if( in_array( 'm', $gender ) && !in_array( 'f', $gender ) ) {
								$gender_sql = "SELECT post_id FROM wp_postmeta WHERE ( meta_key = '_vn_bike_review_apparel_gender' AND meta_value IN ({$gender_value}, 'u') )";
							} elseif( !in_array( 'm', $gender ) && in_array( 'f', $gender ) ) {
								$gender_sql = "SELECT post_id FROM wp_postmeta WHERE ( meta_key = '_vn_bike_review_apparel_gender' AND meta_value IN ({$gender_value}) )";
							} else {
								$gender_sql = "SELECT post_id FROM wp_postmeta WHERE ( meta_key = '_vn_bike_review_apparel_gender' AND meta_value IN ({$gender_value}) )";
							}
						}


						/**
						 * brand filter was modified
						 * get post_id's for as many brands selected
						 */
						if( $bike_brand[0] != 'all' ) {

							$bike_brand_or		= '';
							$bike_brand_count	= count( $bike_brand );

							for( $i = 0; $i < $bike_brand_count; $i++ ) {
								$bike_brand_name = str_replace( '-', ' ', $bike_brand[$i] );

								$bike_brand_or .= 'meta_value like "%' . $bike_brand_name . '%"';
								if( $i != ( $bike_brand_count -1 ) ) {
									$bike_brand_or .= ' OR ';
								}
							}

							$bike_brand_sql = 'SELECT post_id FROM wp_postmeta WHERE meta_key = "_vn_bike_review_bike_brand" AND ('. $bike_brand_or .')';

						}


						/**
						 * user typed in bike brand into text input field
						 * get post_id's where brand matches
						 */
						if( $bike_search != '' ) {
							$bike_search_sql = 'SELECT post_id FROM wp_postmeta WHERE meta_key = "_vn_bike_review_bike_brand" AND meta_value LIKE "%'. $bike_search .'%"';
						}


						/**
						 * build onto main query based on selections the user made
						 * logic is set up to be top-down to cover all search scenarios
						 */
						if( $bike_type_sql != "" ) {
							$query .= ' AND r.ID IN ('. $bike_type_sql;

							if( $bike_brand_sql != "" ) {
								$query .= ' AND post_id IN ( '. $bike_brand_sql .')';
							}
							if( $price_range_sql != "" ) {
								$query .= ' AND post_id IN ( '. $price_range_sql .')';
							}
							if( $gender_sql != "" ) {
								$query .= ' AND post_id IN ( '. $gender_sql .')';
							}
							$query .= ')';

						} elseif( $bike_brand_sql != "" ) {
							$query .= ' AND ID IN ( '. $bike_brand_sql;

							if( $price_range_sql != "" ) {
								$query .= ' AND post_id IN ( '. $price_range_sql .')';
							}
							if( $gender_sql != "" ) {
								$query .= ' AND post_id IN ( '. $gender_sql .')';
							}
							$query .= ')';

						} elseif( $price_range_sql != "" ) {
							$query .= ' AND ID IN ( '. $price_range_sql .')';

							if( $gender_sql != "" ) {
								$query .= ' AND post_id IN ( '. $gender_sql .')';
							}
						} elseif( $gender_sql != "" ) {
							$query .= ' AND r.ID IN ( '. $gender_sql .')';

						} elseif( $bike_search_sql != "" ) {
							$query .= ' AND r.ID IN ('. $bike_search_sql .')';
						}


						$offset = 0;

						if( $resultspage > 1 ) {
							/**
							 * show 12 results per page
							 */
							$offset =  ( $resultspage - 1 ) * 12; // (page 2 - 1)*12 = offset of 12
						}

						$query_sans_offset = $query . ' AND t.post_id = r.ID AND t.meta_key = "_vn_bike_review_bike_overall" ORDER BY Overall DESC';
						$query .= ' AND t.post_id = r.ID AND t.meta_key = "_vn_bike_review_bike_overall" ORDER BY Overall DESC LIMIT 12 OFFSET '. $offset;

						/**
						 * to remember the url (history)
						 */
						$page_query = $type_url_string = $price_range_url_string = $gender_url_string = $brand_url_string = '';

						/**
						 * type
						 */
						if( $bike_type[0] != 'all' ) {
							foreach( $bike_type as $type ) {
								$page_query .= '&type[]='. $type;
								$type_url_string .= '&type[]='. $type;
							}
						} else {
							$page_query .= '&type[]=all';
							$type_url_string .= '&type[]=all';
						}

						/**
						 * price range
						 */
						if( $price_range[0] != 'all' ) {
							foreach( $price_range as $range ) {
								$page_query .= '&price_range[]='. $range;
								$price_range_url_string .= '&price_range[]='. $range;
							}
						} else {
							$page_query .= '&price_range[]=all';
							$price_range_url_string .= '&price_range[]=all';
						}

						/**
						 * gender
						 */
						if( $gender[0] != 'all' ) {
							foreach( $gender as $brand ) {
								$page_query .= '&gender[]='. $brand;
								$gender_url_string .= '&gender[]='. $brand;
							}
						} else {
							$page_query .= '&gender[]=all';
							$gender_url_string .= '&gender[]=all';
						}

						/**
						 * brand
						 */
						if( $bike_brand[0] != 'all' ) {
							foreach( $bike_brand as $brand ) {
								$page_query .= '&brand[]='. $brand;
								$brand_url_string .= '&brand[]='. $brand;
							}
						} else {
							$page_query .= '&brand[]=all';
							$brand_url_string .= '&brand[]=all';
						}

						/**
						 * brand search (text input)
						 */
						if( $bike_search ) {
							$page_query .= '&product_search='. $bike_search;
						} else {
							$page_query .= '&product_search=';
						}


						if( $query != '' ) {

							$adcount				= 0;
							$entries				= $wpdb->get_results( $query );
							$entries_sans_offset	= $wpdb->get_results( $query_sans_offset );
							$entries_count			= count( $entries_sans_offset );

							$results = '<section class="product-search">
								<div class="col-xs-12 product-search__results">
									<div class="product-search__results__wrap">
										<p class="product-search__form__toggle">
											<a href="#" target="_self" class="btn btn--red product-search__form-toggle">Toggle Search Filters &raquo;</a>
										</p>
										<p class="alignleft">Bikes Found: 0</p>
									</div>
								</div>';

								if( $entries_count > 0 ) {

									foreach( $entries as $entry ) {

										$bike_name		= get_the_title( $entry->ID );
										$image			= get_the_post_thumbnail( $entry->ID, 'newsletter-top', array( 'class' => 'article__thumbnail' ) );
										$type			= get_post_meta( $entry->ID, '_vn_bike_review_bike_type', 1 );
										$type_pieces	= explode( '::', $type );
										$type_key		= ( isset( $type_pieces[0] ) ? $type_pieces[0] : '' );
										$type_name		= ( isset( $type_pieces[1] ) ? $type_pieces[1] : '' );
										$msrp			= number_format( (int) get_post_meta( $entry->ID, '_vn_bike_review_bike_msrp', 1 ) );
										$score			= get_post_meta( $entry->ID, '_vn_bike_review_bike_overall', 1 );
										$rank			= get_bike_standing( $entry->ID, $type_key );


										/**
										 * display result: image, name, rank/type, score, price
										 */
										$results .= '<article class="col-xs-6 col-md-4 article article_type_search-result">
											<a href="'. site_url( '?p='. $entry->ID .'&resultspage='. $resultspage . $type_url_string . $price_range_url_string . $gender_url_string . $brand_url_string .'&product_search='. $bike_search ) .'" target="_self" class="article__permalink" title="'. $bike_name .'">
												'. $image .'
												<p class="article__title">'. $bike_name .'</p>
												<p class="article__rank">#'. $rank .' in <strong>'. $type_name .'</strong></p>
												<p class="article__score">Overall Score <strong>'. $score .'/100</strong></p>
												<p class="article__msrp"><strong>$'. $msrp .'</strong></p>';

												if( $gender == 'm' ) {
													$gender_display = 'Men';
												} elseif( $gender == 'f' ) {
													$gender_display = 'Women';
												} else {
													$gender_display = '';
												}
												if( $gender_display != '' ) {
													$results .= '<p class="article__gender">'. $gender_display .'</p>';
												}

											$results .= '</a>
										</article>';


										/**
										 * show 300x250 after 6th result -- only on mobile
										 */
										if( $adcount == 5 && is_mobile() ) {
											$results .= '<div class="col-xs-12 visible-xs advert advert_xs_300x250 advert_location_middle">
												<div class="advert__wrap">';

													$results .= class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom-side' ) : '';

												$results .= '</div>
											</div>';
										}

										$adcount++;

									}
									echo $results;


									/**
									 * pagination
									 */
									echo '<div class="col-xs-12 page-links">';

										$query_count_comma	= number_format( $entries_count );
										$num_per_page		= 12;
										$total_pages		= ceil( $entries_count / $num_per_page );
										$start_page			= 1;
										$end_page			= $total_pages + 1;

										if( $total_pages > 5  ) {
											if( $resultspage > 3 ) {
												if( $total_pages == $resultspage ) {
													$start_page = $total_pages - 4;
												} elseif( $total_pages - $resultspage < 2 ) {
													$start_page = $total_pages - 5 + ( $total_pages - $resultspage );
												} else {
													$start_page = $resultspage - 2;
												}
											}

											if( $start_page + 4 < $total_pages ) {
												$end_page = $start_page + 5;
											}
										}

										if( $total_pages > 1 ) {
											echo 'Pages: ';
												for( $i = $start_page; $i < $end_page; $i++ ) {
													if( $i != $resultspage ) {
														$link = '?resultspage='. $i . $page_query;
														echo '<a href="'. $link .'"><span>'. $i .'</span></a>';
													} else {
														echo '<span>'. $i .'</span>';
													}
												}
											echo '</ul>';
										}

									echo '</div>';

								} else {
									/**
									 * no bikes found from query
									 */
									$results .= '<div class="col-xs-12 product-search__no-results">
										<h2>No Bikes Found</h2>
										<p>We\'re sorry, but no bikes matching your search criteria were found. You may find the following suggestions helpful in finding the right bike.</p>
										<ul>
											<li>Use single words, such as "mountain" or "endurance"</li>
											<li>Try more general terms</li>
											<li>Check your search query for typos</li>
											<li>Select fewer filtering options</li>
										</ul>
									</div>';

									echo $results;
								}

							echo '</section>';

						} else {

							echo '<section class="product-search">
								<div class="col-xs-12 product-search__no-results">
									<h2>No Bikes Found</h2>
									<p>We\'re sorry, but no bikes matching your search criteria were found. You may find the following suggestions helpful in finding the right bike.</p>
									<ul>
										<li>Use single words, such as "mountain" or "endurance"</li>
										<li>Try more general terms</li>
										<li>Check your search query for typos</li>
										<li>Select fewer filtering options</li>
									</ul>
								</div>
							</section>';

						}

					} else {

						/**
						 * landing page
						 * show grid of 6 bike types
						 */

						echo '<section class="product-search">
							<div class="col-xs-12 product-search__results">
								<div class="product-search__results__wrap">
									<p class="product-search__form__toggle">
										<a href="#" target="_self" class="btn btn--red product-search__form-toggle">Toggle Search Filters &raquo;</a>
									</p>
								</div>
							</div>';

							if( have_posts() ) {
								while( have_posts() ) {
									the_post();

									echo '<div class="col-xs-12 product-search__intro">';

										$intro_title = ( get_post_meta( get_the_ID(), '_vn_intro_title', 1 ) != '' ) ? get_post_meta( get_the_ID(), '_vn_intro_title', 1 ) : '';

										if( $intro_title != '' ) {

											echo '<h2>'. $intro_title .'</h2>';

										}


										$featured_bike_id = ( get_post_meta( get_the_ID(), '_vn_featured_bike_id', 1 ) != '' ) ? get_post_meta( get_the_ID(), '_vn_featured_bike_id', 1 ) : '';

										if( $featured_bike_id != '' ) {

											$bike_type	= explode( '::', get_post_meta( $featured_bike_id, '_vn_bike_review_bike_type', 1 ) );
											$bike_name	= get_the_title( $featured_bike_id );
											$image		= get_the_post_thumbnail( $featured_bike_id, 'newsletter-top', array( 'class' => 'article__thumbnail' ) );
											$rank		= get_bike_standing( $featured_bike_id, $bike_type[0] );
											$score		= get_post_meta( $featured_bike_id, '_vn_bike_review_bike_overall', 1 );
											$msrp		= number_format( (int) get_post_meta( $featured_bike_id, '_vn_bike_review_bike_msrp', 1 ) );


											echo '<aside class="alignright article_type_search-result bike-search__featured">
												<h3 class="bike-search__featured__title">Featured Bike Review</h3>

												<div class="bike-search__featured__wrap">

													<a href="'. site_url( '?p='. $featured_bike_id .'&resultspage=1&type[]='. $bike_type[0] .'&price_range[]=all&gender[]=all&brand[]=all&product_search=' ) .'" target="_self" class="article__permalink" title="'. $bike_name .'">'.
														$image .'
														<div class="article__title">'. $bike_name .'</div>
														<div class="article__rank">#'. $rank .' in <strong>'. $bike_type[1] .'</strong></div>
														<div class="article__score">Overall Score <strong>'. $score .'/100</strong></div>
														<div class="article__msrp"><strong>$'. $msrp .'</strong></div>';

													echo '</a>

												</div>
											</aside>';

										}

										echo apply_filters( 'the_content', get_the_content() ) .'

									</div>';
								}
							}


							/**
							 * show generic images of each bike type
							 * images are modified in the edit screen of this page in a cmb2 group
							 */
							$bike_types = get_post_meta( get_the_ID(), '_vn_bike_type', 1 );

							if( !empty( $bike_types ) ) {

								echo '<div class="col-xs-12 product-search__categorytitle"><h2>Rankings By Category</h2></div>';

								foreach( $bike_types as $bike_type ) {

									$type_name	= explode( '::', $bike_type['bike_type'] );
									$img		= wp_get_attachment_image( $bike_type['image_id'], 'newsletter-top', null, array( 'class' => 'product-search__category__thumbnail' ) );

									echo '<div class="col-xs-6 bike-search__category product-search__category">
										<a href="?resultspage=1&type[]='. $type_name[0] .'&price_range[]=all&gender[]=all&brand[]=all&product_search=" target="_self" title="'. $type_name[1] .'">
											'. $img .'
											<div class="bike-search__img-filter"></div>
											<div class="product-search__category__title bike-search__category__title">'. strtoupper( $type_name[1] )  .'</div>
										</a>
									</div>';

								}

							}


							/**
							 * latest reviews
							 * show last 9 bike reviews
							 */
							echo '<div class="col-xs-12 product-search__recent">
								<h2>Latest Reviews</h2>
							</div>';

							// transient caching
							if ( false === ( $latest_bike_reviews_query = get_transient( 'latest_bike_reviews_query' ) ) ) {
								$args = array(
									'post_type'					=> 'bike-reviews',
									'post_status'				=> 'publish',
									'posts_per_page'			=> 9,
									'ignore_sticky_posts'		=> true,
									'no_found_rows'				=> false,
									'update_post_meta_cache'	=> false
								);

								$latest_bike_reviews_query = new WP_Query( $args );
								set_transient( 'latest_bike_reviews_query', $latest_bike_reviews_query, 15 * MINUTE_IN_SECONDS );
							}

							if( $latest_bike_reviews_query->have_posts() ) {
								while( $latest_bike_reviews_query->have_posts() ) {
									$latest_bike_reviews_query->the_post();

									$bike_type	= explode( '::', get_post_meta( get_the_ID(), '_vn_bike_review_bike_type', 1 ) );
									$bike_name	= get_the_title( get_the_ID() );
									$image		= get_the_post_thumbnail( get_the_ID(), 'newsletter-top', array( 'class' => 'article__thumbnail' ) );
									$rank		= get_bike_standing( get_the_ID(), $bike_type[0] );
									$score		= get_post_meta( get_the_ID(), '_vn_bike_review_bike_overall', 1 );
									$msrp		= number_format( (int) get_post_meta( get_the_ID(), '_vn_bike_review_bike_msrp', 1 ) );

									echo '<article class="col-xs-6 col-md-4 article article_type_search-result">
										<a href="'. site_url( '?p='. get_the_ID() .'&resultspage=1&type[]='. $bike_type[0] .'&price_range[]=all&gender[]=all&brand[]=all&product_search=' ) .'" target="_self" class="article__permalink" title="'. $bike_name .'">'.
											$image .'
											<div class="article__title">'. $bike_name .'</div>
											<div class="article__rank">#'. $rank .' in <strong>'. $bike_type[1] .'</strong></div>
											<div class="article__score">Overall Score <strong>'. $score .'/100</strong></div>
											<div class="article__msrp"><strong>$'. $msrp .'</strong></div>
										</a>
									</article>';

								}
								wp_reset_postdata();
							}

							echo '</section>

						<div class="col-xs-12">
							<a href="'. get_permalink() .'?resultspage=1&type[]=all&price_range[]=all&gender[]=all&brand[]=all&product_search=" target="_self" class="content__section__more">VIEW MORE <span class="fa fa-caret-right"></span></a>
						</div>';
					}
				?>

			</section>

			<?php get_sidebar( 'right' ); ?>

		</section>

	</div>
</section>


<?php
	/**
	 * ad
	 */
?>
<section class="advert advert_xs_300x250 advert_sm_728x90 advert_location_bottom ">
	<div class="advert__wrap">
		<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom' ) : ''; ?>
	</div>
</section>


<?php get_footer(); ?>
