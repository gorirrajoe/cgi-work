<?php
	/**
	 * Template Name: Single Column
	 *
	 * @package velonews
	 */
	get_header();
?>

<section id="content" class="content template--single">
	<div class="container content__container">
		<div class="row">

			<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', 'single-column' );

				endwhile; // End of the loop.
			?>

		</div>
	</div>
</section>


<?php
	/**
	 * ad
	 */
?>
<section class="advert advert_xs_300x250 advert_sm_728x90 advert_location_bottom ">
	<div class="advert__wrap">
		<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom' ) : ''; ?>
	</div>
</section>


<?php get_footer(); ?>
