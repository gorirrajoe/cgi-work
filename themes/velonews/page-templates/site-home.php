<?php
/**
 * Template Name: Site Home
 *
 * @package velonews
 */

get_header(); ?>

<section id="content" class="content">
	<div class="container content__container">
		<h1 class="screen-reader-text">Velo News Homepage</h1>
		<section class="row content__flex">

			<?php
				/**
				 * news feed
				 */
			?>
			<section class="content__section content__flex_position_first news-feed">
				<?php get_sidebar( 'left' ); ?>
			</section>


			<?php
				/**
				 * 5 articles that have been marked as "featured"
				 * center column
				 */
				get_template_part( 'template-parts/content', 'top-five' );
			?>


			<section class="content__section content__flex_position_third">
				<?php
					/**
					 * one column (featured post)
					 */
					include( locate_template( 'template-parts/content-one.php' ) );


					/**
					 * ad: 300x600
					 */
				?>
				<section class="col-xs-12 hidden-xs advert advert_sm_300x600">
					<div class="advert__wrap">
						<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'side-top' ) : ''; ?>
					</div>
				</section>


				<?php
					/**
					 * two column (tablet)
					 */
					echo '<!-- two column (responsive) -->';
					get_template_part( 'template-parts/content', 'two-homepage' );
				?>


			</section>
		</section>


		<?php
			/**
			 * four column
			 */
			include( locate_template( 'template-parts/content-four-top.php' ) );


			/**
			 * 970x250 ad
			 */
		?>
		<section class="row content__section">
			<section class="advert advert_xs_300x250 advert_sm_728x90 advert_md_970x250">
				<div class="advert__wrap">
					<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom' ) : ''; ?>
				</div>
			</section>
		</section>


		<?php
			/**
			 * two column - featured articles (rich media?)
			 */
			include( locate_template( 'template-parts/content-two.php' ) );


			/**
			 * five column
			 */
			include( locate_template( 'template-parts/content-five.php' ) );


			/**
			 * two by two
			 */
			include( locate_template( 'template-parts/content-two-by-two.php' ) );


			/**
			 * four column
			 */
			include( locate_template( 'template-parts/content-four-bottom.php' ) );
		?>


	</div>
</section>


<?php
	/**
	 * ad
	 */
?>
<section class="advert advert_xs_300x250 advert_sm_728x90 advert_location_bottom ">
	<div class="advert__wrap">
		<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom' ) : ''; ?>
	</div>
</section>


<?php get_footer();
