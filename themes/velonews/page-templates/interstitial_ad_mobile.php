<?php
/* Template Name: Mobile Interstitial Ad */
?>
<!DOCTYPE html>
<html lang="en-US">
	<head>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="robots" content="noindex">
		<title>Advertisement</title>
		<?php
			/**
			 * START OF DFP AD SERVER CODE
			 */
		?>
		<script type='text/javascript'>
			(function() {
			var useSSL = 'https:' == document.location.protocol;
			var src = (useSSL ? 'https:' : 'http:') +
			'//www.googletagservices.com/tag/js/gpt.js';
			document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
			})();
		</script>
		<?php $adKw = class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::singleton()->generate_dfp_keywords() : ''; ?>
		<script type='text/javascript'>
			googletag.defineSlot( '<?php echo $adKw; ?>', [250, 208], 'div-250_208_interstitial').addService(googletag.pubads()).setTargeting('pos', 'interstitial');

			googletag.pubads().enableSyncRendering();
			googletag.pubads().enableSingleRequest();
			googletag.enableServices();
		</script>

	</head>
	<body style="margin:0 auto;">
		<div id="interstitial-ad" style="">
			<div id="div-250_208_interstitial" style="width: 250px; height: 208px">
				<script type="text/javascript">googletag.display( "div-250_208_interstitial" );</script>
			</div>
		</div>
	</body>
</html>
