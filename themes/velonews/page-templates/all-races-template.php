<?php
	/**
	 * Template Name: All Races
	 *
	 * @package velonews
	 */
	get_header();
?>

<section id="content" class="content template--single">
	<div class="container content__container">
		<header class="row">
			<div class="col-xs-12">
				<h1 class="content__title"><?php echo get_the_title(); ?></h1>
			</div>
		</header>
		<div class="row">
			<?php
				$year	= is_numeric( get_query_var('raceyear') ) ? get_query_var('raceyear') : date( 'Y' );
				$args	= array(
					'post_type'			=> 'all-races',
					'posts_per_page'	=> -1,
					'post_status'		=> 'publish',
					'orderby'			=> 'meta_value',
					'order'				=> 'ASC',
					'meta_query'		=> array(
						array(
							'key'		=> '_vn_race_date_start',
							'value'		=> strtotime( '12/31/' . $year ),
							'compare'	=> '<'
						),
						array(
							'key'		=> '_vn_race_date_start',
							'value'		=> strtotime( '01/01/' . $year ),
							'compare'	=> '>'
						),
					),
					'no_found_rows'				=> false,
					'update_post_meta_cache'	=> false
				);
				$allraces_query = new WP_Query( $args );

			?>

			<article class="col-xs-12 col-sm-7 col-md-8 article">
				<header class="article__header">
					<h2 class="article__header__title"><?php echo $year; ?> Races</h2>
				</header>

				<section class="article__body race-table">
					<div class="race-table__wrap">

						<ul id="race-table-nav" class="nav nav-pills race-table__nav" role="tablist">
							<li role="presentation" class="active">
								<a href="#mens-road" aria-controls="mens-road" role="tab" data-toggle="tab" aria-expanded="true">MENS ROAD</a>
							</li>
							<li role="presentation" class="">
								<a href="#womens-road" aria-controls="womens-road" role="tab" data-toggle="tab" aria-expanded="false">WOMENS ROAD</a>
							</li>
							<li role="presentation" class="">
								<a href="#cyclocross" aria-controls="cyclocross" role="tab" data-toggle="tab" aria-expanded="false">CYCLOCROSS</a>
							</li>
						</ul>

						<div class="tab-content race-table__content">
							<div id="mens-road" class="tab-pane race-table__pane active" role="tabpanel">
								<table class="race-table__table">
									<thead>
										<tr>
											<th>DATE</th>
											<th>RACE</th>
											<th>CONTENT</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if ( $allraces_query->have_posts() ) {
												$count = 0;
												while ( $allraces_query->have_posts() ) {
													$allraces_query->the_post();

													$race_date_start	= date( 'm/d/Y', get_post_meta( get_the_ID(), '_vn_race_date_start', 1 ) );
													$race_date_end		= date( 'm/d/Y', get_post_meta( get_the_ID(), '_vn_race_date_end', 1 ) );
													$race_url			= get_post_meta( get_the_ID(), '_vn_race_url', 1 );

													if( get_post_meta( get_the_ID(), '_vn_race_type', 1 ) == 'mens_road' ) {
														echo '<tr>';
															if( $race_date_start == $race_date_end ) {
																$race_date_display = $race_date_start;
															} else {
																$race_date_display = $race_date_start . ' &ndash; ' . $race_date_end;
															}

															echo '<td>'. $race_date_display .'</td>
															<td>'. get_the_title() .'</td>
															<td><a href="'. $race_url .'" target="_self"><strong>News &amp; Reports</strong></a></td>
														</tr>';
														$count++;
													}
												}
												if( $count == 0 ) {
													echo '<tr>
														<td colspan="3">No races added at this time.</td>
													</tr>';
												}

											}
											wp_reset_postdata();

										?>
									</tbody>
								</table>
							</div>
							<div id="womens-road" class="tab-pane race-table__pane" role="tabpanel">
								<table class="race-table__table">
									<thead>
										<tr>
											<th>DATE</th>
											<th>RACE</th>
											<th>CONTENT</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if ( $allraces_query->have_posts() ) {
												$count = 0;
												while ( $allraces_query->have_posts() ) {
													$allraces_query->the_post();

													$race_date_start	= date( 'm/d/Y', get_post_meta( get_the_ID(), '_vn_race_date_start', 1 ) );
													$race_date_end		= date( 'm/d/Y', get_post_meta( get_the_ID(), '_vn_race_date_end', 1 ) );
													$race_url			= get_post_meta( get_the_ID(), '_vn_race_url', 1 );

													if( get_post_meta( get_the_ID(), '_vn_race_type', 1 ) == 'womens_road' ) {
														echo '<tr>';
															if( $race_date_start == $race_date_end ) {
																$race_date_display = $race_date_start;
															} else {
																$race_date_display = $race_date_start . ' &ndash; ' . $race_date_end;
															}

															echo '<td>'. $race_date_display .'</td>
															<td>'. get_the_title() .'</td>
															<td><a href="'. $race_url .'" target="_self"><strong>News &amp; Reports</strong></a></td>
														</tr>';
														$count++;
													}
												}
												if( $count == 0 ) {
													echo '<tr>
														<td colspan="3">No races added at this time.</td>
													</tr>';
												}
											}
											wp_reset_postdata();

										?>

									</tbody>
								</table>
							</div>
							<div id="cyclocross" class="tab-pane race-table__pane" role="tabpanel">
								<table class="race-table__table">
									<thead>
										<tr>
											<th>DATE</th>
											<th>RACE</th>
											<th>CONTENT</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if ( $allraces_query->have_posts() ) {
												$count = 0;
												while ( $allraces_query->have_posts() ) {
													$allraces_query->the_post();

													$race_date_start	= date( 'm/d/Y', get_post_meta( get_the_ID(), '_vn_race_date_start', 1 ) );
													$race_date_end		= date( 'm/d/Y', get_post_meta( get_the_ID(), '_vn_race_date_end', 1 ) );
													$race_url			= get_post_meta( get_the_ID(), '_vn_race_url', 1 );

													if( get_post_meta( get_the_ID(), '_vn_race_type', 1 ) == 'cyclocross' ) {
														echo '<tr>';
															if( $race_date_start == $race_date_end ) {
																$race_date_display = $race_date_start;
															} else {
																$race_date_display = $race_date_start . ' &ndash; ' . $race_date_end;
															}
															echo '<td>'. $race_date_display .'</td>
															<td>'. get_the_title() .'</td>
															<td><a href="'. $race_url .'" target="_self"><strong>News &amp; Reports</strong></a></td>
														</tr>';
														$count++;
													}
												}
												if( $count == 0 ) {
													echo '<tr>
														<td colspan="3">No races added at this time.</td>
													</tr>';
												}

											}
											wp_reset_postdata();
										?>

									</tbody>
								</table>
							</div>
						</div>
					</div>
					<br>


					<?php
						/**
						 * check for previous or next year races
						 * only need 1 of each to determine if one exists
						 */
						$prev_year		= $year - 1;
						$next_year		= $year + 1;

						$prev_year_args	= array(
							'post_type'			=> 'all-races',
							'posts_per_page'	=> 1,
							'post_status'		=> 'publish',
							'orderby'			=> 'meta_value',
							'order'				=> 'ASC',
							'meta_query'		=> array(
								array(
									'key'		=> '_vn_race_date_start',
									'value'		=> strtotime( '12/31/' . $prev_year ),
									'compare'	=> '<'
								),
								array(
									'key'		=> '_vn_race_date_start',
									'value'		=> strtotime( '01/01/' . $prev_year ),
									'compare'	=> '>'
								),
							),
							'no_found_rows'				=> false,
							'update_post_meta_cache'	=> false
						);
						$prev_year_allraces_query = new WP_Query( $prev_year_args );


						$next_year_args	= array(
							'post_type'			=> 'all-races',
							'posts_per_page'	=> 1,
							'post_status'		=> 'publish',
							'orderby'			=> 'meta_value',
							'order'				=> 'ASC',
							'meta_query'		=> array(
								array(
									'key'		=> '_vn_race_date_start',
									'value'		=> strtotime( '12/31/' . $next_year ),
									'compare'	=> '<'
								),
								array(
									'key'		=> '_vn_race_date_start',
									'value'		=> strtotime( '01/01/' . $next_year ),
									'compare'	=> '>'
								),
							),
							'no_found_rows'				=> false,
							'update_post_meta_cache'	=> false
						);
						$next_year_allraces_query = new WP_Query( $next_year_args );


						if( $prev_year_allraces_query->found_posts != 0 || $next_year_allraces_query->found_posts != 0 ) {
							echo '<nav class="navigation posts-navigation" role="navigation">
								<h2 class="screen-reader-text">Posts navigation</h2>
								<div class="nav-links">';

									if( $prev_year_allraces_query->found_posts != 0 ) {
										$prev_year_nav = ( $prev_year == date( 'Y' ) ) ? $prev_year_nav = '' : $prev_year;
										echo '<div class="nav-previous">
											<a href="'. get_permalink() .'/'. $prev_year_nav .'" target="_self">&laquo; Previous</a>
										</div>';
									}
									if( $next_year_allraces_query->found_posts != 0 ) {
										$next_year_nav = ( $next_year == date( 'Y' ) ) ? $next_year_nav = '' : $next_year;
										echo '<div class="nav-next">
											<a href="'. get_permalink() .'/'. $next_year_nav .'" target="_self">Next &raquo;</a>
										</div>';
									}
								echo '</div>
							</nav>';

						}

					?>

				</section>
			</article>

			<?php get_sidebar( 'right' ); ?>

		</div>
	</div>
</section>


<?php
	/**
	 * ad
	 */
?>
<section class="advert advert_xs_300x250 advert_sm_728x90 advert_location_bottom ">
	<div class="advert__wrap">
		<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom' ) : ''; ?>
	</div>
</section>


<?php get_footer(); ?>
