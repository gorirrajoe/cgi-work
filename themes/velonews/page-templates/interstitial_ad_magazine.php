<?php
/* Template Name: Interstitial Ad Magazine */
?>
<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>Advertisement</title>
	</head>
	<body style="overflow:hidden;">
		<div id="interstitial-ad" style="height:215px;">
			<a href="<?php echo cgi_bikes_get_option( 'magazine_interstitial_link' ); ?>" target="_blank">
				<img src="<?php echo cgi_bikes_get_option( 'magazine_interstitial_image' ); ?>" />
			</a>
		</div>
	</body>
</html>
