<?php
/* Template Name: Interstitial Ad Newsletter */
?>
<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>Advertisement</title>
		<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js'></script>
	</head>
	<body style="overflow:hidden;">
		<script type="text/javascript">
			jQuery(document).ready(function() {
				jQuery("form#icontact_side").prepend("<div id='please_wait' style='display: none;position:relative;top:220px;margin-left:240px;'><img src='<?php echo get_bloginfo('stylesheet_directory'); ?>/images/icontact-wait.gif'/></div>");
				jQuery("form#icontact_side").submit(function() {
					jQuery("input#fields_email_side").attr("readonly", true);
					jQuery("#icontact_side .submit-button").attr("disabled", true);
					/* Validation */
					var error = new Boolean(false);
					var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
					if(
						jQuery("input#fields_email_side").val() == '' ||
						jQuery("input#fields_email_side").val() == jQuery("input#fields_email_side")[0].defaultValue ||
						regex.test(jQuery("input#fields_email_side").val()) == false
					) {
						jQuery("input#fields_email_side").addClass("validation-error");
						error = true;
						jQuery("#errorMessage_side").html('There was an error with your email address.');
						jQuery("input#fields_email_side").removeAttr("readonly");
						jQuery("#icontact_side .submit-button").removeAttr("disabled");
					} else {
						jQuery("input#fields_email_side").removeClass("validation-error");
					}
					if(false != error) {
						return false;
					};
					jQuery("input#fields_email_side").hide();
					jQuery("#icontact_side .submit-button").hide();
					jQuery("#errorMessage_side").hide();
					jQuery("#signedUpLabel").hide();
					jQuery("#please_wait").show();
					/* Ajax submission code here */
					//Submit form
					jQuery.post(
						"./",
						jQuery("#icontact_side").serialize() + "&ajax=true",
						 function(data){
							 jQuery("div#icontact_wrapper_side").html(data);
							 jQuery('h2').css('marginLeft', '240px');
							 jQuery('h2').css('position', 'relative');
							 jQuery('h2').css('top', '200px');
							 jQuery('h2').css('fontFamily', 'sans-serif');
							 jQuery("div#icontact_wrapper_side p").css('fontFamily', 'sans-serif');
							 jQuery("div#icontact_wrapper_side p").css('fontSize', '.8em');
							 jQuery("div#icontact_wrapper_side p").css('marginLeft', '240px');
							 jQuery("div#icontact_wrapper_side p").css('marginTop', '215px');
						 }
					);
					return false;

				});
				jQuery("#icontact_side input[type=text]").focus(function() {
					if (jQuery(this).val() == this.defaultValue) {
						jQuery(this).val("").removeClass("default");
					}
					jQuery(this).removeClass("validation-error");
				});
				jQuery("#icontact_side input[type=text]").blur(function() {
					if (jQuery(this).val() == "") {
						jQuery(this).val(this.defaultValue).addClass("default");
					}
				});
			});
		</script>
		<div id="interstitial-ad" style="height:340px;">
			<?php
				if( have_posts() ) : while ( have_posts() ) : the_post();
					the_content();
				endwhile; endif;
			?>
		</div>
	</body>
</html>
