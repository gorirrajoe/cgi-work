<?php
	/* Template Name: Apparel Search Page */

	get_header();
	global $wpdb;

	global $post_id_array;
	$post_id_array = array();

	// query setup
	$page				= 0;
	$resultspage		= 1;
	$apparel_search		= '';
	$query				= '';
	$gender_sql			= '';
	$apparel_type_sql	= '';
	$apparel_brand_sql	= '';
	$apparel_search_sql	= '';
	$guide				= '';

	$apparel_type		= array();
	$apparel_brand		= array();
	$helmet_type		= array();
	$gender				= array();
	$apparel_choices	= array(
		'all'			=> 'All Accessories',
		'jersey'		=> 'Jerseys',
		'bib'			=> 'Bibs/Shorts',
		'outerwear'		=> 'Outerwear',
		'shoe'			=> 'Shoes',
		'helmet'		=> 'Helmets',
		'tool'			=> 'Tools',
		'wheels'		=> 'Wheels',
		'misc'			=> 'Miscellaneous',
	);


	/**
	 * form submitted
	 */
	if( isset( $_REQUEST['resultspage'] ) ) {
		if( !isset( $_REQUEST['resultspage'] ) ) {
			$resultspage = 1;
		} else {
			$resultspage = $_REQUEST['resultspage'];
		}
		$apparel_type	= $_REQUEST['type'];
		$gender			= $_REQUEST['gender'];
		$apparel_brand	= $_REQUEST['brand'];
		$guide			= isset( $_REQUEST['cat'] ) ? $_REQUEST['cat'] : '';
		$apparel_search	= sanitize_text_field( $_REQUEST['apparel_search'] );
	}
if( strpos( $guide, 'guide') !== FALSE ) {?>

<section id="content" class="content template--product-search">
	<div class="container content__container">
		<header class="row">
			<div class="col-xs-12">
				<h1 class="content__title"><?php the_title(); ?></h1>
				<ol class="article__bread-crumb">
					<li><a href="<?php echo site_url() ?>" target="_self">Home</a></li>
					&raquo;
					<?php if( !isset( $_REQUEST['resultspage'] ) ): ?>
						<li><?php the_title(); ?></li>
					<?php else: ?>
						<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
					<?php endif; ?>
					<?php if( isset( $_REQUEST['type'] ) ): ?>
						<?php
							if( 1 == count($_REQUEST['type']) ) {
								$type = $_REQUEST['type'][0];
								printf(
									'&raquo; <li><a href="%s?resultspage=1&type[]=%s&gender[]=all&brand[]=all&apparel_search=">%s</a></li>',
									get_permalink(),
									$type,
									$apparel_choices[$type],
									$type
								);
							}
						?>
						&raquo;
						<li>Search Results</li>
					<?php endif; ?>
				</ol>
			</div>
		</header>
		<section class="row content__flex">
			<section class="content__section content__flex_position_first product-search">
				<?php
					/**
					 * apparel model text search
					 */
				?>
				<div class="product-search__wrap" id="product-search-form">

					<h3 class="product-search__heading product-search__heading_name_brand">Search By Brand</h3>

					<form action="<?php echo get_permalink(); ?>" method="GET" name="text_product_search" id="product-search-text" class="product-search__form product-search__form_name_text">
						<input type="hidden" value="1" name="resultspage">
						<input type="hidden" value="all" name="type[]">
						<input type="hidden" value="all" name="gender[]">
						<input type="hidden" value="all" name="brand[]">

						<fieldset class="product-search__form__fieldset product-search__form__fieldset_name_search">
							<div class="product-search__form__option">
								<label for="product-search-search" class="screen-reader-text">Search Apparel</label>
								<input type="text" name="apparel_search" id="product-search-search" value="<?php echo $apparel_search; ?>" placeholder="Search brands" class="product-search__form__input">
								<button class="btn btn--red"><span class="fa fa-search"></span></button>
							</div>
							<a href="#" target="_self" class="product-search__form-toggle"><span class="fa fa-times"></span></a>
						</fieldset>
					</form>

					<h3 class="product-search__heading product-search__heading_name_filter">Search By Filter</h3>

					<?php
						/**
						 * apparel multi-option search
						 */
					?>
					<form action="<?php echo get_permalink(); ?>" method="GET" name="multifacet_product_search" id="product-search-multifacet" class="product-search__form product-search__form_name_multifacet">
						<input type="hidden" value="1" name="resultspage">

						<?php
							/**
							 * apparel type
							 * helmet, jersey, shoe, bib, outerwear, tool, misc
							 * these types are hard-coded
							 */
						?>

						<fieldset class="product-search__form__fieldset">
							<legend class="product-search__form__legend">
								<a href="#product-search-type-filters" target="_self" title="Toggle Type Filters" class="product-search__filter-toggle">
									ACCESSORIES <span class="fa fa-caret-down"></span>
								</a>
							</legend>
							<div id="product-search-type-filters" class="product-search__form__group">
								<?php
									foreach( $apparel_choices as $key => $value ) {
										$checked = (in_array( $key, $apparel_type ) ? 'checked' : '');

										if( $key == 'all' && ( !isset( $apparel_type[0] ) || $apparel_type[0] == 'all' ) ) {
											$checked = 'checked';
										}

										echo '<div class="product-search__form__option">
											<input '. $checked .' type="checkbox" name="type[]" id="type-'. $key .'" value="'. $key .'" class="product-search__form__checkbox">
											<label for="type-'. $key .'" class="product-search__form__label">'. $value .'</label>
										</div>';
									}
								?>
							</div>
						</fieldset>


						<?php
							/**
							 * gender
							 */
						?>
						<fieldset class="product-search__form__fieldset">
							<legend class="product-search__form__legend">
								<a href="#product-search-gender-filters" target="_self" title="Toggle Gender Filters" class="product-search__filter-toggle">
									GENDER <span class="fa fa-caret-down"></span>
								</a>
							</legend>
							<div id="product-search-gender-filters" class="product-search__form__group">
								<div class="product-search__form__option">
									<input <?php echo ( !isset( $gender[0] ) || $gender[0] == 'all' ) ? 'checked' : ''; ?> type="checkbox" name="gender[]" id="gender-all" value="all" class="product-search__form__checkbox">
									<label for="gender-all" class="product-search__form__label">All</label>
								</div>

								<?php
									$men_checked	= in_array( 'm', $gender ) ? 'checked' : '';
									$women_checked	= in_array( 'f', $gender ) ? 'checked' : '';

									echo '<div class="product-search__form__option">
										<input '. $men_checked .' type="checkbox" name="gender[]" id="gender-men" value="m" class="product-search__form__checkbox">
										<label for="gender-men" class="product-search__form__label">Men</label>
									</div>
									<div class="product-search__form__option">
										<input '. $women_checked .' type="checkbox" name="gender[]" id="gender-women" value="f" class="product-search__form__checkbox">
										<label for="gender-women" class="product-search__form__label">Women</label>
									</div>';
								?>

							</div>
						</fieldset>

						<?php
							/**
							 * brand
							 * this list will populate as more products are added
							 */
							$query = 'SELECT DISTINCT r.meta_value FROM wp_postmeta r, wp_posts s WHERE r.meta_key = "_vn_apparel_brand" AND s.ID = r.post_id AND s.post_status = "publish" order by r.meta_value';
							$entries_brand = $wpdb->get_results( $query );
						?>
						<fieldset class="product-search__form__fieldset">
							<legend class="product-search__form__legend">
								<a href="#product-search-brand-filters" target="_self" title="Toggle Brand Filters" class="product-search__filter-toggle">
									BRAND <span class="fa fa-caret-down"></span>
								</a>
							</legend>
							<div id="product-search-brand-filters" class="product-search__form__group">
								<div class="product-search__form__option">
									<input <?php echo ( !isset( $apparel_brand[0] ) || $apparel_brand[0] == 'all' ) ? 'checked' : ''; ?> type="checkbox" name="brand[]" id="brand-all" value="all" class="product-search__form__checkbox">
									<label for="brand-all" class="product-search__form__label">All</label>
								</div>

								<?php
									$brand_count = 1;
									foreach( $entries_brand as $entry_brand ) {
										$dashed_name	= str_replace( array( ' ', '\'' ), '_', $entry_brand->meta_value );
										$checked		= in_array( $dashed_name, $apparel_brand ) ? 'checked' : '';

										/**
										 * show only 4 brands + "all", hide the rest under a "see all brands" toggle link
										 */
										// if( $brand_count == 5 ) {
										// 	echo '<div id="product-search__brand__hidden" class="collapse">';
										// }
										if( $brand_count > 4 ) {
											$option_hide_class = ' product-search__form__option__hidden';
										} else {
											$option_hide_class = '';
										}

										echo '<div class="product-search__form__option'. $option_hide_class .'">
											<input '. $checked .' type="checkbox" name="brand[]" id="brand-'. $dashed_name .'" value="'. $dashed_name .'" class="product-search__form__checkbox">
											<label for="brand-'. $dashed_name .'" class="product-search__form__label">'. $entry_brand->meta_value .'</label>
										</div>';

										$brand_count++;
									}
									if( $brand_count > 4 ) {
										// echo '</div>
										echo '<div class="product-search__more"><a href="javascript:;" class="show-hidden-toggle">See All Brands</a></div>';
									}
								?>
							</div>
						</fieldset>

						<fieldset class="product-search__form__fieldset product-search__form__fieldset_name_submit">
							<input type="hidden" value="" name="apparel_search">
							<input type="submit" value="Filter Results" class="btn btn--red">
						</fieldset>

					</form>
				</div>
			</section>

			<section class="content__section content__flex_position_second">

				<?php
					/**
					 * a search was made
					 */
					if( isset( $_REQUEST['resultspage'] ) ) {
						/**
						 * set up the main query
						 * only search for products that were added within the past 36 months
						 */
						$query = 'SELECT r.ID, t.meta_value FROM wp_posts r, wp_postmeta t WHERE r.post_type = "apparel-reviews" AND r.post_status = "publish" AND r.post_date >= DATE_SUB(NOW(), INTERVAL 36 MONTH) ';


						/**
						 * type filter was modified
						 * get post_id's for as many types selected
						 */

						if( $apparel_type[0] != 'all' ) {
							$apparel_type_or	= '';
							$apparel_type_count	= count( $apparel_type );

							for( $i = 0; $i < $apparel_type_count; $i++ ) {
								$apparel_type_or .= 'meta_value like "%' . $apparel_type[$i] . '%"';
								if( $i != ( $apparel_type_count -1 ) ) {
									$apparel_type_or .= ' OR ';
								}
							}
							$apparel_type_sql = 'SELECT post_id FROM wp_postmeta WHERE meta_key = "_vn_apparel_type" AND ('. $apparel_type_or .')';
						}


						/**
						 * gender filter was modified
						 * get post_id's for as many gender types selected
						 */
						if( $gender[0] != 'all' ) {
							$gender_or		= '';
							$gender_count	= count( $gender );

							for( $i = 0; $i < $gender_count; $i++ ) {
								$gender_or .= 'meta_value like "%' . $gender[$i] . '%"';
								if( $i != ( $gender_count -1 ) ) {
									$gender_or .= ' OR ';
								}
							}
							$gender_sql = 'SELECT post_id FROM wp_postmeta WHERE ( meta_key = "_vn_apparel_gender" AND '. $gender_or . 'OR meta_key = "_vn_apparel_gender" AND meta_value like "%u%" )';
						}


						/**
						 * brand filter was modified
						 * get post_id's for as many brands selected
						 */
						if( $apparel_brand[0] != 'all' ) {
							$apparel_brand_or		= '';
							$apparel_brand_count	= count( $apparel_brand );

							for( $i = 0; $i < $apparel_brand_count; $i++ ) {
								$apparel_brand_or .= 'meta_value like "%' . str_replace( ' ', '_', $apparel_brand[$i] ) . '%"';
								if( $i != ( $apparel_brand_count -1 ) ) {
									$apparel_brand_or .= ' OR ';
								}
							}
							$apparel_brand_sql = 'SELECT post_id FROM wp_postmeta WHERE meta_key = "_vn_apparel_brand" AND '. $apparel_brand_or;

						}


						/**
						 * user typed in product brand into text input field
						 * get post_id's where brand matches
						 */
						if( $apparel_search != '' ) {
							$apparel_search_sql = 'SELECT post_id FROM wp_postmeta WHERE meta_key = "_vn_apparel_brand" AND meta_value LIKE "%'. $apparel_search .'%"';
						}


						/**
						 * build onto main query based on selections the user made
						 * logic is set up to be top-down to cover all search scenarios
						 */
						if( $apparel_type_sql != "" ) {
							$query .= ' AND r.ID IN ( '. $apparel_type_sql;

							if( $gender_sql != "" ) {
								$query .= ' AND post_id IN ( '. $gender_sql . ')';
							}
							if( $apparel_brand_sql != "" ) {
								$query .= ' AND post_id IN ( '. $apparel_brand_sql . ')';
							}
							$query .= ')';

						} elseif( $gender_sql != "" ) {
							$query .= ' AND ID IN ( '. $gender_sql;

							if( $apparel_brand_sql != "" ) {
								$query .= ' AND post_id IN ( '. $apparel_brand_sql . ')';
							}
							$query .= ')';

						} elseif( $apparel_brand_sql != "" ) {
							$query .= ' AND r.ID IN ( '. $apparel_brand_sql . ')';

						} elseif( $apparel_search_sql != "" ) {
							$query .= ' AND r.ID IN ( '. $apparel_search_sql . ')';
						}


						$offset = 0;
						if( $resultspage > 1 ) {
							/**
							 * show 12 results per page
							 */
							$offset = ( $resultspage - 1 ) * 12; // (page 2 - 1)*12 = offset of 12
						}
						$query_sans_offset = $query . ' AND t.post_id = r.ID AND t.meta_key = "_vn_rating" ORDER BY post_date DESC';
						$query .= ' AND t.post_id = r.ID AND t.meta_key = "_vn_rating" ORDER BY post_date DESC LIMIT 12 OFFSET '. $offset;

						/**
						 * to remember the url (history)
						 */
						$page_query = $type_url_string = $gender_url_string = $brand_url_string = '';

						/**
						 * type
						 */
						if( $apparel_type[0] != 'all' ) {
							foreach( $apparel_type as $type ) {
								$page_query .= '&type[]='. $type;
								$type_url_string .= '&type[]='. $type;
							}
						} else {
							$page_query .= '&type[]=all';
							$type_url_string .= '&type[]=all';
						}

						/**
						 * gender
						 */
						if( $gender[0] != 'all' ) {
							foreach( $gender as $brand ) {
								$page_query .= '&gender[]='. $brand;
								$gender_url_string .= '&gender[]='. $brand;
							}
						} else {
							$page_query .= '&gender[]=all';
							$gender_url_string .= '&gender[]=all';
						}


						/**
						 * brand
						 */
						if( $apparel_brand[0] != 'all' ) {
							foreach( $apparel_brand as $brand ) {
								$page_query .= '&brand[]='. $brand;
								$brand_url_string .= '&brand[]='. $brand;
							}
						} else {
							$page_query .= '&brand[]=all';
							$brand_url_string .= '&brand[]=all';
						}

						/**
						 * brand search (text input)
						 */
						if( $apparel_search ) {
							$page_query .= '&apparel_search='. $apparel_search;
						} else {
							$page_query .= '&apparel_search=';
						}


						if( $query != '' ) {
							$adcount				= 0;
							$entries				= $wpdb->get_results( $query );
							$entries_sans_offset	= $wpdb->get_results( $query_sans_offset );
							$entries_count			= count( $entries_sans_offset );

							$results = '<section class="product-search">
								<div class="col-xs-12 product-search__results">
									<div class="product-search__results__wrap">
										<p class="product-search__form__toggle">
											<a href="#" target="_self" class="btn btn--red product-search__form-toggle">Toggle Search Filters &raquo;</a>
										</p>
										<p class="alignleft">Products Found: '. $entries_count .'</p>
										<p class="alignright"><a href="#" target="_self" class="product-search__form-toggle">Filter Results &raquo;</a></p>
									</div>
								</div>';

								if( $entries_count > 0 ) {
									foreach( $entries as $entry ) {
										$image			= get_the_post_thumbnail( $entry->ID, 'bike-search', array( 'class' => 'article__thumbnail' ) );
										$type			= get_post_meta( $entry->ID, '_vn_apparel_type', 1 );
										$type_pieces	= explode( '::', $type );
										$type_key		= ( isset( $type_pieces[0] ) ? $type_pieces[0] : '' );
										$type_name		= ( isset( $type_pieces[1] ) ? $type_pieces[1] : '' );
										$msrp			= number_format( get_post_meta( $entry->ID, '_vn_apparel_msrp', 1 ) );
										$gender			= get_post_meta( $entry->ID, '_vn_apparel_gender', 1 );
										$rating			= get_post_meta( $entry->ID, '_vn_rating', 1 );

										/**
										 * display result: image, name, rating, gender, price
										 */
										$results .= '<article class="col-xs-6 col-md-4 article article_type_search-result">
											<a href="'. site_url( '?p='. $entry->ID .'&resultspage='. $resultspage . $type_url_string . $gender_url_string . $brand_url_string . '&apparel_search='. $apparel_search ) .'" target="_self" class="article__permalink" title="'. get_the_title( $entry->ID ) .'">
												'. $image .'
												<p class="article__title">'. get_the_title( $entry->ID ) .'</p>';

												if( $rating != '' ) {
													$results .= '<p class="article__rating">Rating <strong>'. $rating .'/10</strong></p>';
												}
												if( $gender == 'm' ) {
													$gender_display = 'Men';
												} elseif( $gender == 'f' ) {
													$gender_display = 'Women';
												} else {
													$gender_display = '';
												}
												if( $gender_display != '' ) {
													$results .= '<p class="article__gender">Gender <strong>'. $gender_display .'</strong></p>';
												}


												$results .= '<p class="article__msrp"><strong>$'. $msrp .'</strong></p>
											</a>
										</article>';


										/**
										 * show 300x250 after 6th result -- only on mobile
										 */
										if( $adcount == 5 && is_mobile() ) {
											$results .= '<div class="col-xs-12 visible-xs advert advert_xs_300x250 advert_location_middle">
												<div class="advert__wrap">';
													echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'side-middle' ) : '';
												$results .= '</div>
											</div>';
										}

										$adcount++;

									}
									echo $results;


									/**
									 * pagination
									 */
									echo '<div class="col-xs-12 page-links">';
										$query_count_comma	= number_format( $entries_count );
										$num_per_page		= 12;
										$total_pages		= ceil( $entries_count / $num_per_page );
										$start_page			= 1;
										$end_page			= $total_pages + 1;

										if( $total_pages > 5  ) {
											if( $resultspage > 3 ) {
												if( $total_pages == $resultspage ) {
													$start_page = $total_pages - 4;
												} elseif( $total_pages - $resultspage < 2 ) {
													$start_page = $total_pages - 5 + ( $total_pages - $resultspage );
												} else {
													$start_page = $resultspage - 2;
												}
											}
											if( $start_page + 4 < $total_pages ) {
												$end_page = $start_page + 5;
											}
										}

										if( $total_pages > 1 ) {
											echo 'Pages: ';
												for( $i = $start_page; $i < $end_page; $i++ ) {
													if( $i != $resultspage ) {
														$link = '?resultspage='. $i . $page_query;
														echo '<a href="'. $link .'"><span>'. $i .'</span></a>';
													} else {
														echo '<span>'. $i .'</span>';
													}
												}
											echo '</ul>';
										}
									echo '</div>';
								} else {
									/**
									 * no products found from query
									 */
									$results .= '<div class="col-xs-12 product-search__no-results">
										<h2>No Products Found</h2>
										<p>We\'re sorry, but no products matching your search criteria were found. You may find the following suggestions helpful in finding the right product.</p>
										<ul>
											<li>Try more general terms</li>
											<li>Check your search query for typos</li>
											<li>Select fewer filtering options</li>
										</ul>
									</div>';
									echo $results;
								}
							echo '</section>';
						} else {
							echo '<section class="product-search">
								<div class="col-xs-12 product-search__no-results">
									<h2>No Products Found</h2>
									<p>We\'re sorry, but no products matching your search criteria were found. You may find the following suggestions helpful in finding the right product.</p>
									<ul>
										<li>Try more general terms</li>
										<li>Check your search query for typos</li>
										<li>Select fewer filtering options</li>
									</ul>
								</div>
							</section>';
						}
					} else {
						/**
						 * landing page
						 * show grid of 8 product types (
						 * image is pulled from last post of that particular type
						 * cache each type query for 15 minutes
						 */

						echo '<section class="product-search">
							<div class="col-xs-12 product-search__results">
								<div class="product-search__results__wrap">
									<p class="product-search__form__toggle">
										<a href="#" target="_self" class="btn btn--red product-search__form-toggle">Toggle Search Filters &raquo;</a>
									</p>
									<p class="alignright"><a href="#" target="_self" class="product-search__form-toggle">Filter Results &raquo;</a></p>
								</div>
							</div>';

							$query = 'SELECT DISTINCT meta_value FROM wp_postmeta WHERE meta_key = "_vn_apparel_type" order by meta_value';
							$entries_type = $wpdb->get_results( $query );

							foreach( $entries_type as $type ) {
								$type_pieces		= explode( '::', $type->meta_value );
								$type_key			= $type_pieces[0];
								$type_name			= $type_pieces[1];
								$type_obj_name		= 'categoryboxes_' . $type_key;

								// transient caching
								if ( false === ( $recent_apparel = get_transient( $type_obj_name ) ) ) {
									$args = array(
										'post_type'			=> 'apparel-reviews',
										'posts_per_page'	=> 1,
										'post_status'		=> 'publish',
										'orderby'			=> 'meta_value',
										'order'				=> 'ASC',
										'meta_key'			=> '_vn_apparel_type',
										'meta_value'		=> $type->meta_value
									);
									$recent_apparel = new WP_Query( $args );
									set_transient( $type_obj_name, $recent_apparel, 15 * MINUTE_IN_SECONDS );
								}

								if( $recent_apparel->have_posts() ) {
									while( $recent_apparel->have_posts() ) {
										$recent_apparel->the_post();

										echo '<div class="col-xs-4 product-search__category">
											<a href="?resultspage=1&type[]='. $type_key . '&gender[]=all&brand[]=all&apparel_search=" target="_self" title="'. $type_name .'">
												'. get_the_post_thumbnail( get_the_ID(), 'bike-search', array( 'class' => 'product-search__category__thumbnail' ) ) .'
												<div class="product-search__category__title">Browse '. $type_name .'</div>
											</a>
										</div>';

									}
								}
								wp_reset_postdata();

							}

						echo '</section>';

					}
				?>


			</section>


			<?php get_sidebar( 'right' ); ?>

		</section>
		<?php

			/**
			 * 970x250 ad
			 */
		?>
		<div class="row content__section">
			<div class="advert advert_xs_300x250 advert_sm_728x90 advert_md_970x250">
				<div class="advert__wrap">
					<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'middle' ) : ''; ?>
				</div>
			</div>
		</div>

	</div>
</section>
<?php } else{
?>


<section id="content" class="content template--product-search">
	<div class="container content__container">
		<header class="row">
			<div class="col-xs-12">
				<h1 class="content__title"><?php the_title(); ?></h1>
				<ol class="article__bread-crumb">
					<li><a href="<?php echo site_url() ?>" target="_self">Home</a></li>
					&raquo;
					<?php if( !isset( $_REQUEST['resultspage'] ) ): ?>
						<li><?php the_title(); ?></li>
					<?php else: ?>
						<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
					<?php endif; ?>
					<?php if( isset( $_REQUEST['type'] ) ): ?>
						<?php
							if( 1 == count($_REQUEST['type']) ) {
								$type = $_REQUEST['type'][0];
								printf(
									'&raquo; <li><a href="%s?resultspage=1&type[]=%s&gender[]=all&brand[]=all&apparel_search=">%s</a></li>',
									get_permalink(),
									$type,
									$apparel_choices[$type],
									$type
								);
							}
						?>
						&raquo;
						<li>Search Results</li>
					<?php endif; ?>
				</ol>
			</div>
		</header>
		<section class="row content__flex">
			<section class="content__section content__flex_position_first product-search">
				<?php
					/**
					 * apparel model text search
					 */
				?>
				<div class="product-search__wrap" id="product-search-form">

					<h3 class="product-search__heading product-search__heading_name_brand">Search By Brand</h3>

					<form action="<?php echo get_permalink(); ?>" method="GET" name="text_product_search" id="product-search-text" class="product-search__form product-search__form_name_text">
						<input type="hidden" value="1" name="resultspage">
						<input type="hidden" value="all" name="type[]">
						<input type="hidden" value="all" name="gender[]">
						<input type="hidden" value="all" name="brand[]">

						<fieldset class="product-search__form__fieldset product-search__form__fieldset_name_search">
							<div class="product-search__form__option">
								<label for="product-search-search" class="screen-reader-text">Search Apparel</label>
								<input type="text" name="apparel_search" id="product-search-search" value="<?php echo $apparel_search; ?>" placeholder="Search brands" class="product-search__form__input">
								<button class="btn btn--red"><span class="fa fa-search"></span></button>
							</div>
							<a href="#" target="_self" class="product-search__form-toggle"><span class="fa fa-times"></span></a>
						</fieldset>
					</form>

					<h3 class="product-search__heading product-search__heading_name_filter">Search By Filter</h3>

					<?php
						/**
						 * apparel multi-option search
						 */
					?>
					<form action="<?php echo get_permalink(); ?>" method="GET" name="multifacet_product_search" id="product-search-multifacet" class="product-search__form product-search__form_name_multifacet">
						<input type="hidden" value="1" name="resultspage">

						<?php
							/**
							 * apparel type
							 * helmet, jersey, shoe, bib, outerwear, tool, misc
							 * these types are hard-coded
							 */
						?>

						<fieldset class="product-search__form__fieldset">
							<legend class="product-search__form__legend">
								<a href="#product-search-type-filters" target="_self" title="Toggle Type Filters" class="product-search__filter-toggle">
									ACCESSORIES <span class="fa fa-caret-down"></span>
								</a>
							</legend>
							<div id="product-search-type-filters" class="product-search__form__group">
								<?php
									foreach( $apparel_choices as $key => $value ) {
										$checked = (in_array( $key, $apparel_type ) ? 'checked' : '');

										if( $key == 'all' && ( !isset( $apparel_type[0] ) || $apparel_type[0] == 'all' ) ) {
											$checked = 'checked';
										}

										echo '<div class="product-search__form__option">
											<input '. $checked .' type="checkbox" name="type[]" id="type-'. $key .'" value="'. $key .'" class="product-search__form__checkbox">
											<label for="type-'. $key .'" class="product-search__form__label">'. $value .'</label>
										</div>';
									}
								?>
							</div>
						</fieldset>


						<?php
							/**
							 * gender
							 */
						?>
						<fieldset class="product-search__form__fieldset">
							<legend class="product-search__form__legend">
								<a href="#product-search-gender-filters" target="_self" title="Toggle Gender Filters" class="product-search__filter-toggle">
									GENDER <span class="fa fa-caret-down"></span>
								</a>
							</legend>
							<div id="product-search-gender-filters" class="product-search__form__group">
								<div class="product-search__form__option">
									<input <?php echo ( !isset( $gender[0] ) || $gender[0] == 'all' ) ? 'checked' : ''; ?> type="checkbox" name="gender[]" id="gender-all" value="all" class="product-search__form__checkbox">
									<label for="gender-all" class="product-search__form__label">All</label>
								</div>

								<?php
									$men_checked	= in_array( 'm', $gender ) ? 'checked' : '';
									$women_checked	= in_array( 'f', $gender ) ? 'checked' : '';

									echo '<div class="product-search__form__option">
										<input '. $men_checked .' type="checkbox" name="gender[]" id="gender-men" value="m" class="product-search__form__checkbox">
										<label for="gender-men" class="product-search__form__label">Men</label>
									</div>
									<div class="product-search__form__option">
										<input '. $women_checked .' type="checkbox" name="gender[]" id="gender-women" value="f" class="product-search__form__checkbox">
										<label for="gender-women" class="product-search__form__label">Women</label>
									</div>';
								?>

							</div>
						</fieldset>

						<?php
							/**
							 * brand
							 * this list will populate as more products are added
							 */
							$query = 'SELECT DISTINCT r.meta_value FROM wp_postmeta r, wp_posts s WHERE r.meta_key = "_vn_apparel_brand" AND s.ID = r.post_id AND s.post_status = "publish" order by r.meta_value';
							$entries_brand = $wpdb->get_results( $query );
						?>
						<fieldset class="product-search__form__fieldset">
							<legend class="product-search__form__legend">
								<a href="#product-search-brand-filters" target="_self" title="Toggle Brand Filters" class="product-search__filter-toggle">
									BRAND <span class="fa fa-caret-down"></span>
								</a>
							</legend>
							<div id="product-search-brand-filters" class="product-search__form__group">
								<div class="product-search__form__option">
									<input <?php echo ( !isset( $apparel_brand[0] ) || $apparel_brand[0] == 'all' ) ? 'checked' : ''; ?> type="checkbox" name="brand[]" id="brand-all" value="all" class="product-search__form__checkbox">
									<label for="brand-all" class="product-search__form__label">All</label>
								</div>

								<?php
									$brand_count = 1;
									foreach( $entries_brand as $entry_brand ) {
										$dashed_name	= str_replace( array( ' ', '\'' ), '_', $entry_brand->meta_value );
										$checked		= in_array( $dashed_name, $apparel_brand ) ? 'checked' : '';

										/**
										 * show only 4 brands + "all", hide the rest under a "see all brands" toggle link
										 */
										// if( $brand_count == 5 ) {
										// 	echo '<div id="product-search__brand__hidden" class="collapse">';
										// }
										if( $brand_count > 4 ) {
											$option_hide_class = ' product-search__form__option__hidden';
										} else {
											$option_hide_class = '';
										}

										echo '<div class="product-search__form__option'. $option_hide_class .'">
											<input '. $checked .' type="checkbox" name="brand[]" id="brand-'. $dashed_name .'" value="'. $dashed_name .'" class="product-search__form__checkbox">
											<label for="brand-'. $dashed_name .'" class="product-search__form__label">'. $entry_brand->meta_value .'</label>
										</div>';

										$brand_count++;
									}
									if( $brand_count > 4 ) {
										// echo '</div>
										echo '<div class="product-search__more"><a href="javascript:;" class="show-hidden-toggle">See All Brands</a></div>';
									}
								?>
							</div>
						</fieldset>

						<fieldset class="product-search__form__fieldset product-search__form__fieldset_name_submit">
							<input type="hidden" value="" name="apparel_search">
							<input type="submit" value="Filter Results" class="btn btn--red">
						</fieldset>

					</form>
				</div>
			</section>

			<section class="content__section content__flex_position_second">

				<?php
					/**
					 * a search was made
					 */
					if( isset( $_REQUEST['resultspage'] ) ) {
						/**
						 * set up the main query
						 * only search for products that were added within the past 36 months
						 */
						$query = 'SELECT r.ID, t.meta_value FROM wp_posts r, wp_postmeta t WHERE r.post_type = "apparel-reviews" AND r.post_status = "publish" AND r.post_date >= DATE_SUB(NOW(), INTERVAL 36 MONTH) ';


						/**
						 * type filter was modified
						 * get post_id's for as many types selected
						 */

						if( $apparel_type[0] != 'all' ) {
							$apparel_type_or	= '';
							$apparel_type_count	= count( $apparel_type );

							for( $i = 0; $i < $apparel_type_count; $i++ ) {
								$apparel_type_or .= 'meta_value like "%' . $apparel_type[$i] . '%"';
								if( $i != ( $apparel_type_count -1 ) ) {
									$apparel_type_or .= ' OR ';
								}
							}
							$apparel_type_sql = 'SELECT post_id FROM wp_postmeta WHERE meta_key = "_vn_apparel_type" AND ('. $apparel_type_or .')';
						}


						/**
						 * gender filter was modified
						 * get post_id's for as many gender types selected
						 */
						if( $gender[0] != 'all' ) {
							$gender_or		= '';
							$gender_count	= count( $gender );

							for( $i = 0; $i < $gender_count; $i++ ) {
								$gender_or .= 'meta_value like "%' . $gender[$i] . '%"';
								if( $i != ( $gender_count -1 ) ) {
									$gender_or .= ' OR ';
								}
							}
							$gender_sql = 'SELECT post_id FROM wp_postmeta WHERE ( meta_key = "_vn_apparel_gender" AND '. $gender_or . 'OR meta_key = "_vn_apparel_gender" AND meta_value like "%u%" )';
						}


						/**
						 * brand filter was modified
						 * get post_id's for as many brands selected
						 */
						if( $apparel_brand[0] != 'all' ) {
							$apparel_brand_or		= '';
							$apparel_brand_count	= count( $apparel_brand );

							for( $i = 0; $i < $apparel_brand_count; $i++ ) {
								$apparel_brand_or .= 'meta_value like "%' . str_replace( ' ', '_', $apparel_brand[$i] ) . '%"';
								if( $i != ( $apparel_brand_count -1 ) ) {
									$apparel_brand_or .= ' OR ';
								}
							}
							$apparel_brand_sql = 'SELECT post_id FROM wp_postmeta WHERE meta_key = "_vn_apparel_brand" AND '. $apparel_brand_or;

						}


						/**
						 * user typed in product brand into text input field
						 * get post_id's where brand matches
						 */
						if( $apparel_search != '' ) {
							$apparel_search_sql = 'SELECT post_id FROM wp_postmeta WHERE meta_key = "_vn_apparel_brand" AND meta_value LIKE "%'. $apparel_search .'%"';
						}


						/**
						 * build onto main query based on selections the user made
						 * logic is set up to be top-down to cover all search scenarios
						 */
						if( $apparel_type_sql != "" ) {
							$query .= ' AND r.ID IN ( '. $apparel_type_sql;

							if( $gender_sql != "" ) {
								$query .= ' AND post_id IN ( '. $gender_sql . ')';
							}
							if( $apparel_brand_sql != "" ) {
								$query .= ' AND post_id IN ( '. $apparel_brand_sql . ')';
							}
							$query .= ')';

						} elseif( $gender_sql != "" ) {
							$query .= ' AND ID IN ( '. $gender_sql;

							if( $apparel_brand_sql != "" ) {
								$query .= ' AND post_id IN ( '. $apparel_brand_sql . ')';
							}
							$query .= ')';

						} elseif( $apparel_brand_sql != "" ) {
							$query .= ' AND r.ID IN ( '. $apparel_brand_sql . ')';

						} elseif( $apparel_search_sql != "" ) {
							$query .= ' AND r.ID IN ( '. $apparel_search_sql . ')';
						}


						$offset = 0;
						if( $resultspage > 1 ) {
							/**
							 * show 12 results per page
							 */
							$offset = ( $resultspage - 1 ) * 12; // (page 2 - 1)*12 = offset of 12
						}
						$query_sans_offset = $query . ' AND t.post_id = r.ID AND t.meta_key = "_vn_rating" ORDER BY post_date DESC';
						$query .= ' AND t.post_id = r.ID AND t.meta_key = "_vn_rating" ORDER BY post_date DESC LIMIT 12 OFFSET '. $offset;

						/**
						 * to remember the url (history)
						 */
						$page_query = $type_url_string = $gender_url_string = $brand_url_string = '';

						/**
						 * type
						 */
						if( $apparel_type[0] != 'all' ) {
							foreach( $apparel_type as $type ) {
								$page_query .= '&type[]='. $type;
								$type_url_string .= '&type[]='. $type;
							}
						} else {
							$page_query .= '&type[]=all';
							$type_url_string .= '&type[]=all';
						}

						/**
						 * gender
						 */
						if( $gender[0] != 'all' ) {
							foreach( $gender as $brand ) {
								$page_query .= '&gender[]='. $brand;
								$gender_url_string .= '&gender[]='. $brand;
							}
						} else {
							$page_query .= '&gender[]=all';
							$gender_url_string .= '&gender[]=all';
						}


						/**
						 * brand
						 */
						if( $apparel_brand[0] != 'all' ) {
							foreach( $apparel_brand as $brand ) {
								$page_query .= '&brand[]='. $brand;
								$brand_url_string .= '&brand[]='. $brand;
							}
						} else {
							$page_query .= '&brand[]=all';
							$brand_url_string .= '&brand[]=all';
						}

						/**
						 * brand search (text input)
						 */
						if( $apparel_search ) {
							$page_query .= '&apparel_search='. $apparel_search;
						} else {
							$page_query .= '&apparel_search=';
						}


						if( $query != '' ) {
							$adcount				= 0;
							$entries				= $wpdb->get_results( $query );
							$entries_sans_offset	= $wpdb->get_results( $query_sans_offset );
							$entries_count			= count( $entries_sans_offset );

							$results = '<section class="product-search">
								<div class="col-xs-12 product-search__results">
									<div class="product-search__results__wrap">
										<p class="product-search__form__toggle">
											<a href="#" target="_self" class="btn btn--red product-search__form-toggle">Toggle Search Filters &raquo;</a>
										</p>
										<p class="alignleft">Products Found: '. $entries_count .'</p>
										<p class="alignright"><a href="#" target="_self" class="product-search__form-toggle">Filter Results &raquo;</a></p>
									</div>
								</div>';

								if( $entries_count > 0 ) {
									foreach( $entries as $entry ) {
										$image			= get_the_post_thumbnail( $entry->ID, 'bike-search', array( 'class' => 'article__thumbnail' ) );
										$type			= get_post_meta( $entry->ID, '_vn_apparel_type', 1 );
										$type_pieces	= explode( '::', $type );
										$type_key		= ( isset( $type_pieces[0] ) ? $type_pieces[0] : '' );
										$type_name		= ( isset( $type_pieces[1] ) ? $type_pieces[1] : '' );
										$msrp			= number_format( get_post_meta( $entry->ID, '_vn_apparel_msrp', 1 ) );
										$gender			= get_post_meta( $entry->ID, '_vn_apparel_gender', 1 );
										$rating			= get_post_meta( $entry->ID, '_vn_rating', 1 );

										/**
										 * display result: image, name, rating, gender, price
										 */
										$results .= '<article class="col-xs-6 col-md-4 article article_type_search-result">
											<a href="'. site_url( '?p='. $entry->ID .'&resultspage='. $resultspage . $type_url_string . $gender_url_string . $brand_url_string . '&apparel_search='. $apparel_search ) .'" target="_self" class="article__permalink" title="'. get_the_title( $entry->ID ) .'">
												'. $image .'
												<p class="article__title">'. get_the_title( $entry->ID ) .'</p>';

												if( $rating != '' ) {
													$results .= '<p class="article__rating">Rating <strong>'. $rating .'/10</strong></p>';
												}
												if( $gender == 'm' ) {
													$gender_display = 'Men';
												} elseif( $gender == 'f' ) {
													$gender_display = 'Women';
												} else {
													$gender_display = '';
												}
												if( $gender_display != '' ) {
													$results .= '<p class="article__gender">Gender <strong>'. $gender_display .'</strong></p>';
												}


												$results .= '<p class="article__msrp"><strong>$'. $msrp .'</strong></p>
											</a>
										</article>';


										/**
										 * show 300x250 after 6th result -- only on mobile
										 */
										if( $adcount == 5 && is_mobile() ) {
											$results .= '<div class="col-xs-12 visible-xs advert advert_xs_300x250 advert_location_middle">
												<div class="advert__wrap">';
													echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'side-middle' ) : '';
												$results .= '</div>
											</div>';
										}

										$adcount++;

									}
									echo $results;


									/**
									 * pagination
									 */
									echo '<div class="col-xs-12 page-links">';
										$query_count_comma	= number_format( $entries_count );
										$num_per_page		= 12;
										$total_pages		= ceil( $entries_count / $num_per_page );
										$start_page			= 1;
										$end_page			= $total_pages + 1;

										if( $total_pages > 5  ) {
											if( $resultspage > 3 ) {
												if( $total_pages == $resultspage ) {
													$start_page = $total_pages - 4;
												} elseif( $total_pages - $resultspage < 2 ) {
													$start_page = $total_pages - 5 + ( $total_pages - $resultspage );
												} else {
													$start_page = $resultspage - 2;
												}
											}
											if( $start_page + 4 < $total_pages ) {
												$end_page = $start_page + 5;
											}
										}

										if( $total_pages > 1 ) {
											echo 'Pages: ';
												for( $i = $start_page; $i < $end_page; $i++ ) {
													if( $i != $resultspage ) {
														$link = '?resultspage='. $i . $page_query;
														echo '<a href="'. $link .'"><span>'. $i .'</span></a>';
													} else {
														echo '<span>'. $i .'</span>';
													}
												}
											echo '</ul>';
										}
									echo '</div>';
								} else {
									/**
									 * no products found from query
									 */
									$results .= '<div class="col-xs-12 product-search__no-results">
										<h2>No Products Found</h2>
										<p>We\'re sorry, but no products matching your search criteria were found. You may find the following suggestions helpful in finding the right product.</p>
										<ul>
											<li>Try more general terms</li>
											<li>Check your search query for typos</li>
											<li>Select fewer filtering options</li>
										</ul>
									</div>';
									echo $results;
								}
							echo '</section>';
						} else {
							echo '<section class="product-search">
								<div class="col-xs-12 product-search__no-results">
									<h2>No Products Found</h2>
									<p>We\'re sorry, but no products matching your search criteria were found. You may find the following suggestions helpful in finding the right product.</p>
									<ul>
										<li>Try more general terms</li>
										<li>Check your search query for typos</li>
										<li>Select fewer filtering options</li>
									</ul>
								</div>
							</section>';
						}
					} else {
						/**
						 * landing page
						 * show grid of 8 product types (
						 * image is pulled from last post of that particular type
						 * cache each type query for 15 minutes
						 */

						echo '<section class="product-search">
							<div class="col-xs-12 product-search__results">
								<div class="product-search__results__wrap">
									<p class="product-search__form__toggle">
										<a href="#" target="_self" class="btn btn--red product-search__form-toggle">Toggle Search Filters &raquo;</a>
									</p>
									<p class="alignright"><a href="#" target="_self" class="product-search__form-toggle">Filter Results &raquo;</a></p>
								</div>
							</div>';

							$query = 'SELECT DISTINCT meta_value FROM wp_postmeta WHERE meta_key = "_vn_apparel_type" order by meta_value';
							$entries_type = $wpdb->get_results( $query );

							foreach( $entries_type as $type ) {
								$type_pieces		= explode( '::', $type->meta_value );
								$type_key			= $type_pieces[0];
								$type_name			= $type_pieces[1];
								$type_obj_name		= 'categoryboxes_' . $type_key;

								// transient caching
								if ( false === ( $recent_apparel = get_transient( $type_obj_name ) ) ) {
									$args = array(
										'post_type'			=> 'apparel-reviews',
										'posts_per_page'	=> 1,
										'post_status'		=> 'publish',
										'orderby'			=> 'meta_value',
										'order'				=> 'ASC',
										'meta_key'			=> '_vn_apparel_type',
										'meta_value'		=> $type->meta_value
									);
									$recent_apparel = new WP_Query( $args );
									set_transient( $type_obj_name, $recent_apparel, 15 * MINUTE_IN_SECONDS );
								}

								if( $recent_apparel->have_posts() ) {
									while( $recent_apparel->have_posts() ) {
										$recent_apparel->the_post();

										echo '<div class="col-xs-4 product-search__category">
											<a href="?resultspage=1&type[]='. $type_key . '&gender[]=all&brand[]=all&apparel_search=" target="_self" title="'. $type_name .'">
												'. get_the_post_thumbnail( get_the_ID(), 'bike-search', array( 'class' => 'product-search__category__thumbnail' ) ) .'
												<div class="product-search__category__title">Browse '. $type_name .'</div>
											</a>
										</div>';

									}
								}
								wp_reset_postdata();

							}

						echo '</section>';

						/**
						 * two special column
						 */
						if( get_post_meta( get_the_ID(), '_vn_2special_category', 1 ) ) {
							include( locate_template( 'template-parts/content-two-special.php' ) );
						}
					}
				?>


			</section>


			<?php get_sidebar( 'right' ); ?>

		</section>
		<?php
			/**
			 * four column
			 */
			if( get_post_meta( get_the_ID(), '_vn_4top_category', 1 ) != '' ) {
				include( locate_template( 'template-parts/content-four-top.php' ) );
			}


			if( !isset( $_REQUEST['resultspage'] ) ) {
				/**
				 * two column - featured articles (rich media?)
				 */
				if( get_post_meta( get_the_ID(), '_vn_featured_articles', 1 ) != '' ) {
					include( locate_template( 'template-parts/content-two.php' ) );
				}
			}

			/**
			 * 970x250 ad
			 */
		?>
		<div class="row content__section">
			<div class="advert advert_xs_300x250 advert_sm_728x90 advert_md_970x250">
				<div class="advert__wrap">
					<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'middle' ) : ''; ?>
				</div>
			</div>
		</div>


		<?php
			if( !isset( $_REQUEST['resultspage'] ) ) {

				/**
				 * five column
				 */
				if( get_post_meta( get_the_ID(), '_vn_5_category', 1 ) != '' ) {
					include( locate_template( 'template-parts/content-five.php' ) );
				}


				/**
				 * two by two
				 */
				if( get_post_meta( get_the_ID(), '_vn_2by2_category', 1 ) != '' ) {
					include( locate_template( 'template-parts/content-two-by-two.php' ) );
				}



				/**
				 * four column
				 */
				if( get_post_meta( get_the_ID(), '_vn_4bottom_category', 1 ) != '' ) {
					include( locate_template( 'template-parts/content-four-bottom.php' ) );
				}
			}

		?>

	</div>
</section>

<?php
}//end not guide section
	/**
	 * ad
	 */
?>
<section class="advert advert_xs_300x250 advert_sm_728x90 advert_location_bottom ">
	<div class="advert__wrap">
		<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom' ) : ''; ?>
	</div>
</section>

<?php get_footer(); ?>
