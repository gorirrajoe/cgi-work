<?php
/**
 * Template Name: Latest and Popular
 *
 * @package velonews
 */

get_header(); ?>

<section id="content" class="content archive template--blogroll">
	<div class="container content__container">

		<?php
			/**
			 * meta option: popular
			 * show most popular articles
			 */
			if( get_post_meta( get_the_ID(), '_vn_latest_or_popular', 1 ) == 'popular' ) { ?>
				<header class="row">
					<div class="col-xs-12">
						<?php
							/**
							 * breadcrumb
							 */
							get_breadcrumb();
						?>
					</div>


					<div class="col-xs-12">
						<?php echo '<h1 class="content__title">Most Popular Articles</h1>'; ?>
					</div>
				</header>


				<div class="row">
					<section class="col-xs-12 col-sm-7 col-md-8 archive">
						<?php if( is_active_sidebar( 'inpage-hotstories' ) ) {
							dynamic_sidebar( 'inpage-hotstories' );
						} ?>
					</section>

					<?php get_sidebar( 'right' ); ?>

				</div>

			<?php } elseif( get_post_meta( get_the_ID(), '_vn_latest_or_popular', 1 ) == 'latest' ) {
				/**
				 * meta option: latest
				 */ ?>
				<header class="row">
					<div class="col-xs-12">
						<?php
							/**
							 * breadcrumb
							 */
							get_breadcrumb();
						?>
					</div>


					<div class="col-xs-12">
						<?php
							$page = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

							echo '<h1 class="content__title">All Recent Articles: Page '. $page .'</h1>';
						?>
					</div>
				</header>


				<div class="row">
					<section class="col-xs-12 col-sm-7 col-md-8 archive">
						<?php
							$args = array(
								'posts_per_page'			=> 10,
								'post_status'				=> 'publish',
								'post_type'					=> 'any',
								'has_password'				=> false,
								'paged'						=> $page,
								'ignore_sticky_posts'		=> true,
								'no_found_rows'				=> false,
								'update_post_meta_cache'	=> false
							);
							$latest_query = new WP_Query( $args );

							if( $latest_query->have_posts() ) {
								$count		= 1;

								while( $latest_query->have_posts() ) {
									$latest_query->the_post();

									get_template_part( 'template-parts/content', 'archive' );

									if( $count == 3 ) { ?>

										<div class="advert advert_xs_300x250 advert_md_728x90 advert_location_inline">
											<div class="advert__wrap">
												<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'middle-archive' ) : ''; ?>
											</div>
										</div><!-- ad unit -->

									<?php
									}
									$count++;
								}

								echo get_paginated_links( $latest_query );

							} else {

								get_template_part( 'template-parts/content', 'none' );

							}
							wp_reset_postdata();
						?>
					</section>

					<?php get_sidebar( 'right' ); ?>

				</div>
			<?php }
		?>
	</div>
</section>


<?php
	/**
	 * ad
	 */
?>
<section class="advert advert_xs_300x250 advert_sm_728x90 advert_location_bottom ">
	<div class="advert__wrap">
		<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad( 'bottom' ) : ''; ?>
	</div>
</section>

<?php get_footer();
