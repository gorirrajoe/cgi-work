<?php
	/**
	 * Template Name: Gift Guide
	 * The template for displaying the current gift guide.
	 * Made for seo/pretty url purposes
	 */

	get_header();
?>

<section id="content" class="content">

	<?php
		$active_gift_guide = get_post_meta( get_the_ID(), '_vn_gift_guide_cat', 1 );

		if( $active_gift_guide != '' ) {

			get_template_part( 'template-parts/content', $active_gift_guide . '-cat' );

		}
	?>

</section>

<?php get_footer(); ?>
