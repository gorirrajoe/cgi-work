<?php
/**
 * The template for displaying search dropdown from nav.
 *
 * @package velonews
 */

$search_type	= function_exists('cgi_bikes_get_option') ? cgi_bikes_get_option( 'search_type' ) : 'wordpress';

if ( $search_type == 'bing' ) {
	$search_text		= isset( $_REQUEST['q'] ) ? htmlentities( stripslashes( strip_tags( $_REQUEST['q'] ) ), ENT_QUOTES | ENT_HTML5, 'UTF-8' ) : '';
	$search_name_value	= 'q';
} else {
	$search_text		= get_search_query();
	$search_name_value	= 's';
}
?>

<form role="search" action="<?php echo site_url( '/' ); ?>" method="GET" class="clearfix site-search__form">

	<div class="site-search__form__wrap">
		<label for="search-field" class="screen-reader-text">Search VeloNews.com</label>
				<input type="text" name="<?php echo $search_name_value; ?>" id="search-field" class="site-search__form__field" value="<?php echo $search_text; ?>">
	</div>
	<button type="submit" id="search-submit" class="site-search__form__btn">
		<span class="screen-reader-text">Search</span>
		<span>Search</span>

	</button>
</form>

