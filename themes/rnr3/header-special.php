<?php
	/**
	 * header for special templates like tourpass
	 * usage: series pages
	 */
	$prefix = '_rnr3_';

	$market = get_market2();
	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';

	/**
	 * global variables
	 */
	$twitter				= rnr_get_option( 'rnrgv_twitter');
	$instagram				= rnr_get_option( 'rnrgv_instagram');
	$pinterest				= rnr_get_option( 'rnrgv_pinterest');
	$youtube				= rnr_get_option( 'rnrgv_youtube');
	$header_code			= rnr_get_option( 'rnrgv_header');
	$google_analytics		= rnr_get_option( 'rnrgv_google_analytics');
	$gtm					= rnr_get_option( 'rnrgv_gtm');
	$optimizely				= rnr_get_option( 'rnrgv_optimizely');
	$twenty_years_enabled	= ( rnr_get_option( 'rnrgv_20_enabled' ) == 'on' ) ? 'on' : 'off';

	$twenty_years_exception_list	= ( rnr_get_option( 'rnrgv_20year_exception' ) != '' ) ? rnr_get_option( 'rnrgv_20year_exception' ) : '';
	$twenty_years_exception_events	= strpos( $twenty_years_exception_list, ',' ) ? explode( ',', $twenty_years_exception_list ) : array( $twenty_years_exception_list );

	$youtube_vid			= get_post_meta( get_the_ID(), '_rnr3_youtube', true );
	$page_location			= rnr3_home_or_sub( $market, get_the_ID() );

?><!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
	<head>

		<?php if( $optimizely != '' ) {
			echo stripslashes( $optimizely );
		} ?>

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

		<?php rnr3_display_favicon();

		if( $google_analytics != '' ) { ?>
			<script type="text/javascript">
				var _gaq = _gaq || [];
				var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
				_gaq.push(
					['_require', 'inpage_linkid', pluginUrl],
					['_setAccount', '<?php echo $google_analytics;?>'],
					['_setDomainName', 'runrocknroll.com'],
					['_setAllowLinker', true],
					['_trackPageview']
				);
				(function() {
					var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
				})();
			</script>
		<?php }

		wp_head();

		if( $event_info != '' ) {

			if($event_info->header_settings != '') {
				echo $event_info->header_settings;
			}

		}

		// setting big hero image background
		$header_image = '';

		// if you set a custom hero image for a page, use that
		if( is_singular( 'partner' ) ) {

			$partners_id = get_id_by_slug( 'partners' );
			$header_image = get_post_meta( $partners_id, '_rnr3_special_header_img', true);

		} else {

			if( get_post_meta( get_the_ID(), '_rnr3_special_header_img', true) != '' ) {
				$header_image = get_post_meta( get_the_ID(), '_rnr3_special_header_img', true);
			} elseif( get_post_meta( get_the_ID(), '_rnr3_unique_header_img', 1 ) != '' ) {
				$header_image = get_post_meta( get_the_ID(), '_rnr3_unique_header_img', 1 );
			} else {
				if( $event_info != '' ) {
					$header_image = $event_info->header_image;
				}
			}

		}

		if( $header_image != '' ) { ?>
			<style>
				#hero {
					background:url("<?php echo $header_image; ?>") no-repeat scroll 50% 0 rgba(0, 0, 0, 0);
				}
				#new-hero {
					background:url("<?php echo $header_image; ?>") no-repeat scroll 50% 0 rgba(0, 0, 0, 0);
				}
				<?php if( $youtube_vid != '' ) { ?>
					#hero-video {
						background:linear-gradient(rgba(0,0,0,.8), rgba(0, 0, 0, 0.8)), url("<?php echo $header_image; ?>") no-repeat scroll 50% 0 ;
					}
				<?php } ?>
			</style>
		<?php }

		if( is_front_page() && $market == 'humana' ) {

			$dfp_market = str_replace( '-', '_', $market ); ?>
			<script>
				(function() {
				var useSSL = 'https:' == document.location.protocol;
				var src = (useSSL ? 'https:' : 'http:') +
				'//www.googletagservices.com/tag/js/gpt.js';
				document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
				})();
			</script>
			<script>
				slot_600_600_1=googletag.defineSlot('/8221/runrocknroll/sm-<?php echo $dfp_market; ?>', [600, 600], 'div-600_600_1').addService(googletag.pubads()).setTargeting('tile', '1');
				slot_600_600_2=googletag.defineSlot('/8221/runrocknroll/sm-<?php echo $dfp_market; ?>', [600, 600], 'div-600_600_2').addService(googletag.pubads()).setTargeting('tile', '2');
				slot_600_600_3=googletag.defineSlot('/8221/runrocknroll/sm-<?php echo $dfp_market; ?>', [600, 600], 'div-600_600_3').addService(googletag.pubads()).setTargeting('tile', '3');
				slot_600_600_4=googletag.defineSlot('/8221/runrocknroll/sm-<?php echo $dfp_market; ?>', [600, 600], 'div-600_600_4').addService(googletag.pubads()).setTargeting('tile', '4');
				googletag.pubads().enableAsyncRendering();
				googletag.pubads().enableSingleRequest();
				googletag.enableServices();
			</script>

		<?php } ?>
	</head>


	<body id="rnr-series" <?php body_class( array( 'market-'. $market, 'minimal-nav' ) ); ?>>

		<?php if( $gtm != '' ) {
			echo stripslashes( $gtm );
		} ?>

		<div id="container">

			<!-- alert banner -->
			<?php if( $event_info->alert_msg != '' ) {

				if( $qt_lang['enabled'] == 1 ) {
					$alert_msg = qtrans_split( $event_info->alert_msg );
					$alert_msg = $alert_msg[$qt_lang['lang']];
				} else {
					$alert_msg = $event_info->alert_msg;
				} ?>

				<section id="alert">
					<section class="wrapper">
						<p class="mid2"><?php echo $alert_msg; ?></p>
					</section>
				</section>

			<?php } ?>


			<!-- begin main header -->
			<header>
				<section class="wrapper">
					<div id="getgoing" class="clearfix">

						<?php if( $market != 'virtual-run' ) { ?>
							<ul id="reg">
								<li><a href="<?php echo network_home_url( '/#findrace' ); ?>" class="register"><?php echo $txt_find_a_race; ?></a></li>
							</ul>
						<?php } ?>

					</div>

					<?php
						/**
						 * get appropriate logo, 20 years or normal?
						 */
						get_logo( $twenty_years_enabled, $market, $twenty_years_exception_events );
					?>

					<nav>

						<div class="hamburger">
							<span></span>
							<span></span>
							<span></span>
							<span class="hamburger__label">Menu</span>
						</div>

						<?php
							$global_nav = rnr3_get_global_nav( $qt_lang['lang'] );
							echo $global_nav;
						?>
					</nav>

				</section>

				<?php if( is_page_template( 'travel-series.php' ) ) { ?>

					<div class="event_nav_series">
						<nav id="subnav">
							<section class="wrapper">

								<div class="subnav_social">
									<?php /* social ribbon -- show on desktop */ ?>
									<section id="ribbon">
										<?php echo $event_info->twitter_hashtag != '' ? '<div class="hash">'.$event_info->twitter_hashtag . '  <span class="pop">/</span></div>': ''; ?>
										<div class="twitter"><a href="<?php echo $twitter; ?>" target="_blank"><span class="icon-twitter"></span></a></div>
										<div class="facebook"><a href="<?php echo $event_info->facebook_page; ?>" target="_blank"><span class="icon-facebook"></span></a></div>
										<div class="instagram"><a href="<?php echo $instagram; ?>" target="_blank"><span class="icon-instagram"></span></a></div>

									</section>
								</div>

							</section>
						</nav>

					</div>

				<?php } ?>
			</header>


			<?php
				$disable_hero = get_post_meta( get_the_ID(), '_rnr3_disable_hero', 1 );

				if( $disable_hero != 'on' && !is_page_template( 'travel-series.php' ) && $market != 'virtual-run' ) { ?>

					<!-- big hero -->
					<?php if( !is_page_template( 'medals-series-2.php' ) ) { ?>

						<section id="hero">
							<?php

								if( is_page_template( 'nrd-training.php' ) ) { ?>
									<section class="wrapper mid">
										<h1>
											<?php echo get_post_meta( get_the_ID(), '_rnr3_nrdtrain_hero_title', true ); ?>
										</h1>
									</section>

									<div class="competitor-logo">
										<img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/competitor-logo-01.svg">
									</div>

								<?php } elseif( is_page_template( 'humana-event.php' ) || is_page_template( 'humana-home.php' ) ) { ?>

									<section id="bigtext" class="wrapper mid">
										<h1>
											<span class="headline">Start With a Tune Up</span>
											<img class="humana5k-logo" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/humana-logo.svg">
										</h1>
									</section>

								<?php } elseif( is_page_template( 'st-jude-series.php' ) ) { ?>

									<section class="wrapper mid">
										<img class="stjude-logo" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/stj-logo.svg">
									</section>

								<?php } elseif( is_singular( 'partner' ) ) {

									$partner_id			= get_id_by_slug( 'partners' );
									$hero_small_txt		= get_post_meta( $partner_id, '_rnr3_small_text', true );
									$hero_huge_txt		= get_post_meta( $partner_id, '_rnr3_huge_text', true );
									$hero_subline_txt	= get_post_meta( $partner_id, '_rnr3_sub_line', true ); ?>

									<section id="bigtext" class="wrapper mid">
										<h1>
											<span class="kicker bigtext-exempt"><?php echo $hero_small_txt; ?></span>
											<span class="headline"><?php echo $hero_huge_txt; ?></span>
											<span class="subhead bigtext-exempt"><?php echo $hero_subline_txt; ?></span>
										</h1>
									</section>

								<?php } elseif( $market == 'finisher-zone' ) {

									$finisherzone_id	= get_option( 'page_on_front' );
									$hero_small_txt		= get_post_meta( $finisherzone_id, '_rnr3_small_text', true );
									$hero_huge_txt		= get_post_meta( $finisherzone_id, '_rnr3_huge_text', true );
									$hero_subline_txt	= get_post_meta( $finisherzone_id, '_rnr3_sub_line', true ); ?>

									<section id="bigtext" class="wrapper mid">
										<h1>
											<span class="kicker bigtext-exempt"><?php echo $hero_small_txt; ?></span>
											<span class="headline"><?php echo $hero_huge_txt; ?></span>
											<span class="subhead bigtext-exempt"><?php echo $hero_subline_txt; ?></span>
										</h1>
									</section>

								<?php } elseif( $market == 'rock-blog' ) { ?>
									<section id="bigtext" class="wrapper">
										<h1>
											<span class="kicker bigtext-exempt">Rock &lsquo;n&lsquo; Roll</span>
											<span class="headline">Blog</span>
										</h1>
									</section>

								<?php } elseif( is_page_template( 'competitor-wireless-series.php' ) ) {

										$hero_small_txt		= get_post_meta( get_the_ID(), '_rnr3_small_text', 1 ) != '' ? get_post_meta( get_the_ID(), '_rnr3_small_text', 1 ) : '';
										$hero_huge_txt		= get_post_meta( get_the_ID(), '_rnr3_huge_text', 1 ) != '' ? get_post_meta( get_the_ID(), '_rnr3_huge_text', 1 ) : '';
										$hero_subline_txt	= get_post_meta( get_the_ID(), '_rnr3_sub_line', 1 ) != '' ? get_post_meta( get_the_ID(), '_rnr3_sub_line', 1 ) : ''; ?>

										<section id="bigtext" class="wrapper">
											<h1>
												<?php if( $hero_small_txt != '' ) {
													echo '<span class="kicker bigtext-exempt">'. $hero_small_txt .' </span>';
												}
												if( $hero_huge_txt != '' ) {
													echo '<span class="headline">'. $hero_huge_txt .'</span>';
												}
												if( $hero_subline_txt != '' ) {
													echo '<span class="subhead bigtext-exempt">'. $hero_subline_txt .'</span>';
												} ?>
											</h1>

								<?php } elseif( is_page_template( 'ironman.php' ) ) { ?>

									<section id="bigtext" class="wrapper">
										<p><img class="ironman-logo" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/ironman-wordmark.svg"></p>
										</h1>
									</section>

								<?php } else {

									$hero_small_txt		= get_post_meta( get_the_ID(), '_rnr3_small_text', true );
									$hero_huge_txt		= get_post_meta( get_the_ID(), '_rnr3_huge_text', true );
									$hero_subline_txt	= get_post_meta( get_the_ID(), '_rnr3_sub_line', true ); ?>

									<section id="bigtext" class="wrapper mid">
										<h1>
											<span class="kicker bigtext-exempt"><?php echo $hero_small_txt; ?></span>
											<span class="headline"><?php echo $hero_huge_txt; ?></span>
											<span class="subhead bigtext-exempt"><?php echo $hero_subline_txt; ?></span>
										</h1>

								<?php }

								if( $youtube_vid != '' ) {
									echo '<div class="play bigtext-exempt">
										<a class="big_video" href="javascript:;" onClick="toggleVideo();"><span class="icon-play"></span></a>
									</div>';
								}
							if( !is_page_template( 'nrd-training.php' ) ) {
								echo '</section>';
							} ?>
						</section>

					<?php }
				}

				if( $youtube_vid != '' ) {

					echo '<section id="hero-video">
						<div id="popupVid" class="hero-video-wrapper">
							<a class="big_video_close" href="javascript:;" onClick="toggleVideo(\'hide\');"><span class="icon-cancel"></span></a>
							<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'. $youtube_vid .'?enablejsapi=1&rel=0&showinfo=0&modestbranding=1" frameborder="0" allowfullscreen></iframe>
						</div>
					</section>';

				}

				if( is_page_template( 'travel-series.php' ) ) { ?>

					<section id="new-hero-container">
						<section id="new-hero">
							<section class="wrapper bigtext" itemscope itemtype="http://schema.org/Event">

								<?php
									$hero_small_txt		= get_post_meta( get_the_ID(), '_rnr3_small_text', true );
									$hero_huge_txt		= get_post_meta( get_the_ID(), '_rnr3_huge_text', true );
									$hero_subline_txt	= get_post_meta( get_the_ID(), '_rnr3_sub_line', true );
								?>

								<meta itemprop="organizer" content="Rock 'n' Roll Marathon Series" />

								<div class="hero-copy">
									<?php
										echo '
											<span class="kicker bigtext-exempt">'. $hero_small_txt .'</span>
											<span class="hero_huge_txt">'. $hero_huge_txt .'</span>
											<span class="subhead bigtext-exempt">'. $hero_subline_txt .'</span>
										';
									?>
								</div> <!-- END .HERO-COPY -->

							</section>
						</section>
					</section>

				<?php }

			?>

