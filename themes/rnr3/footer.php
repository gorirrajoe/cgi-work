<?php
	/**
	 * global variables
	 */
	$twitter	= rnr_get_option( 'rnrgv_twitter');
	$instagram	= rnr_get_option( 'rnrgv_instagram');
	$pinterest	= rnr_get_option( 'rnrgv_pinterest');
	$youtube	= rnr_get_option( 'rnrgv_youtube');

	$market		= get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	// big red boxes
	if( isset($event_info->big_red_boxes) && $event_info->big_red_boxes == 1 && !is_front_page() && !is_404() ) {
		rnr3_get_big_red_boxes( $market );
	}

	// sponsors
	// rnr3_get_sponsor_module2( $market ); -- don't use... yet
	rnr3_get_sponsor_module( $market );

	$qt_lang = rnr3_get_language();

	include 'languages.php';
?>
		<a href="#" id="goTop">
			<span class="icon-up-open"></span>
		</a>

		<!-- combined footers -->
		<footer>

			<section class="wrapper">

				<!-- event footer -->
				<nav id="eventfooter" role="navigation">

					<div class="serieslinks">
						<h4><?php echo $series_txt; ?></h4>

						<?php /* hard code for now
							$global_nav = rnr3_get_global_nav( 'footer' );
							echo $global_nav;
						*/ ?>

						<ul>
							<li><a href="<?php echo network_site_url('/about-the-rock-n-roll-marathon-series/'); ?>"><?php echo $about_txt; ?></a></li>
							<li><a href="<?php echo network_site_url('/press/'); ?>"><?php echo $press_txt; ?></a></li>
							<li><a href="<?php echo network_site_url('/results/'); ?>"><?php echo $results_txt; ?></a></li>
							<li><a href="<?php echo network_site_url('/expo/'); ?>"><?php echo $expo_txt; ?></a></li>
							<li><a href="<?php echo network_site_url('/partners/'); ?>"><?php echo $partners_txt; ?></a></li>
							<li><a href="<?php echo network_site_url('/charities/'); ?>"><?php echo $charity_txt; ?></a></li>
							<li><a href="<?php echo network_site_url('/tempo/'); ?>"><?php echo $blog_txt; ?></a></li>
							<li><a href="<?php echo network_site_url('/elite-athlete-program/'); ?>"><?php echo $elite_txt; ?></a></li>
						</ul>

					</div>


					<div class="racelinks">
						<h4><?php echo $races_txt; ?></h4>
						<?php rnr3_get_secondary_nav( 'the-races', '', 1 ); ?>
					</div>


					<div class="weekendlinks">
						<h4><?php echo $weekend_txt; ?></h4>
						<?php rnr3_get_secondary_nav( 'the-weekend', '', 1 ); ?>
					</div>


					<?php /*
					<div class="communitylinks">
						<h4>Community</h4>
						<?php
							$nav_settings = array(
								'menu'            => 'community',
								'fallback_cb'     => false,
								'theme_location'  => '__no_such_location',
							);
							$navmenu = wp_nav_menu( $nav_settings );
						?>
					</div>
					*/ ?>


					<div class="connectlinks">
						<h4><?php echo $connect_txt; ?></h4>

						<?php
							// check if event has a custom instagram account
							if( $event_info->instagram_acct != '' ) {
								$instagram = 'https://instagram.com/'. $event_info->instagram_acct;
							}
							// check if event has a custom twitter account
							if( $event_info->twitter_acct != NULL ) {
								$twitter = 'https://twitter.com/'. $event_info->twitter_acct;
							}
							// check if event has a custom youtube account
							if( $event_info->youtube_acct != NULL ) {
								$youtube = 'https://www.youtube.com/user/'. $event_info->youtube_acct;
							}

						?>

						<ul>
							<?php
								if( $market == 'montreal') { ?>

									<li><a href="mailto:rnrmontreal@competitorgroup.com"><?php echo $contact_txt; ?></a></li>

								<?php } else { ?>

									<li><a href="<?php echo network_site_url( '/contact/' ); ?>"><?php echo $contact_txt; ?></a></li>

								<?php }
							?>
							<li><a href="<?php echo network_site_url( '/contact/faq/' ); ?>">FAQ</a></li>
							<li class="connect-contact"><a href="https://rtrt.me/app/rnra" target="_blank">Download our App</a></li>

							<li><a href="<?php echo $twitter; ?>" target="_blank">Twitter</a></li>
							<li><a href="<?php echo $event_info->facebook_page; ?>" target="_blank">Facebook</a></li>
							<li><a href="<?php echo $instagram; ?>" target="_blank">Instagram</a></li>
							<li><a href="<?php echo $youtube; ?>" target="_blank">YouTube</a></li>
							<li class="footer-pinterest-link"><a href="<?php echo $pinterest; ?>" target="_blank">Pinterest</a></li>
						</ul>
					</div>

				</nav>

				<!-- global footer -->
				<section id="globalfooter">
					<a href="<?php echo rnr_get_option( 'siteurl' ); ?>"><div id="footerlogo"><img class="lazy" data-original="<?php echo get_stylesheet_directory_uri(); ?>/img/logo_footer@2x.png" alt="Rock 'N' Roll Marathon Series logo"></div></a>
					<p>&copy; <?php echo date('Y'); ?> &ndash; Competitor Group, Inc. All Rights Reserved. <a href="<?php echo  network_site_url('/about-competitor-group/'); ?>">About</a> | <a href="http://competitorgroup.com/privacy-policy/" target="_blank">Privacy and Cookie Policy</a> | <a href="http://competitorgroup.com/privacy-policy#third-parties" target="_blank">AdChoices</a> | <a href="http://competitorgroup.com/" target="_blank">Media Kit</a> | <a href="https://careers-competitorgroup.icims.com/jobs/intro?hashed=0" target="_blank">Careers</a></p>
				</section>

			</section>
		</footer> <!-- /combined footers -->

	</div><!-- /container -->

	<?php wp_footer();

	// only show big hero video on home page
	if( is_front_page() && $event_info->youtube_video != '' ) { ?>

		<script>
			$(document).ready(function(){
				$(".big_video_close").click(function(){
					$("#hero-video").hide();
					$("#hero").show();
				});
				$(".big_video").click(function(){
					$("#hero-video").show();
					$("#hero").hide();
				});
			});
			function toggleVideo(state) {
				// if state == 'hide', hide. Else: show video
				var div = document.getElementById("popupVid");
				var iframe = div.getElementsByTagName("iframe")[0].contentWindow;
				div.style.display = state == 'hide' ? 'none' : '';
				func = state == 'hide' ? 'pauseVideo' : 'playVideo';
				iframe.postMessage('{"event":"command","func":"' + func + '","args":""}','*');
			}
		</script>

	<?php }

	$pixlee = get_post_meta( get_the_ID(), '_rnr3_pixlee_code', TRUE );

	if( $pixlee != '' ) {
		echo '<script type="text/javascript">
			function downloadJSAtOnload() {
				var element = document.createElement("script");
				element.src = "//assets.pixlee.com/assets/pixlee_widget_1_0_0.js";
				document.body.appendChild(element);
			}
			if (window.addEventListener)
				window.addEventListener("load", downloadJSAtOnload, false);
			else if (window.attachEvent)
				window.attachEvent("onload", downloadJSAtOnload);
			else window.onload = downloadJSAtOnload;
		</script>';
	} ?>
</body>
</html>
