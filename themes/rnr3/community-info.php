<?php
  /* Template Name: Community Info */
  get_header();

  $parent_slug = the_parent_slug();
  rnr3_get_secondary_nav( $parent_slug );

  $prefix = '_rnr3_';

  // titles
  if ( !post_password_required() ) {
    $community['title']     = 'Community Information';
    $downloads['title']     = 'Downloads';
    $church['title']        = 'Church Access Directions &amp; Passes';
    $waze['title']          = 'Waze';
    $beaware['title']       = 'Be Aware. Plan Ahead';
    $details['title']       = 'Event Details';
    $road_closures['title'] = 'Road Closures';
    $fwy_closures['title']  = 'Freeway and Ramp Closures';
    $bus_boxes['title']     = 'Bus Boxes';
    $closure_map['title']   = 'Road Closure Map';
    $metering['title']      = 'Metering Locations';
    $public_trans['title']  = 'Public Transportation Information';
    $no_parking['title']    = 'No Parking';
    $alt_routes['title']    = 'Alternate Access Routes';
    $hotel_access['title']  = 'Hotel Access Information';
    $local_biz['title']     = 'Local Business? Get Involved!';
    $local_comm['title']    = 'Local Communities - Get Involved in the Fun!';
    $more_info['title']     = 'For More Information';

    // contents
    $community['content']     = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'community', true ) );
    $downloads['content']     = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'downloads', true ) );
    $church['content']        = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'church', true ) );
    $waze['content']          = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'waze', true ) );
    $beaware['content']       = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'beaware', true ) );
    $details['content']       = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'details', true ) );
    $road_closures['content'] = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'road_closures', true ) );
    $fwy_closures['content']  = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'fwy_closures', true ) );
    $bus_boxes['content']     = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'bus_boxes', true ) );
    $closure_map['content']   = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'closure_map', true ) );
    $metering['content']      = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'metering', true ) );
    $public_trans['content']  = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'public_trans', true ) );
    $no_parking['content']    = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'no_parking', true ) );
    $alt_routes['content']    = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'alt_routes', true ) );
    $hotel_access['content']  = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'hotel_access', true ) );
    $local_biz['content']     = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'local_biz', true ) );
    $local_comm['content']    = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'local_comm', true ) );
    $more_info['content']     = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'more_info', true ) );

    $community_array = array(
      $community,
      $downloads,
      $waze,
      $details,
      $beaware,
      $road_closures,
      $fwy_closures,
      $closure_map,
      $alt_routes,
      $church,
      $hotel_access,
      $metering,
      $bus_boxes,
      $public_trans,
      $no_parking,
      $local_biz,
      $local_comm,
      $more_info
    );
  }

?>


<!-- main content -->
<main role="main" id="main">
  <section class="wrapper grid_2 offset240left">
    <div class="column sidenav">
      <div id="nav-anchor"></div>
      <nav class="sticky-nav">
        <ul>
          <?php
            foreach( $community_array as $key => $value ) {
              if( $value['content'] != '' ) {
                echo '<li><a href="#community'.$key.'">'.$value['title'].'</a></li>';
              }
            }
          ?>
        </ul>
      </nav>
    </div>

    <div class="column">
      <div class="content">
        <?php
          if (have_posts()) : while (have_posts()) : the_post();
            if ( post_password_required() ) {
              the_title('<h2>', '</h2>');
            }
            the_content();
            endwhile;
          endif;

          if( isset( $community_array ) ) {
            foreach( $community_array as $key => $value ) {
              if( $value['content'] != '' ) {
                echo '<section id="community'.$key.'">
                  <h2>'.$value['title'].'</h2>
                  '. $value['content'] .
                '</section>';
              }
            }
          }
        ?>
      </div>
    </div>
  </section>
  <script>
    /*
    jQuery(function($) {
      $( "#accordion" ).accordion({
        header:'h4',
        heightStyle: 'content'
      });
    });
    */
    $(document).ready(function(){

        /**
         * This part does the "fixed navigation after scroll" functionality
         * We use the jQuery function scroll() to recalculate our variables as the
         * page is scrolled/
        $(window).scroll(function(){
            var window_top = $(window).scrollTop() + 50; // the "12" should equal the margin-top value for nav.stick
            var div_top = $('#nav-anchor').offset().top;
                if (window_top > div_top) {
                    $('.sticky-nav').addClass('stick');
                } else {
                    $('.sticky-nav').removeClass('stick');
                }
        });
         */


        /**
         * This part causes smooth scrolling using scrollto.js
         * We target all a tags inside the nav, and apply the scrollto.js to it.
         */
        $(".sticky-nav a, .backtotop").click(function(evn){
            evn.preventDefault();
            $('html,body').scrollTo(this.hash, this.hash);
        });



        /**
         * This part handles the highlighting functionality.
         * We use the scroll functionality again, some array creation and
         * manipulation, class adding and class removing, and conditional testing
        var aChildren = $(".sticky-nav li").children(); // find the a children of the list items
        var aArray = []; // create the empty aArray
        for (var i=0; i < aChildren.length; i++) {
            var aChild = aChildren[i];
            var ahref = $(aChild).attr('href');
            aArray.push(ahref);
        } // this for loop fills the aArray with attribute href values

        $(window).scroll(function(){
            var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
            var windowHeight = $(window).height(); // get the height of the window
            var docHeight = $(document).height();

            for (var i=0; i < aArray.length; i++) {
                var theID = aArray[i];
                var divPos = $(theID).offset().top; // get the offset of the div from the top of page
                var divHeight = $(theID).height(); // get the height of the div in question
                if (windowPos >= (divPos - 1) && windowPos < (divPos + divHeight -1 )) {
                    $("a[href='" + theID + "']").addClass("nav-active");
                } else {
                    $("a[href='" + theID + "']").removeClass("nav-active");
                }
            }

            if(windowPos + windowHeight == docHeight) {
                if (!$(".sticky-nav li:last-child a").hasClass("nav-active")) {
                    var navActiveCurrent = $(".nav-active").attr("href");
                    $("a[href='" + navActiveCurrent + "']").removeClass("nav-active");
                    $(".sticky-nav li:last-child a").addClass("nav-active");
                }
            }
        });
         */
    });

  </script>
</main>
<?php get_footer(); ?>
