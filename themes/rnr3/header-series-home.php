<?php
	/**
	 * header for the series homepage
	 */
	global $wpdb;
	$market		= get_market2();
	$qt_lang	= rnr3_get_language();

	/**
	 * global variables
	 */
	$twitter				= rnr_get_option( 'rnrgv_twitter' );
	$instagram				= rnr_get_option( 'rnrgv_instagram' );
	$pinterest				= rnr_get_option( 'rnrgv_pinterest' );
	$youtube				= rnr_get_option( 'rnrgv_youtube' );
	$header_code			= rnr_get_option( 'rnrgv_header' );
	$google_analytics		= rnr_get_option( 'rnrgv_google_analytics' );
	$gtm					= rnr_get_option( 'rnrgv_gtm' );
	$optimizely				= rnr_get_option( 'rnrgv_optimizely' );
	$twenty_years_enabled	= ( rnr_get_option( 'rnrgv_20_enabled_series' ) == 'on' ) ? rnr_get_option( 'rnrgv_20_enabled_series' ) : 'off';

	$twenty_years_exception_list	= ( rnr_get_option( 'rnrgv_20year_exception' ) != '' ) ? rnr_get_option( 'rnrgv_20year_exception' ) : '';
	$twenty_years_exception_events	= strpos( $twenty_years_exception_list, ',' ) ? explode( ',', $twenty_years_exception_list ) : array( $twenty_years_exception_list );
	$tempo_enabled = ( rnr_get_option( 'rnrgv_tempo_enabled' ) == 'on' ) ? true : false;


	/**
	 * we only want the mid class gone from the series homepage
	 * we don't want to make the other series subpage heroes look bad
	 */
	$twenty_years_mid_class	= ( $twenty_years_enabled == 'on' && is_page_template( 'series-home.php' ) ) ? '' : 'mid';


	if( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

?><!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
	<head>

		<?php if( $optimizely != '' ) {
			echo stripslashes( $optimizely );
		} ?>

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

		<?php rnr3_display_favicon();

		if( $google_analytics != '' ) { ?>
			<script type="text/javascript">
				var _gaq = _gaq || [];
				var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
				_gaq.push(
					['_require', 'inpage_linkid', pluginUrl],
					['_setAccount', '<?php echo $google_analytics;?>'],
					['_setDomainName', 'runrocknroll.com'],
					['_setAllowLinker', true],
					['_trackPageview']
				 );
				( function() {
					var ga = document.createElement( 'script' ); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ( 'https:' == document.location.protocol ? 'https://' : 'http://' ) + 'stats.g.doubleclick.net/dc.js';
					var s = document.getElementsByTagName( 'script' )[0]; s.parentNode.insertBefore( ga, s );
				} )();
			</script>
		<?php }

		wp_head();

		if( $event_info->header_settings != '' ) {
			echo $event_info->header_settings;
		}

		// setting big hero image background
		$header_image = '';

		// if you set a custom hero image for a page, use that
		if( ( $unique_header = get_post_meta( get_the_ID(), '_rnr3_unique_header_img', 1 ) ) != '' ) {
			$header_image = $unique_header;
		} elseif( is_singular( 'moment' ) || is_post_type_archive( 'moment' ) ) {
			$moment_single_id = get_id_by_slug( 'moments' );
			$header_image = get_post_meta( $moment_single_id, '_rnr3_special_header_img', true );
		} elseif( get_post_meta( get_the_ID(), '_rnr3_special_header_img', true ) != '' ) {
			$header_image = get_post_meta( get_the_ID(), '_rnr3_special_header_img', true );
		} else {
			$header_image = $event_info->header_image;
		}


		if( $header_image != '' ) { ?>
			<style>
				#hero {
					background:url( "<?php echo $header_image; ?>" ) no-repeat scroll 50% 0 rgba( 0, 0, 0, 0 );
				}
				<?php if( is_front_page() && $event_info->youtube_video != '' ) { ?>
					#hero-video {
						background:linear-gradient( rgba( 0,0,0,.8 ), rgba( 0, 0, 0, 0.8 ) ), url( "<?php echo $header_image; ?>" ) no-repeat scroll 50% 0 ;
					}
				<?php } ?>
			</style>
		<?php }

		if( is_front_page() ) {
			$dfp_market = str_replace( '-', '_', $market ); ?>

			<script>
				(function() {
				var useSSL = 'https:' == document.location.protocol;
				var src = (useSSL ? 'https:' : 'http:') +
				'//www.googletagservices.com/tag/js/gpt.js';
				document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
				})();
			</script>

			<script>
				slot_600_600_1=googletag.defineSlot('/8221/runrocknroll/sm-<?php echo $dfp_market; ?>', [600, 600], 'div-600_600_1').addService(googletag.pubads()).setTargeting('tile', '1');
				slot_600_600_2=googletag.defineSlot('/8221/runrocknroll/sm-<?php echo $dfp_market; ?>', [600, 600], 'div-600_600_2').addService(googletag.pubads()).setTargeting('tile', '2');
				slot_600_600_3=googletag.defineSlot('/8221/runrocknroll/sm-<?php echo $dfp_market; ?>', [600, 600], 'div-600_600_3').addService(googletag.pubads()).setTargeting('tile', '3');
				slot_600_600_4=googletag.defineSlot('/8221/runrocknroll/sm-<?php echo $dfp_market; ?>', [600, 600], 'div-600_600_4').addService(googletag.pubads()).setTargeting('tile', '4');
				googletag.pubads().enableAsyncRendering();
				googletag.pubads().enableSingleRequest();
				googletag.enableServices();
			</script>
		<?php } ?>

	</head>


	<?php
		$body_array_class	= array();
		$body_array_class[]	= 'market-'. $market;
		$body_array_class[]	= ( $twenty_years_enabled == 'on' && is_page_template( 'series-home.php' ) ) ? 'twenty_years' : '';
		$body_array_class[] = $tempo_enabled ? 'tempo' : '';
	?>

	<body id="rnr-series" <?php body_class( $body_array_class ); ?>>

		<?php
			if( $gtm != '' ) {
				echo stripslashes( $gtm );
			}
			eu_regulation();
		?>

		<div id="container">

			<!-- alert banner -->
			<?php
				/**
				 * global alert banner
				 * managed through the global variables manager
				 * countdown clock can be toggled on/off
				 * exceptions for individual events (event slug) can be added
				 * messaging can be before or after set date
				 * translations are allowed using the language shortcode (ie [:es])
				 *
				 * will override the alert bar set in the event manager
				 */
				$gab_exceptions = strpos( rnr_get_option( 'rnrgv_gab_exceptions' ), ',' ) ? explode( ',', rnr_get_option( 'rnrgv_gab_exceptions' ) ) : array( rnr_get_option( 'rnrgv_gab_exceptions' ) );

				$current_date	= new DateTime( 'now', new DateTimeZone( 'America/Los_Angeles' ) );
				$gab_day		= date( 'Ymd', rnr_get_option( 'rnrgv_gab_day' ) );

				if( rnr_get_option( 'rnrgv_enable_gab_countdown' ) == 'on' && !in_array( $market, $gab_exceptions ) && $current_date->format( 'Ymd' ) <= $gab_day ) {

					$gab_afterday	= strtotime( '+1 day', rnr_get_option( 'rnrgv_gab_day' ) );
					$gab_afterday	= date( 'Ymd', $gab_afterday );

					if( $current_date->format( 'Ymd' ) < $gab_day ) {

						$phase = 'pre';

						if( $qt_lang['enabled'] == 1 ) {

							$gab_countdown_langs	= qtrans_split( rnr_get_option( 'rnrgv_gab_txt_pre' ) );
							$gab_countdown_txt		= $gab_countdown_langs[$qt_lang['lang']];

						} else {

							$gab_countdown_txt = rnr3_content_qtx_filter( rnr_get_option( 'rnrgv_gab_txt_pre' ) );

						}

					} elseif( $current_date->format( 'Ymd' ) >= $gab_day && $current_date->format( 'Ymd' ) < $gab_afterday ) {

						$phase = 'dayof';

						if( $qt_lang['enabled'] == 1 ) {

							$gab_countdown_langs	= qtrans_split( rnr_get_option( 'rnrgv_gab_txt_day' ) );
							$gab_countdown_txt		= $gab_countdown_langs[$qt_lang['lang']];

						} else {


							$gab_countdown_txt = rnr3_content_qtx_filter( rnr_get_option( 'rnrgv_gab_txt_day' ) );

						}

					}

					if( $current_date->format( 'Ymd' ) < $gab_afterday ) {

						echo '<section id="alert" class="grd_countdown">
							<section class="wrapper">
								<p class="mid2">'. $gab_countdown_txt . ' ' . rnr3_global_countdown_timer( $phase, $qt_lang ) .'</p>
							</section>
						</section>';

					}

				} elseif( $event_info->alert_msg != '' ) { ?>

					<section id="alert">
						<section class="wrapper">
							<p class="mid2"><?php echo $event_info->alert_msg; ?></p>
						</section>
					</section>

				<?php }
			?>


			<!-- begin main header -->
			<header>
				<section class="wrapper">
					<div id="getgoing" class="clearfix">

						<ul id="reg">
							<?php echo '<li><a href="'.site_url().'#findrace" class="register">Register</a></li>'; ?>
						</ul>

					</div>

					<?php
						/**
						 * get appropriate logo, 20 years or normal?
						 */
						get_logo( $twenty_years_enabled, $market, $twenty_years_exception_events );
					?>

					<nav>
						<div class="hamburger">
							<span></span>
							<span></span>
							<span></span>
							<span class="hamburger__label">Menu</span>
						</div>

						<?php
							$global_nav = rnr3_get_global_nav( $qt_lang['lang'] );
							echo $global_nav;

							$nav_settings = array(
								'menu'				=> 'primary-series',
								'container_id'		=> 'primary',
								'fallback_cb'		=> false,
								'theme_location'	=> '__no_such_location',
								'container_class'	=> 'fourup'
							 );
							wp_nav_menu( $nav_settings );
						?>
					</nav>

				</section>
			</header>


			<?php
				$disable_hero = get_post_meta( get_the_ID(), '_rnr3_disable_hero', 1 );

				if( $disable_hero != 'on' ) { ?>

					<!-- big hero -->
					<section id="hero">

						<?php
							$hero_small_txt		= get_post_meta( get_the_ID(), '_rnr3_small_text', true );
							$hero_huge_txt		= get_post_meta( get_the_ID(), '_rnr3_huge_text', true );
							$hero_subline_txt	= get_post_meta( get_the_ID(), '_rnr3_sub_line', true );

							if( !is_front_page() ) {

								if( is_page() ) {
									$hero_small_txt		= 'Rock &lsquo;n&lsquo; Roll';
									$hero_huge_txt		= get_the_title( get_the_ID() );
									$hero_subline_txt	= '';
								} elseif( is_category() ) {
									$hero_small_txt		= 'Rock &lsquo;n&lsquo; Roll';
									$hero_huge_txt		= single_cat_title( '', false );
									$hero_subline_txt	= '';
								} elseif( is_singular( 'moment' ) || is_post_type_archive( 'moment' ) ) {
									$hero_small_txt		= 'Rock &lsquo;n&lsquo; Roll';
									$hero_huge_txt		= 'Moments';
									$hero_subline_txt	= 'Marathon Series';
								} elseif( is_single() ) {
									$pCat				= get_the_category( get_the_ID() );
									$hero_small_txt		= 'Rock &lsquo;n&lsquo; Roll';
									$hero_huge_txt		= $pCat[0]->cat_name;
									$hero_subline_txt	= '';
								} elseif( is_404() ) {
									$hero_small_txt		= 'Error';
									$hero_huge_txt		= '404';
									$hero_subline_txt	= '';
								} ?>

								<section id="bigtext" class="wrapper <?php echo $twenty_years_mid_class; ?>">
									<h1>
										<span class="kicker bigtext-exempt"><?php echo $hero_small_txt; ?></span>
										<span class="headline"><?php echo $hero_huge_txt; ?></span>

										<?php if( $hero_subline_txt != '' ) { ?>

											<span class="subhead bigtext-exempt">
												<?php echo $hero_subline_txt; ?>
											</span>

										<?php } ?>

									</h1>
								</section>


								<?php if( is_front_page() && $event_info->youtube_video != '' ) {
									echo '<div class="play bigtext-exempt">
										<a class="big_video" href="javascript:;" onClick="toggleVideo();"><span class="icon-play"></span></a>
									</div>';

								}

							} else {

								if( $twenty_years_enabled == 'on' ) {

									echo '<section class="wrapper">';

										$twenty_years_logo_square = ( rnr_get_option( 'rnrgv_20year_logo_square' ) != '' ) ? rnr_get_option( 'rnrgv_20year_logo_square' ) : '';

										if( $twenty_years_logo_square != '' ) {

											echo '<div class="twenty_years__logo bigtext-exempt">
												<img src="'. $twenty_years_logo_square .'">
											</div>';

										}

										$twenty_years_title		= ( get_post_meta( get_the_ID(), '_rnr3_twenty_years_title', 1 ) != '' ) ? get_post_meta( get_the_ID(), '_rnr3_twenty_years_title', 1 ) : '';
										$twenty_years_content	= ( get_post_meta( get_the_ID(), '_rnr3_twenty_years_content', 1 ) != '' ) ? apply_filters( 'the_content', get_post_meta( get_the_ID(), '_rnr3_twenty_years_content', 1 ) ) : '';

										echo '<section id="bigtext">

											<h1 class="bigtext">'. $twenty_years_title .'</h1>

											<div class="twenty_years__content bigtext-exempt">'.
												$twenty_years_content .'
											</div>

										</section>
									</section>';

								} else { ?>

									<section id="bigtext" class="wrapper mid">
										<h1>
											<span class="kicker bigtext-exempt"><?php echo $hero_small_txt; ?></span>
											<span class="headline"><?php echo $hero_huge_txt; ?></span>

											<?php if( $hero_subline_txt != '' ) { ?>

												<span class="subhead bigtext-exempt">
													<?php echo $hero_subline_txt; ?>
												</span>

											<?php } ?>

										</h1>

										<?php if( is_front_page() && $event_info->youtube_video != '' ) {
											echo '<div class="play bigtext-exempt">
												<a class="big_video" href="javascript:;" onClick="toggleVideo();"><span class="icon-play"></span></a>
											</div>';

										} ?>

									</section>

								<?php }
							}
						?>

					</section>


					<?php if( is_front_page() && $event_info->youtube_video != '' ) {
						echo '<section id="hero-video">
							<div id="popupVid" class="hero-video-wrapper">
								<a class="big_video_close" href="javascript:;" onClick="toggleVideo( \'hide\' );"><span class="icon-cancel"></span></a>
								<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'. $event_info->youtube_video .'?enablejsapi=1&rel=0&showinfo=0&modestbranding=1" frameborder="0" allowfullscreen></iframe>
							</div>
						</section>';
					}

				}


				if( is_front_page() ) { ?>

					<!-- social stuff -->
					<section id="ribbon">
						<section class="wrapper mid">

							<?php if( $qt_lang['enabled'] == 100 ) {
								surveygizmo_form( $market, $qt_lang['lang'] ) ;
							} ?>

							<div class="hash"><?php echo strtoupper( $event_info->twitter_hashtag ); ?> <span class="pop">/</span></div>
							<div class="twitter"><a href="<?php echo $twitter; ?>" target="_blank"><span class="icon-twitter"></span></a></div>
							<div class="facebook"><a href="<?php echo $event_info->facebook_page; ?>" target="_blank"><span class="icon-facebook"></span></a></div>
							<div class="instagram"><a href="<?php echo $instagram; ?>" target="_blank"><span class="icon-instagram"></span></a></div>
						</section>
					</section>

				<?php }
				?>
