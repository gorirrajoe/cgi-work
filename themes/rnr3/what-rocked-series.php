<?php
  /* Template Name: What Rocked? (Series) */
  get_header( 'special' );
  $prefix = '_rnr3_';
  global $post;
  $page_slug = $post->post_name;
?>

  <!-- main content -->
  <main role="main" id="main" class="no-hero">
    <nav id="subnav">
      <?php
        $miles_active = $music_active = $medals_active = $moments_active = '';
        if( !isset( $_GET['section'] ) ) {
          $miles_active = 'class="subnav-current-page"';
        } elseif( $_GET['section'] == 'music' ) {
          $music_active = 'class="subnav-current-page"';
        } elseif( $_GET['section'] == 'medals' ) {
          $medals_active = 'class="subnav-current-page"';
        } elseif( $_GET['section'] == 'moments' ) {
          $moments_active = 'class="subnav-current-page"';
        }
      ?>

      <section class="wrapper">
        <div class="subnav-menu-label"><?php echo get_post_meta( get_the_ID(), $prefix . 'submenu_title', 1 ); ?></div>
        <div class="subnav-menu-primary-event-container">
          <ul class="subnav-menu">
            <li><a <?php echo $miles_active; ?> href="<?php echo site_url( $page_slug ); ?>">Miles</a></li>
            <li><a <?php echo $music_active; ?> href="<?php echo site_url( $page_slug . '/?section=music'); ?>">Music</a></li>
            <li><a <?php echo $medals_active; ?> href="<?php echo site_url( $page_slug . '/?section=medals'); ?>">Medals</a></li>
            <li><a <?php echo $moments_active; ?> href="<?php echo site_url( $page_slug . '/?section=moments'); ?>">Moments</a></li>
          </ul>
        </div>
      </section>

    </nav>

    <section class="wrapper">
      <?php if (have_posts()) : while (have_posts()) : the_post();
        echo '<h2>'. get_the_title(). '</h2>';
        // the_content();
      endwhile; endif;

      if( !isset( $_GET['section'] ) ) {
        echo '<div id="miles-pixlee">'
          . apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'miles_intro', 1 ) ) .
					'<div class="pixlee-widget">'
						. get_post_meta( get_the_ID(), $prefix . 'pixlee_miles', 1 ) .
					'</div>'
          . apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'miles_after', 1 ) ) .
          '<div class="pixlee_nav">
            <div class="pixlee_nav_next"><a href="'. site_url( $page_slug . '/?section=music') .'">Vote for Music &raquo;</a></div>
          </div>
        </div>';
      } elseif( $_GET['section'] == 'music' ) {
        echo '<div id="music-pixlee">'
          . apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'music_intro', 1 ) ) .
					'<div class="pixlee-widget">'
						. get_post_meta( get_the_ID(), $prefix . 'pixlee_music', 1 ) .
					'</div>'
          . apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'music_after', 1 ) ) .
          '<div class="pixlee_nav">
            <div class="pixlee_nav_prev"><a href="'. site_url( $page_slug ) .'">&laquo; Vote for Miles</a></div>
            <div class="pixlee_nav_next"><a href="'. site_url( $page_slug . '/?section=medals') .'">Vote for Medals &raquo;</a></div>
          </div>
        </div>';
      } elseif( $_GET['section'] == 'medals' ) {
        echo '<div id="medals-pixlee">'
          . apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'medals_intro', 1 ) ) .
					'<div class="pixlee-widget">'
						. get_post_meta( get_the_ID(), $prefix . 'pixlee_medals', 1 ) .
					'</div>'
          . apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'medals_after', 1 ) ) .
          '<div class="pixlee_nav">
            <div class="pixlee_nav_prev"><a href="'. site_url( $page_slug ) .'/?section=music">&laquo; Vote for Music</a></div>
            <div class="pixlee_nav_next"><a href="'. site_url( $page_slug . '/?section=moments') .'">Vote for Moments &raquo;</a></div>
          </div>
        </div>';
      } elseif( $_GET['section'] == 'moments' ) {
        echo '<div id="moments-pixlee">'
          . apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'moments_intro', 1 ) ) .
					'<div class="pixlee-widget">'
	          . get_post_meta( get_the_ID(), $prefix . 'pixlee_moments', 1 ) .
					'</div>'
          . apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'moments_after', 1 ) ) .
          '<div class="pixlee_nav">
            <div class="pixlee_nav_prev"><a href="'. site_url( $page_slug ) .'/?section=medals">&laquo; Vote for Medals</a></div>
          </div>
        </div>';
      } ?>
    </section>

    <script>
      $(document).ready(function(){

          /**
           * This part causes smooth scrolling using scrollto.js
           * We target all a tags inside the nav, and apply the scrollto.js to it.
          $(".sticky-nav a, .backtotop").click(function(evn){
              evn.preventDefault();
              $('html,body').scrollTo(this.hash, this.hash);
          });
           */
          /*
          $('main').stickem({
            container: '.offset240left',
          });
          */


          /**
           * This part handles the highlighting functionality.
           * We use the scroll functionality again, some array creation and
           * manipulation, class adding and class removing, and conditional testing
           */
          var aChildren = $(".sticky-nav li").children(); // find the a children of the list items
          var aArray = []; // create the empty aArray
          for (var i=0; i < aChildren.length; i++) {
              var aChild = aChildren[i];
              var ahref = $(aChild).attr('href');
              aArray.push(ahref);
          } // this for loop fills the aArray with attribute href values

          $(window).scroll(function(){
              var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
              var windowHeight = $(window).height(); // get the height of the window
              var docHeight = $(document).height();

              for (var i=0; i < aArray.length; i++) {
                  var theID = aArray[i];
                  var divPos = $(theID).offset().top; // get the offset of the div from the top of page
                  var divHeight = $(theID).height(); // get the height of the div in question
                  if (windowPos >= (divPos - 1) && windowPos < (divPos + divHeight -1 )) {
                      $("a[href='" + theID + "']").addClass("nav-active");
                  } else {
                      $("a[href='" + theID + "']").removeClass("nav-active");
                  }
              }

              if(windowPos + windowHeight == docHeight) {
                  if (!$(".sticky-nav li:last-child a").hasClass("nav-active")) {
                      var navActiveCurrent = $(".nav-active").attr("href");
                      $("a[href='" + navActiveCurrent + "']").removeClass("nav-active");
                      $(".sticky-nav li:last-child a").addClass("nav-active");
                  }
              }
          });
      });

    </script>
  </main>

  <?php get_footer( 'series' );
