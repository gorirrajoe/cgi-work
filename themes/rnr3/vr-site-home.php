<?php
	/* Template Name: Virtual Run: Site Home */
	get_header( 'oneoffs' );

	$market = get_market2();
	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$prefix = '_rnr3_';

	// apparently languages aren't a thing with this just yet
	// $qt_lang		= rnr3_get_language();
	// $qt_status		= $qt_lang['enabled'] == 1 ? true : false;
	// $qt_cur_lang	= $qt_lang['lang'];

	// include 'languages.php';

	/**
	 * set time to PDT so we can accurately disable the button at 11:59:59pm our time
	 */
	$timezone_offset	= ( timezone_offset_get( timezone_open( "America/Los_Angeles" ), new DateTime() ) ) / 3600;
	$today_unix			= strtotime( $timezone_offset . ' hours' );

	$virtualrun_allmeta	= get_meta( get_the_ID() );
	// print_r( $virtualrun_allmeta );


	/**
	 * phase tabs
	 * since it's dynamic, set the names first
	 * (from the generic "phase" names)
	 */
	$phase_array = array( 'phase1', 'phase2', 'phase3' );

	foreach( $phase_array as $key => $value ) {
		$event_array[$value] = vr_get_option( $prefix . $value . '_phase_name' );
	}


	$vr_nav = '<li>
		<a class="scrolly" href="#virtual-runs">Virtual Runs</a>
		<ul class="vr_subnav">';

			$i = 0;
			foreach( $event_array as $key => $value ) {
				$vr_nav .= '<li><a href="#tabs-'. $key .'" class="scrolly open-tab" data-tab-index="'. $i .'">'. $value .'</a></li>';
				$i++;
			}
		$vr_nav .= '</ul>
	</li>

	<li><a class="scrolly" href="#phase-bonus">'. vr_get_option( 'bonus_phase_name' ) .'</a></li>';

	if( array_key_exists( $prefix . 'perks', $virtualrun_allmeta ) && $virtualrun_allmeta[$prefix . 'perks'] != '' ) {
		$vr_nav .= '<li><a class="scrolly" href="#runner-perks">Runner Perks</a></li>';
	}

	if( array_key_exists( $prefix . 'faq', $virtualrun_allmeta ) && $virtualrun_allmeta[$prefix . 'faq'] != '' ) {
		$vr_nav .= '<li><a href="#faq" class="scrolly">FAQ</a></li>';
	}
?>
	<nav id="secondary">
		<section class="wrapper">
			<div class="menu-label">Menu</div>
			<ul class="menu">
				<?php
					echo $vr_nav;

					if( wp_is_mobile() && ( array_key_exists( $prefix . 'enable_data_capture', $virtualrun_allmeta ) && $virtualrun_allmeta[$prefix . 'enable_data_capture'] == 'on' ) ) { ?>

						<li class="vr_stayinformed">
							<span>STAY INFORMED</span>
							<p>Subscribe and stay informed on Virtual Run activities</p>
							<?php echo $virtualrun_allmeta[$prefix . 'sidebar_email']; ?>
						</li>
					<?php }
				?>
			</ul>
		</section>
	</nav>


	<main role="main" id="main" class="virtual-run">
		<section class="wrapper grid_2 offset240left">
			<div class="column sidenav stickem">
				<nav class="sticky-nav">
					<ul>
						<?php echo $vr_nav; ?>
					</ul>

					<?php if( array_key_exists( $prefix . 'enable_data_capture', $virtualrun_allmeta ) && $virtualrun_allmeta[$prefix . 'enable_data_capture'] == 'on' ) { ?>

						<div class="subscribe">

							<h4>Stay Informed</h4>
							<p>Subscribe and stay informed on Virtual Run activities</p>

							<?php if( !wp_is_mobile() ) {
								echo $virtualrun_allmeta[$prefix . 'sidebar_email'];
							} ?>

						</div>
					<?php } ?>
				</nav>
			</div> <!-- END .COLUMN.SIDENAV.STICKEM -->

			<div class="column">

				<section id="virtual-runs">
					<?php
						if( array_key_exists( $prefix . 'intro_txt', $virtualrun_allmeta ) && $virtualrun_allmeta[$prefix . 'intro_txt'] != '' ) {
							echo '<div class="vr_text_intro">'
								. apply_filters( 'the_content', $virtualrun_allmeta[$prefix . 'intro_txt'] ) .'
							</div>';
						}


						echo '<div id="phase-tabs">';
							/**
							 * tab navigation
							 */
							echo '<ul class="tabs">';
								$count				= 0;
								$active_tab			= 0;
								$active_tab_change	= 0;
								$active_event		= 'phase1';	// default

								foreach( $event_array as $key => $value ) {

									$date1_unix		= vr_get_option( $prefix . $key . '_date1' );
									$date1_display	= date( 'M d', $date1_unix );
									$date1			= date( 'M d Y', $date1_unix );

									$regdate_start_unix	= vr_get_option( $prefix . $key . '_regdate1' );
									$regdate_start		= date( 'M d Y', $regdate_start_unix );

									$regdate_end_unix	= vr_get_option( $prefix . $key . '_regdate2' );
									$regdate_end		= date( 'M d Y', $regdate_end_unix );

									$date2_unix		= vr_get_option( $prefix . $key . '_date2' );
									$date2_display	= date( 'M d', $date2_unix );
									$date2			= date( 'M d', $date2_unix );

									${$key . '_date1_ext'}		= strtotime( $date1 . ' 12:00:01 AM' );
									${$key . '_date2_ext'}		= strtotime( $date2 . ' 11:59:59 PM' );

									${$key . '_regdate_start'}	= strtotime( $regdate_start . ' 12:00:01 AM' );
									${$key . '_regdate_end'}	= strtotime( $regdate_end . ' 11:59:59 PM' );


									/**
									 * set the way the date will display for all types of solos
									 * for use outside of foreach
									 */
									if( date( 'M', $date1_unix ) == date( 'M', $date2_unix ) ) {
										$date2_day = date( 'd', $date2_unix );
										${'display_date' . $key} = $date1_display .' &ndash; '. $date2_day;
									} else {
										${'display_date' . $key} = $date1_display . ' &ndash; ' . $date2;
									}

									if( ( ${$key . '_date1_ext'} < $today_unix ) && ( $today_unix < ${$key . '_date2_ext'} ) ) {

										$active_event		= $key;
										$active_tab			= $count;	// based on dates
										$active_tab_change	= 1;
										$inactive_tab_class	= '';

									} elseif( $active_tab_change == 0 ) {
										$inactive_tab_class = ' class="vr_inactive"';
									}

									echo '<li>
										<a'. $inactive_tab_class .' role="presentation" data-toggle="tab" href="#tabs-'. $key .'">
											<div>'. strtoupper( $value ) .'</div>
											<span class="vr_upper">'. ${'display_date' . $key} .'</span>
										</a>
									</li>';

									$count++;
								}
							echo '</ul>';


							/**
							 * content for tabs
							 */
							foreach( $event_array as $key => $value ) {

								echo '<div role="tabpanel" class="tab-content" id="tabs-'. $key .'">

									<h2>'. $value .'</h2>';

									if( array_key_exists( $prefix . $key . '_img_id', $virtualrun_allmeta ) && $virtualrun_allmeta[$prefix . $key . '_img_id'] != '' ) {

										echo '<figure class="alignright">
											<a href="'. $virtualrun_allmeta[$prefix . $key . '_img'] .'" class="fancybox" data-fancybox data-caption="'. $value .'">' . wp_get_attachment_image( $virtualrun_allmeta[$prefix . $key . '_img_id'], 'infobox-thumbs' ) .'</a>
											<figcaption class="wp-caption-text gallery-caption">Click to enlarge</figcaption>
										</figure>';

									}

									echo '<p>'. ${'display_date' . $key} .' / '. vr_get_option( $prefix . $key . '_cost' ) .'</p>';

									if( array_key_exists( $prefix . $key . '_desc', $virtualrun_allmeta ) && $virtualrun_allmeta[$prefix . $key . '_desc'] != '' ) {
										echo apply_filters( 'the_content', $virtualrun_allmeta[$prefix . $key . '_desc'] );
									}

									if( $today_unix < ${$key . '_regdate_start'} ) {

										echo '<div style="text-align: center;"><span class="highlight gray">Coming Soon</span></div>';

									} elseif( $today_unix < ${$key . '_regdate_end'} ) {

										$cta_txt = ( array_key_exists( $prefix . $key . '_ctatxt', $virtualrun_allmeta ) && $virtualrun_allmeta[$prefix . $key . '_ctatxt'] != '' ) ? $virtualrun_allmeta[$prefix . $key . '_ctatxt'] : 'Register For';

										echo '<div style="text-align: center;"><a class="highlight red" href="'. vr_get_option( $prefix . $key . '_regurl' ) .'">'. $cta_txt . ' ' . vr_get_option( $prefix . $key . '_cost' ) .'</a></div>';

									} else {

										echo '<div style="text-align: center;"><span class="highlight gray">Registration Over</span></div>';

									}

								echo '</div>';
							}

						echo '</div>';
					?>
				</section>


				<section id="phase-bonus">
					<div class="vr_triangle_down"></div>
					<h2><?php echo vr_get_option( 'bonus_phase_name' ); ?></h2>

					<?php
						if( array_key_exists( $prefix . 'bonus_phase_txt', $virtualrun_allmeta ) && $virtualrun_allmeta[$prefix . 'bonus_phase_txt'] != '' ) {
							echo apply_filters( 'the_content', $virtualrun_allmeta[$prefix . 'bonus_phase_txt'] );
						}


						/**
						 * only show an active link if today's date/time hasn't past the guitar solo's end date
						 */
						if( $today_unix < $phase1_regdate_start ) {

							echo '<p style="text-align: center;"><span class="highlight gray">Coming Soon</span></p>';

						} elseif( vr_get_option( 'bonus_regurl' ) != '' && ( $today_unix < $phase3_regdate_end ) ) {

							$bonus_cta_txt = ( array_key_exists( $prefix . 'bonus_phase_ctatxt', $virtualrun_allmeta ) && $virtualrun_allmeta[$prefix . 'bonus_phase_ctatxt'] != '' ) ? $virtualrun_allmeta[$prefix . 'bonus_phase_ctatxt'] : 'Register For';

							echo '<p style="text-align:center;"><a class="highlight red" href="'. vr_get_option( 'bonus_regurl' ) .'">'. $bonus_cta_txt .' '. vr_get_option( 'bonus_cost' ) .'</a></p>';

						} else {

							echo '<p style="text-align:center;"><span class="highlight gray">Registration Over</span></p>';

						}
					?>
				</section>


				<?php
					$perks = ( array_key_exists( $prefix . 'perks', $virtualrun_allmeta ) && $virtualrun_allmeta[$prefix . 'perks'] != '' )? unserialize( $virtualrun_allmeta[$prefix . 'perks'] ) : '';

					if( !empty( $perks) ) { ?>
						<section id="runner-perks">
							<h2>Runner Perks</h2>

							<?php
								/**
								 * display perks
								 * first 3 will show as columns, after that, show the perk in a full-width row
								 */
								$count = 1;

								if( !empty( $perks ) ) {
									echo '<section class="grid_3_special">';

										foreach( $perks as $key => $entry ) {

											$img = wp_get_attachment_image_src( $entry['image_id'], 'infobox-thumbs' );
											$url = ( isset( $entry['cta_url'] ) ) ? $entry['cta_url'] : '';

											if( $count < 4 ) {

												echo '<div id="perk-'. $count .'" class="column_special">

													<figure>
														<img data-original="'. $img[0] .'" class="lazy" width="'. $img[1] .'" height="'. $img[2] .'">
													</figure>

													<h3>'. $entry['title'] .'</h3>'.
													apply_filters( 'the_content', $entry['description'] );

													if( ( $url != '' ) && ( $url != '' ) ) {
														echo '<p><a class="highlight black" href="'. $entry['cta_url'] .'">'. $entry['cta_txt'] .'</a></p>';
													}

												echo '</div>';

											} else {

												echo '<div id="perk-'. $count .'" class="vr_full_column">

													<figure class="alignleft">
														<img data-original="'. $img[0] .'" class="lazy" width="'. $img[1] .'" height="'. $img[2] .'">
													</figure>

													<h3>'. $entry['title'] .'</h3>'.
													apply_filters( 'the_content', $entry['description'] );

													if( ( $url != '' ) && ( $url != '' ) ) {
														echo '<p><a class="highlight black" href="'. $entry['cta_url'] .'">'. $entry['cta_txt'] .'</a></p>';
													}

												echo '</div>';

											}

											$count++;
										}

									echo '</section>';
								}
							?>
						</section>

					<?php }


					if( array_key_exists( $prefix . 'faq', $virtualrun_allmeta ) && $virtualrun_allmeta[$prefix . 'faq'] != '' ) { ?>
						<section id="faq">
							<h2>FAQ</h2>

							<?php echo apply_filters( 'the_content', $virtualrun_allmeta[$prefix . 'faq'] ); ?>

						</section>
					<?php }
				?>

			</div>
		</section>


		<?php
			/**
			 * medals modal
			 */
			echo '<section id="medals-modal" style="display:none; overflow: visible; background: transparent">
				<div class="wrapper">
					<h2>Finisher Medals</h2>
					<section class="grid_4_special">';

						foreach( $event_array as $key => $value ) {

							$img = wp_get_attachment_image_src( get_post_meta( get_the_ID(), $prefix . $key . '_medal_id', 1 ), 'finisher-medals' );

							echo '<div class="column_special">

								<figure>
									<img src="'. $img[0] .'">
								</figure>

								<h3>'. strtoupper( $value ) .'</h3>
								<p>'. ${'display_date' . $key} .'</p>';

								if( $today_unix < ${$key . '_regdate_start'} ) {

									echo '<p style="text-align: center;"><span class="highlight gray">Coming Soon</span></p>';

								} elseif( $today_unix < ${$key . '_regdate_end'} ) {

									echo '<div style="text-align: center;"><a class="highlight red" href="'. vr_get_option( $prefix . $key . '_regurl' ) .'">Register</a></div>';

								} else {

									echo '<div style="text-align: center;"><span class="highlight gray">Registration Over</span></div>';

								}

							echo '</div>';
						}

						$img = wp_get_attachment_image_src( get_post_meta( get_the_ID(), $prefix . 'bonus_phase_medal_id', 1 ), 'finisher-medals' );

						echo '<div class="column_special">

							<figure>
								<img src="'. $img[0] .'">
							</figure>

							<h3 class="vr_gap">'. vr_get_option( 'bonus_phase_name' ) .'</h3>';


							/**
							 * coming soon before the date starts
							 * then end it when guitar solo ends
							 */
							if( $today_unix < $phase1_regdate_start ) {

								echo '<p style="text-align: center;"><span class="highlight gray">Coming Soon</span></p>';

							} elseif( $today_unix < $phase3_regdate_end ) {

								echo '<div style="text-align: center;"><a class="highlight red" href="'. vr_get_option( 'bonus_regurl' ) .'">Register</a></div>';

							} else {

								echo '<div style="text-align: center;"><span class="highlight gray">Registration Over</span></div>';

							}
						echo '</div>

					</section>
				</div>
			</section>';


			/**
			 * virtual bib modal
			 */
			echo '<section id="bibs-modal" style="display:none; overflow: visible; background: transparent">
				<div class="wrapper">

					<h2>Virtual Bibs</h2>

					<section class="grid_4_special">';

						foreach( $event_array as $key => $value ) {

							echo '<div class="column_special">';

								if( array_key_exists( $prefix . $key . '_bib_img_id', $virtualrun_allmeta ) && $virtualrun_allmeta[$prefix . $key . '_bib_img_id'] != '' ) {

									$img = wp_get_attachment_image_src( $virtualrun_allmeta[$prefix . $key . '_bib_img_id'], 'finisher-medals' );

									echo '<figure>
										<img src="'. $img[0] .'">
									</figure>';

								}

								echo '<h3>'. strtoupper( $value ) .'</h3>
								<p>'. ${'display_date' . $key} .'</p>';

								if( array_key_exists( $prefix . $key . '_bib_pdf', $virtualrun_allmeta ) && $virtualrun_allmeta[$prefix . $key . '_bib_pdf'] != '' ) {

									echo '<div style="text-align: center;"><a target="_blank" class="highlight red" href="'. $virtualrun_allmeta[$prefix . $key . '_bib_pdf'] .'">Download</a></div>';

								} else {

									echo '<div style="text-align: center;"><span class="highlight gray">Available Soon</span></div>';

								}

							echo '</div>';
						}


						echo '<div class="column_special">';

							if( array_key_exists( $prefix . 'bonus_phase_bib_img_id', $virtualrun_allmeta ) && $virtualrun_allmeta[$prefix . 'bonus_phase_bib_img_id'] != '' ) {

								$img = wp_get_attachment_image_src( $virtualrun_allmeta[$prefix . 'bonus_phase_bib_img_id'], 'finisher-medals' );

								echo '<figure>
									<img src="'. $img[0] .'">
								</figure>';

							}


							echo '<h3 class="vr_gap">'. vr_get_option( 'bonus_phase_name' ) .'</h3>';

							if( ( $bibpdf = get_post_meta( get_the_ID(), $prefix . 'bonus_phase_bib_pdf', 1 ) ) != '' ) {
								echo '<div style="text-align: center;"><a target="_blank" class="highlight red" href="'. $bibpdf .'">Download</a></div>';
							} else {
								echo '<div style="text-align: center;"><span class="highlight gray">Available Soon</span></div>';
							}

						echo '</div>

					</section>
				</div>
			</section>';

		?>

	</main>


	<script>
		$(function() {
			/**
			 * check if a hash was added in the url
			 * ex: "#tabs-phase2"
			 * trying to link to a particular tab
			 * otherwise, just go to the active tab
			 */
			if( window.location.hash ) {
				$( '#phase-tabs' ).tabs( {
					activate: function( event, ui ) {
						var url = window.location;
						window.history.pushState( {
							'html': '',
							'pageTitle': title,
						}, '', url.href.replace( url.hash, '' ) + ui.newTab.context.hash );
					}
				});
			} else {
				$( '#phase-tabs' ).tabs( {
					active: <?php echo $active_tab; ?>,
				});
			}

			$( '#phase-tabs' ).show();

			$('.open-tab').click(function (event) {
				event.preventDefault();
				$('#phase-tabs').tabs('option', 'active', $(this).data('tab-index'));
			});

			$( 'img.lazy' ).lazyload({
				effect: 'fadeIn',
			});

		});

		$(document).ready(function(){
			/**
			 * This part causes smooth scrolling using scrollto.js
			 * We target all a tags inside the nav, and apply the scrollto.js to it.
			 */
			$("a.scrolly").click(function(evn){
				evn.preventDefault();
				$('html,body').scrollTo(this.hash, this.hash);
			});

			/*$('main').stickem({
				offset: 50,
				container: '.offset240left',
			});*/

			$(".fancybox").fancybox({
				fullScreen: false
			});


			/**
			 * in the future, you can cut these next two fancybox inits to one
			 * just use the same class for each link
			 */
			$(".medals-modal-link").fancybox({
				baseTpl: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
					'<div class="fancybox-bg"></div>' +
					'<div class="fancybox-controls">' +
						'<div class="fancybox-infobar">' +
							'<button data-fancybox-previous class="fancybox-button fancybox-button--left" title="Previous"></button>' +
							'<div class="fancybox-infobar__body">' +
								'<span class="js-fancybox-index"></span>&nbsp;/&nbsp;<span class="js-fancybox-count"></span>' +
							'</div>' +
							'<button data-fancybox-next class="fancybox-button fancybox-button--right" title="Next"></button>' +
						'</div>' +
						'<div class="fancybox-buttons">' +
							'<button data-fancybox-close class="fancybox-button fancybox-button--close" title="Close (Esc)"></button>' +
						'</div>' +
					'</div>' +
					'<div class="fancybox-slider-wrap fancybox-medals-wrap">' +
						'<div class="fancybox-slider"></div>' +
					'</div>' +
					'<div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div>' +
				'</div>',
			});

			$(".bibs-modal-link").fancybox({
				baseTpl: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
					'<div class="fancybox-bg"></div>' +
					'<div class="fancybox-controls">' +
						'<div class="fancybox-infobar">' +
							'<button data-fancybox-previous class="fancybox-button fancybox-button--left" title="Previous"></button>' +
							'<div class="fancybox-infobar__body">' +
								'<span class="js-fancybox-index"></span>&nbsp;/&nbsp;<span class="js-fancybox-count"></span>' +
							'</div>' +
							'<button data-fancybox-next class="fancybox-button fancybox-button--right" title="Next"></button>' +
						'</div>' +
						'<div class="fancybox-buttons">' +
							'<button data-fancybox-close class="fancybox-button fancybox-button--close" title="Close (Esc)"></button>' +
						'</div>' +
					'</div>' +
					'<div class="fancybox-slider-wrap fancybox-medals-wrap">' +
						'<div class="fancybox-slider"></div>' +
					'</div>' +
					'<div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div>' +
				'</div>',
			});
		});

	</script>


<?php get_footer('series'); ?>
