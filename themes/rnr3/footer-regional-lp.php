<?php
	/**
	 * global variables
	 */
	$twitter	= rnr_get_option( 'rnrgv_twitter');
	$instagram	= rnr_get_option( 'rnrgv_instagram');
	$pinterest	= rnr_get_option( 'rnrgv_pinterest');
	$youtube	= rnr_get_option( 'rnrgv_youtube');

	if( get_post_meta( get_the_ID(), '_rnr3_reg_lp_gototop', 1 ) == 'on' ) {
		echo '<a href="#" id="goTop">
			<span class="icon-up-open"></span>
		</a>';
	}
?>

		<!-- combined footers -->
		<footer>

			<section class="wrapper">

				<!-- global footer -->
				<section id="globalfooter">
					<a href="<?php echo rnr_get_option( 'siteurl' ); ?>"><div id="footerlogo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo_footer@2x.png" alt="Rock 'N' Roll Marathon Series logo"></div></a>
					<p>&copy; <?php echo date('Y'); ?> &ndash; Competitor Group, Inc. All Rights Reserved. <a href="<?php echo network_site_url('/about-competitor-group/'); ?>">About</a> | <a href="http://competitorgroup.com/privacy-policy/" target="_blank">Privacy and Cookie Policy</a> | <a href="http://competitorgroup.com/privacy-policy#third-parties" target="_blank">AdChoices</a> | <a href="http://competitorgroup.com/" target="_blank">Media Kit</a> | <a href="https://careers-competitorgroup.icims.com/jobs/intro?hashed=0" target="_blank">Careers</a></p>
				</section>

			</section>
		</footer> <!-- /combined footers -->

	</div><!-- /container -->

	<?php wp_footer(); ?>

</body>
</html>
