<?php
	/**
	 * Template Name: Running Sale
	 * one day only, no phases
	 * no alert bar, no interstitial
	 */

	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$prefix			= '_rnr3_';
	$current_date	= date('YmdH');

	// append midnight (pacific time from utc)
	$phase_start	= date( 'Ymd', get_post_meta( get_the_ID(), $prefix . 'phasestart', 1 ) ) . '08';
	$phase_end		= date( 'Ymd', get_post_meta( get_the_ID(), $prefix . 'phaseend', 1 ) ) . '08';

	if( $current_date >= $phase_start && $current_date < $phase_end ) {
		$current_phase = 'phase_go';
	} else {
		$current_phase = 'phase_stop';
	}

	get_header( 'special' );

	// $market = get_market2();
	// if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
	// 	$event_info = rnr3_get_event_info( $market );
	// }

	$twitter_hashtag	= $event_info->twitter_hashtag;
	$qt_lang			= rnr3_get_language();

?>

	<!-- main content -->
	<main role="main" id="main" class="no-hero running-sale">
		<section id="deals">

			<?php
				if( $current_phase == 'phase_go' ) {
					$marquee_desktop	= get_post_meta( get_the_ID(), $prefix . 'marquee_big', 1 );
					$marquee_mobile		= get_post_meta( get_the_ID(), $prefix . 'marquee_small', 1 );
				} else {
					$marquee_desktop	= get_post_meta( get_the_ID(), $prefix . 'after_marquee_big', 1 );
					$marquee_mobile		= get_post_meta( get_the_ID(), $prefix . 'aftermarquee_small', 1 );
				}

				/**
				 * show a different marquee image if qtranslate-x is enabled AND language is not english
				 */
				if( $qt_lang['enabled'] == 1 ) {
					if( $qt_lang['lang'] != 'en' ) {
						$marquee_desktop	= str_replace( '.jpg', '_' . $qt_lang['lang'] . '.jpg', $marquee_desktop );
						$marquee_mobile		= str_replace( '.jpg', '_' . $qt_lang['lang'] . '.jpg', $marquee_mobile );
					}
				}

				echo '<div class="marquee">
					<div class="showdesktop"><img src="'. $marquee_desktop .'"></div>
					<div class="showmobile"><img src="'. $marquee_mobile .'"></div>
				</div>';
			?>

			<section class="wrapper">

				<?php
					if( $current_phase == 'phase_go' ) {

						echo apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'intro', 1 ) );

						$findrace_content = specialoffers_findrace();

						echo $findrace_content['text'];

					} else {

						echo apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'aftersale', 1 ) );

					}

				?>

			</section>
		</section>


		<script src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/js/vendor/mixitup.min.js"></script>
		<script>
			var containerEl = document.querySelector('.mix_group');

			var mixer = mixitup(containerEl, {
				load: {
					sort: 'featured:desc date:asc'
				}
			});

			const sortSelect = document.querySelector( '#sortselect' );

			sortSelect.onchange = function() {
				mixer.sort( this.value );

			};

		</script>

	</main>

<?php
	get_footer( 'series' );


	/* template specific functions */

	/**
	 * FUNCTION: template-specific
	 * returns find a race mixitup grid ( index: 'text' )
	 * will also return count of grid items ( index: 'count' )
	 * transient: 5 minute cache
	 */
	function specialoffers_findrace() {

		$market = get_market2();

		if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
			$event_info = rnr3_get_event_info( $market );
		}

		$prefix				= '_rnr3_';
		$content			= array();
		$content['text']	= '';
		$qt_lang			= rnr3_get_language();
		include 'languages.php';

		if ( false === ( $promo_locations = get_transient( 'promo_locations_results' ) ) ) {
			$args = array(
				'post_type'			=> 'running_sale',
				'posts_per_page'	=> -1,
				'post_status'		=> 'publish',
			);
			$promo_locations = new WP_Query( $args );
			set_transient( 'promo_locations_results', $promo_locations, 15 * MINUTE_IN_SECONDS );
		}

		$modal_count = 1;

		if( $promo_locations->have_posts() ) {

			$content['text'] .= '<section id="findarace">
				<h3 class="subtitle">'. $txt_find_a_race .'</h3>
				<div class="controls">
					<label>'. $sortby_txt .'</label>
					<div id="sort">
						<select id="sortselect">
							<option value="featured:desc date:asc">'. $txt_date .'</option>
							<option value="featured:desc city:asc">'. $city_txt .'</option>
						</select>
					</div>

					<label>'. $txt_distance .'</label>
					<button class="filter" data-filter="all">'. $all_txt .'</button>
					<button class="filter" data-filter=".fivek">'. $fivek_txt .'</button>
					<button class="filter" data-filter=".tenk">'. $tenk_txt .'</button>
					<button class="filter" data-filter=".half-marathon">'. $halfmarathon_txt .'</button>
					<button class="filter" data-filter=".marathon">'. $marathon_txt .'</button>
					<button class="filter" data-filter=".relay">'. $relay_txt .'</button>
				</div>


				<div class="mix_group">';

					while( $promo_locations->have_posts() ) {
						$promo_locations->the_post();

						// set distance and price arrays to be used as keys for mixitup and display in modal
						$distance_display = $distance_key = $price_array = $savings_array = array();

						$entries = get_post_meta( get_the_ID(), $prefix . 'race_group', 1 );

						if( !empty( $entries ) ) {

							foreach( $entries as $key => $entry ) {
								$distance_pieces	= isset( $entry['type'] ) ? explode( '::', $entry['type'] ) : '';
								$distance_key[]		= ( $distance_pieces != '' ) ? $distance_pieces[0] : '';
								$distance_display[]	= ( $distance_pieces != '' ) ? $distance_pieces[1] : '';

								$price_array[]		= isset( $entry['price'] ) ? $entry['price'] : '';
								$savings_array[]	= isset( $entry['savings'] ) ? $entry['savings'] : '';
							}

						}

						$event_distances		= implode( ' ', $distance_key );
						$festival				= get_post_meta( get_the_ID(), $prefix . 'festival', 1 );
						$start_date_timestamp	= get_post_meta( get_the_ID(), $prefix . 'startdate', 1 );
						$end_date_timestamp		= get_post_meta( get_the_ID(), $prefix . 'enddate', 1 );
						$is_tbd					= get_post_meta( get_the_ID(), $prefix . 'is_tbd', 1 );
						$is_limited				= get_post_meta( get_the_ID(), $prefix . 'limited', 1 );


						/**
						 * check if qtranslate-x is enabled
						 * handles dates
						 * display dates like F j (or F j-j) for english
						 */
						if( $qt_lang['enabled'] == 1 ) {
							$start_date_d			= date( 'j', get_post_meta( get_the_ID(), $prefix . 'startdate', 1 ) );
							$start_date_m_foreign	= get_day_or_month_by_language( $qt_lang['lang'], ( date( 'n', get_post_meta( get_the_ID(), $prefix . 'startdate', 1 ) ) - 1 ), 'month_long' );

							if( $qt_lang['lang'] == 'en' ) {
								$start_date = $start_date_m_foreign . ' ' . $start_date_d;
							} else {
								$start_date = $start_date_d . ' ' . $start_date_m_foreign;
							}

							if( $festival == 'on' ) {

								$end_date_d			= date( 'j', get_post_meta( get_the_ID(), $prefix . 'enddate', 1 ) );
								$end_date_m_foreign	= get_day_or_month_by_language( $qt_lang['lang'], ( date( 'n', get_post_meta( get_the_ID(), $prefix . 'enddate', 1 ) ) - 1 ), 'month_long' );

								if( $qt_lang['lang'] == 'en' ) {
									if( date( 'M', $start_date_timestamp ) != ( date( 'M', $end_date_timestamp) ) ) {
										// diff months
										$end_date		= $end_date_m_foreign . ' ' . $end_date_d;
										$display_date	= $start_date . '-' . $end_date;
									} else {
										// same month
										$display_date = $end_date_m_foreign . ' ' . $start_date_d . '-' . $end_date_d;
									}
								} else {
									if( date( 'M', $start_date_timestamp ) != ( date( 'M', $end_date_timestamp) ) ) {
										// diff months
										$end_date		= $end_date_d . ' ' . $end_date_m_foreign;
										$display_date	= $start_date . '-' . $end_date;
									} else {
										// same month
										$display_date = $start_date_d . '-' . $end_date_d . ' ' . $end_date_m_foreign;
									}
								}

							} elseif( $is_tbd == 'on' ) {

								$display_date			= 'TBD';
								// jun 10, 2020 -- arbitrary date because it NEEDS a date, but they want tbd
								$start_date_timestamp	= '1591747200';

							} else {

								$display_date = $start_date;

							}

						} else {
							if( $is_tbd != 'on' ) {
								// qt translate off
								$start_date = date( 'M d', get_post_meta( get_the_ID(), $prefix . 'startdate', 1 ) );
							}

							if( $festival == 'on' ) {

								if( date( 'M', $start_date_timestamp ) != ( date( 'M', $end_date_timestamp) ) ) {

									// diff months
									$end_date		= date( 'M d', $end_date_timestamp );
									$display_date	= $start_date . '-' . $end_date;

								} else {

									// same month
									$display_date = $start_date . '-' . date( 'd', $end_date_timestamp );

								}

							} elseif( $is_tbd == 'on' ) {

								$display_date = 'TBD';
								// jun 10, 2020 -- arbitrary date because it NEEDS a date, but they want tbd
								$start_date_timestamp	= '1591747200';

							} else {

								$display_date = $start_date;

							}
						}


						$display_city	= remove_accents( get_the_title() );
						$display_city	= strtolower( str_replace( array( ' ', '.' ), '-', $display_city ) );
						$saveupto		= get_post_meta( get_the_ID(), $prefix . 'saveupto', 1 );


						/**
						 * check for spaces
						 * spaces will denote special currency like "mxn" or "cad"
						 */
						if( preg_match( '/\s/ ', $saveupto ) == 1 ) {
							$saveupto_explode	= explode( ' ', $saveupto );
							$saveupto			= $saveupto_explode[0] . ' <span class="mix-price-currency">' . $saveupto_explode[1] .'</span>';
						}


						/**
						 * treatment for featured races
						 */
						if( get_post_meta( get_the_ID() , $prefix . 'featured', 1 ) == 'on' ) {
							$is_featured	= 1;
							$featured_class	= ' mix-featured';
						} else {
							$is_featured	= 0;
							$featured_class	= '';
						}


						/**
						 * checking for canada/mexico isn't necessary anymore since we aren't using slanted price tags
						 * user can enter CAD or MXN as needed
						 */
						/*
							$can_mex = get_post_meta( get_the_ID(), $prefix . 'canada_mexico', 1 );

							if( $can_mex == 'canada' ) {

								$can_mex_class		= ' canmex_class';
								$can_mex_currency	= '<span class="canmex_price">CAD</span>';

							} elseif( $can_mex == 'mexico' ) {

								if( strlen( $saveupto ) > 3 ) {
									$can_mex_class		= ' canmex_class big_price_class';
								} else {
									$can_mex_class		= ' canmex_class';
								}
								$can_mex_currency	= '<span class="canmex_price">MXN</span>';

							} else {
								$can_mex_class		= '';
								$can_mex_currency	= '';
							}
						*/

						$content['text'] .= '<div class="running-sale-mix mix '. $event_distances .' event-group-'. $display_city . $featured_class .'" data-city="'. $display_city .'" data-date="'. $start_date_timestamp .'" data-featured="'. $is_featured .'">

							<div class="inner_mix">

								<h3 class="mix-title">'. get_the_title() .'</h3>

								<div class="mix-bottom">

									<p class="mix-date">'. $display_date .'</p>';

									/**
									 * since price verbiage is a new meta option, default it to "As Low As"
									 * to prevent the need to go through each CPT and click "Update"
									 */
									$priceverbiage_txt = get_post_meta( get_the_ID(), $prefix . 'price_verbiage', 1 ) != '' ? get_post_meta( get_the_ID(), $prefix . 'price_verbiage', 1 ) : 'As Low As';

									if( $priceverbiage_txt == 'off' ) {

										// don't show price verbiage nor price
										$content['text'] .= '
											<p class="mix-price-head">&nbsp;</p>
											<p class="mix-price-price">&nbsp;</p>
											<p class="mix-price-limited">&nbsp;</p>
										';

									} else {

										if( $is_limited == 'on' ) {

											// limited price tag treatment (show "limited supply")
											$content['text'] .= '
												<p class="mix-price-head">'. $priceverbiage_txt .'</p>
												<p class="mix-price-price">'. $saveupto .' </p>
												<p class="mix-price-limited">'. $limitedsupply_txt .'</p>
											';

										} else {

											// all other price tags (show savings)
											$content['text'] .= '
												<p class="mix-price-head">'. $priceverbiage_txt .'</p>
												<p class="mix-price-price">'. $saveupto .' </p>
											';

											if( ( $tilesavings = get_post_meta( get_the_ID(), $prefix . 'tilesavings', 1 ) ) != '' ) {

												$content['text'] .= '
													<p class="mix-price-savings">'. $tilesavings .' '. $savings_txt .'</p>
												';

											} else {
												$content['text'] .= '<p class="mix-price-savings">&nbsp;</p>';
											}
										}

									}


									/**
									 * open modal when cta is clicked
									 */
									$cta_html	= '<a data-fancybox class="mix-cta fancybox" data-post-id="'. get_the_ID() .'" data-rnr-event-slug="'. $display_city .'" data-src="#modal-'. $display_city .'" href="javascript:;">'. $registernow_txt .'</a>';

									$content['text'] .= '<div class="mix-get_deets">'. $cta_html .'</div>


									<div class="mix-img">'.
										get_the_post_thumbnail() .'
									</div>

								</div>
							</div>';


							/**
							 * event modal
							 */
							$content['text'] .= '<div style="display:none; max-width: 90%; width: 800px;" class="mix-modal" id="modal-'. $display_city .'">
								<div class="mix-modal_inside">';

									if( get_post_meta( get_the_ID(), $prefix . 'modal_gallery', 1 ) == '' && get_post_meta( get_the_ID(), $prefix . 'modal_bulleted_txt', 1 ) == '' ) {

										$content['text'] .= '<div class="grid_1_modal">';
										$modal_column_count = 1;

									} else {

										$content['text'] .= '<div class="grid_2_modal">';
										$modal_column_count = 2;

									}
										$content['text'] .= '<div class="column">';

											$display_date_year = ( get_post_meta( get_the_ID(), $prefix . 'enddate', 1 ) != '' ) ? get_post_meta( get_the_ID(), $prefix . 'enddate', 1 ) : get_post_meta( get_the_ID(), $prefix . 'startdate', 1 );
											$display_date_year = ( $is_tbd ) ? '' : date( 'Y', $display_date_year );

											$content['text'] .= '<h2 class="mix-modal-title">'. get_the_title() .'</h2>

											<p class="mix-modal-date">'. $display_date . ', ' . $display_date_year .'</p>
											'. apply_filters( 'the_content', get_the_content() );

											if( $distance_display[0] != '' ) {

												$content['text'] .= '<ul class="mix-modal_prices">';
													// show distances & prices, which were set up above
													for( $x = 0; $x < count( $distance_display ); $x++ ) {

														switch( $distance_display[$x] ) {
															case '1 Mile':
																$distance_translated = $onemile_txt;
																break;
															case '1K';
																$distance_translated = $onek_txt;
																break;
															case '5K';
																$distance_translated = $fivek_txt;
																break;
															case '10K';
																$distance_translated = $tenk_txt;
																break;
															case '11K';
																$distance_translated = $elevenk_txt;
																break;
															case '20K';
																$distance_translated = $twentyk_txt;
																break;
															case 'Mini';
																$distance_translated = 'Mini';
																break;
															case 'Fun Run';
																$distance_translated = 'Fun Run';
																break;
															case 'Relay';
																$distance_translated = $relay_txt;
																break;
															case 'Half Marathon';
																$distance_translated = $halfmarathon_txt;
																break;
															case 'Marathon';
																$distance_translated = $marathon_txt;
																break;
															case 'Winter Runningland';
																$distance_translated = 'Winter Runningland';
																break;
															case 'KiDS ROCK';
																$distance_translated = 'KiDS ROCK';
																break;
														}

														/**
														 * prices in modal
														 */
														$content['text'] .= '<li>
															<strong>'. $distance_translated . ' - <span class="price">' . $price_array[$x] . '</span></strong>';
															if( isset( $savings_array[$x] ) && $savings_array[$x] != '' ) {
																$content['text'] .= '<br><span class="mix-modal-savings">'. $savings_array[$x] .' '. $savings_txt .'</span>';
															}
														$content['text'] .= '</li>';
													}

												$content['text'] .= '</ul>';
											}


											$cta_modaltxt = $registernow_txt;

											if( $is_tbd == 'on' ) {
												$cta_modaltxt = $getdetails_txt;
											}

											$content['text'] .= '<p><a target="_blank" href="'. get_post_meta( get_the_ID(), $prefix . 'register_url', 1 ) .'" class="mix-modal-cta">'. $cta_modaltxt .'</a></p>
										</div>';


										if( $modal_column_count == 2 ) {
											$content['text'] .= '<div class="column">

												<div id="gallery-carousel-'. $modal_count .'" class="mix-modal-carousel owl-carousel"></div>

												<div class="moreinfo">'.
													apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'modal_bulleted_txt', 1 ) ) .'
												</div>

											</div>';
										}

									$content['text'] .= '</div>
								</div>
							</div>
						</div>';

						$modal_count++;
					}

					$content['text'] .= '<div class="gap"></div>
						<div class="gap"></div>
						<div class="gap"></div>
						<div class="gap"></div>
				</div>
			</section>';

			wp_reset_postdata();
		}

		$content['count'] = $modal_count;
		return $content;
	}


?>
