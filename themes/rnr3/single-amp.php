<?php
	// global variables
	global $wpdb;
	global $multipage;

	$twitter	= rnr_get_option( 'rnrgv_twitter' );
	$instagram	= rnr_get_option( 'rnrgv_instagram' );
	$youtube	= rnr_get_option( 'rnrgv_youtube' );

	// get FB series URL
	$series_info = $wpdb->get_row(
		"SELECT facebook_page FROM wp_rocknroll_events WHERE event_slug = 'series'"
	);

	$facebook	= !empty($series_info->facebook_page) ? $series_info->facebook_page : '';

	// check language and get our languages
	$qt_lang = rnr3_get_language();
	include 'languages.php';
?>
<!doctype html>
<html amp <?php echo AMP_HTML_Utils::build_attributes_string( $this->get( 'html_tag_attributes' ) ); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php do_action( 'amp_post_template_head', $this ); ?>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700|Open+Sans:300,400,600,400italic|Crimson+Text" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<style amp-custom><?php do_action( 'amp_post_template_css', $this ); ?></style>
</head>

<body <?php body_class('amp'); ?>>

	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'rrnr3' ); ?></a>

	<header class="header">
		<div class="container header__container">
			<div class="row">
				<div class="col-xs-6">
					<h1 class="screen-reader-text"><?php bloginfo('name'); ?></h1>
					<div class="header__logo">
						<a href="<?php echo site_url( '/' ); ?>">
							<?php
								$sitelogo = preg_replace('/https?:/', '', get_template_directory_uri() .'/img/logo@2x.png' );
							?>
							<amp-img src="<?php echo $sitelogo; ?>" alt="Run Rock 'n' Roll Logo" height="25" width="135"></amp-img>
						</a>
					</div>
				</div>
				<div class="col-xs-6">
					<nav id="social-nav" class="social-nav">
						<ul class="social-nav__menu">
							<?php if( !empty( $twitter ) ): ?>
								<li class="social-nav__menu-item">
									<a href="<?php echo $twitter; ?>">
										<span class="screen-reader-text">Twitter</span>
										<span class="fa fa-twitter"></span>
									</a>
								</li>
							<?php endif; ?>
							<?php if( !empty( $facebook ) ): ?>
							<li class="social-nav__menu-item">
								<a href="<?php echo $facebook; ?>">
									<span class="screen-reader-text">Facebook</span>
									<span class="fa fa-facebook"></span>
								</a>
							</li>
							<?php endif; ?>
							<?php if( !empty( $instagram ) ): ?>
							<li class="social-nav__menu-item">
								<a href="<?php echo $instagram; ?>">
									<span class="screen-reader-text">Instagram</span>
									<span class="fa fa-instagram"></span>
								</a>
							</li>
							<?php endif; ?>
							<?php if( !empty( $youtube ) ): ?>
							<li class="social-nav__menu-item">
								<a href="<?php echo $youtube; ?>">
									<span class="screen-reader-text">YouTube</span>
									<span class="fa fa-youtube-play"></span>
								</a>
							</li>
							<?php endif; ?>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>

	<section id="content" class="content template--single">
		<div class="container content__container">
			<div class="row">


				<article <?php post_class( array( 'article', 'article_type_single' ) ); ?>>

					<?php
						if ( have_posts() ): while ( have_posts() ): the_post();

							$prevent_featured_image	= !empty( $post_metadata['_prevent_featured_image'] ) ? $post_metadata['_prevent_featured_image'][0] : '';
							$prevent_author_bio		= !empty( $post_metadata['_prevent_author_bio'] ) ? $post_metadata['_prevent_author_bio'][0] : '';

							// lets make the author markup
							if ( !empty( $post_metadata['_guest_author'] ) ) {

								$post_author		= $post_metadata['_guest_author'][0];
								$post_author_markup	= sprintf( '<span class="author">%s</span>', $post_author );

							} else {

								$post_author		= get_the_author();
								$post_author_link	= get_author_posts_url( get_the_author_meta('ID') );
								$post_author_markup	= sprintf( '<a href="%s" class="author">%s</a>', $post_author_link, $post_author );

							}

							// lets make the category markup
							$primary_cat	 	= get_primary_cat( 'all' );
							$primary_cat_name	= $primary_cat->name;
							$primary_cat_url	= get_category_link( $primary_cat->cat_ID );
						?>

						<?php
							if ( has_post_thumbnail() && $prevent_featured_image != 'on' ) {
								$thumb_id				= get_post_thumbnail_id( get_the_ID() );
								$thumb_array			= wp_get_attachment_image_src( $thumb_id, 'medium' );
								$featured_image_info	= get_post( get_post_thumbnail_id() ); ?>

							<figure class="article__thumbnail">

								<amp-img src="<?php echo $thumb_array[0]; ?>" layout="responsive" width="<?php echo $thumb_array[1]; ?>" height="<?php echo $thumb_array[2]; ?>" ></amp-img>

								<?php if ( isset($featured_image_info->post_excerpt) && $featured_image_info->post_excerpt != '' ) {
									printf( '<figcaption>%s</figcaption>', $featured_image_info->post_excerpt );
								} ?>

							</figure>

						<?php } ?>

						<div class="article__details">

							<?php the_title( '<h2 class="article__title">', '</h2>' ); ?>
							<?php echo ( has_excerpt() ) ? '<h5 class="article__subheading">'. get_the_excerpt() .'</h5>' : '' ?>
							<div class="article__meta"><?php echo $by_txt .' '. $post_author_markup; ?><a href="<?php echo $primary_cat_url; ?>" class="category"><?php echo $primary_cat_name; ?></a></div>

						</div>


						<section class="article__content">
							<?php echo $this->get( 'post_amp_content' ); ?>
						</section>

					<?php endwhile; endif; ?>

					<section class="article-footer">

						<section class="article__categories">

							<h4><?php echo $browse_by_cat_txt; ?>:</h4>

							<ul class="article__categories__list">

								<?php
									$cats	= get_the_category();

									foreach( $cats as $cat ) {
										$cat_url	= get_category_link( $cat->cat_ID );
										$cat_title	= $cat->name; ?>

									<li class="article__categories__item"><a href="<?php echo $cat_url; ?>"><?php echo $cat_title; ?></a></li>

								<?php } ?>

							</ul>

						</section>

						<?php if ( $prevent_author_bio !== 'on' ) : ?>

						<section class="article__author-bio">

							<?php
								$authorid		= get_the_author_meta( 'ID' );
								$authormeta		= get_user_meta( $authorid );
								$avatarurl		= !empty( $authormeta['_rnr3_avatar'] ) ? $authormeta['_rnr3_avatar'][0] : '';
								// $avatarid		= $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE guid='%s';", $avatarurl ) );
								$avatarid		= !empty( $authormeta['_rnr3_avatar_id'] ) ? $authormeta['_rnr3_avatar_id'][0] : '';
								$avatarsized	= wp_get_attachment_image_src( $avatarid, 'thumbnail' );
								$gravatar		= get_avatar_url( $authorid );
								$displayname	= get_the_author_meta( 'display_name' );
								$authorurl		= get_author_posts_url( $authorid );
								$bio			= apply_filters( 'the_content', get_the_author_meta( 'description' ) );

								/**
								 * use gravatar as a fallback if no user profile image is uploaded
								 */
								if( $avatarsized[0] ) {
									$author_avatar = '<amp-img src="'. $avatarsized[0] .'" layout="fixed" width="'. $avatarsized[1] .'" height="'. $avatarsized[2] .'" ></amp-img>';
								} else {
									$author_avatar = '<amp-img src="'. $gravatar .'" layout="fixed" width="96" height="96"></amp-img>';
								}
								?>

								<div class="col-xs-3 article__author-bio__thumbnail">
									<a href="<?php echo $authorurl; ?>"><?php echo $author_avatar; ?></a>
								</div>

								<div class="article__author-bio__text">
									<h3 class="article__author-bio__name"><?php echo $author_txt; ?>: <a href="<?php echo $authorurl; ?>"><?php echo $displayname; ?></a></h3>

									<?php echo $bio; ?>

									<a class="article__author-bio__posts-link" href="<?php echo $authorurl; ?>"><?php echo $author_posts_by_txt . $displayname; ?></a>

								</div>

						</section>

						<?php endif; ?>

					</section><!-- END ARTICLE-FOOTER -->

				</article>

			</div>
		</div>
	</section>

	<footer class="footer">
		<div class="footer__copyright">
			<p>&copy; <?php echo date( 'Y' );?> COMPETITOR GROUP, INC. ALL RIGHTS RESERVED.</p>
		</div>
	</footer>

	<?php do_action( 'amp_post_template_footer', $this ); ?>

</body>
</html>
