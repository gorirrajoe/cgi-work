<?php
	/**
	 * global variables
	 */
	$twitter	= rnr_get_option( 'rnrgv_twitter');
	$instagram	= rnr_get_option( 'rnrgv_instagram');
	$pinterest	= rnr_get_option( 'rnrgv_pinterest');
	$youtube	= rnr_get_option( 'rnrgv_youtube');
	$market		= get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

?>
		<a href="#" id="goTop">
			<span class="icon-up-open"></span>
		</a>

		<!-- combined footers -->
		<footer>

			<section class="wrapper">

			<!-- event footer -->
			<nav id="seriesfooter" role="navigation">

				<?php /*
					$menu_names = array( 'primary-series', 'global' );

					$menu_list = '<ul class="footer-nav">';
						foreach( $menu_names as $menu_name ) {
							$menu = wp_get_nav_menu_object( $menu_name );
							$menu_items = wp_get_nav_menu_items( $menu->term_id );

								foreach( $menu_items as $key => $menu_item ) {
									$title = $menu_item->title;
									$url = $menu_item->url;
									$menu_list .= '<li><a href="'.$url.'">'.$title.'</a></li>';
								}
						}
					$menu_list .= '</ul>';
					echo $menu_list;
				*/ ?>
				<ul class="footer-nav">
					<li><a href="<?php echo network_site_url('/about-the-rock-n-roll-marathon-series/'); ?>">About</a></li>
					<li><a href="<?php echo network_site_url('/press/'); ?>">Press</a></li>
					<li><a href="<?php echo network_site_url('/results/'); ?>">Race Results</a></li>
					<li><a href="<?php echo network_site_url('/expo/'); ?>">Expo Vendor</a></li>
					<li><a href="<?php echo network_site_url('/partners/'); ?>">Partners</a></li>
					<li><a href="<?php echo network_site_url('/charities/'); ?>">Charity</a></li>
					<li><a href="<?php echo network_site_url('/contact/'); ?>">Contact</a></li>
					<li><a href="<?php echo network_site_url('/tempo/'); ?>">Blog</a></li>
					<li><a href="<?php echo network_site_url('/elite-athlete-program/'); ?>">Elite Athletes</a></li>
					<li><a href="https://rtrt.me/app/rnra" target="_blank">Download our App</a></li>
				</ul>

			</nav>

			<!-- global footer -->
			<section id="globalfooter">
				<a href="<?php echo rnr_get_option( 'siteurl' ); ?>"><div id="footerlogo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo_footer@2x.png" alt="Rock 'N' Roll Marathon Series logo"></div></a>
				<p>&copy; <?php echo date('Y'); ?> &ndash; Competitor Group, Inc. All Rights Reserved. <a href="<?php echo network_site_url('/about-competitor-group/'); ?>">About</a> | <a href="http://competitorgroup.com/privacy-policy/" target="_blank">Privacy and CookiePolicy</a> | <a href="http://competitorgroup.com/privacy-policy#third-parties" target="_blank">AdChoices</a> | <a href="http://competitorgroup.com/" target="_blank">Media Kit</a> | <a href="https://careers-competitorgroup.icims.com/jobs/intro?hashed=0" target="_blank">Careers</a></p>
			</section>

			</section>
		</footer> <!-- /combined footers -->

	</div><!-- /container -->

	<?php wp_footer(); ?>

</body>
</html>
