<?php
	/**
	 * regional/state landing page headers
	 */

	/**
	 * global variables
	 */
	// $header_code		= rnr_get_option( 'rnrgv_header');
	$google_analytics	= rnr_get_option( 'rnrgv_google_analytics');
	$gtm				= rnr_get_option( 'rnrgv_gtm');
	$optimizely			= rnr_get_option( 'rnrgv_optimizely');

?><!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
	<head>
		<?php if( $optimizely != '' ) {
			echo stripslashes( $optimizely );
		} ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<?php rnr3_display_favicon();

		if( $google_analytics != '' ) { ?>
			<script type="text/javascript">
				var _gaq = _gaq || [];
				var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
				_gaq.push(
					['_require', 'inpage_linkid', pluginUrl],
					['_setAccount', '<?php echo $google_analytics;?>'],
					['_setDomainName', 'runrocknroll.com'],
					['_setAllowLinker', true],
					['_trackPageview']
				);
				(function() {
					var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
				})();
			</script>
		<?php }

		wp_head(); ?>

	</head>
	<?php
		$is_parent = get_post( get_the_ID() );
		if( $is_parent->post_parent != 0 ) {
			$regional_class = 'regional_child';
		} else {
			$regional_class = 'regional_parent';
		}
	?>
	<body id="rnr-regional" <?php body_class( $regional_class ); ?>>
		<?php if( $gtm != '' ) {
			echo stripslashes( $gtm );
		} ?>
		<div id="container">
			<!-- begin main header -->
			<header>
				<section class="wrapper">
					<?php
						$logo_url = get_site_url();

						if( $post->post_parent ) {
							echo '<div id="logo" class="regional"><a href="'. get_permalink( $post->post_parent ) .'"><img src="'. get_stylesheet_directory_uri() .'/img/logo@2x.png" alt="Rock \'N\' Roll Marathon Series" class="mid"></a></div>';
						} else {
							echo '<div id="logo" class="regional"><img src="'. get_stylesheet_directory_uri() .'/img/logo@2x.png" alt="Rock \'N\' Roll Marathon Series" class="mid"></div>';
						}


						/**
						 * hamburger menu
						 * first check if this is a child page
						 * if it is, then only show when there is more than one sibling
						 */
						if( $is_parent->post_parent != 0 ) {
							$parent_id = wp_get_post_parent_id( get_the_ID() );
							$args = array(
								'child_of'	=> $parent_id,
								'title_li'	=> '',
								'exclude'		=> get_the_ID(),
								'echo'			=> false
							);
							$siblings = wp_list_pages( $args );
							$bullet_count = substr_count( $siblings, '<li' );
							if( $bullet_count > 0 ) { ?>

								<nav class="regional_hamburger">
									<div class="menulabel"><?php echo get_post_meta( get_the_ID(), '_rnr3_reg_lp_menu_txt', 1 ); ?></div>
									<div class="hamburger">
										<span></span>
										<span></span>
										<span></span>
										<span class="hamburger__label">Menu</span>
									</div>
									<?php
										echo '<div id="global-rnr-menu">
											<ul>'
												. $siblings .
												'<li><a href="'. network_home_url( '/#findrace' ) .'">All Events</a></li>
											</ul>
										</div>';
									?>
								</nav>
							<?php }
						}
					?>
				</section>
			</header>


