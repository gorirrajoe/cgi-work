<?php
	/*
	 * Template Name: Expo Registration
	 * Main Template to show all expo events
	 */

	get_header('exporeg');

	//query to get all expo events
	$args = array(
		'post_status'     => array( 'publish', 'future' ),
		'order'           => 'ASC',
		'posts_per_page'  => -1,
	);

	$expo_events = new WP_Query( $args );

	if ( $expo_events->have_posts() ) : ?>
		<div id="findrace">
			<div class="controls">

				<label>Show</label>
				<button class="filter" data-filter="all">All</button>
				<button class="filter" data-filter=".5k">5k</button>
				<button class="filter" data-filter=".10k">10k</button>
				<button class="filter" data-filter=".half-marathon">Half</button>
				<button class="filter" data-filter=".marathon">Marathon</button>
				<button class="filter" data-filter=".relay">Relay</button>
				<button class="filter" data-filter=".virtual-run">Virtual Run</button>

				<div id="sort">
					<label>Sort By</label>
					<select id="sortselect">
						<option value="date:asc">Date ASC</option>
						<option value="date:des">Date DES</option>
						<option value="city:asc">Name</option>
					</select>
				</div>

			</div><!-- END .CONTROLS -->

			<div class="event-entries">
			<?php
				while ( $expo_events->have_posts() ) : $expo_events->the_post();
					//declare variables we will need
					$event_id         = get_the_ID();
					$race_types_list  = array();
					$race_classes     = '';
					$reg_link         = get_post_meta( $event_id, '_reg_details_reg_link', true ) ?: NULL;
					$reg_reminder     = get_post_meta( $event_id, '_reg_details_reg_reminder', true ) ?: NULL;
					$reg_sale         = get_post_meta( $event_id, '_additional_details_sale', true ) ?: NULL;
					$tbd              = get_post_meta( $event_id, '_additional_details_tbd', true ) ?: NULL;
					$festival         = get_post_meta( $event_id, '_additional_details_festival', true ) ?: NULL;

					if ( $tbd != '' ) {
						$date_displayed = $date = 'TBD';
					} elseif ( $festival != '' ) {
						$fest_start_date  = get_post_meta( $event_id, '_additional_details_start_date', true );
						$fest_end_date    = get_post_meta( $event_id, '_additional_details_end_date', true );

						$date           = get_the_date('Y-m-d');
						$date_displayed = date( 'F d', strtotime( $fest_start_date) ) . ' - ' . date( 'F d, Y', strtotime( $fest_end_date) );

					} else {
						$date           = get_the_date('Y-m-d');
						$date_displayed = get_the_date('F d, Y');
					}

					//get all needed data from each event
					$race_types = wp_get_post_terms( $event_id, 'race_types' );


					//manipulate the data we saved above into our needs
					foreach ( $race_types as $race ) {
						$race_classes[]     = $race->slug;
						$race_types_list[]  = $race->name;
					}
					$race_classes = implode( ' ', $race_classes );
			?>
				<div class="event mix<?php echo ' ' . $race_classes; ?>" data-city="<?php echo $post->post_name; ?>" data-date="<?php echo $date; ?>" data-post-id="<?php echo $event_id; ?>">
					<div class="event-box">
						<?php if ( $reg_sale ) : ?>
						<div class="ribbon"><div class="text">SALE</div></div>
						<?php endif; ?>
						<div class="race-types">
							<?php
								foreach( $race_types_list as $race_type ){
									$race_type = explode( ' ',trim($race_type) );
									echo '<div class="race-type-box">' . $race_type[0] . '</div>';
								}
							?>
						</div>

						<h2><?php echo the_title(); ?></h2>
						<div class="date"><?php echo $date_displayed; ?></div>

						<?php if ( $reg_link ) : ?>
						<a href="<?php echo $reg_link; ?>" class="register2">Register</a>
						<?php elseif ( !$reg_link && $reg_reminder) : ?>
						<a class="register2 fancybox" data-fancybox data-options='{"src": "<?php echo $reg_reminder; ?>", "preload": "true"}' href="javascript:;">Get Notified</a>
						<?php endif; ?>

						<a href="#" class="results get-details">Details</a>
					</div><!-- .EVENT-BOX -->
				</div><!-- END .EVENT.MIX -->
			<?php endwhile; ?>

			<!-- extra DIVs for proper grid spacing -->
			<div class="gap"></div>
			<div class="gap"></div>
			<div class="gap"></div>
			<div class="gap"></div>
			<div class="gap"></div>
			<div class="gap"></div>
			<div class="gap"></div>
			<div class="gap"></div>

			</div><!-- END .EVENT-ENTRIES -->
		</div><!-- END #FINDRACE -->
	<?php else: ?>
	<!-- no posts found -->
	<?php endif;

	wp_reset_postdata();

	$footer = wpautop( get_post_meta( get_the_ID(), '_footer_copy', true ) ?: '' );

	if ( $footer != '' ) : ?>
	<div id="footer">
		<?php echo do_shortcode( $footer ); ?>
	</div>
	<?php endif; ?>

		<script src="http://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
		<script>
			jQuery(function($){
				var $sortSelect = $('#sortselect'),
					$container = $('#findrace');

				$container.mixItUp({
					load: { sort: 'date:asc' },
					callbacks: {
						onMixEnd: function(state){
							var $last = state.$targets.last();

							state.$targets.removeClass('last-visible');
							$last.addClass('last-visible');
						}
					}
				});

				$sortSelect.on('change', function(){
					$('.current-gridItem').find('.get-details').trigger('click');
					$container.mixItUp('sort', this.value);
				});

			});

		</script>

		<a href="#" id="goTop">
			<span class="icon-up-open"></span>
		</a>
		<?php wp_footer(); ?>
	</body>
</html>
