<?php
	/* Template Name: Travel - v2 */

	get_header();

	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';

	$prefix = '_rnr3_';

	$travel_subpage_array = array(
		'fly'		=> 'flights',
		'stay'		=> 'hotels',
		'pack'		=> 'pack',
		'transport'	=> 'transport'
	);

	$post_meta = get_meta( get_the_ID() );

	$active_subpages = array();
	foreach( $travel_subpage_array as $key => $value ) {

		if( get_post_meta( get_the_ID(), $prefix . $key . '_img', 1 ) != '' ) {

			$active_subpages[$key] = $value;

		}

	}
?>

<!-- main content -->
<main role="main" id="main" class="travel">

	<?php
		if( !empty( $active_subpages ) ) {

			echo '<section class="grid_'. count( $active_subpages ) .'_special travel_subpages">
				<div class="wrapper">';

					foreach( $active_subpages as $key => $value ) {

						if( $qt_lang['enabled'] == 1 ) {
							$col_title_array	= qtrans_split( $post_meta[$prefix . $key . '_title'] );
							$col_title			= $col_title_array[$qt_lang['lang']];
						} else {
							$col_title = $post_meta[$prefix . $key . '_title'];
						}

						echo '<div class="column_special">

							<figure>
								<img src="'. $post_meta[$prefix . $key . '_img'] .'" alt="">
							</figure>

							<h3>'. $col_title .'</h3>';

							/**
							 * sponsor (optional)
							 */
							if( $key == 'pack' ) {
								switch_to_blog( 1 );

									$blog1_pack_page_id = get_id_by_slug( 'travel/pack' );

									$subpage_sponsor = ( get_post_meta( $blog1_pack_page_id, '_rnr3_sponsor_img', 1 ) != '' ) ? get_post_meta( $blog1_pack_page_id, '_rnr3_sponsor_img', 1 ) : '';

									if( $subpage_sponsor != '' ) {

										echo '<p class="type_sponsor">'. $presented_by_txt .' <img src="'. $subpage_sponsor .'" alt=""></p>';

									}

								restore_current_blog();
							}
							if( array_key_exists( $prefix . $key . '_sponsor', $post_meta ) ) {
								echo '<p class="type_sponsor">'. $presented_by_txt .' <img src="'. $post_meta[$prefix . $key . '_sponsor'] .'" alt=""></p>';
							}

							if( $qt_lang['enabled'] == 1 ) {
								$col_desc_array	= qtrans_split( $post_meta[$prefix . $key . '_desc'] );
								$col_desc		= $col_desc_array[$qt_lang['lang']];
							} else {
								$col_desc = $post_meta[$prefix . $key . '_desc'];
							}

							echo '<p>'. $col_desc .'</p>';

							if( $qt_lang['enabled'] == 1 ) {
								$col_cta_array	= qtrans_split( $post_meta[$prefix . $key . '_cta'] );
								$col_cta		= $col_cta_array[$qt_lang['lang']];
							} else {
								$col_cta = $post_meta[$prefix . $key . '_cta'];
							}

							echo '<p><a class="cta" href="'. $active_subpages[$key] .'">'. $col_cta .'</a></p>';

						echo '</div>';

					}

				echo '</div>
			</section>';

		}


		/**
		 * get travel events dropdown
		 * switch to blog 1, grab meta information, then restore current blog
		 */
		$choose_hdr = $post_meta['_rnr3_choose_rnr_hdr'];

		if( $qt_lang['enabled'] == 1 ) {
			$choosehdr_array	= qtrans_split( $post_meta['_rnr3_choose_rnr_hdr'] );
			$choosehdr			= $choosehdr_array[$qt_lang['lang']];
		} else {
			$choosehdr = $post_meta['_rnr3_choose_rnr_hdr'];
		}

		switch_to_blog( 1 );

			$page_id = get_id_by_slug( 'travel' );

			$events_dropdown = ( get_post_meta( $page_id, '_rnr3_choose_rnr_dropdown', 1 ) != '' ) ? get_post_meta( $page_id, '_rnr3_choose_rnr_dropdown', 1 ) : '';

			if( !empty( $events_dropdown ) ) { ?>

				<section class="wrapper travel_events">

					<h3><?php echo $choosehdr; ?></h3>

					<?php
						echo '<select id="selectrace">
							<option value="box">-- '. $display_text['choose_race_location'] .' --</option>';

							foreach( $events_dropdown as $event_solo ) {

								$event_pieces	= explode( '::', $event_solo );
								$event_url		= site_url( $event_pieces[0] . '/the-weekend/travel/' );

								echo '<option value="'. $event_url .'">'. $event_pieces[1] .'</option>';
							}
						echo '</select>';
					?>

					<script>
						$(function(){
							// bind change event to select
							$('#selectrace').bind('change', function () {
								var url = $(this).val(); // get selected value
								if (url) { // require a URL
									window.location = url; // redirect
								}
								return false;
							});
						});
					</script>

				</section>

			<?php }

		restore_current_blog();


		/**
		 * sponsors section
		 */
		travel_sponsors( get_the_ID() );

	?>

</main>

<?php get_footer(); ?>
