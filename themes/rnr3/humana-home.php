<?php
	/* Template Name: Humana - Series Home */
	get_header('special');

	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
		include 'languages.php';

	rnr3_get_secondary_nav( '', 'humana' );

?>
<!-- schedule / news -->
<section id="info">
	<section class="wrapper grid_2">
		<?php
			$about_img		= get_post_meta( $post->ID, '_rnr3_about_img', TRUE );
			$about_blurb	= apply_filters( 'the_content', get_post_meta( $post->ID, '_rnr3_about_blurb', TRUE ) );

			if( $qt_lang['enabled'] == 1 ) {
				$box_array		= qtrans_split( get_post_meta( $post->ID, '_rnr3_about_title', TRUE ) );
				$about_title	= $box_array[$qt_lang['lang']];
			} else {
				$about_title = get_post_meta( $post->ID, '_rnr3_about_title', TRUE );
			}

			echo '<h2>'.$about_title.'</h2>';
		?>
		<div class="column"><!-- column 1 -->
			<?php echo '<figure class="center"><img src="'.$about_img.'" alt=""></figure>'; ?>
		</div>

		<div class="column"><!-- column 2 -->
			<?php echo $about_blurb; ?>
		</div>
	</section>
</section>


<!-- three-spot panel/infoboxes -->
<section id="features">
	<section class="wrapper grid_3">
		<?php
			if( $qt_lang['enabled'] == 1 ) {
				$box_array		= qtrans_split( get_post_meta( $post->ID, '_rnr3_info_box_title', TRUE ) );
				$info_box_title	= $box_array[$qt_lang['lang']];
			} else {
				$info_box_title = get_post_meta( $post->ID, '_rnr3_info_box_title', TRUE );
			}
			echo '<h2>'.$info_box_title.'</h2>';

			for( $i = 1; $i < 7; $i++ ) {
				${'box' . $i . '_img'}		= get_post_meta( $post->ID, '_rnr3_box'.$i.'_img', TRUE );
				${'box' . $i . '_blurb'}	= apply_filters( 'the_content', get_post_meta( $post->ID, '_rnr3_box'.$i.'_blurb', TRUE ) );

				if( $qt_lang['enabled'] == 1 ) {
					${'box' . $i . '_url'} = qtrans_convertURL( get_post_meta($post->ID, '_rnr3_box'.$i.'_url', TRUE) );
				} else {
					${'box' . $i . '_url'} = get_post_meta( $post->ID, '_rnr3_box'.$i.'_url', TRUE );
				}

				${'box' . $i . '_tuneup_url'}	= get_post_meta( $post->ID, '_rnr3_box'.$i.'_tuneup_url', TRUE );
				${'box' . $i . '_date'}			= get_post_meta( $post->ID, '_rnr3_box'.$i.'_date', TRUE );

				if( strtotime( ${'box' . $i . '_date'} ) < strtotime('now') ) {
					$race_date = $txt_TBD;
				} elseif($qt_lang['lang'] != 'en'){
					$race_date = date( "d-m-Y", strtotime( ${'box' . $i . '_date'} ) );
				} else {
					$race_date = ${'box' . $i . '_date'};
				}

				if( $qt_lang['enabled'] == 1 ) {

					$box_array					= qtrans_split( get_post_meta( $post->ID, '_rnr3_box'.$i.'_title', TRUE ) );
					${'box' . $i . '_title'}	= $box_array[$qt_lang['lang']];

					$link_array					= qtrans_split( get_post_meta( $post->ID, '_rnr3_box'.$i.'_link_txt', TRUE ) );
					${'box' . $i . '_link_txt'}	= $link_array[$qt_lang['lang']];

					$tuneup_link_array					= qtrans_split( get_post_meta( $post->ID, '_rnr3_box'.$i.'_tuneup_link_txt', TRUE ) );
					${'box' . $i . '_tuneup_link_txt'}	= $tuneup_link_array[$qt_lang['lang']];

				} else {

					${'box' . $i . '_title'}			= get_post_meta( $post->ID, '_rnr3_box'.$i.'_title', TRUE );
					${'box' . $i . '_link_txt'}			= get_post_meta( $post->ID, '_rnr3_box'.$i.'_link_txt', TRUE );
					${'box' . $i . '_tuneup_link_txt'}	= get_post_meta( $post->ID, '_rnr3_box'.$i.'_tuneup_link_txt', TRUE );

				}

				if(!empty(${'box' . $i . '_img'}) and !empty(${'box' . $i . '_blurb'})){

					echo '<div class="column">';

						// if no url is added, don't link the image nor show the cta link
						if( ${'box' . $i . '_url'} != '' ) {
							echo '<figure><a href="'.${'box' . $i . '_url'}.'"><img src="'.${'box' . $i . '_img'}.'" alt=""></a></figure>

							<h3>'.${'box' . $i . '_title'}.'</h3>
							<p class="tuneupfor">'. $race_date.'</p>
							'. ${'box' . $i . '_blurb'};

							if( ${'box' . $i . '_tuneup_link_txt'} != '' ) {
								echo '<p class="tuneupfor">Tune up run for<br/>';
									if( ${'box' . $i . '_tuneup_url'} != '' ) {
										echo '<a href="'.${'box' . $i . '_tuneup_url'}.'">';
									}
									echo ${'box' . $i . '_tuneup_link_txt'};
									if( ${'box' . $i . '_tuneup_url'} != '' ) {
										echo '</a>';
									}
								echo '</p>';
							}
							echo '<p style="text-align: center;"><a class="cta" href="'.${'box' . $i . '_url'}.'">'.${'box' . $i . '_link_txt'}.'</a></p>';

						} else {

							echo '<figure><img src="'.${'box' . $i . '_img'}.'" alt=""></figure>

							<h3>'.${'box' . $i . '_title'}.'</h3>
							<p class="tuneupfor">'. $race_date.'</p>
							'. ${'box' . $i . '_blurb'};

							if( ${'box' . $i . '_tuneup_link_txt'} != '' ) {
								echo '<p class="tuneupfor">Tune up run for<br/>';
									if( ${'box' . $i . '_tuneup_url'} != '' ) {
										echo '<a href="'.${'box' . $i . '_tuneup_url'}.'">';
									}
									echo ${'box' . $i . '_tuneup_link_txt'};
									if( ${'box' . $i . '_tuneup_url'} != '' ) {
										echo '</a>';
									}
								echo '</p>';

							}
							// echo '<p class="cta">'.${'box' . $i . '_link_txt'}.'</p>';

						}
					echo '</div>';
				}
			}
		?>

	</section>
</section>


<!-- training -->
<section id="info">
	<section class="wrapper grid_2">
		<?php
			$training_img	= get_post_meta($post->ID, '_rnr3_training_img', TRUE);
			$training_blurb	= apply_filters('the_content', get_post_meta($post->ID, '_rnr3_training_blurb', TRUE));
			$training_url	= get_post_meta($post->ID, '_rnr3_training_url', TRUE);

		if($qt_lang['enabled'] == 1){
			$box_array			= qtrans_split(get_post_meta($post->ID, '_rnr3_training_title', TRUE));
			$training_title		= $box_array[$qt_lang['lang']];
			$link_array			= qtrans_split(get_post_meta($post->ID, '_rnr3_training_link_txt', TRUE));
			$training_link_txt	= $link_array[$qt_lang['lang']];
		} else {
			$training_title		= get_post_meta($post->ID, '_rnr3_training_title', TRUE);
			$training_link_txt	= get_post_meta($post->ID, '_rnr3_training_link_txt', TRUE);
		}

		echo '<h2>'.$training_title.'</h2>'; ?>
		<div class="training column"><!-- column 1 -->
			<?php
				if( $training_url != '' ) {
					echo $training_blurb;
					echo '<p style="text-align: center;"><a class="cta" href="'.$training_url.'" target="_blank">'.$training_link_txt.'</a></p>';
				} else {
					echo $training_blurb;
				}
			?>
		</div>

		<div class="news column"><!-- column 2 -->
			<?php // if no url is added, don't link the image nor show the cta link
				if( $training_url != '' ) {
					echo '<figure><a href="'.$training_url.'"><img src="'.$training_img.'" alt=""></a></figure>';
				} else {
					echo '<figure><img src="'.$training_img.'" alt=""></figure>';
				}
			?>
		</div>
	</section>
</section>


<!-- pixlee content -->
<?php
	$pixlee			= get_post_meta( get_the_ID(), '_rnr3_pixlee', TRUE );
	$pixlee_album	= get_post_meta( get_the_ID(), '_rnr3_pixlee_album_id', 1 );

	if( $pixlee != '' || $pixlee_album != '' ) { ?>
		<section id="socialfeed">
			<section class="wrapper">
				<h2><?php echo get_post_meta( get_the_ID(), '_rnr3_hashtag', TRUE ); ?></h2>
				<?php
					if( $pixlee != '' ) {
						echo $pixlee;
					} else {
						$pixlee_album = get_post_meta( get_the_ID(), '_rnr3_pixlee_album_id', 1 );
						get_pixlee_grid( $pixlee_album );
					}
				?>
			</section>
		</section>
	<?php }

get_global_overlay( $qt_lang );

get_footer('series'); ?>
