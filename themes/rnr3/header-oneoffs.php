<?php
	/**
	 * super slimmed down header
	 * usage: series - mainly the old templates or templates you don't want the hero image on
	 * exception: virtual run, which has a hero
	 */
	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';

	/**
	 * global variables
	 */
	$twitter				= rnr_get_option( 'rnrgv_twitter');
	$instagram				= rnr_get_option( 'rnrgv_instagram');
	$pinterest				= rnr_get_option( 'rnrgv_pinterest');
	$youtube				= rnr_get_option( 'rnrgv_youtube');
	$header_code			= rnr_get_option( 'rnrgv_header');
	$google_analytics		= rnr_get_option( 'rnrgv_google_analytics');
	$gtm					= rnr_get_option( 'rnrgv_gtm');
	$optimizely				= rnr_get_option( 'rnrgv_optimizely');

	$twenty_years_enabled		= ( rnr_get_option( 'rnrgv_20_enabled' ) == 'on' ) ? 'on' : 'off';

	$twenty_years_event_list	= ( rnr_get_option( 'rnrgv_20year_events' ) != '' ) ? rnr_get_option( 'rnrgv_20year_events' ) : '';
	$twenty_years_event			= strpos( $twenty_years_event_list, ',' ) ? explode( ',', $twenty_years_event_list ) : array( $twenty_years_event_list );

	$twenty_years_exception_list	= ( rnr_get_option( 'rnrgv_20year_exception' ) != '' ) ? rnr_get_option( 'rnrgv_20year_exception' ) : '';
	$twenty_years_exception_events	= strpos( $twenty_years_exception_list, ',' ) ? explode( ',', $twenty_years_exception_list ) : array( $twenty_years_exception_list );

?><!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
	<head>
		<?php if( $optimizely != '' ) {
			echo stripslashes( $optimizely );
		} ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<?php rnr3_display_favicon();

		if( $google_analytics != '' ) { ?>
			<script type="text/javascript">
				var _gaq = _gaq || [];
				var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
				_gaq.push(
					['_require', 'inpage_linkid', pluginUrl],
					['_setAccount', '<?php echo $google_analytics;?>'],
					['_setDomainName', 'runrocknroll.com'],
					['_setAllowLinker', true],
					['_trackPageview']
				);
				(function() {
					var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
				})();
			</script>
		<?php }

		wp_head();

		if( !empty( $event_info->header_settings ) ) {
			echo $event_info->header_settings;
		} ?>

	</head>


	<body id="rnr-series" <?php body_class( array( 'market-'.$market, 'minimal-nav' ) ); ?>>
		<?php if( $gtm != '' ) {
			echo stripslashes( $gtm );
		} ?>

		<div id="container">

			<!-- alert banner -->
			<?php if( !empty( $event_info->alert_msg ) ) {

				if( $qt_lang['enabled'] == 1 ) {
					$alert_msg = qtrans_split( $event_info->alert_msg );
					$alert_msg = $alert_msg[$qt_lang['lang']];
				} else {
					$alert_msg = $event_info->alert_msg;
				} ?>

				<section id="alert">
					<section class="wrapper">
						<p class="mid2"><?php echo $alert_msg; ?></p>
					</section>
				</section>

			<?php } ?>

			<!-- begin main header -->
			<header>
				<section class="wrapper">

					<?php
						if( get_current_blog_id() == 41 ) {

							wp_nav_menu( array(
								'menu'		=> 'sidebar-cat-nav',
								'menu_id'	=> 'menu-header-cat-nav'
							) );

						}
					?>

					<div id="getgoing" class="clearfix showdesktop">

						<?php if( $market != 'virtual-run' ) { ?>
							<ul id="reg">
								<li><a href="<?php echo network_site_url(); ?>/#findrace" class="register"><?php echo $txt_find_a_race; ?></a></li>
							</ul>
						<?php } ?>

					</div>

					<?php
						/**
						 * get appropriate logo, 20 years or normal?
						 */
						get_logo( $twenty_years_enabled, $market, $twenty_years_exception_events );
					?>

					<nav>

						<div class="hamburger">
							<span></span>
							<span></span>
							<span></span>
							<span class="hamburger__label">Menu</span>
						</div>

						<?php
							$global_nav = rnr3_get_global_nav( $qt_lang['lang'] );
							echo $global_nav;
						?>

						<div id="getgoing-mobile" class="clearfix showmobile">

							<?php if( $market != 'virtual-run' ) { ?>
								<ul id="reg">
									<li><a href="<?php echo network_site_url(); ?>/#findrace" class="register"><?php echo $txt_find_a_race; ?></a></li>
								</ul>
							<?php } ?>

						</div>

					</nav>

				</section>
			</header>

			<?php
			if( $market == 'virtual-run' ) {

				if( get_post_meta( get_the_ID(), '_rnr3_unique_header_img', 1 ) != '' ) {
					$header_image = get_post_meta( get_the_ID(), '_rnr3_unique_header_img', 1 );
				} else {
					$header_image = $event_info->header_image;
				}

				/**
				 * mobile header image
				 * will appear for screen resolutions less than or equal to 640px
				 * currently, no per page specifics
				 */
				$mobile_header_image = ( $event_info->mobile_header_image != '' ) ? $event_info->mobile_header_image : '';


				if( $mobile_header_image != '' || $header_image != '' ) { ?>

					<style>

						<?php if( $mobile_header_image != '' ) { ?>

							.narrow-hero {
								background: url("<?php echo $mobile_header_image; ?>") no-repeat scroll center bottom/cover rgba(0, 0, 0, 0);
							}

						<?php } else { ?>

							.narrow-hero {
								background: url("<?php echo $header_image; ?>") no-repeat scroll center bottom/cover rgba(0, 0, 0, 0);
							}

						<?php }

						if( $mobile_header_image != '' && $header_image != '' ) { ?>

							@media (min-width: 40em) {
								.narrow-hero {
									background: url("<?php echo $header_image; ?>") no-repeat scroll center bottom/cover rgba(0, 0, 0, 0);
								}
							}

						<?php } ?>

					</style>

				<?php } ?>


				<div class="narrow-hero">
					<section class="wrapper">
						<div class="copy">
							<?php
								if (have_posts()) : while (have_posts()) : the_post();
									the_content();
								endwhile; endif;
							?>
						</div>
					</section>
				</div>


				<!-- social stuff -->
				<section id="ribbon">
					<section class="wrapper mid">
						<div class="hash"><?php echo strtoupper($event_info->twitter_hashtag); ?> <span class="pop">/</span></div>
						<div class="twitter"><a href="<?php echo $twitter; ?>" target="_blank"><span class="icon-twitter"></span></a></div>
						<div class="facebook"><a href="<?php echo $event_info->facebook_page; ?>" target="_blank"><span class="icon-facebook"></span></a></div>
						<div class="instagram"><a href="<?php echo $instagram; ?>" target="_blank"><span class="icon-instagram"></span></a></div>
					</section>
				</section>
			<?php } ?>
