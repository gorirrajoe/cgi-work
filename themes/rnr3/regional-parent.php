<?php
	/* Template Name: Regional Landing - PARENT */

	get_header( 'regional-lp' );
	$prefix = '_rnr3_reg_';
	$qt_lang = rnr3_get_language();

	$twitter		= rnr_get_option( 'rnrgv_twitter');
	$instagram		= rnr_get_option( 'rnrgv_instagram');
	$pinterest		= rnr_get_option( 'rnrgv_pinterest');
	$youtube		= rnr_get_option( 'rnrgv_youtube');

	$facebook_page	= 'https://www.facebook.com/runrocknroll/';

?>
	<main role="main" id="main" class="regional_lp">
		<section class="wrapper">
			<h1><?php echo get_the_title(); ?></h1>

			<?php
				if ( false === ( $regional_query = get_transient( 'regional_query_'. get_the_ID() .'_results' ) ) ) {
					$sortby_choice = get_post_meta( get_the_ID(), $prefix . 'sort', 1 );
					if( $sortby_choice == 'racedate' ) {
						$args = array(
							'post_parent'			=> get_the_ID(),
							'posts_per_page'	=> -1,
							'post_type'				=> 'page',
							'meta_key'        => '_rnr3_reg_event_date',
							'orderby'         => 'meta_value_num title',
							'order'						=> 'ASC'
						);
					} else {
						$args = array(
							'post_parent'			=> get_the_ID(),
							'posts_per_page'	=> -1,
							'post_type'				=> 'page',
							'orderby'         => 'title',
							'order'						=> 'ASC'
						);
					}
					$regional_query = new WP_Query( $args );
					set_transient( 'regional_query_'. get_the_ID() .'_results', $regional_query, 1 * MINUTE_IN_SECONDS );
				}

				if( $regional_query->have_posts() ) {
					while( $regional_query->have_posts() ) {
						$regional_query->the_post();

						echo '<div class="regional_event">';
							if ( has_post_thumbnail() ) {
								$featured_img_id	= get_post_thumbnail_id( get_the_ID() );
								$featured_img			= wp_get_attachment_image_src( $featured_img_id, 'regional' );
								echo '<div class="regional_featuredimg"><a href="'. get_permalink() .'"><img src="'. $featured_img[0] .'"></a></div>';
							}

							$event_location_pieces = explode( '::', get_post_meta( get_the_ID(), $prefix . 'event_location', 1 ) );
							$market = $event_location_pieces[0];
							$event_info = $wpdb->get_row(
								"SELECT event_name, event_slug, event_date, festival, festival_start_date, festival_end_date, event_location, has_races FROM wp_rocknroll_events WHERE event_slug = '".$market."'"
							);
							echo '<div class="regional_eventdetails mid_prevent_mobile">
								<div class="mid">';
									/**
									 * display title of page, which should be event location
									 */
									echo '<h2 class="regional_eventlocation"><a href="'. get_permalink() .'">'. get_the_title() .'</a></h2>';

									/**
									 * display race types
									 */
									if( $event_info->has_races != '' ) {
										$races_array = explode( ',', $event_info->has_races );
										$has_races_array = array();
										foreach( $races_array as $race ) {
											$has_races_array[] = get_race_display_name_regional( $race );
										}
										$has_races = implode( ' / ', $has_races_array );
										echo '<div class="regional_racelist">' . $has_races . '</div>';
									}

									/**
									 * display race date
									 */
									$event_date = get_event_date_regional( $qt_lang[ 'lang' ], $market, $event_info );
									if( $event_date['mmmdd'] == 'TBD' ) {
										echo '<h3 class="regional_eventdate">TBD</h3>';
									} else {
										echo '<h3 class="regional_eventdate">' . $event_date['mmmdd'] . ', ' . $event_date['year'] . '</h3>';
									}

									/**
									 * cta
									 */
									$cta_txt = get_post_meta( get_the_ID(), $prefix . 'lp_cta_txt', 1 );
									echo '<a href="'. get_permalink() .'" class="cta regional">'. $cta_txt .'</a>';

								echo '</div>
							</div>
						</div>';
					}
					wp_reset_postdata();
				}

				if (have_posts()) : while (have_posts()) : the_post();
					the_content();
				endwhile; endif;

				if( get_post_meta( get_the_ID(), $prefix . 'social', 1 ) == 'on' ) {
					echo '
						<section id="ribbon" class="regional">
							<section class="wrapper">
								<h4>@RunRockNRoll</h4>
								<ul class="social regional">
									<li class="facebook"><a href="'. $facebook_page .'" target="_blank"><span class="icon-facebook"></span></a></li>
									<li class="twitter"><a href="'. $twitter .'" target="_blank"><span class="icon-twitter"></span></a></li>
									<li class="instagram"><a href="'. $instagram .'" target="_blank"><span class="icon-instagram"></span></a></li>
									<li class="pinterest"><a href="'. $pinterest .'" target="_blank"><span class="icon-pinterest"></span></a></li>
								</ul>
							</section>
						</section>
					';
				}
			?>

		</section>

	</main>
<?php
	get_footer( 'regional-lp' );
?>

