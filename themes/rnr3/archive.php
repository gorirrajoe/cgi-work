<?php
	$market = get_market2();

	if(get_current_blog_id() <= 1){
		get_header('series-home');
	} elseif( $market == 'rock-blog' ) {
		get_header('special');
	} else {
		get_header();
	}

	$parent_slug = the_parent_slug();
	rnr3_get_secondary_nav( $parent_slug );

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';

?>

	<!-- main content -->
	<main role="main" id="main">
		<section class="wrapper grid_2 offset240left">

			<?php get_sidebar(); ?>

			<div class="column">
				<div class="content">
					<?php if( have_posts() ) :

						$post = $posts[0]; // Hack. Set $post so that the_date() works.

						/* If this is a category archive */
						if( is_category() ) {
							if( $qt_lang['enabled'] == 1 ) {
								$categoryname_split = qtrans_split( single_cat_title( '', false ) ); ?>
								<h2 class="pagetitle">Archive for the '<?php echo $categoryname_split[$qt_lang['lang']]; ?>' Category</h2>
							<?php } else { ?>
								<h2 class="pagetitle">Archive for the '<?php single_cat_title(); ?>' Category</h2>
							<?php /* If this is a tag archive */ }
						} elseif( is_tag() ) { ?>
							<h2 class="pagetitle">Posts Tagged '<?php single_tag_title(); ?>'</h2>
						<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
							<h2 class="pagetitle">Archive for <?php the_time('F jS, Y'); ?></h2>
						<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
							<h2 class="pagetitle">Archive for <?php the_time('F, Y'); ?></h2>
						<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
							<h2 class="pagetitle">Archive for <?php the_time('Y'); ?></h2>
						<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
							<h2 class="pagetitle">Blog Archives</h2>
						<?php }


						/**
						 * author archive
						 */
						if( is_author() ) {
							$displayname	= get_the_author_meta( 'display_name' );
							$gravatar		= get_avatar( get_the_author_meta( 'ID' ), 120, '', $author_name, array( 'class' => 'alignleft' ) );
							$bio			= apply_filters( 'the_content', get_the_author_meta( 'description' ) );
							$twitter		= get_the_author_meta( '_rnr3_twitter' );
							$facebook		= get_the_author_meta( '_rnr3_facebook' );
							$instagram		= get_the_author_meta( '_rnr3_instagram' );
							$pinterest		= get_the_author_meta( '_rnr3_pinterest' );
							$googleplus		= get_the_author_meta( '_rnr3_google_plus' );

							echo '<section id="author-profile">
								<section class="wrapper">';
									echo '<h2>'. $displayname .'</h2>';

									if( $gravatar != '' ) {
										echo $gravatar;
									}

									echo '<div class="author_bio">'. $bio .'</div>

									<ul class="author_social">';
										if( $twitter ) {
											echo '<li>
												<a href="'. $twitter .'" target="_blank"><span class="icon-twitter"></span></a>
											</li>';
										}
										if( $facebook ) {
											echo '<li>
												<a href="'. $facebook .'" target="_blank"><span class="icon-facebook"></span></a>
											</li>';
										}
										if( $instagram ) {
											echo '<li>
												<a href="'. $instagram .'" target="_blank"><span class="icon-instagram"></span></a>
											</li>';
										}
										if( $pinterest ) {
											echo '<li>
												<a href="'. $pinterest .'" target="_blank"><span class="icon-pinterest"></span></a>
											</li>';
										}
									echo '</ul>

								</section>
							</section>';
						}

						while (have_posts()) : the_post(); ?>

							<article <?php post_class() ?>>
								<?php
									$image_array		= wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID()), 'news-promos-thumbs' );

									if( $image_array[0] ) { ?>
										<a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><img class="wp-post-image alignleft" src="<?php echo $image_array[0];?>"></a>
									<?php }
								?>

								<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>

								<p><?php the_excerpt(); ?></p>
								<p><a href="<?php the_permalink(); ?>" class="read-more-link"><?php echo $readmore_txt; ?></a></p>

							</article>

						<?php endwhile; ?>

							<nav class="archive_nav">
								<?php if( get_next_posts_link() ) { ?>
									<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
								<?php }

								if( get_previous_posts_link() ) { ?>
									<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
								<?php } ?>
							</nav>

						<?php else :

							if ( is_category() ) { // If this is a category archive
								printf("<h2 class='center'>Sorry, but there aren't any posts in the %s category yet.</h2>", single_cat_title('',false));
							} else if ( is_date() ) { // If this is a date archive
								echo("<h2>Sorry, but there aren't any posts with this date.</h2>");
							} else if ( is_author() ) { // If this is a category archive
								$userdata = get_userdatabylogin(get_query_var('author_name'));
								printf("<h2 class='center'>Sorry, but there aren't any posts by %s yet.</h2>", $userdata->display_name);
							} else {
								echo("<h2 class='center'>No posts found.</h2>");
							}

					endif; ?>
				</div>
			</div>
		</section>
	</main>

<?php if( get_current_blog_id() <= 1 ) {
	get_footer('series');
} else {
	get_footer();
} ?>
