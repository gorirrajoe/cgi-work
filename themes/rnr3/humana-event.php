<?php
	/* Template Name: Humana - Event Page */
	get_header('special');

	rnr3_get_secondary_nav( '', 'humana' );

	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';
	$prefix = '_rnr3_';


	// contents
	$registration['content']		= 'asdfghjkl';
	$custom_content['content']		= apply_filters( 'the_content', get_post_meta( $post->ID, '_rnr3_custom_txt', 1 ) );
	$tuneup['content']				= apply_filters( 'the_content', get_post_meta( $post->ID, '_rnr3_tune_up', true ) );
	$training['content']			= apply_filters( 'the_content', get_post_meta( $post->ID, '_rnr3_training', true ) );
	$coursemap['content']			= apply_filters( 'the_content', get_post_meta( $post->ID, '_rnr3_course_map', true ) );
	$parking['content']				= apply_filters( 'the_content', get_post_meta( $post->ID, '_rnr3_parking', true ) );
	$postrace['content']			= apply_filters( 'the_content', get_post_meta( $post->ID, '_rnr3_post_race', true ) );
	$contact['content']				= apply_filters( 'the_content', get_post_meta( $post->ID, '_rnr3_contact', true ) );
	$packet['content']				= apply_filters( 'the_content', get_post_meta( $post->ID, '_rnr3_packet_pickup', true ) );
	$startwithhealthy['content']	= apply_filters( 'the_content', get_post_meta( $post->ID, '_rnr3_startwithhealthy', true ) );
	$is_sold_out					= get_post_meta( get_the_ID(), '_rnr3_sold_out', true );
	$is_no_show_reg_details			= get_post_meta( get_the_ID(), '_rnr3_no_show_reg_details', true );


	if( $is_no_show_reg_details != 'on' ) {
		$humana_event_array = array(
			$registration,
			$custom_content,
			$tuneup,
			$coursemap,
			$packet,
			$parking,
			$postrace,
			$training,
			$startwithhealthy,
			$contact,
		);
	} else {
		$humana_event_array = array(
			$custom_content,
			$tuneup,
			$coursemap,
			$packet,
			$parking,
			$postrace,
			$training,
			$startwithhealthy,
			$contact,
		);
	}

?>

	<!-- main content -->
	<main role="main" id="main">
		<div id="nav-anchor"></div>
		<section class="wrapper grid_2 offset240left">
			<div class="column sidenav stickem">
				<h2><?php echo get_the_title();?></h2>
				<nav class="sticky-nav">
					<ul>
						<?php
							foreach( $humana_event_array as $key => $value ) {
								if( $value['content'] != '') {
									echo '<li class="sticky-nav-2"><a href="#before'.$key.'">'.$value['title'].'</a></li>';
								}
							}
						?>
					</ul>
				</nav>
			</div>

			<div class="column">
				<div class="content">
					<h2><?php echo $txt_title;?></h2>
					<?php if (have_posts()) : while (have_posts()) : the_post();
						the_content();
					endwhile; endif;

					//Regisration Details
					if( $is_no_show_reg_details != 'on' ) {

						echo '<section id="before0">
							<h3>'.$registration['title'].'</h3>';

							$start_line = apply_filters( 'the_content', get_post_meta( get_the_ID(), '_rnr3_start_line', true ) );
							$race_date = get_post_meta( get_the_ID(), '_rnr3_race_date', true );

							if( strtotime( $race_date ) < strtotime('now') ) {
								$race_date = $txt_TBD;
							} elseif( $qt_lang['lang'] != 'en' ) {
								$race_date = date( "d-m-Y", strtotime( $race_date ) );
							}

							$race_time = get_post_meta( get_the_ID(), '_rnr3_race_time', true );

							if( $race_time == '' ) {
								$race_time = $txt_TBD;
							} else {

								if( $qt_lang['enabled'] == 1 ) {
									if( $qt_lang['lang'] == 'fr' ) {
										$race_timehr	= date('G', strtotime( $race_time ));
										$race_timemin	= date('i', strtotime( $race_time ));
										$race_time		= $race_timehr.' h '.$race_timemin;
									}
								}

							}

							$register_url = get_post_meta( get_the_ID(), '_rnr3_register_url', true );

							if( $qt_lang['enabled'] == 1 ) {

								$start_line_array		= qtrans_split($start_line);
								$finish_line_array		= qtrans_split($finish_line);
								$time_limit_array		= qtrans_split($time_limit);
								$register_content_array	= qtrans_split($register_content);

								echo '<div class="glance">
									<div class="column">
										<p><strong>'.$txt_date.':</strong> '. $race_date .'</p>
										<p><strong>'.$txt_start_time.':</strong> '. $race_time .'</p>
									</div>
									<div class="column">
										<p><strong>'.$txt_start_line.':</strong></p>
										'. $start_line_array[$qt_lang['lang']] .'
									</div>';

									if( $is_sold_out == 'on' ) {
										echo '<div class="column reg">
											<span class="highlight black">'.$txt_sold_out.'</span>
										</div>';
									} else {
										echo '<div class="column reg">
											<a class="cta" href="'.$register_url.'" target="_blank">'.$txt_register.' '.get_post_meta( get_the_ID(), '_rnr3_price', true ).'</a>
										</div>';
									}
								echo '</div>';

							} else {
								echo '<div class="glance">
									<div class="column">
										<p><strong>Date:</strong> '.$race_date .'</p>
										<p><strong>Start Time:</strong> '. $race_time .'*</p>
										<p>* Times subject to change</p>
									</div>
									<div class="column">
										<p><strong>Start/Finish Line:</strong></p>
										'. $start_line .'
									</div>';

									if( $is_sold_out == 'on' ) {
										echo '<div class="column reg">
											<span class="highlight black">'.$txt_sold_out.'</span>
										</div>';
									} else {
										echo '<div class="column reg">
											<a class="cta" href="'.$register_url.'" target="_blank">'.$txt_register.' '.get_post_meta( get_the_ID(), '_rnr3_price', true ).'</a>
										</div>';
									}
								echo '</div>';
							}

						echo '</section>';
					}

					foreach( $humana_event_array as $key => $value ) {
						if( $value['content'] != '' AND $value['content'] != 'asdfghjkl') {
							echo '<section id="before'.$key.'">
								<h3>'.$value['title'].'</h3>
								'. $value['content'] .
							'</section>';
						}
					} ?>
				</div>
			</div>
		</section>


		<script>
			$(document).ready(function(){

				/**
				 * This part causes smooth scrolling using scrollto.js
				 * We target all a tags inside the nav, and apply the scrollto.js to it.
				 */
				$(".sticky-nav .sticky-nav-2 a, .backtotop").click(function(evn){
					evn.preventDefault();
					$('html,body').scrollTo(this.hash, this.hash);
				});

				/*$('main').stickem({
					container: '.offset240left',
				});*/

				/**
				 * This part handles the highlighting functionality.
				 * We use the scroll functionality again, some array creation and
				 * manipulation, class adding and class removing, and conditional testing
				 */
				var aChildren = $(".sticky-nav li").children(); // find the a children of the list items
				var aArray = []; // create the empty aArray
				for (var i=0; i < aChildren.length; i++) {
					var aChild = aChildren[i];
					var ahref = $(aChild).attr('href');
					aArray.push(ahref);
				} // this for loop fills the aArray with attribute href values

				$(window).scroll(function(){
					var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
					var windowHeight = $(window).height(); // get the height of the window
					var docHeight = $(document).height();

					for (var i=0; i < aArray.length; i++) {
						var theID = aArray[i];
						var divPos = $(theID).offset().top; // get the offset of the div from the top of page
						var divHeight = $(theID).height(); // get the height of the div in question
						if (windowPos >= (divPos - 1) && windowPos < (divPos + divHeight -1 )) {
							$("a[href='" + theID + "']").addClass("nav-active");
						} else {
							$("a[href='" + theID + "']").removeClass("nav-active");
						}
					}

					if(windowPos + windowHeight == docHeight) {
						if (!$(".sticky-nav li:last-child a").hasClass("nav-active")) {
							var navActiveCurrent = $(".nav-active").attr("href");
							$("a[href='" + navActiveCurrent + "']").removeClass("nav-active");
							$(".sticky-nav li:last-child a").addClass("nav-active");
						}
					}
				});
			});

		</script>
	</main>

<?php get_footer('series'); ?>
