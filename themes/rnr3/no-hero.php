<?php 
  /* Template Name: No Hero (Series) */

  get_header('oneoffs');
?>

  <!-- main content -->
  <main role="main" id="main" class="oneoff">
    <section class="wrapper">
      <div class="content">
        <?php
          if (have_posts()) : while (have_posts()) : the_post();
            echo '<h1>'. get_the_title(). '</h1>';
            the_content();
          endwhile; endif;
        ?>
      </div>
    </section>

  </main>


<?php get_footer('oneoffs'); ?>