<?php
	/* Template Name: TourPass ( Series ) */
	get_header( 'oneoffs' );

	$new_year_2016_seconds	= strtotime( '2016-01-01' );
	$nextyear				= mktime(0, 0, 0, 1,   1,   date("Y")+1 );
	$currentyear			= mktime(0, 0, 0, 1,   1,   date("Y") );

	// Grab all fields
	$tp_intro_copy		= apply_filters( 'the_content', get_post_meta ( get_the_ID(), '_rnr3_tp_intro', 1 ) );
	$tp_year			= get_post_meta ( get_the_ID(), '_rnr3_tp_year', 1 );
	$tp_next_year		= get_post_meta ( get_the_ID(), '_rnr3_tp_next_year', 1 );
	$tp_3pk_copy		= apply_filters( 'the_content', get_post_meta ( get_the_ID(), '_rnr3_tp_3pk', 1 ) );
	$tp_global_copy		= apply_filters( 'the_content', get_post_meta ( get_the_ID(), '_rnr3_tp_global', 1 ) );
	$tp_ny_3pk_copy		= apply_filters( 'the_content', get_post_meta ( get_the_ID(), '_rnr3_tp_ny_3pk', 1 ) );
	$tp_ny_global_copy	= apply_filters( 'the_content', get_post_meta ( get_the_ID(), '_rnr3_tp_ny_global', 1 ) );
	$tp_intro_img		= get_post_meta ( get_the_ID(), '_rnr3_tp_intro_img', 1 );
	$tp_3pk_img			= get_post_meta ( get_the_ID(), '_rnr3_tp_3pk_img', 1 );
	$tp_global_img		= get_post_meta ( get_the_ID(), '_rnr3_tp_global_img', 1 );
	$tp_ny_3pk_img		= get_post_meta ( get_the_ID(), '_rnr3_tp_ny_3pk_img', 1 );
	$tp_ny_global_img	= get_post_meta ( get_the_ID(), '_rnr3_tp_ny_global_img', 1 );
	$tp_disclaimer		= apply_filters( 'the_content', get_post_meta ( get_the_ID(), '_rnr3_tp_disclaimer', 1 ) );
	$tp_youtube_id		= get_post_meta ( get_the_ID(), '_rnr3_tp_step1_yt', 1 );
	$tp_step1_copy		= apply_filters( 'the_content', get_post_meta ( get_the_ID(), '_rnr3_tp_step1', 1 ) );
	$tp_step2_copy		= apply_filters( 'the_content', get_post_meta ( get_the_ID(), '_rnr3_tp_step2', 1 ) );
	$tp_step3_copy		= apply_filters( 'the_content', get_post_meta ( get_the_ID(), '_rnr3_tp_step3', 1 ) );
	$tp_faq_copy		= apply_filters( 'the_content', get_post_meta ( get_the_ID(), '_rnr3_tp_faq', 1 ) );
	$tp_terms_copy		= apply_filters( 'the_content', get_post_meta ( get_the_ID(), '_rnr3_tp_terms', 1 ) );
	$tp_medals_copy		= apply_filters( 'the_content', get_post_meta ( get_the_ID(), '_rnr3_tp_medals', 1 ) );
	$tp_tweets			= get_post_meta ( get_the_ID(), '_rnr3_tp_tweets', 1 );
	$tp_pixlee			= get_post_meta ( get_the_ID(), '_rnr3_tp_pixlee', 1 );
	$tp_contact			= get_post_meta ( get_the_ID(), '_rnr3_tp_contact', 1 );

?>
	<main role="main" id="main" class="no-hero">

		<nav id="subnav">
			<section class="wrapper">
				<div class="subnav-menu-label">Menu</div>
				<div class="subnav-menu-primary-event-container">

					<ul class="subnav-menu">
						<?php
							if( has_nav_menu( 'TourPass Menu' ) ) {
								wp_nav_menu(
									array(
										'menu'			=> 'TourPass Menu',
										'container'		=> false,
										'items_wrap'	=> '%3$s'
									)
								);
							} else { ?>
								<li><a href="#start">Start</a></li>
								<li><a href="#purchase">Purchase</a></li>
								<li><a href="#info2">Info</a></li>
								<li><a href="#pass-medals">Medals</a></li>
								<li><a href="#social">Social</a></li>
								<?php if( $tp_contact ): ?>
									<li><a href="#contact">Contact</a></li>
								<?php endif; ?>
							<?php }
						?>
					</ul>

				</div>
			</section>
		</nav>


		<section class="wrapper group" id="start">
			<div class="grid_2 center">

				<div class="column">
					<figure>
						<img src="<?php if(!empty($tp_intro_img)){ echo $tp_intro_img; } else { bloginfo( 'stylesheet_directory' );echo "/img/tourpass/tourpass-logo.png"; }?>">
					</figure>
				</div>

				<div class="column">
					<h1 class="tp_title">Start Your Rock 'n' Roll Tour!</h1>
					<?php echo $tp_intro_copy; ?>
					<p><a class="cta" href="#purchase">Get Your Tourpass</a></p>
				</div>

			</div>
		</section>


		<section class="wrapper group center" id="purchase">

			<h2>Choose Your TourPass</h1>

			<div id="pkg-tabs" class="tp-tabs">

				<?php if( $tp_year AND empty($tp_next_year)): ?>
					<ul class="ui-tabs-nav">
						<li class="ui-state-active"><span><?php echo $tp_year; ?></span></li>
					</ul>
				<?php else: ?>
					<ul class="ui-tabs-nav">
						<li><a href="<?php echo "#".$tp_year; ?>"><?php echo $tp_year; ?></a></li>
						<li><a href="<?php echo "#".$tp_next_year; ?>"><?php echo $tp_next_year; ?></a></li>
					</ul>
				<?php endif; ?>

				<div class="grid_2" id="<?php echo $tp_year; ?>">

					<div class="column">
						<figure>
							<img src="<?php if(!empty($tp_3pk_img)){ echo $tp_3pk_img; } else { bloginfo( 'stylesheet_directory' );echo "/img/tourpass/package-3-pack.png"; }?>">
						</figure>
						<!-- <h3>TourPass 3-Pack</h3> -->
						<?php echo $tp_3pk_copy; ?>
					</div>

					<div class="column">
						<figure>
							<img src="<?php if(!empty($tp_global_img)){ echo $tp_global_img; } else { bloginfo( 'stylesheet_directory' );echo "/img/tourpass/tourpass-global.png"; }?>">
						</figure>
						<!-- <h3>TourPass Global</h3> -->
						<?php echo $tp_global_copy; ?>
					</div>

				</div>

				<?php if( !empty($tp_next_year)): ?>
				<div class="grid_2" id="<?php echo $tp_next_year; ?>">

					<div class="column">
						<figure>
							<img src="<?php if(!empty($tp_ny_3pk_img)){ echo $tp_ny_3pk_img; } else { bloginfo( 'stylesheet_directory' );echo "/img/tourpass/package-3-pack.png"; }?>">
						</figure>
						<!-- <h3>TourPass 3-Pack</h3> -->
						<?php echo $tp_ny_3pk_copy; ?>
					</div>

					<div class="column">
						<figure>
							<img src="<?php if(!empty($tp_ny_global_img)){ echo $tp_ny_global_img; } else { bloginfo( 'stylesheet_directory' );echo "/img/tourpass/tourpass-global.png"; }?>">
						</figure>
						<!-- <h3>TourPass Global</h3> -->
						<?php echo $tp_ny_global_copy; ?>
					</div>

				</div>
				<?php endif; ?>

				<div class="group narrow">
					<?php echo $tp_disclaimer; ?>
				</div>
			</div>

		</section>


		<section class="wrapper group" id="info2">

			<h2 class="center">Information</h2>

			<div id="info-tabs" class="tp-tabs">
				<ul>
					<li><a href="#info-how">How</a></li>
					<li><a href="#info-where">Where</a></li>
					<li><a href="#info-faqs">FAQs</a></li>
					<li><a href="#info-terms">Terms</a></li>
				</ul>

				<div id="info-how">
					<?php if( $tp_youtube_id ) { ?>
						<div class="tourpass_video">
						<h3>How Tourpass Works</h2>
							<div class="video-container">
								<iframe width="1280" height="720" src="//www.youtube.com/embed/<?php echo $tp_youtube_id; ?>" frameborder="0" allowfullscreen></iframe>
							</div>
						</div>
					<?php } ?>

					<div class="grid_3 center">
						<div class="column">
							<figure><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/blank.gif" class="steps circle1"></figure>
							<h3>Choose your TourPass</h2>
							<?php echo $tp_step1_copy; ?>
							<p><a href="#purchase">See TourPass options</a></p>
						</div><div class="column">
							<figure><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/blank.gif" class="steps circle2"></figure>
							<h3>Register To Rock</h2>
							<?php echo $tp_step2_copy; ?>
						</div><div class="column">
							<figure><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/blank.gif" class="steps circle3"></figure>
							<h3>Reap The Rewards</h2>
							<?php echo $tp_step3_copy; ?>
						</div>
					</div>
					<p class="note center">Still have questions? <a class="info-tab-open" href="#info-faqs">Check out the TourPass FAQ</a></p>
				</div>


				<div id="info-where">
					<div class="module_fourth">

						<h3>Where Will You Rock?</h3>

						<?php
							$myrows = $wpdb->get_results(
								"SELECT event_location, event_url, event_date, international_event, event_slug, event_logo, event_name, event_sold_out, RemixChallenge FROM wp_rocknroll_events WHERE event_slug != 'series' AND event_slug NOT LIKE 'virtual%' AND ( event_location LIKE '%Canada%' OR international_event = 0 ) AND publish = 1 ORDER BY CASE WHEN event_date < NOW() THEN 1 ELSE 0 END, event_date"
							);

							$event_amount	= count( $myrows );
							$odd_event		= $event_amount % 2;

							if ( $odd_event == 1 ) {
								$half_event_amount = ( $event_amount + 1 ) / 2;
							} else {
								$half_event_amount = $event_amount / 2;
							}

							echo '<div class="tour_stops_list">
								<h3>U.S. Tour Stops</h3>';

								/* how many events are there in north america? */
								$numbNAEvents	= count( $myrows );
								$remainder		= $numbNAEvents % 2;

								if( $remainder == 1 ) {
									// do offset
									$array1Size = floor( $numbNAEvents / 2 ) + $remainder;
									$array2Size = floor( $numbNAEvents / 2 );
								} else {
									// no offset
									$array1Size = $numbNAEvents / 2;
									$array2Size = $array1Size;
								}

								$i = 0;

								foreach( $myrows as $row ) {

									if( $i == 0 ) {
										//start first table
										echo '<table class="tour_stops_table" cellspacing="0" cellpadding="0" border="0">';
									}

									if( $i == $array1Size ) {
										//end one table and start the next
										echo '</table>';
										echo '<table class="tour_stops_table" cellspacing="0" cellpadding="0" border="0">';
									}

									$event_date_seconds = strtotime( $row->event_date );

									/* determine the subpage ( 2015 or 2016 ), then show appropriate dates,
									"tba" for dates out of range */
									$date = "";

									if( $event_date_seconds < $currentyear OR $event_date_seconds > $nextyear ) {
										$date = "TBA";
									} else {
										$date = date( 'M d, Y', strtotime( $row->event_date ) );
									}

									if( $row->event_date == '0000-00-00' || $row->event_date == '1970-01-01' ) {
										$date = "TBA";
									}

									if( $row->RemixChallenge == 1 ) {
										$remix_star = '<span class="red">*</span> ';
									} else {
										$remix_star = '';
									}

									echo '<tr class="'.$row->event_slug.'">

										<td>' . $remix_star;

											if( $row->event_sold_out == 1 ) {
												echo '<del class="so_tp_d_d"><span class="so_tp_s_d">'.$date .'</span></del>';
											} else {
												echo $date;
											}

										echo '</td>

										<td><a href="'.$row->event_url.'">';

											if( $row->event_sold_out == 1 ) {
												echo '<del class="so_tp_d"><span class="so_tp_s">'.$row->event_location .'</span></del>';
											} else {
												echo $row->event_location;
											}

										echo '</a></td>

									</tr>';

									//increase count by 1
									$i++;

									if( $i == $numbNAEvents ) {
										//close final table, as all results have been looped through
										echo '</table>';
									}
								}

							echo '</div>';


							/* international evenets - non-canada */
							/* get list of events into one array */
							$tourstops0 = $wpdb->get_results(
								"SELECT event_location, event_slug, event_url, event_date, international_event, event_name, event_sold_out, RemixChallenge FROM wp_rocknroll_events WHERE event_slug != 'series' AND event_location NOT LIKE '%Canada%' AND international_event = 1 and publish = 1 ORDER BY CASE WHEN event_date < NOW() THEN 1 ELSE 0 END, event_date"
							);

							echo '<div class="tour_stops_list europe_stops">

								<h3>International Tour Stops</h3>';

								/* how many events are there in europe? */
								$numbInterEvents	= sizeof( $tourstops0 );
								$remainder			= $numbInterEvents % 2;

								if( $remainder == 1 ) {
									//do offset
									$array1Size = ( $numbInterEvents / 2 ) + $remainder;
									$array2Size = $numbInterEvents / 2;
								} else {
									//no offset
									$array1Size = $numbInterEvents / 2;
									$array2Size = $array1Size;
								}

								$i = 0;

								foreach( $tourstops0 as $tourstop0 ) {

									if( $i == 0 ) {
										//start first table
										echo '<table class="tour_stops_table" cellspacing="0" cellpadding="0" border="0">';
									}
									if( $i == $array1Size ) {
										//end one table and start the next
										echo '</table>';
										echo '<table class="tour_stops_table" cellspacing="0" cellpadding="0" border="0">';
									}

									$event_date_seconds = strtotime( $tourstop0->event_date );

									/* determine the subpage ( 2015 or 2016 ), then show appropriate dates,
									"tba" for dates out of range */
									$date = "";

									if( $event_date_seconds < $currentyear OR $event_date_seconds > $nextyear ) {
										$date = "TBA";
									} else {
										$date = date( 'M d, Y', strtotime( $tourstop0->event_date ) );
									}

									if( $tourstop0->event_date == '0000-00-00' || $tourstop0->event_date == '1970-01-01' ) {
										$date = "TBA";
									}

									if( $tourstop0->event_slug == 'nice' ) {
										$nice_exception = ' *';
									} else {
										$nice_exception = '';
									}

									if( $tourstop0->RemixChallenge == 1 ) {
										$remix_star = '<span class="red">*</span>';
									} else {
										$remix_star = '';
									}

									$event_location = explode( ',', $tourstop0->event_location );

									if( strpos( $event_location[0], '[:en]' ) !== false ) {
										$event_location_split = preg_split( '(\\[.*?\\])', $event_location[0] );
										$event_location_display = $event_location_split[1];
									} else {
										$event_location_display = $event_location[0];
									}

									echo '<tr class="'.$tourstop0->event_slug.'">
										<td>'.$remix_star .$date.'</td>
										<td><a href="'.$tourstop0->event_url.'">'.$event_location_display . $nice_exception .'</a></td>
									</tr>';

									//increase count by 1
									$i++;

									if( $i == $numbInterEvents ) {
										//close final table, as all results have been looped through
										echo '</table>';
									}
								}
							echo '</div>';
						?>

						<p class="center"><span class="red">*</span> <strong>Remix Challenge Events</strong></p>

					</div>
				</div>

				<div class="module_sixth" id="info-faqs">
					<div class="narrow">
					 <h3>Frequently Asked Questions</h3>
					 <?php echo $tp_faq_copy; ?>
					</div>
				</div>

				<div class="module_seventh_container" id="info-terms">
					<div class="module_seventh">
						<div class="narrow">
							<h3>Terms And Conditions</h3>
							<?php echo $tp_terms_copy; ?>
						</div>
					</div>
				</div>

			</div>

		</section>


		<?php if( $tp_medals_copy ) : ?>
			<section class="wrapper group center narrrow" id="pass-medals">
				<h2>Rock 'n' Roll Series Medals</h2>

				<?php echo $tp_medals_copy; ?>

			</section>
		<?php endif; ?>


		<section class="wrapper group" id="social">

			<h2 class="center">#RNRTourPass</h2>

			<?php if( $tp_tweets != '' ): ?>
				<div id="tp-quotes" class="owl-carousel oc-noborder">

					<?php foreach( $tp_tweets as $tweet ) { ?>
						<div>
							<p class="quote"><?php echo $tweet['quote']; ?></p>
							<p class="attrib"><?php echo $tweet['attribute']; ?></p>
						</div>
					<?php } ?>

				</div>
			<?php endif; ?>

			<?php if( $tp_pixlee ) : ?>

				<div class="ig_gallery">
					<div id="pixlee_container"></div>
					<?php echo $tp_pixlee; ?>
					<script src="//assets.pixlee.com/assets/pixlee_widget_1_0_0.js"></script>
				</div>

			<?php endif; ?>
		</section>


		<?php if( $tp_contact ) : ?>

			<section class="wrapper group" id="contact">

				<h2 class="center">Contact</h2>

				<?php echo $tp_contact; ?>

			</section>

		<?php endif; ?>

	</main>

	<script>
		jQuery( function( $ ) {
			$( '#info-tabs' ).tabs();
			$( '.info-tab-open' ).click( function ( event ) {
				/*
					var info_tab = $( this ).attr( 'href' );
					$( '#info-tabs' ).tabs( 'select', info_tab );
				*/
				$( '#info-tabs' ).tabs( 'option', 'active', 2 );
			} );
			$( '#pkg-tabs' ).tabs();
			$( '.pkg-tab-open' ).click( function ( event ) {
				$( '#pkg-tabs' ).tabs( 'option', 'active', 2 );
			} );
			/*
			$( '#perks-tabs' ).tabs();
			$( '.perks-tab-open' ).click( function ( event ) {
					var perks_tab = $( this ).attr( 'href' );
					$( '#perks-tabs' ).tabs( 'select', perks_tab );
			} );
			*/
		} );

		$( document ).ready( function() {
			$( ".tp_nav a, .backtotop, #subnav a" ).click( function( evn ) {
					evn.preventDefault();
					$( 'html,body' ).scrollTo( this.hash, this.hash );
			} );

			$( "#hm-limited" ).owlCarousel( {
				autoPlay:true,
				navigation:true,
				navigationText:false,
				pagination:false
			} );
			$( "#hm-remix" ).owlCarousel( {
				autoPlay:true,
				navigation:true,
				navigationText:false,
				pagination:false
			} );
			$( "#tp-quotes" ).owlCarousel( {
				autoPlay:true,
				navigation:false,
				pagination:false,
				singleItem:true
			} );
		} );

	</script>

<?php
	get_global_overlay();
	get_footer( 'oneoffs' );
?>
