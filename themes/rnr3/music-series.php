<?php
	/* Template Name: M's - Music (Series) */
	get_header('special');

	rnr3_get_secondary_nav( '', 'm-music' );
?>
<style>
	#hero-lower {
		background:url("<?php echo get_bloginfo('stylesheet_directory'); ?>/img/grey.gif") no-repeat scroll 50% 0;
		height:600px;
	}
</style>

<main role="main" id="main" class="m_music">
	<section id="music-panel1">
		<section class="wrapper">
			<section class="grid_2_special about">
				<div class="column_special column_left">
					<?php echo apply_filters( 'the_content', get_post_meta( get_the_ID(), '_rnr3_m_music_intro', 1 ) ); ?>
				</div>

				<div class="column_special column_right">
					<?php
						$intro_img_id	= rnr3_get_image_id( get_post_meta( get_the_ID(), '_rnr3_m_music_intro_img', 1 ) );
						$intro_img_alt	= get_post_meta( $intro_img_id, '_wp_attachment_image_alt', true );
					?>
					<figure>
						<img src="<?php echo get_post_meta( get_the_ID(), '_rnr3_m_music_intro_img', 1 ); ?>" alt="<?php echo $intro_img_alt; ?>">
					</figure>
				</div>
			</section>
		</section>
	</section>


	<section id="music-panel2">
		<section class="wrapper upcoming">
			<figure>
				<img src="<?php echo get_post_meta( get_the_ID(), '_rnr3_m_music_sponsor_logo', 1 ); ?>" alt="">
			</figure>

			<?php echo apply_filters( 'the_content', get_post_meta( get_the_ID(), '_rnr3_m_music_upcoming', 1 ) ); ?>

			<div class="controls">
				<label>Sort By</label>

				<button class="sort" data-sort="date:asc">Date</button>
				<button class="sort" data-sort="artist:asc">A-Z</button>
				<button class="sort" data-sort="city:asc">City</button>
			</div>

			<?php
				if ( false === ( $artists_query = get_transient( 'artists_query_results' ) ) ) {
					$args = array(
						'post_type'			=> 'upcoming_artist',
						'posts_per_page'	=> -1,
						'meta_key'			=> '_rnr3_artist_date',
						'orderby'			=> 'meta_value_num',
						'order'				=> 'ASC'
					);
					$artists_query = new WP_Query( $args );
					set_transient( 'artists_query_results', $artists_query, 5 * MINUTE_IN_SECONDS );
				}

				if( $artists_query->have_posts() ) {
					echo '<div class="mix_group">';
						while( $artists_query->have_posts() ) {
							$artists_query->the_post();

							$artist_location		= get_post_meta( get_the_ID(), '_rnr3_artist_location', 1 );
							$artist_location_filter	= str_replace( ' ', '-', explode( ',', $artist_location ) );
							$artist_date			= get_post_meta( get_the_ID(), '_rnr3_artist_date', 1 );
							$artist_url				= get_post_meta( get_the_ID(), '_rnr3_artist_url', 1 );
							$artist_race_url		= get_post_meta( get_the_ID(), '_rnr3_artist_race_url', 1 );

							if( get_the_post_thumbnail() == '' ) {
								$artist_thumbnail = '<img src="'.get_bloginfo( 'stylesheet_directory' ) . '/img/mmmm/tba-clean.svg">';
							} else {
								$artist_thumbnail = get_the_post_thumbnail();
							}

							echo '<div class="mix" data-artist="'.str_replace( ' ', '-', strtolower( get_the_title() ) ).'" data-city="'.strtolower( $artist_location_filter[0] ).'" data-date="'.$artist_date.'">
								<div class="inner_mix">
									<figure>'.$artist_thumbnail.'</figure>
									<div class="mix_info">
										<h3>'.get_the_title().'</h3>
										<section class="artist_deets_group">
											<div class="location">
												'.$artist_location.'
											</div>
											<div class="date">
												'.date( 'M j, Y', $artist_date ).'
											</div>
										</section>';
										/*<div class="link_group">
											<div class="link">
												<a class="headliner_link" href="'.$artist_url.'">Headliner Info</a>
											</div>
											<div class="link">
												<a class="racedetails_link" href="'.$artist_race_url.'">Race Details</a>
											</div>
										</div>*/
										echo '<div class="button_group button_group_center">
											<ul>
												<li><a href="'. $artist_url .'" class="button_single highlight red">Headliner Info</a></li>
												<li><a href="'. $artist_race_url .'" class="button_single highlight platinum">Race Details</a></li>
											</ul>
										</div>';
									echo '</div>
								</div>
							</div>';
						}
					echo '</div>';
				}

				wp_reset_postdata();

				echo '<div class="gap"></div>
				<div class="gap"></div>
				<div class="gap"></div>
				<div class="gap"></div>';

			?>
			<script src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/js/vendor/mixitup.min.js"></script>
			<script>
				var containerEl = document.querySelector('.mix_group');

				var mixer = mixitup(containerEl, {
					load: {
						sort: 'date:asc'
					}
				});

			</script>

		</section>
	</section>


	<section id="music-panel3">
		<section class="wrapper">
			<section class="grid_2_special radio">
				<div class="column_special column_left">
					<?php echo apply_filters( 'the_content', get_post_meta( get_the_ID(), '_rnr3_m_music_radio', 1 ) ); ?>
				</div>
				<div class="column_special column_right iheart">
					<?php echo get_post_meta( get_the_ID(), '_rnr3_m_music_iheart', 1 ); ?>
				</div>
			</section>
		</section>
	</section>


	<section id="music-panel4">
		<section id="hero-lower" class="lazy" data-original="<?php echo get_post_meta( get_the_ID(), '_rnr3_m_music_past_artists_bg', 1 ); ?>">
			<section id="bigtext-lower" class="wrapper mid bigtext">
				<h1 class="bigtext-line0">
					<span class="headline"><?php echo get_post_meta( get_the_ID(), '_rnr3_m_music_past_artists_hdr', 1 ); ?></span>
				</h1>
			</section>
		</section>
	</section>


	<section id="music-panel5">
		<section class="wrapper past-artists">
			<?php
				// don't echo the opening/closing "ul" tag
				$remove_uls			= array( '<ul>', '</ul>' );

				$past_artists_12	= apply_filters( 'the_content', get_post_meta( get_the_ID(), '_rnr3_m_music_past_artists_list12', 1 ) );
				$past_artists_12	= str_replace( $remove_uls, '', $past_artists_12 );
				$past_artists_12	= str_replace( "\n", '', $past_artists_12 );

				echo '<ul>';
					echo $past_artists_12;

					$past_artists_rest	= apply_filters( 'the_content', get_post_meta( get_the_ID(), '_rnr3_m_music_past_artists_list', 1 ) );
					$past_artists_rest	= str_replace( $remove_uls, '', $past_artists_rest );
					$past_artists_rest	= str_replace( '<li>', '<li class="no_mobile">', $past_artists_rest );
					$past_artists_rest	= str_replace( "\n", '', $past_artists_rest );

					echo $past_artists_rest;
				echo '</ul>';
			?>
		</section>
	</section>


	<section id="music-panel6">
		<section class="wrapper course-music">
			<figure>
				<img src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/mmmm/microphone-clean.svg">
			</figure>
			<h2 class="center"><?php echo get_post_meta( get_the_ID(), '_rnr3_m_music_course_ent_hdr', 1 ); ?></h2>

			<section class="grid_2_special">
				<div class="column_special column_right">
					<?php
						$coursemusic1_img_id	= rnr3_get_image_id( get_post_meta( get_the_ID(), '_rnr3_m_music_course_1_img', 1 ) );
						$coursemusic1_img_alt	= get_post_meta( $coursemusic1_img_id, '_wp_attachment_image_alt', true );
					?>
					<figure>
						<img src="<?php echo get_post_meta( get_the_ID(), '_rnr3_m_music_course_1_img', 1 ); ?>" alt="<?php echo $coursemusic1_img_alt; ?>">
					</figure>
				</div>
				<div class="column_special column_left">
					<?php echo apply_filters( 'the_content', get_post_meta( get_the_ID(), '_rnr3_m_music_course_1_txt', 1 ) ); ?>
				</div>
			</section>


			<?php
				$coursemusic2_img_id	= rnr3_get_image_id( get_post_meta( get_the_ID(), '_rnr3_m_music_course_2_img', 1 ) );
				$coursemusic2_img_alt	= get_post_meta( $coursemusic2_img_id, '_wp_attachment_image_alt', true );

				if( $coursemusic2_img_id != '' ) { ?>

					<section class="grid_2_special">
						<div class="column_special column_right">

							<figure>
								<img src="<?php echo get_post_meta( get_the_ID(), '_rnr3_m_music_course_2_img', 1 ); ?>" alt="<?php echo $coursemusic2_img_alt; ?>">
							</figure>
						</div>
						<div class="column_special column_left">
							<?php echo apply_filters( 'the_content', get_post_meta( get_the_ID(), '_rnr3_m_music_course_2_txt', 1 ) ); ?>
						</div>
					</section>

				<?php }
			?>

		</section>

	</section>
</main>
<script>
	$(document).ready(function(){

			/**
			 * This part causes smooth scrolling using scrollto.js
			 * We target all a tags inside the nav, and apply the scrollto.js to it.
			 */
			$("#secondary a, .scrolly").click(function(evn){
					evn.preventDefault();
					$('html,body').scrollTo(this.hash, this.hash);
			});
	});
</script>
<?php get_footer('series'); ?>
