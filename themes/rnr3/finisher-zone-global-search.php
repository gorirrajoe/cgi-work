<?php
	/**
	 * Template Name: Finisher Zone - Global Search & Results
	 * Notes: perhaps make `fz_results_grid` & `finisher-zone-leaderboard` & `fz_event_hdr` a function
	 */

	get_header('oneoffs');
	$prefix = '_rnr3_';
	require_once 'functions_fz.php';

	$redirecturl = $_SERVER['REDIRECT_URL'];

	$resultspage = $resultpage = '';

	if ( isset( $_GET['resultpage'] ) && is_numeric( $_GET['resultpage'] ) ) {
		$resultpage		= $_GET['resultpage'];
	}
	if ( isset( $_GET['resultspage'] ) && is_numeric( $_GET['resultspage'] ) ) {
		$resultspage	= $_GET['resultspage'];

		if ( isset( $_GET['perpage'] ) && is_numeric( $_GET['perpage'] ) ) {
			 $perpage	= $_GET['perpage'];
		}

		$firstname	= !empty( $_GET['firstname'] ) ? $_GET['firstname'] : '';
		$lastname	= !empty( $_GET['lastname'] ) ? $_GET['lastname'] : '';
		$email		= !empty( $_GET['email'] ) ? $_GET['email'] : '';
		$gender		= !empty( $_GET['gender'] ) ? $_GET['gender'] : '';
		$city		= !empty( $_GET['city'] ) ? $_GET['city'] : '';
		$state		= !empty( $_GET['state'] ) ? $_GET['state'] : '';

		$criteria = array(
			'resultspage' => $resultspage,
			'firstname'   => trim( $firstname ),
			'lastname'    => trim( $lastname ),
			'email'       => trim( $email ),
			'gender'      => trim( $gender ),
			'state'       => trim( $state ),
			'city'        => trim( $city ),
			'perpage'     => $perpage
		);

	}

	if ( isset( $_GET['gl_a_id'] ) && is_numeric( $_GET['gl_a_id'] ) ) {
		$gl_a_id= $_GET['gl_a_id'];
	}else{
		$gl_a_id = '';
	}

	$qt_lang      = rnr3_get_language();
	$qt_status    = $qt_lang['enabled'] == 1 ? true : false;
	$qt_cur_lang  = $qt_lang['lang'];

	include 'languages.php';

	$front_page_ID		= get_option( 'page_on_front' );
	$presenting_sponsor	= '';

	if ( ( $presenting_sponsor_img = get_post_meta( $front_page_ID, $prefix . 'sponsor_img', true ) ) != '' ) {
		$presenting_sponsor = '<img src="'. $presenting_sponsor_img .'">';

		$presenting_sponsor_url = get_post_meta( $front_page_ID, $prefix . 'sponsor_url', true );

		if ( $presenting_sponsor_url != ''  ) {
			$presenting_sponsor	= '<a href="'. $presenting_sponsor_url .'" target="_blank">'. $presenting_sponsor .'</a>';
		}

		$presenting_sponsor = '<div class="presented_by"><span>'. $presented_by_txt .'</span>'. $presenting_sponsor_img .'</div>';
	}
?>

	<main role="main" id="main" class="no-hero finisher_zone_search_results">

		<nav id="subnav">
			<section class="wrapper">

				<a href="<?php echo esc_url( home_url() ); ?>" class="fz-home">Finisher Zone</a>

				<div class="subnav-menu-label">Menu</div>

				<div class="subnav-menu-primary-event-container">
					<ul class="subnav-menu">
						<li><a href="<?php echo esc_url( home_url() ); ?>/#past_results"><?php echo trim($results_txt); ?></a></li>
						<li><a href="<?php echo esc_url( home_url() ); ?>/#badges"><?php echo $finisher_badges_txt; ?></a></li>
						<li><a href="<?php echo esc_url( home_url() ); ?>/#personas"><?php echo $runner_personas_txt; ?></a></li>
						<li><a href="<?php echo esc_url( home_url() ); ?>/#more_features"><?php echo $features_txt; ?></a></li>
					</ul>
				</div>

				<?php echo $presenting_sponsor; ?>

			</section>
		</nav>

		<?php
		if ( $gl_a_id != '' /*&& $resultpage == 1*/ ) {

			/*
			 * Runner selected (individual result)
			 */
			include( locate_template( 'template-parts/fz-global-single-result.php' ) );

		} elseif ( $resultspage != '' ) {

			/*
			 * Search results page
			 */
			include( locate_template( 'template-parts/fz-global-search-results.php' ) );

		} else {

			/*
			 * Default case -- landing with no event/year chosen
			 */
			include( locate_template( 'template-parts/fz-global-landing.php' ) );

		} ?>
	</main>

<?php
	// script for pinterest share
	echo '<script async defer src="//assets.pinterest.com/js/pinit.js"></script>';
	//script for po.st
	$post_pubkey  = rnr_get_option( 'rnrgv_post_pubkey');

	if ( !empty( $post_pubkey ) ) {
		echo '<script src="http://i.po.st/static/v3/post-widget.js#publisherKey='. $post_pubkey .'&retina=true" type="text/javascript"></script>';
	}

get_footer('series'); ?>
