<?php
	/* Template Name: Series Home */

	get_header( 'series-home' );

	$qt_lang = rnr3_get_language();
	include 'languages.php';

	$the_id		= get_the_ID();

	$miles_txt		= apply_filters( 'the_content', get_post_meta( $the_id, '_rnr3_miles_text', true ) );
	$music_txt		= apply_filters( 'the_content', get_post_meta( $the_id, '_rnr3_music_text', true ) );
	$medals_txt		= apply_filters( 'the_content', get_post_meta( $the_id, '_rnr3_medals_text', true ) );
	$moments_txt	= apply_filters( 'the_content', get_post_meta( $the_id, '_rnr3_moments_text', true ) );

	$tempo_enabled = ( rnr_get_option( 'rnrgv_tempo_enabled' ) == 'on' ) ? true : false;

	if ( false === ( $all_events = get_transient( 'all_events_data' ) ) ) {
		$all_events = rnr3_get_all_events();
	}

	$market = get_market2();
	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

?>

<!-- main content -->
<main role="main" id="main">

	<?php if( $tempo_enabled ) {

		// thumbnails for tempo
		$miles_bg_sm	= wp_get_attachment_image_src( get_post_meta( $the_id, '_rnr3_miles_bg_id', true ), 'finisher-medals', false )[0];
		$music_bg_sm	= wp_get_attachment_image_src( get_post_meta( $the_id, '_rnr3_music_bg_id', true ), 'finisher-medals', false )[0];
		$medals_bg_sm	= wp_get_attachment_image_src( get_post_meta( $the_id, '_rnr3_medals_bg_id', true ), 'finisher-medals', false )[0];
		$moments_bg_sm	= wp_get_attachment_image_src( get_post_meta( $the_id, '_rnr3_moments_bg_id', true ), 'finisher-medals', false )[0];

		$blog_title 	= get_post_meta( $the_id, '_rnr3_from_blog_title', true ) != '' ? apply_filters( 'the_content', get_post_meta( $the_id, '_rnr3_from_blog_title', true ) ) : 'From the blog';
		$blog_intro 	= get_post_meta( $the_id, '_rnr3_blog_intro_text', true ) != '' ? apply_filters( 'the_content', get_post_meta( $the_id, '_rnr3_blog_intro_text', true ) ) : 'Helpful stories to inform, advise, and motivate runners of all ages and abilities.'; ?>

		<section class="blog-row">
			<section class="wrapper">

				<?php switch_to_blog( 41 ); ?>

					<div class="blog icon">
						<span class="icon-blog_icon"></span>
					</div>

					<div class="blog-title">
						<a href="<?php echo get_site_url(); ?>"><?php echo $blog_title; ?></a>
					</div>

					<div class="blog-intro">
						<?php echo $blog_intro; ?>
					</div>

					<div class="blog-posts grid_3">
						<?php
							if( false === ( $blog_post_object = get_transient( 'series_home_tempo_posts' ) ) ) {
								$blog_posts = [];
								$featured_args = array(
									'posts_per_page'	=> '1',
									'meta_query'		=> array(
										array(
											'key'		=> '_rnr3_featured',
											'value'		=> 'on',
											'compare'	=> '=',
										)
									),
									'ignore_sticky_posts'		=> true,
									'no_found_rows'				=> true,
									'update_post_meta_cache'	=> false,
									'update_post_term_cache'	=> false

								);
								$video_args = array(
									'posts_per_page'	=> '1',
									'post__not_in'		=> $blog_posts,
									'tax_query'			=> array(
										array(
											'taxonomy'	=> 'post_format',
											'field'		=> 'slug',
											'terms'		=> array( 'post-format-video' )
										)
									),
									'ignore_sticky_posts'		=> true,
									'no_found_rows'				=> true,
									'update_post_meta_cache'	=> false,
									'update_post_term_cache'	=> false
								);

								$blog_post_1	= wp_list_pluck( get_posts( $featured_args ), 'ID' );
								$blog_post_2	= wp_list_pluck( get_posts( $video_args ), 'ID' );

								if( !empty( $blog_post_1 ) ) {
									array_push( $blog_posts, $blog_post_1[0] );
								}

								if( !empty( $blog_post_2 ) && ( $blog_post_2 !== $blog_post_1 ) ) {
									array_push( $blog_posts, $blog_post_2[0] );
								}

								$num_posts = 3 - ( count( $blog_posts ) );

								$extra_posts = wp_get_recent_posts( array(
									'orderby'		=> 'post_date',
									'numberposts'	=> $num_posts,
									'post_status'	=> 'publish',
									'exclude'		=> implode( ',', $blog_posts )
								) );

								if( !empty( $extra_posts ) ) {
									$extra_post_ids = wp_list_pluck( $extra_posts, 'ID' );

									foreach ( $extra_post_ids as $key => $value ) {
										if ( !in_array( $value, $blog_posts ) ) {
											array_push( $blog_posts, $value );
										}
									}

								}

								$blog_post_object = new WP_Query( array(
									'post__in'					=> $blog_posts,
									'orderby'					=> 'post__in',
									'posts_per_page'			=> 3,
									'ignore_sticky_posts'		=> true,
									'no_found_rows'				=> true,
									'update_post_meta_cache'	=> false,
									'update_post_term_cache'	=> false
								) );

								set_transient( 'series_home_tempo_posts', $blog_post_object, 5 * MINUTE_IN_SECONDS );

								echo '<!-- TEMPO POSTS REGENERATED -->';

							} else {
								echo '<!-- TEMPO POSTS FROM CACHE -->';
							}

							// default image if no featured images on posts
							$default_image_url		= get_option( 'default-featured-image' );
							$image_data				= $default_image_url != '' ? $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE guid='%s';", $default_image_url ) ) : get_stylesheet_directory_uri() .'/img/logo@2x.png';
							$default_image_thumb	= $default_image_url != '' ? wp_get_attachment_image_src( $image_data[0], 'blog-medium' ) : $image_data;

							if( $blog_post_object->have_posts() ) : while ( $blog_post_object->have_posts() ) : $blog_post_object->the_post(); ?>

								<div class="blog-single column">
									<?php
										$blog_post_id	= $blog_post_object->post->ID;
										// $blog_img		= has_post_thumbnail() ? get_the_post_thumbnail( $blog_post_id, 'blog-medium', array( 'class' => 'lazy' ) ) : $default_image_thumb;
										$blog_img_url	= has_post_thumbnail() ? get_the_post_thumbnail_url( $blog_post_id, 'blog-medium' ) : $default_image_thumb[0];
										$categories		= get_the_category( $blog_post_id );
										$auth_id		= $blog_post_object->post->post_author;
										$author_link	= get_author_posts_url( $auth_id );
										$author_name	= get_the_author_meta( 'display_name', $auth_id );
										$blog_post_link = get_the_permalink();
										$category		= $categories[0]->name;
										$category_id	= $categories[0]->term_id;
										$category_link	= get_category_link( $category_id );

										// check if CDN linker is on and do it's work for it, since plugin does not take switch_to_blog() into account
										if ( is_plugin_active('CDN-Linker-master/wp-cdn-linker.php') ) {
											$blog_img_url	= str_replace( get_site_url(), get_option('ossdl_off_cdn_url'), $blog_img_url );
										}

										// featured image
										if( has_post_format( 'video', $blog_post_id ) ) {
											echo '<figure class="video_blog_post"><a href="'. $blog_post_link .'"><img class="lazy" data-original="'. $blog_img_url .'"><img src="'.get_template_directory_uri() .'/img/play.svg" class="play_button"></figure></a>';
										} else {
											echo '<a href="'. $blog_post_link .'"><figure><img class="lazy" data-original="'. $blog_img_url .'"></figure></a>';
										}
									?>

									<div class="blog-single__text_wrapper">
										<h3 class="blog-single__title">
											<?php echo '<a href="' . $blog_post_link . '">' . $blog_post_object->post->post_title . '</a>';?>
										</h3>

										<div class="blog-single__excerpt">
											<?php echo has_excerpt() ? the_excerpt() : wp_trim_words( strip_shortcodes( $blog_post_object->post->post_content ), 20, '...' ); ?>
										</div>

										<div class="blog-single__byline">
											 <span class="blog-single__author"><a href="<?php echo $author_link; ?>">By <?php echo $author_name; ?></a></span> | <span class="blog-single__category"><a href="<?php echo $category_link; ?>"><?php echo $category; ?></a></span>
										</div>

										<div class="blog-single__read_more">
											<a href="<?php echo $blog_post_link;?>">
												<span class="blog-single__readmore"><?php echo has_post_format('video') ? 'See Video' : 'Read More'; ?> </span>
											</a>
										</div>

									</div>

								</div><!-- .BLOG_SINGLE.COLUMN -->

							<?php endwhile; endif;
						?>

					</div>

					<?php
						$nav_settings = array(
							'menu'				=> 'series-categories',
							'container'			=> '',
							'depth'				=> 0,
							'items_wrap'		=> '%3$s',
							'fallback_cb'		=> false,
							'theme_location'	=> '__no_such_location',
							'echo'				=> false,
						);

						$cat_menu = wp_nav_menu( $nav_settings );

						if ( $cat_menu !==  false) {

							echo '<div class="post_categories">';

								echo $browse_by_cat_txt .': ';
								echo strip_tags( $cat_menu, '<a>' );

							echo '</div>';

						}

						wp_reset_postdata();

				restore_current_blog(); ?>

			</section>

		</section>


		<section class="miles-row">
			<section class="wrapper">
				<div class="grid_4">

					<section id="miles" class="column miles-row__section">
						<div class="blackout lazy" data-original="<?php echo $miles_bg_sm; ?>" style="background-image:url(<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/blank.gif); width: 250px; height: 250px;"></div>
						<div class="outro">
							<h3>Miles</h3>
							<?php echo $miles_txt; ?>
						</div>
					</section>


					<section id="music" class="column miles-row__section">
						<div class="blackout lazy" data-original="<?php echo $music_bg_sm; ?>" style="background-image:url(<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/blank.gif); width: 250px; height: 250px;"></div>
						<div class="outro">
							<h3>Music</h3>
							<?php echo $music_txt; ?>
						</div>
					</section>


					<section id="medals" class="column miles-row__section">
						<div class="blackout lazy" data-original="<?php echo $medals_bg_sm; ?>" style="background-image:url(<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/blank.gif); width: 250px; height: 250px;"></div>
						<div class="outro">
							<h3>Medals</h3>
							<?php echo $medals_txt; ?>
						</div>
					</section>


					<section id="moments" class="column miles-row__section">
						<div class="blackout lazy" data-original="<?php echo $moments_bg_sm; ?>" style="background-image:url(<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/blank.gif); width: 250px; height: 250px;"></div>
						<div class="outro">
							<h3>Moments</h3>
							<?php echo $moments_txt; ?>
						</div>
					</section>

				</div><!-- END .GRID_4 -->
			</section>
		</section>

	<?php } else { // if no Tempo blog

		$miles_bg	= get_post_meta( $the_id, '_rnr3_miles_bg', true );
		$music_bg	= get_post_meta( $the_id, '_rnr3_music_bg', true );
		$medals_bg	= get_post_meta( $the_id, '_rnr3_medals_bg', true );
		$moments_bg	= get_post_meta( $the_id, '_rnr3_moments_bg', true ); ?>

		<style>
			#rnr-series #miles,
			#rnr-series #music,
			#rnr-series #medals,
			#rnr-series #moments {
				background-image:url("<?php echo get_bloginfo('stylesheet_directory'); ?>/img/grey.gif");
			}
		</style>

		<section id="miles" class="lazy" data-original="<?php echo $miles_bg; ?>">
			<div class="half-l">
				<div class="blackout">
					<div class="mid">
						<h3>Miles</h3>
						<?php echo $miles_txt; ?>
					</div>
				</div>
			</div>
		</section>


		<section id="music" class="lazy" data-original="<?php echo $music_bg; ?>">
			<div class="half-r">
				<div class="blackout">
					<div class="mid">
						<h3>Music</h3>
						<?php echo $music_txt; ?>
					</div>
				</div>
			</div>
		</section>


		<section id="medals" class="lazy" data-original="<?php echo $medals_bg; ?>">
			<div class="half-l">
				<div class="blackout">
					<div class="mid">
						<h3>Medals</h3>
						<?php echo $medals_txt; ?>
					</div>
				</div>
			</div>
		</section>


		<section id="moments" class="lazy" data-original="<?php echo $moments_bg; ?>">
			<div class="half-r">
				<div class="blackout">
					<div class="mid">
						<h3>Moments</h3>
						<?php echo $moments_txt; ?>
					</div>
				</div>
			</div>
		</section>

	<?php }

	rnr3_findrace( $all_events, $qt_lang );

	$pixlee = get_post_meta( get_the_ID(), '_rnr3_pixlee', TRUE );

	if( $pixlee != '' ) { ?>

		<!-- pixlee content -->
		<section id="socialfeed">
			<section class="wrapper">

				<h2><?php echo $event_info->twitter_hashtag; ?></h2>
				<?php echo $pixlee; ?>

			</section>
		</section>

	<?php } else {

		$pixlee_album = get_post_meta( get_the_ID(), '_rnr3_pixlee_album_id', 1 );

		if( $pixlee_album != '' ) { ?>

			<!-- pixlee content -->
			<section id="socialfeed">
				<section class="wrapper">

					<h2><?php echo $event_info->twitter_hashtag; ?></h2>
					<?php get_pixlee_grid( $pixlee_album ); ?>

				</section>
			</section>

		<?php }
	} ?>

</main>


<script>
	jQuery(document).ready(function($){

		$( "#primary a, .footer-nav a.scrolly, .register" ).click( function( evn ) {
			evn.preventDefault();
			$( 'html,body' ).scrollTo( this.hash, this.hash );
		});

	});
</script>


<?php
	get_global_overlay( $qt_lang );
	get_footer( 'series' );
?>
