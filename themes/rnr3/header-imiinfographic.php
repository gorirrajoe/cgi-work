		<!-- IMI Stuff -->
		<meta name="viewport" content="width=1200">
		<link href="<?php echo network_site_url(); ?>/wp-content/rnrinfo/san-diego/assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo network_site_url(); ?>/wp-content/rnrinfo/san-diego/assets/css/core.css" rel="stylesheet">


		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="assets/js/jquery.min.js"><\/script>')</script>

		<script src="http://code.createjs.com/createjs-2015.11.26.min.js"></script>


		<script src="<?php echo network_site_url(); ?>/wp-content/rnrinfo/san-diego/assets/js/states/animated_header.js"></script>

		<script>
			var canvas, stage, exportRoot;
			function init() {
				canvas = document.getElementById("canvas_header");
				exportRoot = new lib.animated_header();

				stage = new createjs.Stage(canvas);
				stage.addChild(exportRoot);
				stage.update();

				createjs.Ticker.setFPS(lib.properties.fps);
				createjs.Ticker.addEventListener("tick", stage);
			}
			window.onload = init;
		</script>
