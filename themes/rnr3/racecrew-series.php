<?php
	/* Template Name: Race Crew (Series) */

	get_header( 'series-home' );
	$prefix = '_rnr3_volunteer_';
	$id 			= get_the_id();
	$racecrew_meta = get_meta( $id );
	
	// meta boxes
	$vol_left  	 	= get_post_meta( $id, $prefix . 'left_info', true ); 
	$vol_right		= get_post_meta( $id, $prefix . 'right_info', true );
	$testimonials 	= get_post_meta( $id, $prefix . 'testimonials', true );
	$signup 		= get_post_meta( $id, $prefix . 'signup', true );
	$benefits		= get_post_meta( $id, $prefix . 'benefits_text', true );
	$rights			= get_post_meta( $id, $prefix . 'rights', true );
	$resp			= get_post_meta( $id, $prefix . 'resp', true );
	$gallery 		= get_post_meta( $id, $prefix . 'gallery', true );
	$faq 			= get_post_meta( $id, $prefix . 'faq', true );
	$positions 	 	= get_post_meta( $id, $prefix . 'position_guides', true );
	$gallery_count 	= 0;
	//event info
	if ( false === ( $all_events = get_transient( 'all_events_data' ) ) ) {
		$all_events = rnr3_get_all_events();
	}
	$non_us_events = array( 'mexico-city', 'merida', 'montreal', 'queretaro', 'vancouver', 'lisbon', 'madrid', 'dublin', 'virtual-run', 'liverpool', 'chengdu' );
	$event_array = [];
	//filter out events not in the US
	for ( $i = 0; $i < count( $all_events ) ; $i++ ) { 
		if( !in_array($all_events[$i]->event_slug , $non_us_events) ) {
			$event_array[$i]['name'] = $all_events[$i]->event_name;
			$event_array[$i]['url'] = $all_events[$i]->event_url;
		}
	}
	//alphabetically sort the array 
	array_multisort( $event_array, SORT_ASC );

	rnr3_get_secondary_nav( '', 'volunteer' );
	?>


	<main `="main" role="main">
		<section class="wrapper">
			<section class="grid_2_special" id="volunteer">
				<section class="wrapper">
					<column class="column_special column_left">
						<?php echo apply_filters( 'the_content', $vol_left );	?>
						<p id="red">I want to VOLUNTEER in <?php if($event_array != '' ){
							?>
							<select id="volunteer-select">
								<option value="#">-- Select a race --</option>
								<?php
								foreach ($event_array as $key => $value) {
									
									echo '<option value="'.$value['url'].'/volunteer/">'.$value['name'].'</option>';

								}?>
							</select>
							<script>
								$(function() {
									// bind change event to select
									$( '#volunteer-select' ).bind( 'change', function() {
										var url = $( this ).val(); // get selected value
										if( url ) { // require a URL
											window.location = url; // redirect
										}
										return false;
									});
								});
							</script>
						<?php
						}?>
						</p> 
					</column>
					<column class="column_special column_right">
						<img src="<?php echo  $vol_right ; ?>"/>
					</column>
				</section>
			</section>
		</section>

		<section id="updates">
			<section class="wrapper">
				<?php echo apply_filters( 'the_content', $signup );?>
			</section>
		</section>

		<section class="wrapper">
			<section id="benefits">
				<section class="wrapper">
					<div class="grid_2_special">
						<div class="column_special">
							<div class="grid_2_special">
								<?php echo apply_filters( 'the_content', $benefits );?>
								
							</div>
						</div>
						<div class="column_special">
							<?php if( !empty($testimonials ) ){

								echo apply_filters( 'the_content', $testimonials );

							}
							?>
						</div>
					</div>
					<div class="grid_2">
						<?php 
						if( $gallery != '' ) {
									$gallery_count++;

									echo 

									'<div class="gallery-carousel owl-theme ">';

										foreach( $gallery as $attachment_id => $attachment_url ) {
											$large_img_url	= wp_get_attachment_image_src( $attachment_id, 'large' );

											$img_deets = get_post( $attachment_id );

											echo '<div>
												<a data-caption="'. $img_deets->post_excerpt .'" data-fancybox="gallery-'. $gallery_count .'" href="'. $large_img_url[0] .'">'. wp_get_attachment_image( $attachment_id, 'distance-gallery' ) .'</a>';

												if( $img_deets->post_excerpt != '' ) {
													echo '<p>'. $img_deets->post_excerpt .'</p>';
												}
											echo '</div>';

										}
									echo '</div>';
								}

						?>
					</div>
				</section>
			</section>
			<section id="rc-info">
				<section class="wrapper group">
					<h3 class="center">RACE CREW INFO</h3>
					<div id="info-tabs" class="tp-tabs">
						<ul>
							<li><a href="#rc-faqs">FAQs</a></li>
							<li><a href="#rc-positions">Positions Guide</a></li>
							<li><a href="#rc-rights">Rights & Responsibilites</a></li>
						</ul>
					<div id="rc-faqs">
						<?php if( !empty( $faq ) ) {
							echo apply_filters( 'the_content', $faq );
						}?>
					</div>
					<div id="rc-positions">
						<p><?php if( !empty( $positions ) ) {
							echo apply_filters( 'the_content', $positions );
						}?></p>
					</div>
					<div id="rc-rights">
						<div class="grid_2_special">
		
								<div class="column_special">
									<?php echo apply_filters( 'the_content', $rights );?>
								</div>
								<div class="column_special">
									<?php echo apply_filters( 'the_content', $resp );?>
								</div>
						
						</div>
					</div>
				</section>
			</section>
		</section>	
		</section>
	</main>
	<script>
	jQuery( function( $ ) {
			$( '#info-tabs' ).tabs();
		} );

    $(document).ready(function(){
        
        /**
         * This part causes smooth scrolling using scrollto.js
         * We target all a tags inside the nav, and apply the scrollto.js to it.
         */
        $("#secondary a").click(function(evn){
            evn.preventDefault();
            $('html,body').scrollTo(this.hash, this.hash); 
        });
    });

        </script>

<?php get_footer('series'); ?>