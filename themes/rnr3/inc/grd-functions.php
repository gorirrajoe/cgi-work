<?php
/**
 * Global Running Day/Winter Promo
 * Template Specific Functions
 *
 * Dev Notes:
 * Winter Promo will now share the same template/functions as GRD to avoid unwanted branching of code
 *
 * Price Ranges: Update price ranges as needed: `grd_get_distanceprice_ranges()`
 * Date Ranges: Update the date ranges, based on astronomical seasons: `grdpromo_findrace()` && `grd_get_quarters()`
 * Javascript: update datafilterarray in `onMixClick` to correspond with date range updates: `grd-mixitup.js`
 * SurveyGizmo: Update `$sg_url` based on updated form: `grd-sg-proxy.php`
 */


/**
 * price ranges for the distance filter bar
 * if price ranges change:
 * modify below, grd_get_distances(), and mixitup (js)
 *
 * @return [arr]  array of ranges as set by marketing
 */
function grd_get_distanceprice_ranges() {
	$range_array = array(
		'fivek'	=> array(
			'ranges' => array( 30.00, 50.00 )
		),
		'tenk'	=> array(
			'ranges' => array( 40.00, 50.00 )
		),
		'halfmarathon'	=> array(
			'ranges' => array( 60.00, 100.00 )
		),
		'marathon'	=> array(
			'ranges' => array( 80.00, 100.00 )
		),
		'twentyk' => array(
			'ranges' => array()
		),
		'kidsrock' => array(
			'ranges' => array()
		),
		'funrun' => array(
			'ranges' => array()
		),
		'onemile' => array(
			'ranges' => array()
		),
		'onek' => array(
			'ranges' => array()
		),
		'relay' => array(
			'ranges' => array()
		)
	);

	return $range_array;
}


/**
 * get list of all sale_promo's (cpt)
 * transient: 5 minute cache
 *
 * @return [type] [description]
 */
function get_grd_wpquery( $is_grd ) {
	$qt_lang = rnr3_get_language();

	$post_type_to_use = $is_grd == true ? 'sale_promo' : 'welcome_locations';

	// order doesn't really matter because js will handle initial sort
	if ( false	=== ( $promo_locations	= get_transient( 'promo_locations_results_' . $qt_lang['lang'] ) ) ) {
		$args = array(
			'post_type'			=> $post_type_to_use,
			'posts_per_page'	=> -1,
			'post_status'		=> 'publish',
			'orderby'			=> 'name',
			'order'				=> 'ASC'
		 );
		$promo_locations = new WP_Query( $args );
		set_transient( 'promo_locations_results_' . $qt_lang['lang'], $promo_locations, 5 * MINUTE_IN_SECONDS );
		echo '<!-- new query -->';
	} else {
		echo '<!-- cached -->';
	}

	return $promo_locations;
}


/**
 * returns find a race mixitup grid
 */
function grdpromo_findrace( $phase, $qt_lang, $event_info, $is_grd = true ) {
	$content = '';

	include get_template_directory() . '/languages.php';

	$promo_locations = get_grd_wpquery( $is_grd );


	if( $promo_locations->have_posts() ) {

		$content .= '<section id="findarace">
			<div class="mix_group">
				<div class="mix_group__fail-message">
					<p>No events were found matching the current filters.</p>
					<button class="mix_group__fail-button" type="button" data-filter="all" data-basedistance="halfmarathon">Reset Filters</button>
				</div>';

				while( $promo_locations->have_posts() ) {
					$promo_locations->the_post();

					$promo_id = get_the_ID();

					// get ALL meta data
					$single_event_meta = get_meta( $promo_id );

					// set distance to be used as keys for mixitup and display in modal
					$distance_key = array();


					/**
					 * get distances for the single event
					 * need to get this early to be able to set the classes for the mix div
					 */
					$entries = array_key_exists( '_rnr3_race_group', $single_event_meta ) ? unserialize( $single_event_meta['_rnr3_race_group'] ) : '';

					if( $entries != '' ) {
						foreach( $entries as $key => $entry ) {
							$distance_pieces	= explode( '::', $entry['type'] );
							$distance_key[]		= $distance_pieces[0];
						}
					}

					$distanceprice_class_array	= grd_get_distanceprice( $entries, $qt_lang );
					$distanceprice_class_list	= implode( ' ', $distanceprice_class_array );

					// list distances, separated by spaces, for mix filter
					$event_distances		= implode( ' ', $distance_key );

					$start_date_timestamp	= array_key_exists( '_rnr3_startdate', $single_event_meta ) ? $single_event_meta['_rnr3_startdate'] : '';
					$end_date_timestamp		= array_key_exists( '_rnr3_enddate', $single_event_meta ) ? $single_event_meta['_rnr3_enddate'] : '';


					/**
					 * astronomical seasons - for mike (thanks joey)
					 * will need to be updated for following years
					 */
					date_default_timezone_set('America/Los_Angeles');
					$winter17_start	= strtotime( 'december 21, 2017' );		// unix: 1513843200
					$spring18_start	= strtotime( 'march 20, 2018' );		// unix: 1521529200
					$summer18_start	= strtotime( 'june 21, 2018' );			// unix: 1529564400
					$fall18_start	= strtotime( 'september 22, 2018' );	// unix: 1537599600

					if( $start_date_timestamp >= $winter17_start && $start_date_timestamp < $spring18_start ) {
						$seasons_class = 'winter17';
					} elseif( $start_date_timestamp >= $spring18_start && $start_date_timestamp < $summer18_start ) {
						$seasons_class = 'spring18';
					} elseif( $start_date_timestamp >= $summer18_start && $start_date_timestamp < $fall18_start ) {
						$seasons_class = 'summer18';
					} elseif( $start_date_timestamp >= $fall18_start ) {
						$seasons_class = 'fall18';
					}


					$festival	= ( array_key_exists( '_rnr3_festival', $single_event_meta ) && $single_event_meta['_rnr3_festival'] == 'on' ) ? 'on' : 'off';
					$event_year	= ', ' . date( 'Y', $start_date_timestamp );

					// build race date based on language
					$display_date	= grd_get_racedate( $start_date_timestamp, $end_date_timestamp, $event_year, $festival, $qt_lang );

					// mix filters
					$is_featured	= array_key_exists( '_rnr3_featured', $single_event_meta ) && $single_event_meta['_rnr3_featured'] == 'on' ? 1 : 0;
					$id_city		= grd_city_classname( get_the_title() );

					$tagline_array	= array_key_exists( '_rnr3_tagline', $single_event_meta ) && $single_event_meta['_rnr3_tagline'] != '' ? qtrans_split( $single_event_meta['_rnr3_tagline'] ) : '';
					$tagline		= $tagline_array != '' ? '<h3>'. $tagline_array[$qt_lang['lang']] .'</h3>' : '';


					/**
					 * start mix box - event
					 */
					$content .= '<div class="mix '. $event_distances . ' ' . $id_city .' '. $seasons_class .' '. $distanceprice_class_list .'" data-city="'. $id_city .'" data-date="'. $start_date_timestamp .'" data-featured="'. $is_featured .'">';

						if( $is_featured == 1 ) {
							$content .= '<div class="mix-featured"><span>Featured Race</span></div>';
						}

						$content .= '<div class="inner_mix">

							<div class="inner_mix_tabletup_column">

								<h3 class="mix-title">'. get_the_title() .'</h3>

								<p class="mix-date">'. $display_date .'</p>';


								/**
								 * distance list
								 * click or hover to change price of that event on the fly (js)
								 */
								$content .= '<ul class="mix-distancelist">';

									foreach( $distance_key as $distance ) {

										if ( !empty( ${$distance . '_distancelist'} ) ) {

											$content .= '<li>
												<div class="mix-distancelist-item" data-distancelist-city="'. $id_city .'" data-distancelist-distance="mix-price-'. $distance .'">'. ${$distance . '_distancelist'} .'</div>
											</li>';

										}

									}

								$content .= '</ul>';


								/**
								 * image/video and price columns
								 * images are lazy loaded for perf
								 */
								$event_featuredimg = get_the_post_thumbnail_url( $promo_id, 'medium' );

								$content .= '<div class="mix_grid_2">

									<div class="mix_grid_2_column grd_mobile">
										<img class="lazy" data-original="'. $event_featuredimg .'">';

										// details toggle link that only appears on mobile (<720px)
										$content .= '<p><a class="mix-details-toggle grd_mobile" data-post-id="'. $promo_id .'" data-toggle-div="toggle-'. $id_city .'" href="#">'. $getdetails_txt .'<span class="icon-play"></span></a></p>
									</div>

									<div class="mix_grid_2_column">';

										$content .= grd_get_price( $entries, $phase, $promo_id, $qt_lang, $event_info ) .'

									</div>

								</div>

							</div>';

							// on >720px, show youtube thumb or gallery in collapsed card
							$content .= '<div class="grd_tablet inner_mix_tabletup_column">'.

								grd_get_youtube_or_gallery( $promo_id, 1 ) .'

							</div>';

							// on >960px, show tagline and details toggle link in collapsed card
							$content .= '<div class="grd_desktop inner_mix_desktopup_column">'.

								$tagline .'
								<p><a class="mix-details-toggle" data-post-id="'. $promo_id .'" data-toggle-div="toggle-'. $id_city .'" href="#">'. $getdetails_txt .'<span class="icon-play"></span></a></p>

							</div>
						</div>';

						// content that gets toggled hide/display
						$content .= '<div style="display: none;" class="mix-togglecontent" id="toggle-'. $id_city .'">'.

							$tagline .'

							<div class="mix-togglecontent__amenities mix-togglecontent-content">
								<div class="mix-triangle-down"></div>
								<div class="mix-togglecontent__title"><h4>'. $grd_race_amenities_txt .'</h4></div>

								<div class="mix-togglecontent__amenities-grid">
									<div class="mix-togglecontent__amenities-column">';

										if( array_key_exists( '_rnr3_amenities1', $single_event_meta ) && $single_event_meta['_rnr3_amenities1'] != '' ) {

											$content .= grd_get_eventamenities( $single_event_meta['_rnr3_amenities1'], $qt_lang, $event_info );

										}

									$content .='</div>
									<div class="mix-togglecontent__amenities-column">';

										if( array_key_exists( '_rnr3_amenities2', $single_event_meta ) && $single_event_meta['_rnr3_amenities2'] != '' ) {

											$content .= grd_get_eventamenities( $single_event_meta['_rnr3_amenities2'], $qt_lang, $event_info );

										}

									$content .='</div>
								</div>
							</div>

							<div class="mix-togglecontent__features mix-togglecontent-content">
								<div class="mix-triangle-down"></div>
								<div class="mix-togglecontent__title"><h4>'. $grd_race_features_txt .'</h4></div>' .

								grd_get_eventfeatures( $single_event_meta, $qt_lang, $event_info ) .'

							</div>';

							$event_website = array_key_exists( '_rnr3_event_url', $single_event_meta ) ? $single_event_meta['_rnr3_event_url'] : '';
							if( $event_website != '' ) {

								$content .= '<div class="mix-togglecontent__link">
									<a href="'. $event_website .'" target="_blank">'. $grd_website .'</a>
								</div>';

							}


							$content .= '<div class="mix-togglecontent__media">'.
								grd_get_youtube_or_gallery( $promo_id ) .'
							</div>

						</div>
					</div>';

				}
				wp_reset_postdata();

				$content .= '<div class="gap"></div>
			</div>
		</section>';
	}

	return $content;
}


/**
 * formats surveygizmo form based on its location
 *
 * @param  [bool] $is_marquee     location: marquee or inline (modal)
 * @param  [obj]  $all_post_meta  post meta from page
 * @param  [arr]  $qt_lang        language array
 * @return [str]                  form
 */
function grd_getsurveygizmo_form( $is_marquee, $all_post_meta, $qt_lang ) {

	$surveygizz_form	= '';

	if( $is_marquee != 1 ) {
		$signup_intro_array	= array_key_exists( '_rnr3_signup_txt', $all_post_meta ) ? qtrans_split( $all_post_meta['_rnr3_signup_event_txt'] ) : '';
		$inline_class	= '__inline';
		$form_hide		= ' style="display: none;" id="grd-sg__inline"';
		$button_value	= '';
	} else {
		$signup_intro_array	= array_key_exists( '_rnr3_signup_txt', $all_post_meta ) ? qtrans_split( $all_post_meta['_rnr3_signup_txt'] ) : '';
		$inline_class	= '';
		$form_hide		= '';
		$button_value	= ' value="marquee"';
	}


	if( $qt_lang['lang'] == 'es' ) {
		$emailaddress_placeholder	= 'Direcci&oacute;n de email';
		$submit_btn					= 'Enviar';
	} elseif( $qt_lang['lang'] == 'fr' ) {
		$emailaddress_placeholder	= 'Adresse courriel';
		$submit_btn					= 'Envoyer';
	} else {
		$emailaddress_placeholder	= 'Email Address';
		$submit_btn					= 'Submit';
	}


	$signup_intro	= $signup_intro_array != '' ? $signup_intro_array[$qt_lang['lang']] : '';

	/**
	 * form validation/responses
	 * user can enter different languages in admin
	 */
	$signup_success_array	= array_key_exists( '_rnr3_signup_success_txt', $all_post_meta ) ? qtrans_split( $all_post_meta['_rnr3_signup_success_txt'] ) : '';
	$signup_success			= $signup_success_array != '' ? $signup_success_array[$qt_lang['lang']] : '';
	$signup_empty_array		= array_key_exists( '_rnr3_signup_empty_txt', $all_post_meta ) ? qtrans_split( $all_post_meta['_rnr3_signup_empty_txt'] ) : '';
	$signup_empty			= $signup_empty_array != '' ? $signup_empty_array[$qt_lang['lang']] : '';
	$signup_fail_array		= array_key_exists( '_rnr3_signup_otherfail_txt', $all_post_meta ) ? qtrans_split( $all_post_meta['_rnr3_signup_otherfail_txt'] ) : '';
	$signup_fail			= $signup_fail_array != '' ? $signup_fail_array[$qt_lang['lang']] : '';

	$surveygizz_form .= '<div class="grd-sg"'. $form_hide .'>

		<div class="grd-sg-intro">'. $signup_intro .'</div>

		<form id="grd-sg'. $inline_class .'-datacap" name="grd-sg'. $inline_class .'-datacap" class="grd-sg-datacap" method="POST">
			<input type="email" id="grd-sg'. $inline_class .'-email" class="grd-sg-email" name="grd-sg'. $inline_class .'-email" placeholder="'. $emailaddress_placeholder .'">
			<input type="hidden" id="grd-sg'. $inline_class .'-event" name="grd-sg'. $inline_class .'-event" value="series">
			<button type="submit" class="grd-sg'. $inline_class .'-submit"'. $button_value .'>'. $submit_btn .'</button>
		</form>

		<p class="grd-sg-success" style="display: none;">'. $signup_success .'</p>
		<div class="grd-sg'. $inline_class .'-msg grd-sg-empty" style="display: none;">'. $signup_empty .'</div>
		<div class="grd-sg'. $inline_class .'-msg grd-sg'. $inline_class .'-failure" style="display: none;">'. $signup_fail .'</div>

	</div>';

	return $surveygizz_form;

}


/**
 * GRD countdown timer
 * NOTE: countdown js assumes GMT (and can't be updated apparently)
 */
function grd_countdown_timer( $current_phase_num, $lang_info ) {
	if( $current_phase_num != 2 ) {
		if( $lang_info['lang'] == 'es' ) {
			$countdown_day	= "D&#237;as";
			$countdown_hour	= "Horas";
			$countdown_min	= "Min";
			$countdown_sec	= "Seg";
		} elseif( $lang_info['lang'] == 'fr' ) {
			$countdown_day	= "Jours";
			$countdown_hour	= "Heures";
			$countdown_min	= "Min";
			$countdown_sec	= "Sec";
		} elseif( $lang_info['lang'] == 'pt' ) {
			$countdown_day	= "Dias";
			$countdown_hour	= "Horas";
			$countdown_min	= "Mins";
			$countdown_sec	= "Seg";
		} else {
			$countdown_day	= "Days";
			$countdown_hour	= "Hrs";
			$countdown_min	= "Min";
			$countdown_sec	= "Sec";
		}


		/**
		 * for promo phase, we want to countdown to the end of the day (23:59)
		 * for teaser and upsell, we want to countdown to beginning of next day (midnight)
		 */
		if( $current_phase_num == 1 ) {

			$phase_date	= get_post_meta( get_the_ID(), '_rnr3_phase1_start', 1 );

			// will end at 11:59pm (23:59)
			$clock_time	= $phase_date + ( 24 * 60 * 60 ) + ( 59 * 60 );

		} else {

			$next_phase_date_key	= '_rnr3_phase' . ( $current_phase_num + 1 ) . '_start';
			$next_phase_date		= get_post_meta( get_the_ID(), $next_phase_date_key, 1 );

			// will start at midnight
			$clock_time				= $next_phase_date;

		}


		$countdown_clock = '<table data-countdown="'. $clock_time .'" class="grd-countdown-table countdown">
			<tr>';

				// hide first column on promo phase (it'll only say 0 days)
				if( $current_phase_num != 1 ) {

					$countdown_clock .= '<td><div class="grd-countdown-table-val countdown-days">--</div> <div class="grd-countdown-table-label">'. $countdown_day .'</div></td>';

				}

				$countdown_clock .= '<td><div class="grd-countdown-table-val countdown-hrs">--</div> <div class="grd-countdown-table-label">'. $countdown_hour .'</div></td>
				<td><div class="grd-countdown-table-val countdown-mins">--</div> <div class="grd-countdown-table-label">'. $countdown_min .'</div></td>';

				if( $current_phase_num != 0 ) {

					$countdown_clock .= '<td><div class="grd-countdown-table-val countdown-secs">--</div> <div class="grd-countdown-table-label">'. $countdown_sec .'</div></td>';

				}

			$countdown_clock .= '</tr>
		</table>';

		return $countdown_clock;
	} else {
		return;
	}

}


/**
 * date formatting
 */
function grd_get_racedate( $start, $end, $year, $festival, $qt_lang ) {

	date_default_timezone_set('GMT' );
	// qt translate on!
	if( $qt_lang['enabled']	== 1 && $qt_lang['lang'] != 'en' ) {

		$start_date_d			= date( 'j', $start );
		$start_date_m_foreign	= get_day_or_month_by_language( $qt_lang['lang'], ( date( 'n', $start ) - 1 ), 'month' );
		$start_date				= $start_date_d . ' ' . $start_date_m_foreign;

		// multi-day event
		if( $festival == 'on' ) {

			$end_date_d			= date( 'j', $end );
			$end_date_m_foreign	= get_day_or_month_by_language( $qt_lang['lang'], ( date( 'n', $end ) - 1 ), 'month' );


			/**
			 * diff months vs same month
			 */
			if( date( 'M', $start ) != ( date( 'M', $end ) ) ) {
				$end_date		= $end_date_d . ' ' . $end_date_m_foreign;
				$display_date	= $start_date . '-' . $end_date . $year;
			} else {
				$display_date	= $start_date_d . '-' . $end_date_d . ' ' . $end_date_m_foreign . $year;
			}

		} elseif( $start == '1591747200' ) {

			// set date in admin to jun 10, 2020
			// arbitrary date because it NEEDS a date, but they want tbd
			$display_date = 'TBD';

		} else {

			$display_date = $start_date . $year;

		}

	} else {

		// qt translate off
		$start_date	= date( 'M d', $start );

		// multi-day event
		if( $festival == 'on' ) {


			/**
			 * diff months vs same month
			 */
			if( date( 'M', $start ) != ( date( 'M', $end ) ) ) {
				$end_date	= date( 'M d', $end );
				$display_date	= $start_date . '-' . $end_date . $year;
			} else {
				$display_date	= $start_date . '-' . date( 'd', $end ) .$year;
			}

		} elseif( $start	== '1591747200' ) {

			// set date in admin to jun 10, 2020
			// arbitrary date because it NEEDS a date, but they want tbd
			$display_date	= 'TBD';

		} else {

			$display_date	= $start_date . $year;

		}

	}

	return $display_date;
}


/**
 * get the thumbnail (either the gallery or the youtube thumbnail)
 * youtube id will override the gallery
 * gallery will be loaded via ajax!
 * $tabletup is
 *
 * @param  [int] $postid    id of the event
 * @param  [int] $tabletup  gallery will have a different set up since it's displaying on card
 * @return [str]            image
 */
function grd_get_youtube_or_gallery( $postid, $tabletup = 0 ) {
	$gallery_imgs = get_post_meta( $postid, '_rnr3_modal_gallery', 1 );

	if( ( $youtubeid = get_post_meta( $postid, '_rnr3_youtube', 1 ) ) != '' ) {

		// thumbnail will be taken from youtube
		// make sure a good thumbnail is chosen!
		$thumbnail = '<a class="grd-youtube" data-fancybox href="https://www.youtube.com/watch?v='. $youtubeid .'&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0"><img width="480" height="360" class="lazy" data-original="https://img.youtube.com/vi/'. $youtubeid .'/hqdefault.jpg" alt="'. get_the_title( $postid ) .'"><span class="icon-youtube-play"></span></a>';

	} elseif( $tabletup == 1 && $gallery_imgs != '' ) {

		$thumbnail = '<div class="mix-modal-carousel-tabletup owl-carousel">';

			foreach( $gallery_imgs as $id => $url ) {

				$lazyimg = wp_get_attachment_image_src( $id, 'medium' );
				$thumbnail .= '<div><img data-src="'. $lazyimg[0] .'" class="lazyOwl" alt=""></div>';

			}

		$thumbnail .= '</div>';

	} elseif( $tabletup == 0 && $gallery_imgs != '' ) {

		$thumbnail = '<div class="mix-modal-carousel owl-carousel"></div>';

	} else {

		$thumbnail = '';

	}

	return $thumbnail;
}


/**
 * format price properly (cents, foreign currency)
 * make sure ALL prices have cents
 *
 * @param  [arr] $entries  price group
 * @param  [arr] $qt_lang  language
 * @return [str]           properly formatted price
 */
function grd_get_price( $entries, $phase, $promo_id, $qt_lang, $event_info ) {
	include get_template_directory() . '/languages.php';

	$event_register_url	= get_post_meta( get_the_ID(), '_rnr3_register_url', 1 ) != '' ? get_post_meta( get_the_ID(), '_rnr3_register_url', 1 ) : '';
	$id_city			= grd_city_classname( get_the_title( $promo_id ) );

	$price_group = '<div class="mix-price-group">';

		foreach( $entries as $key => $value ) {

			$distance_pieces = explode( '::', $value['type'] );


			/**
			 * if distance is marked as closed or sold out, don't show any of its price fields
			 */
			if( array_key_exists( 'reg_closed', $value ) && $value['reg_closed'] == 'on' ) {

				$price_group .= '<span class="mix-price-span mix-price-'. $distance_pieces[0] .'">
					<p class="mix-price-span__regclosed">'. $grd_event_reg_closed .'</p>
				</span>';

			} elseif( array_key_exists( 'sold_out', $value ) && $value['sold_out'] == 'on' ) {

				$price_group .= '<span class="mix-price-span mix-price-'. $distance_pieces[0] .'">
					<p class="mix-price-span__regclosed">'. $grd_event_reg_soldout .'</p>
				</span>';

			} else {

				/**
				 * price
				 * check for cents and superscript it
				 */
				$price_lang_array	= qtrans_split( $value['price'] );
				$price_lang			= $price_lang_array[$qt_lang['lang']];
				$dollars_cents		= explode( '.', $price_lang );

				if( count( $dollars_cents ) > 1 ) {

					// case for MXN, CAD
					$foreign_notation = explode( ' ', $dollars_cents[1] );

					if( count( $foreign_notation ) > 1 ) {

						$foreign_suffix = '<span class="mix-price-span-code">'. $foreign_notation[1] .'</span>';

					} else {

						$foreign_suffix = '';

					}

					$dollars_cents_combined = $dollars_cents[0] . '<span class="mix-price-span-cents">'. $foreign_notation[0] .'</span>' . $foreign_suffix;

					// check for dollar (or pound/euro) sign
					$display_price = preg_replace( '/^(\$|\£|\€)/', "<span class='mix-price-span-symbol'>$1</span>", $dollars_cents_combined );

				} else {

					// don't display price if it's not formed properly
					// $display_price = preg_replace( '/^(\$|\£|\€)/', "<span class='mix-price-span-dollar'>$1</span>", $value );
					$display_price = '';

				}


				/**
				 * distance and date
				 * ex: "Marathon / Sunday"
				 */
				$display_distance		= !empty( ${ $distance_pieces[0] . '_distancename' } ) ? ${ $distance_pieces[0] . '_distancename' } : '';
				$distance_dateofweek	= date( 'w', $value['distance_date'] );
				$display_distance_date	= get_day_or_month_by_language( $qt_lang['lang'], $distance_dateofweek, 'day' );


				/**
				 * savings
				 */
				$savings_lang_array	= array_key_exists( 'savings', $value ) ? qtrans_split( $value['savings'] ) : '';
				$savings_lang		= $savings_lang_array != '' ? $savings_lang_array[$qt_lang['lang']] : '';

				if( $savings_lang != '' ) {
					if( $qt_lang['lang'] == 'fr' ) {

						$display_savings = '<p class="mix-price-span__savings">'. $savings_txt . ' ' . $savings_lang .'</p>';

					} else {

						$display_savings = '<p class="mix-price-span__savings">'. $savings_lang . ' ' . $savings_txt .'</p>';

					}
				} else {
					$display_savings = '';
				}


				/**
				 * limited?
				 */
				$display_limited = array_key_exists( 'limited', $value ) && $value['limited'] == 'on' ? '<p class="mix-price-span__limited">'. $limitedsupply_txt .'</p>' : '';


				/**
				 * at expo?
				 */
				$display_atexpo = array_key_exists( 'at_expo', $value ) && $value['at_expo'] == 'on' ? '<p class="mix-price-span__limited">'. $atexpo_txt .'</p>' : '';


				$price_group .= '<span class="mix-price-span mix-price-'. $distance_pieces[0] .'">
					<p class="mix-price-span__price">'. $display_price .'</p>
					<div class="mix-price-span__price-deets">
						<p class="mix-price-span__distance">'. $display_distance .' / '. $display_distance_date .'</p>'.
						$display_savings .
						$display_limited .
						$display_atexpo .'
					</div>
				</span>';
			}
		}

	$price_group .= '</div>';

	if( $phase == 0 ) {

		$price_group .= '<div class="mix-cta-group">
			<p><a data-fancybox data-src="#grd-sg__inline" data-sg-event="'. $id_city .'" href="javascript:;" class="grd-sg-cta cta">'. $remindme_txt .'</a></p>
			<p><a class="mix-details-toggle grd_tablet grd_desktop_no" data-post-id="'. $promo_id .'" data-toggle-div="toggle-'. $id_city .'" href="#">'. $getdetails_txt .'<span class="icon-play"></span></a></p>
		</div>';

	} else {

		$price_group .= '<div class="mix-cta-group">';

			foreach( $entries as $key => $value ) {
				$distance_pieces = explode( '::', $value['type'] );

				if( array_key_exists( 'sold_out', $value ) && $value['sold_out'] == 'on' ) {

					$price_group .= '<p class="mix-price-span mix-price-'. $distance_pieces[0] .'"><span class="highlight black">'. $txt_sold_out .'</span></p>';

				} elseif( array_key_exists( 'reg_closed', $value ) && $value['reg_closed'] == 'on' ) {

					if( array_key_exists( 'sg_url', $value ) && $value['sg_url'] != '' ) {

						$price_group .= '<p class="mix-price-span mix-price-'. $distance_pieces[0] .'"><a data-fancybox data-src="'. $value['sg_url'] .'" href="javascript:;" class="grd-sg-cta cta">'. $txt_reg_closed .'</a></p>';

					} else {

						$price_group .= '<p class="mix-price-span mix-price-'. $distance_pieces[0] .'"><span class="highlight black">'. $txt_reg_closed .'</span></p>';

					}


				} elseif( array_key_exists( 'at_expo', $value ) && $value['at_expo'] == 'on' ) {

					$price_group .= '<p class="mix-price-span mix-price-'. $distance_pieces[0] .'"><span class="highlight black">'. $txt_reg_closed .'</span></p>';

				} else {

					$price_group .= '<p class="mix-price-span mix-price-'. $distance_pieces[0] .'"><a href="'. $event_register_url .'" class="cta" target="_blank">'. $registernow_txt .'</a></p>';

				}
			}

			// details toggle link that only appears on tablet (>720px, <960px)
			$price_group .= '<p><a class="mix-details-toggle grd_tablet grd_desktop_no" data-post-id="'. $promo_id .'" data-toggle-div="toggle-'. $id_city .'" href="#">'. $getdetails_txt .'<span class="icon-play"></span></a></p>

		</div>';

	}

	return $price_group;
}


/**
 * get cities for filterbars
 *
 * @return str  unordered list of all cities (sale_promo cpt)
 */
function grd_get_cities( $qt_lang, $is_grd = true ) {
	if( $qt_lang['lang'] == 'es' ) {
		$allevents_txt	= 'Todos Los Eventos';
	} elseif( $qt_lang['lang'] == 'fr' ) {
		$allevents_txt	= 'All Events';
	} else {
		$allevents_txt	= 'All Events';
	}


	$citylist = '';

	$promo_locations = get_grd_wpquery( $is_grd );

	if( $promo_locations->have_posts() ) {

		$citylist .= '<ul class="grd-filterbar-content-controls">
			<li><button type="button" class="grd-filterbar-content-control" data-filter="all">'. $allevents_txt .'</button></li>';

			while( $promo_locations->have_posts() ) {

				$promo_locations->the_post();

				$cityname_class = grd_city_classname( get_the_title() );

				$citylist .= '<li><button type="button" class="grd-filterbar-content-control" data-toggle=".'. $cityname_class .'">'. get_the_title() .'</button></li>';

			}

		$citylist .= '</ul>';

		wp_reset_postdata();
	}

	return $citylist;

}


/**
 * get distances for filterbars
 * for non english, just show standard list of distances (no prices)
 *
 * @return [str]  unordered list of various distances to filter
 */
function grd_get_distances( $qt_lang, $event_info ) {
	include get_template_directory() . '/languages.php';

	if( $qt_lang['lang'] != 'en' ) {

		$distancelist = '<ul class="grd-filterbar-content-controls">
			<li><button type="button" class="grd-filterbar-content-control" data-filter="al"l>'. $alldistances_txt .'</button></li>
			<li><button type="button" class="grd-filterbar-content-control" data-basedistance="fivek" data-filter=".fivek">'. $fivek_txt .'</button></li>
			<li><button type="button" class="grd-filterbar-content-control" data-basedistance="tenk" data-filter=".tenk">'. $tenk_txt .'</button></li>
			<li><button type="button" class="grd-filterbar-content-control" data-basedistance="halfmarathon" data-filter=".halfmarathon">'. $halfmarathon_txt .'</button></li>
			<li><button type="button" class="grd-filterbar-content-control" data-basedistance="marathon" data-filter=".marathon">'. $marathon_txt .'</button></li>
		</ul>';

	} else {

		$distancelist = '<ul class="grd-filterbar-content-controls grd-filterbar-content-controls-nonus">
			<li><button type="button" class="grd-filterbar-content-control" data-filter="all">'. $alldistances_txt .'</button></li>
			<li>
				<div class="grd-filterbar-content-label">'. $fivek_txt .'<span class="icon-play"></span></div>
				<ul style="display: none;">
					<li><button type="button" class="grd-filterbar-content-control" data-basedistance="fivek" data-filter=".fivek">Any Price</button></li>
					<li><button type="button" class="grd-filterbar-content-control" data-basedistance="fivek" data-filter=".fivek_30">Under $30</button></li>
					<li><button type="button" class="grd-filterbar-content-control" data-basedistance="fivek" data-filter=".fivek_50">Under $50</button></li>
					<li><button type="button" class="grd-filterbar-content-control" data-basedistance="fivek" data-filter=".fivek_more">$51 and up</button></li>
				</ul>

			</li>
			<li>
				<div class="grd-filterbar-content-label">'. $tenk_txt .'<span class="icon-play"></span></div>
				<ul style="display: none;">
					<li><button type="button" class="grd-filterbar-content-control" data-basedistance="tenk" data-filter=".tenk">Any Price</button></li>
					<li><button type="button" class="grd-filterbar-content-control" data-basedistance="tenk" data-filter=".tenk_40">Under $40</button></li>
					<li><button type="button" class="grd-filterbar-content-control" data-basedistance="tenk" data-filter=".tenk_50">Under $50</button></li>
					<li><button type="button" class="grd-filterbar-content-control" data-basedistance="tenk" data-filter=".tenk_more">$51 and up</button></li>
				</ul>
			</li>
			<li>
				<div class="grd-filterbar-content-label">'. $halfmarathon_txt .'<span class="icon-play"></span></div>
				<ul style="display: none;">
					<li><button type="button" class="grd-filterbar-content-control" data-basedistance="halfmarathon" data-filter=".halfmarathon">Any Price</button></li>
					<li><button type="button" class="grd-filterbar-content-control" data-basedistance="halfmarathon" data-filter=".halfmarathon_60">Under $60</button></li>
					<li><button type="button" class="grd-filterbar-content-control" data-basedistance="halfmarathon" data-filter=".halfmarathon_100">Under $100</button></li>
					<li><button type="button" class="grd-filterbar-content-control" data-basedistance="halfmarathon" data-filter=".halfmarathon_more">$101 and up</button></li>
				</ul>
			</li>
			<li>
				<div class="grd-filterbar-content-label">'. $marathon_txt .'<span class="icon-play"></span></div>
				<ul style="display: none;">
					<li><button type="button" class="grd-filterbar-content-control" data-basedistance="marathon" data-filter=".marathon">Any Price</button></li>
					<li><button type="button" class="grd-filterbar-content-control" data-basedistance="marathon" data-filter=".marathon_80">Under $80</button></li>
					<li><button type="button" class="grd-filterbar-content-control" data-basedistance="marathon" data-filter=".marathon_100">Under $100</button></li>
					<li><button type="button" class="grd-filterbar-content-control" data-basedistance="marathon" data-filter=".marathon_more">$101 and up</button></li>
				</ul>
			</li>
		</ul>';

	}

	return $distancelist;
}


/**
 * get seasons for filterbars
 *
 * currently (as updated above):
 * fall 2017
 * winter 2017
 * spring 2018
 * summer 2018
 * fall 2018
 *
 * @return [str]
 */
function grd_get_quarters( $qt_lang, $event_info ) {
	include get_template_directory() . '/languages.php';


	$quarterlist = '<ul class="grd-filterbar-content-controls">
		<li><button type="button" class="grd-filterbar-content-control" data-season-distance=".halfmarathon" data-filter="all">'. $alldates_txt .'</button></li>
		<li><button type="button" class="grd-filterbar-content-control" data-season-distance=".halfmarathon" data-filter=".winter17">'. $season_winter_txt .' 2017</button></li>
		<li><button type="button" class="grd-filterbar-content-control" data-season-distance=".halfmarathon" data-filter=".spring18">'. $season_spring_txt .' 2018</button></li>
		<li><button type="button" class="grd-filterbar-content-control" data-season-distance=".halfmarathon" data-filter=".summer18">'. $season_summer_txt .' 2018</button></li>
		<li><button type="button" class="grd-filterbar-content-control" data-season-distance=".halfmarathon" data-filter=".fall18">'. $season_fall_txt .' 2018</button></li>
	</ul>';

	return $quarterlist;
}


/**
 * get amenities per event
 * will pull from checkbox meta (foreach)
 *
 * @param  [obj] $amenities_obj  obj of amenities
 * @return [str]                 list of amenities
 */
function grd_get_eventamenities( $amenities_obj, $qt_lang, $event_info ) {
	include get_template_directory() . '/languages.php';

	$featureslist = '';

	$amenities_list = unserialize( $amenities_obj );

	$featureslist .= '<ul>';

		foreach( $amenities_list as $amenities_item ) {

			$featureslist .= '<li>';

				if( strpos( $amenities_item, '::' ) ) {
					$amenities_extra = explode( '::', $amenities_item );

					$featureslist .= '<div class="mix-togglecontent-content-img"><img src="'. get_bloginfo( 'stylesheet_directory' ) .'/img/grd/'. $amenities_extra[0] .'.svg"></div>
					<p class="mix-togglecontent-content-para">'. ${$amenities_extra[0]} .'<br>
					<span>'. ${$amenities_extra[1]} .'</span></p>';

				} else {

					$featureslist .= '<div class="mix-togglecontent-content-img"><img src="'. get_bloginfo( 'stylesheet_directory' ) .'/img/grd/'. $amenities_item .'.svg"></div>
					<span class="mix-togglecontent__amenities-span">'. ${$amenities_item} .'</span>';

				}

			$featureslist .= '</li>';

		}

	$featureslist .= '</ul>';

	return $featureslist;
}


/**
 * get features per event
 * @param  [type] $single_event_meta [description]
 * @param  [type] $qt_lang           [description]
 * @param  [type] $event_info_data   [description]
 * @return [type]                    [description]
 */
function grd_get_eventfeatures( $single_event_meta, $qt_lang, $event_info ) {

	include get_template_directory() . '/languages.php';

	$featureslist = '';

	$featureslist .= '<ul>';

		// check miles first
		if( array_key_exists( '_rnr3_miles', $single_event_meta ) && $single_event_meta['_rnr3_miles'] != '' ) {
			$miles_list = unserialize( $single_event_meta['_rnr3_miles'] );

			$featureslist .= '<li>
				<div class="mix-togglecontent-content-img mix-togglecontent__features-img"><img src="'. get_bloginfo( 'stylesheet_directory' ) .'/img/grd/grd_feature_miles.svg"></div>
				<p class="mix-togglecontent-content-para">'. $grd_feature_miles .'<br>';

				foreach( $miles_list as $miles_item ) {
					$featureslist .= '<span>'. ${$miles_item} .'</span><br>';
				}

				$featureslist .= '</p>
			</li>';
		}

		// check views
		if( array_key_exists( '_rnr3_views', $single_event_meta ) && $single_event_meta['_rnr3_views'] != '' ) {
			$views_list = unserialize( $single_event_meta['_rnr3_views'] );

			$featureslist .= '<li>
				<div class="mix-togglecontent-content-img mix-togglecontent__features-img"><img src="'. get_bloginfo( 'stylesheet_directory' ) .'/img/grd/grd_feature_views.svg"></div>
				<p class="mix-togglecontent-content-para">'. $grd_feature_views .'<br>';

				foreach( $views_list as $views_item ) {
					$featureslist .= '<span>'. ${$views_item} .'</span><br>';
				}

				$featureslist .= '</p>
			</li>';
		}

		// check qualifiers
		if( array_key_exists( '_rnr3_quals', $single_event_meta ) && $single_event_meta['_rnr3_quals'] != '' ) {
			$quals_list = unserialize( $single_event_meta['_rnr3_quals'] );

			$featureslist .= '<li>
				<div class="mix-togglecontent-content-img mix-togglecontent__features-img"><img src="'. get_bloginfo( 'stylesheet_directory' ) .'/img/grd/grd_feature_views_quals.svg"></div>
				<p class="mix-togglecontent-content-para">'. $grd_feature_qualifier .'<br>';

				$quals_keys	= array_keys( $quals_list );
				$last_qual	= array_pop( $quals_keys );

				foreach( $quals_list as $k => $v ) {
					$featureslist .= '<span>'. ${$v} .'</span>';

					if( $k != $last_qual ) {
						$featureslist .= '<br><br>';
					}
				}

				$featureslist .= '</p>
			</li>';
		}

		// years runnin'
		if( array_key_exists( '_rnr3_number_years', $single_event_meta ) && $single_event_meta['_rnr3_number_years'] != '' ) {

			$featureslist .= '<li>
				<div class="mix-togglecontent-content-img mix-togglecontent__features-img"><img src="'. get_bloginfo( 'stylesheet_directory' ) .'/img/grd/grd_feature_anniversary.svg"></div>
				<p class="mix-togglecontent-content-para">'. $single_event_meta['_rnr3_number_years'] .' ' . $grd_years_running .'</p>
			</li>';

		}

		// check jacket
		if( array_key_exists( '_rnr3_jacket', $single_event_meta ) && $single_event_meta['_rnr3_jacket'] == 'on' ) {

			$featureslist .= '<li>
				<div class="mix-togglecontent-content-img mix-togglecontent__features-img"><img src="'. get_bloginfo( 'stylesheet_directory' ) .'/img/grd/grd_amenity_8.svg"></div>
				<p class="mix-togglecontent-content-para">'. $grd_amenity_8 .'</p>
			</li>';

		}


	$featureslist .= '</ul>';

	return $featureslist;

}


/**
 * gets distance and price, combines them for mix class (filtering)
 */
function grd_get_distanceprice( $distances, $qt_lang ) {
	$distanceprice_classes = array();

	foreach( $distances as $individual_distance ) {

		$soldout	= array_key_exists( 'sold_out', $individual_distance ) ? $individual_distance['sold_out'] : '';
		$regclosed	= array_key_exists( 'reg_closed', $individual_distance ) ? $individual_distance['reg_closed'] : '';

		if( $soldout != 'on' && $regclosed != 'on' ) {
			$distance_pieces	= explode( '::', $individual_distance['type'] );
			$distance			= $distance_pieces[0];

			$price_lang_array	= qtrans_split( $individual_distance['price'] );
			$price_lang			= $price_lang_array[$qt_lang['lang']];

			// strip currency symbol so we can compare
			$price = preg_replace( '/^(\$|\£|\€)/', "", $price_lang );

			$price_ranges = grd_get_distanceprice_ranges();

			if ( !empty( $price_ranges[$distance] ) ) {

				foreach( $price_ranges[$distance] as $key => $values ) {

					$i = 0;

					foreach( $values as $value ) {

						if( $price < $value ) {

							$distanceprice_classes[] = $distance . '_' . $value;

						} elseif( $price > $value && $i == 0 ) {

							// skip

						} elseif( $price > $value && $i == 1 ) {

							$distanceprice_classes[] = $distance . '_more';
							break;

						}
						$i++;
					}
				}

			}
		}

	}

	return $distanceprice_classes;

}


/**
 * cleans the city name for use with classes
 * ex: chengdu, china will be "chengdu"
 *
 * @param  [str] $title  title of custom post (aka event location)
 * @return [str]         location name without commas, extra country info, spaces, or periods
 */
function grd_city_classname( $title ) {
	$cityname_class = '';

	if( strpos( get_the_title(), ',' ) === false ) {

		$cityname_class = strtolower( str_replace( array( ' ' ), '-', $title ) );
		$cityname_class = strtolower( str_replace( array( '.' ), '', $cityname_class ) );

	} else {

		$city_explode	= explode( ',', $title );
		$cityname_class	= strtolower( str_replace( array( ' ', '.' ), '-', $city_explode[0] ) );

	}


	return $cityname_class;
}


/**
 * FUNCTION: template-specific
 * display leadgen form ( surveygizz )
 */
/*function grdpromo_leadgen() {
	echo '<section id="leadgen">
		'. get_post_meta( get_the_ID(), '_rnr3_leadgen', 1 ) .'
	</section>';
}*/


/**
 * FUNCTION: template-specific
 * sets cookie timeout
 * must be set in header BEFORE any output
 * accepts phase number ( int ) to change the name of the cookie
 */
/*function set_interstitial_cookie_timeout( $phase ) {
	$cookie_name	= 'grd_interstitial_' . $phase;
	$cookie_path	= '/';
	$cookie_domain	= ( $_SERVER['HTTP_HOST'] != 'localhost' ) ? $_SERVER['HTTP_HOST'] : false;

	if( !isset( $_COOKIE[$cookie_name] ) ) {
		$successcookie	= setcookie( $cookie_name, 'set', time() + 60*60*24*2, $cookie_path, $cookie_domain, false );
	}
}*/


/**
 * FUNCTION: template-specific
 * displays interstitial
 * accepts phase number ( int ) and cookie name ( string )
 */
/*function get_interstitial( $phase, $cookiename ) {
	if( isset( $_COOKIE[$cookiename] ) && $_COOKIE[$cookiename] != '' ) {
		$qt_lang	= rnr3_get_language();

		if( $phase	== 1 ) {
			$int_img = get_post_meta( get_the_ID(), '_rnr3_interstitial1_img', 1 );
		} elseif( $phase	== 2 ) {
			$int_img = get_post_meta( get_the_ID(), '_rnr3_interstitial2_img', 1 );
		}

		$int_img = image_translation( $qt_lang, $int_img );

		echo '
			<a href="#grd_interstitial" class="interstitial_fancybox" style="display:none;" id="grd_interstitial_link"></a>
			<div id="grd_interstitial" style="display:none; padding: 0;">
				<a href="#"><img src="'. $int_img .'"></a>
			</div>
		';
	}
}*/
