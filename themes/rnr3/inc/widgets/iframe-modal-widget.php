<?php

/**
 * Adds Iframe_Modal_Widget.
 */
class Iframe_Modal_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'iframe_modal', // Base ID
			esc_html__( 'Iframe Modal', 'rrnr3' ), // Name
			array( 'description' => esc_html__( 'Adds an Iframe modal box dependent of fancybox. Code for that is in theme JS.', 'rrnr3' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {

		$qt_lang		= rnr3_get_language();
		$qt_status		= $qt_lang['enabled'] == 1 ? true : false;
		$qt_cur_lang	= $qt_lang['lang'];

		$markup			= $qt_status ? qtrans_split( $instance['markup'] )[$qt_cur_lang] : $instance['markup'];

		echo $args['before_widget'];

		if ( ! empty( $instance['link'] ) && !empty( $instance['markup'] ) ) {
			printf( '<a href="%s" class="fancybox" data-fancybox-type="iframe">%s</a>', $instance['link'], $markup );
		}

		echo $args['after_widget'];

	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		$instance		= wp_parse_args( (array) $instance, array( 'link' => '', 'markup' => '' ) );

		$link			= $instance['link'];
		$featured_check	= isset( $instance['featured_check'] ) ? (bool) $instance['featured_check'] : false;
		?>
		<p>
			<label for="<?php echo $this->get_field_id('link'); ?>"><?php _e( 'Link:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo esc_attr( $link ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'markup' ); ?>"><?php _e( 'Markup:' ); ?></label>
			<textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('markup'); ?>" name="<?php echo $this->get_field_name('markup'); ?>"><?php echo esc_textarea( $instance['markup'] ); ?></textarea>
		</p>

		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['link']			= esc_url_raw( $new_instance['link'] );

		if ( current_user_can( 'unfiltered_html' ) ) {
			$instance['markup'] = $new_instance['markup'];
		} else {
			$instance['markup'] = wp_kses_post( $new_instance['markup'] );
		}
		return $instance;
	}

} // class Foo_Widget
