<?php

/**
 * Adds Featured_Authors_Widget.
 */
class Featured_Authors_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'featured_authors', // Base ID
			esc_html__( 'Featured Authors', 'rrnr3' ), // Name
			array( 'description' => esc_html__( 'Add the Featured Authors. Checkbox under user profile needs to be checked. Does NOT display on single posts.', 'rrnr3' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {

		// Is not displayed on single posts because that's what was the set up in the design
		if ( !is_single() ) {

			$featured_check	= isset( $instance['featured_check'] ) ? (bool) $instance['featured_check'] : false;

			$author_args	= array(
				'fields'	=> 'ID',
				'orderby'	=> 'post_count',
				'order'		=> 'DESC',
				'number'	=> 9,
				'who'		=> 'authors'
			);

			if ( $featured_check ) {

				$featured_author_args	= array_merge( $author_args, array(
					'meta_query'	=> array(
						'key'		=> '_rnr3_featured_author',
						'value'		=> 'on',
						'compare'	=> '=',
						'type'		=> 'string'
					)
				) );
				// try to get the featured authors
				$author_ids	= get_users( $featured_author_args );

			}

			// if there are no featured authors, then default to 9 authors based on post_count
			if ( empty($author_ids) ) {
				$author_ids	= get_users( $author_args );
			}

			echo $args['before_widget'];

			if ( ! empty( $instance['title'] ) ) {
				echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
			}
			echo '<ul class="'. $this->option_name .'__list">';

				foreach ( $author_ids as $author_id ) {
					$post_count	= count_user_posts( $author_id );

					if ( !$post_count ) { continue; }

					$authormeta		= get_user_meta( $author_id );
					$avatarurl		= !empty( $authormeta['_rnr3_avatar'] ) ? $authormeta['_rnr3_avatar'][0] : '';
					$avatarid		= !empty( $authormeta['_rnr3_avatar_id'] ) ? $authormeta['_rnr3_avatar_id'][0] : '';
					$avatarsized	= wp_get_attachment_image_src( $avatarid, 'thumbnail' );
					$displayname	= get_the_author_meta( 'display_name', $author_id );
					$authorurl		= get_author_posts_url( $author_id );
					$gravatar		= get_avatar( $author_id, 60,'', $displayname );

					if( $avatarsized[0] ) {
						$author_avatar = '<img src="'. $avatarsized[0] .'" alt="'. $displayname .'" width="60" height="60">';
					} else {
						$author_avatar = $gravatar;
					}

					echo '<li class="'. $this->option_name .'__item"><a href="'. $authorurl .'">'. $author_avatar .'</a></li>';
				}

			echo '</ul>';

			echo $args['after_widget'];

		}

	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		$title			= !empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Featured Authors', 'rrnr3' );
		$featured_check	= isset( $instance['featured_check'] ) ? (bool) $instance['featured_check'] : false;

		// Check to see if there are any authors to begin with
		$featured_author_args	= array(
			'fields'		=> 'ID',
			'orderby'		=> 'post_count',
			'order'			=> 'DESC',
			'number'		=> 9,
			'who'			=> 'authors',
			'meta_query'	=> array(
				'key'			=> '_rnr3_featured_author',
				'value'			=> 'on',
				'compare'		=> '=',
				'type'			=> 'string'
			)
		);
		$author_ids	= get_users( $featured_author_args );
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('featured_check'); ?>" name="<?php echo $this->get_field_name('featured_check'); ?>"<?php checked( $featured_check ); ?><?php if ( empty($author_ids) ) { echo ' disabled'; } ?>/>
			<label for="<?php echo $this->get_field_id('featured_check'); ?>"><?php _e( 'Use Featured Authors? (If you can\'t check this on it\'s because no featured authors have been set)' ); ?></label>
		</p>
		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title']			= sanitize_text_field( $new_instance['title'] );
		$instance['featured_check']	= !empty($new_instance['featured_check']) ? 1 : 0;

		return $instance;
	}

} // class Foo_Widget
