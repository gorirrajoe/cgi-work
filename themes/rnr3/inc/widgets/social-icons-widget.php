<?php

/**
 * Adds Social_Icons_Widget.
 */
class Social_Icons_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'social_icons', // Base ID
			esc_html__( 'Social Icons', 'rrnr3' ), // Name
			array( 'description' => esc_html__( 'Add the optional social icons to the sidebar. Does NOT display on single posts.', 'rrnr3' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {

		// Is not displayed on single posts because that's what was the set up in the design
		if ( !is_single() ) {

			global $wpdb;

			$series_info	= $wpdb->get_row( "SELECT facebook_page FROM wp_rocknroll_events WHERE event_slug = 'series'" );

			$facebook		= isset( $series_info->facebook_page ) ? $series_info->facebook_page : false;
			$twitter		= rnr_get_option( 'rnrgv_twitter' );
			$instagram		= rnr_get_option( 'rnrgv_instagram' );
			$youtube		= rnr_get_option( 'rnrgv_youtube' );
			$pinterest		= rnr_get_option( 'rnrgv_pinterest' );

			$facebook_on	= !empty( $facebook ) && isset( $instance['facebook'] ) ? (bool) $instance['facebook'] : false;
			$twitter_on		= !empty( $twitter ) && isset( $instance['twitter'] ) ? (bool) $instance['twitter'] : false;
			$instagram_on	= !empty( $instagram ) && isset( $instance['instagram'] ) ? (bool) $instance['instagram'] : false;
			$youtube_on		= !empty( $youtube ) && isset( $instance['youtube'] ) ? (bool) $instance['youtube'] : false;
			$pinterest_on	= !empty( $pinterest ) && isset( $instance['pinterest'] ) ? (bool) $instance['pinterest'] : false;

			echo $args['before_widget'];

			if ( ! empty( $instance['title'] ) ) {
				echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
			}
			echo '<ul class="'. $this->option_name .'__list">';

				if ( $facebook_on ) {
					echo '<li class="'. $this->option_name .'__item facebook"><a href="'. $facebook .'" target="_blank"><span class="icon-facebook"></span></a></li>';
				}

				if ( $twitter_on ) {
					echo '<li class="'. $this->option_name .'__item twitter"><a href="'. rnr_get_option( 'rnrgv_twitter' ) .'" target="_blank"><span class="icon-twitter"></span></a></li>';
				}

				if ( $instagram_on ) {
					echo '<li class="'. $this->option_name .'__item instagram"><a href="'. rnr_get_option( 'rnrgv_instagram' ) .'" target="_blank"><span class="icon-instagram"></span></a></li>';
				}

				if ( $youtube_on ) {
					echo '<li class="'. $this->option_name .'__item youtube"><a href="'. rnr_get_option( 'rnrgv_youtube' ) .'" target="_blank"><span class="icon-youtube-play"></span></a></li>';
				}

				if ( $pinterest_on ) {
					echo '<li class="'. $this->option_name .'__item pinterest"><a href="'. rnr_get_option( 'rnrgv_pinterest' ) .'" target="_blank"><span class="icon-pinterest"></span></a></li>';
				}

			echo '</ul>';

			echo $args['after_widget'];

		}

	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		global $wpdb;

		$title			= !empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Stay Up To Date!', 'rrnr3' );

		$series_info	= $wpdb->get_row( "SELECT facebook_page FROM wp_rocknroll_events WHERE event_slug = 'series'" );

		$facebook_option	= $series_info->facebook_page;
		$twitter_option		= rnr_get_option( 'rnrgv_twitter' );
		$instagram_option	= rnr_get_option( 'rnrgv_instagram' );
		$youtube_option		= rnr_get_option( 'rnrgv_youtube' );
		$pinterest_option	= rnr_get_option( 'rnrgv_pinterest' );

		$facebook		= isset( $instance['facebook'] ) ? (bool) $instance['facebook'] : false;
		$facebook_on	= !empty( $facebook_option ) ? true : false;
		$twitter		= isset( $instance['twitter'] ) ? (bool) $instance['twitter'] : false;
		$twitter_on		= !empty( $twitter_option ) ? true : false;
		$instagram		= isset( $instance['instagram'] ) ? (bool) $instance['instagram'] : false;
		$instagram_on	= !empty( $instagram_option ) ? true : false;
		$youtube		= isset( $instance['youtube'] ) ? (bool) $instance['youtube'] : false;
		$youtube_on		= !empty( $youtube_option ) ? true : false;
		$pinterest		= isset( $instance['pinterest'] ) ? (bool) $instance['pinterest'] : false;
		$pinterest_on	= !empty( $pinterest_option ) ? true : false;
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p class="description">Select which social networks to show.</p>
		<p>
			<?php if ( false !== $facebook_on ) : ?>
			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('facebook'); ?>" name="<?php echo $this->get_field_name('facebook'); ?>"<?php checked( $facebook ); ?> />
			<label for="<?php echo $this->get_field_id('facebook'); ?>"><?php _e( 'Facebook' ); ?></label><br />
			<?php endif; ?>

			<?php if ( false !== $twitter_on ) : ?>
			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('twitter'); ?>" name="<?php echo $this->get_field_name('twitter'); ?>"<?php checked( $twitter ); ?> />
			<label for="<?php echo $this->get_field_id('twitter'); ?>"><?php _e( 'Twitter' ); ?></label><br />
			<?php endif; ?>

			<?php if (  false !== $instagram_on ) : ?>
			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('instagram'); ?>" name="<?php echo $this->get_field_name('instagram'); ?>"<?php checked( $instagram ); ?> />
			<label for="<?php echo $this->get_field_id('instagram'); ?>"><?php _e( 'Instagram' ); ?></label><br />
			<?php endif; ?>

			<?php if (  false !== $youtube_on ) : ?>
			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('youtube'); ?>" name="<?php echo $this->get_field_name('youtube'); ?>"<?php checked( $youtube ); ?> />
			<label for="<?php echo $this->get_field_id('youtube'); ?>"><?php _e( 'Youtube' ); ?></label><br />
			<?php endif; ?>

			<?php if (  false !== $pinterest_on ) : ?>
			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('pinterest'); ?>" name="<?php echo $this->get_field_name('pinterest'); ?>"<?php checked( $pinterest ); ?> />
			<label for="<?php echo $this->get_field_id('pinterest'); ?>"><?php _e( 'Pinterest' ); ?></label>
			<?php endif; ?>
		</p>
		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title']		= sanitize_text_field( $new_instance['title'] );
		$instance['facebook']	= !empty($new_instance['facebook']) ? 1 : 0;
		$instance['twitter']	= !empty($new_instance['twitter']) ? 1 : 0;
		$instance['instagram']	= !empty($new_instance['instagram']) ? 1 : 0;
		$instance['youtube']	= !empty($new_instance['youtube']) ? 1 : 0;
		$instance['pinterest']	= !empty($new_instance['pinterest']) ? 1 : 0;

		return $instance;
	}

} // class Foo_Widget
