<?php
/**
 * CMB2 Theme Options
 * @version 0.1.0
 * call rnr_get_option( 'XXX' ) to grab a variable
 */

class RNR_Admin {
	/**
 	 * Option key, and option page slug
 	 * @var string
 	 */
	private $key = 'rnr_options';

	/**
 	 * Options page metabox id
 	 * @var string
 	 */
	private $metabox_id = 'rnr_option_metabox';

	/**
	 * Options Page title
	 * @var string
	 */
	protected $title = '';

	/**
	 * Options ShortPage title
	 * @var string
	 */
	protected $short_title = '';


	/**
	 * Options Page hook
	 * @var string
	 */
	protected $options_page = '';

	/**
	 * Holds an instance of the object
	 *
	 * @var RNR_Admin
	 **/
	private static $instance = null;

	/**
	 * Constructor
	 * @since 0.1.0
	 */
	private function __construct() {
		// Set our title
		$this->title = __( 'Rock \'n\' Roll Global Variables Manager', 'rnr' );
		$this->short_title = __( 'RNR Global Variables Manager', 'rnr' );
	}

	/**
	 * Returns the running object
	 *
	 * @return RNR_Admin
	 **/
	public static function get_instance() {
		if( is_null( self::$instance ) ) {
			self::$instance = new RNR_Admin();
			self::$instance->hooks();
		}
		return self::$instance;
	}

	/**
	 * Initiate our hooks
	 * @since 0.1.0
	 */
	public function hooks() {
		add_action( 'admin_init', array( $this, 'init' ) );

		if( get_current_blog_id() == 1 ) {
			add_action( 'admin_menu', array( $this, 'add_options_page' ) );
		}

		add_action( 'cmb2_admin_init', array( $this, 'add_header_options_page_metabox' ) );
	}

	/**
	 * Register our setting to WP
	 * @since  0.1.0
	 */
	public function init() {
		register_setting( $this->key, $this->key );
	}

	/**
	 * Add menu options page
	 * @since 0.1.0
	 */
	public function add_options_page() {

		$this->options_page = add_menu_page( $this->title, $this->short_title, 'edit_pages', $this->key, array( $this, 'admin_page_display' ) );

		// Include CMB CSS in the head to avoid FOUC
		add_action( "admin_print_styles-{$this->options_page}", array( 'CMB2_hookup', 'enqueue_cmb_css' ) );

	}


	/**
	 * Admin page markup. Mostly handled by CMB2
	 * @since  0.1.0
	 */
	public function admin_page_display() { ?>

		<div class="wrap cmb2-options-page <?php echo $this->key; ?>">
			<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
			<?php cmb2_metabox_form( $this->metabox_id, $this->key ); ?>
		</div>

	<?php }


	function add_header_options_page_metabox() {
		// hook in our save notices
		add_action( "cmb2_save_options-page_fields_{$this->metabox_id}", array( $this, 'settings_notices' ), 10, 2 );

		$cmb = new_cmb2_box( array(
			'id'			=> $this->metabox_id,
			'hookup'		=> false,
			'cmb_styles'	=> false,
			'show_on'		=> array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( $this->key, )
			),
		) );


		/**
		 * 20 years
		 */
		$cmb->add_field( array(
		    'name' => '20 Years',
		    'type' => 'title',
		    'id'   => 'twentyyears_title'
		) );
		$cmb->add_field( array(
		    'name' => '20 Years - Enabled?',
		    'desc' => '(Series Only)',
		    'id'   => 'rnrgv_20_enabled_series',
		    'type' => 'checkbox',
		) );
		$cmb->add_field( array(
		    'name' => '20 Years - Enabled?',
		    'id'   => 'rnrgv_20_enabled',
		    'type' => 'checkbox',
		) );
		$cmb->add_field( array(
		    'name'    => '20 Year Logo',
		    'desc'    => 'Upload an image or enter an URL.',
		    'id'      => 'rnrgv_alternate_logo',
		    'type'    => 'file',
		) );
		$cmb->add_field( array(
		    'name'    => '20 Year Logo - Square',
		    'desc'    => 'Upload an image or enter an URL.',
		    'id'      => 'rnrgv_20year_logo_square',
		    'type'    => 'file',
		) );
		$cmb->add_field( array(
		    'name'    => '20 Year Logo - Events',
		    'desc'    => 'enter event slugs (separated by commas if necessary)',
		    'id'      => 'rnrgv_20year_events',
		    'type'    => 'text',
		) );
		$cmb->add_field( array(
		    'name'    => '20 Year - Exceptions',
		    'desc'    => 'enter event slugs (separated by commas if necessary)',
		    'id'      => 'rnrgv_20year_exception',
		    'type'    => 'text',
		) );

		/*
		* Tempo Options
		*/

		//show blog or old schedule
	    $cmb->add_field( array(
	      'name'    => 'Tempo Blog Active',
	      'id'    =>  'rnrgv_tempo_enabled',
	      'type'    => 'checkbox',
	      'desc'  => 'check this to show Tempo blog or leave unchecked for schedule section',
	    ) );

		/**
		 * analytics
		 */
		$cmb->add_field( array(
		    'name' => 'Analytics',
		    'type' => 'title',
		    'id'   => 'analytics_title'
		) );

		$cmb->add_field( array(
		    'name'    => 'Google Analytics',
		    'id'      => 'rnrgv_google_analytics',
		    'type'    => 'text',
		) );

		$cmb->add_field( array(
		    'name'    => 'Chartbeat',
		    'id'      => 'rnrgv_chartbeat',
		    'type'    => 'text',
		) );

		$cmb->add_field( array(
		    'name'    => 'Reinvigorate',
		    'id'      => 'rnrgv_reinvigorate',
		    'type'    => 'text',
		) );

		$cmb->add_field( array(
		    'name'    => 'Bing Search API Key',
		    'id'      => 'rnrgv_bing_search_api_key',
		    'type'    => 'text',
		) );

		$cmb->add_field( array(
		    'name'    => 'Google Tag Manager',
		    'id'      => 'rnrgv_gtm',
		    'type'    => 'textarea_code',
		) );


		/**
		 * a/b testing
		 */
		$cmb->add_field( array(
		    'name' => 'A/B Testing',
		    'type' => 'title',
		    'id'   => 'abtesting_title'
		) );

		$cmb->add_field( array(
		    'name' => 'Optimizely',
		    'type' => 'textarea_code',
		    'id'   => 'rnrgv_optimizely'
		) );

		/**
		 * social
		 */
		$cmb->add_field( array(
		    'name' => 'Social',
		    'type' => 'title',
		    'id'   => 'social_title'
		) );

		$cmb->add_field( array(
		    'name' => 'Pinterest',
		    'type' => 'text',
		    'id'   => 'rnrgv_pinterest'
		) );

		$cmb->add_field( array(
		    'name' => 'Instagram',
		    'type' => 'text',
		    'id'   => 'rnrgv_instagram'
		) );

		$cmb->add_field( array(
		    'name' => 'YouTube',
		    'type' => 'text',
		    'id'   => 'rnrgv_youtube'
		) );

		$cmb->add_field( array(
		    'name' => 'Twitter',
		    'type' => 'text',
		    'id'   => 'rnrgv_twitter'
		) );

		$cmb->add_field( array(
		    'name' => 'Po.st Publisher Key',
		    'type' => 'text',
		    'id'   => 'rnrgv_post_pubkey'
		) );

		/**
		 * style
		 */
		$cmb->add_field( array(
		    'name' => 'Style',
		    'type' => 'title',
		    'id'   => 'style_title'
		) );

		$cmb->add_field( array(
			'name'	=> __( 'Alternate Logo', 'rnr' ),
			'desc'	=> __( 'Upload the Logo. Must be 135x25 pixels. SVG is preferred. Leave empty for the default logo.', 'rnr' ),
			'id'	=> 'rnrgv_alternate_logo',
			'type'	=> 'file',
		) );

		$cmb->add_field( array(
			'name'	=> 'Header Code',
			'id'	=> 'rnrgv_header',
			'type'	=> 'textarea'
		) );

		$cmb->add_field( array(
			'name'	=> 'Footer Code',
			'id'	=> 'rnrgv_footer',
			'type'	=> 'textarea'
		) );

		/**
		 * rock n blog
		 */
		$cmb->add_field( array(
		    'name' => 'Rock N Blog',
		    'type' => 'title',
		    'id'   => 'rnb_title'
		) );

		$cmb->add_field( array(
		    'name' => 'About Title',
		    'type' => 'text',
		    'desc' => 'Enter the Title of the Rock N Blog About Us Section, if left empty the default text "About Us" will display',
		    'id'   => 'rnrgv_rnb_about'
		) );

		$cmb->add_field( array(
		    'name' => 'Become Member Title',
		    'type' => 'text',
		    'desc' => 'Enter the Title of the Rock N Become a Member Section, if left empty the default text "BECOME A MEMBER" will display',
		    'id'   => 'rnrgv_rnb_become'
		) );

		$cmb->add_field( array(
		    'name' => 'Team Title',
		    'type' => 'text',
		    'id'   => 'rnrgv_rnb_team',
		    'desc' => 'Default: "The Team"'
		) );

		$cmb->add_field( array(
		    'name' => 'Team Description',
		    'type' => 'text',
		    'id'   => 'rnrgv_rnb_description',
		    'desc' => 'Enter description text will be displayed above the team images, if left empty description text will not display.'
		) );

		$cmb->add_field( array(
		    'name' => 'View All Button',
		    'type' => 'text',
		    'id'   => 'rnrgv_rnb_viewall',
		    'desc' => 'Enter the title on the view all button. If no title is entered, the View All button will NOT display. ( Please limit characters or the button will wrap )'
		) );

		$cmb->add_field( array(
		    'name' => 'Tweets Title',
		    'type' => 'text',
		    'id'   => 'rnrgv_rnb_tweets',
		    'desc' => 'Enter the Title of the Rock N Blog Tweet Title, if left empty the default text "#ROCKNBLOG TWEETS" will display'
		) );

		$cmb->add_field( array(
		    'name' => 'FAQ Title',
		    'type' => 'text',
		    'id'   => 'rnrgv_rnb_faq',
		    'desc' => 'Enter the Title of the Rock N Blog FAQs Title, if left empty the default text "FAQs" will display'
		) );

		/**
		 * tumblr api
		 */
		$cmb->add_field( array(
		    'name' => 'Tumblr API',
		    'type' => 'title',
		    'id'   => 'tumblr_title'
		) );

		$cmb->add_field( array(
		    'name' => 'Consumer Key',
		    'type' => 'text',
		    'desc' => 'Enter Tumblr Consumer API key for liveblog',
		    'id'   => 'rnrgv_tumblr_consumer_key'
		) );

		$cmb->add_field( array(
		    'name' => 'Base Hostname',
		    'type' => 'text',
		    'desc' => 'Enter Tumblr SUBDOMAIN URL of site for liveblog. Ex: cgittest',
		    'id'   => 'rnrgv_tumblr_base_hostname'
		) );

		/**
		 * pixlee api
		 */
		$cmb->add_field( array(
		    'name' => 'Pixlee API',
		    'type' => 'title',
		    'id'   => 'pixlee_title'
		) );

		$cmb->add_field( array(
		    'name' => 'API Key',
		    'type' => 'text',
		    'desc' => 'Enter Pixlee API Key',
		    'id'   => 'rnrgv_pixlee_api_key'
		) );

		$cmb->add_field( array(
		    'name' => 'Secret Key',
		    'type' => 'text',
		    'desc' => 'Enter Pixlee Secret Key',
		    'id'   => 'rnrgv_pixlee_secret_key'
		) );

		$cmb->add_field( array(
		    'name' => 'User ID',
		    'type' => 'text',
		    'desc' => 'Enter Pixlee User ID',
		    'id'   => 'rnrgv_pixlee_userid'
		) );

		/**
		 * EU Regulations Privacy Policy
		 */
		$cmb->add_field( array(
		    'name' => 'EU Regulations Privacy Policy',
		    'type' => 'title',
		    'id'   => 'eu_title'
		) );

		$cmb->add_field( array(
			'id'				=> 'eu_regulation_txt',
			'name'				=> 'EU Regulations Text',
			'type'				=> 'textarea',
			'sanitization_cb'	=> 'stripslashes_deep',
			'attributes'		=> array(
				'rows'		=> 20,
			)
		) );

		/**
		 * grd
		 */
		$cmb->add_field( array(
		    'name' => 'Global Alert Bar',
		    'type' => 'title',
		    'id'   => 'gab_title'
		) );

		$cmb->add_field( array(
			'name' => 'Enable Global Alert Bar',
			'desc' => 'check ON if you want the sticky countdown to appear on all event sites',
			'id'   => 'rnrgv_enable_gab_countdown',
			'type' => 'checkbox',
		) );

		$cmb->add_field( array(
			'name' => 'Enable Global Alert Bar - Countdown Clock',
			'desc' => 'check ON if you want the countdown clock to appear',
			'id'   => 'rnrgv_enable_gab_enable_clock',
			'type' => 'checkbox',
		) );

		$cmb->add_field( array(
		    'name' => 'Global Alert Bar - Exceptions',
		    'type' => 'text',
		    'desc' => 'event slugs. don\'t show the countdown on these particular events (comma separated)',
		    'id'   => 'rnrgv_gab_exceptions'
		) );

		$cmb->add_field( array(
		    'name' => 'Global Alert Bar - PRE',
		    'type' => 'textarea_code',
		    'desc' => 'don\'t forget your translations!',
		    'id'   => 'rnrgv_gab_txt_pre'
		) );

		$cmb->add_field( array(
		    'name' => 'Global Alert Bar - DAY OF',
		    'type' => 'textarea_code',
		    'desc' => 'don\'t forget your translations!',
		    'id'   => 'rnrgv_gab_txt_day'
		) );

		$cmb->add_field( array(
			'name' => 'Global Alert Bar - Date',
			'id'   => 'rnrgv_gab_day',
			'type' => 'text_date_timestamp',
			'desc' => 'leave blank if you just want your custom message without a countdown'
		) );

	}


	/**
	 * Register settings notices for display
	 *
	 * @since  0.1.0
	 * @param  int   $object_id Option key
	 * @param  array $updated   Array of updated fields
	 * @return void
	 */
	public function settings_notices( $object_id, $updated ) {
		if ( $object_id !== $this->key || empty( $updated ) ) {
			return;
		}
		add_settings_error( $this->key . '-notices', '', __( 'Settings updated.', 'rnr' ), 'updated' );
		settings_errors( $this->key . '-notices' );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if (!self::$instance)
			self::$instance = new self;

		return self::$instance;
	}


	/**
	 * Public getter method for retrieving protected/private variables
	 * @since  0.1.0
	 * @param  string  $field Field to retrieve
	 * @return mixed          Field value or exception is thrown
	 */
	public function __get( $field ) {
		// Allowed fields to retrieve
		if ( in_array( $field, array( 'key', 'metabox_id', 'title', 'options_page' ), true ) ) {
			return $this->{$field};
		}
		throw new Exception( 'Invalid property: ' . $field );
	}


}


/**
 * Helper function to get/return the RNR_Admin object
 * @since  0.1.0
 * @return RNR_Admin object
 */
function rnr_admin() {
	return RNR_Admin::get_instance();
}


/**
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 * @param  string  $key Options array key
 * @return mixed        Option value
 */
function rnr_get_option( $key = '' ) {
	switch_to_blog( 1 );
		$option = cmb2_get_option( rnr_admin()->key, $key );
	restore_current_blog();

	return $option;
}


// Get it started
rnr_admin();
