<?php

	class RNR_AMP_Facebook extends AMP_Base_Embed_Handler {
		public function register_embed() {
			add_shortcode( 'facebook', array( $this, 'facebook_embed' ) );
		}

		public function unregister_embed() {
			remove_shortcode( 'facebook' );
		}

		public function get_scripts() {
			if ( ! $this->did_convert_elements ) {
				return array();
			}

			return array( 'amp-facebook' => 'https://cdn.ampproject.org/v0/amp-facebook-0.1.js' );
		}

		public function facebook_embed( $atts = array() ) {
			$a = shortcode_atts(
				array(
					'url'	=> '',
					'type'	=> 'video'
				),
				$atts
			);

			if ( empty( $a['url'] ) ) {
				return;
			}

			// If we've made it this far then we will be displaying the shortcode
			// Record this
			$this->did_convert_elements = true;

			if( $a['type'] == 'video' ) {

				return sprintf(
					'<div class="row">
						<div class="col-xs-12 video-container">
							<amp-facebook width="560" height="315" layout="responsive" data-embed-as="video" data-href="%s"></amp-facebook>
						</div>
					</div>',
					$a['url']
				);

			} elseif( $a['type'] == 'post' ) {

				return sprintf(
					'<amp-facebook width=486 height=657
						layout="responsive"
						data-href="%s">
					</amp-facebook>',
					$a['url']
				);

			}

		}
	}

?>
