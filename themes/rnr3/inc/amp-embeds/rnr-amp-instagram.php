<?php

	class RNR_AMP_Instagram extends AMP_Base_Embed_Handler {
		public function register_embed() {
			add_shortcode( 'instagram', array( $this, 'instagram_embed' ) );
		}

		public function unregister_embed() {
			remove_shortcode( 'instagram' );
		}

		public function get_scripts() {
			if ( ! $this->did_convert_elements ) {
				return array();
			}

			return array( 'amp-instagram' => 'https://cdn.ampproject.org/v0/amp-instagram-0.1.js' );
		}

		public function instagram_embed( $atts = array() ) {
			$a = shortcode_atts(
				array(
					'url' => '',
					'hidecaption' => '',
					'align' => ''
				),
				$atts
			);

			if ( empty( $a['url'] ) ) {
				return;
			}

			$align = ( $a['align'] == '' ? '' : 'align'. $a['align'] );

			// /p/{id} on both, short url and normal urls
			$instagram_id = trim( mb_substr( parse_url( $a['url'], PHP_URL_PATH ), 3 ), '/' );

			if ( empty( $instagram_id ) ) {
				return;
			}

			// If we've made it this far then we will be displaying the shortcode
			// Record this
			$this->did_convert_elements = true;

			return sprintf(
				'<div class="instagram_embed">
					<amp-instagram data-shortcode="%s" width="1" height="1" layout="responsive"></amp-instagram>
				</div>',
				$instagram_id
			);

		}
	}

?>
