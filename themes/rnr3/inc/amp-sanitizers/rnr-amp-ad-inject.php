<?php
	require_once( AMP__DIR__ . '/includes/sanitizers/class-amp-base-sanitizer.php' );
	require_once( AMP__DIR__ . '/includes/utils/class-amp-dom-utils.php' );
	require_once( AMP__DIR__ . '/includes/utils/class-amp-html-utils.php' );
	require_once( AMP__DIR__ . '/includes/class-amp-content.php' );

	class RNR_AMP_Ad_Injection_Sanitizer extends AMP_Base_Sanitizer {

		public function get_scripts() {
			if ( !$this->did_convert_elements ) {
				return array();
			}

			return array( 'amp-ad' => 'https://cdn.ampproject.org/v0/amp-ad-0.1.js' );
		}

		public function sanitize() {

			if ( class_exists('Wp_Dfp_Ads') ) {

				$adKw =  Wp_Dfp_Ads::singleton()->generate_dfp_keywords();

				$body = $this->get_body_node();

				// Build our amp-ad tag
				$ad_node = AMP_DOM_Utils::create_node( $this->dom, 'amp-ad', array(
					'width'		=> 300,
					'height'	=> 250,
					'type'		=> 'doubleclick',
					'data-slot'	=> "$adKw",
				) );

				// Add a placeholder to show while loading
				$fallback_node = AMP_DOM_Utils::create_node( $this->dom, 'amp-img', array(
					'placeholder'	=> 'Loading ad...',
					'layout'		=> 'fill',
					'src'			=> 'https://placehold.it/300x250?text=Loading',
				) );
				$ad_node->appendChild( $fallback_node );

				// Create our wrapper divs
				$advert_div		= $this->dom->createElement( 'div', '' );
				$advert_wrap	= $this->dom->createElement( 'div', '' );
				$advert_div->setAttribute( 'class', 'advert advert_xs_300x250 advert_location_inline' );
				$advert_wrap->setAttribute( 'class', 'advert__wrap' );

				$advert_wrap->appendChild( $ad_node );
				$advert_div->appendChild( $advert_wrap );

				// Only add the ad if there is more than 8 paragraphs
				// Add it at the middle of the article
				$p_nodes = $body->getElementsByTagName( 'p' );
				if ( $p_nodes->length > 8 ) {
					$this->did_convert_elements = true;

					$middle_p	= floor( $p_nodes->length / 2 );

					$p_nodes->item( $middle_p )->parentNode->insertBefore( $advert_div, $p_nodes->item( $middle_p ) );
				}

			}

		}

	}
