<?php
	/**
	 * ADMIN function: SHORTCODES
	 * youtube, vimeo, surveygizmo (3 flavors), accordions,
	 * cta (reg and big), google maps, confcard, narrow, boxed alert,
	 * green button, blue button, alert
	 */
	class RNR_Shortcodes {

		public static $instance = false;

		public function __construct() {
			$this->_add_actions();
		}


		public function gallery_shortcode_update( $attr ) {
			$post = get_post();

			static $instance = 0;
			$instance++;

			if ( ! empty( $attr['ids'] ) ) {
				// 'ids' is explicitly ordered, unless you specify otherwise.
				if ( empty( $attr['orderby'] ) ) {
					$attr['orderby'] = 'post__in';
				}
				$attr['include'] = $attr['ids'];
			}

			$output = apply_filters( 'post_gallery', '', $attr, $instance );
			if ( $output != '' ) {
				return $output;
			}

			$html5 = current_theme_supports( 'html5', 'gallery' );
			$atts = shortcode_atts( array(
				'order'			=> 'ASC',
				'orderby'		=> 'menu_order ID',
				'id'			=> $post ? $post->ID : 0,
				'itemtag'		=> $html5 ? 'figure'     : 'dl',
				'icontag'		=> $html5 ? 'div'        : 'dt',
				'captiontag'	=> $html5 ? 'figcaption' : 'dd',
				'columns'		=> 1,
				'size'			=> 'distance-gallery',
				'include'		=> '',
				'exclude'		=> '',
				'link'			=> '',
				'autoplay'		=> ''
			), $attr, 'gallery' );

			$id = intval( $atts['id'] );

			if ( ! empty( $atts['include'] ) ) {
				$_attachments = get_posts( array( 'include' => $atts['include'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );

				$attachments = array();
				foreach ( $_attachments as $key => $val ) {
					$attachments[$val->ID] = $_attachments[$key];
				}
			} elseif ( ! empty( $atts['exclude'] ) ) {
				$attachments = get_children( array( 'post_parent' => $id, 'exclude' => $atts['exclude'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
			} else {
				$attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
			}

			if ( empty( $attachments ) ) {
				return '';
			}

			if ( is_feed() ) {
				$output = "\n";
				foreach ( $attachments as $att_id => $attachment ) {
					$output .= wp_get_attachment_link( $att_id, $atts['size'], true ) . "\n";
				}
				return $output;
			}

			$itemtag	= tag_escape( $atts['itemtag'] );
			$captiontag	= tag_escape( $atts['captiontag'] );
			$icontag	= tag_escape( $atts['icontag'] );
			$valid_tags	= wp_kses_allowed_html( 'post' );

			if ( ! isset( $valid_tags[ $itemtag ] ) ) {
				$itemtag = 'dl';
			}
			if ( ! isset( $valid_tags[ $captiontag ] ) ) {
				$captiontag = 'dd';
			}
			if ( ! isset( $valid_tags[ $icontag ] ) ) {
				$icontag = 'dt';
			}

			$columns		= intval( $atts['columns'] );
			$itemwidth		= $columns > 0 ? floor(100/$columns) : 100;
			$float			= is_rtl() ? 'right' : 'left';

			$selector		= "gallery-{$instance}";

			$gallery_style	= '';

			$size_class		= sanitize_html_class( $atts['size'] );

			if ( function_exists( 'is_amp_endpoint' ) && is_amp_endpoint() ) {

				add_action( 'amp_post_template_head', function() {
					echo '<script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>';
				});

				// size we want to use in AMP gallery
				$size	= 'medium';
				$output = '<amp-carousel width="400" height="300" layout="responsive" type="slides">';

					foreach ( $attachments as $id => $attachment ) {

						$image_meta  = wp_get_attachment_metadata( $id );

						$width = $height = '';
						if ( !empty( $image_meta['sizes'][$size]['width'] ) ) {
							$width	= $image_meta['sizes'][$size]['width'];
						} elseif ( !empty( $image_meta['width'] ) ) {
							$width	= $image_meta['width'];
						}
						if ( !empty( $image_meta['sizes'][$size]['height'] ) ) {
							$height	= $image_meta['sizes'][$size]['height'];
						} elseif ( !empty( $image_meta['height'] ) ) {
							$height	= $image_meta['height'];
						}

						$excerpt = trim( $attachment->post_excerpt );
						$excerpt = ( $excerpt ? wptexturize($excerpt) : '' );

						$output .= sprintf(
							'<amp-img src="%s" height="%s" width="%s">%s</amp-img>',
							wp_get_attachment_image_src( $attachment->ID, $size )[0],
							$height,
							$width,
							$excerpt
						);

					}

				$output .= '</amp-carousel>';

			} else {

				if( $atts['autoplay'] == '' ) {
					$autoplay_class = ' owl-singleposts';
				} else {
					$autoplay_class = ' owl-autoplay';
				}

				$gallery_div = "<div class='owl-singlepost-controls' id='$selector-nav'>
					<div class='owl-buttons'>
						<a class='owl-prev'>prev</a>
						<span class='current-gallery-item'></span> <span>of</span> <span class='total-gallery-items'></span>
						<a class='owl-next'>next</a>
					</div>
				</div>

				<div id='$selector' class='gallery galleryid-{$id}" .$autoplay_class. "'>";

					$output = apply_filters( 'gallery_style', $gallery_style . $gallery_div );

					$i = 0;
					foreach ( $attachments as $id => $attachment ) {

						// $attr = ( trim( $attachment->post_excerpt ) ) ? array( 'aria-describedby' => "$selector-$id" ) : '';
						$attr			= array( 'class' => 'lazyOwl' );
						$image_output	= wp_get_attachment_link( $id, $atts['size'], false, false, false, $attr );
						$image_output	= str_replace( 'href', 'data-fancybox="'. $selector .'" data-caption="'. wptexturize($attachment->post_excerpt) .'" href', $image_output );
						$image_output	= str_replace( 'src', 'data-src', $image_output );
						$image_meta		= wp_get_attachment_metadata( $id );

						$orientation = '';
						if ( isset( $image_meta['height'], $image_meta['width'] ) ) {
							$orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';
						}

						$output .= "<{$itemtag} class='gallery-item'>";

							$output .= "<{$icontag} class='gallery-icon {$orientation}'>
								$image_output
							</{$icontag}>";

							if ( $captiontag && trim($attachment->post_excerpt) ) {
								$output .= "<{$captiontag} class='wp-caption-text gallery-caption' id='$selector-$id'>
									" . wptexturize($attachment->post_excerpt) . "
								</{$captiontag}>";
							}

						$output .= "</{$itemtag}>";

						if ( ! $html5 && $columns > 0 && ++$i % $columns == 0 ) {
							$output .= '<br style="clear: both" />';
						}
					}

					if ( ! $html5 && $columns > 0 && $i % $columns !== 0 ) {
						$output .= "<br style='clear: both' />";
					}

				$output .= "</div>\n";

			}

			return $output;
		}


		public function youtube_embed( $atts ) {
			$a = shortcode_atts( array(
				'id' => ''
			), $atts );
			return '
				<div class="video-container">
					<iframe width="1280" height="720" src="https://www.youtube.com/embed/'.$a['id'].'?rel=0" frameborder="0" allowfullscreen></iframe>
				</div>
			';
		}


		public function brightcovex_embed( $atts ) {
			$a = shortcode_atts( array(
				'id' => ''
			), $atts );
			return '
				<!-- Start of Brightcove Player -->
				<div style="display:none"></div><script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>
				<div class="video-container">
					<object id="myExperience'.$a['id'].'" class="BrightcoveExperience">
						<param name="bgcolor" value="#FFFFFF" />
						<param name="width" value="480" />
						<param name="height" value="270" />
						<param name="playerID" value="4462592489001" />
						<param name="playerKey" value="AQ~~,AAADUxzyV0k~,28cyIIHd3stexCscuQbxoAsNRyfsyXOP" />
						<param name="isVid" value="true" />
						<param name="isUI" value="true" />
						<param name="dynamicStreaming" value="true" />
						<param name="@videoPlayer" value="'.$a['id'].'" />
					</object>
				</div><script type="text/javascript">brightcove.createExperiences();</script>
				<!-- End of Brightcove Player -->
			';
		}


		public function brightcove_embed( $atts ) {
			$a = shortcode_atts( array(
				'id' => ''
			), $atts );

			if ( empty( $a['id'] ) ) {
				return;
			}

			ob_start(); ?>

				<div class="video-container">
					<div style="display: block; position: relative; max-width: 100%;">
						<div style="padding-top: 56.25%;">
							<video id="myVideo"
								data-video-id="<?php echo trim( $a['id'] ); ?>"
								data-account="3655502813001"
								data-player="r1oC9M1S"
								data-embed="default"
								data-application-id
								class="video-js"
								controls style="width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;">
							</video>
						</div>
					</div>

					<script src="//players.brightcove.net/3655502813001/r1oC9M1S_default/index.min.js"></script>
				</div>
			<?php

			return ob_get_clean();
		}

		public function vimeo_embed( $atts ) {
			$a = shortcode_atts( array(
				'id' => ''
			), $atts );
			return '
				<div class="video-container">
					<iframe src="https://player.vimeo.com/video/'.$a['id'].'" width="1280" height="719" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				</div>
			';
		}

		public function surveygizmo_embed( $atts ) {
			$a = shortcode_atts( array(
				'code' => ''
			), $atts );
			return '
				<script type="text/javascript">// <![CDATA[
				document.write(\'<script src="http\' + ( ("https:" == document.location.protocol) ? "s" : "") + \'://'.$a['code'].'?__ref=\' + escape(document.location) + \'" type="text/javascript" ></scr\'  + \'ipt>\');
				// ]]></script>
			';
		}


		public function surveygizmo_embed_new( $atts ) {
			$a = shortcode_atts( array(
				'code' => ''
			), $atts );
			return '
				<script type="text/javascript" >document.write(\'<script src="http\' + ( ("https:" == document.location.protocol) ? "s" : "") + \'://'.$a['code'].'?__output=embedjs&__ref=\' + escape(document.location.origin + document.location.pathname) + \'" type="text/javascript" ></scr\'  + \'ipt>\');</script>
			';
		}


		public function surveygizmo_iframe_embed( $atts ) {
			$a = shortcode_atts( array(
				'url'     => '',
				'width'   => '',
				'height'  => ''
			), $atts );
			return '
				<iframe src="'.$a['url'].'" frameborder="0" width="'.$a['width'].'" height="'.$a['height'].'" style="overflow:hidden"></iframe>
			';
		}


		// begin old accordion method
		public function accordion_start( $atts ) {
			return '<div class="accordion">';
		}


		public function accordion_content( $atts ) {
			$a = shortcode_atts( array(
				'panel' => '',
				'title' => '',
				'content' => ''
			), $atts );
			return '<div class="accordion-section custom" style="position: relative;">
					<div class="accordion-section-title" data-reveal="#panel'.$a['panel'].'">'.$a['title'].'</div>
					<div class="accordion-section-content" id="panel'.$a['panel'].'">
						'.$a['content'].'
					</div>
				</div>';
		}


		public function accordion_end( $atts ) {
			return '</div>';
		}
		// end old accordion method


		public function accordion_wrapper( $atts, $content = null ) {
			return '<div class="accordion">
				'. do_shortcode( $content ) .'
			</div>';
		}


		public function accordion_single( $atts, $content = null ) {
			STATIC $i = 0;
			$i++;
			$a = shortcode_atts( array(
				'title' => '',
			), $atts );
			return '<div class="accordion-section custom" style="position: relative;">
				<div class="accordion-section-title" data-reveal="#panel'.$i.'">'.$a['title'].'</div>
				<div class="accordion-section-content" id="panel'.$i.'">
					'. do_shortcode( $content ) .'
				</div>
			</div>';
		}


		public function cta_shortcode( $atts ) {
			$a = shortcode_atts( array(
				'url'   => '',
				'text'  => '',
				'blank' => 0
			), $atts );
			if( $a['blank'] == 1 ) {
				$blank = 'target="_blank"';
			} else {
				$blank = '';
			}
			return '<a class="cta cta_shortcode" href="'.$a['url'].'" '.$blank.'>'.$a['text'].'</a>';
		}


		public function ctabig_shortcode( $atts ) {
			$a = shortcode_atts( array(
				'url'   => '',
				'text'  => '',
				'blank' => 0
			), $atts );
			if( $a['blank'] == 1 ) {
				$blank = 'target="_blank"';
			} else {
				$blank = '';
			}
			return '<a class="cta cta_big cta_shortcode" href="'.$a['url'].'" '.$blank.'>'.$a['text'].'</a>';
		}


		public function googlemap_shortcode( $atts ) {
			$a = shortcode_atts( array(
				'url'   => ''
			), $atts );
			return '<iframe width="100%" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="'.$a['url'].'&output=embed"></iframe>
			<p><small><a href="'.$a['url'].'" target="_blank">View in a larger map</a></small></p>';
		}


		public function confcard_shortcode( $atts ) {
			$a = shortcode_atts( array(
				'slug'    => '',
				'height'  => ''
			), $atts );
			return '<iframe src="http://confirmation.competitor.com/'.$a['slug'].'" frameborder="0" width="100%" height="'.$a['height'].'" ></iframe>';

		}


		public function narrow_shortcode( $atts, $content = null ) {
			return '<div class="narrow">'.do_shortcode( $content ).'</div>';
		}


		public function alert_shortcode( $atts, $content = null ) {
			return '<div class="attn_alert">'.do_shortcode( $content ).'</div>';
		}


		public function nobox_alert_shortcode( $atts, $content = null ) {
			return '<div class="alert">'.do_shortcode( $content ).'</div>';
		}


		public function green_btn_shortcode( $atts ) {
			$a = shortcode_atts( array(
				'url'   => '',
				'text'  => '',
				'blank' => 0
			), $atts );
			if( $a['blank'] == 1 ) {
				$blank = 'target="_blank"';
			} else {
				$blank = '';
			}
			return '<a class="highlight green" href="'.$a['url'].'" '.$blank.'>'.$a['text'].'</a>';
		}


		public function blue_btn_shortcode( $atts ) {
			$a = shortcode_atts( array(
				'url'   => '',
				'text'  => '',
				'blank' => 0
			), $atts );
			if( $a['blank'] == 1 ) {
				$blank = 'target="_blank"';
			} else {
				$blank = '';
			}
			return '<a class="highlight blue" href="'.$a['url'].'" '.$blank.'>'.$a['text'].'</a>';
		}


		public function color_btn_shortcode( $atts ) {
			$a = shortcode_atts( array(
				'url'   => '',
				'text'  => '',
				'blank' => 0,
				'color' => '',
				'class' => ''

			), $atts );
			if( $a['blank'] == 1 ) {
				$blank = 'target="_blank"';
			} else {
				$blank = '';
			}
			return '<a class="'. $a['class'] . ' highlight '. $a['color'] .'" href="'.$a['url'].'" '.$blank.'>'.$a['text'].'</a>';
		}


		// show_mobile: [mobile] will only show content on mobile < 960px
		public function show_mobile( $atts, $content = null ) {
			return '<div class="showmobile">'.do_shortcode( $content ).'</div>';
		}


		// show_desktop: [desktop] will only show content on desktop > 960px
		public function show_desktop( $atts, $content = null ) {
			return '<div class="showdesktop">'.do_shortcode( $content ).'</div>';
		}


		// grid/columns
		public function grid_container( $atts, $content = null ) {
			$a = shortcode_atts( array(
				'count' => ''
			), $atts );
			return '<div class="grid_'. $a['count'] .'_special">
				'. do_shortcode( $content ) .'
			</div>';
		}


		public function grid_column( $atts, $content = null ) {
			return '<div class="column_special">'. do_shortcode( $content ) .'</div>';
		}


		public function regional_youtube_embed( $atts, $content = null ) {
			$a = shortcode_atts( array(
				'id' => ''
			), $atts );
			return '
				<div class="grid_2_special">
					<div class="column_special">
						<div class="video-container">
							<iframe width="1280" height="720" src="https://www.youtube.com/embed/'.$a['id'].'?rel=0" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
					<div class="column_special video_desc mid_prevent_mobile">
						<div class="mid">
							'. apply_filters( 'the_content', $content ) .'
						</div>
					</div>
				</div>
			';
		}


		// testimonials
		public function testimonial_wrapper( $atts, $content = null ) {
			$a = shortcode_atts( array(
				'nav' => '0',
			), $atts );

			$reg_divid = $a['nav'] != '0' ? 'reg_testimonial_navi' : 'reg_testimonial';

			return '<div id="'. $reg_divid .'" class="owl-carousel">
				'. do_shortcode( $content ) .'
			</div>';
		}


		public function testimonial_single( $atts, $content = null ) {
			$a = shortcode_atts( array(
				'person' => '',
			), $atts );
			return '<div>
				<p class="quote"><span class="icon-quote-left"></span> '. do_shortcode( $content ) . '</p>
				<p class="attrib">&ndash; ' . $a['person'] .'</p>
			</div>';
		}


		// button group
		public function button_group( $atts, $content = null ) {
			$a = shortcode_atts( array(
				'align' => '',
			), $atts );
			if( $a['align'] == '' ) {
				$align = 'left';
			} else {
				$align = $a['align'];
			}
			return '<div class="button_group button_group_'. $align .'">
				<ul>
					'. do_shortcode( $content ) .'
				</ul>
			</div>';
		}


		public function button_single( $atts, $content = null ) {
			$a = shortcode_atts( array(
				'url'	=> '',
				'color'	=> '',
				'blank'	=> '',
				'text'	=> '',
				'class'	=> ''
			), $atts );

			if( $a['blank'] == 1 ) {
				$blank = 'target="_blank"';
			} else {
				$blank = '';
			}

			if( $a['color'] != '' ) {
				$btn_color = $a['color'];
			} else {
				$btn_color = 'gray';
			}
			return '<li><a href="'. $a['url'] .'" '.$blank.' class="'. $a['class'] .' button_single highlight '. $btn_color .'">'. $a['text'] .'</a></li>';
		}


		public function instagram_embed( $atts, $content = null ) {
			$a = shortcode_atts( array(
				'url'         => '',
				'hidecaption' => '',
				'align'       => ''
			), $atts );
			if( $a['hidecaption'] == 'true' ) {
				$hidecaption = '&hidecaption=true';
			} else {
				$hidecaption = '';
			}
			if( $a['align'] == '' ) {
				$align = '';
			} else {
				$align = 'align' . $a['align'];
			}
			$ig_json  = file_get_contents( 'https://api.instagram.com/oembed?url=' . $a['url'] . $hidecaption );
			$ig_obj   = json_decode( $ig_json );
			return '<div class="instagram_embed '. $align.'">'. $ig_obj->html .'</div>';
		}


		public function twitter_embed( $atts ) {
			$a = shortcode_atts( array(
				'url'   => '',
				'align' => ''
			), $atts );
			if( $a['align'] == '' ) {
				$align = '';
				$divalign = '';
			} else {
				$align = '&align=' . $a['align'];
				$divalign = ' twitter_' . $a['align'];
			}
			$tw_json  = file_get_contents( 'https://api.twitter.com/1/statuses/oembed.json?url=' . $a['url'] . $align );
			$tw_obj   = json_decode( $tw_json );
			return '<div class="twitter_embed'. $divalign .'">'. $tw_obj->html .'</div>';
		}


		public function anchor_group( $atts ) {
			$a = shortcode_atts( array(
				'id' => ''
			), $atts );
			return '<span id="'. $a['id'] .'"></span>';
		}


		public function pixlee_embed( $atts ) {
			$a = shortcode_atts( array(
				'albumid'			=> '',
				'recipeid'			=> '',
				'displayoptionsid'	=> '',
				'type'				=> '',
				'widgetid'			=> ''
			), $atts );

			if( $a['widgetid'] != '' ) {
				// new method
				return '<div id="pixlee_container"></div><script type="text/javascript">window.PixleeAsyncInit = function() {Pixlee.init({apiKey:"WQoUPrRWZqiM92HGW5X"});Pixlee.addSimpleWidget({widgetId:'. $a['widgetid'] .'});};</script><script src="//assets.pixlee.com/assets/pixlee_widget_1_0_0.js"></script>';
			} else {
				// old method
				return '<div id="pixlee_container"></div><script type="text/javascript">window.PixleeAsyncInit = function() {Pixlee.init({apiKey:"WQoUPrRWZqiM92HGW5X"});Pixlee.addSimpleWidget({albumId:'. $a['albumid'] .',recipeId:'. $a['recipeid'] .',displayOptionsId:'. $a['displayoptionsid'] .',type:"'. $a['type'] .'",accountId:501});};</script><script src="//assets.pixlee.com/assets/pixlee_widget_1_0_0.js"></script>';
			}
		}


		public function infographic_embed( $atts ) {
			$a = shortcode_atts( array(
				'link_back' => '',
				'source'    => '',
				'alt'       => ''
			), $atts );
			return '<div style="margin:0 auto; text-align:center; clear:both;" id="info-embed"><p style="font-family: Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold;">Post this on your site (Embed Code):</p><input type="text" style="overflow:hidden; width:85%;" onclick="javascript:this.focus();this.select();" value="&lt;a href=&quot;'. $a['link_back'] .'&quot;&gt;&lt;img src=&quot;'. $a['source'] .'&quot; alt=&quot;'. $a['alt'] .'&quot; width=&quot;800&quot; style=&quot;max-width:95%; width:800px&quot; border=&quot;0&quot; /&gt;&lt;/a&gt;" /></div>';
		}


		public function issuu_embed( $atts ) {
			$a = shortcode_atts( array(
				'id' => ''
			), $atts );
			return '
				<div class="issuuembed" style="width: 600px; height: 388px; max-width: 100%;" data-configid="'.$a['id'].'"></div>
				<script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>
			';
		}


		public function fb_live_video( $atts ) {
			/*$a = shortcode_atts( array(
				'id'   => '',
			), $atts );

			return '<div class="video-container">
				<iframe src="https://www.facebook.com/video/embed?video_id='. $a['id'] .'" width="400" height="400" frameborder="0"></iframe>
			</div>';*/

			$a = shortcode_atts( array(
				'url' => ''
			), $atts );

			return '
				<!-- Load Facebook SDK for JavaScript -->
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, "script", "facebook-jssdk"));</script>

				<!-- Your embedded video player code -->
				<div class="video-container">
					<div class="fb-video" data-href="'. $a['url'] .'" data-width="960" data-show-text="false"></div>
				</div>
			';
		}

		public function hiddenimage_embed( $atts ) {

			global $post;

			$excerpt	= get_the_excerpt( $post->ID  );

			$a = shortcode_atts( array(
				'url'	=> '',
				'alt'	=> $excerpt
			), $atts );

			return '<div style="display:none;"><img src ="'. $a['url'] .'" alt="'. $a['alt'] .'"></div>';
		}

		public function smalltxt_shortcode( $atts, $content = null ) {
			$a = shortcode_atts( array(
				'align' => ''
			), $atts );

			$align = $a['align'] != '' ? $a['align'] : 'left';

			return '<p style="text-align: '. $align .'"><small>
				'. do_shortcode( $content ) .'
			</small></p>';
		}

		/**
		 * Related Articles Embed
		 *
		 * Adds a set of related articles to the current post.
		 *
		 * Example:
		 * [related title="Sample Title" tag="Sample Tag" align="{left|center|right}"]
		 *
		 * @param array $atts User defined attributes in shortcode tag.
		 */
		public function related_articles_embed( $atts = array(), $content = null ) {

			$post_ID	= get_the_ID();

			/**
			 * static array to keep track of what posts have been used,
			 * in case the shortcode is used multiple times on the post/page,
			 * which is a likely scenario since the bottom of the posts uses a "related articles" area
			 */
			static $used_posts	= array();

			// push the post ID into the used posts
			array_push( $used_posts, $post_ID );

			// Include language posts
			$qt_lang = rnr3_get_language();
			include get_template_directory() .'/languages.php';

			$post_tags	= get_the_tags( $post_ID );
			$tags_array	= array();
			$tag_string	= '';

			if ( !empty( $post_tags ) ) {

				foreach( $post_tags as $tag ) {
					$tags_array[]	.= $tag->slug;
				}

				$tag_string = implode( ',', $tags_array );
			}

			$a = shortcode_atts( array(
				'title'		=> 'Read More',
				'tag'		=> $tag_string,
				'align'		=> 'none',
				'post-ids'	=> '',
				'style'		=> 0
			), $atts );

			$align = ( !empty( $a['align'] ) ? 'align'. $a['align'] : '' );

			if ( !empty( $a['tag'] ) ) {

				$widget = '';

				$args = array(
					'tag'					=> $a['tag'],
					'posts_per_page'		=> 3,
					'post__not_in'			=> $used_posts,
					'ignore_sticky_posts'	=> true
				);

				// use IDs instead of by tag if defined
				if ( !empty( $a['post-ids'] ) ) {

					unset( $args['tag'] );
					unset( $args['post__not_in'] );

					$args['post__in'] = explode( ',', $a['post-ids'] );
				}

				$related_query = new WP_Query( $args );

				// Save our data into an array we can loop over several times instead of making all the calls repeatedly
				$posts_array	= array();

				if ( $related_query->have_posts() ): while( $related_query->have_posts() ): $related_query->the_post();

					// save the post ID into $used_posts
					array_push( $used_posts, $related_query->post->ID );

					// so we can keep adding to array
					$loop_count	= $related_query->current_post;

					// get the metadata
					$post_metadata	= get_post_meta( $related_query->post->ID );

					$posts_array[$loop_count]['title']		= get_the_title();
					$posts_array[$loop_count]['permalink']	= get_permalink();
					$posts_array[$loop_count]['excerpt']	= get_the_excerpt();

					// replace the title with the short one if it exists
					if ( !empty( $post_metadata['short_title'] ) ) {
						$posts_array[$loop_count]['title']	= strip_tags( $post_metadata['short_title'][0] );
					}

					if ( has_post_thumbnail() ) {

						$image = '<figure class="article__thumbnail">';
							$image .= get_the_post_thumbnail( null, 'thumbnail' );
						$image .= '</figure>';

					} else {

						// if there is no featured image, use default in theme settings
						global $wpdb;
						$default_image_url	= get_option( 'default-featured-image' );
						$image_data			= $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE guid='%s';", $default_image_url ) );
						$default_image		= wp_get_attachment_image( $image_data[0], 'thumbnail' );

						$image = '<figure class="article__thumbnail">';
							$image .= $default_image;
						$image .= '</figure>';

					}

					$posts_array[$loop_count]['image'] = $image;

					// set up the author into the array
					$posts_array[$loop_count]['author']	= !empty( $post_metadata['_guest_author'] ) ? $post_metadata['_guest_author'][0] : get_the_author();

				endwhile; endif;

				wp_reset_postdata();

				// Make sure the array of posts is not empty
				if ( !empty( $posts_array ) ) {

					// Original widget (which I still prefer, styling wise)
					$style	= $a['style'] == 0 ? '' : ' hide';
					$widget = sprintf( '<aside class="recommended-widget alt-0 %s%s">', $align, $style );

						$widget .= sprintf( '<h2 class="recommended-widget__title"><span>%s</span></h2>', $a['title'] );

						$widget .= '<ul class="recommended-widget__menu">';

							foreach ( $posts_array as $post ) {

								$widget .= sprintf( '<li class="recommended-widget__menu-item"><a href="%s">%s</a></li>', $post['permalink'], $post['title'] );

							}

						$widget .= '</ul>';

					$widget .= '</aside>';

					$widget = sprintf( '<section class="article__related more-stories %s">', $style );

						$widget .= sprintf( '<h3>%s</h3>', $a['title'] );

						foreach ( $posts_array as $post ) :

							$widget .= '<article class="article article_type_related">';

								$widget .= sprintf( '<a href="%s" class="article__permalink">', $post['permalink'] );

									$widget .= $post['image'];

									$widget .= '<div class="article__details">';
										$widget .= sprintf( '<h3 class="article__title">%s</h3>', $post['title'] );
										$widget .= sprintf( '<div class="article__meta"><span class="author">%s %s</span></div>', $by_txt, $post['author'] );
									$widget .= '</div>';

								$widget .= '</a>';

							$widget .= '</article>';

						endforeach;

					$widget .= '</section>';

					// Widget alt version 1 for A/B (AND EVEN C whoaaaaa) Testing
					$style	= $a['style'] == 1 ? '' : ' hide';
					$widget .= sprintf( '<aside class="recommended-widget alt-1 %s%s">', $align, $style );

						$widget .= sprintf( '<h2 class="recommended-widget__title"><span>%s</span></h2>', $a['title'] );

						$widget .= '<ul class="recommended-widget__menu">';

							foreach ( $posts_array as $post ) {

								$widget .= sprintf( '<li class="recommended-widget__menu-item"><a href="%s">%s</a></li>', $post['permalink'], $post['title'] );

							}

						$widget .= '</ul>';

					$widget .= '</aside>';

					// Widget alt version 2 OR B? C?
					$style	= $a['style'] == 2 ? '' : ' hide';
					$widget .= sprintf( '<aside class="recommended-widget alt-2 %s%s">', $align, $style );

					$widget .= sprintf( '<h2 class="recommended-widget__title"><span>%s</span></h2>', $a['title'] );

						foreach ( $posts_array as $post ) {

							$widget .='<article class="article article_type_latest">';

								$widget .= sprintf( '<a href="%s" class="article__permalink">', $post['permalink'] );

									$widget .= function_exists( 'is_amp_endpoint' ) && is_amp_endpoint() ? preg_replace( '/<img/', '<amp-img layout="fixed"', $post['image'] ) : $post['image'];

									$widget .= '<div class="article__details">';
										$widget .= sprintf( '<h2 class="article__title">%s</h2>', $post['title'] );
										$widget .= sprintf( '<p class="article__excerpt">%s</p>', $post['excerpt'] );
									$widget .= '</div>';

								$widget .= '</a>';

							$widget .= '</article>';

							break; // we only need the first post so we break out ASAP

						}

					$widget .= '</aside>';

				}

				return $widget;
			}

			return $content;

		}

		/**
		 * Marketo Embed
		 *
		 * Adds a Marketo form (either embed or lightbox)
		 *
 		 * @param array $atts User defined attributes in shortcode tag.
 		 * 'id' (required) - form id
 		 * 'munchkin_id' - for tracking (marketo) -- defaults to "124-QVG-738"
 		 * 'lightbox' (optional) - set "1" for true
		 */
		public function marketo_embed( $atts ) {
			$a = shortcode_atts( array(
				'id'		=> '',
				'munchkin_id'	=> '124-QVG-738',
				'lightbox'		=> '0'
			), $atts );

			if( $a['id'] == '' ) return;

			// lightbox option
			if( $a['lightbox'] == 1 ) {

				return '<script src="//app-sj15.marketo.com/js/forms2/js/forms2.min.js"></script>
				<form id="mktoForm_'. $a['id'] .'"></form>
				<script>MktoForms2.loadForm("//app-sj15.marketo.com", "'. $a['munchkin_id'] .'", '. $a['id'] .', function (form){MktoForms2.lightbox(form).show();});</script>';

			} else {

				return '<script src="//app-sj15.marketo.com/js/forms2/js/forms2.min.js"></script>
				<form id="mktoForm_'. $a['id'] .'"></form>
				<script>MktoForms2.loadForm("//app-sj15.marketo.com", "'. $a['munchkin_id'] .'", '. $a['id'] .');</script>';

			}
		}

		/**
		 * Singleton
		 *
		 * Returns a single instance of the current class.
		 */
		public static function singleton() {

			if ( ! self::$instance )
				self::$instance = new self();

			return self::$instance;
		}


		/**
		 * Add Actions
		 *
		 * Defines all the WordPress actions and filters used by this class.
		 */
		protected function _add_actions() {

			add_shortcode( 'gallery', array( $this, 'gallery_shortcode_update' ) );
			add_shortcode( 'red', array( $this, 'nobox_alert_shortcode' ) );
			add_shortcode( 'green-button', array( $this, 'green_btn_shortcode' ) );
			add_shortcode( 'youtube', array( $this, 'youtube_embed' ) );
			add_shortcode( 'brightcovex', array( $this, 'brightcove_embed' ) );
			add_shortcode( 'brightcove', array( $this, 'brightcove_embed' ) );
			add_shortcode( 'vimeo', array( $this, 'vimeo_embed' ) );
			add_shortcode( 'surveygizmo', array( $this, 'surveygizmo_embed' ) );
			add_shortcode( 'sg', array( $this, 'surveygizmo_embed_new' ) );
			add_shortcode( 'surveygizmo-iframe', array( $this, 'surveygizmo_iframe_embed' ) );
			add_shortcode( 'accordion-start', array( $this, 'accordion_start' ) );
			add_shortcode( 'accordion-content', array( $this, 'accordion_content' ) );
			add_shortcode( 'accordion-end', array( $this, 'accordion_end' ) );
			add_shortcode( 'accordion', array( $this, 'accordion_wrapper' ) );
			add_shortcode( 'acc-single', array( $this, 'accordion_single' ) );
			add_shortcode( 'cta', array( $this, 'cta_shortcode' ) );
			add_shortcode( 'cta-big', array( $this, 'ctabig_shortcode' ) );
			add_shortcode( 'google-map', array( $this, 'googlemap_shortcode' ) );
			add_shortcode( 'confcard', array( $this, 'confcard_shortcode' ) );
			add_shortcode( 'narrow', array( $this, 'narrow_shortcode' ) );
			add_shortcode( 'alert', array( $this, 'alert_shortcode' ) );
			add_shortcode( 'blue-button', array( $this, 'blue_btn_shortcode' ) );
			add_shortcode( 'color-button', array( $this, 'color_btn_shortcode' ) );
			add_shortcode( 'mobile', array( $this, 'show_mobile' ) );
			add_shortcode( 'desktop', array( $this, 'show_desktop' ) );
			add_shortcode( 'grid', array( $this, 'grid_container' ) );
			add_shortcode( 'column', array( $this, 'grid_column' ) );
			add_shortcode( 'youtube-text', array( $this, 'regional_youtube_embed' ) );
			add_shortcode( 'testimonial-group', array( $this, 'testimonial_wrapper' ) );
			add_shortcode( 'quote', array( $this, 'testimonial_single' ) );
			add_shortcode( 'button-group', array( $this, 'button_group' ) );
			add_shortcode( 'button', array( $this, 'button_single' ) );
			add_shortcode( 'instagram', array( $this, 'instagram_embed' ) );
			add_shortcode( 'twitter', array( $this, 'twitter_embed' ) );
			add_shortcode( 'anchor', array( $this, 'anchor_group' ) );
			add_shortcode( 'pixlee', array( $this, 'pixlee_embed' ) );
			add_shortcode( 'issuu', array( $this, 'issuu_embed' ) );
			add_shortcode( 'infographic', array( $this, 'infographic_embed' ) );
			add_shortcode( 'fb-live', array( $this, 'fb_live_video' ) );
			add_shortcode( 'hiddenimage', array( $this, 'hiddenimage_embed' ) );
			add_shortcode( 'smalltext', array( $this, 'smalltxt_shortcode' ) );
			add_shortcode( 'related',  array( $this, 'related_articles_embed' ) );
			add_shortcode( 'marketo',  array( $this, 'marketo_embed' ) );
		}
	}

