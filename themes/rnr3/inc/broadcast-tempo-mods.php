<?php
/**
 * Code to modify how Broadcast Plugin works
 * Features:
 * 		1) Lock all broadcasted posts that come from the tempo
 */

class Lockdown_Broadcasted_Posts {

	var $post_types;
	var $lock_capabilities;
	static $instance = false;

	function __construct() {

		// define the capabilities to edit locked posts
		$this->lock_capabilities = 'manage_options';

		// only lock if posts came from the tempo
		$tempo_id	= get_id_from_blogname( 'tempo' );

		if ( $tempo_id === get_current_blog_id() ) {
			add_filter( 'threewp_broadcast_broadcast_post', array( $this, 'lock_broadcasted_posts' ) );
		}

		add_action( 'init', array( $this, 'add_columns' ), 99 );

		add_action( 'pre_get_posts', array( $this, 'check' ) );

		add_filter( 'post_row_actions', array( $this, 'my_disable_quick_edit' ), 10, 2 );

	}

	public function my_disable_quick_edit( $actions, $post ) {

		$post_lock_status = get_post_meta( $post->ID, '_post_lock_status', true );

		if ( !current_user_can( $this->lock_capabilities ) && $post_lock_status === 'locked' ) {
			unset( $actions['inline hide-if-no-js'] );
			unset( $actions['edit'] );
			unset( $actions['trash'] );
		}

		// Return the set of links without Quick Edit
		return $actions;

	}

	public function lock_broadcasted_posts( $bcd ) {

		$blog_id	= get_current_blog_id();
		$post_id	= $bcd->broadcast_data->post_id;

		$broadcast_data = ThreeWP_Broadcast()->get_post_broadcast_data( $blog_id, $post_id );

		foreach( $broadcast_data->get_linked_children() as $child_blog_id => $child_post_id ) {
			switch_to_blog( $child_blog_id );
			update_post_meta( $child_post_id, '_post_lock_status', 'locked' );
			restore_current_blog();
		}

		return $bcd;

	}

	/**
	 * Properly manage columns
	 */
	public function add_columns() {
		// $this->post_types = get_post_types(array('show_ui' => true, 'public' => true));
		// unset( $this->post_types['attachment'] );

		// only want to add this column to posts
		$this->post_types	= array( 'post' );

		foreach ($this->post_types as $post_type) {
			add_filter( 'manage_edit-'.$post_type.'_columns', array( $this, 'status_column' ), 999 );
			add_action( 'manage_'.$post_type.'_posts_custom_column', array( $this, 'status_output' ), 99, 2 );
		}
	}

	/**
	 * Add column to posts management panel
	 */
	public function status_column( $columns ) {
		$columns['lock_status'] = __( 'Lock Status', 'lock_posts' );
		return $columns;
	}

	/**
	 * Display columns content on posts management panel
	 */
	public function status_output( $column, $id ) {
		if ( $column == 'lock_status' ) {
			$post_lock_status = get_post_meta( $id, '_post_lock_status' );

			if ( is_array( $post_lock_status ) && isset( $post_lock_status[0] ) )
				$post_lock_status = $post_lock_status[0];

			if ( 'locked' == $post_lock_status )
				echo __( 'Locked', 'lock_posts' );
			else
				echo __( 'Unlocked', 'lock_posts' );
		}
	}

	/**
	 * Check post status and redirect if the user is not super admin and post is locked
	 */
	public function check() {

		if ( !is_super_admin() && !empty( $_GET['action'] ) && 'edit' == $_GET['action'] && !empty( $_GET['post'] ) ) {
			$post_lock_status = get_post_meta( $_GET['post'], '_post_lock_status' );

			if ( is_array($post_lock_status) )
				$post_lock_status = $post_lock_status[0];

			if ( $post_lock_status == 'locked' )
				wp_redirect( admin_url( 'edit.php?page=post-locked&post=' . $_GET['post'] ) );
		}
	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if( ! self::$instance )
			self::$instance = new self();

		return self::$instance;
	}

}
