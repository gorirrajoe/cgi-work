<?php
  get_header('oneoffs');

?>
<style>
  .column {
    margin-bottom:60px;
  }
</style>
<main role="main" id="main">
  <section class="wrapper center">
    <?php echo '<figure><img src="'. get_bloginfo('stylesheet_directory') . '/img/newsletter/the-beat.png' .'" alt="The Beat"></figure>'; ?>
    <h2>Bringing You Partner Updates, News and Highlights from the Rock 'n' Roll Marathon Series</h2>
    <div class="narrow">
      <p><em>The Beat is your insider resource for updates from the Rock 'n' Roll Marathon Series, including in-depth case studies of successful activations by our partners, news and announcements about the series and industry updates. Our mission is to inspire and enable creativity amongst our partners. We believe that by sharing good ideas, we can enable deeper engagement between our partners and the Rock 'n' Roll audience.</em></p>
    </div>
  </section>

  <section class="wrapper grid_2">
    <?php
      $count = 0;

      $args = array( 
        'post_type' => 'thebeat',
        'posts_per_page' => -1
      );
      if ( false === ( $beat_query = get_transient( 'beat_query_results' ) ) ) {
        $beat_query = new WP_Query( $args );
        set_transient( 'beat_query_results', $beat_query, 5 * MINUTE_IN_SECONDS );
      }
 
      if ( $beat_query->have_posts() ) : 
        while ( $beat_query->have_posts() ) : 
          $beat_query->the_post();
          
          if( ($count % 2 ) == 0 && $count != 0 ) {
            echo '</section><section class="wrapper grid_2">';
          }
          echo '<div class="column">';
            $image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'newsletter-post'); ?>
            <figure>
              <a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><img src="<?php echo $image_array[0];?>"></a>
            </figure>
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <?php the_excerpt(); ?>
            <p><a href="<?php the_permalink(); ?>" class="read-more-link">Read Full Story</a></p>
            <?php $count++;
          echo '</div>';
        endwhile;
      else :

        if ( is_category() ) { // If this is a category archive
          printf("<h2 class='center'>Sorry, but there aren't any posts in the %s category yet.</h2>", single_cat_title('',false));
        } else if ( is_date() ) { // If this is a date archive
          echo("<h2>Sorry, but there aren't any posts with this date.</h2>");
        } else {
          echo("<h2 class='center'>No posts found.</h2>");
        }

      endif;

      wp_reset_postdata();
    ?>
  </section>

</main>
<?php get_footer( 'oneoffs' ); ?>