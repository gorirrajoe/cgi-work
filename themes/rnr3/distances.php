<?php
	/* Template Name: Distances */

	get_header();

	$parent_slug = the_parent_slug();
	rnr3_get_secondary_nav( $parent_slug );

	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}
	$prefix = '_rnr3_';

	$qt_lang = rnr3_get_language();
	include 'languages.php';

	$distances = rnr3_get_distances( $qt_lang['lang'], $market );
	$gallery_count = 0;
?>

<!-- main content -->
<main role="main" id="main">
	<section class="wrapper grid_2 offset240left">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<div id="tabs">

				<div class="column sidenav">
					<h2><?php echo $txt_distances_title; ?></h2>
					<ul>
						<?php foreach( $distances as $key => $value ) {
							if( isset( $value['active'] ) && $value['active'] == 'on' ) {
								echo '<li><a href="#tab-'.$key.'">'.$value['name'].'</a></li>';
							}
						} ?>
					</ul>
				</div>


				<div class="column">
					<div class="content">
						<?php foreach( $distances as $key => $value ) {
							if( isset( $value['active'] ) && $value['active'] == 'on' ) {
								$exclude 			= ( $value['label'] == 'pup'  ) ?  true : false;
								$top_image			= get_post_meta( get_the_ID(), $prefix . $value['label'].'_top_image', true );
								$start_line			= get_post_meta( get_the_ID(), $prefix . $value['label'].'_start_line', true );
								$finish_line		= get_post_meta( get_the_ID(), $prefix . $value['label'].'_finish_line', true );
								$time_limit			= get_post_meta( get_the_ID(), $prefix . $value['label'].'_time_limit', true );
								$race_distance		= get_post_meta( get_the_ID(), $prefix . $value['label'].'_race_distance', true );
								$about				= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . $value['label'].'_about', true ) );
								$finisher_jacket	= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . $value['label'].'_finisher_jacket', true ) );
								$team				= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . $value['label'].'_team', true ) );
								$perks				= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . $value['label'].'_perks', true ) );
								$perkscheck			= get_post_meta( get_the_ID(), $prefix . $value['label'].'_perkchecks', true );
								$bottom_image		= get_post_meta( get_the_ID(), $prefix . $value['label'].'_bottom_image', true );
								$misc_info			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . $value['label'].'_misc_info', true ) );
								$register_url		= get_post_meta( get_the_ID(), $prefix . $value['label'].'_register_url', true );

								$race_date			= get_post_meta( get_the_ID(), $prefix . $value['label'].'_race_date', true );

								if( $race_date != '' ) {
									$race_date_unix		= strtotime( $race_date );
									$race_date_array	= explode( '/', $race_date );
									$race_date_m		= get_day_or_month_by_language( $qt_lang['lang'], ( $race_date_array[0] -1 ), 'month_long' );
								}


								$race_time = get_post_meta( get_the_ID(), $prefix . $value['label'].'_race_time', true );
								if( $race_time == '' ) {
									$race_time = $txt_TBD;
								} else {
									if( $qt_lang['enabled'] == 1 ) {
										if( $qt_lang['lang'] == 'fr' ) {
											$race_timehr	= date('G', strtotime( $race_time ));
											$race_timemin	= date('i', strtotime( $race_time ));
											$race_time		= $race_timehr.' h '.$race_timemin;
										} elseif( $qt_lang['lang'] == 'es' || $qt_lang['lang'] == 'pt' ) {
											$race_timehr	= date('G', strtotime( $race_time ));
											$race_timemin	= date('i', strtotime( $race_time ));
											$race_time		= $race_timehr.':'.$race_timemin;
										}
									}
								}

								$course_tbd = get_post_meta( get_the_ID(), $prefix . $value['label'].'_course_tbd', 1 );

								echo '<div id="tab-'.$key.'">
									<h2>'.$value['name'].'</h2>';

									if( $top_image != '' ) {
										echo '<figure><img src="'. $top_image .'"></figure>';
									}


									/**
									 * if qtranslate_x is enabled
									 */
									if( $qt_lang['enabled'] == 1 ) {
										$start_line_array	= qtrans_split( $start_line );
										$finish_line_array	= qtrans_split( $finish_line );
										$time_limit_array	= qtrans_split( $time_limit );
										$distance_array		= qtrans_split( $race_distance );

										echo '<h3>'. $txt_at_a_glance .'</h3>

										<div class="glance">
											<div class="column">';

												if( $race_date != '' ) {
													echo '<p><strong>'. $txt_date .':</strong> ';
														if( $qt_lang['lang'] == 'es' ) {
															echo date( 'j', $race_date_unix ) .' de '. $race_date_m .' de '. date( 'Y', $race_date_unix );
														} elseif( $qt_lang['lang'] == 'pt' ) {
															echo date( 'j', $race_date_unix ) .' de '. ucfirst( $race_date_m ) .' de '. date( 'Y', $race_date_unix );
														} elseif( $qt_lang['lang'] == 'fr' ) {
															echo date( 'j', $race_date_unix ) .' '. $race_date_m .' '. date( 'Y', $race_date_unix );
														} else {
															echo ucfirst( $race_date_m ) . ' ' . date( 'j', $race_date_unix ) . ', ' . date( 'Y', $race_date_unix );
														}
													echo '</p>';
												} else {
													echo '<p><strong>'.$txt_date.':</strong> TBD</p>';

												}

												echo '<p><strong>'.$txt_start_time.':</strong> '. $race_time .'</p>
												<p><strong>'.$txt_time_limit.':</strong> '. $time_limit_array[$qt_lang['lang']] .'</p>';

												if( $race_distance != '' ) {
													echo '<p><strong>'. $txt_distance .':</strong> '. $distance_array[$qt_lang['lang']] .'</p>';
												}

											echo '</div>


											<div class="column">
												<p><strong>'.$txt_start_line.':</strong> '. $start_line_array[$qt_lang['lang']] .'</p>
												<p><strong>'.$txt_finish_line.':</strong> '. $finish_line_array[$qt_lang['lang']] .'</p>';
												if( !$exclude ) {
													if( $course_tbd == 'on' ) {
														echo '<p><strong>'.$txt_course_map.':</strong> '.$txt_TBD.' </p>';
													} else {
														echo '<p><strong>'.$txt_course_map.':</strong> <a href="'.site_url('/the-races/course/').'">'.$txt_view.'</a> </p>';
													}
												}

											echo '</div>


											<div class="column reg">
												<a class="cta" href="'.site_url('register').'">'.$txt_register.'</a>
											</div>
										</div>';
									} else {
										echo '<h3>At a Glance</h3>
										<div class="glance">
											<div class="column">';

												if( $race_date != '' ) {
													echo '<p><strong>Date:</strong> '. ucfirst( $race_date_m ) . ' ' . date( 'j', $race_date_unix ) . ', ' . date( 'Y', $race_date_unix ) .'</p>';
												} else {
													echo '<p><strong>Date:</strong> TBD</p>';

												}

												echo '<p><strong>Start Time:</strong> '. $race_time .'*</p>
												<p><strong>Time Limit:</strong> '. $time_limit .'</p>';

												if( $race_distance != '' ) {
													echo '<p><strong>'. $txt_distance .':</strong> '. $race_distance .'</p>';
												}

												echo '<p>* Times subject to change</p>
											</div>

											<div class="column">
												<p><strong>Start Line:</strong> '. $start_line .'</p>
												<p><strong>Finish Line:</strong> '. $finish_line .'</p>';

												if( !$exclude ) {
													if( $course_tbd == 'on' ) {
														echo '<p><strong>Course Map:</strong> TBD </p>';
													} else {
														echo '<p><strong>Course Map:</strong> <a href="'.site_url('/the-races/course/').'">View</a></p>';
													}
												}

											echo '</div>

											<div class="column reg">
												<a class="cta" href="'.site_url('register').'">'.$txt_register.'</a>
											</div>
										</div>';
									}


									if( $about != '' ) {
										if($qt_lang['enabled'] == 1){
											$about_array = qtrans_split($about);

											echo '<h3>'.$txt_about_course.'</h3>
											'.$about_array[$qt_lang['lang']];
										} else {
											echo '<h3>About the Course</h3>
											'.$about;
										}
									}

									$gallery = get_post_meta( get_the_ID(), $prefix . $value['label'] . '_gallery', 1 );

									if( get_post_meta( get_the_ID(), $prefix . $value['label'] . '_gallery_title', 1 ) == '' ) {
										$gallery_title = 'Gallery';
									} else {
										$gallery_title = get_post_meta( get_the_ID(), $prefix . $value['label'] . '_gallery_title', 1 );
									}


									if( $gallery != '' ) {
										$gallery_count++;

										echo '<h3>'. $gallery_title .'</h3>

										<div class="gallery-carousel owl-theme owl-distances">';

											foreach( $gallery as $attachment_id => $attachment_url ) {
												$large_img_url	= wp_get_attachment_image_src( $attachment_id, 'large' );

												$img_deets = get_post( $attachment_id );

												echo '<div>
													<a data-caption="'. $img_deets->post_excerpt .'" data-fancybox="gallery-'. $gallery_count .'" href="'. $large_img_url[0] .'">'. wp_get_attachment_image( $attachment_id, 'distance-gallery' ) .'</a>';

													if( $img_deets->post_excerpt != '' ) {
														echo '<p>'. $img_deets->post_excerpt .'</p>';
													}
												echo '</div>';

											}
										echo '</div>';
									}


									if( $team != '' ) {
										if( $qt_lang['enabled'] == 1 ) {
											$team_array = qtrans_split($team);
											echo '<h3>Team Competition</h3>
											'.$team_array[$qt_lang['lang']];
										} else {
											echo '<h3>Team Competition</h3>
											'.$team;
										}
									}


									if( $perks != '' || !empty( $perkscheck ) ) {
										if( $qt_lang['enabled'] == 1 ) {
											$perks_array = qtrans_split($perks);
											echo '<h3>'.$txt_part_perk.'</h3>'
											.$perks_array[$qt_lang['lang']];
										} else {
											echo '<h3>Participant Perks</h3>'
											.$perks;
										}
									}


									if( !empty( $perkscheck ) ) {
										echo '<div class="perks">
											<ul>';
												foreach( $perkscheck as $check ) {
													if( $qt_lang['enabled'] == 1 ) {
														$check_array = qtrans_split( $check );

														//Remove Brooks
														$clean_str1 = str_replace( "Brooks ", "", $check_array[$qt_lang['lang']] );

														//Change Tee to T-Shirt
														$clean_str2 = str_replace( " Tee", " T-Shirt", $clean_str1 );
														echo '<li>'. $clean_str2 .'</li>';
													} else {
														//Remove Brooks
														$clean_str1 = str_replace( "Brooks ", "", $check );

														//Change Tee to T-Shirt
														$clean_str2 = str_replace( " Tee", " T-Shirt", $clean_str1 );
														echo '<li>'. $clean_str2 .'</li>';

													}

												}
											echo '</ul>
										</div>';
									}


									if( $bottom_image != '' ) {
										echo '<figure><img src="'.$bottom_image.'"></figure>';
									}


									if( $finisher_jacket != '' ) {
										if( $qt_lang['enabled'] == 1 ) {
											$finisher_jacket_array = qtrans_split( $finisher_jacket );
											echo '<h3>'. $txt_finisher_jacket .'</h3>
											<div class="perks" id="fjacket">'. $finisher_jacket_array[$qt_lang['lang']] ."</div>";
										} else {
											echo '<h3>'. $value['name'] .' Finisher Jacket</h3>
											<div class="perks">'. $finisher_jacket ."</div>";
										}
									}

									if( $misc_info != '' ) {
										if( $qt_lang['enabled'] == 1 ) {
											$misc_array = qtrans_split( $misc_info );
											echo $misc_array[$qt_lang['lang']];
										} else {
											echo $misc_info;
										}
									}
								echo '</div>';
							}
						} ?>
					</div>
				</div>
			</div>
		<?php endwhile; endif; ?>
	</section>

</main>

<script>
	jQuery(function($) {
		$( "#tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
	});

	$(document).ready(function(){

		/**
		 * This part causes smooth scrolling using scrollto.js
		 * We target all a tags inside the nav, and apply the scrollto.js to it.
		 */
		 $(".sticky-nav a, .backtotop").click(function(evn){
		 	evn.preventDefault();
		 	$('html,body').scrollTo(this.hash, this.hash);
		 });

	});

</script>

<?php get_footer(); ?>
