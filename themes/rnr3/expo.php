<?php
	/* Template Name: Expo */

	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}
	$qt_lang = rnr3_get_language();
	include 'languages.php';

	$prefix = '_rnr3_';

	// contents
	$participant['content']	= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'participant', true ) );
	$donation['content']	= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'donation', true ) );
	$exhibitor['content']	= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'exhibitor', true ) );

	$expo_array = array(
		$participant,
		$donation,
		$exhibitor
	);

	// app page -- will need to pull in all meta boxes too
	if(isset($_GET) && isset($_GET['app-page'])){

		echo '<!doctype html>
			<html class="no-js">
			<head>
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<link rel="stylesheet" href="'.get_bloginfo('template_url').'/css/app.min.css" type="text/css" media="screen" />
			</head>
			<body>
				<div id="nav-anchor"></div>';
				if (have_posts()) : while (have_posts()) : the_post();
					// the_title('<h2>', '</h2>');
					the_content();
					endwhile;
				endif;

				foreach( $expo_array as $key => $value ) {
					if( $value['content'] != '' ) {
						echo '<section id="before'.$key.'">
							<h2>'.$value['title'].'</h2>
							'. $value['content'] .
						'</section>';
					}
				}

			echo '</body>
		</html>';

	} else {

		get_header();

		$parent_slug = the_parent_slug();
		rnr3_get_secondary_nav( $parent_slug ); ?>

		<!-- main content -->
		<main role="main" id="main">
			<section class="wrapper grid_2 offset240left">
				<div class="column sidenav stickem">
					<div id="nav-anchor"></div>
					<h2>Expo</h2>
					<nav class="sticky-nav">
						<ul>
							<?php
								foreach( $expo_array as $key => $value ) {
									if( $value['content'] != '' ) {
										echo '<li><a href="#before'.$key.'">'.$value['title'].'</a></li>';
									}
								}
							?>
						</ul>
					</nav>
				</div>

				<div class="column">
					<div class="content">
						<?php if (have_posts()) : while (have_posts()) : the_post();
							// echo '<h2>'. get_the_title(). '</h2>';
							the_content();
						endwhile; endif;

						foreach( $expo_array as $key => $value ) {
							if( $value['content'] != '' ) {
								echo '<section id="before'.$key.'">
									<h2>'.$value['title'].'</h2>
									'. $value['content'] .
								'</section>';
							}
						} ?>
					</div>
				</div>
			</section>
			<script>
				$(document).ready(function(){
					/**
					 * This part causes smooth scrolling using scrollto.js
					 * We target all a tags inside the nav, and apply the scrollto.js to it.
					 */
					$(".sticky-nav a, .backtotop").click(function(evn){
						evn.preventDefault();
						$('html,body').scrollTo(this.hash, this.hash);
					});

					/*$('main').stickem({
						container: '.offset240left',
					});*/

					/**
					 * This part handles the highlighting functionality.
					 * We use the scroll functionality again, some array creation and
					 * manipulation, class adding and class removing, and conditional testing
					 */
					var aChildren = $(".sticky-nav li").children(); // find the a children of the list items
					var aArray = []; // create the empty aArray
					for (var i=0; i < aChildren.length; i++) {
						var aChild = aChildren[i];
						var ahref = $(aChild).attr('href');
						aArray.push(ahref);
					} // this for loop fills the aArray with attribute href values

					$(window).scroll(function(){
						var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
						var windowHeight = $(window).height(); // get the height of the window
						var docHeight = $(document).height();

						for (var i=0; i < aArray.length; i++) {
							var theID = aArray[i];
							var divPos = $(theID).offset().top; // get the offset of the div from the top of page
							var divHeight = $(theID).height(); // get the height of the div in question
							if (windowPos >= (divPos - 1) && windowPos < (divPos + divHeight -1 )) {
								$("a[href='" + theID + "']").addClass("nav-active");
							} else {
								$("a[href='" + theID + "']").removeClass("nav-active");
							}
						}

						if(windowPos + windowHeight == docHeight) {
							if (!$(".sticky-nav li:last-child a").hasClass("nav-active")) {
								var navActiveCurrent = $(".nav-active").attr("href");
								$("a[href='" + navActiveCurrent + "']").removeClass("nav-active");
								$(".sticky-nav li:last-child a").addClass("nav-active");
							}
						}
					});
				});

			</script>
		</main>
		<?php get_footer();

	}
?>
