<?php
	/* Template Name: News & Promotions Archive Page */
	get_header();

	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

	$args = array(
		'category_name'		=> 'news, promotion',
		'posts_per_page'	=> 10,
		'paged'				=> $paged,
		'post_status'		=> 'publish',
		'has_password'		=> false
	);

	if ( false === ( $news_promo_query = get_transient( 'news_promo_query_results' ) ) ) {
		$news_promo_query = new WP_Query( $args );
		set_transient( 'news_promo_query', $news_promo_query, 5 * MINUTE_IN_SECONDS );
	}

	// since get_next_posts_link() function only works with $wp_query
	$wp_query = $news_promo_query;


?>

<!-- main content -->
<main role="main" id="main">
	<section class="wrapper grid_2 offset240left">
		<div class="column archive_list">

			<h2>News &amp; Promotions Archive</h2>

			<?php if ($news_promo_query->have_posts()) :
				while ($news_promo_query->have_posts()) :
					$news_promo_query->the_post(); ?>

					<article <?php post_class(); ?>>
						<?php $image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'news-promos-thumbs');
						if($image_array[0]) { ?>
							<a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><img class="wp-post-image alignleft" src="<?php echo $image_array[0];?>"></a>
						<?php }

						echo '<h3><a href="'.get_the_permalink().'">'. get_the_title() .'</a></h3>
						<p>'.get_the_excerpt().'</p>
						<a href="'.get_the_permalink().'" class="read-more-link">Read Full Story</a>

					</article>';

				endwhile; ?>

				<nav class="archive_nav">
					<?php if( get_next_posts_link() ) { ?>
						<div class="alignleft"><?php next_posts_link('&laquo; Older Entries', $news_promo_query->max_num_pages) ?></div>
					<?php }

					if( get_previous_posts_link() ) { ?>
						<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
					<?php } ?>

				</nav>

				<?php wp_reset_postdata();
			endif; ?>
		</div>

		<?php get_sidebar(); ?>

	</section>
</main>

<?php get_footer(); ?>
