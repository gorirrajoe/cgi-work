<?php
	/* Template Name: VIP - Westin */
	get_header();

	$parent_slug = the_parent_slug();
	rnr3_get_secondary_nav( $parent_slug );

	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$prefix = '_rnr3_';

	/**
	 * Lets prep all our data here.
	 * Creating a custom array with all info needed for each package. Title, cost, URL, perks, etc.
	 */
	$packages_array = array(
		array(
			'title'	=> 'Westin Platinum',
		),
		array(
			'title'	=> 'Gold',
		),
		array(
			'title'	=> 'Pre-Race Silver',
		),
	);

	$post_ID	= get_the_ID();

	$post_meta	= get_post_meta( $post_ID );

	if ( !empty( $post_meta ) ) {

		$packages_replacements	= array_filter_key( $post_meta, function( $key ) {
			return strpos( $key, '_rnr3_package_' ) === 0;
		} );

		if ( !empty( $packages_replacements ) ) {
			foreach ( $packages_replacements as $key => $value_array ) {
				$array_position	= substr( $key, -1 );
				$packages_array[$array_position]['title']	= $value_array[0];
			}
		}

	}

	$perks_array	= get_post_meta( $post_ID, $prefix . 'perks', true );

	foreach ( $packages_array as $key => $package ) {
		// add perks to the packages
		foreach ( $perks_array as $k => $perk ) {
			if ( in_array( $key, $perk['package']) ) {
				$packages_array[$key]['perks'][] = $k;
			}
		}

		// add cost and URL
		$packages_array[$key]['cost']	= !empty( $post_meta[$prefix .'package'. $key .'_cost'] ) ? $post_meta[$prefix .'package'. $key .'_cost'][0] : '';
		$packages_array[$key]['url']	= !empty( $post_meta[$prefix .'package'. $key .'_url'] ) ? $post_meta[$prefix .'package'. $key .'_url'][0] : '';

	}
?>

	<!-- main content -->
	<main role="main" id="main">
		<section class="wrapper">
			<div class="content">
				<?php
					if (have_posts()) : while (have_posts()) : the_post();
						echo '<h2>'. get_the_title(). '</h2>';
						the_content();
					endwhile; endif;
				?>
				<table class="responsive_table" cellspacing="0" cellpadding="0" border="0">
					<thead>
						<tr>
							<th></th>
							<?php
								foreach ( $packages_array as $key => $package ) {
									if ( !empty( $package['perks'] ) ) {
										echo '<th class="vip_hdr"><span class="vip_spectator">'. $package['title'] .'</span></th>';
									}
								}
							?>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach ( $perks_array as $key => $perk ) {
								echo '<tr>';

									echo '<td class="vip_perk">';

										echo $perk['title'];

										if ( !empty( $perk['description'] ) ) {
											echo '<a href="#" class="toggle"><span class="icon-help-circled"></span></a>
											<div class="vip_tooltip">'. $perk['description'] .'</div>';
										}

									echo '</td>';

									foreach ( $packages_array as $k => $package ) {
										if ( empty( $package['perks'] ) ) {
											// blank if perks are blank
										} elseif ( in_array( $k, $perk['package'] ) ) {
											echo '<td><span class="icon-ok-circled"></span></td>';
										} else {
											echo '<td></td>';
										}
									}

								echo '</tr>';

							}
						?>
						<tr class="vip_no_shade">
							<td class="vip_no_border"></td>

							<?php
								foreach ( $packages_array as $key => $package ) {

									if ( !empty( $package['perks'] ) ) {

										echo '<td>';
											if ( $package['cost'] == '0' ) {
												echo '<div class="vip_pricing">Sold Out</div>';
											} else {
												echo '<div class="vip_pricing">$'. $package['cost'] .'</div>';
											}

											if ( $package['url'] != '' ) {
												echo '<div><a class="cta highlight" href="'. $package['url'] .'">Purchase</a></div>';
											}
										echo '</td>';

									}

								}
							?>
						</tr>
						<tr>
							<td class="vip_no_border"></td>
							<td class="vip_footnote" colspan="5">NOTE: VIP Packages do NOT include race entry.</td>
						</tr>
					</tbody>
				</table>


				<?php /* tabs for mobile */ ?>
				<div id="vip_tabs">
					<ul class="vip_tabs_nav">
						<?php
							foreach ( $packages_array as $key => $package ) {
								if ( !empty( $package['perks'] ) ) {
									echo '<li class="sp_tab"><a href="#tabs-'. $key .'">'. $package['title'] .'</a></li>';
								}
							}
						?>
					</ul>

					<?php
						foreach ( $packages_array as $key => $package ) {

							if ( !empty( $package['perks'] ) ) {

								echo '<div id="tabs-'.$key.'">';

									if ( $package['cost'] != '0' ) {
										echo '<h3>'. $package['title'] .' &mdash; $'. $package['cost'] .'</h3>';
									} else {
										echo '<h3>'. $package['title'] .' &mdash; Sold Out</h3>';
									}

									echo '<ul>';

										foreach ( $package['perks'] as $perk_key ) {
											echo '<li>';
												// show link and toggle if a tooltip is written
												if ( $perks_array[$perk_key]['description'] != '' ) {
													echo $perks_array[$perk_key]['title'] .'<a href="#" class="toggle"><span class="icon-help-circled"></span></a>
													<div class="vip_tooltip">'. $perks_array[$perk_key]['description'] .'</div>';
												} else {
													echo $perks_array[$perk_key]['title'];
												}
											echo '</li>';

										}

									echo '</ul>';

									if ( $package['cost'] != '0' ) {
										echo '<div class="vip_purchase"><a class="cta highlight" href="'. $package['url'] .'">Purchase &mdash; $'. $package['cost'] .'</a></div>';
									} else {
										echo '<div class="vip_purchase"><span class="cta highlight">Sold Out</span></div>';
									}

								echo '</div>';

							}

						}
					?>
				</div>
				<p class="vip_footnote_mobile">NOTE: VIP Packages do NOT include race entry.</p>
			</div>
		</section>
	</main>


	<script>
		$(function() {
			$( '#vip_tabs' ).tabs();
		});

		$(document).ready(function() {
			$(".toggle").click(function(e){
				$(this).nextAll(".vip_tooltip").first().slideToggle();
				e.preventDefault();
			});
		});

	</script>

<?php get_footer(); ?>
