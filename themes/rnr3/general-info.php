<?php
	/* Template Name: General Info */

	// meta boxes!
	$prefix	= '_rnr3_';
	$market	= get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';

	// contents
	$how_to_register['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'how_to_register', 1 ) );
	$confirmation['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'confirmation', 1 ) );
	$personalize_bib['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'personalize_bib', 1 ) );
	$packet_pickup['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'packet_pickup', 1 ) );
	$corral_changes['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'corral_changes', 1 ) );
	$deferral['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'deferral', 1 ) );
	$cancellations['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'cancellations', 1 ) );
	$race_rules['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'race_rules', 1 ) );
	$directions['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'directions', 1 ) );
	$parking['content']				= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'parking_transportation', 1 ) );
	$allday_20k['content']				= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'all_day_20k', 1 ) );
	$road_closures['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'road_closures', 1 ) );
	$gear_check['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'gear_check', 1 ) );
	$wave_start['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'wave_start', 1 ) );
	$time_limits['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'time_limits', 1 ) );
	$course_support['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'course_support', 1 ) );
	$race_timing['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'race_timing', 1 ) );
	$charity['content']				= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'charity', 1 ) );
	$volunteer['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'volunteer', 1 ) );
	$competitor_wireless['content']	= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'competitor_wireless', 1 ) );
	$secure_zone['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'secure_zone', 1 ) );
	$family_reunion['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'family_reunion', 1 ) );
	$medal_info['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'medal_info', 1 ) );
	$age_group['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'age_group', 1 ) );
	$marathon_jacket['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'marathon_jacket', 1 ) );
	$info_booth['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'info_booth', 1 ) );
	$medical['content']				= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'medical', 1 ) );
	$race_changes['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'race_changes', 1 ) );
	$wedding['content']				= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'wedding', 1 ) );
	$pace_team['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'pace_team', 1 ) );
	$trainingplans['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'trainingplans', 1 ) );
	$grand_prix['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'grand_prix', 1 ) );

	$before_array = array(
		$how_to_register,
		$confirmation,
		$personalize_bib,
		$packet_pickup,
		$race_changes,
		$corral_changes,
		$deferral,
		$cancellations,
		$trainingplans
	);

	$raceday_array = array(
		$race_rules,
		$directions,
		$parking,
		$allday_20k,
		$road_closures,
		$gear_check,
		$wave_start,
		$time_limits,
		$info_booth,
		$wedding,
		$medical,
		$course_support,
		$race_timing,
		$charity,
		$volunteer,
		$pace_team,
		$competitor_wireless
	);

	$after_array = array(
		$secure_zone,
		$family_reunion,
		$medal_info,
		$age_group,
		$marathon_jacket,
		$grand_prix
	);

	$beforesidebar = $beforecontent = $racedaysidebar = $racedaycontent = $aftersidebar = $aftercontent = '';

	// app page -- will need to pull in all meta boxes too
	if( isset( $_GET ) && isset( $_GET['app-page'] ) ) {
		echo '<!doctype html>
			<html class="no-js">
			<head>
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<link rel="stylesheet" href="'. get_bloginfo('template_url') .'/css/app.min.css" type="text/css" media="screen" />';
			echo '</head>
			<body>
				<div id="nav-anchor"></div>';
				if (have_posts()) : while (have_posts()) : the_post();
					// the_title('<h2>', '</h2>');
					the_content();
					endwhile;
				endif;

				echo '<h2>'.$txt_before_race.'</h2>';

				foreach( $before_array as $key => $value ) {
					if( $value['content'] != '' ) {
						echo '<section id="before'.$key.'">
							<h3>'.$value['title'].'</h3>'
							. $value['content'] .
						'</section>';
					}
				}

				echo '<h2>'.$txt_race_day.'</h2>';

				foreach( $raceday_array as $key => $value ) {
					if( $value['content'] != '' ) {
						echo '<section id="raceday'.$key.'">
							<h3>'.$value['title'].'</h3>'
							. $value['content'] .
						'</section>';
					}
				}

				echo '<h2>'.$txt_after_race.'</h2>';

				foreach( $after_array as $key => $value ) {
					if( $value['content'] != '' ) {
						echo '<section id="after'.$key.'">
							<h3>'.$value['title'].'</h3>'
							. $value['content'] .
						'</section>';
					}
				}


			echo '</body>
		</html>';
	} else {

		get_header();

		$parent_slug = the_parent_slug();
		rnr3_get_secondary_nav( $parent_slug ); ?>

		<!-- main content -->
		<main role="main" id="main">
			<section class="wrapper grid_2 offset240left">
				<div class="column sidenav">
					<div id="nav-anchor"></div>
					<nav class="sticky-nav">
						<?php
							foreach( $before_array as $key => $value ) {
								if( $value['content'] != '' ) {
									$beforesidebar .= '<li><a href="#before'.$key.'">'.$value['title'].'</a></li>';
									$beforecontent .= '<section id="before'.$key.'">
										<h3>'.$value['title'].'</h3>'
										. $value['content'] .
									'</section>';

								}
							}
							if( $beforesidebar != '' ) {
								echo '<h2>'. $txt_before_race.' </h2>
								<ul>
									'. $beforesidebar .'
								</ul>';
							}

							foreach( $raceday_array as $key => $value ) {
								if( $value['content'] != '' ) {
									$racedaysidebar .= '<li><a href="#raceday'.$key.'">'.$value['title'].'</a></li>';
									$racedaycontent .= '<section id="raceday'.$key.'">
										<h3>'.$value['title'].'</h3>'
										. $value['content'] .
									'</section>';
								}
							}
							if( $racedaysidebar != '' ) {
								echo '<h2>'. $txt_race_day .'</h2>
								<ul>
									'. $racedaysidebar .'
								</ul>';
							}

							foreach( $after_array as $key => $value ) {
								if( $value['content'] != '' ) {
									$aftersidebar .= '<li><a href="#after'.$key.'">'.$value['title'].'</a></li>';
									$aftercontent .= '<section id="after'.$key.'">
										<h3>'.$value['title'].'</h3>'
										. $value['content'] .
									'</section>';
								}
							}
							if( $aftersidebar != '' ) {
								echo '<h2>'. $txt_after_race .'</h2>
								<ul>
									'. $aftersidebar .'
								</ul>';
							}


							/**
							 * faq link
							 */
							echo '<h2>Questions</h2>
							<ul>
								<li><a class="faq_link" href="'. network_site_url( '/contact/faq/' ) .'">FAQ</a></li>
							</ul>';

						?>
					</nav>
				</div>

				<div class="column">
					<div class="content">
						<?php if (have_posts()) : while (have_posts()) : the_post();
							// echo '<h2>'. get_the_title(). '</h2>';
							the_content();
						endwhile; endif; ?>

						<?php
							if( $beforesidebar != '' ) {
								echo '<h2>'.$txt_before_race.'</h2>'
								. $beforecontent;

							}
							if( $racedaysidebar != '' ) {
								echo '<h2 class="separatortitle">'.$txt_race_day.'</h2>'
								. $racedaycontent;
							}

							if( $aftersidebar != '' ) {
								echo '<h2 class="separatortitle">'.$txt_after_race.'</h2>'
								. $aftercontent;
							}

						?>
					</div>
				</div>
			</section>
			<script>
				$(document).ready(function(){

					/**
					 * This part causes smooth scrolling using scrollto.js
					 * We target all a tags inside the nav, and apply the scrollto.js to it.
					 */
					$( ".sticky-nav a" ).click( function( evn ) {
						if( !$( this ).hasClass( 'faq_link' ) ) {
							evn.preventDefault();
							$( 'html, body' ).scrollTo( this.hash, this.hash );
						}
					});

				});

			</script>
		</main>

		<?php get_footer();
	}
