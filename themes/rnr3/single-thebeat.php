<?php
  get_header('oneoffs' ); 
?>
<style>
  .morearticles {
    margin:60px auto;
    overflow:hidden;
  }
</style>
<main role="main" id="main">
  <section class="wrapper center">
    <?php echo '<figure><img src="'. get_bloginfo('stylesheet_directory') . '/img/newsletter/the-beat.png' .'" alt="The Beat"></figure>'; ?>
    <h2>Bringing You Partner Updates, News and Highlights from the Rock 'n' Roll Marathon Series</h2>
  </section>

  <section class="wrapper">
    <div class="content">
      <?php 
        if ( have_posts() ) : 
          while ( have_posts() ) : 
            the_post();
            the_title( '<h1>', '</h1>' ); 
            echo '<p class="byline">Posted by: <strong>Rock \'n\' Roll Marathon Series</strong> on '. get_the_time('F jS, Y') .'</p>
            <div class="narrow">';
              the_content();
            echo '</div>';
          endwhile;
        endif;
      ?>
    </div>
  </section>

  <section class="wrapper morearticles">
    <h2>More Articles From The Beat</h2>
    <section class="grid_4">
      <?php 
        $args = array(
          'post_type' => 'thebeat',
          'posts_per_page' => 4
        );
        $beat_query = new WP_Query($args);

        if ( $beat_query->have_posts() ) :
          while ( $beat_query->have_posts() ) :
            $beat_query->the_post(); ?>
            
            <div class="column">
              <?php $image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'newsletter-post'); ?>
              <figure>
                <a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><img src="<?php echo $image_array[0];?>"></a>
              </figure>
              <p><strong><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></strong></p>
            </div>
          <?php endwhile;
        endif;
      ?>
    </section>
    <p><a class="cta" href="<?php echo site_url('thebeat') ?>">View All Articles &gt;></a></p>
  </section>

</main>
<?php get_footer( 'oneoffs' ); ?>
