<?php get_header( 'oneoffs' );

  $rnb_ambo_event_readable = get_post_meta(get_the_ID(), '_rnb_ambo_event_readable', TRUE);
  $name = get_post_meta(get_the_ID(), '_rnb_ambo_name', TRUE);
  $location = get_post_meta(get_the_ID(), '_rnb_ambo_location', TRUE);
  $blog = get_post_meta(get_the_ID(), '_rnb_ambo_rnbblog', TRUE);
  $rnbtwitter = get_post_meta(get_the_ID(), '_rnb_ambo_rnbtwitter', TRUE);
  $rnbinstagrom = get_post_meta(get_the_ID(), '_rnb_ambo_rnbinstagram', TRUE);
  $rnbpinterest = get_post_meta(get_the_ID(), '_rnb_ambo_rnbpinterst', TRUE);
  $rnbfacebook = get_post_meta(get_the_ID(), '_rnb_ambo_rnbfacebook', TRUE);
  $fav_quote = apply_filters('the_content', get_post_meta( get_the_ID(), '_rnb_ambo_fav_quote', true ));
  $why_do_you_run = apply_filters('the_content', get_post_meta( get_the_ID(), '_rnb_ambo_why_do_you_run', TRUE ));
  $fav_place_to_run = apply_filters('the_content', get_post_meta( get_the_ID(), '_rnb_ambo_fav_place', TRUE ));
  $running_gear = apply_filters('the_content', get_post_meta( get_the_ID(), '_rnb_ambo_run_gear', TRUE ));
  $where_find = apply_filters('the_content', get_post_meta( get_the_ID(), '_rnb_ambo_where_find', TRUE ));
  $post_race = apply_filters('the_content', get_post_meta( get_the_ID(), '_rnb_ambo_post_race', TRUE ));
  $song = apply_filters('the_content', get_post_meta( get_the_ID(), '_rnb_ambo_song', TRUE ));
?>
  <style>
    .rnbround .attachment-rnb-ambassador-thumblg {
      border-radius:50%;
      margin-bottom:30px;
    }
  </style>

  <main role="main" id="main">
    <section class="wrapper grid_2 offset240left">
      <div class="column sidenav">
        <div style="margin-bottom:30px; text-align:center;"><a class="cta" href="<?php echo site_url('/rock-n-blog/'); ?>">View All</a></div>
        <div class="rnbround">
          <?php
            $image = get_the_post_thumbnail( $entry->ID, 'travel-hotel' );
            echo $image;
          ?>

        </div>

        <?php
          if ($name) {
            echo '<p><strong>Name: </strong>'.$name.'</p>';
          } if ($location) {
            echo '<p><strong>Location: </strong>'.$location.'</p>';
          } if ($blog) {
            echo '<p><strong>Blog: </strong>'.$blog.'</p>';
          } if ($rnbtwitter) {
            echo '<p><strong>Twitter:</strong> '.$rnbtwitter.'</p>';
          } if ($rnbinstagrom) {
            echo '<p><strong>Instagram: </strong>'.$rnbinstagrom.'</p>';
          } if ($rnbpinterest) {
          echo '
            <div class="ambo_action_photos">
              <p>'.$rnbpinterest.'</p>
            </div>
          ';
          } if ($rnbfacebook) {
          echo '
            <p><strong>Facebook: </strong>'.$rnbfacebook.'</p>
          ';
          } ?>

      </div>
      <div class="column">
        <div class="content">
          <h1>Rock 'N' Blog Ambassador</h1>
          <?php the_title( '<h2>', '</h2>' ); ?>
          <?php if ($fav_quote) {
            echo '<h3>Favorite Quote:</h3>
            '.$fav_quote;
          } if ($why_do_you_run) {
            echo '<h3>Why do you run?</h3>
            '.$why_do_you_run;
          } if ($fav_place_to_run) {
            echo '<h3>Favorite place to run:</h3>
            '.$fav_place_to_run;
          } if ($running_gear) {
            echo '<h3>Running gear you can not live without:</h3>
            '.$running_gear;
          } if ($where_find) {
            echo '<h3>When not running where would we find you?</h3>
            '.$where_find;
          } if ($post_race) {
            echo '<h3>Favorite post race fuel:</h3>
            '.$post_race;
          } if ($song) {
            echo '<h3>Song you want to hear at the finish line:</h3>
            '.$song;
          } ?>
        </div>
      </div>
    </section>
  </main>


<?php get_footer( 'oneoffs' ); ?>
