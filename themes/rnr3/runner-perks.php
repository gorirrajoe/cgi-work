<?php
  /* Template Name: Runner Perks */
  get_header();

  $parent_slug = the_parent_slug();
  rnr3_get_secondary_nav( $parent_slug );

  // meta boxes!
  $prefix = '_rnr3_';

  // titles
  $retail['title']          = 'Shopping and Retail';
  $transportation['title']  = 'Transportation';
  $dining['title']          = 'Dining';
  $entertainment['title']   = 'Entertainment';
  $recreation['title']      = 'Recreation';

  // contents
  $retail['content']          = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'retail', true ) );
  $transportation['content']  = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'transportation', true ) );
  $dining['content']          = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'dining', true ) );
  $entertainment['content']   = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'entertainment', true ) );
  $recreation['content']      = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'recreation', true ) );

  $perks_array = array(
    $retail,
    $transportation,
    $dining,
    $entertainment,
    $recreation
  );

?>

  <!-- main content -->
  <main role="main" id="main">
    <section class="wrapper grid_2 offset240left">
      <div class="column sidenav">
        <div id="nav-anchor"></div>
        <nav class="sticky-nav">
          <ul>
            <?php
              foreach( $perks_array as $key => $value ) {
                if( $value['content'] != '' ) {
                  echo '<li><a href="#before'.$key.'">'.$value['title'].'</a></li>';
                }
              }
            ?>
          </ul>
        </nav>
      </div>

      <div class="column">
        <div class="content">
          <?php if (have_posts()) : while (have_posts()) : the_post();
            // echo '<h2>'. get_the_title(). '</h2>';
            the_content();
          endwhile; endif; ?>

          <?php
            foreach( $perks_array as $key => $value ) {
              if( $value['content'] != '' ) {
                echo '<section id="before'.$key.'">
                  <h2>'.$value['title'].'</h2>
                  '. $value['content'] .
                '</section>';
              }
            }
          ?>
        </div>
      </div>
    </section>
    <script>
      /*
      jQuery(function($) {
        $( "#accordion" ).accordion({
          header:'h4',
          heightStyle: 'content'
        });
      });
      */
      $(document).ready(function(){
          
          /** 
           * This part does the "fixed navigation after scroll" functionality
           * We use the jQuery function scroll() to recalculate our variables as the 
           * page is scrolled/
          $(window).scroll(function(){
              var window_top = $(window).scrollTop() + 50; // the "12" should equal the margin-top value for nav.stick
              var div_top = $('#nav-anchor').offset().top;
                  if (window_top > div_top) {
                      $('.sticky-nav').addClass('stick');
                  } else {
                      $('.sticky-nav').removeClass('stick');
                  }
          });
           */
          
          
          /**
           * This part causes smooth scrolling using scrollto.js
           * We target all a tags inside the nav, and apply the scrollto.js to it.
           */
          $(".sticky-nav a, .backtotop").click(function(evn){
              evn.preventDefault();
              $('html,body').scrollTo(this.hash, this.hash); 
          });
          
          
          
          /**
           * This part handles the highlighting functionality.
           * We use the scroll functionality again, some array creation and 
           * manipulation, class adding and class removing, and conditional testing
          var aChildren = $(".sticky-nav li").children(); // find the a children of the list items
          var aArray = []; // create the empty aArray
          for (var i=0; i < aChildren.length; i++) {    
              var aChild = aChildren[i];
              var ahref = $(aChild).attr('href');
              aArray.push(ahref);
          } // this for loop fills the aArray with attribute href values
          
          $(window).scroll(function(){
              var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
              var windowHeight = $(window).height(); // get the height of the window
              var docHeight = $(document).height();
              
              for (var i=0; i < aArray.length; i++) {
                  var theID = aArray[i];
                  var divPos = $(theID).offset().top; // get the offset of the div from the top of page
                  var divHeight = $(theID).height(); // get the height of the div in question
                  if (windowPos >= (divPos - 1) && windowPos < (divPos + divHeight -1 )) {
                      $("a[href='" + theID + "']").addClass("nav-active");
                  } else {
                      $("a[href='" + theID + "']").removeClass("nav-active");
                  }
              }
              
              if(windowPos + windowHeight == docHeight) {
                  if (!$(".sticky-nav li:last-child a").hasClass("nav-active")) {
                      var navActiveCurrent = $(".nav-active").attr("href");
                      $("a[href='" + navActiveCurrent + "']").removeClass("nav-active");
                      $(".sticky-nav li:last-child a").addClass("nav-active");
                  }
              }
          });
           */
      });

    </script>
  </main>
  
<?php get_footer(); ?>
