<?php
  /* Template Name: 15 in 2015 */
  get_header('oneoffs');

  global $wpdb;

  $fifteen_in_fifteen_events = $wpdb->get_results("SELECT event_location, event_slug, fifteen_in_fifteen, fifteen_in_fifteen_txt FROM wp_rocknroll_events WHERE fifteen_in_fifteen = 1 ORDER BY event_location");
  $banner = get_post_meta( $post->ID, '_rnr3_fifteen_in_fifteen_banner', TRUE );
  $bottom_txt = apply_filters( 'the_content', get_post_meta( $post->ID, '_rnr3_fifteen_in_fifteen_bottom_txt', TRUE ) );

?>
<main role="main" id="main">
  <section class="wrapper">
    <div class="content">
      <img class="banner15" src="<?php echo $banner; ?>" alt="Save $15 in 2015" />
      <?php
        if ( have_posts() ) : while ( have_posts() ) : the_post();
          echo '<div class="intro15">';
            the_content();
          echo '</div>';
        endwhile;
        endif;
        
        $count = 0;
        if($fifteen_in_fifteen_events) {
          echo '<div class="grid_3_fifteen">';
            foreach($fifteen_in_fifteen_events as $event) {
              echo '<div class="column15 center">
                <div class="box15">
                  <h3>'.$event->event_location.'</h3>
                  <p>'.$event->fifteen_in_fifteen_txt.'</p>
                  <a class="cta" href="'.site_url('/' . $event->event_slug .'/register').'">REGISTER NOW</a>
                </div>
              </div>';

              $count++;
            }
          echo '</div>';
        }
        echo '<div class="bottom15">
          '.$bottom_txt.'
        </div>
      </div>

    </section>
  </main>';


get_footer('oneoffs'); ?>