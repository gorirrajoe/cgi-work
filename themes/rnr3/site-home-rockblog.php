<?php
	/*
	 * Template Name: Site Home: The Tempo
	 */
	get_header('oneoffs');

	$page = ( get_query_var('page') ) ? get_query_var('page') : 1;

	$market = get_market2();
	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}
	// is this needed?
	// $parent_slug = the_parent_slug();
	// rnr3_get_secondary_nav( $parent_slug );

	// check language and get our languages
	$qt_lang = rnr3_get_language();
	include 'languages.php';

	// posts to be excluded in the latest posts loop
	$excluded_posts_ids	= array();

	// get featured posts
	if ( false === ( $featured_posts = get_transient( 'tempo_featured_posts' ) ) ) {

		$featured_posts_ids	= array();

		$args = array(
			'post_type'					=> 'post',
			'meta_key'					=> '_rnr3_featured',
			'meta_value'				=> 'on',
			'orderby'					=> 'rand',
			'fields'					=> 'ids',
			'has_password'				=> false,
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false,
			'update_post_term_cache'	=> false
		);

		if ( '' !== $anchored_featured_post_id = get_option( 'anchored_featured_post_id' ) ) {

			array_push( $featured_posts_ids, $anchored_featured_post_id );

			$args['posts_per_page']	= '2';
			$args['post__not_in']	= array( $anchored_featured_post_id );

		} else {
			$args['posts_per_page']	= '3';
		}

		$featured_random_posts = new WP_Query( $args );

		$featured_posts_ids	= array_merge( $featured_posts_ids, $featured_random_posts->posts );

		$featured_posts	= new WP_Query( array(
			'post__in'					=> $featured_posts_ids,
			'orderby'					=> 'post__in',
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false,
			'update_post_term_cache'	=> false
		) );

		set_transient( 'tempo_featured_posts', $featured_posts, 15 * MINUTE_IN_SECONDS );
	}

	$excluded_posts_ids	= array_merge( wp_list_pluck( $featured_posts->posts, 'ID' ), $excluded_posts_ids );

	// get video posts (posts that are labeled as video post format...or maybe we should switch to a category...or have them labeled as both??)
	$args_vid = array(
		'tax_query' => array(
			array(
				'taxonomy'	=> 'post_format',
				'field'		=> 'slug',
				'terms'		=> 'post-format-video',
			)
		)
	);

	$video_posts = new WP_Query( $args_vid );

	$excluded_posts_ids	= array_merge( wp_list_pluck( $video_posts->posts, 'ID' ), $excluded_posts_ids );

	// get latest posts
	if ( false === ( $rb_entries = get_transient( 'tempo_latest_posts' ) ) ) {
		// figure out how many posts we are going to get
		$latest_posts_count	= 8 - $featured_posts->post_count;
		$latest_posts_count	= $latest_posts_count % 2 == 0 ? $latest_posts_count : $latest_posts_count - 1;
		$args = array(
			'posts_per_page'			=> $latest_posts_count,
			'paged'						=> $page,
			'post__not_in'				=> $excluded_posts_ids,
			'has_password'				=> false,
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false,
			'update_post_term_cache'	=> false
		);
		$rb_entries = new WP_Query( $args );
		set_transient( 'tempo_latest_posts', $rb_entries, 15 * MINUTE_IN_SECONDS );
	}
	// since get_next_posts_link() function only works with $wp_query
	$wp_query = $rb_entries;

	// default image if no featured images on posts
	$default_image_url		= get_option( 'default-featured-image' );
	$image_data				= $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE guid='%s';", $default_image_url ) );
	// $image_data				= rnr3_get_image_id( $default_image_url );
	$default_image_thumb	= wp_get_attachment_image( $image_data[0], 'blog-thumb' );
	$default_image_medium	= wp_get_attachment_image( $image_data[0], 'blog-medium' );
	$default_image_large	= wp_get_attachment_image( $image_data[0], 'blog-large' );

	$blog_tagline			= get_bloginfo('description');
?>

	<!-- main content -->
	<main role="main" id="main">

		<section class="wrapper grid_2 offset240left">
			<div class="column archive_list">
				<div class="content">

					<h1><?php echo $welcome_txt; ?></h1>

					<?php if ( $blog_tagline != '' ) { echo '<h4>'. $blog_tagline . '</h4>'; } ?>

					<?php
						// loop through the first item only
						if ( $featured_posts->have_posts() ) : while( $featured_posts->have_posts()) : $featured_posts->the_post();

							if ( $featured_posts->current_post > 0 ) { break; }

							// if this is the first featured post (large post), get information to display it
							$featured_image_info	= get_post( get_post_thumbnail_id() );
							$post_metadata			= get_post_meta( $featured_posts->post->ID );
							$post_title				= !empty( $post_metadata['_short_title'] ) ? $post_metadata['_short_title'][0] : get_the_title();
							$sponsored_image		= !empty( $metadata['_sponsored-content-logo'] ) ? $metadata['_sponsored-content-logo'][0] : '';
							$sponsored_url			= !empty( $metadata['_sponsored-content-url'] ) ? $metadata['_sponsored-content-url'][0] : ''; ?>

							<article <?php post_class( array('article', 'article_type_featured', 'article_type_featured--main') ); ?>>

								<a href="<?php the_permalink(); ?>" class="article__permalink">
									<figure class="article__thumbnail">
										<?php if ( has_post_thumbnail() ) {

											the_post_thumbnail( 'blog-large' );

											echo get_sponsor_label( $post_metadata );

											if ( $featured_image_info->post_excerpt != '' ) {
												printf( '<figcaption>%s</figcaption>', $featured_image_info->post_excerpt );
											}

										} else {
											echo $default_image_large;
										} ?>
									</figure>

									<div class="article__details">
										<h2 class="article__title"><?php echo $post_title; ?></h2>
										<?php echo ( has_excerpt() ) ? '<h5 class="article__subheading">'. $post->post_excerpt .'</h5>' : '' ?>
										<?php echo get_post_metadata(); ?>
										<div class="article__excerpt"><?php echo wp_trim_excerpt(); ?></div>
									</div>

								</a>

							</article>

					<?php
						endwhile; endif;

						$featured_posts->rewind_posts();
					?>

					<?php
						// loop through the other two items if they exist
						if ( $featured_posts->post_count > 1 ) {
							echo '<section class="more-stories more-stories--featured">';
								echo '<h4>'. $more_featured_stories_txt .' / '. $inspire_you_txt .'</h4>';
							if ( $featured_posts->have_posts() ) : while( $featured_posts->have_posts()) : $featured_posts->the_post();

								// skip first item
								if ( $featured_posts->current_post == 0 ) { continue; }
								$post_metadata			= get_post_meta( $featured_posts->post->ID );
								$post_title				= !empty( $post_metadata['_short_title'] ) ? $post_metadata['_short_title'][0] : get_the_title(); ?>

								<article <?php post_class( array('article', 'article_type_featured') ) ?>>

									<a href="<?php the_permalink(); ?>" class="article__permalink">
										<figure class="article__thumbnail">

											<?php if ( has_post_thumbnail() ) {

												the_post_thumbnail( 'blog-thumb' );

												echo get_sponsor_label( $post_metadata );

											} else {
												echo $default_image_thumb;
											} ?>

										</figure>

										<div class="article__details">
											<h3 class="article__title"><?php echo $post_title; ?></h3>
											<?php echo ( $post->post_excerpt != '' ) ? '<h5 class="article__subheading">'. $post->post_excerpt .'</h5>' : '' ?>
											<?php echo get_post_metadata(); ?>
										</div>

									</a>

								</article>

								<?php

							endwhile; endif;
							echo '</section>';
						}

						wp_reset_postdata();
					?>


					<?php
						if ( $rb_entries->have_posts() ) :

							echo '<section class="more-stories more-stories--latest">';
							echo '<h4>'. $latest_posts_txt .'</h4>';

								echo '<div class="more-stories__wrap">';

								while( $rb_entries->have_posts() ) : $rb_entries->the_post();
									$post_metadata	= get_post_meta( get_the_ID() );
									$post_title		= !empty( $post_metadata['_short_title'] ) ? $post_metadata['_short_title'][0] : get_the_title(); ?>

									<article <?php post_class( array('article', 'artilce_type_latest') ) ?>>
										<a href="<?php the_permalink(); ?>" class="article__permalink">

											<figure class="article__thumbnail">

												<?php if ( has_post_thumbnail() ) {

													the_post_thumbnail( 'blog-medium' );

													echo get_sponsor_label( $post_metadata );

												} else {
													echo $default_image_medium;
												} ?>

											</figure>

											<div class="article__details">
												<h3 class="article__title"><?php echo $post_title; ?></h3>
												<?php echo ( $post->post_excerpt != '' ) ? '<h5 class="article__subheading">'. $post->post_excerpt .'</h5>' : '' ?>
												<?php echo get_post_metadata(); ?>
											</div>

										</a>
									</article>

								<?php endwhile;

								echo '</div>';

							echo '</section>';

							wp_reset_postdata();

						endif;
					?>

					<?php
						// loop through video posts
						if ( $video_posts->have_posts() ) :
							echo '<section class="more-stories more-stories--videos">';
								echo '<h4>'. $featured_videos_txt .' / '. $motivate_you_txt .'</h4>';

								while( $video_posts->have_posts() ) : $video_posts->the_post();
									$post_metadata	= get_post_meta( $video_posts->post->ID );
									$post_title		= !empty( $post_metadata['_short_title'] ) ? $post_metadata['_short_title'][0] : get_the_title(); ?>

									<article <?php post_class( array('article', 'article_type_video') ); ?>>

										<a href="<?php the_permalink(); ?>" class="article__permalink">
											<figure class="article__thumbnail">

												<?php if ( has_post_thumbnail() ) {

													the_post_thumbnail( 'blog-thumb' );
													echo get_sponsor_label( $post_metadata );

												} else {

													echo $default_image_thumb;

												} ?>

											</figure>

											<div class="article__details">
												<h3 class="article__title"><?php echo $post_title; ?></h3>
												<?php echo get_post_metadata(); ?>
											</div>
										</a>

									</article>

									<?php
								endwhile;

							echo '</section>';
						endif;
					?>

					<nav class="archive_nav">
						<a class="cta" href="<?php bloginfo('url');?>/all">See all posts</a>
					</nav>

				</div>
			</div>
			<?php get_sidebar('tempo'); ?>

		</section>
	</main>

<?php get_footer('series'); ?>
