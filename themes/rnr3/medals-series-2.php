<?php
	/* Template Name: M's - Medals (Series) - 2 */
	get_header('special');
	$pageid = get_the_ID();
	$prefix = '_rnr3_m_medals_';

	$landing_active	= $finisher_active = $remix_active = $le_active = $hm_active = $hof_active = $charity_active = '';
	$section		= !empty( $_GET['section'] ) ? $_GET['section'] : '';
?>
	<main role="main" id="main" class="no-hero m_medals">

		<?php
			/**
			 * subnav
			 */
		?>
		<nav id="subnav">
			<?php
				if( $section == '' ) {
					$landing_active = 'class="subnav-current-page"';
				} elseif( $section == 'finisher-medals' ) {
					$finisher_active = 'class="subnav-current-page"';
				} elseif( $section == 'remix-challenge' ) {
					$remix_active = 'class="subnav-current-page"';
				} elseif( $section == 'limited-edition' ) {
					$le_active = 'class="subnav-current-page"';
				} elseif( $section == 'heavy-medals' ) {
					$hm_active = 'class="subnav-current-page"';
				} elseif( $section == 'hall-of-fame' ) {
					$hof_active = 'class="subnav-current-page"';
				} elseif( $section == 'charity' ) {
					$charity_active = 'class="subnav-current-page"';
				}
			?>

			<section class="wrapper">
				<div class="subnav-menu-label">Menu</div>
				<div class="subnav-menu-primary-event-container">
					<ul class="subnav-menu">
						<li><a <?php echo $landing_active; ?> href="<?php echo site_url('/medals/'); ?>">Medals</a></li>
						<li><a <?php echo $finisher_active; ?> href="<?php echo site_url('/medals/?section=finisher-medals'); ?>">Finisher Medals</a></li>
						<li><a <?php echo $remix_active; ?> href="<?php echo site_url('/medals/?section=remix-challenge'); ?>">Remix Challenge</a></li>
						<li><a <?php echo $le_active; ?> href="<?php echo site_url('/medals/?section=limited-edition'); ?>">Limited Edition</a></li>
						<li><a <?php echo $hm_active; ?> href="<?php echo site_url('/medals/?section=heavy-medals'); ?>">Heavy Medals</a></li>
						<li><a <?php echo $hof_active; ?> href="<?php echo site_url('/medals/?section=hall-of-fame'); ?>">Hall of Fame</a></li>
						<li><a <?php echo $charity_active; ?> href="<?php echo site_url('/medals/?section=charity'); ?>">Charity</a></li>
					</ul>
				</div>
			</section>

		</nav>


		<?php
			/**
			 * medals landing page
			 */

			if( $section == '' ) { ?>

				<section id="medals-landing">
					<section class="wrapper">

						<div class="narrow">
							<?php echo apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'landing_intro_txt', 1 ) ); ?>
						</div>

						<div class="grid_3_special">

							<?php for( $i = 1; $i < 7; $i++ ) {
								if( get_post_meta( $pageid, $prefix . 'landing_col_section_' . $i, 1 ) != '' ) {
									echo '<div class="column_special">
										<figure><a href="?section='. get_post_meta( $pageid, $prefix . 'landing_col_section_' . $i, 1 ) .'"><img src="'.get_post_meta( $pageid, $prefix . 'landing_col_img_' . $i , 1 ).'"></a></figure>
										'. apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'landing_col_txt_' . $i, 1 ) ) .'
									</div>';
								}

							} ?>

						</div>

						<?php
							$sponsor_middle_txt = get_post_meta( $pageid, $prefix . 'sponsor_middle_txt', 1 ) != '' ? get_post_meta( $pageid, $prefix . 'sponsor_middle_txt', 1 ) : '';

							if( $sponsor_middle_txt != '' ) {
								echo '<div class="narrow">'.
									apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'sponsor_middle_txt', 1 ) ) .'
								</div>';
							}
						?>

					</section>
				</section>

			<?php }

			/**
			 * finisher medals section
			 */

			elseif( $section == 'finisher-medals' ) { ?>

				<section id="medals-panel1">
					<section class="wrapper">

						<div class="narrow">
							<?php echo apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'finisher_intro_txt', 1 ) ); ?>
						</div>

						<div class="controls">
							<label>Sort By</label>

							<button class="sort" data-sort="date:asc">Date</button>
							<button class="sort" data-sort="city:asc">A-Z</button>

						</div>

						<?php
							if ( false === ( $medal_query = get_transient( 'medal_query_results' ) ) ) {
								$args = array(
									'post_type'			=> 'medal',
									'post_status'		=> 'publish',
									'posts_per_page'	=> 100,
									'orderby'			=> 'title',
									'order'				=> 'ASC'
								);
								$medal_query = new WP_Query( $args );
								set_transient( 'medal_query_results', $medal_query, 15 * MINUTE_IN_SECONDS );
							}


							$gallery_count = 0;

							if( $medal_query->have_posts() ) {
								$i = 0;

								echo '<div class="mix_group">';

									while( $medal_query->have_posts() ) {
										$medal_query->the_post();

										$gallery_count++;

										$event_location	= get_the_title();
										$event_url		= get_post_meta( $pageid, '_rnr3_event_url', 1 );
										$event_date		= get_post_meta( $pageid, '_rnr3_event_date', 1 );
										$event_enddate	= get_post_meta( $pageid, '_rnr3_event_enddate', 1 );
										$event_medals	= get_post_meta( $pageid, '_rnr3_racetype_group', 1 );

										if( $event_date != $event_enddate && $event_enddate != '' ) {
											if( date( 'M', $event_date ) != date( 'M', $event_enddate ) ) {
												$event_monthdate	= date( 'M d', $event_date ).'-'.date( 'M d', $event_enddate );
												$event_year			= date( 'Y', $event_enddate );
											} else {
												$event_monthdate	= date( 'M d', $event_date ).'-'.date( 'd', $event_enddate );
												$event_year			= date( 'Y', $event_enddate );
											}
										} else {
											$event_monthdate	= date( 'M d', $event_date );
											$event_year			= date( 'Y', $event_date );
										}

										if( count( $event_medals ) == 1 ) {
											$carousel_or_no = ' class="no_carousel"';
										} else {
											$carousel_or_no = '';
										}

										echo '<div class="mix" data-city="'. str_replace( ' ', '-', strtolower( $event_location ) ) .'" data-date="'. $event_enddate .'">

											<div class="inner_mix">
												<h3>
													'. $event_location .'
													<span>'. $event_monthdate .', '. $event_year .'</span>
												</h3>';

												if( $event_medals != '' ) {

													echo '<div class="owl-carousel" id="gallery-carousel-'. $gallery_count .'">';

														foreach( $event_medals as $key => $entry ) {

															$medal_year = ( array_key_exists( 'year', $entry ) && $entry['year'] != '' ) ? $entry['year'] . ' ' : '' ;

															if( isset( $entry['image'] ) ) {
																$medal_id		= rnr3_get_image_id( $entry['image'] );
																$medal_thumb	= wp_get_attachment_image_src( $medal_id, 'finisher-medals' );

																echo '<div>
																	<figure><a href="'. $entry['image'] .'" class="fancybox" data-caption="Finisher Medals - '. $event_location .' - '. $entry['_rnr3_race_type'] . '" data-fancybox="medal-'. $medal_query->current_post .'"><img alt="'. $event_location .' - '. $entry['_rnr3_race_type'] . '" src="'. $medal_thumb[0] .'"></a></figure>';

																	if( $entry['_rnr3_race_type'] == 'Virtual Run' ) {
																		echo '<p'. $carousel_or_no .'>'. $medal_year . $entry['vr_medal_name'] .' Medal</p>';
																	} else {
																		echo '<p'. $carousel_or_no .'>'. $medal_year . $entry['_rnr3_race_type'] .' Medal</p>';
																	}

																echo '</div>';

															}
														}

													echo '</div>';

												}
												echo '<p><a class="cta" href="'. $event_url .'">Go To Race</a></p>
											</div>
										</div>';

										$i++;
									}
								echo '</div>';
							}
							wp_reset_postdata();

							echo '<div class="gap"></div>
							<div class="gap"></div>
							<div class="gap"></div>
							<div class="gap"></div>';
						?>

						<script src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/js/vendor/mixitup.min.js"></script>
						<script>
							var containerEl = document.querySelector('.mix_group');

							var mixer = mixitup(containerEl, {
								load: {
									sort: 'date:asc'
								}
							});
						</script>

					</section>
				</section>

				<script>
					$(document).ready(function(){

							/**
							 * This part causes smooth scrolling using scrollto.js
							 * We target all a tags inside the nav, and apply the scrollto.js to it.
							 */
							$("#secondary a").click(function(evn){
									evn.preventDefault();
									$('html,body').scrollTo(this.hash, this.hash);
							});

							<?php for( $i = 1; $i <= $gallery_count; $i++ ) { ?>
								$('#gallery-carousel-<?php echo $i; ?>').owlCarousel({
									singleItem : true,
									autoPlay : false,
									navigation : true,
									navigationText : ["prev", "next"],
									pagination : true,
									lazyLoad : true
								});

							<?php } ?>

					});

				</script>

			<?php }

			/**
			 * remix challenge section
			 */

			elseif( $section == 'remix-challenge' ) { ?>

				<section id="remix-landing">
					<section class="wrapper">

						<div class="narrow">
							<?php echo apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'remix_intro_txt', 1 ) ); ?>
						</div>

						<div id="bling-carousel" class="owl-carousel">
							<?php
								$remix_medals = get_post_meta( $pageid, $prefix . 'remix_medals', 1 );

								foreach( $remix_medals as $key => $entry ) {
									echo '<div>
										<figure><a href="'. $entry['image'] .'" class="fancybox" data-fancybox="remix-challenge" data-caption="Remix Challenge - '. $entry['location'] .'"><img alt="" src="'. $entry['image'] .'"></a></figure>
										<h3><a href="'. $entry['image'] .'" class="fancybox">'. $entry['location'] .'</a></h3>

									</div>';
								}
							?>
						</div>

						<div class="narrow">
							<?php echo apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'remix_txt', 1 ) ); ?>
						</div>

					</section>
				</section>

			<?php }

			/**
			 * limited edition section
			 */

			elseif( $section == 'limited-edition' ) { ?>

				<section id="limited-edition-landing">
					<section class="wrapper">

						<div class="narrow">
							<?php echo apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'limited_intro_txt', 1 ) ); ?>
						</div>

						<div id="bling-carousel" class="owl-carousel">

							<?php
								$limited_medals	= get_post_meta( $pageid, $prefix . 'limited_medals', 1 );
								$i				= 0;

								foreach( $limited_medals as $key => $entry ) {

									echo '<div>

										<figure>
											<a class="fancybox" data-fancybox="limited-edition-gallery" data-src="#medal_info_'.$i.'" href="javascript:;"><img alt="" src="'. $entry['image'] .'"></a>
										</figure>
										<h3>'. $entry['name'] .'</h3>
										<p>Click medal for more info</p>
										<div style="display: none; max-width: 90%; width: 560px;" class="medal_info" id="medal_info_'.$i.'">
											<figure>
												<img src="'. $entry['image'] .'">
												<figcaption>
													<strong>Limited Edition - '. $entry['name'] . '</strong><br>' .
													$entry['description'] .'
												</figcaption>
											</figure>
										</div>

									</div>';
									$i++;

								}
							?>

						</div>

						<div class="narrow">
							<?php echo apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'limited_txt', 1 ) ); ?>
						</div>

					</section>
				</section>

			<?php }

			/**
			 * heavy medals section
			 */

			elseif( $section == 'heavy-medals' ) { ?>

				<section id="heavy-medals-landing">
					<section class="wrapper">

						<div class="narrow">
							<?php echo apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'heavymedals_intro_txt', 1 ) ); ?>
						</div>

						<div id="bling-carousel" class="owl-carousel">

							<?php
								$heavy_medals = get_post_meta( $pageid, $prefix . 'heavy_medals', 1 );

								foreach ( $heavy_medals as $key => $entry ) {
									$entry_image	= !empty( $entry['image'] ) ? $entry['image'] : '';
									$entry_name		= !empty( $entry['name'] ) ? $entry['name'] : '';
									$entry_quals	= !empty( $entry['qualifications'] ) ? $entry['qualifications'] : '';
									$entry_caption	= !empty( $entry['caption'] ) ? $entry['caption'] : ''; ?>

									<div>
										<a class="fancybox" data-caption="Heavy Medals - <?php echo $entry_name . '<br>' . $entry_quals; ?>" href="<?php echo $entry_image; ?>" data-fancybox="heavy-gallery" >
											<figure>
												<img alt="" src="<?php echo $entry_image; ?>">
												<?php echo $entry_caption != '' ? '<figcaption style="display: none;" id ="medal_info_'.$key.'">'. $entry_caption .'</figcaption>' : ''; ?>
											</figure>
											<h3><?php echo $entry_name; ?></h3>
											<p><?php echo $entry_quals; ?></p>
										</a>
									</div>

								<?php }
							?>

						</div>


						<?php
							$perks = get_post_meta( $pageid, $prefix . 'perks_group', 1 ) != '' ? get_post_meta( $pageid, $prefix . 'perks_group', 1 ) : '';

							if( $perks != '' ) {

								$events_three = $events_six = $events_ten = $events_fifteen = array();

								$perks_intro = get_post_meta( $pageid, $prefix . 'perks_group_intro', 1 ) != '' ? apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'perks_group_intro', 1 ) ) .'</h2>' : '';


								echo $perks_intro .'

								<table class="responsive_table heavy_medals_table" cellspacing="0" cellpadding="0" border="0">
									<thead>
										<tr>
											<th></th>
											<th class="vip_hdr">3 Events</th>
											<th class="vip_hdr">6 Events</th>
											<th class="vip_hdr">10 Events</th>
											<th class="vip_hdr">15 Events</th>
										</tr>
									</thead>
									<tbody>';

										foreach( $perks as $key => $entry ) {

											${$key . 'perk_desc'} = isset( $entry['perk_desc'] ) && $entry['perk_desc'] != '' ? '<a href="#" class="toggle"><span class="icon-help-circled"></span></a><div class="vip_tooltip">'. $entry['perk_desc'] .'</div>' : '';

											echo '<tr>
												<td class="vip_perk">'.
													$entry['perk_name'] . ${$key . 'perk_desc'} . '
												</td>';

												if( in_array( 'events3', $entry['perk_benchmark'] ) ) {
													echo '<td><span class="icon-ok-circled"></span></td>';
													$events_three[] = $entry['perk_name'];
												} else {
													echo '<td></td>';
												}

												if( in_array( 'events6', $entry['perk_benchmark'] ) ) {
													echo '<td><span class="icon-ok-circled"></span></td>';
													$events_six[] = $entry['perk_name'];
												} else {
													echo '<td></td>';
												}

												if( in_array( 'events10', $entry['perk_benchmark'] ) ) {
													echo '<td><span class="icon-ok-circled"></span></td>';
													$events_ten[] = $entry['perk_name'];
												} else {
													echo '<td></td>';
												}

												if( in_array( 'events15', $entry['perk_benchmark'] ) ) {
													echo '<td><span class="icon-ok-circled"></span></td>';
													$events_fifteen[] = $entry['perk_name'];
												} else {
													echo '<td></td>';
												}

											echo '</tr>';

										}

									echo '</tbody>
								</table>';

								$event_count_array = array(
									'three'		=> '3 Events',
									'six'		=> '6 Events',
									'ten'		=> '10 Events',
									'fifteen'	=> '15 Events',
								); ?>


								<div id="vip_tabs">
									<ul class="vip_tabs_nav">
										<li><a href="#tabs-three">3 Events</a></li>
										<li><a href="#tabs-six">6 Events</a></li>
										<li><a href="#tabs-ten">10 Events</a></li>
										<li><a href="#tabs-fifteen">15 Events</a></li>
									</ul>

									<?php
										foreach( $event_count_array as $key => $value ) {

											echo '<div id="tabs-'. $key .'">
												<h3>'. $value .'</h3>
												<ul>';

													foreach( ${'events_'. $key} as $key => $perk ) {

														echo '<li>'.
															$perk . ${$key . 'perk_desc'} .'
														</li>';

													}

												echo '</ul>
											</div>';

										}
									?>
								</div>

							<?php }
						?>


						<div class="narrow">
							<?php echo apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'heavymedals_txt', 1 ) ); ?>
						</div>

					</section>
				</section>

				<script>
					$(function() {
						$('#vip_tabs').tabs();
					});

					$(document).ready(function() {
						$(".toggle").click(function(e){
							$(this).nextAll(".vip_tooltip").first().slideToggle();
							e.preventDefault();
						});
					});
				</script>

			<?php }

			/**
			 * hall of fame section
			 */

			elseif( $section == 'hall-of-fame' ) { ?>

				<section id="hof-landing">
					<section class="wrapper">

						<div class="narrow">
							<?php echo apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'hof_intro_2017_txt', 1 ) ); ?>
						</div>

						<section class="">

							<h2 class="center">2017 Heavy Medals Hall of Famers</h2>

							<?php
								$name = array();

								/**
								 * create a transient for the mega meta group
								 * there will be nearly 200, so best to transient the meta AND its resulting sort
								 */
								if ( false === ( $hofers = get_transient( 'hofers_meta' ) ) ) {
									$hofers	= get_post_meta( $pageid, $prefix . 'hof_2017_group', 1 ) != '' ? get_post_meta( $pageid, $prefix . 'hof_2017_group', 1 ) : '';

									if( $hofers != '' ) {

										foreach( $hofers as $key => $row ) {
											$name[$key] = $row['hofer'];
										}

										// sort by name so names can be added on the back-end in any order
										array_multisort( $name, SORT_ASC, $hofers );
									}

									set_transient( 'hofers_meta', $hofers, 15 * MINUTE_IN_SECONDS );
								}

								if( $hofers != '' ) {

									echo '<div class="grid_4_special">';

										foreach( $hofers as $key => $entry ) {
											echo '<div class="column_special">
												<figure>
													<img class="lazy" data-original="'. $entry['image'] .'">
												</figure>
												<span>'. $entry['hofer'] .'</span>
											</div>';
										}

									echo '</div>';

								}

								// not pictured
								$hofers_np	= get_post_meta( $pageid, $prefix . 'hof_np_2017_group', 1 ) != '' ? get_post_meta( $pageid, $prefix . 'hof_np_2017_group', 1 ) : '';
								$count		= 0;

								if( $hofers_np != '' ) {

									echo '<h2 class="center">Not Pictured</h2>
									<div class="grid_4_special">
										<div class="column_special">';

											foreach( $hofers_np as $key => $entry ) {

												if( $count != 0 && $count % 3 == 0 ) {
													echo '</div>
													<div class="column_special">';
												}

												echo '<p>'. $entry['hofer'] .'</p>';

												$count++;

											}

										echo '</div>
									</div>';

								}


							?>

							<p class="center"><a href="<?php echo site_url('/medals/?section=hall-of-fame-2014'); ?>">Click here to see the 2014 Heavy Medals Hall of Famers</a></p>
							<p class="center"><a href="<?php echo site_url('/medals/?section=hall-of-fame-2015'); ?>">Click here to see the 2015 Heavy Medals Hall of Famers</a></p>
							<p class="center"><a href="<?php echo site_url('/medals/?section=hall-of-fame-2016'); ?>">Click here to see the 2016 Heavy Medals Hall of Famers</a></p>

						</section>

						<div class="narrow">
							<?php echo apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'hofmedals_txt', 1 ) ); ?>
						</div>

					</section>
				</section>

			<?php }

			/**
			 * hall of fame 2016 section
			 */

			elseif( $section == 'hall-of-fame-2016' ) { ?>

				<section id="hof-landing">
					<section class="wrapper">

						<div class="narrow">
							<?php echo apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'hof_intro_2016_txt', 1 ) ); ?>
						</div>

						<section class="blinggroup center">

							<h2>2016 Heavy Medals Hall of Famers</h2>

							<div class="grid_4_special">
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Oba-Adewunmi.jpg">
									</figure>
									<span>Obafemi A.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Hyalker-Amaral.jpg">
									</figure>
									<span>Hyalker A.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/John-Anderson.jpg">
									</figure>
									<span>John A.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/K-B.jpg">
									</figure>
									<span>Kelly B.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Brian-Barnes.jpg">
									</figure>
									<span>Brian B.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Karen-Barth.jpg">
									</figure>
									<span>Karen B.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Noah-Batkin.jpg">
									</figure>
									<span>Noah B.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Michael-Bauer.jpg">
									</figure>
									<span>Michael B.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Melissa-Beasley.jpg">
									</figure>
									<span>Melissa B.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Kristal-Betanzo.jpg">
									</figure>
									<span>Kristal B.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Jason-Bonenfant.jpg">
									</figure>
									<span>Jason B.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Laura-Braunbeck.jpg">
									</figure>
									<span>Laura B.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Gisela-Bravo.jpg">
									</figure>
									<span>Gisela B.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Karen-Bravo.jpg">
									</figure>
									<span>Karen B.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Justin-Bravo.jpg">
									</figure>
									<span>Justin B.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Breanna-Bringer.jpg">
									</figure>
									<span>Breanna B.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Trin-Brocato.jpg">
									</figure>
									<span>Trinity B.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Jeff-Calene.jpg">
									</figure>
									<span>Jeff C.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Jennifer-Canale.jpg">
									</figure>
									<span>Jennifer C.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Nikki-Cannon.jpg">
									</figure>
									<span>Nicole C.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Susan-Carino.jpg">
									</figure>
									<span>Susan C.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Ron-Carino.jpg">
									</figure>
									<span>Ronald C.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Lucero-Carmona.jpg">
									</figure>
									<span>Lucero C.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Wesley-Cho.jpg">
									</figure>
									<span>Wesley C.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Kerianne-Christie.jpg">
									</figure>
									<span>Kerianne C.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Charlie-Cory.jpg">
									</figure>
									<span>Charles C.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Lisa-Costello.jpg">
									</figure>
									<span>Lisa C.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Jeremy-Cox.jpg">
									</figure>
									<span>Jeremy C.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Anthony-Cruz.jpg">
									</figure>
									<span>Anthony C.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Nichole-Curtis.jpg">
									</figure>
									<span>Nichole C.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Natalie-DaSilva.jpg">
									</figure>
									<span>Natalie D.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Cory-Daviau.jpg">
									</figure>
									<span>Cory D.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Mariel-David.jpg">
									</figure>
									<span>Mariel D.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Joel-Davis.jpg">
									</figure>
									<span>Joel D.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Marissa-De-Luna.jpg">
									</figure>
									<span>Marissa D.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/DL-Dean.jpg">
									</figure>
									<span>DL D.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Elena-Debler.jpg">
									</figure>
									<span>Elena D.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Almi-Del-Villar.jpg">
									</figure>
									<span>Almi D.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Dominique-Diaz.jpg">
									</figure>
									<span>Dominique D.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Heather-DuBey.jpg">
									</figure>
									<span>Heather D.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Mickael-Dubois.jpg">
									</figure>
									<span>Mickael D.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Arlene-Eliano.jpg">
									</figure>
									<span>Arlene E.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Michelle-Ellis.jpg">
									</figure>
									<span>Michelle E.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Larry-Ellsworth.jpg">
									</figure>
									<span>Larry E.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Mike-Engberg.jpg">
									</figure>
									<span>Michael E.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Courtney-Beth-Enloe.jpg">
									</figure>
									<span>Courtney Beth E.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Aidin-Esparza.jpg">
									</figure>
									<span>Aidin E.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Brian-Evans.jpg">
									</figure>
									<span>Brian E.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/William-Flynn.jpg">
									</figure>
									<span>William F.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Chris-Foo.jpg">
									</figure>
									<span>Chris F.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Rebecca-Frazier.jpg">
									</figure>
									<span>Rebecca F.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Monique-French.jpg">
									</figure>
									<span>Monique F.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Brittney-Garza.jpg">
									</figure>
									<span>Brittney G.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Rebecca-Gentrup.jpg">
									</figure>
									<span>Rebecca G.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Mitchell-Ginsburg.jpg">
									</figure>
									<span>Mitchell G.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Veronica-Gongora.jpg">
									</figure>
									<span>Veronica G.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Kevin-Gonzalez.jpg">
									</figure>
									<span>Kevin G.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Alan-Granza.jpg">
									</figure>
									<span>Alan G.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Dora-Gutierrez.jpg">
									</figure>
									<span>Dora G.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Oscar-Gutierrez.jpg">
									</figure>
									<span>Oscar G.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Dan-Hagarty.jpg">
									</figure>
									<span>Daniel H.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Hope-Harms.jpg">
									</figure>
									<span>Hope H.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Larry-Harris.jpg">
									</figure>
									<span>Larry H.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Joe-Harris.jpg">
									</figure>
									<span>Joseph H.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/David-Hawkins.jpg">
									</figure>
									<span>David H.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Thomas-Hay.jpg">
									</figure>
									<span>Thomas H.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Jared-Hegvold.jpg">
									</figure>
									<span>Jared H.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Greg-Heilers.jpg">
									</figure>
									<span>Greg H.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Fred-Henninger.jpg">
									</figure>
									<span>Fred H.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/jose-hernandez.jpg">
									</figure>
									<span>Jose H.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Dariel-Hernandez.jpg">
									</figure>
									<span>Dariel H.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Amy-Heveran.jpg">
									</figure>
									<span>Amy H.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Jamie-Heveran.jpg">
									</figure>
									<span>Jamie H.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Kristy-Hill.jpg">
									</figure>
									<span>Kristy H.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Steve-Horwood.jpg">
									</figure>
									<span>Steve H.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Elaine-Hu.jpg">
									</figure>
									<span>Elaine H.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Ryan-Ibarra.jpg">
									</figure>
									<span>Ryan I.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Ardee-Jagt.jpg">
									</figure>
									<span>Ardee J.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Jayson-Danilo.jpg">
									</figure>
									<span>Danilo J.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Samantha-Killeen.jpg">
									</figure>
									<span>Samantha K.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Mike-Kimball.jpg">
									</figure>
									<span>Mike K.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Sean-Kinne.jpg">
									</figure>
									<span>Sean K.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Tim-Kovach.jpg">
									</figure>
									<span>Timothy K.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Shirley-Lam.jpg">
									</figure>
									<span>Shirley L.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Melinda-Langlois.jpg">
									</figure>
									<span>Melinda L.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/John-Laputz.jpg">
									</figure>
									<span>John L.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Deborah-Lazaroff.jpg">
									</figure>
									<span>Deborah L.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/James-Levins.jpg">
									</figure>
									<span>James L.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Tee-Lewis.jpg">
									</figure>
									<span>Kteetra L.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Laura-Lindsey.jpg">
									</figure>
									<span>Laura L.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Brian-Lombardi.jpg">
									</figure>
									<span>Brian L.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Jen-MacPherson.jpg">
									</figure>
									<span>Jen M.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Steve-Maddox.jpg">
									</figure>
									<span>Steve M.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Henry-Magalong.jpg">
									</figure>
									<span>Henry M.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Adrian-Mauricio.jpg">
									</figure>
									<span>Adrian M.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Mary-May.jpg">
									</figure>
									<span>Mary M.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Carole-McCluskey.jpg">
									</figure>
									<span>Carole M.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/James-McCoy.jpg">
									</figure>
									<span>James M.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Tiki-Miller.jpg">
									</figure>
									<span>Tiki M.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Joseph-Moore.jpg">
									</figure>
									<span>Joseph M.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Rodney-Moorhead.jpg">
									</figure>
									<span>Rodney M.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/John-Moyer.jpg">
									</figure>
									<span>John M.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Tanya-Murnock.jpg">
									</figure>
									<span>Tanya M.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Jennifer-Ngo.jpg">
									</figure>
									<span>Jennifer N.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Dennis-O'Connell.jpg">
									</figure>
									<span>Dennis O.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Andrew-Ogan.jpg">
									</figure>
									<span>Andrew O.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Rafael-Ortiz.jpg">
									</figure>
									<span>Rafael O.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Paula-Palacios.jpg">
									</figure>
									<span>Paula P.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Kelly-Paul.jpg">
									</figure>
									<span>Kelly P.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Aaron-Peppers.jpg">
									</figure>
									<span>Aaron P.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Patrick-Planeaux.jpg">
									</figure>
									<span>Patrick P.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Al--Hernandez.jpg">
									</figure>
									<span>Gonzalo P.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Kristen-Preyer.jpg">
									</figure>
									<span>Kristen P.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Richard-Quon.jpg">
									</figure>
									<span>Richard Q.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Gima-Ramirez.jpg">
									</figure>
									<span>Gima R.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Victor-Ramirez.jpg">
									</figure>
									<span>Victor R.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Cassirae-Renteria.jpg">
									</figure>
									<span>Cassirae R.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Sherry-Ricker.jpg">
									</figure>
									<span>Sherry R.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Brooks-Roach.jpg">
									</figure>
									<span>Brooks R.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Josie-Robarge.jpg">
									</figure>
									<span>Josie R.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Josh-Rubino.jpg">
									</figure>
									<span>Josh R.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Erika-Santoyo.jpg">
									</figure>
									<span>Erika S.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/John-Schmidt.jpg">
									</figure>
									<span>John S.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Laura-Schwenkel.jpg">
									</figure>
									<span>Laura S.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Briana-Sharp.jpg">
									</figure>
									<span>Briana S.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Jon-Shigematsu.jpg">
									</figure>
									<span>Jon S.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Kamika-Smith.jpg">
									</figure>
									<span>Kamika S.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Julie-Smith.jpg">
									</figure>
									<span>Julie S.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Wm-Bart-Sparks.jpg">
									</figure>
									<span>Wm Bart S.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Alan-Squyres.jpg">
									</figure>
									<span>Alan S.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Stiliyana-Stamenova.jpg">
									</figure>
									<span>Stiliyana S.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Arthur-Sterkenburg.jpg">
									</figure>
									<span>Arthur S.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/David-Stout.jpg">
									</figure>
									<span>David S.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Charles-Taylor.jpg">
									</figure>
									<span>Chuck T.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Christina-Thomas.jpg">
									</figure>
									<span>Christina T.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/David-Toellner.jpg">
									</figure>
									<span>David T.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Reginald-Tonge.jpg">
									</figure>
									<span>Reginald H T.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Taryn-Tyler.jpg">
									</figure>
									<span>Taryn T.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Elliot-Uhlig.jpg">
									</figure>
									<span>Elliot U.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Jun-Ulama.jpg">
									</figure>
									<span>Sindatuk Jun U.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Jorge-Valdez.jpg">
									</figure>
									<span>Jorge V.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Jessica-Valmonte.jpg">
									</figure>
									<span>Jessica V.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Emily-Van-Gundy.jpg">
									</figure>
									<span>Emily V.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Quinton-Van-Gundy.jpg">
									</figure>
									<span>Quinton V.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Chuck-VanDenBossche.jpg">
									</figure>
									<span>Chuck V.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Margo-VanDenBossche.jpg">
									</figure>
									<span>Margo V.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/David-Vasquez-Jr.jpg">
									</figure>
									<span>David V.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Eduardo-Villavicencio.jpg">
									</figure>
									<span>Eduardo V.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/John-Wesendunk.jpg">
									</figure>
									<span>John W.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Odiesha-Williams.jpg">
									</figure>
									<span>Odiesha W.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Patricia-Williams.jpg">
									</figure>
									<span>Patricia W.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Eric-Woo.jpg">
									</figure>
									<span>Eric W.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Sean-Yoder.jpg">
									</figure>
									<span>Sean Y.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Cristine-Zacher.jpg">
									</figure>
									<span>Cristine Z.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Kevin-Zhou.jpg">
									</figure>
									<span>Kevin Z.</span>
								</div>
								<div class="column_special">
									<figure>
										<img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016/Zachary-Zubiate.jpg">
									</figure>
									<span>Zachary Z.</span>
								</div>
							</div>


							<h2>Not Pictured</h2>
							<div class="grid_4_special">
								<div class="column_special">
									<p>Mike N.</p>
									<p>Matthew F.</p>
									<p>Nathaniel D.</p>
									<p>Donald L.</p>
								</div>
								<div class="column_special">
									<p>Terri R.</p>
									<p>Cary Y.</p>
									<p>Dan K.</p>
								</div>
							</div>

							<p class="center"><a href="<?php echo site_url('/medals/?section=hall-of-fame-2014'); ?>">Click here to see the 2014 Heavy Medals Hall of Famers</a></p>
							<p class="center"><a href="<?php echo site_url('/medals/?section=hall-of-fame-2015'); ?>">Click here to see the 2015 Heavy Medals Hall of Famers</a></p>

						</section>
					</section>
				</section>

			<?php }

			/**
			 * hall of fame 2015 section
			 */

			elseif( $section == 'hall-of-fame-2015' ) { ?>

				<section id="hof-landing">
					<section class="wrapper">


						<div class="narrow">
							<?php echo apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'hof_intro_2015_txt', 1 ) ); ?>
						</div>

						<section class="blinggroup center">

							<h2>2015 Heavy Medals Hall of Famers</h2>

							<div class="grid_4_special">
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Aidin-E.jpg"></figure>
									<span>Aidin E.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Ainsley-T.jpg"></figure>
									<span>Ainsley T.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Al-H.jpg"></figure>
									<span>Al H.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Alan-L.jpg"></figure>
									<span>Alan L.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Amy-H.jpg"></figure>
									<span>Amy H.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Andrea-C.jpg"></figure>
									<span>Andrea C.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Andrew-B.jpg"></figure>
									<span>Andrew B.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Annie-B.jpg"></figure>
									<span>Annie B.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Anthony-L.jpg"></figure>
									<span>Anthony L.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Arlene-E.jpg"></figure>
									<span>Arlene E.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Arnold-L.jpg"></figure>
									<span>Arnold L.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Barry-C.jpg"></figure>
									<span>Barry C.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Beth-D.jpg"></figure>
									<span>Beth D.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Bill-K.jpg"></figure>
									<span>Bill K.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Bradley-C.jpg"></figure>
									<span>Bradley C.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Brittany-C.jpg"></figure>
									<span>Brittany C.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Bruce-D.jpg"></figure>
									<span>Bruce D.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Carl-M.jpg"></figure>
									<span>Carl M.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Charles-P.jpg"></figure>
									<span>Charles P.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Charles-V.jpg"></figure>
									<span>Charles V.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Colleen-D.jpg"></figure>
									<span>Colleen D.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Courtney-P.jpg"></figure>
									<span>Courtney P.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Darryl-W.jpg"></figure>
									<span>Darryl W.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Deborah-L.jpg"></figure>
									<span>Deborah L.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Diana-B.jpg"></figure>
									<span>Diana B.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Elaine-H.jpg"></figure>
									<span>Elaine H.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Elena-D.jpg"></figure>
									<span>Elena D.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Emily-S.jpg"></figure>
									<span>Emily S.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Fred-H.jpg"></figure>
									<span>Fred H.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Gene-E.jpg"></figure>
									<span>Gene E.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Greg-H.jpg"></figure>
									<span>Greg H.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Hugh-H.jpg"></figure>
									<span>Hugh H.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Hyalker-A.jpg"></figure>
									<span>Hyalker A.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Ilona-M.jpg"></figure>
									<span>Ilona M.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Ivy-M.jpg"></figure>
									<span>Ivy M.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/James-T.jpg"></figure>
									<span>James T.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Jared-S.jpg"></figure>
									<span>Jared S.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Jennifer-K.jpg"></figure>
									<span>Jennifer K.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Jessica-V.jpg"></figure>
									<span>Jessica V.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Jill-O.jpg"></figure>
									<span>Jill O.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Jim-A.jpg"></figure>
									<span>Jim A.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Jim-D.jpg"></figure>
									<span>Jim D.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Joe-H.jpg"></figure>
									<span>Joe H.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Joe-P.jpg"></figure>
									<span>Joe P.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/John-A.jpg"></figure>
									<span>John A.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/John-P.jpg"></figure>
									<span>John P.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/John-W.jpg"></figure>
									<span>John W.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Juan-A.jpg"></figure>
									<span>Juan A.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Justin-B.jpg"></figure>
									<span>Justin B.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Justin-L.jpg"></figure>
									<span>Justin L.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Kamika-S.jpg"></figure>
									<span>Kamika S.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Karen-B.jpg"></figure>
									<span>Karen B.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Kevin-G.jpg"></figure>
									<span>Kevin G.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Kristin-M.jpg"></figure>
									<span>Kristin M.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Lanel-M.jpg"></figure>
									<span>Lanel M.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Laytoamb-W.jpg"></figure>
									<span>Laytoamb W.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Leah-R.jpg"></figure>
									<span>Leah R.<br>Top Hall of Famer - 23 events</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Leonard-M.jpg"></figure>
									<span>Leonard M.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Maria-M.jpg"></figure>
									<span>Maria M.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Martin-E.jpg"></figure>
									<span>Martin E.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Melinda-L.jpg"></figure>
									<span>Melinda L.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/MG-M.jpg"></figure>
									<span>M.G. M.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Michelle-B.jpg"></figure>
									<span>Michelle B.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Michelle-J.jpg"></figure>
									<span>Michelle J.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Mitch-G.jpg"></figure>
									<span>Mitch G.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Monique-F.jpg"></figure>
									<span>Monique F.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Mwenyewe-D.jpg"></figure>
									<span>Mwenyewe D.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Nichole-C.jpg"></figure>
									<span>Nichole C.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Nicole-M.jpg"></figure>
									<span>Nicole M.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Nicole-S.jpg"></figure>
									<span>Nicole S.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Paul-V.jpg"></figure>
									<span>Paul V.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Randi-Z.jpg"></figure>
									<span>Randi Z.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Richard-O.jpg"></figure>
									<span>Richard O.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Richard-S.jpg"></figure>
									<span>Richard S.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Rodney-M.jpg"></figure>
									<span>Rodney M.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Ronald-C.jpg"></figure>
									<span>Ronald C.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Ron-C.jpg"></figure>
									<span>Ron C.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Ropizah-E.jpg"></figure>
									<span>Ropizah E.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Ryiah-N.jpg"></figure>
									<span>Ryiah N.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Shannon-W.jpg"></figure>
									<span>Shannon W.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Sherry-R.jpg"></figure>
									<span>Sherry R.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Susan-C.jpg"></figure>
									<span>Susan C.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Susan-H.jpg"></figure>
									<span>Susan H.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Sylvia-M.jpg"></figure>
									<span>Sylvia M.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Tammi-M.jpg"></figure>
									<span>Tammi M.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Ted-R.jpg"></figure>
									<span>Ted R.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Tiki-M.jpg"></figure>
									<span>Tiki M.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Tim-D.jpg"></figure>
									<span>Tim D.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Veronica-G.jpg"></figure>
									<span>Veronica G.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015/Wesley-S.jpg"></figure>
									<span>Wesley S.</span>
								</div>
								<div class="column_special">
									<p>Alex S.</p>
									<p>Bonnie Q.</p>
									<p>Catherine G.</p>
									<p>Dena M.</p>
								</div>
								<div class="column_special">
									<p>Derek O.</p>
									<p>Joshua M.</p>
									<p>Mike N.</p>
								</div>
							</div>

							<p class="center"><a href="<?php echo site_url('/medals/?section=hall-of-fame-2014'); ?>">Click here to see the 2014 Heavy Medals Hall of Famers</a></p>
							<p class="center"><a href="<?php echo site_url('/medals/?section=hall-of-fame'); ?>">Click here to see the 2016 Heavy Medals Hall of Famers</a></p>

						</section>
					</section>
				</section>

			<?php }

			/**
			 * hall of fame 2014 section
			 */

			elseif( $section == 'hall-of-fame-2014' ) { ?>

				<section id="hof-landing">
					<section class="wrapper">

						<div class="narrow">
							<?php echo apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'hof_intro_2014_txt', 1 ) ); ?>
						</div>

						<section class="blinggroup center">

							<h2>2014 Heavy Medals Hall of Famers</h2>

							<div class="grid_4_special">
								<div class="column_special">
									<figure><img class="lazy" alt="Arnold-L" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Arnold-L.jpg"></figure>
									<span>Arnold L.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Callie-U" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Callie-U.jpg"></figure>
									<span>Callie U.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Carl M." data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Carl-M.jpg"></figure>
									<span>Carl M.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Christopher-S" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Christopher-S.jpg"></figure>
									<span>Christopher S.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="David-H" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/David-H.jpg"></figure>
									<span>David H.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Heather-D" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Heather-D.jpg"></figure>
									<span>Heather D.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Hyun-Joo-P" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Hyun-Joo-P.jpg"></figure>
									<span>Hyun Joo P.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Ilona-M" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Ilona-M.jpg"></figure>
									<span>Ilona M.
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Jade-Shyree-K" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Jade-Shyree-K.jpg"></figure>
									<span>Jade Shyree K.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Jessica V" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Jessica-V.jpg"></figure>
									<span>Jessica V.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Jocelin-R" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Jocelin-R.jpg"></figure>
									<span>Jocelin R.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Joseph-H" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Joseph-H.jpg"></figure>
									<span>Joseph H.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Juan-A" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Juan-A.jpg"></figure>
									<span>Juan A.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Justin-B" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Justin-B.jpg"></figure>
									<span>Justin B.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Kelvin-S" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Kelvin-S.jpg"></figure>
									<span>Kelvin S.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="KristinP-3" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/KristinP-3.jpg"></figure>
									<span>Kristin P.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Luzviminda-V" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Luzviminda-V.jpg"></figure>
									<span>Luzviminda V.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="MartinE-2" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Martin-E.jpg"></figure>
									<span>Martin E.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Michael-H" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Michael-H.jpg"></figure>
									<span>Michael H.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Mindy-E" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Mindy-E.jpg"></figure>
									<span>Mindy E.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Mitchell-G" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Mitchell-G.jpg"></figure>
									<span>Mitchell G.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Rodney-M" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Rodney-M.jpg"></figure>
									<span>Rodney M.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Ronald C" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Ronald-C.jpg"></figure>
									<span>Ronald C.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Ron C" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Ron-C-300x300.jpg"></figure>
									<span>Ron C.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="susanc" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Susan-C.jpg"></figure>
									<span>Susan C.</span>
								</div>
								<div class="column_special">
									<figure><img class="lazy" alt="Wesley-S" data-original="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Wesley-S.jpg"></figure>
									<span>Wesley S.</span>
								</div>
							</div>

						</section>
					</section>
				</section>

			<?php }

			/**
			 * charity section
			 */

			elseif( $section == 'charity' ) { ?>

				<section id="charity-landing">
					<section class="wrapper">

						<div class="narrow">
							<?php echo apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'charity_intro_txt', 1 ) ); ?>
						</div>

						<?php if( get_post_meta( $pageid, $prefix . 'charity_txt', 1 ) != '' ) {
							echo '<div class="narrow">'.
								apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'charity_txt', 1 ) ) .'
							</div>';
						} ?>


					</section>
				</section>

			<?php }
		?>


		<section class="wrapper pixlee">
			<script id="pixlee_script" src="//assets.pixlee.com/assets/pixlee_widget_v3.js"></script><script>// <![CDATA[
			var pixlee = new PixleeWidget("https://photos.pixlee.com/competitorgroup/2991/v2?horizontal=true&#038;filter_sort_id=23350","pixlee_container", "https://photos.pixlee.com/", document.location.hash);pixlee.setupWidget(true);
			// ]]></script>
		</section>

	</main>

	<script type="text/javascript">
		jQuery(function($) {

			$(".fancybox").fancybox({
				slideShow: false,
				fullScreen: false,
				thumbs: false,
			});

		});
	</script>

<?php get_footer('series'); ?>
