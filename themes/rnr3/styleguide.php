<?php
  /* Template Name: StyleGuide */
  get_header('special');

  $prefix = '_rnr3_';

  // main styleguide groups
  $entries = get_post_meta( get_the_ID(), $prefix . 'styleguide_group', 1 );
  $count = 0;
  foreach( $entries as $key => $entry ) {
    $title    = esc_html( $entry['title'] );
    $content  = apply_filters( 'the_content', $entry['content'] );

    ${'styleguide'.$count}['title']   = $title;
    ${'styleguide'.$count}['content'] = $content;

    $count++;
  }

  $styleguide_array = array();
  for( $i = 0; $i < $count; $i++ ) {
    $add = ${'styleguide'.$i};
    array_push( $styleguide_array, $add );
  }

  // shortcodes
  $shortcodes = get_post_meta( get_the_ID(), $prefix . 'shortcode_group', 1 );
  $count = 0;
  foreach( $shortcodes as $key => $shortcode ) {
    $title    = esc_html( $shortcode['title'] );
    $code     = $shortcode['code'];
    $content  = apply_filters( 'the_content', $shortcode['content'] );

    ${'shortcode'.$count}['title']    = $title;
    ${'shortcode'.$count}['code']     = $code;
    ${'shortcode'.$count}['content']  = $content;

    $count++;
  }

  $shortcode_array = array();
  for( $i = 0; $i < $count; $i++ ) {
    $sc_add = ${'shortcode'.$i};
    array_push( $shortcode_array, $sc_add );
  }

  $nohero_class = '';
  if( get_post_meta( get_the_ID(), '_rnr3_disable_hero', 1 ) == 'on' ) {
    $nohero_class = 'class="no-hero"';
  }
?>

  <!-- main content -->
  <main role="main" id="main" <?php echo $nohero_class; ?>>
    <?php
      // create a menu in wordpress labeled "styleguide-subnav"
      $subnav_args = array(
        'menu' => 'styleguide-subnav',
        'fallback_cb' => false,
        'menu_class' => 'subnav-menu',
        'theme_location' => '__no_such_location',
        'echo' => false
      );
      wp_nav_menu( $subnav_args );

      if( wp_nav_menu( $subnav_args ) != '' ) {
        echo '<nav id="subnav">
          <section class="wrapper">
            <div class="subnav-menu-label">Menu</div>
            <div class="subnav-menu-primary-event-container">
              '. wp_nav_menu( $subnav_args ) .'
            </div>
          </section>
        </nav>';
      }
    ?>

    <div id="nav-anchor"></div>
    <section class="wrapper grid_2 offset240left">
      <div class="column sidenav stickem">
        <h2>RNR3 Styleguide</h2>
        <nav class="sticky-nav">
          <ul>
            <?php
              foreach( $styleguide_array as $key => $value ) {
                if( $value['content'] != '' ) {
                  echo '<li><a href="#styleguide-'.$key.'">'.$value['title'].'</a></li>';
                }
              }
            ?>
            <li><a href="#styleguide-100">Shortcodes</a></li>
          </ul>
        </nav>
      </div>

      <div class="column">
        <div class="content">
          <?php if (have_posts()) : while (have_posts()) : the_post();
            the_content();
          endwhile; endif;

          foreach( $styleguide_array as $key => $value ) {
            if( $value['content'] != '' ) {
              echo '<div id="styleguide-'.$key.'">
                <h3>'.$value['title'].'</h3>'
                . $value['content'] .
              '</div>';
            }
          } ?>
          <div id="styleguide-100">
            <h3>Shortcodes</h3>
            <p>Below is a list of handy shortcodes you can use within your post/page:</p>
            <ul>
              <?php foreach( $shortcode_array as $key => $value ) {
                echo '<li>
                  <p><strong>'. $value['title'] .'</strong><br>
                  '. $value['code'] .'</p>';
                  if( $value['content'] != '' ) {
                    echo $value['content'];
                  }
                echo '</li>';
              } ?>
            </ul>
          </div>

        </div>
      </div>
    </section>
    <script>
      $(document).ready(function(){

          /**
           * This part causes smooth scrolling using scrollto.js
           * We target all a tags inside the nav, and apply the scrollto.js to it.
           */
          $(".sticky-nav a, .backtotop, .scrolly").click(function(evn){
              evn.preventDefault();
              $('html,body').scrollTo(this.hash, this.hash);
          });

          /*$('main').stickem({
            container: '.offset240left',
          });*/

          /**
           * This part handles the highlighting functionality.
           * We use the scroll functionality again, some array creation and
           * manipulation, class adding and class removing, and conditional testing
           */
          var aChildren = $(".sticky-nav li").children(); // find the a children of the list items
          var aArray = []; // create the empty aArray
          for (var i=0; i < aChildren.length; i++) {
              var aChild = aChildren[i];
              var ahref = $(aChild).attr('href');
              aArray.push(ahref);
          } // this for loop fills the aArray with attribute href values

          $(window).scroll(function(){
              var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
              var windowHeight = $(window).height(); // get the height of the window
              var docHeight = $(document).height();

              for (var i=0; i < aArray.length; i++) {
                  var theID = aArray[i];
                  var divPos = $(theID).offset().top; // get the offset of the div from the top of page
                  var divHeight = $(theID).height(); // get the height of the div in question
                  if (windowPos >= (divPos - 1) && windowPos < (divPos + divHeight -1 )) {
                      $("a[href='" + theID + "']").addClass("nav-active");
                  } else {
                      $("a[href='" + theID + "']").removeClass("nav-active");
                  }
              }

              if(windowPos + windowHeight == docHeight) {
                  if (!$(".sticky-nav li:last-child a").hasClass("nav-active")) {
                      var navActiveCurrent = $(".nav-active").attr("href");
                      $("a[href='" + navActiveCurrent + "']").removeClass("nav-active");
                      $(".sticky-nav li:last-child a").addClass("nav-active");
                  }
              }
          });
      });

    </script>
  </main>

<?php get_footer( 'series' ); ?>
