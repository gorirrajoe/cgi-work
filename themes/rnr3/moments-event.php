<?php
	/* Template Name: Moments ( Event ) */

	get_header();

	$market = get_market2();
	if( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$prefix = '_rnr3_';

	if( ( $presenting_sponsor_img = get_post_meta( get_the_ID(), $prefix . 'sponsor', 1 ) ) != '' ) {
		$presenting_sponsor = '<div class="presented_by"><span>Presented by</span><img src="'. $presenting_sponsor_img .'"></div>';
	} else {
		$presenting_sponsor = '';
	}

	/**
	 * the first year of lv moments (2015) didn't have a year attached to the videos category
	 */
	$year = ( get_the_date( 'Y' ) < 2016 ) ? '' : '-' . date( 'Y' );

	$twitter_hashtag	= $event_info->twitter_hashtag;
	$twitter			= rnr_get_option( 'rnrgv_twitter' );
	$instagram			= rnr_get_option( 'rnrgv_instagram' );
	$youtube			= rnr_get_option( 'rnrgv_youtube' );

	$social_ribbon = '<div id="ribbon" class="moments_ribbon">
		<div class="youtube">
			<a href="'. $youtube .'" target="_blank"><span class="icon-youtube-play"></span></a>
		</div>
		<div class="facebook">
			<a href="'. $event_info->facebook_page .'" target="_blank"><span class="icon-facebook"></span></a>
		</div>
		<div class="twitter">
			<a href="'. $twitter .'" target="_blank"><span class="icon-twitter"></span></a>
		</div>
		<div class="instagram">
			<a href="'. $instagram .'" target="_blank"><span class="icon-instagram"></span></a>
		</div>
	</div>';

?>
	<!-- main content -->
	<main role="main" id="main" class="no-hero moments-event">

		<nav id="subnav">
			<section class="wrapper">
				<div class="subnav-menu-label">Menu</div>
				<div class="subnav-menu-primary-event-container">
					<?php
						// create a menu in wordpress labeled "moments-subnav"
						$subnav_args = array(
							'menu'				=> 'moments-subnav' . $year,
							'fallback_cb'		=> false,
							'menu_class'		=> 'subnav-menu',
							'theme_location'	=> '__no_such_location',
						);
						wp_nav_menu( $subnav_args );
					?>
				</div>
			</section>
		</nav>

		<section class="wrapper">

			<div class="content">
				<?php if( have_posts() ) : while ( have_posts() ) : the_post();

					if( $presenting_sponsor != '' ) {
						echo '<h2 id="section-1">'. get_the_title() . $presenting_sponsor .'</h2>';
						echo $social_ribbon;
					} else {
						echo '<h2 id="section-1">'. get_the_title() . $social_ribbon .'</h2>';
					}

					the_content();

				endwhile; endif; ?>
			</div>


			<?php
				$args = array(
					'post_type'			=> 'moment_event',
					'post_status'		=> 'publish',
					'posts_per_page'	=> -1,
					'tax_query'			=> array(
						array(
							'taxonomy'	=> 'moments_event',
							'field'		=> 'slug',
							'terms'		=> 'videos' . $year
						)
					)
				);

				$count			= 0;
				$video_query	= new WP_Query( $args );

				if( $video_query->have_posts() ) {

					echo '<section id="section-2">
						<h2>'. get_post_meta( get_the_ID(), $prefix . 'videos_title', 1 ) .'</h2>';

						$videos_limit = 8;

						echo '<div id="switcher-panel1"></div>
						<div class="grid_4_special video_grid">';

							while( $video_query->have_posts() ) {

								$video_query->the_post();

								$youtube_id = get_post_meta( get_the_ID(), $prefix . 'youtube_id', 1 );

								if( $count < $videos_limit ) {

									if( $count == 0 ) {
										$divshow = ' show';
									} else {
										$divshow = '';
									}

									echo '<div class="column_special">
										<figure>
											<a href="#section-2" id="contentvid-'. $count .'" class="switcher set1">'.
												get_the_post_thumbnail( get_the_ID(), 'moments-video-thumb' ) .'
												<span class="icon-play"></span>
											</a>
										</figure>
										<h4>'. get_the_title() .'</h4>
									</div>

									<div id="contentvid-'. $count .'-content" class="switcher-content set1'. $divshow .'">
										<div class="video-container">
											<iframe width="1280" height="720" src="https://www.youtube.com/embed/'. $youtube_id .'?rel=0" frameborder="0" allowfullscreen></iframe>
										</div>
										<h3>'. get_the_title() .'</h3>
										<p>'. get_the_excerpt() .'</p>' .
										get_share_module( get_the_ID(), $youtube_id, $twitter_hashtag ) .'
									</div>';

								} else {

									if( $count == $videos_limit ) {
										echo '</div>
										<p><a class="cta morevideos">View More</a></p>
										<div class="lessvideos grid_4_special video_grid" style="display:none;">';
									}

									echo '<div class="column_special">
										<figure><a href="#section-2" id="contentvid-'. $count .'" class="switcher set1">'. get_the_post_thumbnail( get_the_ID(), 'moments-video-thumb' ) .'<span class="icon-play"></span></a></figure>
										<h4>'. get_the_title() .'</h4>
									</div>

									<div id="contentvid-'. $count .'-content" class="switcher-content set1'. $divshow .'">
										<div class="video-container">
											<iframe width="1280" height="720" src="https://www.youtube.com/embed/'. $youtube_id .'?rel=0" frameborder="0" allowfullscreen></iframe>
										</div>
										<h3>'. get_the_title() .'</h3>
										<p>'. get_the_excerpt() .'</p>' .
										get_share_module( get_the_ID(), $youtube_id, $twitter_hashtag ) .'
									</div>';
								}
								$count++;

							}

						echo '</div>
					</section>';
				}

				wp_reset_postdata();


				if( get_post_meta( get_the_ID(), $prefix . 'rnb_enable', 1 ) ) { ?>

					<section id="section-3">
						<h2><?php echo get_post_meta( get_the_ID(), $prefix . 'rnb_title', 1 ); ?></h2>

						<?php
							echo apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'rocknbloggers_main', 1 ) );

							$args = array(
								'post_type'			=> 'moment_event',
								'post_status'		=> 'publish',
								'posts_per_page'	=> -1,
								'tax_query'			=> array(
									array(
										'taxonomy'	=> 'moments_event',
										'field'		=> 'slug',
										'terms'		=> 'rock-n-bloggers'
									)
								)
							);

							$count					= 0;
							$rocknbloggers_query	= new WP_Query( $args );

							if( $rocknbloggers_query->have_posts() ) {

								$rnb_limit = 4;

								echo '<div id="switcher-panel2"></div>
								<div class="grid_4_special video_grid">';

									while( $rocknbloggers_query->have_posts() ) {

										$rocknbloggers_query->the_post();

										$youtube_id = get_post_meta( get_the_ID(), $prefix . 'youtube_id', 1 );

										if( $count < $rnb_limit ) {

											$divshow = ( $count == 0 ) ? ' show' : '';

											echo '<div class="column_special">
												<figure><a href="#section-3" id="contentrnb-'. $count .'" class="switcher set2">'. get_the_post_thumbnail( get_the_ID(), 'moments-video-thumb' ) .'<span class="icon-play"></span></a></figure>
												<h4>'. get_the_title() .'</h4>
											</div>

											<div id="contentrnb-'. $count .'-content" class="switcher-content set2'. $divshow .'">
												<div class="video-container">
													<iframe width="1280" height="720" src="https://www.youtube.com/embed/'. $youtube_id .'?rel=0" frameborder="0" allowfullscreen></iframe>
												</div>
												<h3>'. get_the_title() .'</h3>
												<p>'. get_the_excerpt() .'</p>' .
												get_share_module( get_the_ID(), $youtube_id, $twitter_hashtag ) .'
											</div>';

										} else {

											if( $count == $rnb_limit ) {
												echo '</div>
												<p><a class="cta morevideos2">View More</a></p>
												<div class="lessvideos2 grid_4_special video_grid" style="display:none;">';
											}

											echo '<div class="column_special">
												<figure>
													<a href="#section-3" id="contentrnb-'. $count .'" class="switcher set2">'.
														get_the_post_thumbnail( get_the_ID(), 'moments-video-thumb' ) .'
														<span class="icon-play"></span>
													</a>
												</figure>
												<h4>'. get_the_title() .'</h4>
											</div>

											<div id="contentrnb-'. $count .'-content" class="switcher-content set2'. $divshow .'">
												<div class="video-container">
													<iframe width="1280" height="720" src="https://www.youtube.com/embed/'. $youtube_id .'?rel=0" frameborder="0" allowfullscreen></iframe>
												</div>
												<h3>'. get_the_title() .'</h3>
												<p>'. get_the_excerpt() .'</p>' .
												get_share_module( get_the_ID(), $youtube_id, $twitter_hashtag ) .'
											</div>';

										}

										$count ++;
									}

								echo '</div>';
							}

							wp_reset_postdata();
						?>
					</section>

				<?php }


				if( ( $presale_txt = get_post_meta( get_the_ID(), $prefix . 'presale', 1 ) ) != '' ) { ?>

					<section id="section-4">
						<?php echo apply_filters( 'the_content', $presale_txt ); ?>
					</section>

				<?php }
			?>

		</section>

		<script>
			$( document ).ready( function(){
					/**
					 * This part causes smooth scrolling using scrollto.js
					 * We target all a tags inside the nav, and apply the scrollto.js to it.
					 */
					$( "#subnav a, .switcher" ).click( function( evn ){
						evn.preventDefault();
						$( 'html,body' ).scrollTo( this.hash, this.hash );
					} );

					jcps.fader( 0, '#switcher-panel1', '.set1' );
					jcps.fader( 0, '#switcher-panel2', '.set2' );
			} );

		</script>
	</main>

<?php get_footer(); ?>
