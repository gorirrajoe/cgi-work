<?php
  /* Template Name: NRD Training (Series) */
  get_header('special');
  global $wpdb;
?>
  <main role="main" id="main">
    <section class="wrapper" id="training-group">
      <h3 class="intro"><?php echo get_post_meta( get_the_ID(), '_rnr3_nrdtrain_intro', true ); ?></h3>
      <section class="grid_2">
        <?php
          for( $i = 1; $i < 9; $i++ ) {
            if( ( $i % 2 ) != 0 && ( $i != 1) ) {
              echo '</section>
              <section class="grid_2">';
            }
            echo '<article class="column">
              <div class="header">
                <figure class="training-graphic"><img src="'.get_post_meta( get_the_ID(), '_rnr3_nrdtrain_graphic_'.$i, true ).'"></figure>
                <h2>
                  <span class="nrd_number">'.$i.'</span>
                  '.get_post_meta( get_the_ID(), '_rnr3_nrdtrain_title_'.$i, true ).'
                </h2>
              </div>
              <div class="training-content">
                '.apply_filters( 'the_content', get_post_meta( get_the_ID(), '_rnr3_nrdtrain_content_'.$i, true ) ).'
              </div>
            </article>';
          }
        ?>
      </section>
      <div class="banner">
        <a href="http://whyrunningrocks.com">
          <div>
            <figure class="runner-left"><img src="<?php echo get_post_meta( get_the_ID(), '_rnr3_nrdtrain_runnerleft', true ); ?>"></figure>
            <figure class="runner-right"><img src="<?php echo get_post_meta( get_the_ID(), '_rnr3_nrdtrain_runnerright', true ); ?>"></figure>
            <div class="chkout">Check Out</div>
            <div class="banner_url">WhyRunningRocks.com</div>
          </div>
        </a>
      </div>
    </section>
  </main>
<?php get_footer('oneoffs'); ?>