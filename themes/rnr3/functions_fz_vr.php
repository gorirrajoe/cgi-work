<?php
if(stripos($_SERVER['SERVER_NAME'], 'cgitesting') !== false OR stripos($_SERVER['SERVER_NAME'], 'lan') !== false OR stripos($_SERVER['SERVER_NAME'], 'loc') !== false){
	define( 'VRRESULTSHOSTNAME', 'localhost' ); 
	define( 'VRRESULTSDBUSER', 'rnruserdb' ); 
	define( 'VRRESULTSDBPWD', 'cgi123' ); 
	define( 'VRRESULTSDBNAME', 'rnr_results' );
	
	define( 'VREVENTHOSTNAME', 'localhost' ); 
	define( 'VREVENTDBUSER', 'rnruserdb' ); 
	define( 'VREVENTDBPWD', 'cgi123' ); 
	define( 'VREVENTDBNAME', 'runrocknroll' );
}else{
	define( 'VRRESULTSHOSTNAME', '192.168.0.10' ); 
	define( 'VRRESULTSDBUSER', 'rnruserdb' ); 
	define( 'VRRESULTSDBPWD', 'cGi@waples921' ); 
	define( 'VRRESULTSDBNAME', 'runrocknroll' );
	
	define( 'VREVENTHOSTNAME', '192.168.0.10' ); 
	define( 'VREVENTDBUSER', 'rnruserdb' ); 
	define( 'VREVENTDBPWD', 'cGi@waples921' ); 
	define( 'VREVENTDBNAME', 'runrocknroll' );
}

/**
 * function: dbConnect
 */
function dbConnectVR( $hostToUse ) {
	//connect to db
	if( $hostToUse == 'results' ) {
		$wp_host	= VRRESULTSHOSTNAME;
		$wp_user	= VRRESULTSDBUSER;
		$wp_pwd		= VRRESULTSDBPWD;
		$wp_db		= VRRESULTSDBNAME;
	} else {
		$wp_host	= VREVENTHOSTNAME;
		$wp_user	= VREVENTDBUSER;
		$wp_pwd		= VREVENTDBPWD;
		$wp_db		= VREVENTDBNAME;
	}
	// we connect to db
	$link_wrwp = mysqli_connect( $wp_host, $wp_user, $wp_pwd );
	$link_wrwp->set_charset('utf8');
	if ( !$link_wrwp ) {
		die( 'Could not connect: ' . mysqli_error( ) );
	}
	//echo 'Connected to db successfully<br/>';
	return $link_wrwp;
}


/**
 * function: dbSelection
 */
function dbSelectionVR( $dbLinkConnection, $hostToUse ) {
	if( $hostToUse == 'results' ) {
		$wp_db = VRRESULTSDBNAME;
	} else {
		$wp_db = VREVENTDBNAME;
	}

	if ( !mysqli_select_db( $wp_db, $dbLinkConnection ) ) {
		die( 'Could not select database: ' . mysqli_error( ) );
	}
}


/**
 * function: dbClose
 */
function dbCloseVR( $dbLinkConnection ) {
	mysqli_close( $dbLinkConnection );
}

/**
 * function: getRunnerStats
 */
function getRunnerStatsVR( $cust_id, $dbLinkConnection) {
	$table_customers = "wp_fz_vr_customers";
	$rStatsQuery = 'SELECT * FROM '.VREVENTDBNAME.'.'.$table_customers.' WHERE pk_customer_id = "'.$cust_id.'"';
	$result1 = mysqli_query( $dbLinkConnection, $rStatsQuery );
	$values = "";
	$runnerStats = array( );
	while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
		$runnerStats = $row1;
	}
	return $runnerStats;
}

/**
 * function: getRunnerOtherInfo
 */
function getRunnerOtherInfoVR( $cust_id, $dbLinkConnection) {
	$table_customers = "wp_fz_vr_other_info";
	$rStatsQuery = 'SELECT * FROM '.VREVENTDBNAME.'.'.$table_customers.' WHERE fk_customer_id = "'.$cust_id.'"';
	$result1 = mysqli_query( $dbLinkConnection, $rStatsQuery );
	$runnerOInfo = array( );
	while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
		//print_r($row1);
		$runnerOInfo[] = $row1;
	}
	return $runnerOInfo;
}

/**
 * function: getInstagramHandle
 */
function getRunnerInstagrmaVR( $cust_id, $dbLinkConnection) {
	$table_customers = "wp_fz_vr_other_info";
	$rStatsQuery = 'SELECT answer FROM '.VREVENTDBNAME.'.'.$table_customers.' WHERE fk_customer_id = "'.$cust_id.'" AND field = "what is your instagram handle?"';
	$result1 = mysqli_query( $dbLinkConnection, $rStatsQuery );
	$runnerIG = '';
	while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
		$runnerIG = $row1;
	}
	return $runnerIG;
}

/**
 * function: getRunnerEvents
 */
function getRunnerEventsVR( $cust_id, $dbLinkConnection) {
	$table_reg = "wp_fz_vr_registrations s";
	$table_events = "wp_fz_vr_event t";
	$rEventsQuery = 'SELECT * FROM '.VREVENTDBNAME.'.'.$table_reg.', '.VREVENTDBNAME.'.'.$table_events.' WHERE s.fk_customer_id = "'.$cust_id.'" AND t.pk_event_id = s.fk_event_id AND t.published = 1 ORDER BY date_start DESC';
	//echo $rEventsQuery;
	$result1 = mysqli_query( $dbLinkConnection, $rEventsQuery );
	$values = "";
	$runnerStats = array( );
	while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
		$runnerStats[] = $row1;
	}
	return $runnerStats;
}

/**
 * function: getChildEventsVR
 */
function getChildEventsVR( $event_id, $dbLinkConnection) {
	$table_reg = "wp_fz_vr_event_relation s";
	$table_events = "wp_fz_vr_event t";
	$rEventsQuery = 'SELECT t.* FROM '.VREVENTDBNAME.'.'.$table_reg.', '.VREVENTDBNAME.'.'.$table_events.' WHERE s.fk_event_id ';
	if(is_array($event_id)){
		$comma_separated = implode(",", $event_id);
		$rEventsQuery .= ' IN ('.$comma_separated.')';
	}else{
		$rEventsQuery .= ' = "'.$event_id.'"';
	}
	$rEventsQuery .= ' AND t.pk_event_id = s.child_event_id AND t.published = 1';
	$result1 = mysqli_query( $dbLinkConnection, $rEventsQuery );
	$values = "";
	$runnerStats = array( );
	while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
		$runnerStats[] = $row1;
	}
	return $runnerStats;
}

/**
 * function: getChildEventAndBadgesVR
 */
function getChildEventAndBadgesVR( $event_id, $dbLinkConnection) {

	$table_reg = "wp_fz_vr_event_relation s";
	$table_events = "wp_fz_vr_event t";
	$table_badges = "wp_fz_vr_badges r";
	$rEventsQuery = 'SELECT t.display_title AS E_Title,t.series_name AS E_SName,t.date_end AS E_DateE,t.date_start AS E_DateS,r.* FROM '.VREVENTDBNAME.'.'.$table_reg.', '.VREVENTDBNAME.'.'.$table_events.', '.VREVENTDBNAME.'.'.$table_badges.' WHERE s.fk_event_id ';
	if(is_array($event_id)){
		$comma_separated = implode(",", $event_id);
		$rEventsQuery .= ' IN ('.$comma_separated.')';
	}else{
		$rEventsQuery .= ' = "'.$event_id.'"';
	}
	$rEventsQuery .= ' AND t.pk_event_id = s.child_event_id AND r.fk_vr_event_id = s.child_event_id ORDER BY t.date_end DESC,t.pk_event_id';
	$result1 = mysqli_query( $dbLinkConnection, $rEventsQuery );
	$values = "";
	$runnerStats = array( );
	while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
		$runnerStats[] = $row1;
	}
	return $runnerStats;
}

/**
 * function: getPromoEventAndBadgesVR
 */
function getPromoEventAndBadgesVR( $event_id, $dbLinkConnection) {

	$table_events = "wp_fz_vr_event t";
	$table_badges = "wp_fz_vr_badges r";
	$rPromoQuery = 'SELECT t.display_title AS E_Title,t.series_name AS E_SName,t.date_end AS E_DateE,t.date_start AS E_DateS,r.* FROM '.VREVENTDBNAME.'.'.$table_events.', '.VREVENTDBNAME.'.'.$table_badges.' WHERE ';
	$rPromoQuery .= 't.pk_event_id = r.fk_vr_event_id AND r.fk_vr_event_id IN (SELECT DISTINCT(fk_event_id) FROM wp_fz_vr_event_relation WHERE child_event_id';
	if(is_array($event_id)){
		$comma_separated = implode(",", $event_id);
		$rPromoQuery .= ' IN ('.$comma_separated.')';
	}else{
		$rPromoQuery .= ' = "'.$event_id.'"';
	}
	$rPromoQuery .= ')  AND t.date_end >= CURDATE() ORDER BY t.date_end DESC,t.pk_event_id';
	$result1 = mysqli_query( $dbLinkConnection, $rPromoQuery );
	$values = "";
	$runnerStats = array( );
	while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
		$runnerStats[] = $row1;
	}
	return $runnerStats;
}


/**
 * function: doSearchResults
 */
function doSearchResultsVR( $searchParameters ) {
	$dbConnLink = dbConnectVR( 'results' );
	$search1 = getSearchResultsVR( $dbConnLink, $searchParameters );
	dbCloseVR( $dbConnLink );
	return $search1;
}


/**
 * function: getSearchResults
 */
function getSearchResultsVR( $dbLinkConnection, $searchParameters ) {

	$table_customers = "wp_fz_vr_customers r";
	$table_reg = "wp_fz_vr_registrations s";
	$table_events = "wp_fz_vr_event t";
	$searchStmt = "SELECT r.pk_customer_id, r.FIRSTNAME, r.LASTNAME, r.CITY, r.STATE, r.PROVINCE, r.COUNTRYCODE, t.display_title AS EVENT_NAME FROM ".VREVENTDBNAME.".".$table_customers.",".VREVENTDBNAME.".".$table_reg.",".VREVENTDBNAME.".".$table_events." WHERE";
	if( !empty( $searchParameters['firstname'] ) ) {
		$searchStmt .= ' r.FIRSTNAME LIKE "'.$searchParameters['firstname'].'%"';
	}
	if( !empty( $searchParameters['lastname'] ) ) {
		if( !empty( $searchParameters['firstname'] ) ) {
			$searchStmt .= ' AND';
		}
		$searchStmt .= ' r.LASTNAME LIKE "'.$searchParameters['lastname'].'%"';
	}
	if( !empty( $searchParameters['firstname'] ) OR !empty( $searchParameters['lastname'] ) ) {
		$searchStmt .= ' AND';
	}else{
		//add something not to kill the db
		$searchStmt .= ' r.FIRSTNAME = "xxyyzz" AND';
	}
	$searchStmt .= " s.fk_customer_id = r.pk_customer_id AND t.pk_event_id = s.fk_event_id AND t.published = 1 ORDER BY r.LASTNAME, r.FIRSTNAME, t.display_title";

	if( !empty( $searchParameters['perpage'] ) ) {
		$searchStmt .= ' LIMIT ' . $searchParameters['perpage'];
	}
	if( !empty( $searchParameters['resultspage'] ) ) {
		$resultspage = $searchParameters['resultspage'];
		$totalrunners = getTotalRunnersVR( $searchParameters );
		$totalpages = $totalrunners[0] / $searchParameters['perpage'];

		if( $totalrunners[0] != 0 ) {
			if( $resultspage > ceil( $totalpages ) ) {
				$offset = ( $totalpages * $searchParameters['perpage'] ) - $searchParameters['perpage'];
			} else {
				$offset = ( ( $resultspage - 1 ) * $searchParameters['perpage'] );
			}
			$searchStmt .= ' OFFSET ' . $offset;
		}
	}

	//echo $searchStmt;
	$result1 = mysqli_query( $dbLinkConnection, $searchStmt );
	$runnerStats = array( );
	while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
		$runnerStats[] = $row1;
	}
	return $runnerStats;
}


/**
 * function: getTotalRunners
 */
function getTotalRunnersVR( $criteria ) {
	$dbConnLink = dbConnectVR( 'results' );

	$table_customers = "wp_fz_vr_customers r";
	$table_reg = "wp_fz_vr_registrations s";
	$table_events = "wp_fz_vr_event t";
	$total_runners_query = "SELECT COUNT(*) FROM ".VREVENTDBNAME.".".$table_customers.",".VREVENTDBNAME.".".$table_reg.",".VREVENTDBNAME.".".$table_events." WHERE";
	if( !empty( $criteria['firstname'] ) ) {
		$total_runners_query .= ' r.FIRSTNAME LIKE "'.$criteria['firstname'].'%"';
	}
	if( !empty( $criteria['lastname'] ) ) {
		if( !empty( $criteria['firstname'] ) ) {
			$total_runners_query .= ' AND';
		}
		$total_runners_query .= ' r.LASTNAME LIKE "'.$criteria['lastname'].'%"';
	}
	if( !empty( $criteria['firstname'] ) OR !empty( $criteria['lastname'] ) ) {
		$total_runners_query .= ' AND';
	}else{
		//add something not to kill the db
		$total_runners_query .= ' r.FIRSTNAME = "xxyyzz" AND';
	}
	$total_runners_query .= " s.fk_customer_id = r.pk_customer_id AND t.pk_event_id = s.fk_event_id AND t.published = 1";
	//echo $total_runners_query;

	if( $total_runners_result = mysqli_query( $dbConnLink, $total_runners_query ) ) {
		$total_runners = mysqli_fetch_array( $total_runners_result, MYSQLI_NUM );
	}
	$total_runners_result->close();

	dbCloseVR( $dbConnLink );
	return $total_runners;
}


/**
 * function: get_correction_link
 */
function get_correction_linkVR($qt_lang) {
	if( $qt_lang['lang'] == 'es'){
		$submit_corr_txt			= 'Somete una correcci&oacute;n';
	}else{
		$submit_corr_txt			= 'Submit a Correction';
	}
	echo '<p class="center"><a href="http://www.runrocknroll.com/contact/corrections/">'. $submit_corr_txt .'</a></p>';
}


/**
 * function:
 */
function get_query_stringVR( $criteria, $exclude = NULL, $individual = 0 ) {
	if( $individual != 1 ) {
		$query_string = '';
		foreach( $criteria as $key => $value ) {
			if( $key != $exclude ) {
				$query_string .= '&' . $key . '=' . $value;
			}
		}
	} else {
		$query_string = '';
		foreach( $criteria as $key => $value ) {
			if( $key != $exclude ) {
				$query_string .= '&' . $key . '=' . $value;
			}
		}

	}
	return $query_string;
}

function getFZWhatsNextVR( $whatsNextid ){

	global $wpdb;
	if(is_array($whatsNextid)){
		$comma_separated = implode(",", $whatsNextid);
		$query = 'SELECT * FROM wp_fz_whats_next WHERE pk_whats_next_id IN ('.$comma_separated.')';
	}else{
		$query = 'SELECT * FROM wp_fz_whats_next WHERE pk_whats_next_id = '.$whatsNextid;
	}
	$nextInfo = $wpdb->get_results( $query, ARRAY_A);
	if ( count($nextInfo) == 0 ) {
		$wpdb->print_error();
		return null;
	} else {
		$whatsNextInfo = array();
		foreach($nextInfo as $keyWN => $subWN){
					$whatsNextInfo[$subWN['pk_whats_next_id']] = $subWN;
		}
		return $whatsNextInfo;
	}
}

function getFZBadgesVR( $eventid ){

	global $wpdb;
	if(is_array($eventid)){
		$comma_separated = implode(",", $eventid);
		$query = 'SELECT * FROM wp_fz_vr_badges WHERE fk_vr_event_id IN ('.$comma_separated.') ORDER BY fk_vr_event_id';
	}else{
		//$query = 'SELECT s.*,r.argument FROM wp_fz_vr_badges s, wp_fz_qualifications r WHERE s.fk_vr_event_id = '.$eventid.' AND r.pk_qual_id = s.fk_qualification_default AND r.type = "badge" ORDER BY s.fk_vr_event_id';
		$query = 'SELECT * FROM wp_fz_vr_badges WHERE fk_vr_event_id = '.$eventid.' ORDER BY fk_vr_event_id';
	}
	$badgesInfo = $wpdb->get_results( $query, ARRAY_A);
	if ( count($badgesInfo) == 0 ) {
		$wpdb->print_error();
		return null;
	} else {
		$badgeInfo = array();
		foreach($badgesInfo as $keyWN => $subWN){
			if(is_array($eventid)){
				foreach($eventid as $keyBID => $subBID){
					if($subBID == $subWN['pk_vr_badage_id']){
						$badgeInfo[$keyBID] = $subWN;
					}
				}
			}else{
				$badgeInfo[$subWN['pk_vr_badage_id']] = $subWN;
			}
		}
		return $badgeInfo;
	}
}

/**
 * function getTally
 * returns an array:
 * index 0 = count of possiblities for this runner
 * index 1 = array of possiblities
 */
function getTallyVR( $subEventInfoToUse, $field = NULL) {
	$i = 1;
	$bdCount = 0;
	$badgeArray = array();
	if($field == null){
		$field = 'fk_badges_id_';
	}
	while($i <= 20) {
		if( $i < 10 ) {
			$badgeId = $field.'0' . $i;
		} else {
			$badgeId = $field . $i;
		}
		if( !empty( $subEventInfoToUse[$badgeId] ) ) {
			$badgeArray[$i] = $subEventInfoToUse[$badgeId];
			$bdCount++;
		}
		$i++;
	}
	$badgeTally[0] = $bdCount;
	$badgeTally[1] = $badgeArray;
	return $badgeTally;
}

function getPreviousVREvents( $eventid, $customer_id ){

	global $wpdb;
	$query = 'SELECT pk_registration_id From wp_fz_vr_registrations WHERE fk_event_id IN (SELECT pk_event_id FROM wp_fz_vr_event ';
	$query .= 'WHERE date_end <= (SELECT date_start FROM wp_fz_vr_event WHERE pk_event_id =  '.$eventid.')) AND fk_customer_id = '.$customer_id;
	$previousEvents = $wpdb->get_results( $query, ARRAY_A);
	if ( count($previousEvents) == 0 ) {
		$wpdb->print_error();
		return null;
	} else {
		return 1;
	}
}

function isQualforBadge($badge_info,$runner_info,$lucky_finisher_info,$relation,$cust_id){
	switch($badge_info['argument']){
		case 'finish':
			return 1;
		case 'lucky':
			if( !is_array( $lucky_finisher_info ) ) {
				return 0;
			}else{
				foreach( $lucky_finisher_info as $key => $value ){
					if($value['fk_vr_event_id'] == $badge_info['fk_vr_event_id'] AND $value['fk_vr_badge_id'] == $badge_info['pk_vr_badage_id']){
						return 1;
					}
				}
				return 0;
			}
			return 0;
		case 'alumni':
			$eventsPrevious = getPreviousVREvents( $badge_info['fk_vr_event_id'], $cust_id );
			return $eventsPrevious;
		case 'question':
			if( !is_array( $runner_info ) ) {
				return 0;
			}else{
				foreach( $runner_info as $key => $value ){
					//print_r($value);
					//print_r($badge_info);
					if($relation == "child"){
						if(strtolower($value['field']) == strtolower($badge_info['question']) AND strtolower($value['answer']) == strtolower($badge_info['answer'])){
							return 1;
						}
					}else{
						//print_r($value);
						//print_r($badge_info);
						if(strtolower($value['field']) == strtolower($badge_info['question']) AND strtolower($value['answer']) == strtolower($badge_info['answer']) AND $value['fk_event_id'] == $badge_info['fk_vr_event_id']){
							return 1;
						}
					}
				}
				return 0;
			}
			return 1;
		default:
			return 0;
	}
}


/**
 * function: get day or month by language
 */
function get_month_by_languageVR( $lang, $position) {
	--$position;
	switch( $lang ){
		case 'pt':
			$meses_short = array("jan", "fev", "mar&ccedil;o", "abril", "maio", "junho", "julho", "agosto", "set", "out", "nov", "dez");
			break;
		case 'es':
			$meses_short = array("enero", "feb", "marzo", "abr", "mayo", "jun", "jul", "agosto", "set", "oct", "nov", "dic");
			break;
		case 'fr':
			$meses_short = array("janv", "fevr", "mars", "avril", "mai", "juin", "juil", "aout", "sept", "oct", "nov", "dec");
			break;
		default:
			$meses_short = array("jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sept", "oct", "nov", "dec");
			break;
	}

	return $meses_short[$position];
}

function isLuckyFinisherVR( $cust_id ){

	global $wpdb;

	$query = 'SELECT * FROM wp_fz_vr_lucky_winners WHERE fk_customer_id = "'.$cust_id.'"';

	$luckyFInfo = $wpdb->get_results( $query, ARRAY_A);
	if ( count($luckyFInfo) == 0 ) {
		//$wpdb->print_error();
		return 0;
	} else {
		//Result found is lucky finisher
		$lFInfo = array();
		foreach($luckyFInfo as $keyWN => $subWN){
					$lFInfo[$keyWN]['fk_vr_badge_id'] = $subWN['fk_vr_badge_id'];
					$lFInfo[$keyWN]['fk_vr_event_id'] = $subWN['fk_vr_event_id'];
		}
		return $lFInfo;
	}
}

function getWhatsNextVR(){

	global $wpdb;

	$query = 'SELECT * FROM wp_fz_vr_whats_next WHERE date_start <= CURDATE() and date_end >= CURDATE() ORDER BY date_start DESC';

	$wNextInfo = $wpdb->get_results( $query, ARRAY_A);
	if ( count($wNextInfo) == 0 ) {
		//$wpdb->print_error();
		return 0;
	} else {
		//Result found all badges for event
		$aWNInfo = array();
		foreach($wNextInfo as $keyWN => $subWN){
			$aWNInfo[$keyWN]['display_title'] = $subWN['display_title'];
			$aWNInfo[$keyWN]['display_text'] = $subWN['display_text'];
			$aWNInfo[$keyWN]['image_display'] = $subWN['image_display'];
			$aWNInfo[$keyWN]['link_text'] = $subWN['link_text'];
			$aWNInfo[$keyWN]['link'] = $subWN['link'];
		}
		return $aWNInfo;
	}
}

function getDiscountOffersVR(){

	global $wpdb;

	$query = 'SELECT * FROM wp_fz_vr_offers WHERE date_start <= CURDATE() and date_end >= CURDATE() ORDER BY date_start DESC';

	$dOffersInfo = $wpdb->get_results( $query, ARRAY_A);
	if ( count($dOffersInfo) == 0 ) {
		//$wpdb->print_error();
		return 0;
	} else {
		//Result found all badges for event
		$aDOInfo = array();
		foreach($dOffersInfo as $keyWN => $subWN){
			$aDOInfo[$keyWN]['display_title'] = $subWN['display_title'];
			$aDOInfo[$keyWN]['display_text'] = $subWN['display_text'];
			$aDOInfo[$keyWN]['image_display'] = $subWN['image_display'];
			$aDOInfo[$keyWN]['link_text'] = $subWN['link_text'];
			$aDOInfo[$keyWN]['link'] = $subWN['link'];
		}
		return $aDOInfo;
	}
}

/**
 * function: shareMessage
 * Translating function
 * returns string to print/manipulate
 */

function shareMessageVR( $qt_lang, $item_title, $item_type, $hashtag, $fb = false ){
	$message = '';

	if( $qt_lang['lang'] == 'es' ){
		$message .= "Gan&eacute; el distintivo $item_title";
		$message .= !$fb ? ". &iexcl;Checa mis estad&iacute;sticas! $hashtag" : " en ";
	}else{
		$message .= "I earned the $item_title badge.";
		$message .= !$fb ? ". Check out my stats! $hashtag" : " at ";
	}

	return $message;
}

/**
 * function: shareMessage
 * Translating function
 * returns string to print/manipulate
 */
function viewItemMessageVR( $qt_lang, $item_type, $runner_name ){
	$qt_status		= $qt_lang['enabled'] == 1 ? true : false;
	$qt_cur_lang	= $qt_lang['lang'];

	$item					= ($item_type == 'badge') ? 'Badges' : 'Rock \'n\' Roll Assessment';
	$message			= "See $runner_name's $item";

	if( $qt_status ){
		if( $qt_cur_lang == 'es' ){
			$item			= ($item_type == 'badge') ? 'Distintivos de' : 'Evaluac&iacute;on de Rock \'n\' Roll de';
			$message	= "Ver $item $runner_name";
		}
	}
	return $message;
}

function getPixleeImages ( $userhandle ){
	//$pixleeUrl = "https://distillery.pixlee.com/api/v2/albums/947626/photos?api_key=WQoUPrRWZqiM92HGW5X&filters=%7B%22filter_by_userhandle%22:%7B%22equals%22:%22llldcreate%22%7D%7D&page=1&per_page=30";
	$pixleeUrl = "https://distillery.pixlee.com/api/v2/albums/947626/photos?api_key=WQoUPrRWZqiM92HGW5X&filters=%7B%22filter_by_userhandle%22:%7B%22equals%22:%22".$userhandle."%22%7D%7D&page=1&per_page=16";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$pixleeUrl);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($ch, CURLOPT_TIMEOUT, 4);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  //to suppress the curl output 
	curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
	curl_setopt($ch, CURLOPT_FORBID_REUSE, 1); //makesure you watch for cache issue
	curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1); 
	$pixlee_result = curl_exec($ch);
	//print_r($pixlee_result);
	//mb_internal_encoding("UTF-8");
	// content retrieval
	$iGstuff = json_decode($pixlee_result);
	curl_close ($ch);
	return $iGstuff;
}

?>
