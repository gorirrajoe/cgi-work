<?php
	/* Template Name: Regional Landing - CHILD */

	get_header( 'regional-lp' );
	$prefix		= '_rnr3_reg_';
	$qt_lang	= rnr3_get_language();

	$event_location_pieces	= explode( '::', get_post_meta( get_the_ID(), '_rnr3_reg_event_location', 1 ) );
	$market					= $event_location_pieces[0];

	global $wpdb;
	$event_info = $wpdb->get_row( "SELECT event_url, event_date, has_races, festival, festival_start_date, festival_end_date FROM wp_rocknroll_events WHERE event_slug = '$market'" );
?>
	<main role="main" id="main" class="regional_lp">
		<section class="wrapper">
			<div class="regional_hdr">
				<h1><?php echo get_the_title(); ?></h1>

				<?php
					/**
					 * event distances
					 */
					if( $event_info->has_races != '' ) {
						$races_array		= explode( ',', $event_info->has_races );
						$has_races_array	= array();

						foreach( $races_array as $race ) {
							$has_races_array[] = get_race_display_name_regional( $race );
						}

						$has_races = implode( ' / ', $has_races_array );

						echo '<div class="regional_racelist">' . $has_races . '</div>';
					}

					/**
					 * event date
					 */
					$event_date = get_event_date_regional( $qt_lang[ 'lang' ], $market, $event_info );

					if( $event_date['mmmdd'] == 'TBD' ) {
						echo '<h3 class="regional_eventdate">TBD</h3>';
					} else {
						echo '<h3 class="regional_eventdate">' . $event_date['monthdate'] . ', ' . $event_date['year'] . '</h3>';
					}

				?>
			</div>

			<?php
				/**
				 * selling point
				 */
				echo '<div class="regional_event_selling_pt">';
					if ( has_post_thumbnail() ) {
						$featured_img_id	= get_post_thumbnail_id( get_the_ID() );
						$featured_img		= wp_get_attachment_image_src( $featured_img_id, 'regional-lg' );

						echo '<div class="regional_featuredimg"><img src="'. $featured_img[0] .'"></div>';
					}

					echo '<div class="regional_eventdetails">';
						$selling_pt = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'selling_point_txt', 1 ) );

						echo $selling_pt;

						/*
						$register_cta_txt = get_post_meta( get_the_ID(), $prefix . 'register_cta_txt', 1 );
						echo '<a href="'. $event_info->event_url .'register/" class="cta regional">'. $register_cta_txt .'</a>';
						*/

					echo '</div>
				</div>';


				/**
				 * about race
				 */
				$about_race = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'about_txt', 1 ) );

				echo '<div class="regional_about_race narrow">'
					. $about_race .
				'</div>';


				/**
				 * extra modules
				 */
				$extra_modules = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'extra_modules', 1 ) );
				echo '<div class="regional_extra_modules">'
					. $extra_modules .
				'</div>';


				/**
				 * cta group
				 */
				$reg_cta_txt	= get_post_meta( get_the_ID(), $prefix . 'register_cta_txt', 1 );
				$lp_cta_txt		= get_post_meta( get_the_ID(), $prefix . 'lp_cta_txt', 1 );

				echo '<div class="regional_cta_group button_group">
					<ul>
						<li><a href="'. $event_info->event_url .'register/" class="button_single highlight red">'. $reg_cta_txt .'</a></li>
						<li><a href="'. $event_info->event_url .'" class="button_single highlight gray">'. $lp_cta_txt .'</a></li>
					</ul>
				</div>';

				if (have_posts()) : while (have_posts()) : the_post();
					the_content();
					echo "<br/><br/>";
				endwhile; endif;


				/**
				 * sibling navigation
				 */
				$parent_id = wp_get_post_parent_id( get_the_ID() );

				$args = array(
					'child_of'	=> $parent_id,
					'title_li'	=> '',
					'exclude'	=> get_the_ID(),
					'echo'		=> false
				);

				$siblings		= wp_list_pages( $args );
				$bullet_count	= substr_count( $siblings, '<li' );

				if( $bullet_count > 0 ) {

					echo '<div class="regional_sibling_nav">
						<h2>Other Races in This Region</h2>
						<ul>'
							. $siblings .
						'</ul>
					</div>';
				}
			?>

		</section>

	</main>

<?php get_footer( 'regional-lp' ); ?>
