<?php
	/**
	 * Template Name: Registration Prices ( Send - TEST )
	 */

	date_default_timezone_set( 'America/Los_Angeles' );

	// just set "http" for now
	$is_https = isset( $_SERVER['HTTPS'] ) ? "http" : "http";


?>
 <!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>TEST - Registration Prices - SEND</title>
	</head>
	<body>
		<form method="POST" action="<?php echo $is_https . '://' . $_SERVER['HTTP_HOST']; ?>/registration-prices/">
			<input type="hidden" name="credentials" value="<?php echo md5('virginia'.date('Ymd'));?>" />
			<label>Org Id:</label> <input type="text" name="org_id" value="3090"/><br />
			<label>Org Name:</label> <input type="text" name="org_name" value="Rock 'n' Roll Marathon Series"/><br />
			<label>Sub Event Id:</label> <input type="text" name="sub_event_id" value="40445"/><br />
			<label>Sub Event Name:</label> <input type="text" name="sub_event_name" value="Testing Price Changes"/><br />
			<label>Division Id:</label> <input type="text" name="division_id" value="166037"/><br />
			<label>Division Name:</label> <input type="text" name="division_name" value="category"/><br />
			<label>Current Price:</label> <input type="text" name="current_price" value="100.00"/><br />
			<label>Base Price:</label> <input type="text" name="base_price" value="100.00"/><br />
			<label>Previous Price:</label> <input type="text" name="previous_price" value="-1"/><br />
			<label>Adjustment Id:</label> <input type="text" name="adjustment_id" value="-1"/><br />
			<label>Adjustment Amount:</label> <input type="text" name="adjustment_amount" value="-1"/><br />
			<label>Adjustment Start Date:</label> <input type="text" name="adjustment_start_date" value="-1"/><br />
			<label>Adjustment End Date:</label> <input type="text" name="adjustment_end_date" value="-1"/><br />
			<input type="submit" />
		</form>
	</body>
</html>
