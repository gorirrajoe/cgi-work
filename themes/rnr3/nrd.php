<?php
  /* Template Name: National Running Day (Series) */
  get_header('oneoffs');

  // set timezone to pacific
  // date_default_timezone_set('America/Los_Angeles' );

  $qt_lang = rnr3_get_language();
  if ( false === ( $all_events = get_transient( 'all_events_data' ) ) ) {
    $all_events = rnr3_get_all_events();
  } 
  /* should need to convert to time, dates are set
   * $current_date = strtotime( date( 'n/j/Y H:i' ) );
  $phase3_date = strtotime( '5/31/2015 00:00' );
  $phase4_date = strtotime( '6/3/2015 00:00' );
  $phase5_date = strtotime( '6/4/2015 00:00' );*/
  
  $current_date = date('YmdH');
  if( (WP_ENV === 'development' || WP_ENV === 'testing') OR strpos ( $_SERVER['HTTP_HOST'] , "lan" ) >= 1) {
  	$phase3_date = '2015060107';
  	$phase4_date = '2015060307';
  	$phase5_date = '2015060407';
  } else {
  	$phase3_date = '2015060107';
  	$phase4_date = '2015060307';
  	$phase5_date = '2015060407';
  }
?>
  <main role="main" id="main">
    <?php if( ( $banner_txt = get_post_meta( get_the_ID(), '_rnr3_wrr_banner_txt', true ) ) != '' ) { ?>
      <section id="banner">
        <section class="wrapper">
          <span><?php echo $banner_txt; ?></span>
        </section>
      </section>
    <?php } ?>

    <section id="city">
      <section class="wrapper">

        <div id="wrr-logo">
          <img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr-logo.svg" class="wrr-logo" alt="Why Running Rocks">
        </div>

        <span class="byline">
          <em>by</em><br>
          Rock 'n' Roll<br>
          Marathon Series
        </span>

        <?php
          echo apply_filters( 'the_content', get_post_meta( get_the_ID(), '_rnr3_wrr_panel1', true ) );
          echo '<a class="big_video wrr-play" id="panel1-play" href="javascript:;" onClick="toggleVideo();"><div class="preview"><img src="'. get_bloginfo('stylesheet_directory') .'/img/nrd/icon_video.png" class="play-icon" alt="Play"><span class="wrr_desktop_watch">' .get_post_meta( get_the_ID(), '_rnr3_wrr_watch_txt', true ) .'</span></div><span class="wrr_mobile_watch">' .get_post_meta( get_the_ID(), '_rnr3_wrr_watch_txt', true ) .'</span></a>';
        ?>

        <img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/runner-w.svg" class="runner" alt="Runner">

      </section>
    </section>
    <?php if( ( $wrr_youtube = get_post_meta( get_the_ID(), '_rnr3_wrr_youtube', true ) ) != '' ) {
      echo '<section id="city-video">
        <div id="popupVid" class="city-video-wrapper">
          <a class="big_video_close" id="panel1-close" href="javascript:;" onClick="toggleVideo(\'hide\');"><span class="icon-cancel"></span></a>
          <iframe width="100%" height="100%" src="https://www.youtube.com/embed/'. $wrr_youtube.'?enablejsapi=1&rel=0&showinfo=0&modestbranding=1" frameborder="0" allowfullscreen></iframe>
        </div>
      </section>';

    }


    /*if( ( $current_date < $phase4_date ) OR ( $current_date >= $phase5_date ) ) {*/ ?>

      <section id="desert">
        <section class="wrapper">

          <?php
            echo apply_filters( 'the_content', get_post_meta( get_the_ID(), '_rnr3_wrr_panel2', true ) );
            echo '<a id="panel2-link" href="'.get_post_meta( get_the_ID(), '_rnr3_wrr_reminderurl', true ).'" class="wrr-reminder">
            '.get_post_meta( get_the_ID(), '_rnr3_wrr_reminder_txt', true ).'</a>';

          ?>

          <img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/runner-m.svg" class="runner" alt="Runner">

        </section>
      </section>
    <?php /*}*/ ?>

    <?php /* Do not show signup form if is NRD */
    if( ( $current_date < $phase4_date ) OR ( $current_date >= $phase5_date ) ) { ?>
    <section id="beach">
      <section class="wrapper">
        <div class="wrr_left">
          <?php echo apply_filters( 'the_content', get_post_meta( get_the_ID(), '_rnr3_wrr_panel3', true ) ); ?>
        </div>

        <div class="sgform">
          <?php echo get_post_meta( get_the_ID(), '_rnr3_wrr_sg', true ); ?>
        </div>

        <img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/runner-m2.svg" class="runner" alt="Runner">

      </section>
    </section>
    <?php } ?>
    
    <?php 
      if( ( $current_date >= $phase3_date ) && ( $current_date < $phase5_date ) ) {
        rnr3_findrace( $all_events, $qt_lang );
      }
      if( ( $wrr_checkbox = get_post_meta( get_the_ID(), '_rnr3_wrr_checkbox', true ) ) != '' ){
      	rnr3_findrace( $all_events, $qt_lang );
      }

      if( $current_date >= $phase5_date ) { ?>
        <section id="nrd_pixlee">
          <section class="wrapper">
            <?php
              echo '
                <div class="pixlee_txt_group">
                  <h2>'.get_post_meta( get_the_ID(), '_rnr3_wrr_pixlee_title', true ).'</h2>'
                  . apply_filters( 'the_content', get_post_meta( get_the_ID(), '_rnr3_wrr_pixlee_description', true ) ) .
                '</div>' .
                get_post_meta( get_the_ID(), '_rnr3_wrr_pixlee', true );
            ?>
          </section>
        </section>
      <?php }
    ?>

    <section id="partners">
      <section class="wrapper">

        <h2>Partners</h2>
        <p>Thank you for running with us!</p>

        <div id="sponsor-carousel" class="owl-carousel">
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_aacr.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_be-well-philly.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_brooks.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_cbs4.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_cep.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_chocolate-milk.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_eagle-creek.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_fox10-phoenix.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_garmin.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_geico.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_gu.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_humana.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_john-volken.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_lls.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_michelob-ultra.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_my-sa.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_nbc-bay-area.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_precor.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_rungevity.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_sa-cvb.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_sports-authority.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_toyota.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_transamerica.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_visit-raleigh.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_visit-savannah.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_wral.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_wtoc.png"></div>
          <div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/nrd/wrr_wvec.png"></div>
        </div>

      </section>
    </section>
  </main>

  <script>
    $(document).ready(function(){
      /**
       * This part causes smooth scrolling using scrollto.js
       * We target all a tags inside the nav, and apply the scrollto.js to it.
       */
      $(".wrr-reminder, .wrr-banner").click(function(evn){
          evn.preventDefault();
          $('html,body').scrollTo(this.hash, this.hash); 
      });
      <?php if( $banner_txt != '' ) { ?>
        var stickyNavTop = $('#banner').offset().top;
         
        var stickyNav = function(){
        var scrollTop = $(window).scrollTop();
              
        if (scrollTop > stickyNavTop) { 
            $('#banner').addClass('sticky');
            $('#city').addClass('stickycity');
            $('#city-video').addClass('stickycity');
        } else {
            $('#banner').removeClass('sticky'); 
            $('#city').removeClass('stickycity'); 
            $('#city-video').removeClass('stickycity'); 
        }
        };
         
        stickyNav();
         
        $(window).scroll(function() {
            stickyNav();
        });
      <?php } ?>
    });

  </script>

  <?php if( $wrr_youtube != '' ) { ?>
    <script>
      $(document).ready(function(){
        $(".big_video_close").click(function(){
          $("#city-video").hide();
          $("#city").show();
        });
        $(".big_video").click(function(){
          $("#city-video").show();
          $("#city").hide();
        });
      });
      function toggleVideo(state) {
        // if state == 'hide', hide. Else: show video
        var div = document.getElementById("popupVid");
        var iframe = div.getElementsByTagName("iframe")[0].contentWindow;
        div.style.display = state == 'hide' ? 'none' : '';
        func = state == 'hide' ? 'pauseVideo' : 'playVideo';
        iframe.postMessage('{"event":"command","func":"' + func + '","args":""}','*');
      }
    </script>
    
  <?php }

get_footer('oneoffs'); ?>