<?php
	$market = get_market2();

	if( get_current_blog_id() <= 1 ) {
		get_header('series-home');
	} elseif( $market == 'finisher-zone' || $market == 'rock-blog' || $market == 'virtual-run' ) {
		get_header('special');
	} else {
		get_header();
	}
	$parent_slug	= the_parent_slug();

	$post_pubkey	= rnr_get_option( 'rnrgv_post_pubkey');

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';
?>

	<main role="main" id="main">
		<section class="wrapper grid_2 offset240left">

			<?php get_sidebar(); ?>

			<div class="column">
				<div class="content">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
							<h2>
								<?php the_title(); ?>
							</h2>


							<?php if( is_singular( 'moment' ) && has_tag( 'humana' ) ) {
								// get humana logo that was uploaded to the moments page
								$moment_single_id	= get_id_by_slug( 'moments' );
								$humana_logo		= get_post_meta( $moment_single_id, '_rnr3_moments_humana_logo', true );

								echo '<div class="single_humana_byline">Presented by <img src="'. $humana_logo .'" alt="Humana"></div>';
							} ?>

							<div class="entry">

								<?php
									echo '<div class="social_container">';

										$social_disabled = get_post_meta( get_the_ID(), '_rnr3_sharing_disabled', 1 );

										if( $social_disabled != 'on' ) {
											/**
											 * social sharing (top)
											 */
											social_sharing( $post_pubkey );

										}
										/**
										 * show category/tags
										 */
										rnr3_entry_meta( true, $market );

									echo '</div>';


									/**
									 * when a post is broadcast from the series blog, it'll have qtranslate-x tags, eg: [:en]
									 * if the blog doesn't have qtranslate-x enabled, then make sure it properly filters that out
									 */
									the_content();


									wp_link_pages( array(
										'before'			=> '<div class="post_navigation"><p><strong>Pages:</strong> ',
										'after'				=> '</p></div>',
										'next_or_number'	=> 'number'
									) );


									/**
									 * liveblog
									 */
									if( get_post_meta( get_the_ID(), '_rnr3_liveblog', 1 ) == 'on' ) {
										get_liveblog_entries();
									}


									echo '<div class="entry-footer">';

										if( $social_disabled != 'on' ) {
											/**
											 * social sharing (bottom)
											 */
											social_sharing( $post_pubkey, 0 );
										}

										/**
										 * show category/tags
										 */
										rnr3_entry_meta( false, $market );

									echo '</div>';


									/**
									 * show author byline only for rock blog posts
									 */
									if( $market == 'rock-blog' ) {
										$authorurl		= get_author_posts_url( get_the_author_meta( 'ID' ) );
										$displayname	= get_the_author_meta( 'display_name' );
										$gravatar		= get_avatar( get_the_author_meta( 'ID' ), 120, '', $displayname, array( 'class' => 'alignleft' ) );
										$bio			= apply_filters( 'the_content', get_the_author_meta( 'description' ) );
										$twitter		= get_the_author_meta( '_rnr3_twitter' );
										$facebook		= get_the_author_meta( '_rnr3_facebook' );
										$instagram		= get_the_author_meta( '_rnr3_instagram' );
										$pinterest		= get_the_author_meta( '_rnr3_pinterest' );
										$googleplus		= get_the_author_meta( '_rnr3_google_plus' );

										echo '<section id="author-profile-single">
											<section class="wrapper">';

												if( $gravatar != '' ) {
													echo $gravatar;
												}

												echo '<h3><a href="'. $authorurl .'">'. $displayname .'</a></h3>
												<div class="author_bio">'. $bio .'</div>
												<ul class="author_social">';

													if( $twitter ) {
														echo '<li>
															<a href="'. $twitter .'" target="_blank"><span class="icon-twitter"></span></a>
														</li>';
													}
													if( $facebook ) {
														echo '<li>
															<a href="'. $facebook .'" target="_blank"><span class="icon-facebook"></span></a>
														</li>';
													}
													if( $instagram ) {
														echo '<li>
															<a href="'. $instagram .'" target="_blank"><span class="icon-instagram"></span></a>
														</li>';
													}
													if( $pinterest ) {
														echo '<li>
															<a href="'. $pinterest .'" target="_blank"><span class="icon-pinterest"></span></a>
														</li>';
													}

												echo '</ul>

											</section>
										</section>';
									}
								?>

							</div>
						</div>

					<?php endwhile; else: ?>

						<p>Sorry, no posts matched your criteria.</p>

					<?php endif; ?>
				</div>


				<?php
					/**
					 * post navigation
					 */

					if( $market != 'virtual-run' ) { ?>

						<div class="post_nav">
							<div class="prev_post">
								<?php if( get_adjacent_post( false, '', true ) != '' ) { ?>
									<span><?php echo $postnav_previous; ?></span>
									<?php previous_post_link(); ?>
								<?php } ?>
							</div>
							<div class="next_post">
								<?php if( get_adjacent_post( false, '', false ) != '' ) { ?>
									<span><?php echo $postnav_next; ?></span>
									<?php next_post_link(); ?>
								<?php } ?>
							</div>
						</div>

					<?php }
				?>

			</div>

		</section>
	</main>

<?php if(get_current_blog_id() <= 1){
	get_footer('series');
} elseif( $market == 'bonus-track' || $market == 'rock-blog' || $market == 'virtual-run' ) {
	get_footer('series');
} else {
	get_footer();
} ?>
