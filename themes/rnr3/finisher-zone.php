<?php
	/* Template Name: Finisher Zone */
	global $wpdb;
	get_header('oneoffs');

	$market = get_market2();
	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$prefix       = '_rnr3_';
	$page_id      = get_the_ID();
	$qt_lang      = rnr3_get_language();
	$qt_status    = $qt_lang['enabled'] == 1 ? true : false;
	$qt_cur_lang  = $qt_lang['lang'];

	include 'languages.php';
	require_once 'functions_fz.php';

	$presenting_sponsor = $header_image = $header_blurb = '';

	$presenting_sponsor_img = get_post_meta( $page_id, $prefix . 'sponsor_img', true );

	if ( $presenting_sponsor_img != '' ) {
		$presenting_sponsor = '<img src="'. $presenting_sponsor_img .'">';

		$presenting_sponsor_url = get_post_meta( $page_id, $prefix . 'sponsor_url', true );

		if ( $presenting_sponsor_url != '' ) {
			$presenting_sponsor	= '<a href="'. $presenting_sponsor_url .'" target="_blank">'. $presenting_sponsor .'</a>';
		}

		$presenting_sponsor = '<div class="presented_by"><span>'. $presented_by_txt .'</span>'. $presenting_sponsor .'</div>';
	}


	if ( get_post_meta( $page_id, $prefix . 'special_header_img', true) != '' ) {
		$header_image = get_post_meta( $page_id, '_rnr3_special_header_img', true);
	}

	if ( get_post_meta( $page_id, $prefix . 'header_blurb', true) != '' ){
		$header_blurb = get_post_meta( $page_id, $prefix . 'header_blurb', true);
		$header_blurb = $qt_status ? qtrans_split( $header_blurb )[$qt_cur_lang] : $header_blurb;
	}

	$transient_expiration = '5 * MINUTE_IN_SECONDS';
?>
	<?php if( $header_image != '' ) : ?>
	<style>
		.narrow-hero{
			background:
				linear-gradient(to bottom, #000 0%, rgba(0,0,0,0) 15%),
				url('<?php echo $header_image; ?>') no-repeat center center;
		}
	</style>
	<?php endif; ?>

	<main role="main" id="main" class="no-hero finisher_zone">

		<nav id="subnav">
			<section class="wrapper">
				<a href="<?php echo esc_url( home_url() ); ?>" class="fz-home">Finisher Zone</a>
				<div class="subnav-menu-label">Menu</div>
				<div class="subnav-menu-primary-event-container">
					<ul class="subnav-menu">
						<li><a href="<?php echo esc_url( home_url() ); ?>/#past_results"><?php echo trim($results_txt); ?></a></li>
						<li><a href="<?php echo esc_url( home_url() ); ?>/#badges"><?php echo $finisher_badges_txt; ?></a></li>
						<li><a href="<?php echo esc_url( home_url() ); ?>/#personas"><?php echo $runner_personas_txt; ?></a></li>
						<li><a href="<?php echo esc_url( home_url() ); ?>/#more_features"><?php echo $features_txt; ?></a></li>
					</ul>
				</div>
				<?php echo $presenting_sponsor; ?>
			</section>
		</nav>
		<?php
			// display hero only when on LP (no parameters when in this tempalte)
			if( empty($_GET) && $header_blurb != '' ){
		?>
		<div class="narrow-hero">
			<section class="wrapper">
				<div class="copy">
					<h2>FINISHER ZONE</h2>
					<p><?php echo $header_blurb; ?></p>
				</div>
			</section>
		</div>
		<?php } ?>


		<?php if ( isset( $_GET['archive'] ) && empty( $_GET['archive'] ) ) {
			/* the archives */

			/*
			 * Trying to view the archive of events
			 * Pull in template
			 * Note: get_template_part() not used because function doesn't pass variables
			 */
			include( locate_template( 'template-parts/fz-archive.php' ) );

		} elseif ( !is_front_page() && !is_home() ){ ?>

			<section id="finisher-zone" class="wrapper">
				<?php
					if (have_posts()) : while (have_posts()) : the_post();
						echo '<h2>'. get_the_title(). '</h2>
						<div class="content">';
							the_content();
						echo '</div>';
					endwhile; endif;
				?>
			</section>

		<?php } else {
			/* default landing page */ ?>

			<section id="finisher-zone">
				<section class="wrapper">
					<h2><?php echo $latest_results_txt; ?></h2>
					<?php
						$finisher_zone_promos_array = get_finisher_zone_promo( $qt_cur_lang );
						$event_url = get_permalink() . 'search-and-results?eventid=';
					?>
					<div class="grid_3_special">

						<?php
							$first_event_id     = get_post_meta( $page_id, $prefix . 'event_select_1', true );
							$first_event        = get_fz_data('event', $first_event_id);
							$first_event_image  = !empty($first_event['image_display']) ? $first_event['image_display'] : $first_event['image_logo'];
							$first_event_title  = $qt_status ? qtrans_split( $first_event['display_title'] )[$qt_cur_lang] : $first_event['display_title'];
							$first_event_txt    = $qt_status ? qtrans_split( $first_event['display_text'] )[$qt_cur_lang] : $first_event['display_text'];
						?>
							<div class="column_special">
								<figure>
									<a href="<?php echo $event_url . $first_event_id; ?>"><img src="<?php echo $first_event_image; ?>"></a>
								</figure>
								<h3><a href="<?php echo $event_url . $first_event_id; ?>"><?php echo $first_event_title; ?></a></h3>
								<p><?php echo $first_event_txt; ?></p>
								<p><a class="cta" href="<?php echo $event_url . $first_event_id; ?>"><?php echo $view_results_txt; ?></a></p>
							</div>
						<?php
							for( $i = 1; $i < 2; $i++ ) {
								// first current event box
								$current_event_url = get_post_meta( $page_id, $prefix . 'current_url_' . $i, 1 );
								echo '<div class="column_special">
									<figure><a href="'. $current_event_url .'"><img src="'. get_post_meta( $page_id, $prefix . 'current_img_' . $i, 1 ) .'"></a></figure>
									'. apply_filters( 'the_content', get_post_meta( $page_id, $prefix . 'current_blurb_' . $i, 1 ) ) .'
									<p><a class="cta" href="'. $current_event_url .'">'. $readmore_txt .'</a></p>
								</div>';
							}

							// cap the row with a promotion
							echo $finisher_zone_promos_array[0];

							// make sure there's an image uploaded to the current event - image 3 spot
							// this would lead us to believe that there are 2 events
							//if( get_post_meta( $page_id, $prefix . 'current_img_3', 1 ) != '' ) {

							// new method: make sure that second event dropdown value is not empty
							$second_event_id = get_post_meta( $page_id, $prefix . 'event_select_2', true );

							if( $second_event_id != '' ){

								$second_event        = get_fz_data('event', $second_event_id);
								$second_event_image  = !empty($second_event['image_display']) ? $second_event['image_display'] : $second_event['image_logo'];
								$second_event_title  = $qt_status ? qtrans_split( $second_event['display_title'] )[$qt_cur_lang] : $second_event['display_title'];
								$second_event_txt    = $qt_status ? qtrans_split( $second_event['display_text'] )[$qt_cur_lang] : $second_event['display_text'];
							?>
								<div class="column_special">
									<figure>
										<a href="<?php echo $event_url . $second_event_id; ?>"><img src="<?php echo $second_event_image; ?>"></a>
									</figure>
									<h3><a href="<?php echo $event_url . $second_event_id; ?>"><?php echo $second_event_title; ?></a></h3>
									<p><?php echo $second_event_txt; ?></p>
									<p><a class="cta" href="<?php echo $event_url . $second_event_id; ?>"><?php echo $view_results_txt; ?></a></p>
								</div>

							<?php
								for( $i = 2; $i < 3; $i++ ) {
									// second current event box
									$current_event_url = get_post_meta( $page_id, $prefix . 'current_url_' . $i, 1 );
									echo '<div class="column_special">
										<figure><a href="'. $current_event_url .'"><img src="'. get_post_meta( $page_id, $prefix . 'current_img_' . $i, 1 ) .'"></a></figure>
										'. apply_filters( 'the_content', get_post_meta( $page_id, $prefix . 'current_blurb_' . $i, 1 ) ) .'
										<p><a class="cta" href="'. $current_event_url .'">'. $readmore_txt .'</a></p>
									</div>';
								}

								// cap the row with a promotion
								if( array_key_exists( 1, $finisher_zone_promos_array ) ) {
									echo $finisher_zone_promos_array[1];
								}
							}
						?>
					</div>
				</section>
			</section>

			<section id="past_results">
				<section class="wrapper">
					<h2><?php echo $past_results_txt; ?></h2>
					<?php
						$featured_events = array($first_event_id);
						if($second_event_id != '') array_push( $featured_events, $second_event_id );
						if ( false === ( $past_events_results = get_transient( 'past_events_results' ) ) ) {
							$past_events_query    =
							'SELECT * FROM `wp_fz_event`
							WHERE `pk_event_id` NOT IN (' . implode(',', $featured_events) . ') AND published = 1
							ORDER BY `date_start` DESC LIMIT 3';

							$past_events_results  = $wpdb->get_results( $past_events_query );
							set_transient( 'past_events_results', $past_events_results, $transient_expiration );
						}
						// print_r($past_events_results);

						if( $past_events_results != '' ) {
							echo '<div class="grid_3_special">';

								foreach( $past_events_results as $past_event ) {
									$event_page = get_blog_details( $past_event->blog_id );
									$event_url  = get_permalink() . 'search-and-results?eventid=' . $past_event->pk_event_id;
									echo '<div class="column_special">
										<figure><a href="'. $event_url .'"><img src="';
									if(!empty($past_event->image_display)){
										echo $past_event->image_display;
									}else{
										echo $past_event->image_logo;
									}
									echo '"></a></figure>
										<h3><a href="'. $event_url .'">'. stripslashes( $past_event->display_title ) .'</a></h3>';
									if($qt_lang['lang'] != 'en'){
									$startDateEx = explode("-",$past_event->date_start);
									echo '<p>'.$startDateEx[2].' '.ucfirst(get_month_by_language($qt_lang['lang'], ltrim($startDateEx[1],'0'))).' '.$startDateEx[0];
									if($past_event->date_start != $past_event->date_end){
										$endDateEx = explode("-",$past_event->date_end);
										echo " &ndash; ". $endDateEx[2].' '.ucfirst(get_month_by_language($qt_lang['lang'], ltrim($endDateEx[1],'0'))).' '.$endDateEx[0];
									}
									echo '</p>';
									}else{
									echo '<p>'.date( 'M d, Y', strtotime($past_event->date_start));
									if($past_event->date_start != $past_event->date_end){
										echo" &ndash; ". date( 'M d, Y', strtotime($past_event->date_end));
									}
									echo '</p>';
									}
									echo '<p><a href="'. $event_url .'">'. $view_results_txt .'</a> | <a href="'. $event_page->siteurl .'">'. $event_page_txt .'</a></p>
									</div>';
								}

							echo '</div>';

						}
					?>
					<p class="center"><a href="<?php echo get_permalink(); ?>?archive" class="cta"><?php echo $see_all_events_txt; ?></a></p>
				</section>
			</section>

			<section id="badges">
				<section class="wrapper">
					<h2><?php echo $finisher_badges_txt; ?></h2>
					<?php
						if ( false === ( $badges_results = get_transient( 'badges_results' ) ) ) {
							$badges_query   = 'SELECT * FROM `wp_fz_badges` WHERE `display_order` > 0 AND `display_order` < 13 ORDER BY `display_order`,`display_title` DESC LIMIT 12';
							$badges_results  = $wpdb->get_results( $badges_query );
							set_transient( 'badges_results', $badges_results, $transient_expiration );
						}
						// print_r($badges_results);

						if( $badges_results != '' ) {
							echo '<div class="grid_4_special">';
								foreach( $badges_results as $badge ) {
									$badge_title  = $qt_status ? qtrans_split( $badge->display_title )[$qt_cur_lang] : $badge->display_title;
									$badge_txt    = $qt_status ? qtrans_split( $badge->display_text )[$qt_cur_lang] : $badge->display_text;

									echo '<div class="column_special">
										<figure><img src="'. $badge->image_display .'" alt=""></figure>
										<h3>'. stripslashes( $badge_title ) .'</h3>
										<p>'. stripslashes( $badge_txt ) .'</p>
									</div>';
								}
							echo '</div>';
						}
					?>
				</section>
			</section>

			<?php
				if ( false === ( $persona_results = get_transient( 'persona_results' ) ) ) {
					$persona_query = 'SELECT * FROM `wp_fz_persona` WHERE 1 LIMIT 6';
					$persona_results  = $wpdb->get_results( $persona_query );
					set_transient( 'persona_results', $persona_results, $transient_expiration );
				}
				if( $persona_results != '' ) :?>
					<section id="personas">
						<section class="wrapper">
							<h2><?php echo $rnr_assessment_txt; ?></h2>
							<div class="grid_3_special">
							<?php
								foreach( $persona_results as $personaIDs ) {
									$persona_title  = $qt_status ? qtrans_split( $personaIDs->display_title )[$qt_cur_lang] : $personaIDs->display_title;
									$persona_txt    = !empty($personaIDs->front_page_text) ? $personaIDs->front_page_text : $personaIDs->display_text; //decide which display text to use
									$persona_txt    = $qt_status ? qtrans_split( $persona_txt )[$qt_cur_lang] : $persona_txt; //check if we are translating display text

									echo '<div class="column_special">
									<figure><img src="'. $personaIDs->image_display .'" alt=""></figure>
									<h3>'. stripslashes( $persona_title ) .'</h3>
									<p>'. stripslashes( $persona_txt ) .'</p>
									</div>';
								}
							?>
							</div>
						</section>
					</section>
				<?php endif;?>

			<section id="more_features">
				<section class="wrapper">
					<h2><?php echo $more_features_txt; ?></h2>
					<div class="grid_3_special">
						<?php
							for( $i = 1; $i < 7; $i++ ) {
								if(get_post_meta( $page_id, $prefix . 'mtt_img_' . $i, 1 ) != ''){
									echo '<div class="column_special">';
									$image_url_mf = get_post_meta( $page_id, $prefix . 'mtt_img_' . $i, 1 );
									if(!empty($image_url_mf)){
										echo '<figure><img src="'. get_post_meta( $page_id, $prefix . 'mtt_img_' . $i, 1 ) .'"></figure>';
									}
									echo apply_filters( 'the_content', get_post_meta( $page_id, $prefix . 'mtt_blurb_' . $i, 1 ) ) .'
									</div>';
								}
							}
						?>
					</div>
				</section>

			</section>

		<?php } ?>

	</main>

	<script>
		$(document).ready(function(){

				/**
				 * This part causes smooth scrolling using scrollto.js
				 * We target all a tags inside the nav, and apply the scrollto.js to it.
				 */
				$("#secondary a, .scrolly").click(function(evn){
						evn.preventDefault();
						$('html,body').scrollTo(this.hash, this.hash);
				});
		});
	</script>

	<?php get_global_overlay( $qt_lang ); ?>

	<?php get_footer('series'); ?>
