<?php
	/* Template Name: VIP */

	get_header();

	$parent_slug = the_parent_slug();
	rnr3_get_secondary_nav( $parent_slug );

	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$prefix = '_rnr3_';

	// counts
	$part_count = $pre_count = $post_count = $postconcert_count = 0;

	$qt_lang = rnr3_get_language();
	include 'languages.php';


	// contents
	$overview['content']	= apply_filters( 'the_content', get_the_content() );
	$purchase['content']	= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'purchase', true ) );
	$terms['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'terms', true ) );

	// participants
	$parking['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'parking', true ) );
	$lounge['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'indoor_area', true ) );
	$breakfast['content']	= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'breakfast', true ) );
	$brunch['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'brunch', true ) );
	$massage['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'massage', true ) );
	$yoga['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'yoga', true ) );
	$restrooms['content']	= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'restrooms', true ) );
	$gearcheck['content']	= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'gear_check', true ) );
	$bar['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'bar', true ) );
	$view['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'view', true ) );
	$shuttle['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'shuttle', true ) );
	$tents['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'changing_tents', true ) );
	$heaters['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'heaters', true ) );
	$swag['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'swag', true ) );

	// particpant counts... if 0 after all this, there was nothing filled in!
	if( $parking['content'] != '' )   { $part_count++; }
	if( $lounge['content'] != '' )    { $part_count++; }
	if( $breakfast['content'] != '' ) { $part_count++; }
	if( $brunch['content'] != '' )    { $part_count++; }
	if( $massage['content'] != '' )   { $part_count++; }
	if( $yoga['content'] != '' )      { $part_count++; }
	if( $restrooms['content'] != '' ) { $part_count++; }
	if( $gearcheck['content'] != '' ) { $part_count++; }
	if( $bar['content'] != '' )       { $part_count++; }
	if( $view['content'] != '' )      { $part_count++; }
	if( $shuttle['content'] != '' )   { $part_count++; }
	if( $tents['content'] != '' )     { $part_count++; }
	if( $heaters['content'] != '' )   { $part_count++; }
	if( $swag['content'] != '' )      { $part_count++; }

	// prerace
	$pre_private_area['content']	= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'pre_private_area', true ) );
	$pre_breakfast['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'pre_breakfast', true ) );
	$pre_restrooms['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'pre_restrooms', true ) );
	$pre_gear_check['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'pre_gear_check', true ) );
	$pre_parking['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'pre_parking', true ) );
	$pre_yoga['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'pre_yoga', true ) );
	$pre_shuttle['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'pre_shuttle', true ) );

	// prerace counts
	if( $pre_private_area['content'] != '' )  { $pre_count++; }
	if( $pre_breakfast['content'] != '' )     { $pre_count++; }
	if( $pre_restrooms['content'] != '' )     { $pre_count++; }
	if( $pre_gear_check['content'] != '' )    { $pre_count++; }
	if( $pre_parking['content'] != '' )       { $pre_count++; }
	if( $pre_yoga['content'] != '' )          { $pre_count++; }
	if( $pre_shuttle['content'] != '' )       { $pre_count++; }

	// postrace
	$post_private_area['content']	= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'post_private_area', true ) );
	$post_restrooms['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'post_restrooms', true ) );
	$post_buffet['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'post_buffet', true ) );
	$post_bar['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'post_bar', true ) );
	$post_parking['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'post_parking', true ) );
	$post_massage['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'post_massage', true ) );
	$post_changing_tents['content']	= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'post_changing_tents', true ) );
	$post_shuttle['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'post_shuttle', true ) );
	$post_view['content']			= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'post_view', true ) );

	// postrace concert
	$post_concert['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'post_concert', true ) );

	// postrace counts
	if( $post_private_area['content'] != '' )   { $post_count++; }
	if( $post_restrooms['content'] != '' )      { $post_count++; }
	if( $post_buffet['content'] != '' )         { $post_count++; }
	if( $post_bar['content'] != '' )            { $post_count++; }
	if( $post_parking['content'] != '' )        { $post_count++; }
	if( $post_massage['content'] != '' )        { $post_count++; }
	if( $post_changing_tents['content'] != '' ) { $post_count++; }
	if( $post_shuttle['content'] != '' )        { $post_count++; }
	if( $post_view['content'] != '' )           { $post_count++; }

	// postrace concert count
	if( $post_concert['content'] != '' )        { $postconcert_count++; }


	$program_array = array(
		$overview,
		$terms,
		$purchase
	);
	$participant_array = array(
		$parking,
		$shuttle,
		$lounge,
		$breakfast,
		$brunch,
		$massage,
		$yoga,
		$restrooms,
		$tents,
		$gearcheck,
		$bar,
		$heaters,
		$view,
		$swag
	);
	$prerace_array = array(
		$pre_private_area,
		$pre_breakfast,
		$pre_restrooms,
		$pre_gear_check,
		$pre_parking,
		$pre_yoga,
		$pre_shuttle
	);
	$postrace_array = array(
		$post_private_area,
		$post_restrooms,
		$post_buffet,
		$post_bar,
		$post_parking,
		$post_massage,
		$post_changing_tents,
		$post_shuttle,
		$post_view
	);
	$postconcert_array = array(
		$post_concert
	);

?>

<!-- main content -->
<main role="main" id="main">
	<section class="wrapper grid_2 offset240left">

		<div class="column sidenav">
			<div id="nav-anchor"></div>

			<nav class="sticky-nav">

				<?php
					/**
					 * montreal-specific changes
					 */
					if( $market == 'montreal' ) {
						if( $qt_lang['lang'] == 'fr' ) {
							$program_title = 'L\'espace Confort';
						} else {
							$program_title = 'Hospitality Lounge';
						}

					} else {
						if( $qt_lang['lang'] == 'fr' ) {
							$program_title = 'L\'espace Confort';
						} elseif( $qt_lang['lang'] == 'es' ) {
							$program_title = 'VIP';
						} elseif( $qt_lang['lang'] == 'pt' ) {
							$program_title = 'VIP';
						} else {
							$program_title = 'VIP Program';
						}
					}
				?>

				<h2><?php echo $program_title; ?></h2>
				<ul>
					<?php
						foreach( $program_array as $key => $value ) {
							if( $value['content'] != '' ) {
								echo '<li><a href="#program'.$key.'">'.$value['title'].'</a></li>';
							}
						}
					?>
				</ul>

				<?php if( $part_count > 0 ) {

					/**
					 * montreal-specific changes
					 */
					if( $market == 'montreal' ) {
						if( $qt_lang['lang'] == 'fr' ) {
							$participant_title = 'L\'Espace Confort &agrave; la ligne d\'arriv&eacute;e inclut';
						} else {
							$participant_title = 'Hospitality Lounge';
						}
					} else {
						if( $qt_lang['lang'] == 'fr' ) {
							$participant_title = 'L\'Espace Confort &agrave; la ligne d\'arriv&eacute;e inclut';
						} else {
							$participant_title = 'VIP Participant Amenities';
						}
					} ?>

					<h2><?php echo $participant_title; ?></h2>
					<ul>
						<?php
							foreach( $participant_array as $key => $value ) {
								if( $value['content'] != '' ) {
									echo '<li><a href="#part'. $key .'">'. $value['title'] .'</a></li>';
								}
							}
						?>
					</ul>

				<?php }


				if( $pre_count > 0 ) {
					/**
					 * montreal-specific changes
					 * didn't have any extra languages for this
					 */
					if( $market == 'montreal' ) {
						$prerace_only_title = 'Pre-Race Only Package';
					} else {
						$prerace_only_title = 'Pre-Race Only VIP Package';
					} ?>

					<h2><?php echo $prerace_only_title; ?></h2>
					<ul>
						<?php
							foreach( $prerace_array as $key => $value ) {
								if( $value['content'] != '' ) {
									echo '<li><a href="#spec'.$key.'">'.$value['title'].'</a></li>';
								}
							}
						?>
					</ul>

				<?php }


				if( $post_count > 0 ) {
					/**
					 * montreal-specific changes
					 * didn't have any extra languages for this
					 */
					if( $market == 'montreal' ) {
						$postrace_only_title = 'Post-Race Only Package';
					} else {
						$postrace_only_title = 'Post-Race Only VIP Package';
					} ?>

					<h2><?php echo $postrace_only_title; ?></h2>
					<ul>
						<?php
							foreach( $postrace_array as $key => $value ) {
								if( $value['content'] != '' ) {
									echo '<li><a href="#spec'.$key.'">'.$value['title'].'</a></li>';
								}
							}
						?>
					</ul>

				<?php }


				if( $postconcert_count > 0 ) {
					/**
					 * montreal-specific changes
					 * didn't have any extra languages for this
					 */
					if( $market == 'montreal' ) {
						$postrace_concert_title = 'Post-Race Concert';
					} else {
						$postrace_concert_title = 'Post-Race Concert VIP';
					} ?>

					<h2><?php echo $postrace_concert_title; ?></h2>
					<ul>
						<?php
							foreach( $postconcert_array as $key => $value ) {
								if( $value['content'] != '' ) {
									echo '<li><a href="#prc'.$key.'">'.$value['title'].'</a></li>';
								}
							}
						?>
					</ul>

				<?php } ?>

			</nav>
		</div>


		<div class="column">
			<div class="content">

				<?php if (have_posts()) : while (have_posts()) : the_post();

					echo '<h2>'. $program_title .'</h2>

					<section id="program0">';

						if( $qt_lang['lang'] == 'fr' ) {
							$overview_title = 'Description';
						} else {
							$overview_title = 'Overview';
						}

						echo '<h3>'. $overview_title .'</h3>
						'. $overview['content'] .'

					</section>';

				endwhile; endif;


				if( $purchase['content'] != '' ) {

					echo '<section class="glance_container">
						<div class="glance">
							<h3>'. $txt_purchase .'</h3>
							'. $purchase['content'] .'
						</div>
					</section>';

				}


				if( $part_count > 0 ) {

					echo '<h2>'. $participant_title .'</h2>';
					foreach( $participant_array as $key => $value ) {
						if( $value['content'] != '' ) {
							echo '<section id="part'. $key .'">
								<h3>'. $value['title'] .'</h3>
								'. $value['content'] .
							'</section>';
						}
					}

				}


				if( $pre_count > 0 ) {

					echo '<h2>'. $prerace_only_title .'</h2>';
					foreach( $prerace_array as $key => $value ) {
						if( $value['content'] != '' ) {
							echo '<section id="spec'. $key .'">
								<h3>'. $value['title'] .'</h3>
								'. $value['content'] .
							'</section>';
						}
					}

				}


				if( $post_count > 0 ) {

					echo '<h2>'. $postrace_only_title .'</h2>';
					foreach( $postrace_array as $key => $value ) {
						if( $value['content'] != '' ) {
							echo '<section id="spec'. $key .'">
								<h3>'. $value['title'] .'</h3>
								'. $value['content'] .
							'</section>';
						}
					}

				}


				if( $postconcert_count > 0 ) {

					echo '<h2>'. $postrace_concert_title .'</h2>';
					foreach( $postconcert_array as $key => $value ) {
						if( $value['content'] != '' ) {
							echo '<section id="prc'. $key .'">
								<h3>'. $value['title'] .'</h3>
								'. $value['content'] .
							'</section>';
						}
					}

				}


				if( $terms['content'] != '' ) {

					echo '<section id="program1">
						<h3>'. $terms['title'] .'</h3>
						'. $terms['content'] . '
					</section>';

				}


				if( $purchase['content'] != '' ) {

					echo '<section id="program2" class="glance_container">
						<div class="glance">
							<h3>'. $txt_purchase .'</h3>
							'. $purchase['content'] .'
						</div>
					</section>';
				} ?>

			</div>
		</div>

	</section>

	<script>
		$(document).ready(function(){
			/**
			 * This part causes smooth scrolling using scrollto.js
			 * We target all a tags inside the nav, and apply the scrollto.js to it.
			 */
			$(".sticky-nav a, .backtotop").click(function(evn){
				evn.preventDefault();
				$('html,body').scrollTo(this.hash, this.hash);
			});

		});
	</script>
</main>

<?php get_footer(); ?>
