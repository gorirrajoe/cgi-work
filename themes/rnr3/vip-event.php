<?php
	/* Template Name: VIP (Event) */
	get_header();

	$parent_slug = the_parent_slug();
	rnr3_get_secondary_nav( $parent_slug );

	$market = get_market2();

	if( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	// $prefix = '_rnr3_';

	$qt_lang = rnr3_get_language();
	include 'languages.php';

	$vipevents_meta	= get_meta( get_the_ID() );
	$vip_types		= array_key_exists( '_rnr3_vip_types', $vipevents_meta ) ? unserialize( $vipevents_meta['_rnr3_vip_types'] ) : '';

	/**
	 * store the content so we know if we should include the "overview" section
	 */
	if( have_posts() ) : while( have_posts() ) : the_post();

		$content = apply_filters( 'the_content', get_the_content() );

	endwhile; endif;

?>

<!-- main content -->
<main role="main" id="main" class="vip_event">
	<section class="wrapper grid_2 offset240left">

		<div class="column sidenav stickem">
			<div id="nav-anchor"></div>

			<nav class="sticky-nav">

				<h2><?php echo $vip_program_title; ?></h2>
				<ul>
					<?php
						if( $content != '' ) {
							echo '<li><a href="#vip-overview">'. $vip_overview_title .'</a></li>';
						}

						if( $vip_types != '' ) {
							foreach( $vip_types as $type ) {
								echo '<li><a href="#vip-'. $type .'">'. $vip_type_title[$type] .'</a></li>';
							}
						}
					?>
				</ul>

			</nav>
		</div>


		<div class="column">
			<div class="content">

				<h2><?php echo $vip_program_title; ?></h2>

				<?php if( $content != '' ) {

					echo '<section id="vip-overview">

						<h3>'. $vip_overview_title .'</h3>'.

						$content .'

					</section>';

				}


				/**
				 * platinum group
				 */
				if( $vip_types != '' && in_array( 'platinum', $vip_types ) ) {

					echo '<section id="vip-platinum">';

						/**
						 * platinum accordion
						 */
						if( array_key_exists( '_rnr3_platinum_raceit_id', $vipevents_meta ) ) {

							vip_price_raceit( 'platinum', $vipevents_meta, $event_info );

						} elseif( array_key_exists( '_rnr3_platinum_vip_prices', $vipevents_meta ) ) {

							$vip_prices = unserialize( $vipevents_meta['_rnr3_platinum_vip_prices'] );
							vip_price_accordion( 'platinum', $vip_prices, $vipevents_meta );

						}


						/**
						 * platinum text intro
						 */
						if( array_key_exists( '_rnr3_platinum_vip_exp_txt', $vipevents_meta ) ) {

							if( $qt_lang['enabled'] == 1 ) {

								$vip_exp_array = qtrans_split( $vipevents_meta['_rnr3_platinum_vip_exp_txt'] );
								echo apply_filters( 'the_content', $vip_exp_array[$qt_lang['lang']] );

							} else {
								echo apply_filters( 'the_content', $vipevents_meta['_rnr3_platinum_vip_exp_txt'] );
							}
						}


						/**
						 * platinum amenities
						 */
						if( array_key_exists( '_rnr3_platinum_vip_amenities_txt', $vipevents_meta ) ) {

							echo '<div class="amenity_list">';

								if( $qt_lang['enabled'] == 1 ) {

									$plat_vip_amenities_array = qtrans_split( $vipevents_meta['_rnr3_platinum_vip_amenities_txt'] );
									echo apply_filters( 'the_content', $plat_vip_amenities_array[$qt_lang['lang']] );

								} else {
									echo apply_filters( 'the_content', $vipevents_meta['_rnr3_platinum_vip_amenities_txt'] );
								}

							echo '</div>';

						} elseif( array_key_exists( '_rnr3_platinum_vip_amenities', $vipevents_meta ) ) {

							$vip_amenities = unserialize( $vipevents_meta['_rnr3_platinum_vip_amenities'] );

							echo '<ul class="amenity_list">';

								foreach( $vip_amenities as $vip_amenity ) {

									echo '<li>'. $vip_amenities_lang[$vip_amenity] .'</li>';

								}

							echo '</ul>';

						}

					echo '</section>';

				}


				/**
				 * vip experience group
				 */
				if( $vip_types != '' && in_array( 'vip', $vip_types ) ) {

					echo '<section id="vip-vip">';

						/**
						 * vip accordion
						 */
						if( array_key_exists( '_rnr3_vip_raceit_id', $vipevents_meta ) ) {

							vip_price_raceit( 'vip', $vipevents_meta, $event_info );

						} elseif( array_key_exists( '_rnr3_vip_prices', $vipevents_meta ) ) {

							$vip_prices = unserialize( $vipevents_meta['_rnr3_vip_prices'] );
							vip_price_accordion( 'vip', $vip_prices, $vipevents_meta );

						}


						/**
						 * vip text intro
						 */
						if( array_key_exists( '_rnr3_vip_exp_txt', $vipevents_meta ) ) {

							if( $qt_lang['enabled'] == 1 ) {

								$vip_exp_array = qtrans_split( $vipevents_meta['_rnr3_vip_exp_txt'] );
								echo apply_filters( 'the_content', $vip_exp_array[$qt_lang['lang']] );

							} else {
								echo apply_filters( 'the_content', $vipevents_meta['_rnr3_vip_exp_txt'] );
							}
						}


						/**
						 * vip amenities
						 */
						if( array_key_exists( '_rnr3_vip_amenities_txt', $vipevents_meta ) ) {

							echo '<div class="amenity_list">';

								if( $qt_lang['enabled'] == 1 ) {

									$vip_amenities_array = qtrans_split( $vipevents_meta['_rnr3_vip_amenities_txt'] );
									echo apply_filters( 'the_content', $vip_amenities_array[$qt_lang['lang']] );

								} else {
									echo apply_filters( 'the_content', $vipevents_meta['_rnr3_vip_amenities_txt'] );
								}

							echo '</div>';

						} elseif( array_key_exists( '_rnr3_vip_amenities', $vipevents_meta ) ) {

							$vip_amenities = unserialize( $vipevents_meta['_rnr3_vip_amenities'] );

							echo '<ul class="amenity_list">';

								foreach( $vip_amenities as $vip_amenity ) {

									echo '<li>'. $vip_amenities_lang[$vip_amenity] .'</li>';

								}

							echo '</ul>';

						}

					echo '</section>';

				}


				/**
				 * prerace group
				 */
				if( $vip_types != '' && in_array( 'prerace', $vip_types ) ) {

					echo '<section id="vip-prerace">';

						/**
						 * prerace vip accordion
						 */
						if( array_key_exists( '_rnr3_prerace_raceit_id', $vipevents_meta ) ) {

							vip_price_raceit( 'prerace', $vipevents_meta, $event_info );

						} elseif( array_key_exists( '_rnr3_prerace_vip_prices', $vipevents_meta ) ) {

							$prerace_vip_prices = unserialize( $vipevents_meta['_rnr3_prerace_vip_prices'] );
							vip_price_accordion( 'prerace', $prerace_vip_prices, $vipevents_meta );

						}


						/**
						 * prerace vip text intro
						 */
						if( array_key_exists( '_rnr3_prerace_vip_exp_txt', $vipevents_meta ) ) {

							if( $qt_lang['enabled'] == 1 ) {

								$prerace_vip_exp_array = qtrans_split( $vipevents_meta['_rnr3_prerace_vip_exp_txt'] );
								echo apply_filters( 'the_content', $prerace_vip_exp_array[$qt_lang['lang']] );

							} else {
								echo apply_filters( 'the_content', $vipevents_meta['_rnr3_prerace_vip_exp_txt'] );

							}
						}


						/**
						 * prerace vip amenities
						 */
						if( array_key_exists( '_rnr3_prerace_vip_amenities_txt', $vipevents_meta ) ) {

							echo '<div class="amenity_list">';

								if( $qt_lang['enabled'] == 1 ) {

									$prerace_vip_amenities_array = qtrans_split( $vipevents_meta['_rnr3_prerace_vip_amenities_txt'] );
									echo apply_filters( 'the_content', $prerace_vip_amenities_array[$qt_lang['lang']] );

								} else {
									echo apply_filters( 'the_content', $vipevents_meta['_rnr3_prerace_vip_amenities_txt'] );
								}

							echo '</div>';

						} elseif( array_key_exists( '_rnr3_prerace_vip_amenities', $vipevents_meta ) ) {

							$prerace_vip_amenities = unserialize( $vipevents_meta['_rnr3_prerace_vip_amenities'] );

							echo '<ul class="amenity_list">';

								foreach( $prerace_vip_amenities as $prerace_vip_amenity ) {

									echo '<li>'. $vip_amenities_lang[$prerace_vip_amenity] .'</li>';

								}

							echo '</ul>';

						}


					echo '</section>';

				}


				/**
				 * postrace group
				 */
				if( $vip_types != '' && in_array( 'postrace', $vip_types ) ) {

					echo '<section id="vip-postrace">';

						/**
						 * postrace vip accordion
						 */
						if( array_key_exists( '_rnr3_postrace_raceit_id', $vipevents_meta ) ) {

							vip_price_raceit( 'postrace', $vipevents_meta, $event_info );

						} elseif( array_key_exists( '_rnr3_postrace_vip_prices', $vipevents_meta ) ) {

							$postrace_vip_prices = unserialize( $vipevents_meta['_rnr3_postrace_vip_prices'] );
							vip_price_accordion( 'postrace', $postrace_vip_prices, $vipevents_meta );

						}


						/**
						 * postrace vip text intro
						 */
						if( array_key_exists( '_rnr3_postrace_vip_exp_txt', $vipevents_meta ) ) {

							if( $qt_lang['enabled'] == 1 ) {

								$postrace_vip_exp_array = qtrans_split( $vipevents_meta['_rnr3_postrace_vip_exp_txt'] );
								echo apply_filters( 'the_content', $postrace_vip_exp_array[$qt_lang['lang']] );

							} else {
								echo apply_filters( 'the_content', $vipevents_meta['_rnr3_postrace_vip_exp_txt'] );
							}
						}


						/**
						 * postrace vip amenities
						 */
						if( array_key_exists( '_rnr3_postrace_vip_amenities_txt', $vipevents_meta ) ) {

							echo '<div class="amenity_list">';

								if( $qt_lang['enabled'] == 1 ) {

									$postrace_vip_amenities_array = qtrans_split( $vipevents_meta['_rnr3_postrace_vip_amenities_txt'] );
									echo apply_filters( 'the_content', $postrace_vip_amenities_array[$qt_lang['lang']] );

								} else {
									echo apply_filters( 'the_content', $vipevents_meta['_rnr3_postrace_vip_amenities_txt'] );
								}

							echo '</div>';

						} elseif( array_key_exists( '_rnr3_postrace_vip_amenities', $vipevents_meta ) ) {

							$postrace_vip_amenities = unserialize( $vipevents_meta['_rnr3_postrace_vip_amenities'] );

							echo '<ul class="amenity_list">';

								foreach( $postrace_vip_amenities as $postrace_vip_amenity ) {

									echo '<li>'. $vip_amenities_lang[$postrace_vip_amenity] .'</li>';

								}

							echo '</ul>';

						}

					echo '</section>';

				}


				/**
				 * legal text
				 */
				if( array_key_exists( '_rnr3_vip_legal', $vipevents_meta ) ) {

					echo '<section id="vip-legal">';

						if( $qt_lang['enabled'] == 1 ) {

							$vip_legal_array = qtrans_split( $vipevents_meta['_rnr3_vip_legal'] );
							echo apply_filters( 'the_content', $vip_legal_array[$qt_lang['lang']] );

						} else {
							echo apply_filters( 'the_content', $vipevents_meta['_rnr3_vip_legal'] );
						}

					echo '</section>';

				} ?>

			</div>
		</div>

	</section>


	<script>
		$(document).ready(function() {
			/**
			 * This part causes smooth scrolling using scrollto.js
			 * We target all a tags inside the nav, and apply the scrollto.js to it.
			 */
			$( ".sticky-nav a, .backtotop" ).click( function( evn ) {
				evn.preventDefault();
				$( 'html,body' ).scrollTo( this.hash, this.hash );
			});

			/*$( 'main' ).stickem( {
				container: '.offset240left',
			} );*/

			/**
			 * This part handles the highlighting functionality.
			 * We use the scroll functionality again, some array creation and
			 * manipulation, class adding and class removing, and conditional testing
			 */
			var aChildren	= $( ".sticky-nav li" ).children(); // find the a children of the list items
			var aArray		= []; // create the empty aArray

			for( var i=0; i < aChildren.length; i++ ) {
				var aChild	= aChildren[i];
				var ahref	= $( aChild ).attr( 'href' );
				aArray.push( ahref );
			} // this for loop fills the aArray with attribute href values

			$( window ).scroll( function() {
				var windowPos		= $( window ).scrollTop(); // get the offset of the window from the top of page
				var windowHeight	= $( window ).height(); // get the height of the window
				var docHeight		= $( document ).height();

				for( var i=0; i < aArray.length; i++ ) {
					var theID		= aArray[i];
					var divPos		= $(theID).offset().top; // get the offset of the div from the top of page
					var divHeight	= $(theID).height(); // get the height of the div in question

					if( windowPos >= ( divPos - 1 ) && windowPos < ( divPos + divHeight -1 ) ) {
						$("a[href='" + theID + "']").addClass("nav-active");
					} else {
						$("a[href='" + theID + "']").removeClass("nav-active");
					}
				}

				if( windowPos + windowHeight == docHeight ) {
					if( !$( ".sticky-nav li:last-child a" ).hasClass( "nav-active" ) ) {
						var navActiveCurrent = $( ".nav-active" ).attr( "href" );
						$( "a[href='" + navActiveCurrent + "']" ).removeClass( "nav-active" );
						$( ".sticky-nav li:last-child a" ).addClass( "nav-active" );
					}
				}
			});

		});
	</script>
</main>


<?php
	get_footer();


	/**
	 * overall function to create the accordions on the page
	 * there could be max 3 accordion groups on the page
	 */
	function vip_price_accordion( $vip_type, $vip_prices, $vipevents_meta ) {
		$market = get_market2();

		if( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
			$event_info = rnr3_get_event_info( $market );
		}

		$qt_lang = rnr3_get_language();
		include 'languages.php';

		// $vipevents_meta	= get_meta( get_the_ID() );
		$current_date	= strtotime( date( 'l, F j g:i:s A' ) );

		$vip_price_array = array();

		$vip_url = array_key_exists( '_rnr3_vip_url', $vipevents_meta ) ? $vipevents_meta['_rnr3_vip_url'] : '#';

		$prices_count = count( $vip_prices );


		/**
		 * set up prices array
		 */
		for( $i = 0; $i < $prices_count; $i++ ) {

			$price_data = array();

			$price_start = array_key_exists( 'price_start', $vip_prices[$i] ) ? strtotime( $vip_prices[$i]['price_start'] ) : '';

			if( $vip_prices[$i]['price_end'] != '' ) {
				$price_end = $vip_prices[$i]['price_end'] . ' 07:59:59 PM';
			}
			$price_end = strtotime( $price_end );

			$price					= $vip_prices[$i]['price'];
			$price_verbiage			= $vip_prices[$i]['price_msg'];
			$price_custom_verbiage	= array_key_exists( 'price_custom_msg', $vip_prices[$i] ) ? $vip_prices[$i]['price_custom_msg'] : '';

			$price_data = array(
				'start'		=> $price_start,
				'end'		=> $price_end,
				'price'		=> $price,
				'verbiage'	=> $price_verbiage,
				'custom'	=> $price_custom_verbiage
			);
			$vip_price_array[] = $price_data;

		}



		/**
		 * show the correct price based on date
		 * once current date is EARLIER THAN end date, use that price
		 * then break out
		 */
		foreach( $vip_price_array as $vip_price_indiv ) {

			if( $current_date < $vip_price_indiv['end'] ) {
				$price_display = $vip_price_indiv['price'];
				break;
			} else {
				$price_display = $txt_TBD;
			}

		}

		if( !empty( $vip_price_array ) ) {

			echo '<div class="accordion">
				<div class="accordion-section" style="position: relative;">

					<div class="accordion-section-title" data-reveal="#'. $vip_type .'">'. $vip_type_title[$vip_type] .' &mdash; '. $price_display .'</div>
					<a class="cta" href="'. $vip_url .'" target="_blank">'. $purchase['title'] .'</a>';

					// $current_plus1 = 0;

					echo '<ul id="'. $vip_type .'" class="accordion-section-content" style="display: none;">';

						foreach( $vip_price_array as $vip_price_indiv ) {

							if( $vip_price_indiv['verbiage'] == 'tbd' ) {
								echo '<li class="tier">
									<strong>'. $vip_price_indiv['price'] .'</strong>
								</li>';
							} else {

								if( $current_date > $vip_price_indiv['end'] ) {
									$strike_style	= ' price_inactive';
									// $donotshow		= 1;
								} else {
									$strike_style	= '';
									// $donotshow		= 0;
								}

								if( $qt_lang['lang'] == 'es' ) {
									$display_start_date	= $vip_price_indiv['start'] != '' ? date( 'j.n.y', $vip_price_indiv['start'] ) : '';
									$display_end_date	= date( 'j.n.y', $vip_price_indiv['end'] );
								} elseif( $qt_lang['lang'] == 'fr' ) {
									$display_start_date	= $vip_price_indiv['start'] != '' ? date( 'j/n/y', $vip_price_indiv['start'] ) : '';
									$display_end_date	= date( 'j/n/y', $vip_price_indiv['end'] );
								} else {
									$display_start_date	= $vip_price_indiv['start'] != '' ? strtoupper( date( 'M j', $vip_price_indiv['start'] ) ) : '';
									$display_end_date	= strtoupper( date('M j', $vip_price_indiv['end'] ) );
								}

								if( $vip_price_indiv['verbiage'] == "standard" ) {

									// if( $donotshow == 0 ) {
										echo '<li class="tier'.$strike_style.'">
											 <h4>'. $display_start_date .' ' . $through_text . ' ' . $display_end_date .' </h4>
											 <div class="price">'. $vip_price_indiv['price'] .'</div>
										</li>';
										// $current_plus1++;
									// }

								} elseif( $vip_price_indiv['verbiage'] == 'through' ) {

									// if( $donotshow == 0 ) {
										echo '<li class="tier'.$strike_style.'">
											<h4>'. $through_text .' '. $display_end_date .' </h4>
											<div class="price">'. $vip_price_indiv['price'] .'</div>
										</li>';
										// $current_plus1++;
									// }

								} elseif( $vip_price_indiv['verbiage'] == 'atexpo' ) {

									// if( $donotshow == 0 ) {
										echo '<li class="tier'.$strike_style.'">
											<h4>'. $at_expo_text .'</h4>
											<div class="price">'. $vip_price_indiv['price'] .'</div>
										</li>';
										// $current_plus1++;
									// }

								} elseif( $vip_price_indiv['verbiage'] == 'custom' ) {

									// if( $donotshow == 0 ) {
										echo '<li class="tier'.$strike_style.'">
											<h4>'. $vip_price_indiv['custom'] .'</h4>
											<div class="price">'. $vip_price_indiv['price'] .'</div>
										</li>';
										// $current_plus1++;
									// }

								}
							}

						}

					echo '</ul>
				</div>
			</div>';

		}

	}


	/**
	 * function to grab dynamic raceit data
	 * data stored in wp_registration_prices
	 *
	 * @param  [str] $vip_type       [type of vip]
	 * @param  [obj] $vipevents_meta [all meta data on page]
	 * @param  [obj] $event_info     [for languages.php]
	 * @return echoes div
	 */
	function vip_price_raceit( $vip_type, $vipevents_meta, $event_info ) {
		global $wpdb;

		$qt_lang = rnr3_get_language();
		include 'languages.php';

		$raceit_id = $vipevents_meta['_rnr3_'. $vip_type .'_raceit_id'];

		// get raceit stuffs
		$price_display = $wpdb->get_results(
			$wpdb->prepare(
				"
					SELECT current_price, currency_symbol, iso_currency
					FROM wp_registration_prices
					WHERE division_id = %d
				",
				$raceit_id
			)
		);

		$vip_url		= array_key_exists( '_rnr3_vip_url', $vipevents_meta ) ? $vipevents_meta['_rnr3_vip_url'] : '#';

		$iso_currency	= $price_display[0]->iso_currency != 'USD' ? $price_display[0]->iso_currency : '';
		$price_display	= $price_display[0]->currency_symbol . $price_display[0]->current_price . ' ' . $iso_currency;

		echo '<div class="vip_pricebox">
			<div class="vip_pricebox-title">'. $vip_type_title[$vip_type] .' &mdash; '. $price_display .'</div>
			<a class="cta" href="'. $vip_url .'" target="_blank">'. $purchase['title'] .'</a>
		</div>';


	}
?>
