<?php
	/* Template Name: Rock Idol - Dublin contest */
	get_header();
	$qt_lang = rnr3_get_language();

	rnr3_get_secondary_nav( '', 'rock-idol' );
	$northamerican_array = get_non_us_na_races();

	if ( $qt_lang['lang'] == 'es' ) {
		if( in_array( $event_info->event_slug, $northamerican_array ) ) {
			$txt_and_more = "TBD";
		} else {  // euro spanish
			$txt_and_more = "&iexcl;Y M&aacute;s!";
		}
	} elseif ( $qt_lang['lang'] == 'fr' ) {
		if( in_array( $event_info->event_slug, $northamerican_array ) ) {
			$txt_and_more = "and more!";
		} else { // euro french
			$txt_and_more = "and more!";
		}
	} elseif ( $qt_lang['lang'] == 'pt' ) {
		$txt_and_more = "and more!";
	} else {
		$txt_and_more = "and more!";
	}

	// get page ID for use in fields
	$theID  = get_the_ID();

	// helper function for recursive array_filter
	function arrayFilter( $array ) {
		if ( !empty( $array ) )
			return array_filter( $array );
	}
?>
	<main role="main" id="main">
		<!-- intro -->
		<section id="intro">

			<section class="wrapper">
				<?php
					// get all necessary data for intro
					$intro_txt   = apply_filters( 'the_content', get_post_meta( $theID, '_rnr3_intro_txt', true ) ?: '' );
				?>

				<?php echo $intro_txt; ?>

			</section>

		</section>

		<!-- The Finalists -->
		<?php
			// get all necessary data for The Finalists section
			$finalist_icon		= get_post_meta( $theID, '_rnr3_finalist_icon', true ) ?: '';
			$finalist_txt		= apply_filters( 'the_content', get_post_meta( $theID, '_rnr3_finalist_txt', true ) ?: '' );
			$finalists_form		= do_shortcode( get_post_meta( $theID, '_rnr3_finalists_surveygizmo', true ) ?: '' );
			$finalists_group	= get_post_meta( $theID, '_rnr3_finalists_group', true ) ?: array();

			$finalists_group	= array_filter( $finalists_group, 'arrayFilter' );
		?>

		<?php if ( ( !empty( $finalist_icon ) && !empty( $finalists_txt ) ) || !empty( $finalists_group ) ) { ?>
		<section id="the-finalists">

			<section class="wrapper grid_1">

				<div class="icon">
					<img src="<?php echo $finalist_icon; ?>" alt="">
				</div>
				<?php echo $finalist_txt; ?>

			</section>

			<?php if ( !empty( $finalists_group ) ) : ?>

			<div class="wrapper">

				<section class="grid_3_special the-finalists">

				<?php foreach ( $finalists_group as $k => $finalist ):
					$image_headshot = !empty( $finalist['image'] ) ? $finalist['image'] : ''; ?>

					<div class="column_special">
						<a data-fancybox class="finalist fancybox" href="javascript:;" data-src="#finalist-<?php echo $k; ?>">
							<div class="finalist-image" style="background-image: url(<?php echo $image_headshot; ?>);"></div>
							<h4><?php echo $finalist['name']; ?></h4>
							<div class="finalist-city"><?php echo $finalist['city']; ?></div>
						</a>
						<a class="cta fancybox" href="#finalist-<?php echo $k; ?>">View Bio</a>
					</div>

					<!-- HIDDEN MODAL TARGET -->
					<div id="finalist-<?php echo $k; ?>" class="wrapper grid_3" style="display:none; width: 800px; max-width: 90%;">

						<div class="column">
							<?php
								if( !empty($finalist['gallery']) ){
									echo '<div class="finalist-gallery">';
										foreach( $finalist['gallery'] as $image_id => $gallery_image ){
											echo wp_get_attachment_image( $image_id, 'medium' );
										}
									echo '</div>';
								}
							?>
							<div id="ribbon">
								<?php if( !empty($finalist['social_twitter']) ){ ?>
								<div class="twitter"><a target="_blank" href="<?php echo $finalist['social_twitter']; ?>"><span class="icon-twitter"></span></a></div>
								<?php } if( !empty($finalist['social_fb']) ){ ?>
								<div class="facebook"><a target="_blank" href="<?php echo $finalist['social_fb']; ?>"><span class="icon-facebook"></span></a></div>
								<?php } if( !empty($finalist['social_instagram']) ){ ?>
								<div class="instagram"><a target="_blank" href="<?php echo $finalist['social_instagram']; ?>"><span class="icon-instagram"></span></a></div>
								<?php } ?>
							</div>
						</div>

						<div class="column_2">
							<h4><?php echo $finalist['name']; ?></h4>
							<div class="finalist-city"><?php echo $finalist['city']; ?></div>
							<?php echo apply_filters( 'the_content', $finalist['copy'] ); ?>
						</div>

						<?php if( !empty( $finalist['quote'] ) ){ ?>
							<h3 class="quote">"<?php echo $finalist['quote']; ?>"</h3>
						<?php } ?>
						<h3><a href="#" class="modal-vote cta">Vote for <?php echo $finalist['name']; ?></a></h3>
					</div>

				<?php endforeach; //end foreach finalist ?>

				</section>

			</div>

			<?php endif; // end !empty( $finalists_group ) ?>

			<div class="wrapper" id="finalist-form">
				<?php echo $finalists_form; ?>
			</div>

		</section>
		<?php } // end check for finalists variables ?>

		<?php
			// get all necessary data for how to enter
			$how_to_enter_icon		= get_post_meta( $theID, '_rnr3_how_to_enter_icon', true ) ?: false;
			$how_to_enter_txt		= apply_filters( 'the_content', get_post_meta( $theID, '_rnr3_how_to_enter_txt', true ) ?: false );
			$how_to_enter_survey	= do_shortcode( get_post_meta( $theID, '_rnr3_surveygizmo', true ) ?: false );

			if ( $how_to_enter_icon || $how_to_enter_txt || $how_to_enter_survey ):
		?>
			<!-- How to Enter -->
			<section id="how-to-enter">
				<section class="wrapper grid_1">

					<?php if( $how_to_enter_icon ): ?>
					<div class="icon">
						<img src="<?php echo $how_to_enter_icon; ?>" alt="">
					</div>
					<?php endif; ?>

					<?php echo $how_to_enter_txt; ?>

					<?php echo $how_to_enter_survey; ?>

				</section>
			</section>
		<?php endif; ?>

		<!-- The Package -->
		<?php
			// get all necessary data for The Package
			$package_icon		= get_post_meta( $theID, '_rnr3_package_icon', true ) ?: '';
			$package_txt		= apply_filters( 'the_content', get_post_meta( $theID, '_rnr3_package_txt', true ) ?: '' );
			$package_imagery	= get_post_meta( $theID, '_rnr3_package_imagery', true ) ?: '';
			$package_imagery_id	= wp_get_attachment_image( get_post_meta( $theID, '_rnr3_package_imagery_id', 1 ), 'travel-hotel' );
			$packages			= get_post_meta( $theID, '_rnr3_package_group', true ) ?: array();

			$packages			= array_filter( $packages, 'arrayFilter' );

			if ( !empty( $packages ) ) :
		?>

		<section id="the-package">

			<section class="wrapper grid_1">

				<div class="icon"><img src="<?php echo $package_icon; ?>" alt=""></div>

				<div class="package-copy">
					<?php echo $package_txt; ?>
				</div>

				<div class="showmobile">
					<?php echo $package_imagery_id; ?>
				</div>

				<h2>Includes</h2>
				<section class="grid_2">
					<div class="column">
						<section class="grid_2_special packages">
						<?php foreach( $packages as $package ){ ?>
							<div class="column_special narrow package">
								<img src="<?php echo $package['image']; ?>" alt="Run Rock 'n' Roll - Rock Idol Package - <?php echo $package['title']; ?>" />
								<h3><?php echo $package['title']; ?></h3>
							</div>
						<?php } ?>
						</section>
						<h2 class="and-more"><?php echo $txt_and_more; ?></h2>
					</div>
					<div class="column showdesktop">
						<img src="<?php echo $package_imagery; ?>" alt="Run Rock 'n' Roll - Rock Idol ">
					</div>

				</section>

			</section>

		</section>

		<?php endif; // end !empty( $packages ) ?>

	</main>
	<script>
		$(document).ready(function(){
			$('.fancybox').fancybox({
				baseTpl: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
					'<div class="fancybox-bg"></div>' +
					'<div class="fancybox-controls">' +
						'<div class="fancybox-infobar">' +
							'<button data-fancybox-previous class="fancybox-button fancybox-button--left" title="Previous"></button>' +
							'<div class="fancybox-infobar__body">' +
								'<span class="js-fancybox-index"></span>&nbsp;/&nbsp;<span class="js-fancybox-count"></span>' +
							'</div>' +
							'<button data-fancybox-next class="fancybox-button fancybox-button--right" title="Next"></button>' +
						'</div>' +
						'<div class="fancybox-buttons">' +
							'<button data-fancybox-close class="fancybox-button fancybox-button--close" title="Close (Esc)"></button>' +
						'</div>' +
					'</div>' +
					'<div class="fancybox-slider-wrap fancybox-rock-idol">' +
						'<div class="fancybox-slider"></div>' +
					'</div>' +
					'<div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div>' +
				'</div>',
				beforeLoad: function(){
					$('.modal-vote').on('click', function(e){
						e.preventDefault();

						$.fancybox.close();

						$('html, body').animate({
							scrollTop: $('#finalist-form').offset().top
						}, 500);
					});
				},
			});

			$('.finalist-gallery').owlCarousel({
				singleItem: true,
				autoPlay: true,
				slideSpeed: 300,
				pagination: true
			});
		});
	</script>
<?php
	get_global_overlay( $qt_lang );
	get_footer('oneoffs');
?>
