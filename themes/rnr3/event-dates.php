<?php
	/* Template Name: Event Dates */
	get_header( 'series-home' );
	$qt_lang = rnr3_get_language();

	$disclaimer	= get_post_meta( get_the_ID(), '_rnr3_disclaimer_txt', true );
	$overview	= rnr3_get_all_events();
?>
<main role="main" id="main">
	<section class="wrapper grid_1">
		<div class="content">

			<h2><?php the_title(); ?></h2>
			<p><?php echo apply_filters( 'the_content',  $disclaimer); ?></p>

			<table class="grid sortable" id="eventDate" style="margin-bottom: 30px; width: 100%;" border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr class="rowheader">
						<td width="25%"><strong>Event</strong></td>
						<td width="19%"><strong>Event Date</strong></td>
						<td width="21%" class="sorttable_nosort" ><strong>Course Time Limits*</strong> (Hrs.)</td>
						<td width="23%" class="sorttable_nosort"><strong>Deadline for Race Changes </strong></td>
					</tr>
				</thead>

				<tbody>
					<?php
						foreach( (array) $overview as $key => $value) {

							$city = $start_date = $end_date = $slug = $event_date = $all_metas = $deadline = $limit_half = $limit_10k = $limit_full = '';

							$has_races	= $value->has_races;
							$city		= $value->event_name ? $value->event_name : '';
							$start_date	= $value->festival_start_date ? $value->festival_start_date : '';
							$end_date	= $value->festival_end_date ? $value->festival_end_date : '';
							$slug		= $value->event_slug ? $value->event_slug : '';
							$event_date	= $value->event_date ? $value->event_date : '';
							$all_metas	= rnr3_get_meta_from_blog( $slug, array( 'limit_half' => '_rnr3_half_time_limit', 'limit_full' => '_rnr3_full_time_limit', 'limit_10k' => '_rnr3_tenk_time_limit', 'deadline' => '_rnr3_change_deadline' ) );
							$deadline	= isset( $all_metas['deadline'] ) ? $all_metas['deadline'] : '' ;

							if( ( strpos( $has_races, 'tenk' ) !== false ) && isset( $all_metas['limit_10k'] ) ) {
								$limit_10k =  $all_metas['limit_10k'] ;
							}

							if( ( strpos( $has_races, 'half' ) !== false ) && isset( $all_metas['limit_half'] ) ) {
								$limit_half =  $all_metas['limit_half'] ;
							}

							if( ( strpos( $has_races, 'full' ) !== false ) && isset( $all_metas['limit_full'] ) ) {
								$limit_full =  $all_metas['limit_full'] ;
							}

							if( !empty( $limit_full ) ) {
								if( $limit_full !='TBD') {
									/**
									 * for foreign languages
									 * split the content around the [*] brackets
									 * then do the preg_match_all based off spaces
									 */
									if( strpos( $limit_full, '[:' ) !== false ) {
										$limit_full_split = preg_split( '(\\[.*?\\])', $limit_full );
										preg_match_all( '!\d+!', $limit_full_split[1], $hours );
									} else {

										preg_match_all( '!\d+!', $limit_full, $hours );

									}


									$hours_and_mins = count( $hours[0] );

									if( $hours_and_mins <= 1 ) {
										$limit_full = $hours[0][0];
									} else {
										$mins		= ( $hours[0][1] ) / 60;
										$limit_full	= number_format( $hours[0][0] + $mins, 2 );
									}
								}
							}

							if( !empty( $limit_half ) ) {
								if( $limit_half != 'TBD' ) {
									/**
									 * for foreign languages
									 * split the content around the [*] brackets
									 * then do the preg_match_all based off spaces
									 */
									if( strpos( $limit_half, '[:' ) !== false ) {
										$limit_half_split = preg_split( '(\\[.*?\\])', $limit_half );
										preg_match_all( '!\d+!', $limit_half_split[1], $half_hours );
									} else {
										preg_match_all( '!\d+!', $limit_half, $half_hours );
									}

									$half_hours_and_mins = count( $half_hours[0] );

									if( $half_hours_and_mins <= 1 ) {
										$limit_half = $half_hours[0][0];
									} else {
										$mins		= ( $half_hours[0][1] ) / 60;
										$limit_half	= number_format( $half_hours[0][0] + $mins, 2 );
									}
								}
							}

							if( !empty( $limit_10k ) ) {
								if( $limit_10k !='TBD' ) {
									/**
									 * for foreign languages
									 * split the content around the [*] brackets
									 * then do the preg_match_all based off spaces
									 */
									if( strpos( $limit_10k, '[:' ) !== false ) {
										$limit_10k_split = preg_split( '(\\[.*?\\])', $limit_10k );
										preg_match_all( '!\d+!', $limit_10k_split[1], $hours_10k );
									} else {
										preg_match_all( '!\d+!', $limit_10k, $hours_10k );
									}

									$hours_10k_and_mins = count( $hours_10k[0] );

									if( $hours_10k_and_mins <= 1 ) {
										$limit_10k = $hours_10k[0][0];
									} else {
										$mins		= ( $hours_10k[0][1] )/60;
										$limit_10k	= number_format( $hours_10k[0][0] + $mins , 2 );
									}
								}
							}


							if( $deadline  != '' ) {
								$deadline_date = date( 'M d, Y', strtotime($deadline) );

							} else {
								$deadline_date = date( 'M d, Y', strtotime( '-2 month' , strtotime( $event_date ) ) );
							}

							if( empty( $start_date ) || $start_date == '1970-01-01' ) {
								$date_range = date( 'M d, Y', strtotime( $event_date ) );
							} elseif(  $end_date !== '1970-01-01' ) {
								$begin		= date( 'M d', strtotime( $start_date ) );
								$end		= date( 'd, Y', strtotime( $end_date ) );;
								$date_range	= $begin . '-'. $end;
							} else {
								$date_range = date( 'M d, Y', strtotime( $start_date ) );
							}

							$sort_date = date( 'Ymd', strtotime( $start_date ) );

							if( $city != 'Virtual Run' && $event_date !== '1970-01-01' ) {

								// for each city, print out the row

								echo '<tr>
									<td class="event_City">' . $city . '</td>
									<td sorttable_customkey="' . $sort_date . '">' . $date_range . '</td>
									<td>';

										if( $city == 'Carlsbad 5000' ) {
											echo  '<span class="bolderPop">N/A</span>';
										} else {
											if( $limit_full != '' ) {
												if( ( $limit_half !='' ) || ( $limit_10k!='' ) ) {
													echo $limit_full . ' <span class="bolderPop">M</span><span class="hidden-xs breaker"> | </span>';
												} else {
													echo $limit_full . ' <span class="bolderPop">M</span>  ';
												}
											}

											if( $limit_half != '' ) {
												if( $limit_10k != '' ) {
													echo $limit_half . ' <span class="bolderPop">HM</span> <span class="hidden-xs breaker">| </span>';
												} else {
													echo $limit_half . ' <span class="bolderPop">HM</span>';
												}
											}

											if( $limit_10k != '' ) {
												echo $limit_10k . ' <span class="bolderPop">10k</span>';
											}

											if( empty( $limit_10k ) && empty( $limit_half ) && empty( $limit_full ) ) {
												echo '<span class="bolderPop">TBD</span>';
											}
										}

									echo '</td>
									<td>' . date( 'M d, Y', strtotime( $deadline_date ) ) . '</td>
								</tr>';
							}
						}
					?>

				</tbody>
			</table>
		</div>
	</section>
</main>
<?php get_footer(); ?>
