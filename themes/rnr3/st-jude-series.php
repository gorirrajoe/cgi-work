<?php
	/* Template Name: St. Jude Heroes (Series) */
	get_header('special');

	$prefix = '_rnr3_';

	// headers
	$hero['title']		= 'Join the Team';
	$classic['title']	= 'Classic';
	$bronze['title']	= 'Bronze';
	$silver['title']	= 'Silver';
	$gold['title']		= 'Gold';
	$platinum['title']	= 'Platinum';
	$share['title']		= 'Share Your Story';
	$contact['title']	= 'Contact';

	// contents
	$hero['content']		= apply_filters( 'the_content', get_the_content() );
	$classic['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'classic', true ) );
	$bronze['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'bronze', true ) );
	$silver['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'silver', true ) );
	$gold['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'gold', true ) );
	$platinum['content']	= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'platinum', true ) );
	$share['content']		= get_post_meta( get_the_ID(), $prefix . 'share', 1 );
	$contact['content']		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'contact', true ) );

	$stjude_array = array(
		$hero,
		$platinum,
		$gold,
		$silver,
		$bronze,
		$classic,
		$share,
		$contact
	);
	$no_commitments = array(
		$hero['title'],
		$classic['title'],
		$share['title'],
		$contact['title']
	);
	$plus_commitments = array(
		'$50 Expo Shopping Credit',
		'Two-Night Event Weekend Hotel Accomodations',
		'Roundtrip Airfare Voucher (up to $400) for Event Weekend'
	);
?>

	<!-- main content -->
	<main role="main" id="main">
		<div id="nav-anchor"></div>
		<section class="wrapper grid_2 offset240left">

			<div class="column sidenav stickem">
				<h2>St. Jude Heroes</h2>
				<nav class="sticky-nav">
					<ul>
						<?php
							foreach( $stjude_array as $key => $value ) {
								if( $value['content'] != '' ) {
									echo '<li><a href="#before'.$key.'">'.$value['title'].'</a></li>';
								}
							}
						?>
					</ul>
				</nav>
			</div>

			<div class="column">
				<div class="content">

					<?php
						$count = 3;

						foreach( $stjude_array as $key => $value ) {
							if( $value['content'] != '' ) {

								echo '<section id="before'.$key.'">
									<h3>'.$value['title'].'</h3>

									'. $value['content'];

									if( !in_array( $value['title'], $no_commitments ) ) {

										echo '<div class="stj_glance">
											<h4>'.$value['title'].' Commitment Includes:</h4>

											<ul class="stj_commitments">
												<li>Free Race Entry</li><li>
												St. Jude Hero Race Singlet</li>';

												if( $value['title'] != 'Bronze' ) {
													echo '<li>St. Jude Heroes Training Tech Shirt</li>';
												}
												if( $value['title'] == 'Bronze' ) {
													echo '<li>One Pre-Race St. Jude Heroes Hospitality Pass</li>';
												}
												if( $value['title'] == 'Silver' || $value['title'] == 'Gold' ) {
													echo '<li>Up to Two Pre-Race St. Jude Heroes Hospitality Passes</li><li>
													Up to Two Post-Race St. Jude Heroes Hospitality Passes</li>';
												}
												if( $value['title'] == 'Gold' || $value['title'] == 'Platinum' ) {
													echo '<li>Two Night Event Weekend Hotel Accommodations</li>';
												}
												if( $value['title'] == 'Platinum' ) {
													echo '<li>Roundtrip Airfare Voucher (up to $400) for One Event Day Weekend</li>';
												}

											echo '</ul>';


											if( $count != 0 ){
												echo '<ul class="stj_plus">';
													for( $i = 0; $i < $count; $i++ ) {
														echo '<li>'. $plus_commitments[$i] .'</li>';
													}
												echo '</ul>';
											}
										echo '</div>';
										$count--;
									}

								echo '</section>';
							}
						}
					?>
				</div>
			</div>
		</section>


		<script>
			$(document).ready(function(){

				/**
				 * This part causes smooth scrolling using scrollto.js
				 * We target all a tags inside the nav, and apply the scrollto.js to it.
				 */
				$(".sticky-nav a, .backtotop").click(function(evn){
					evn.preventDefault();
					$('html,body').scrollTo(this.hash, this.hash);
				});

				/*$('main').stickem({
					container: '.offset240left',
				});*/

				/**
				 * This part handles the highlighting functionality.
				 * We use the scroll functionality again, some array creation and
				 * manipulation, class adding and class removing, and conditional testing
				 */
				var aChildren = $(".sticky-nav li").children(); // find the a children of the list items
				var aArray = []; // create the empty aArray
				for (var i=0; i < aChildren.length; i++) {
					var aChild = aChildren[i];
					var ahref = $(aChild).attr('href');
					aArray.push(ahref);
				} // this for loop fills the aArray with attribute href values

				$(window).scroll(function(){
					var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
					var windowHeight = $(window).height(); // get the height of the window
					var docHeight = $(document).height();

					for (var i=0; i < aArray.length; i++) {
						var theID = aArray[i];
						var divPos = $(theID).offset().top; // get the offset of the div from the top of page
						var divHeight = $(theID).height(); // get the height of the div in question
						if (windowPos >= (divPos - 1) && windowPos < (divPos + divHeight -1 )) {
							$("a[href='" + theID + "']").addClass("nav-active");
						} else {
							$("a[href='" + theID + "']").removeClass("nav-active");
						}
					}

					if(windowPos + windowHeight == docHeight) {
						if (!$(".sticky-nav li:last-child a").hasClass("nav-active")) {
							var navActiveCurrent = $(".nav-active").attr("href");
							$("a[href='" + navActiveCurrent + "']").removeClass("nav-active");
							$(".sticky-nav li:last-child a").addClass("nav-active");
						}
					}
				});
			});

		</script>
	</main>

<?php get_footer(); ?>
