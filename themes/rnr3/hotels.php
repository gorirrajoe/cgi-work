<?php
	/* Template Name: Hotels */
	get_header();

	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$parent_slug = the_parent_slug();
	rnr3_get_secondary_nav( $parent_slug );

	$qt_lang = rnr3_get_language();
	include 'languages.php';


	$extra_info				= apply_filters( 'the_content', get_post_meta( get_the_ID(), '_rnr3_extra_info', true ) );
	$extra_info_under_map	= apply_filters( 'the_content', get_post_meta( get_the_ID(), '_rnr3_extra_info_under_map', true ) );

	// set defaults
	$all_hotels				= array();
	$spacer					= array( 'x' );
	$official_hotels		= array();
	$hq_hotels				= array();
	$featured_hotels		= array();
	$official_hotels_count	= 0;
	$featured_hotels_count	= 0;
	$hq_hotels_count		= 0;


	if ( false === ( $hotels_query = get_transient( 'hotels_query_results' ) ) ) {
		$args = array(
			'post_type'			=> 'hotels',
			'orderby'			=> 'name',
			'order'				=> 'ASC',
			'posts_per_page'	=> -1,
			'post_status'		=> 'publish'
		);
		$hotels_query = new WP_Query( $args );
		set_transient( 'hotels_query_results', $hotels_query, 5 * MINUTE_IN_SECONDS );
	}


?>

	<!-- main content -->
	<main role="main" id="main">
		<section class="wrapper center grid_1">
			<?php
				// allowing custom text if wanted
				if (have_posts()) : while (have_posts()) : the_post();
					echo '<h2>'. get_the_title(). '</h2>';
					$content = apply_filters( 'the_content', get_the_content() );

					// default content
					if( $extra_info_under_map != '' ) {
						echo '<div class="grid_2_special">';
					}
					echo '<div class="travel_reserve column_special">';
						if( $content == '' AND $qt_lang['lang'] == 'en') {
							echo '
								<p><strong>Reserve Your Discounted Hotel Today!</strong></p>
								<p>The Rock \'n\' Roll Marathon Series has partnered with select hotels in every destination to offer you and your family discounts and an opportunity to stay with your fellow runners nearby key event locations.</p>
								<p>*Discounted rates available beginning when registration opens until 30 days prior to the event weekend. Inside 30 days is subject to availaibility.</p>
							';
						} else {
							echo $content;
						}
					echo '</div>';

					if( $extra_info_under_map != '' ) {
							echo '<div class="travel_extra_info column_special">'
								.$extra_info_under_map.
							'</div>
						</div>';	// close grid_2_special if applicable
					}
				endwhile; endif;

				if ($hotels_query->have_posts()) {
					while ($hotels_query->have_posts()) {
						$hotels_query->the_post();

						$hotel_type = get_post_meta( get_the_ID(), '_rnr3_hotel_type', true);
						if( $hotel_type == 'official' ) {
							$official_hotels[] = get_the_ID();
							$official_hotels_count++;
						} elseif( $hotel_type == 'headquarter' ) {
							$hq_hotels[] = get_the_ID();
							$hq_hotels_count++;
						} elseif( $hotel_type == 'featured' ) {
							$featured_hotels[] = get_the_ID();
							$featured_hotels_count++;
						}
					}

					$all_hotels = array_merge( $hq_hotels, $spacer, $featured_hotels, $official_hotels );
					$market_query = get_current_blog_id();
					$eventid = $wpdb->get_var( "SELECT wm_event_race_id FROM wp_waypoint_event WHERE wm_marketlist='".$market_query."'" );
					if ($eventid != '') {
						/* begin map */
						echo '
							<link href="https://developers.google.com/maps/documentation/javascript/examples/default.css" rel="stylesheet">
							<script src="https://www.google.com/jsapi"></script>
							<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
							<style type="text/css">
								#travel_map_canvas {
									width:100%;
									height:400px;
									border:1px solid #333;
									margin-bottom:2em;
								}
								#travel_map_canvas img {
									max-width:none;
								}
							</style>
						';

						if( (WP_ENV === 'development' || WP_ENV === 'testing') OR strpos ( $_SERVER['HTTP_HOST'] , "lan" ) >= 1) {
							$siteurl = 'http://www.runrocknroll.com';
						} else {
							$siteurl = 'http://www.runrocknroll.com';
						}
						$kmlurl = $siteurl ."/wp-content/themes/rnr3/travel-kmlbuilder.php?market_query=".$market_query; ?>
						<div id="travel_map_canvas"></div>
						<script type="text/javascript">
							$(document).ready(function(){
								$(".b_opacity").click(function(){
									//this will find all the images in the map canvas container. Normally you would want to target specific images by the value of src
									$("#travel_map_canvas").find("img").css("opacity","0.4")
								})

								//Set center of the map
								var myOptions = {
									zoom: 8,
									zoomControl: true,
									zoomControlOptions: {
										style: google.maps.ZoomControlStyle.DEFAULT
									},
									mapTypeId: google.maps.MapTypeId.ROADMAP
								}
								var map = new google.maps.Map(document.getElementById("travel_map_canvas"), myOptions);
								var ctaLayer = new google.maps.KmlLayer("<?php echo $kmlurl; ?>");
								ctaLayer.setMap(map);
							});
						</script>
						<?php /* end map */
					}

					echo '<div id="hotels">';
						print_hotels( $all_hotels, $display_text, $qt_lang, $market );
					echo '</div>';

					echo '<div class="hotel_extra_info">
						'.$extra_info.'
					</div>';

					wp_reset_postdata();

				} else {
					echo '<p>No hotels available at this time.</p>';
				}


			?>
		</section>

	</main>

<?php get_footer();


/**
 * function: hotels
 * parameters:
 *    $hotel_ids - array - ids of posts
 *    $display_text - array - translations
 */
function print_hotels( $hotel_ids, $display_text, $qT_lang, $market ) {
	foreach( $hotel_ids as $hotel_id ) {
		if( $hotel_id != 'x' ) {
			$hotel_type			= get_post_meta( $hotel_id, '_rnr3_hotel_type', true );
			$hotel_image		= get_post_meta( $hotel_id, '_hotel_image', true );

			if( $hotel_image != '' ) {
				$hotel_image_id		= rnr3_get_image_id( $hotel_image );
				$hotel_image_sized	= wp_get_attachment_image_src( $hotel_image_id, 'travel-hotel' );
				$hotel_image_alt	= get_post_meta( $hotel_image_id, '_wp_attachment_image_alt', true );
			}

			$logo_image			= get_post_meta( $hotel_id, '_logo_image', true );
			$sold_out			= get_post_meta( $hotel_id, '_sold_out',true );
			$hotel_link			= get_post_meta( $hotel_id, '_hotel_link',true );
			$hotel_map			= get_post_meta( $hotel_id, '_maps',true );
			$hotel_address		= get_post_meta( $hotel_id, '_hotel_address',true );
			$hotel_city			= get_post_meta( $hotel_id, '_hotel_city',true );
			$hotel_state		= get_post_meta( $hotel_id, '_hotel_state',true );
			$hotel_zip			= get_post_meta( $hotel_id, '_hotel_zip',true );
			$hotel_phone		= get_post_meta( $hotel_id, '_hotel_phone',true );
			$hotel_distance		= get_post_meta( $hotel_id, '_distance',true );
			$hotel_rates		= apply_filters( 'the_content', get_post_meta( $hotel_id,'_rates',true ) );
			$reservation_url	= get_post_meta( $hotel_id, '_reservation_link',true );
			$shuttle_service	= get_post_meta( $hotel_id, '_shuttle_service',true );
			$hotel_description	= apply_filters( 'the_content', get_post_meta( $hotel_id,'_hotel_description',true ) );
			$westin_vip			= get_post_meta( $hotel_id, '_westin_vip',true );
			$westin_vip_link	= get_post_meta( $hotel_id, '_westin_vip_link',true );
			$westin_pixel_link	= get_post_meta( $hotel_id, '_westin_pixel_link',true );

			if ( $sold_out ) {
				$soldout = ' soldout';
			} else {
				$soldout = '';
			}
			if( $hotel_type == 'headquarter' ) {
				$hotel_type_class	= ' hqhotel';
				$hotel_type_txt		= $display_text['hotel_hq'];
			} elseif( $hotel_type == 'featured' ) {
				$hotel_type_class	= ' featuredhotel';
				$hotel_type_txt		= $display_text['hotel_feat'];
			} elseif( $hotel_type == 'official' ) {
				$hotel_type_class	= ' officialhotel';
				$hotel_type_txt		= $display_text['hotel_offic'];
			}

			echo '<div class="hotel'.$soldout.$hotel_type_class.'">
				<div class="inner_hotel">
					<h2>'. $hotel_type_txt .'</h2>';

					echo '<div class="vcard">';
						if( $hotel_image != '' ) {
							echo '<figure><img src="'. $hotel_image_sized[0] .'" alt="'.$hotel_image_alt.'"></figure>';
						}
						if( $hotel_link != '' ) {
							echo '<a href="'.$hotel_link.'" target="_blank"><h3 class="org">'.get_the_title( $hotel_id ).'</h3></a>';
						} else {
							echo '<h3 class="org">'.get_the_title( $hotel_id ).'</h3>';
						}
						if($hotel_phone != ''){
							echo '<p class="tel">'.$display_text['phone'].': '.$hotel_phone.'</p>';
						}
						if( $shuttle_service == 'on' ) {
							echo '<span class="icon-bus"></span>';
						}
						if( $hotel_distance != '' ) {
							echo '<h4>'.$display_text['dst_from'].':</h4>';
							if( $qT_lang['enabled'] == 1 ) {
								$distance_array = qtrans_split( $hotel_distance );
								echo '<p>'.$distance_array[$qT_lang['lang']].'</p>';
							} else {
								echo '<p>'.$hotel_distance.'</p>';
							}
						}
					echo '</div>';

					if( $hotel_rates != '' ) {
						echo '<h4>'.$display_text['dst_rate'].':</h4>';
						if( $qT_lang['enabled'] == 1 ) {
							$rates_array = qtrans_split($hotel_rates);
							echo apply_filters( 'the_content', $rates_array[$qT_lang['lang']] );
						} else {
							echo $hotel_rates;
						}
					}

					if( $hotel_address != '' || $hotel_description != '' || $shuttle_service == 'on' ) {
						echo '<a class="show">'.$display_text['more_details'].'</a>
						<div class="hide">
							<p class="adr">';
								if( $hotel_map != '' ) {
									echo '<a href="'.$hotel_map.'" target="_blank">';
								}
								echo '<span class="street-address">'.$hotel_address.'</span>
								<span class="locality">'.$hotel_city.',</span>
								<span class="region">'.$hotel_state.'</span>
								<span class="postal-code">'.$hotel_zip.'</span>';
								if( $hotel_map != '' ) {
									echo '</a>';
								}
							echo '</p>';

							if( $hotel_description != '' ) {
								echo '<h4>'.$display_text['hotel_description'].':</h4>';
								if( $qT_lang['enabled'] == 1 ) {
									$description_array = qtrans_split( $hotel_description );
									echo '<p>'.$description_array[$qT_lang['lang']].'</p>';
								} else {
									echo '<div class="desc">'.$hotel_description.'</div>';
								}
							}

							if ( $shuttle_service == 'on' ) {
								echo '<p class="hotel_note">Shuttle service available. Contact hotel for details.</p>';
							}
						echo '</div>';
					}



					if( $sold_out != '' ) {
						echo '<p class="sold">'. $display_text['sold_out'] .'</p>';
					} elseif( $reservation_url ) {
						echo '<p class="cta"><a href="'.$reservation_url.'" target="_blank" id="rnrreservebtn">'. $display_text['rsv_onl'] .'</a></p>';
					}
					if( $westin_vip == 'on' ) {
						//tracking pixel
						if(!empty($westin_pixel_link) AND $westin_pixel_link != ''){
							echo "<img src=\"".$westin_pixel_link."".microtime()."\">";
						}
						echo '<div class="westincta">
							<p>'.$display_text['westin'].'</p>
							<a class="highlight westin" href="'.$westin_vip_link.''.microtime().'" target="_blank" id="westinbtn">'. $display_text['westin_btn'] .'</a>
						</div>';
					}
					edit_post_link('edit', '<p class="post-edit">', '</p>', $hotel_id);

				echo '</div>
			</div>';

		} else {
			echo '</div><div id="hotels">';
		}

	}

}

?>
