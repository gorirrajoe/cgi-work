<?php
	/* Template Name: M's - Moments (Series) */
	get_header('special');

	rnr3_get_secondary_nav( '', 'm-moments' );
	$prefix = '_rnr3_moments_'; // cmb2 variable (ease of reading)

	$humana_logo = get_post_meta( get_the_ID(), $prefix . 'humana_logo', 1 );
?>
<main role="main" id="main" class="m_moments">
	<section id="moments-featured">
		<div class="edge2edge">
			<h2 class="center moment_title">Featured Moments</h2>
		</div>
		<section class="wrapper">

			<?php
				if ( false === ( $featured_query = get_transient( 'featured_query_results' ) ) ) {
					$args = array(
						'post_type'			=> 'moment',
						'posts_per_page'	=> 6,
						'post_status'		=> 'published',
						'tax_query'			=> array(
							array(
								'taxonomy'	=> 'moments',
								'field'		=> 'slug',
								'terms'		=> 'featured'
							)
						)
					);
					$featured_query = new WP_Query( $args );
					set_transient( 'featured_query_results', $featured_query, 5 * MINUTE_IN_SECONDS );
				}

				$count = 1;
				if( $featured_query->have_posts() ) {
					echo '<div class="checkerboard">';

						while( $featured_query->have_posts() ) {

							$featured_query->the_post();
							if( ( $count % 2 ) == 0 ) {
								$odd_even	= ' even';
								$left_right	= array( 'right', 'left' );
							} else {
								$odd_even	= ' odd';
								$left_right	= array( 'left', 'right' );
							}

							if( $count < 5 ) {

								echo '<div class="checkerboard_row'.$odd_even.'">
									<div class="chk_'.$left_right[0].'">
										<a href="'.get_the_permalink().'">'.get_the_post_thumbnail( get_the_ID(), 'full' ).'</a>
									</div>';

									if( has_tag( 'humana' ) ) {
										$humana_accent = ' humana_accent';
									} else {
										$humana_accent = '';
									}

									echo '<div class="chk_'.$left_right[1]. $humana_accent .'">
										<h3><a href="'.get_the_permalink().'">'.get_the_title().'</a>';
											if( has_tag( 'humana' ) ) {
												echo '<div class="humana_byline">Presented by <img src="'. $humana_logo .'" alt="Humana"></div>';
											}
										echo '</h3>
										<p>'.get_the_excerpt().'</p>
										<div class="center"><a href="'.get_permalink().'" class="cta">Read More</a></div>
									</div>
								</div>';

							} else {

								if( $count == 5 ) {
									// close div.checkerboard
									echo '</div>
								</section>  <!-- close wrapper -->

								<section id="moments-panel1a">
									<section class="wrapper">
										<h2 class="center">More Moments</h2>

										<div class="grid_2_special moments_grid_2">';
								}
											echo '<div class="column_special">
												<figure><a href="'.get_the_permalink().'">'.get_the_post_thumbnail( get_the_ID(), 'moments-thumb' ).'</a></figure>
												<h3><a href="'.get_the_permalink().'">'.get_the_title().'</a></h3>
												<p>'.get_the_excerpt().' <a href="'.get_the_permalink().'">Read More</a></p>
											</div>';
							}
							$count++;
						}
										echo '</div>
									</section>
								<p class="center"><a href="'.site_url('/moment/').'" class="cta">View All</a></p>
							</section>';
				}
				wp_reset_postdata();
			?>
		</section>

	</section>


	<section id="moments-make-a-difference">
		<section class="wrapper">
			<div class="edge2edge">
				<figure class="center">
					<img src="<?php echo get_post_meta( get_the_ID(), $prefix . 'stjude_logo', 1 ); ?>">
				</figure>

				<?php echo apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'stjude_txt', 1 ) );

			echo '</div>';

			if ( false === ( $featured_query2 = get_transient( 'featured_query2_results' ) ) ) {
				$args = array(
					'post_type'			=> 'moment',
					'posts_per_page'	=> 4,
					'post_status'		=> 'published',
					'tax_query'			=> array(
						array(
							'taxonomy'	=> 'moments',
							'field'		=> 'slug',
							'terms'		=> 'st-jude'
						)
					)
				);
				$featured_query2 = new WP_Query( $args );
				set_transient( 'featured_query2_results', $featured_query2, 5 * MINUTE_IN_SECONDS );
			}

			$count = 1;

			if( $featured_query2->have_posts() ) {
				echo '<div class="checkerboard">';

					while( $featured_query2->have_posts() ) {
						$featured_query2->the_post();

						if( ( $count % 2 ) == 0 ) {
							$odd_even	= ' even';
							$left_right	= array( 'right', 'left' );
						} else {
							$odd_even	= ' odd';
							$left_right	= array( 'left', 'right' );
						}

						if( $count < 3 ) {

							echo '<div class="checkerboard_row'.$odd_even.'">
								<div class="chk_'.$left_right[0].'">
									<a href="'.get_the_permalink().'">'.get_the_post_thumbnail( get_the_ID(), 'full' ).'</a>
								</div>
								<div class="chk_'.$left_right[1].'">
									<h3><a href="'.get_the_permalink().'">'.get_the_title().'</a></h3>
									<p>'.get_the_excerpt().'</p>
									<div class="center"><a href="'.get_permalink().'" class="cta">Read More</a></div>
								</div>
							</div>';

						} else {
							if( $count == 3 ) {
								// close div.checkerboard
								echo '</div>

								<section id="moments-panel2a">
									<div class="wrapper">
										<h2 class="center">More Moments</h2>
										<div class="grid_2_special moments_grid_2">';

							}
											echo '<div class="column_special">
												<figure><a href="'.get_the_permalink().'">'.get_the_post_thumbnail( get_the_ID(), 'moments-thumb' ).'</a></figure>
												<h3><a href="'.get_the_permalink().'">'.get_the_title().'</a></h3>
												<p>'.get_the_excerpt().' <a href="'.get_the_permalink().'">Read More</a></p>
											</div>';
						}
						$count++;
					}
										echo '</div>
									</div>
									<p class="center"><a href="'.site_url('/moment/').'" class="cta">View All</a></p>
								</section>';
			}
			wp_reset_postdata(); ?>

		</section>
	</section>


	<section id="moments-share-your-moment">
		<section class="wrapper">
			<figure class="center">
				<img src="<?php echo $humana_logo; ?>">
			</figure>

			<?php echo apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'humana_txt', 1 ) ); ?>

			<div class="narrow"><?php echo get_post_meta( get_the_ID(), $prefix . 'humana_form', 1 ); ?></div>

			<?php if ( false === ( $featured_query3 = get_transient( 'featured_query3_results' ) ) ) {
				$args = array(
					'post_type'			=> 'moment',
					'posts_per_page'	=> 4,
					'post_status'		=> 'published',
					'tax_query'			=> array(
						array(
							'taxonomy'	=> 'moments',
							'field'		=> 'slug',
							'terms'		=> 'humana'
						)
					)
				);
				$featured_query3 = new WP_Query( $args );
				set_transient( 'featured_query3_results', $featured_query3, 5 * MINUTE_IN_SECONDS );
			}

			$count = 1;

			if( $featured_query3->have_posts() ) {

				echo '<h2 class="center">Your Moments</h2>
				<section class="grid_2_special moments_grid_2">';

					while( $featured_query3->have_posts() ) {
						$featured_query3->the_post();

						echo '<div class="column_special">
							<figure><a href="'.get_the_permalink().'">'.get_the_post_thumbnail( get_the_ID(), 'moments-thumb' ).'</a></figure>
							<h3><a href="'.get_the_permalink().'">'.get_the_title().'</a></h3>
							<p>'.get_the_excerpt().' <a href="'.get_the_permalink().'">Read More</a></p>
						</div>';
						$count++;
					}
				echo '</section>';
			}
			wp_reset_postdata(); ?>

		</section>
	</section>
</main>

<?php get_footer('series'); ?>
