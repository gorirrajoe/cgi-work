<?php
	/* Template Name: Hotels - v2 */
	get_header();

	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$parent_slug = the_parent_slug();
	rnr3_get_secondary_nav( $parent_slug );

	$qt_lang = rnr3_get_language();
	include 'languages.php';


	$extra_info				= apply_filters( 'the_content', get_post_meta( get_the_ID(), '_rnr3_extra_info', true ) );
	$extra_info_under_map	= apply_filters( 'the_content', get_post_meta( get_the_ID(), '_rnr3_extra_info_under_map', true ) );

	// set defaults
	$all_hotels				= array();
	$official_hotels		= array();
	$hq_hotels				= array();
	$featured_hotels		= array();
	// $official_hotels_count	= 0;
	// $featured_hotels_count	= 0;
	// $hq_hotels_count		= 0;


	if ( false === ( $hotels_query = get_transient( 'hotels_query_results' ) ) ) {
		$args = array(
			'post_type'			=> 'hotels',
			'orderby'			=> 'name',
			'order'				=> 'ASC',
			'posts_per_page'	=> 500,
			'post_status'		=> 'publish'
		);
		$hotels_query = new WP_Query( $args );
		set_transient( 'hotels_query_results', $hotels_query, 5 * MINUTE_IN_SECONDS );
	}

	$travel_subpage_array = array(
		'fly'		=> 'flights',
		'stay'		=> 'hotels',
		'pack'		=> 'pack',
		'transport'	=> 'transport'
	);

	$travel_page_id	= get_id_by_slug( 'the-weekend/travel' );
?>

<!-- main content -->
<main role="main" id="main" class="travel">
	<section class="wrapper">

		<?php
			if( have_posts() ) : while( have_posts() ) : the_post();

				$subpage_icon = ( get_post_meta( $travel_page_id, '_rnr3_stay_img', 1 ) != '' ) ? get_post_meta( $travel_page_id, '_rnr3_stay_img', 1 ) : '';
				$subpage_sponsor = ( get_post_meta( $travel_page_id, '_rnr3_stay_sponsor', 1 ) != '' ) ? get_post_meta( $travel_page_id, '_rnr3_stay_sponsor', 1 ) : '';

				echo '<section class="travel_content">
					<h2 class="travel_subpage_title">

						<img class="travel_subpage_icon" src="'. $subpage_icon .'" alt="'. get_the_title() .'">' .

						get_the_title();

						if( $subpage_sponsor != '' ) {
							echo '<span>'. $presented_by_txt .' <img src="'. $subpage_sponsor.'"></span>';
						}

					echo '</h2>

					<div class="narrow">';

						the_content();

					echo '</div>
				</section>';

			endwhile; endif;


			/**
			 * prep the display of hotels
			 * show in order of groups: headquarter, featured, official
			 */
			if( $hotels_query->have_posts() ) { ?>

				<section class="travel_hotel_sort_legend">

					<div class="controls">

						<label><?php echo $sortby_txt; ?></label>
						<div class="hotel_sort_price">
							<select id="sort1" class="sortselect">
								<option value="">-- <?php echo $display_text['price']; ?> --</option>
								<option value="price:asc"><?php echo $display_text['low_high']; ?></option>
								<option value="price:desc"><?php echo $display_text['high_low']; ?></option>
							</select>
						</div>

						<div class="hotel_sort_distance">
							<select id="sort2" class="sortselect">
								<option value="">-- <?php echo $txt_distances_title; ?> --</option>

								<?php
									/**
									 * show only distances that the event actually has
									 */
									$available_races = explode( ',', $event_info->has_races );

									foreach( $available_races as $available_race ) {

										switch( $available_race ) {
											case 'fivek':
												echo '
													<option value="5k_sl:asc">'. $display_text['fivek_start_line'] .'</option>
													<option value="5k_fl:asc">'. $display_text['fivek_finish_line'] .'</option>
												';
												break;
											case 'tenk';
												echo '
													<option value="10k_sl:asc">'. $display_text['tenk_start_line'] .'</option>
													<option value="10k_fl:asc">'. $display_text['tenk_finish_line'] .'</option>
												';
												break;
											case 'half';
												echo '
													<option value="hm_sl:asc">'. $display_text['half_start_line'] .'</option>
													<option value="hm_fl:asc">'. $display_text['half_finish_line'] .'</option>
												';
												break;
											case 'full';
												echo '
													<option value="m_sl:asc">'. $display_text['full_start_line'] .'</option>
													<option value="m_fl:asc">'. $display_text['full_finish_line'] .'</option>
												';
												break;
										}

									}
								?>
								<option value="expo:asc"><?php echo $display_text['expo']; ?></option>
							</select>
						</div>

						<?php
							$market_query	= get_current_blog_id();
							$eventid		= $wpdb->get_var( "SELECT wm_event_race_id FROM wp_waypoint_event WHERE wm_marketlist='". $market_query ."'" );

							if( $eventid != '' ) { ?>

								<div class="hotel_view_links">
									<span><a onclick="toggleByClass( 'showmap' )" href="#travel_map_canvas"><?php echo $display_text['show_map']; ?></a></span>
								</div>

							<?php }
						?>

					</div>


					<div class="hotel_legend">
						<ol>
							<li><span class="type_letter type_letter_h"><?php echo $headquarter_initials; ?></span> <?php echo $display_text['headquarter']; ?></li>
							<li><span class="type_letter type_letter_f"><?php echo $westin_initials; ?></span> <?php echo $display_text['westin_featured']; ?></li>
							<li><span class="type_letter type_letter_o"><?php echo $official_initials; ?></span> <?php echo $display_text['official']; ?></li>
							<li><span class="icon-food"></span> <?php echo $display_text['breakfast']; ?></li>
							<li><span class="icon-bus"></span> <?php echo $display_text['shuttle']; ?></li>
							<li><span class="type_letter type_letter_sl"><?php echo $startline_initials; ?></span> <?php echo $txt_start_line; ?></li>
							<li><span class="type_letter type_letter_fl"><?php echo $finishline_initials; ?></span> <?php echo $txt_finish_line; ?></li>
						</ol>
					</div>


					<?php
						/**
						 * retrieve westin blurb content, stored in blog 1's travel page
						 */
						if( get_post_meta( get_the_ID(), '_rnr3_westin_featured', 1 ) == 'on' ) {
							switch_to_blog( 1 );

								$blog1_travel_page_id = get_id_by_slug( 'travel' );

								$westin_logo	= get_post_meta( $blog1_travel_page_id, '_rnr3_westin_logo', 1 );
								$westin_blurb	= apply_filters( 'the_content', get_post_meta( $blog1_travel_page_id, '_rnr3_westin_blurb', 1 ) );

								if( $westin_blurb != '' ) {

									echo '<div class="westin_blurb">

										<img src="'. $westin_logo .'" class="alignleft">
										'. $westin_blurb .'

									</div>';

								}

							restore_current_blog();
						}
					?>

				</section>

				<?php while( $hotels_query->have_posts() ) {

					$hotels_query->the_post();

					$hotel_type = get_post_meta( get_the_ID(), '_rnr3_hotel_type', true);

					if( $hotel_type == 'official' ) {

						$official_hotels[] = get_the_ID();
						// $official_hotels_count++;

					} elseif( $hotel_type == 'headquarter' ) {

						$hq_hotels[] = get_the_ID();
						// $hq_hotels_count++;

					} elseif( $hotel_type == 'featured' ) {

						$featured_hotels[] = get_the_ID();
						// $featured_hotels_count++;

					}

				}

				$all_hotels		= array_merge( $hq_hotels, $featured_hotels, $official_hotels );

				if( $eventid != '' ) {

					/**
					 * google map
					 */
					echo '<style type="text/css">
						#travel_map_canvas {
							width:100%;
							height:400px;
							border:1px solid #333;
							margin-bottom:2em;
						}
						#travel_map_canvas img {
							max-width:none;
						}
					</style>';

					// hardcode $siteurl since the map will only work on production
					$kmlurl = "http://www.runrocknroll.com/wp-content/themes/rnr3/travel-kmlbuilder.php?market_query=". $market_query ."&". date('d'); ?>

					<div class="showmap" id="travel_map_canvas" style="display: none;"></div>

					<script type="text/javascript">
						function mapInit() {
							var map = new google.maps.Map( document.getElementById( 'travel_map_canvas' ), {
								zoom: 8,
								zoomControl: true,
								zoomControlOptions: {
									style: google.maps.ZoomControlStyle.DEFAULT
								},
								mapTypeId: google.maps.MapTypeId.ROADMAP
							} );
							var ctaLayer = new google.maps.KmlLayer( "<?php echo $kmlurl; ?>" );
							ctaLayer.setMap(map);
							mapInit.called = true;
						}
					</script>

					<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBUSq4eDlz_eo-9HD-unX2LdI7tmhrDMAc"></script>

				<?php }


				echo '<section class="travel_hotel_group">';

					print_hotels( $all_hotels, $display_text, $qt_lang, $market, $event_info );

				echo '</section>';


				if( $extra_info != '' ) {
					echo '<div class="travel_hotel_extra_info narrow">

						'. $extra_info .'

					</div>';
				}


				wp_reset_postdata();

			} else {

				echo '<div class="narrow">
					<p>No hotels available at this time.</p>
				</div>';

			}


			travel_subpage_columns( $travel_page_id, $qt_lang );

			/**
			 * travel sponsors
			 */
			travel_sponsors( $travel_page_id );
		?>


	</section>

	<script src="http://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
	<script>
		jQuery(function( $ ){
			var $sortSelect = $( '.sortselect' ),
			$container = $( '.travel_hotel_group' );

			$container.mixItUp({
				layout: {
					display: 'block'
				}
			});

			$sortSelect.on( 'change', function() {
				$container.mixItUp( 'sort', this.value );
			});
		});


	</script>

</main>

<?php get_footer();


/**
 * function: hotels
 * parameters:
 *    $hotel_ids - array - ids of posts
 *    $display_text - array - translations
 */
function print_hotels( $hotel_ids, $display_text, $qt_lang, $market, $event_info ) {
	include 'languages.php';

	foreach( $hotel_ids as $hotel_id ) {
		$hotel_array		= get_meta( $hotel_id );
		$hotel_image_sized	= array_key_exists( '_hotel_image_id', $hotel_array ) ? wp_get_attachment_image_src( $hotel_array['_hotel_image_id'], 'travel-hotel' ) : '';
		$hotel_image_alt	= $hotel_image_sized != '' ? get_post_meta( $hotel_array['_hotel_image_id'], '_wp_attachment_image_alt', true ) : '';

		$hotel_lowest_rate_number_only_langs = array_key_exists( '_lowest_rate', $hotel_array ) ? str_replace( '$', '', $hotel_array['_lowest_rate'] ) : '';

		if( $qt_lang['enabled'] == 1 ) {
			$hotel_lowest_rate_number_only_array	= $hotel_lowest_rate_number_only_langs != '' ? qtrans_split( $hotel_lowest_rate_number_only_langs ) : '';
			$hotel_lowest_rate_number_only			= $hotel_lowest_rate_number_only_array != '' ? $hotel_lowest_rate_number_only_array[$qt_lang['lang']] : '';
		} else {
			$hotel_lowest_rate_number_only = $hotel_lowest_rate_number_only_langs != '' ? $hotel_lowest_rate_number_only_langs : '';
		}

		$amenities		= array_key_exists( '_rnr3_amenities', $hotel_array ) ? unserialize( $hotel_array['_rnr3_amenities'] ) : '';

		$distance_group	= array_key_exists( '_distances_group', $hotel_array ) ? unserialize( $hotel_array['_distances_group'] ) : '';

		$data_distance		= '';
		$distances_txt_list	= '';
		$prev_distance 		= '';


		if( !empty( $distance_group ) ) {

			$distances_txt_list = '<ol>';

				foreach( $distance_group as $key => $value ) {

					// variables for sorting
					if( $qt_lang['enabled'] == 1 ) {
						$value_distance_array	= qtrans_split( $value['distance'] );
						$value_distance			= $value_distance_array[$qt_lang['lang']];
					} else {
						$value_distance = $value['distance'];
					}

					$distance_option	= str_replace( array( ' mi', ' km', ' KM', ' Mi' ), '', $value_distance );
					$data_distance		.= ' data-'. $value['location'] .'="'. $distance_option .'"';

					// variable for the "distances from" portion
					$exploded_distance	= explode( '_', $value['location'] );

					if( $prev_distance != $exploded_distance[0] ) {

						if( $exploded_distance[0] == 'hm' ) {
							$distance_from = $halfmarathon_initials;
						} elseif( $exploded_distance[0] == '5k' ) {
							$distance_from = $fivek_txt;
						} elseif( $exploded_distance[0] == '10k' ) {
							$distance_from = $tenk_txt;
						} else {
							$distance_from = $exploded_distance[0];
						}

						$distances_txt_list .= '<li>
							<span class="event event__'. $exploded_distance[0] .'-'. $qt_lang['lang'] .'">'. $distance_from .'</span> ';

						if( $exploded_distance[0] != 'expo' ) {

							// if the location is "start line"
							if( $exploded_distance[1] == 'sl' ) {
								$distances_txt_list .= ' <span class="location location__'. $qt_lang['lang'] .'"><span class="type_letter type_letter_sl">'. $startline_initials .'</span> '. $value_distance .'</span>';
							}

						} else {
							$distances_txt_list .= ' <span class="location location__'. $qt_lang['lang'] .'"> '. $value_distance .'</span>';
						}

					} else {

						if( $exploded_distance[0] != 'expo' ) {

							// if the location is "finish line"
							if( $exploded_distance[1] == 'fl' ) {
								$distances_txt_list .= ' <span class="location location__'. $qt_lang['lang'] .'"> | <span class="type_letter type_letter_fl">'. $finishline_initials .'</span> '. $value_distance .'</span>';
							}

						} else {
							$distances_txt_list .= ' <span class="location location__'. $qt_lang['lang'] .'"> '. $value_distance .'</span>';
						}

						$distances_txt_list	.= '</li>';

					}

					$prev_distance = $exploded_distance[0];

				}

			$distances_txt_list .= '</ol>';

		}


		/**
		 * check type of hotel
		 * headquarter, featured, or official
		 */
		if( $hotel_array['_rnr3_hotel_type'] == 'headquarter' ) {

			$hotel_type_class			= ' hqhotel';
			$hotel_type_letter			= 'h';
			$hotel_type_letter_display	= $headquarter_initials;

		} elseif( $hotel_array['_rnr3_hotel_type'] == 'featured' ) {

			$hotel_type_class			= ' featuredhotel';
			$hotel_type_letter			= 'f';
			$hotel_type_letter_display	= $westin_initials;

		} elseif( $hotel_array['_rnr3_hotel_type'] == 'official' ) {

			$hotel_type_class			= ' officialhotel';
			$hotel_type_letter			= 'o';
			$hotel_type_letter_display	= $official_initials;

		}


		echo '<div id="hotel-'. $hotel_id .'" class="mix hotel'. $hotel_type_class .'" data-price="'. $hotel_lowest_rate_number_only .'"'. $data_distance .'>

			<div class="vcard">';

				// thumbnail
				if( $hotel_image_sized != '' ) {

					echo '<figure>
						<span class="type_letter type_letter_'. $hotel_type_letter .'">'. $hotel_type_letter_display .'</span>
						<img src="'. $hotel_image_sized[0] .'" alt="'. $hotel_image_alt .'">
					</figure>';

				}

				echo '<div class="hotel_content">
					<div class="hotel_content_center">';

						// hotel name
						echo '<h3 class="org">'.

							get_the_title( $hotel_id ) . ' ';
							edit_post_link('[edit]', '<span class="post-edit">', '</span>', $hotel_id);

						echo '</h3>';

						// phone number
						if( array_key_exists( '_hotel_phone', $hotel_array ) && $hotel_array['_hotel_phone'] != '' ) {
							echo '<p class="tel">'. $display_text['phone'] .': '. $hotel_array['_hotel_phone'] .'</p>';
						}

						// distances from locations
						if( !empty( $distance_group ) ) {

							echo '<span>'. $display_text['dst_from'] .':</span>'
							. $distances_txt_list;

						}


						// hotel description/sellpoints
						if( array_key_exists( '_hotel_description', $hotel_array ) && $hotel_array['_hotel_description'] != '' ) {

							echo '<div class="hotel_sellpoints">';
								if( $qt_lang['enabled'] == 1 ) {

									$description_array = qtrans_split( $hotel_array['_hotel_description'] );
									echo apply_filters( 'the_content', $description_array[$qt_lang['lang']] );

								} else {
									echo $hotel_array['_hotel_description'];
								}
							echo '</div>';

							// show view details link if rates are added
							if( array_key_exists( '_rates', $hotel_array ) && $hotel_array['_rates'] != '' ) {
								echo '<p class="showmobile view_details"><a onclick="toggleByClass( \'show-'. $hotel_id .'\' )">'. $display_text['more_details'] .' &raquo;</a></p>';
							}

						}

						if( array_key_exists( '_rates', $hotel_array ) && $hotel_array['_rates'] != '' ) {

							echo '<div style="display: none;" class="travel_toggle show-'. $hotel_id .'">';

								if( $qt_lang['enabled'] == 1 ) {

									$rates_array = qtrans_split( $hotel_array['_rates'] );
									echo apply_filters( 'the_content', $rates_array[$qt_lang['lang']] );

								} else {
									echo $hotel_array['_rates'];
								}

							echo '</div>';

						}

						if( array_key_exists( '_westin_vip', $hotel_array ) && $hotel_array['_westin_vip'] == 'on' ) {

							echo '<div class="travel_westin">';

								/**
								 * retrieve westin video from blog 1's travel page
								 */
								switch_to_blog( 1 );

									$blog1_travel_page_id	= get_id_by_slug( 'travel' );

									$westin_youtube_thumb	= get_post_meta( $blog1_travel_page_id, '_rnr3_westin_youtube_thumb', 1 );
									$westin_youtube			= get_post_meta( $blog1_travel_page_id, '_rnr3_westin_youtube', 1 );

									echo '<figure>
										<a data-fancybox href="javascript:;" data-src="http://www.youtube.com/embed/'. $westin_youtube .'" class="video-modal">
											<img src="'. $westin_youtube_thumb .'">
											<span class="icon-play"></span>
										</a>
									</figure>';

								restore_current_blog();

								echo '<p>'. $display_text['westin'] .'</p>
								<p><a class="highlight westin" href="'. $hotel_array['_westin_vip_link'] . microtime() .'" target="_blank">'. $display_text['westin_btn'] .'</a></p>
							</div>';
						}

					echo '</div>';

					// right column
					if( $qt_lang['enabled'] == 1 ) {
						$lowest_rate_array	= array_key_exists( '_lowest_rate', $hotel_array ) ? qtrans_split( $hotel_array['_lowest_rate'] ) : '';
						$lowest_rate		= $lowest_rate_array != '' ? $lowest_rate_array[$qt_lang['lang']] : '';
					} else {
						$lowest_rate = array_key_exists( '_lowest_rate', $hotel_array ) ? $hotel_array['_lowest_rate'] : '';
					}

					echo '<div class="hotel_content_right">
						<span>'. $display_text['aslowas'] .'</span>
						<span class="hotel_content_right_price">'. $lowest_rate .'</span>';

						// show bed type
						if( array_key_exists( '_bed_types', $hotel_array ) && $hotel_array['_bed_types'] != '' ) {

							if( $qt_lang['enabled'] == 1 ) {
								$bed_type_array	= qtrans_split( $hotel_array['_bed_types'] );
								$bed_type		= $bed_type_array[$qt_lang['lang']];
							} else {
								$bed_type = $hotel_array['_bed_types'];
							}

							echo '<p class="bed_type">'. $bed_type .'</p>';
						}

						// show view details link if rates are added
						if( array_key_exists( '_rates', $hotel_array ) && $hotel_array['_rates'] != '' ) {
							echo '<p class="showdesktop view_details"><a onclick="toggleByClass( \'show-'. $hotel_id .'\' )">'. $display_text['more_details'] .' &raquo;</a></p>';
						}

						if( !empty( $amenities ) ) {

							echo '<div class="hotel_content_right_amenities">';

								foreach( $amenities as $key => $value ) {

									echo '<span class="icon-'. $value .'"></span>';

								}

							echo '</div>';

						}

						if( array_key_exists( '_sold_out', $hotel_array ) && $hotel_array['_sold_out'] == 'on' ) {
							echo '<p class="highlight black">'. $display_text['sold_out'] .'</p>';
						} elseif( array_key_exists( '_reservation_link', $hotel_array ) && $hotel_array['_reservation_link'] != '' ) {

							if( $qt_lang['enabled'] == 1 ) {
								$reservationlink_array	= array_key_exists( '_reservation_link', $hotel_array ) ? qtrans_split( $hotel_array['_reservation_link'] ) : '';
								$reservationlink		= $reservationlink_array != '' ? $reservationlink_array[$qt_lang['lang']] : '';
							} else {
								$reservationlink = array_key_exists( '_reservation_link', $hotel_array ) ? $hotel_array['_reservation_link'] : '';
							}

							echo '<p><a class="cta" href="'. $reservationlink .'" target="_blank">'. $display_text['rsv_onl'] .'</a></p>';
						}

					echo '</div>
				</div>
			</div>
		</div>';

	}

} ?>
