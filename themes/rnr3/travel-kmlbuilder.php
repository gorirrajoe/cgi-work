<?php

	require_once( "../../../wp-load.php" );

	global $wpdb;
	global $post;

	$siteurl		= get_option( 'siteurl' );

	$market_query	= $_REQUEST['market_query'];
	$myRaceDetails	= $wpdb->get_row( "SELECT wm_event_race_id, wm_event_name, wm_event_information FROM wp_waypoint_event WHERE wm_marketlist='".$market_query."'" );
	$eventid		= $myRaceDetails->wm_event_race_id;
	$half_event_id	= $wpdb->get_var( "SELECT wm_id FROM wp_waypoint_map WHERE wm_event_id='".$eventid."' AND wm_course_name LIKE '%half%'" );

	header( 'Content-type: application/vnd.google-earth.kml+xml' );

?><kml xmlns="http://www.opengis.net/kml/2.2">
	<Document>
		<?php
			// marker images
			$myMarkers	= $wpdb->get_results( "SELECT wmp_id, wmp_title, wmp_image FROM wp_waypoint_map_points order by wmp_id" );
			$index		= 0;

			foreach( $myMarkers as $row1 ) {

				if ( $index == 0 ) {
					echo "<Style id=\"CustomMarker_".$row1->wmp_id."\">\n";
				} else {
					echo "    <Style id=\"CustomMarker_".$row1->wmp_id."\">\n";
				}

				echo "      <IconStyle>\n";
				echo "        <Icon>\n";
				echo "          <href><![CDATA[".$siteurl.$row1->wmp_image."]]></href>\n";
				echo "        </Icon>\n";
				echo "        <scale>1.0</scale>\n";
				echo "      </IconStyle>\n";
				echo "    </Style>\n";

				$index++;

			}
		?>
		<name>
			<![CDATA[Rock 'n' Roll Marathon]]>
		</name>

		<Folder>
			<visibility>1</visibility>

			<?php
				echo "      <description>\n";
				echo "        <![CDATA[Event Information: ". $myRaceDetails->wm_event_information ."]]>\n";
				echo "      </description>\n";
			?>

			<Folder>
				<visibility>1</visibility>

				<name>
					 Custom Markers
				</name>

				<?php
					// waypoint details -- only grab data for half marathon
					$waypointDetailQuery = "SELECT wm_wmp_id, wm_event_id, wm_id, wmp_id, wm_wmp_lat, wm_wmp_lon, wm_wmp_title, wm_wmp_description FROM wp_waypoint_details WHERE wm_event_id=". $eventid ." AND wmp_id IN ( 11,4,13,7 ) AND wm_id=". $half_event_id ." ORDER BY wm_wmp_id";

					$myWaypointDetails	= $wpdb->get_results( $waypointDetailQuery );
					$index				= 0;

					foreach ( $myWaypointDetails as $row ) {

						if ( $index == 0 ) {
							echo "<Placemark>\n";
						} else {
							echo "        <Placemark>\n";
						}

						echo "          <name>". $row->wm_wmp_title ."</name>\n";
						echo "          <styleUrl>#CustomMarker_". $row->wmp_id ."</styleUrl>\n";
						echo "          <description><![CDATA[". $row->wm_wmp_description ."]]></description>\n";
						echo "          <Point>\n";
						echo "            <styleUrl>#CustomMarker_". $row->wmp_id ."</styleUrl>\n";
						echo "            <coordinates>". $row->wm_wmp_lon .",". $row->wm_wmp_lat ."</coordinates>\n";
						echo "          </Point>\n";
						echo "          <Icon>\n";
						echo "            <href>". $siteurl . $row1->wmp_image ."</href>\n";
						echo "          </Icon>\n";
						echo "          <scale>1.0</scale>\n";
						echo "        </Placemark>\n";

						$index++;

					}
					//$term = get_term( $market_query, 'marketlists' );

					$args = array(
						'post_type'			=> 'hotels',
						'orderby'			=> 'name',
						'order'				=> 'ASC',
						'posts_per_page'	=> 200,
						'suppress_filters'	=> true,
						'post_status'		=> 'publish',
					 );

					switch_to_blog( $market_query );

						$query = new WP_Query( $args );

						if( $query->have_posts() ) {
							while( $query->have_posts() ) {
								$query->the_post();

								$lat_long_check = trim( get_post_meta( $post->ID, '_lat_long', true ) );

								if( $lat_long_check != '' ) {

									$hq_hotel		= get_post_meta( $post->ID, '_hq_hotel', true );
									$featured_hotel	= get_post_meta( $post->ID, '_featured_hotel', true );

									if( $hq_hotel == 1 ) {
										$hotel_type = "Headquarter ";
									} elseif( $featured_hotel == 1 ) {
										$hotel_type = "Featured ";
									} else {
										$hotel_type = "Official ";
									}

									$sold_out = get_post_meta( $post->ID, '_sold_out', true );

									if( $sold_out ) {
										$sold_out_text = ' - SOLD OUT';
									} else {
										$sold_out_text = '';
									}

									// $hotel_link			= get_post_meta( $post->ID, '_hotel_link', true );
									$hotel_address		= get_post_meta( $post->ID, '_hotel_address', true );
									$hotel_city			= get_post_meta( $post->ID, '_hotel_city', true );
									$hotel_state		= get_post_meta( $post->ID, '_hotel_state', true );
									$hotel_zip			= get_post_meta( $post->ID, '_hotel_zip', true );
									$hotel_phone		= get_post_meta( $post->ID, '_hotel_phone', true );
									$reservation_link	= get_post_meta( $post->ID, '_reservation_link', true );

									if( $reservation_link && ( $sold_out != 1 ) ) {
										$reservation_text = '<a href="'.$reservation_link.'" target="_blank" onclick="_gaq.push( [\'_trackEvent\', \'Travel Page Links\', \'Reserve Online Button Map\', \''. $post->ID .'\'] );">Reserve Online</a>';
									} else {
										$reservation_text = '';
									}

									$rates		= get_post_meta( $post->ID, '_lowest_rate', true );
									$lat_long	= explode( ',', $lat_long_check );
									$lat		= $lat_long[0];
									$long		= $lat_long[1];

									echo "        <Placemark>\n";
									echo "          <name>". $hotel_type ."Hotel". $sold_out_text ."</name>\n";
									echo "          <styleUrl>#CustomMarker_7</styleUrl>\n";
									echo "          <description><![CDATA[<a href=\"". $reservation_link ."\">". get_the_title() ."</a><br />". $hotel_address ."<br />". $hotel_city .", ". $hotel_state ." ". $hotel_zip . "<br /><br />as low as " . $rates . "<br /><br />" . $reservation_text . "]]></description>\n";
									echo "          <Point>\n";
									echo "            <styleUrl>#CustomMarker_7</styleUrl>\n";
									echo "            <coordinates>". $long .",". $lat ."</coordinates>\n";
									echo "          </Point>\n";
									echo "          <Icon>\n";
									echo "            <href>". $siteurl ."/wp-content/pluginshare/waypoint-map/images/V5jsiREsj0n86lfORUiY.png</href>\n";
									echo "          </Icon>\n";
									echo "          <scale>1.0</scale>\n";
									echo "        </Placemark>\n";
								}
							}
						}

						wp_reset_postdata();

					restore_current_blog();
				?>
			</Folder>
		</Folder>
	</Document>
</kml>
