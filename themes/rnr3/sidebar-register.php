<?php
	/**
	 * sidebar for the register page
	 */
	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';

	$prefix = '_rnr3_';

	$formatted_date	= get_event_date( $qt_lang[ 'lang' ], $market );

	$participants	= get_post_meta( get_the_ID(), $prefix . 'participants_receive', 1 ) != '' ? apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'participants_receive', 1 ) ) : '';
	$override_txt	= get_post_meta( get_the_ID(), $prefix . 'general_info_override', 1 ) != '' ? apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'general_info_override', 1 ) ) : '';
	$override_old_txt = get_post_meta( get_the_ID(), '_override', 1 ) != '' ? apply_filters( 'the_content', get_post_meta( get_the_ID(), '_override', 1 ) ) : '';

?>

<div class="column">

	<?php if( $override_txt != '' ) {

		echo $override_txt;

	} elseif( $override_old_txt != '' ) {

		echo $override_old_txt;

	} else { ?>

		<h2><?php echo $general_info_txt; ?></h2>

		<?php if( $event_info->official_name != '' ) {

			if( $qt_lang['enabled'] == 1 ) {
				$official_name_split = qtrans_split( $event_info->official_name );

				echo '<p>'. stripslashes( $official_name_split[$qt_lang['lang']] ) .'</p>';
			} else {
				echo '<p>'. stripslashes( $event_info->official_name ) .'</p>';
			}

		} ?>

		<p>
			<?php if( $qt_lang['lang'] != 'en' ) {
				if( $formatted_date[ 'monthdateeuro' ] == 'TBD' ) {
					echo $formatted_date[ 'monthdateeuro' ];
				} else {
					echo $formatted_date[ 'monthdateeuro' ] .', ' . $formatted_date[ 'year' ];
				}
			} else {
				if( $formatted_date[ 'monthdate' ] == 'TBD' ) {
					echo $formatted_date[ 'monthdate' ];
				} else {
					echo $formatted_date[ 'monthdate' ] .', ' . $formatted_date[ 'year' ];
				}
			} ?>
		</p>

	<?php }


	if( $participants != '' ) {
		echo '<h2>'. $txt_pr .'</h2>' .
		$participants;
	} ?>

</div>
