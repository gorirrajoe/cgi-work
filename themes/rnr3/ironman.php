<?php
	/* Template Name: Ironman Landing Page */
	get_header( 'special' );

	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';
?>

<!-- main content -->
<main role="main" id="main">
	<section class="wrapper grid_1">
		<div class="content">
			<?php
				if (have_posts()) : while (have_posts()) : the_post();
					echo '<h2>'. get_the_title(). '</h2>';
					the_content();
				endwhile; endif;
			?>
		</div>
	</section>

</main>

<?php
	get_footer('series');
?>
