<?php 
  /* Template Name: Bling (Series) */
  get_header('oneoffs');
?>
  <style>
    .pixlee {
      margin-bottom:60px;
    }
    .blinggroup {
      margin:60px 0;
    }
    .narrow {
      width:75%;
      margin:auto;
    }
    .grid_3 {
      overflow:hidden;
      margin:60px 0;
      padding:60px 0;
      border-top:1px solid #e5e5e5;
      border-bottom:1px solid #e5e5e5;
    }
    .grid_centered .column {
      text-align:center;
    }
    .grid_centered .column img {
      margin-bottom:30px;
    }
    .bling-nav {
      display:table;
      margin:auto;
      background-color:#e5e5e5;
    }
    .bling-nav li {
      float:left;
    }
    .bling-nav ul {
      margin:0;
      list-style:none;
    }
    .bling-nav li {
      margin:0;
    }
    .bling-nav .bullet-title {
      padding:30px;
    }
    .bling-nav a {
      padding:30px;
      display:block;
    }
    .bling-nav a:hover,
    .bling-nav .bling-current-page {
      background-color:#eee;
    }
    #bling-carousel {
      border-top:1px solid #e5e5e5;
      border-bottom:1px solid #e5e5e5;
      margin:60px 0;
    }
    #bling-carousel .owl-prev,
    #bling-carousel .owl-next {
      top:50%;
    }
    #bling-carousel .owl-pagination {
      margin-bottom:30px;
    }
    #bling-carousel .medal_info {
      background-color: #fff;
      border:1px solid #e5e5e5;
      position: absolute;
      padding:15px;
      top: 20px;
      width: 100%;
      z-index: 200;
    }
    .blinggroup .grid_4 .column {
      text-align:center;
      margin-bottom:30px;
    }
  </style>
  <!-- main content -->
  <main role="main" id="main">
    <section class="wrapper">
      <div class="bling-nav">
        <?php
          $bling_active = $remix_active = $le_active = $hm_active = $hof_active = '';
          if( empty( $_GET ) ) {
            $bling_active = 'class="bling-current-page"';
          } elseif( $_GET['hm'] == 'remix-challenge-medals' ) {
            $remix_active = 'class="bling-current-page"';
          } elseif( $_GET['hm'] == 'limited-edition-medals' ) {
            $le_active = 'class="bling-current-page"';
          } elseif( $_GET['hm'] == 'heavy-medals' ) {
            $hm_active = 'class="bling-current-page"';
          } elseif( $_GET['hm'] == 'hall-of-fame' ) {
            $hof_active = 'class="bling-current-page"';
          }
        ?>
        <ul>
          <li class="bullet-title">MEDALS</li>
          <li><a <?php echo $bling_active; ?> href="<?php echo site_url('/bling/'); ?>">Run for the Bling</a></li>
          <li><a <?php echo $remix_active; ?> href="<?php echo site_url('/bling/?hm=remix-challenge-medals'); ?>">Remix Challenge Medals</a></li>
          <li><a <?php echo $le_active; ?> href="<?php echo site_url('/bling/?hm=limited-edition-medals'); ?>">Limited Edition Medals</a></li>
          <li><a <?php echo $hm_active; ?> href="<?php echo site_url('/bling/?hm=heavy-medals'); ?>">Heavy Medals</a></li>
          <li><a <?php echo $hof_active; ?> href="<?php echo site_url('/bling/?hm=hall-of-fame'); ?>">Hall of Fame</a></li>
        </ul>
      </div>

      <?php if( empty( $_GET ) ) { ?>
        <section class="blinggroup center">
          <div class="narrow">
            <h1>Run For The Bling</h1>
            <p>Do you #RunForTheBling? There are three unique opportunities to earn extra bling by completing multiple Rock 'n' Roll Marathon Series events. Once you're ready to commit, <a href="<?php echo site_url('/tourpass/'); ?>">learn more about TourPass</a> and run multiple races for a single price!</p>
            <p><a href="<?php echo site_url('/tourpass/'); ?>">Rock the Pass</a>. Earn the Bling.</p>
          </div>
        </section>


        <div class="grid_3 grid_centered">
          <div class="column">
            <a href="?hm=remix-challenge-medals"><img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/selection1.png"></a>
            <h3><a href="?hm=remix-challenge-medals">Remix Challenge<br>Medals</a></h3>
            <p>One weekend. Two days of running.<br>
            <a href="?hm=remix-challenge-medals">See Remix Challenge Medals</a></p>

          </div>
          <div class="column">
            <a href="?hm=limited-edition-medals"><img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/selection2.png"></a>
            <h3><a href="?hm=limited-edition-medals">Limited Edition<br>Medals</a></h3>
            <p>Collect these combos, while you can.<br>
            <a href="?hm=limited-edition-medals">See Limited Edition Medals</a></p>
          </div>
          <div class="column">
            <a href="?hm=heavy-medals"><img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/selection3.png"></a>
            <h3><a href="?hm=heavy-medals">Heavy Medals</a></h3>
            <p>In it for the long haul? <a href="?hm=heavy-medals">Enroll today</a>.<br>
            <a href="?hm=heavy-medals">See Heavy Medals</a></p>
          </div>
        </div>
      <?php } elseif( $_GET['hm'] == 'remix-challenge-medals' ) { ?>
        <section class="blinggroup center">
          <div class="narrow">
            <p><strong>1 WEEKEND, 2 DAYS OF RUNNING</strong></p>
            <h1>Remix Challenge Medals</h1>
            <p>Ok, you are already training for one day... why not make it two and earn some sweet bling?! If you just can't get enough Rock 'n' Roll running on your weekend, the Remix Challenge is definitely for you.</p>
          </div>
        </section>

        <div id="bling-carousel" class="owl-carousel">
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/remix-vb.png">
            <h3><a href="<?php echo network_site_url('/virginia-beach/'); ?>">Virginia Beach <?php if(date('Y') != '2015'){ echo '2015';}?></a></h3>
          </div>
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/remix-sj.png">
            <h3><a href="<?php echo network_site_url('/san-jose/'); ?>">San Jose <?php if(date('Y') != '2015'){ echo '2015';}?></a></h3>
          </div>
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/remix-stl.png">
            <h3><a href="<?php echo network_site_url('/st-louis/'); ?>">St. Louis <?php if(date('Y') != '2015'){ echo '2015';}?></a></h3>
          </div>
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/remix-van.png">
            <h3><a href="<?php echo network_site_url('/vancouver/'); ?>">Vancouver <?php if(date('Y') != '2015'){ echo '2015';}?></a></h3>
          </div>
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/remix-sav.png">
            <h3><a href="<?php echo network_site_url('/savannah/'); ?>">Savannah <?php if(date('Y') != '2015'){ echo '2015';}?></a></h3>
          </div>
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/remix-lv.png">
            <h3><a href="<?php echo network_site_url('/las-vegas/'); ?>">Las Vegas <?php if(date('Y') != '2015'){ echo '2015';}?></a></h3>
          </div>
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/remix-sa.png">
            <h3><a href="<?php echo network_site_url('/san-antonio/'); ?>">San Antonio <?php if(date('Y') != '2015'){ echo '2015';}?></a></h3>
          </div>
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/placeholder2.png">
            <h3><a href="<?php echo network_site_url('/arizona/'); ?>">Arizona <?php if(date('Y') != '2016'){ echo '2016';}?></a></h3>
          </div>
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/placeholder2.png">
            <h3><a href="<?php echo network_site_url('/dallas/'); ?>">Dallas <?php if(date('Y') != '2016'){ echo '2016';}?></a></h3>
          </div>
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/placeholder2.png">
            <h3><a href="<?php echo network_site_url('/raleigh/'); ?>">Raleigh <?php if(date('Y') != '2016'){ echo '2016';}?></a></h3>
          </div>
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/placeholder2.png">
            <h3><a href="<?php echo network_site_url('/liverpool/'); ?>">Liverpool <?php if(date('Y') != '2016'){ echo '2016';}?></a></h3>
          </div>
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/placeholder2.png">
            <h3><a href="<?php echo network_site_url('/chicago/'); ?>">Chicago <?php if(date('Y') != '2016'){ echo '2016';}?></a></h3>
          </div>
        </div>

        <section class="blinggroup">
          <div class="narrow">

            <h2>About Remix Challenge Medals</h2>
            <p>Remix Challenge Medals are earned by completing two days of running during a Rock 'n' Roll Marathon Series event.</p>
            <h2>How To Qualify</h2>
            <p>Any combination of distances qualifies for the Remix Challenge Medal. Participants must complete one event on Saturday and one event on Sunday during the same Rock 'n' Roll Marathon Series event weekend. You do not need to be enrolled as an official Heavy Medalist to qualify.</p>
            <h2>Availability</h2>
            <p>Remix Challenge Medals will be presented at the Rock 'n' Roll Marathon Series tent located at the finish line festival upon completion of the second day. The lineup of Remix Challenge Medals is subject to change each year depending on the Rock 'n' Roll Marathon Series Tour schedule.</p>

            <h2>FAQ</h2>
            <div class="accordion">

              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling1">What distances qualify for the Remix Challenge Medal?</div>
                <div class="accordion-section-content" id="bling1">
                  <p>Any distance will qualify you for the Remix Challenge Medal as long as you complete one distance on Saturday and one distance on Sunday during an event.</p>
                </div>
              </div>
              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling2">How do you register for the Remix Challenge?</div>
                <div class="accordion-section-content" id="bling2">
                  <p>Register for each day normally, as you would if you were registering for any individual event. Make sure you use the same name and date of birth for each registration, as this information must be consistent for us to match up your records.</p>
                </div>
              </div>
              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling3">If I am already registered for one event, can I still register for the second day to earn a Remix Challenge Medal?</div>
                <div class="accordion-section-content" id="bling3">
                  <p>Yes! If you have already registered for one event, simply register using the same name and date of birth for an event on the second day. As long as your information matches, you will qualify for a Remix Challenge Medal!</p>
                </div>
              </div>
              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling4">How will I receive my Remix Challenge Medal?</div>
                <div class="accordion-section-content" id="bling4">
                  <p>Remix Challenge Medals will be awarded after you finish the second day of running. You will receive an email with instructions to pick up your Remix Challenge Medal if you qualify.</p>
                </div>
              </div>

            </div>

            <h2>Terms & Conditions</h2>
            <p>Competitor Group, Inc. reserves the right to change the qualification, terms, award designs, and artwork presented here at any time.</p>
          </div>
        </section>

      <?php } elseif( $_GET['hm'] == 'limited-edition-medals' ) { ?>
        <section class="blinggroup center">
          <div class="narrow">
            <p><strong>COLLECT THESE COMBOS WHILE YOU CAN</strong></p>
            <h1>Limited Edition Medals</h1>
            <p>Constantly looking for new bling to add to your collection? Don't hold back now! Get the <a href="<?php echo site_url('/tourpass/'); ?>">TourPass</a> and pack your bags... these Limited Edition Medals will not be around forever!</p>
          </div>
        </section>

        <div id="bling-carousel" class="owl-carousel">
          <div>
            <a class="show" id="link1"><img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015_world_rocker.png"></a>
            <h3>World Rocker</h3>
            <p>Click medal for more info</p>
            <div style="display: none;" class="medal_info" id="medal_info_1">
              <p>Qualification requirements: Complete a Rock 'n' Roll Marathon or Half Marathon in two different countries within the 2015 calendar year. Please visit the <a target="_blank" href="http://runrocknroll.competitor.com/heavy-medals/world-rocker-en">World Rocker</a> website for a full list of eligibility requirements and more information on how to qualify!</p>
            </div>
          </div>
          <div>
            <a class="show" id="link2"><img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016_desert_double_down.png"></a>
            <h3>Desert Double Down</h3>
            <p>Click medal for more info</p>
            <div style="display: none;" class="medal_info" id="medal_info_2">
              <p>BACK BY POPULAR DEMAND! Qualification Requirements: Complete the half or full marathon distance at both the <a href="http://www.runrocknroll.com/las-vegas/news/2015/08/double-down/" target="_blank">2015 Rock 'n' Roll Las Vegas</a> and the <a href="http://www.runrocknroll.com/arizona/news/2015/08/desertdoubledown/" target="_blank">2016 Rock 'n' Roll Arizona</a></p>
            </div>
          </div>
          <div>
            <a class="show" id="link9"><img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015-mex-combo.png"></a>
            <h3>Mexi Combo</h3>
            <p>Click medal for more info</p>
            <div style="display: none;" class="medal_info" id="medal_info_9">
              <p>ONE YEAR ONLY! Qualification Requirements: Complete the half marathon distance at both the 2015 Rock 'n' Roll Mexico City and the 2015 Rock 'n' Roll Merida</p>
            </div>
          </div>
          <div>
            <a class="show" id="link5"><img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015_cascadia2.png"></a>
            <h3>Cascadia</h3>
            <p>Click medal for more info</p>
            <div style="display: none;" class="medal_info" id="medal_info_5">
              <p>Qualification Requirements: Complete the 8K, 10K, half or full marathon distance at the following three events in 2015: Rock 'n' Roll Portland, Rock 'n' Roll Seattle and Rock 'n' Roll Oasis Vancouver</p>
            </div>
          </div>
          <div>
            <a class="show" id="link6"><img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015_cali_combo1.png"></a>
            <h3>Cali Combo</h3>
            <p>Click medal for more info</p>
            <div style="display: none;" class="medal_info" id="medal_info_6">
              <p>Qualification Requirements: Complete the half or full marathon distance at 3 out of 4 of our California events in 2015: Rock 'n' Roll San Francisco, Suja Rock 'n' Roll San Diego, Rock 'n' Roll San Jose, Rock 'n' Roll Los Angeles</p>
            </div>
          </div>
          <div>
            <a class="show" id="link7"><img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015_southern_charm.png"></a>
            <h3>Southern Charm</h3>
            <p>Click medal for more info</p>
            <div style="display: none;" class="medal_info" id="medal_info_7">
              <p>Qualification Requirements: Complete the half or full marathon distance at 3 of the following 4 Rock 'n' Roll events in 2015: Rock 'n' Roll New Orleans, Rock 'n' Roll Raleigh, St. Jude Country Music Nashville, Rock 'n' Roll Savannah</p>
            </div>
          </div>
          <div>
            <a class="show" id="link8"><img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015_beast_east.png"></a><br>
            <h3>Beast of the East</h3>
            <p>Click medal for more info</p>
            <div style="display: none;" class="medal_info" id="medal_info_8">
              <p>Qualification Requirements: Complete the half or full marathon distance at 3 out of 4 of the following events in 2015: Rock 'n' Roll DC, Rock 'n' Roll Virginia Beach, Rock 'n' Roll Brooklyn, Rock 'n' Roll Philadelphia</p>
            </div>
          </div>
          <div>
            <a class="show" id="link3"><img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015_louisiana_double_down.png"></a>
            <h3>Louisiana Double Down</h3>
            <p>Click medal for more info</p>
            <div style="display: none;" class="medal_info" id="medal_info_3">
              <p>ONE YEAR ONLY! Qualification requirements: Complete any distance at the 2015 Louisiana Marathon and any distance at the 2015 Rock 'n' Roll New Orleans Marathon &amp; &frac12;</p>
            </div>
          </div>
          <div>
            <a class="show" id="link4"><img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015_dallas_duo-retired.png"></a>
            <h3>Dallas Duo</h3>
            <p>Click medal for more info</p>
            <div style="display: none;" class="medal_info" id="medal_info_4">
              <p>Qualification Requirements: Complete the 2015 Rock 'n' Roll Dallas Half Marathon and the 2015 MetroPCS Dallas Marathon or Half Marathon</p>
            </div>
          </div>
        </div>

        <section class="blinggroup">
          <div class="narrow">
            <h2>About Limited Edition Medals</h2>
            <p>Limited Edition Medals are earned by completing a specific combination of qualifying Rock 'n' Roll Marathon Series events.</p>
            <h2>How To Qualify</h2>
            <p>Qualification requirements are specified individually for each medal - pay attention to the distances and events listed for each Limited Edition Medal. Limited Edition Medals will be awarded to participants who meet these requirements. You do not need to be enrolled as an official Heavy Medalist to qualify.</p>
            <h2>Availability</h2>
            <p>The lineup of Limited Edition Medals is subject to change each year. These medals are not guaranteed to be produced year after year. Earn them while you can!</p>
            <h2>FAQ</h2>

            <div class="accordion">

              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling1">How do I earn a Limited Edition Medal?</div>
                <div class="accordion-section-content" id="bling1">
                  <p>Each Limited Edition Medal has a specific set of requirements (events and distances) that need to be completed. Be sure you register using the same name and date of birth for each event so we can match up your records. Participants must have a time posted on the <a href="<?php echo site_url('/results/'); ?>">official Results</a> to get credit for an event.</p>
                </div>
              </div>
              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling2">What if my results are not showing for an event?</div>
                <div class="accordion-section-content" id="bling2">
                  <p>If you recently completed an event and your results are not listed, please fill out a Results Correction Request form on our results website: <a href="<?php echo site_url('/contact/corrections/'); ?>">Results Correction</a>. This form will go directly to the timing company for review. Please be sure to put in all information that could help them accurately locate your results. They will then either respond directly or post your results within 5-7 business days. Results Correction Requests will only be accepted up to three weeks after the day of an event.</p>
                </div>
              </div>
              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling3">How will I receive my Limited Edition Medal?</div>
                <div class="accordion-section-content" id="bling3">
                  <p>Limited Edition Medals will be awarded at the finish line of your qualifying event unless stated otherwise. You will receive an email with instructions to pick up your Limited Edition Medal if you qualify.</p>
                </div>
              </div>

            </div>

            <h2>Terms &amp; Conditions</h2>
            <p>Competitor Group, Inc. reserves the right to change the qualification, terms, award designs, and artwork presented here at any time.</p>
          </div>
        </section>

      <?php } elseif( $_GET['hm'] == 'heavy-medals' ) { ?>
        <section class="blinggroup center">
          <div class="narrow">
            <p><strong>IN IT FOR THE LONG HAUL?</strong></p>
            <h1>Heavy Medals</h1>
            <p>You have your <a href="<?php echo site_url('/tourpass/'); ?>">TourPass</a>. Your tickets are booked. Your bling awaits! <a href="https://register.runrocknroll.com/Register/?event=30556" target="_blank">Enroll</a> today and start your 2015 Heavy Medal journey!</p>
            <p><a class="cta" href="https://register.runrocknroll.com/Register/?event=30556" target="_blank">Enroll Today</a><p>
            <p>Already enrolled? <a href="https://register.runrocknroll.com/Register/registrantSearch.aspx?event=30556" target="_blank">Confirm enrollment here</a>.</p>
            <p>Follow Heavy Medals on <a href="https://www.facebook.com/HeavyMedal" target="_blank">Facebook</a></p>
          </div>
        </section>

        <div id="bling-carousel" class="owl-carousel">
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2-Double-Beat.png">
            <h3>Double Beat</h3>
            <p>Complete two qualifying 2015 events</p>
          </div>
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/3-Rock-Trio.png">
            <h3>Rock Trio</h3>
            <p>Complete three qualifying 2015 events</p>
          </div>
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/4-Groupie.png">
            <h3>Groupie</h3>
            <p>Complete four qualifying 2015 events</p>
          </div>
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/5-Roadie.png">
            <h3>Roadie</h3>
            <p>Complete five qualifying 2015 events</p>
          </div>
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/6-Six-String.png">
            <h3>Six-String</h3>
            <p>Complete six qualifying 2015 events</p>
          </div>
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/7-Stairway-to-Seven.png">
            <h3>Stairway to Seven</h3>
            <p>Complete seven qualifying 2015 events</p>
          </div>
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/8-Eight-Track.png">
            <h3>Eight Track</h3>
            <p>Complete eight qualifying 2015 events</p>
          </div>
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/9-Headliner.png">
            <h3>Headliner</h3>
            <p>Complete nine qualifying 2015 events</p>
          </div>
          <div>
            <img alt="" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/10-Gold-Record.png">
            <h3>Gold Record</h3>
            <p>Complete ten qualifying 2015 events</p>
          </div>
        </div>

        <section class="blinggroup">
          <div class="narrow">
            <h2>About Heavy Medals</h2>
            <p>Rock the Tour and earn more bling when you finish two or more Rock 'n' Roll Marathon Series events in a single calendar year (Jan-Dec 2015). Earn multiple Heavy Medals in one year by completing additional events!</p>

          </div>
        </section>
        <section class="blinggroup">
          <div class="narrow">

            <h2>How To Qualify</h2>
            <ol>
              <li>Enroll as an official Heavy Medalist</li>
              <li>Complete two or more 2015 Rock 'n' Roll Marathon Series Tour Stops - only half and full marathon distances count toward your Heavy Medals total</li>
              <li>Receive your Heavy Medals in the mail 8-10 weeks after your qualifying event</li>
              <li>Once you reach Roadie status (5+), you'll receive an e-mail invitation the week of the event to pick up your medal at the finish line*<br>
              (*all medals earned at an event outside of the United States and Canada will be mailed unless stated otherwise prior to the event)</li>
            </ol>
          </div>
        </section>
        <section class="blinggroup">
          <div class="narrow">

            <h2>Tips For The Journey</h2>
            <ul>
              <li>Heavy Medals will only be awarded to participants who enroll as official Heavy Medalists. Sign up using a RaceIt.com account to make Rock 'n' Roll event registration easy.</li>
              <li>Register using the same first and last name, and date of birth for all events. If your records don't match, you won't get a medal!</li>
              <li>Enroll in the Heavy Medals program using an address where you can receive mail. Medals will be mailed to the address listed on your Heavy Medals enrollment. Think yours is old? Send an address update request, and be sure to update your RaceIt.com profile for the future.</li>
              <li>You must have a time posted on the official Results to get credit for an event. If you have an issue with your results, please fill out a <a href="<?php echo site_url('/contact/corrections/'); ?>">Results Correction Request</a> within 30 days of the event.</li>
              <li>Still have a question? It may already be answered - check out the Heavy Medals FAQ</li>
            </ul>
          </div>
        </section>
        <section class="blinggroup">
          <div class="narrow">

            <h2>General Heavy Medals Questions</h2>
            <div class="accordion">

              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling1">Do I have to enroll as an official Heavy Medalist every year?</div>
                <div class="accordion-section-content" id="bling1">
                  <p>Yes - to keep an accurate database of our Heavy Medalists, we need you to <a href="https://register.runrocknroll.com/Register/?event=30556" target="_blank">enroll</a> in the program each year. Thank you!</p>
                </div>
              </div>
              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling2">Are Heavy Medals affected by the event distance I completed?</div>
                <div class="accordion-section-content" id="bling2">
                  <p>Only half marathon or marathon events count for Heavy Medals qualifications. Relay, 10k, 5K, and other ancillary event distances do not count toward earning Heavy Medals.</p>
                </div>
              </div>
              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling3">Will I receive a Heavy Medal after every additional marathon and &frac12; marathon or will I only receive one for the highest level attained?</div>
                <div class="accordion-section-content" id="bling3">
                  <p>Once you have enrolled as an official Heavy Medalist, you will receive a Heavy Medal for completing two or more qualifying Rock 'n' Roll Marathon Series events. As you complete additional events, you will qualify for additional medals.</p>
                </div>
              </div>
              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling4">Do International events count towards my Heavy Medals total?</div>
                <div class="accordion-section-content" id="bling4">
                  <p>Yes! New for 2015, Rock 'n' Roll Marathon Series events outside of North America will count towards your Heavy Medals total. Due to customs and other logistics, all medals earned at events outside of the United States and Canada will be mailed unless stated otherwise prior to the event (even Roadie and greater).</p>
                </div>
              </div>

            </div>
           </div>
        </section>
        <section class="blinggroup">
          <div class="narrow">

            <h2>Registration Questions</h2>
            <div class="accordion">

              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling5">How do I start receiving my Heavy Medals?</div>
                <div class="accordion-section-content" id="bling5">
                  <p>In order to receive Heavy Medals, you must <a href="https://register.runrocknroll.com/Register/?event=30556" target="_blank">enroll</a> as an official Heavy Medalist. Make sure your address and information is current; medals will be sent to the address used on your Heavy Medals enrollment.</p>
                </div>
              </div>
              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling6">Do I need an account on RaceIt to receive Heavy Medals?</div>
                <div class="accordion-section-content" id="bling6">
                  <p>When you register for Rock 'n' Roll Marathon Series events, you have the option to create an account through RaceIt.com. To ensure you information is consistent between registrations, we strongly recommend you use an account during registration. However, it is not necessary to use a RaceIt.com account.</p>
                </div>
              </div>
              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling7">I registered online for one event and then at an expo for another. Will I still receive Heavy Medals?</div>
                <div class="accordion-section-content" id="bling7">
                  <p>Yes - as long as you are enrolled as an official Heavy Medalist. Make sure to use the same name and date of birth for each registration. Be mindful of hyphens, abbreviations and spelling. If you have two registrations with different information, we will not be able to match your race completions and things will get confusing quickly.</p>
                </div>
              </div>
              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling8">I registered with a charity. Will this registration count toward Heavy Medals?</div>
                <div class="accordion-section-content" id="bling8">
                  <p>Yes, but make sure to use the same name and date of birth. Be mindful of hyphens, abbreviations and spelling. If you have two registrations with different information, our awards system probably won't send you anything shiny.</p>
                </div>
              </div>
              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling9">I registered with a charity. Will this registration count toward Heavy Medals?</div>
                <div class="accordion-section-content" id="bling9">
                  <p>Yes, but make sure to use the same name and date of birth. Be mindful of hyphens, abbreviations and spelling. If you have two registrations with different information, our awards system probably won't send you anything shiny.</p>
                </div>
              </div>
              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling10">Can I register to become a Heavy Medalist after I have already completed 2 or more events?</div>
                <div class="accordion-section-content" id="bling10">
                  <p>Yes, you may register for Heavy Medals at any time during the year. We will begin to send you medals after your next qualifying event. If you would like to request a previous medal, please <a href="<?php echo site_url('/contact/heavy-medal-contact/'); ?>">contact us</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="blinggroup">
          <div class="narrow">

            <h2>Heavy Medals Not Received</h2>
            <div class="accordion">
              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling11">I have not received my medal. How can I check the status?</div>
                <div class="accordion-section-content" id="bling11">
                  <p>Please allow at least 8-10 weeks to receive your award. Medals are typically shipped 6-8 weeks after your qualifying event, and typical delivery time after shipment is 2-3 weeks. Around holidays and other busy periods, medal delivery may be delayed. If you have not received your medal after this time frame, please <a href="<?php echo site_url('/contact/heavy-medal-contact/'); ?>">contact us</a>. <strong>Common Reasons Medals are NOT Received</strong>:</p>
                  <ul>
                    <li>You are not enrolled as an official Heavy Medalist</li>
                    <li>The mailing address listed on your Heavy Medals enrollment is incorrect or out-of-date</li>
                    <li>Your name or date of birth differed between registrations</li>
                    <li>You didn't complete the half or full marathon distance</li>
                    <li>The finishing time for one of your races wasn't recorded properly</li>
                    <li>Not sure the problem? <a href="<?php echo site_url('/contact/heavy-medal-contact/'); ?>">Contact us</a> and we'll help you figure it out.</li>
                  </ul>
                </div>
              </div>
              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling12">My Heavy Medal was sent to an old address. Can I still receive it?</div>
                <div class="accordion-section-content" id="bling12">
                  <p>We always send medal awards to the address indicated on your Heavy Medals enrollment. If this address was incorrect or out-of-date, we will not send you a replacement medal. To avoid this issue, please ensure that your address is current. If not, <a href="<?php echo site_url('/contact/heavy-medal-contact/'); ?>">contact us</a> prior to the event and we'll assist you in updating it.</p>
                </div>
              </div>
              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling13">I should have received a Heavy Medal in the past. Can I still receive it?</div>
                <div class="accordion-section-content" id="bling13">
                  <p>Heavy Medals may be requested up to 6 months after the qualifying event, but are only offered as supplies last. Medal designs and inventory change on a yearly basis, so we don't typically carry Heavy Medals from previous years.</p>
                </div>
              </div>
              <div class="accordion-section" style="position: relative;">
                <div class="accordion-section-title" data-reveal="#bling14">I did not pick up my Roadie (or greater) after my qualifying race - can I still receive it?</div>
                <div class="accordion-section-content" id="bling14">
                  <p>If you did not pick up your medal award after the qualifying race, please <a href="<?php echo site_url('/contact/heavy-medal-contact/'); ?>">contact us</a>. Heavy Medals may be requested up to 6 months after the qualifying event, but are only offered as supplies last.</p>
                </div>
              </div>

            </div>
          </div>
        </section>
        <section class="blinggroup">
          <div class="narrow">
            <h2>Terms & Conditions</h2>
            <p>Competitor Group, Inc. reserves the right to change the qualification, terms, award designs, and artwork presented here at any time.</p>
          </div>
        </section>

      <?php } elseif( $_GET['hm'] == 'hall-of-fame' ) { ?>
        <section class="blinggroup center">
          <div class="narrow">
            <p><strong>ROCK 'N' ROLL MARATHON SERIES HEAVY MEDALS</strong></p>
            <h1>Hall of Fame</h1>
            <p>The Rock 'n' Roll Marathon Series Heavy Medals Hall of Fame recognizes the outstanding achievement of Heavy Medalists for the calendar year. Official Heavy Medalists will be inducted into the Hall of Fame once they have completed 15 qualifying distances on the <a href="<?php echo site_url(); ?>/#findrace">2015 Tour Stop schedule</a>.</p>
            <img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015_hall_of_fame.png">
            <p>Complete 15 qualifying 2015 events</p>
          </div>
        </section>
        <section class="blinggroup center">
          <h2>2014 Heavy Medals Hall of Famers</h2>
          <div class="grid_4">
            <div class="column">
              <img width="300" height="300" alt="Arnold-L" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Arnold-L.jpg" class="aligncenter size-medium wp-image-53144"><br>
              Arnold L.
            </div>
            <div class="column">
              <img width="300" height="300" alt="Callie-U" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Callie-U.jpg" class="aligncenter size-medium wp-image-53145"><br>
              Callie U.
            </div>
            <div class="column">
              <img width="300" height="300" alt="Carl M." src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Carl-M.jpg" class="aligncenter size-medium wp-image-53146"><br>
              Carl M.
            </div>
            <div class="column">
              <img width="300" height="300" alt="Christopher-S" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Christopher-S.jpg" class="aligncenter size-medium wp-image-53147"><br>
              Christopher S.
            </div>
            <div class="column">
              <img width="300" height="300" alt="David-H" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/David-H.jpg" class="aligncenter size-medium wp-image-53148"><br>
              David H.
            </div>
            <div class="column">
              <img width="300" height="300" alt="Heather-D" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Heather-D.jpg" class="aligncenter size-medium wp-image-53149"><br>
              Heather D.
            </div>
            <div class="column">
              <img width="300" height="300" alt="Hyun-Joo-P" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Hyun-Joo-P.jpg" class="aligncenter size-medium wp-image-53150"><br>
              Hyun Joo P.
            </div>
            <div class="column">
              <img width="300" height="300" alt="Ilona-M" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Ilona-M.jpg" class="aligncenter size-medium wp-image-53151"><br>
              Ilona M.
            </div>
            <div class="column">
              <img width="300" height="300" alt="Jade-Shyree-K" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Jade-Shyree-K.jpg" class="aligncenter size-medium wp-image-53152"><br>
              Jade Shyree K.
            </div>
            <div class="column">
              <img width="300" height="300" alt="Jessica V" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Jessica-V.jpg" class="alignnone size-medium wp-image-87320"><br>
              Jessica V.
            </div>
            <div class="column">
              <img width="300" height="300" alt="Jocelin-R" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Jocelin-R.jpg" title="Jocelin R." class="aligncenter size-medium rel="><br>
              Jocelin R.
            </div>
            <div class="column">
              <img width="300" height="300" alt="Joseph-H" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Joseph-H.jpg" class="aligncenter size-medium wp-image-53155"><br>
              Joseph H.
            </div>
            <div class="column">
              <img width="300" height="300" alt="Juan-A" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Juan-A.jpg" class="aligncenter size-medium wp-image-53156"><br>
              Juan A.
            </div>
            <div class="column">
              <img width="300" height="300" alt="Justin-B" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Justin-B.jpg" class="aligncenter size-medium wp-image-53157"><br>
              Justin B.
            </div>
            <div class="column">
              <img width="300" height="300" alt="Kelvin-S" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Kelvin-S.jpg" class="aligncenter size-medium wp-image-53158"><br>
              Kelvin S.
            </div>
            <div class="column"><img width="300" height="300" alt="KristinP-3" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/KristinP-3.jpg" class="aligncenter size-medium wp-image-53306"><br>Kristin P.</div>
            <div class="column"><img width="300" height="300" alt="Luzviminda-V" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Luzviminda-V.jpg" class="aligncenter size-medium wp-image-53159"><br>Luzviminda V.</div>
            <div class="column"><img width="300" height="300" alt="MartinE-2" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Martin-E.jpg" class="aligncenter size-medium wp-image-53307"><br>Martin E.</div>
            <div class="column"><img width="300" height="300" alt="Michael-H" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Michael-H.jpg" class="aligncenter size-medium wp-image-53160"><br>Michael H.</div>
            <div class="column"><img width="300" height="300" alt="Mindy-E" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Mindy-E.jpg" class="aligncenter size-medium wp-image-53161"><br>Mindy E.</div>
            <div class="column"><img width="300" height="300" alt="Mitchell-G" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Mitchell-G.jpg" class="aligncenter size-medium wp-image-53162"><br>Mitchell G.</div>
            <div class="column"><img width="300" height="300" alt="Rodney-M" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Rodney-M.jpg" class="aligncenter size-medium wp-image-53163"><br>Rodney M.</div>
            <div class="column"><img width="300" height="300" alt="Ronald C" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Ronald-C.jpg" class="alignnone size-medium wp-image-87334"><br>Ronald C.</div>
            <div class="column"><img width="300" height="300" alt="Ron C" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Ron-C-300x300.jpg" class="alignnone size-medium wp-image-87333"><br>Ron C.</div>
            <div class="column"><img width="300" height="300" alt="susanc" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Susan-C.jpg" class="aligncenter size-medium wp-image-53315"><br>Susan C.</div>
            <div class="column"><img width="300" height="300" alt="Wesley-S" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/heavy-medals/Wesley-S.jpg" class="aligncenter size-medium wp-image-53166"><br>Wesley S.</div>
          </div>
        </section>

        <section class="blinggroup">

          <div class="accordion">
            <div class="accordion-section" style="position: relative;">
              <div class="accordion-section-title" data-reveal="#bling15">What will Rock 'n' Roll Heavy Medals Hall of Famers receive? </div>
              <div class="accordion-section-content" id="bling15">
                <p>Heavy Medals Hall of Famers will receive a vanity bib to mark their achievement for the 15th event and each event thereafter. They will also receive an induction package that includes the Hall of Fame Medal, a Rock 'n' Roll Marathon Series backpack and more. The name and photo (with permission) of each 2015 Rock 'n' Roll Marathon Series Hall of Famer will be displayed on our website and at each expo in 2016.</p>
              </div>
            </div>
            <div class="accordion-section" style="position: relative;">
              <div class="accordion-section-title" data-reveal="#bling16">Do I receive anything extra for completing the most events within the Rock 'n' Roll Heavy Medals Hall of Fame?</div>
              <div class="accordion-section-content" id="bling16">
                <p>The Heavy Medals Hall of Famer(s) that completes the highest number of qualifying events in the calendar year will receive a one-of-a-kind plaque to commemorate their rockin' year!</p>
              </div>
            </div>
            <div class="accordion-section" style="position: relative;">
              <div class="accordion-section-title" data-reveal="#bling17">Will International events count toward earning Heavy Medals Hall of Fame status? </div>
              <div class="accordion-section-content" id="bling17">
                <p>Yes! New for 2015, Rock 'n' Roll Marathon Series events outside of North America will count towards your Heavy Medals total. Pack your bags!</p>
              </div>
            </div>

          </div>

        </section>
      

      <?php } ?>
      </section>
      <section class="wrapper pixlee">
        <script id="pixlee_script" src="//assets.pixlee.com/assets/pixlee_widget_v3.js"></script><script>// <![CDATA[
        var pixlee = new PixleeWidget("https://photos.pixlee.com/competitorgroup/2991/v2?horizontal=true&#038;filter_sort_id=23350","pixlee_container", "https://photos.pixlee.com/", document.location.hash);pixlee.setupWidget(true);
        // ]]></script>
      </section>

  </main>
  <script type="text/javascript">
    jQuery(function() {
      jQuery(".medal_info").hide();
      jQuery("html").click(function() {
        jQuery('div[id^="medal_info_"]').hide();
      });
      jQuery('.show').click(function(event){
        event.stopPropagation();
      });
      jQuery('a[id^="link"]').click(function(){
          var vid_id = jQuery(this).attr("id").replace("link", "#medal_info_");
          //alert(vid_id);
          jQuery('div[id^="medal_info_"]').hide();
          jQuery(vid_id).show();
      });
    });
  </script>
<?php get_footer( 'oneoffs' ); ?>
