<?php
/**
 * only show global variables on blog 1
 */
require( 'inc/rnr-theme-options.php' );
require( 'inc/rnr-shortcodes.php' );
require( 'inc/broadcast-tempo-mods.php' );

add_action( 'init', array( 'Lockdown_Broadcasted_Posts', 'singleton' ) );
add_action( 'init', array( 'RNR_Shortcodes', 'singleton' ) );


if ( ! function_exists( 'rnr3_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function rnr3_setup() {

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );


		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => __( 'Primary Menu', 'rnr3' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'gallery',
			'caption',
		) );


		/*
		 * Registering new image sizes.
		*/
		add_image_size( 'infobox-thumbs', 360, 240, true );
		add_image_size( 'news-promos-thumbs', 200, 200, false );
		add_image_size( 'past-galleries', 360, 360, false );
		add_image_size( 'travel-hotel', 300, 300, true );
		add_image_size( 'rnb-ambassador-thumb', 200, 200, true );
		// add_image_size( 'rnb-ambassador-thumblg', 300, 300, true );
		add_image_size( 'max-twocol', 900, 0, false );
		add_image_size( 'newsletter-post', 570, 350, true ); // Newsletter
		add_image_size( 'newsletter-splash', 620, 400, true ); // Newsletter
		add_image_size( 'newsletter-image', 300, 200, true ); // Newsletter
		add_image_size( 'moments-thumb', 300, 300, false );
		add_image_size( 'moments-video-thumb', 300, 170, true );
		add_image_size( 'distance-gallery', 1000, 600, false );
		add_image_size( 'finisher-medals', 400, 400, false );
		// add_image_size( 'findrace-gallery', 570, 350, true );
		add_image_size( 'regional-lg', 960, 550, true );
		add_image_size( 'regional', 750, 400, true );

		if ( in_array( get_blog_details()->path, array( '/finisher-zone/', '/virtual-run/' ) ) ) {
			add_image_size( 'fz-cert-thumb', 160, 160, false );
		}
	}
endif; // rnr3_setup
add_action( 'after_setup_theme', 'rnr3_setup' );


/**
 * Register widget area.
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function rnr3_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'rnr3' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'rnr3_widgets_init' );


/**
 * Enqueue scripts and styles.
 */
function rnr3_scripts() {
	$version = '13.2';
	wp_enqueue_style( 'rnr3-style', get_template_directory_uri() . '/style.min.css', array(), $version );

	// header scripts
	wp_enqueue_script( 'rnr3-modernizr', get_template_directory_uri() . '/js/vendor/modernizr.min.js', array(), false, false );

	if( is_page_template( 'course-maps.php' ) ) {
		wp_enqueue_script( 'google-jsapi', 'https://www.google.com/jsapi', array(), false, false );
		wp_enqueue_script( 'google-maps-api', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false', array(), false, false );
		wp_enqueue_style( 'google-maps-style', 'https://developers.google.com/maps/documentation/javascript/examples/default.css' );
	}

	/*
	if( is_page_template( 'ambassadors.php' ) || is_post_type_archive( 'ambassador' ) || is_singular( 'ambassador' ) ) {
		wp_enqueue_style( 'ambos', get_template_directory_uri() . '/css/ambassadors.min.css', array(), false, false );
	}
	*/

	if( is_page_template( array( 'nrd.php', 'nrd-training.php' ) ) ) {
		wp_enqueue_style( 'nrd-googleFonts', '//fonts.googleapis.com/css?family=Slabo+13px|Amatic+SC:700|Pacifico|Open+Sans+Condensed:700' );
		wp_enqueue_style( 'nrd-style', get_template_directory_uri() . '/css/page-templates/nrd.min.css', array(), filemtime( get_template_directory() . '/css/page-templates/nrd.min.css' ) );
	}

	if( is_page_template( array( 'moments-series.php', 'music-series.php', 'medals-series.php', 'medals-series-2.php', 'miles-series.php' ) ) ) {
		wp_enqueue_style( 'mmmm-style', get_template_directory_uri() . '/css/page-templates/mmmm.min.css', array(), filemtime( get_template_directory() . '/css/page-templates/mmmm.min.css' ) );
	}

	if( is_page_template( array( 'humana-event.php', 'humana-home.php' ) ) ) {
		wp_enqueue_style( 'humana-style', get_template_directory_uri() . '/css/page-templates/humana.min.css', array(), filemtime( get_template_directory() . '/css/page-templates/humana.min.css' ) );
	}

	if( is_page_template( array( 'st-jude-series.php', 'register-page.php', 'register-page-v2.php', 'presale-page.php' ) ) ) {
		wp_enqueue_style( 'st-jude-style', get_template_directory_uri() . '/css/page-templates/st-jude.min.css', array(), filemtime( get_template_directory() . '/css/page-templates/st-jude.min.css' ) );
	}

	$market = get_market2();

	if( $market == 'finisher-zone' ) {
		wp_enqueue_style( 'finisher-zone-style', get_template_directory_uri() . '/css/page-templates/finisher-zone.min.css', array(), filemtime( get_template_directory() . '/css/page-templates/finisher-zone.min.css' ) );
	}

	if( is_page_template( 'moments-event.php' ) ) {
		wp_enqueue_script( 'panel-switcher', get_template_directory_uri() . '/js/jquery.content-panel-switcher.min.js', array(), false, false );
	}

	if( is_page_template( array( 'regional-parent.php', 'regional-child.php' ) ) ) {
		wp_enqueue_style( 'regional-style', get_template_directory_uri() . '/css/page-templates/regional.min.css', array(), filemtime( get_template_directory() . '/css/page-templates/regional.min.css' ) );
	}

	if( $market == 'virtual-run' ) {
		wp_enqueue_style( 'virtual-run-style', get_template_directory_uri() . '/css/page-templates/virtual-run.min.css', array(), filemtime( get_template_directory() . '/css/page-templates/virtual-run.min.css' ) );
	}

	if( is_page_template( 'event-dates.php' ) ) {
		wp_enqueue_script( 'table-sorter', get_stylesheet_directory_uri() . '/js/vendor/sorttable.js', array( 'jquery' ), filemtime( get_template_directory() . '/js/vendor/sorttable.js' ), true );
	}

	// footer scripts
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', '//code.jquery.com/jquery-1.11.2.min.js', array(), false, false );
	wp_enqueue_script( 'jquery' );
	wp_deregister_script( 'jquery-ui-core' );
	wp_register_script( 'jquery-ui-core', '//code.jquery.com/ui/1.11.4/jquery-ui.min.js', array('jquery'), false, true );
	wp_enqueue_script( 'jquery-ui-core' );

	wp_enqueue_script( 'rnr3-main', get_template_directory_uri() . '/js/rnr3.min.js', array(), $version, true );

	if( is_front_page() ) {
		// for the pixlee gallery
		wp_enqueue_script( 'rnr3-masonry', '//cdnjs.cloudflare.com/ajax/libs/masonry/3.3.1/masonry.pkgd.min.js', array(), false, false );
	}

	if( is_page_template( array( 'finisher-zone-search-and_results.php', 'fb-share.php', 'finisher-zone.php', 'fz-vr-search-and-results.php' ) ) ) {
		wp_enqueue_script( 'fz-scripts', get_template_directory_uri() . '/js/fz-scripts.min.js', array('jquery','rnr3-main'), $version, true );
	}

	if( is_page_template( 'exporeg.php' ) ) {
		wp_enqueue_style( 'exporeg-style', get_template_directory_uri() . '/css/page-templates/exporeg.min.css', array('rnr3-style'), filemtime( get_template_directory() . '/css/page-templates/exporeg.min.css' ) );
		wp_enqueue_script( 'exporeg-script', get_stylesheet_directory_uri() . '/js/exporeg.min.js', array('jquery', 'jquery-ui-core', 'rnr3-main' ), $version, true );

		wp_localize_script( 'exporeg-script', 'expo_post_data', array(
			'ajax_url'  => admin_url( 'admin-ajax.php' ),
			'nonce'     => wp_create_nonce( 'exporeg_nonce' ),
		) );
	}

	if( is_page_template( 'grd.php' ) ) {
		wp_enqueue_style( 'grd-googleFonts', '//fonts.googleapis.com/css?family=Fredericka+the+Great|Montserrat:400,700' );
		wp_enqueue_style( 'grd-style', get_template_directory_uri() . '/css/page-templates/grd.min.css', array(), filemtime( get_template_directory() . '/css/page-templates/grd.min.css' ) );

		wp_enqueue_script( 'grd-script', get_stylesheet_directory_uri() . '/js/page-templates/grd/grd.min.js', array(), filemtime( get_template_directory() . '/js/page-templates/grd/grd.min.js' ), true );

		// set up ajax info for ajax call request
		wp_localize_script( 'rnr3-main', 'mix_ajax_data', array(
			'ajax_url' => admin_url( 'admin-ajax.php' )
		) );
	}


	/**
	 * welcome template essentially uses the grd template, but had to decouple it so we can change colors and such
	 */
	if( is_page_template( 'welcome-locations.php' ) ) {
		wp_enqueue_style( 'grd-googleFonts', '//fonts.googleapis.com/css?family=Montserrat:400,700,800,900' );
		wp_enqueue_style( 'grd-style', get_template_directory_uri() . '/css/page-templates/welcome.min.css', array(), filemtime( get_template_directory() . '/css/page-templates/welcome.min.css' ) );

		wp_enqueue_script( 'grd-script', get_stylesheet_directory_uri() . '/js/page-templates/welcome/welcome.min.js', array(), filemtime( get_template_directory() . '/js/page-templates/welcome/welcome.min.js' ), true );

		// set up ajax info for ajax call request
		wp_localize_script( 'rnr3-main', 'mix_ajax_data', array(
			'ajax_url' => admin_url( 'admin-ajax.php' )
		) );
	}

	/*if( is_page_template( 'winter-promo.php' ) ) {
		wp_enqueue_style( 'wp-googleFonts', '//fonts.googleapis.com/css?family=Fredericka+the+Great' );
		wp_enqueue_style( 'winter-promo-style', get_template_directory_uri() . '/css/page-templates/winter-promo.min.css', array(), filemtime( get_template_directory() . '/css/page-templates/winter-promo.min.css' ) );

		// set up ajax info for ajax call request
		wp_localize_script( 'rnr3-main', 'mix_ajax_data', array(
			'ajax_url'  => admin_url( 'admin-ajax.php' )
		) );
	}*/

	if( ( is_main_site() && is_front_page() ) || is_404() ) {
		// set up ajax info for findrace ajax call request
		wp_localize_script( 'rnr3-main', 'findrace_ajax_data', array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
		) );
	}

	if( $market == 'running-sale' ) {
		wp_enqueue_style( 'running-sale-style', get_template_directory_uri() . '/css/page-templates/running-sale.min.css', array(), filemtime( get_template_directory() . '/css/page-templates/running-sale.min.css' ) );

		// set up ajax info for ajax call request
		wp_localize_script( 'rnr3-main', 'mix_ajax_data', array(
			'ajax_url' => admin_url( 'admin-ajax.php'
		) ) );
	}

	if( is_page_template( 'tourpass.php' ) ) {
		wp_enqueue_style( 'tourpass-style', get_template_directory_uri() . '/css/page-templates/tourpass.min.css', array(), filemtime( get_template_directory() . '/css/page-templates/tourpass.min.css' ) );
	}

	if( is_page_template( 'what-rocked-series.php' ) ) {
		wp_enqueue_style( 'what-rocked-style', get_template_directory_uri() . '/css/page-templates/what-rocked.min.css', array(), filemtime( get_template_directory() . '/css/page-templates/what-rocked.min.css' ) );
	}

	if( $market == 'montreal' ) {
		wp_enqueue_script( 'grayscale-script', get_stylesheet_directory_uri() . '/js/grayscale-sponsor.min.js', array(), filemtime( get_template_directory() . '/js/grayscale-sponsor.min.js' ), true );
	}
}
add_action( 'wp_enqueue_scripts', 'rnr3_scripts' );


/**
 * Adding Google fonts asynchronously because it blocks page loading if loaded old fashion way
 */
function add_fonts_asynchronously() { ?>

	<script>
		WebFontConfig = {
			classes: false,
			events: false,
			google: {
				families: ['Open+Sans+Condensed:300,700', 'Open+Sans:400,600,700']
			},
		};

		(function(d) {
			var wf = document.createElement('script');
			wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
				'://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
		})(document);
	</script>

	<?php
}
add_action( 'wp_head', 'add_fonts_asynchronously', 0 );


/**
 * ADMIN FUNCTIONS
 */
	/**
	 * ADMIN FUNCTION enqueue scripts and styles
	 */
	function load_custom_wp_admin_style() {
		wp_enqueue_script('jquery-ui-datepicker', '/wp-includes/js/jquery/ui/jquery.ui.datepicker.min.js', array('jquery'), false, true );
		wp_enqueue_style('jquery-ui-style', '//ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css');
	}
	add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );


	/**
	 * ADMIN FUNCTION: datepicker (or other js) init
	 */
	function rnr3_js_init() {
		echo '<script>
			jQuery(function($) {
				jQuery(document).ready(function() {
					jQuery(".datepicker").datepicker();
				});
				jQuery( ".event_day_tab" ).tabs();
				jQuery( ".event_day_accordion" ).accordion({
					collapsible: true,
					navigation: true,
					active: false,
					header: "h4",
					heightStyle: "content"
				});
				function checkDelete(){
					var r=confirm("Click Ok to continue DELETING this event.");
					if (r==true){
						return true;
					}
					return false;
				}
			});
		</script>';
		//wp_register_script( 'my-uploader', get_bloginfo( 'stylesheet_directory' ) . '/js/uploader2.js', array('jquery', 'media-upload', 'thickbox') );
		//wp_enqueue_script('my-uploader');
		wp_enqueue_script( 'jquery-ui-accordion', array('jquery') );
		wp_enqueue_script( 'jquery-ui-tabs', array('jquery') );
		wp_enqueue_style( 'ui-style', '//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css' );

	}
	add_action( 'admin_footer', 'rnr3_js_init');


	/**
	 * function: cmb2 filter to exclude a metabox from a template
	 */
	function be_metabox_exclude_on_template( $display, $meta_box ) {

		if ( !isset( $meta_box['show_on']['key'] ) )
			return $display;

		if( isset( $meta_box['show_on']['key'] ) && 'exclude_template' !== $meta_box['show_on']['key'] )
			return $display;

		// Get the current ID
		if( isset( $_GET['post'] ) ) $post_id = $_GET['post'];
		elseif( isset( $_POST['post_ID'] ) ) $post_id = $_POST['post_ID'];
		if( !isset( $post_id ) ) return false;

		$template_name = get_page_template_slug( $post_id );
		$template_name = substr($template_name, 0, -4);

		// If value isn't an array, turn it into one
		if( isset( $meta_box['show_on']['value'] ) ) {
			if( !is_array( $meta_box['show_on']['value'] ) ) {
				$showonvalue = array( $meta_box['show_on']['value'] );
			} else {
				$showonvalue = $meta_box['show_on']['value'];
			}
		} else {
			$showonvalue = array();
		}

		// See if there's a match
		if( in_array( $template_name, $showonvalue ) ) {
			return false;
		} else {
			return true;
		}
	}
	add_filter( 'cmb2_show_on', 'be_metabox_exclude_on_template', 10, 2 );


	/**
	 * ADMIN function: to show or hide custom metaboxes, depending on the page template chosen
	 * will need to keep this for metaboxes that don't use cmb2 (yet)
	 */
	function custom_metabox_per_template() {
		global $pagenow, $typenow;
		if ( is_admin() && in_array( $pagenow, array('post.php', 'post-new.php') ) && $typenow == 'page' ) { ?>
			<script type='text/javascript'>
				jQuery(document).ready(function($) {
					//hide all metaboxes
					function hide_all_custom_metaboxes(){
						$('#register-page').hide();
						$('#regional-child-date').hide();
						// $('#register-page-v2').hide();
						// $('#presale-page').hide();
					}
					//show a metabox
					function show_custom_metabox(meta_id){
						var selector = "#"+meta_id;
						if( $(selector).length)
							$(selector).show();
					}
					//first hide all metaboxes
					hide_all_custom_metaboxes();
					//then check for selected page template and show the corect metabox
					var current_metabox = $('#page_template').val();

					if ( undefined !== current_metabox ) {

						var split = current_metabox.split(".php");
						var split_name = split[0];
						show_custom_metabox(split_name);

					}
					//and last listen for changes update when needed
					$('#page_template').bind("change", function(){
						hide_all_custom_metaboxes();
						//show_custom_metabox($('#page_template').val());
						var current_metabox = $('#page_template').val();
						var split = current_metabox.split(".php");
						var split_name = split[0];
						show_custom_metabox(split_name);
					});
				});
			</script>
		<?php }
	}
	add_action( 'admin_footer', 'custom_metabox_per_template' );


	/**
	 * ADMIN function: don't display menu settings to non-admins
	 */
	function remove_menus_from_editors() {
		if( !current_user_can( 'edit_theme_options' ) ) {
			remove_submenu_page( 'themes.php', 'nav-menus.php' );
		}
	}
	add_action( 'admin_menu', 'remove_menus_from_editors', 999 );


	/**
	 * Filters the content to remove any extra paragraph or break tags
	 * caused by shortcodes.
	 *
	 * @since 1.0.0
	 *
	 * @param string $content  String of HTML content.
	 * @return string $content Amended string of HTML content.
	 */
	function shortcode_empty_paragraph_fix( $content ) {
		$array = array(
				'<p>['    => '[',
				']</p>'   => ']',
				']<br />' => ']'
		);
		return strtr( $content, $array );

	}
	add_filter( 'the_content', 'shortcode_empty_paragraph_fix' );


	/**
	 * FUNCTION: updated gallery shortcode
	 * works with fancybox
	 */
	remove_shortcode( 'gallery' );


	/**
	 * Enable qTranslate for WordPress SEO
	 * @param string $text The string to translate
	 * @return string
	 */
	function qtranslate_filter( $text ) {
		return __( $text );
	}

	add_filter( 'wpseo_title', 'qtranslate_filter', 10, 1 );
	add_filter( 'wpseo_metadesc', 'qtranslate_filter', 10, 1 );
	add_filter( 'wpseo_metakey', 'qtranslate_filter', 10, 1 );
	add_filter( 'wpseo_opengraph_title', 'qtranslate_filter', 10, 1 );


	/**
	 * allow editor to upload svg files
	 */
	function custom_upload_mimes ( $existing_mimes=array() ) {
		// add the file extension to the array
		$existing_mimes['svg'] = 'mime/type';
		// call the modified list of extensions
		return $existing_mimes;
	}
	add_filter('upload_mimes', 'custom_upload_mimes');

	/**
	 * Removes some input fields from the user profile screen
	 */
	function rnr3_remove_user_fields( $contactmethods ) {
		unset( $contactmethods['googleplus'] );
		unset( $contactmethods['twitter'] );
		unset( $contactmethods['facebook'] );
		return $contactmethods;
	}
	add_filter( 'user_contactmethods', 'rnr3_remove_user_fields',10,1 );


	/**
	* get list of events
	* used as a cmb2 callback function
	*/
	function get_updated_event_array() {
		global $wpdb;
		$event_array = $wpdb->get_results( "SELECT event_slug, event_location FROM wp_rocknroll_events WHERE event_slug != 'series' ORDER BY event_location" );
		$updated_event_array = array();

		foreach( $event_array as $event ) {
			$updated_event_array[ esc_attr( $event->event_slug . '::' . $event->event_location ) ] = $event->event_location;
		}

		return $updated_event_array;
	}

	/**
	 * Add a minimum amount of sites the user has access to
	 * For the "my-site-search" plugin
	 *
	 * @return integer	Min number of sites
	 */
	function limit_admin_site_search() {
		return 10;
	}

	add_filter( 'mms_show_search_minimum_sites', 'limit_admin_site_search' );


/**
 * END ADMIN FUNCTIONS
 */


/**
 * FRONT-END FUNCTIONS
 */
	/**
	 * function: get the market of the current blog
	 */
	function get_market2() {
		$get_event_details	= get_blog_details();
		$market				= $get_event_details->path;
		$market				= str_replace('/rnr/', '', $market);
		$market				= str_replace('/', '', $market);

		if( $market == '' ) { // series
			$market = 'series';
		}
		return $market;
	}


	/**
	 * function: get global nav
	 * usage: header and footer
	 * used to have transient cache, but won't work properly with foreign langs
	 */
	function rnr3_get_global_nav( $lang, $div_id = 'global-rnr-menu' ) {

		$menu_id		= 'global-'.$lang;

		$nav_settings = array(
			'menu'				=> $menu_id,
			'container_class'	=> 'global_menu',
			'fallback_cb'		=> false,
			'theme_location'	=> '__no_such_location',
			'echo'				=> false,
			'container_id'		=> $div_id
		);

		$global_menu = wp_nav_menu( $nav_settings );

		return $global_menu;
	}


	/**
	 * function: add language dropdown (if necessary) to the end of global nav
	 * don't enable this for blog 1
	 */
	function add_language_dropdown( $items, $args ) {
		if( get_current_blog_id() != 1 && is_plugin_active( 'qtranslate-x/qtranslate.php' ) ){
			$currentLang = qtrans_getLanguage();

			if( $args->menu == 'global-'.$currentLang ) {
				$market = get_market2();
				$items .= '<li class="lang"><a href="#" class="lang_dropdown">'.$currentLang.'<div class="arrow-down"></div></a>';

					$avalLangs	= qtrans_getSortedLanguages();
					$lang_count	= count( $avalLangs );
					$currentURL	= parse_url( $_SERVER['REQUEST_URI'] );
					$currentURL	= $currentURL['path'];
					$sortLangs	= array();
					//make selected lang 0
					//
					$i = 1;

					foreach ( $avalLangs as $key => $value ){
						if( $value == $currentLang ){
							$sortLangs[0] = $value;
						} else {
							$sortLangs[$i] = $value;
							$i++;
						}
					}

					ksort( $sortLangs );

					$items .= '<ul class="sub-menu sub-menu-'.$lang_count.'">';

						foreach ( $sortLangs as $key2 => $value2 ) {
							if( $value2 != $currentLang ){
								$urlToUse = str_replace( '/'.$currentLang.'/', '/'.$value2.'/', $currentURL );
								//if home page or no lang prefix
								if (strpos ( $currentURL, '/'.$currentLang.'/' ) <= 0 ){
									$urlToUse = str_replace( '/'.$market.'/', '/'.$market.'/'.$value2.'/', $currentURL);
								}
							} else {
								$urlToUse = $currentURL;
							}
							$items .= '<li class="'. $value2 .'"><a href="'.$urlToUse.'">'.$value2.'</a></li>';
						}

					$items .= '</ul>
				</li>';
			}

		}
		return $items;
	}
	add_filter( 'wp_nav_menu_items', 'add_language_dropdown', 10, 2 );


	/**
	 * function to set the $event_info object
	 * caches the query for an amount of time for other php files to access
	 */
	function rnr3_get_event_info( $market, $fz = false ) {
		global $wpdb;

		$transient_string = $fz ? 'event_info_'.$market : 'event_info_data_' . $market;

		if ( false === ( $event_info = get_transient( $transient_string ) ) ) {
			$event_info = $wpdb->get_row(
				"SELECT publish, event_name, event_url, event_date, event_slug, reg_open, event_sold_out, event_time, twitter_hashtag, festival, festival_start_date, festival_end_date, event_location, event_logo, facebook_page, instagram_acct, twitter_acct, header_image, mobile_header_image, header_settings, alert_msg, event_sponsor_blurb, has_races, big_red_boxes, box1_hdr, box2_hdr, box3_hdr, box4_hdr, box1_blurb, box2_blurb, box3_blurb, box4_blurb, box1_url, box2_url, box3_url, box4_url, youtube_video, official_name, is_sold_out, special_hero_option, special_hero_small_txt, special_hero_small_txt_2, special_hero_headline_txt, youtube_acct, hero_license, hero_headline, hero_elevator FROM wp_rocknroll_events WHERE event_slug = '".$market."'"
			);

			set_transient( $transient_string, $event_info, 1 * MINUTE_IN_SECONDS );
		}
		return $event_info;
	}


	/**
	 * function: get all events
	 * usage: $all_events gets used in rnr3_findrace() function
	 */
	function rnr3_get_all_events() {
		global $wpdb;

		if ( false === ( $all_events = get_transient( 'all_events_data' ) ) ) {
			$all_events = $wpdb->get_results(
				"SELECT id, event_name, event_url, event_date, event_location, event_slug, event_sold_out, festival, festival_start_date, festival_end_date, reg_open, has_races, nrd_btn_txt, findrace_desc, findrace_gallery1, findrace_gallery2, findrace_gallery3, findrace_gallery4 FROM wp_rocknroll_events WHERE event_slug != 'series' and publish = 1 ORDER BY CASE WHEN event_date < NOW() THEN 1 ELSE 0 END, event_date"
			);

			set_transient( 'all_events_data', $all_events, 5 * MINUTE_IN_SECONDS );
		}
		return $all_events;

	}


	/**
	 * function: get data from specific blogs
	 * usage: used for event dates page
	 * returns array of meta for requested fields
	 * takes array of variable names and meta fields
	 */
	function rnr3_get_meta_from_blog( $event_slug = null, $metas = null ) {

		if( is_array( $metas ) == false ){return;}
		global $wpdb;

		//if ( false === ( $all_metas = get_transient( "all_meta_data_{$event_slug}" ) ) ) {
			$new_blog = $wpdb->get_results(
				"SELECT `blog_id` FROM `wp_blogs` WHERE `path` LIKE '%{$event_slug}%'"
			);
			$blog_metas = "wp_{$new_blog[0]->blog_id}_postmeta";
			$all_metas = array();
			foreach($metas as $key => $value){
				$meta_result = $wpdb->get_results("SELECT `meta_value` FROM $blog_metas WHERE `meta_key` LIKE '{$value}';");
				if (count($meta_result) > 0 ){
					$all_metas[$key] = $meta_result[0]->meta_value;
				} else {
					$all_metas[$key] ='';
				}
			}
		// 	set_transient(  "all_meta_data_{$event_slug}", $all_metas, 5 * MINUTE_IN_SECONDS );
		// }
		return $all_metas;
	}


	/**
	 * function: get the image id based off its url
	 * used on homepage for boxes
	 *
	 * @return ID of image or NULL if not found
	 */
	function rnr3_get_image_id($image_url) {
		global $wpdb;
		$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url ));
		return !empty($attachment) ? $attachment[0] : null;
	}


	/**
	 * function: create the "big red boxes" section
	 * show all boxes on the homepage, but only top two on subpages
	 */
	function rnr3_get_big_red_boxes( $market ) {
		if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
			$event_info = rnr3_get_event_info( $market );
		}

		if( is_front_page() ) {
			// show all 4 boxes on homepage
			$limit = 5;
		} else {
			$limit = 3;
		}

		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

		if( is_plugin_active( 'qtranslate-x/qtranslate.php' ) && function_exists( 'qtrans_getLanguage' ) ) {
			$qT_lang['lang']    = qtrans_getLanguage();
			$qT_lang['enabled'] = 1;
		} else{
			$qT_lang['lang']    = 'en';
			$qT_lang['enabled'] = 0;
		}

		echo '<section id="promospots">
			<section class="wrapper">
				<ul>';
					for( $i = 1; $i < $limit; $i++ ) {

						if( $event_info->{'box'.$i.'_hdr'}  != '' ) {

							if( $qT_lang['enabled'] == 1 ) {
								$bx_array = qtrans_split( $event_info->{'box'.$i.'_hdr'} );
								$blurb_array = qtrans_split( $event_info->{'box'.$i.'_blurb'} );
								echo '<li>
									<a href="'. $event_info->{'box'.$i.'_url'} .'">
										<div class="mid">
											<h3>'. $bx_array[$qT_lang['lang']].'</h3>
											<p>'. stripslashes( $blurb_array[$qT_lang['lang']] ) .'</p>
										</div>
									</a>
								</li>';
							} else {
								echo '<li>
									<a href="'. $event_info->{'box'.$i.'_url'} .'">
										<div class="mid">
											<h3>'. $event_info->{'box'.$i.'_hdr'} .'</h3>
											<p>'. stripslashes( $event_info->{'box'.$i.'_blurb'} ) .'</p>
										</div>
									</a>
								</li>';
							}

						}

					}
				echo '</ul>
			</section>
		</section>';
	}


	/**
	 * function: display 3 featured articles at bottom of page
	 * replaces the "big red boxes" section
	 */
	function rnr3_get_three_promos() {
		if ( false === ( $three_promos = get_transient( 'three_promos_query_results' ) ) ) {
			$args = array(
				'post_type'			=> 'post',
				'category_name'		=> 'featured',
				'posts_per_page'	=> 3
			);  // will order by date (default)
			$three_promos = new WP_Query( $args );
			set_transient( 'three_promos_query_results', $three_promos, 5 * MINUTE_IN_SECONDS );
		}

		if( $three_promos->have_posts() ) {

			echo '<section id="three-promos">
				<section class="wrapper">
					<div class="grid_3_special">';

						while( $three_promos->have_posts() ) {
							$three_promos->the_post();

							echo '<div class="column_special">
								<figure><a href="'. get_permalink() .'">'. get_the_post_thumbnail( get_the_ID(), 'infobox-thumbs' ) .'</a></figure>
								<h3 class="center"><a href="'. get_permalink() .'">'. get_the_title() .'</a></h3>
								<p>'. get_the_excerpt() .'</p>
								<p class="center"><a class="cta" href="'. get_permalink() .'">Learn More</a></p>
							</div>';
						}

					echo '</div>
				</section>
			</section>';
		}

		wp_reset_postdata();
	}


	/**
	 * function: determine a page's parent's slug
	 */
	function the_parent_slug() {
		global $post;
		if($post->post_parent == 0) return '';
		$post_data = get_post($post->post_parent);
		return $post_data->post_name;
	}


	/**
	 * function: determine a page's parent's title
	 */
	function the_parent_title() {
		global $post;
		if($post->post_parent == 0) return '';
		$post_data = get_post($post->post_parent);
		return $post_data->post_title;
	}


	/**
	 * function: determine language
	 * returns an array with two keys: lang and enabled
	 */
	function rnr3_get_language() {
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		if( is_plugin_active( 'qtranslate-x/qtranslate.php' ) && function_exists( 'qtrans_getLanguage' ) ) {
			$qT_lang['lang'] = qtrans_getLanguage();
			$qT_lang['enabled'] = 1;
		} else {
			$qT_lang['lang'] = 'en';
			$qT_lang['enabled'] = 0;
		}
		return $qT_lang;
	}


	/**
	 * function: find out if this is event home or event subpage
	 */
	function rnr3_home_or_sub( $market, $postid ) {
		if( is_home() or is_front_page() ) {
			$page_location = 'event-home';
		} elseif( is_single() ) {
			$page_location = 'event-post';
		} else {
			$page_location = 'event-sub';
		}
		return $page_location;
	}


	/**
	 * function: call secondary nav
	 * echoes the secondary nav. if it doesn't display, a comment will appear if you view source
	 */
	function rnr3_get_secondary_nav( $parent_slug, $special = '', $footer = 0 ) {
		$qt_lang		= rnr3_get_language();
		$races_array	= 'the-races';
		$weekend_array	= 'the-weekend';

		if( count( get_post_ancestors( get_the_ID() ) ) < 3 ) {

			if( $parent_slug != '' && $footer == 0 ) { ?>

				<!-- secondary navigation -->
				<nav id="secondary">
					<section class="wrapper">

						<?php
							$menu = '';

							if( is_post_type_archive() ) {
								$cpt		= get_post_type();
								$cpt_obj	= get_post_type_object( $cpt );
								$menu		= $cpt_obj->description;
							}

							/* set arrays, in case slug is written in diff language
							 * will check if the parent's slug is in any of these arrays
							 * then sets the $menu var
							 */

							if( $qt_lang['lang'] == 'en' ) {  // english
								if( ( $races_array == $parent_slug ) || is_page( $races_array ) ) {
									$menu = 'The Races';
								} elseif( ( $weekend_array == $parent_slug ) || is_page( $weekend_array ) ) {
									$menu = 'The Weekend';
								}
							} elseif( $qt_lang['lang'] == 'fr' ) {  // french
								if( ( $races_array == $parent_slug ) || is_page( $races_array ) ) {
									$menu = 'Les courses';
								} elseif( ( $weekend_array == $parent_slug ) || is_page( $weekend_array ) ) {
									$menu = 'Le week-end';
								}
							} elseif( $qt_lang['lang'] == 'es' ) {  // spanish
								if( ( $races_array == $parent_slug ) || is_page( $races_array ) ) {
									$menu = 'Las Carreras';
								} elseif( ( $weekend_array == $parent_slug ) || is_page( $weekend_array ) ) {
									$menu = 'El Fin de Semana';
								}

							} elseif( $qt_lang['lang'] == 'pt' ) {  // portuguese
								if( ( $races_array == $parent_slug ) || is_page( $races_array ) ) {
									$menu = 'As corridas';
								} elseif( ( $weekend_array == $parent_slug ) || is_page( $weekend_array ) ) {
									$menu = 'O fim de semana';
								}
							}

							if( $menu != '' ) {
								$nav_settings = array(
									'menu'				=> 'primary-event',
									'fallback_cb'		=> false,
									'theme_location'	=> '__no_such_location',
									'echo'				=> 0,
									'submenu'			=> $menu
								);

								$navmenu = wp_nav_menu( $nav_settings );
								echo '<div class="menu-label">Menu</div>';

								if( $navmenu == '' ) {
									echo '<!-- please create a menu named "'.$menu.'" -->';
								} else {
									echo $navmenu;
								}

							}
						?>
					</section>
				</nav>

			<?php } elseif( $special != '' ) {
				// for special templates (bling, heavy medals, etc.) ?>

				<!-- secondary navigation -->
				<nav id="secondary">
					<section class="wrapper">

						<?php
							$nav_settings = array(
								'menu'            => $special,
								'fallback_cb'     => false,
								'theme_location'  => '__no_such_location',
								'echo'            => 0
							);
							$navmenu = wp_nav_menu( $nav_settings );

							echo '<div class="menu-label">Menu</div>';

							if( $navmenu == '' ) {
								echo '<!-- please create a menu named "'.$special.'" -->';
							} else {
								echo $navmenu;
							}
						?>

					</section>
				</nav>

			<?php } elseif( $footer == 1 ) {
				// a special case since we're passing in the parent slug for the footer
				if( $qt_lang['lang'] == 'en' ) {  // english

					if( $races_array == $parent_slug ) {
						$menu = 'The Races';
					} elseif( $weekend_array == $parent_slug ) {
						$menu = 'The Weekend';
					}
				} elseif( $qt_lang['lang'] == 'fr' ) {  // french
					if( $races_array == $parent_slug ) {
						$menu = 'Les courses';
					} elseif( $weekend_array == $parent_slug ) {
						$menu = 'Le week-end';
					}
				} elseif( $qt_lang['lang'] == 'es' ) {  // spanish
					if( $races_array == $parent_slug ) {
						$menu = 'Las Carreras';
					} elseif( $weekend_array == $parent_slug ) {
						$menu = 'El Fin de Semana';
					}
				} elseif( $qt_lang['lang'] == 'pt' ) {  // portuguese
					if( $races_array == $parent_slug ) {
						$menu = 'As corridas';
					} elseif( $weekend_array == $parent_slug ) {
						$menu = 'O fim de semana';
					}
				}
				$nav_settings = array(
					'menu'				=> 'primary-event',
					'fallback_cb'		=> false,
					'theme_location'	=> '__no_such_location',
					'echo'				=> 0,
					'container'			=> '',
					'submenu'			=> $menu
				);
				$navmenu = wp_nav_menu( $nav_settings );

				if( $navmenu == '' ) {
					echo '<!-- please create a menu named "'. $menu .'" -->';
				} else {
					echo $navmenu;
				}

			}
		}
	}


	/**
	 * function: adding ability to set secondary nav simply by just setting the primary nav
	 */
	function submenu_limit( $items, $args ) {
		if ( empty( $args->submenu ) ) {
			return $items;
		}

		$ids		= wp_filter_object_list( $items, array( 'title' => $args->submenu ), 'and', 'ID' );
		$parent_id	= array_pop( $ids );
		$children	= submenu_get_children_ids( $parent_id, $items );

		foreach ( $items as $key => $item ) {
			if ( ! in_array( $item->ID, $children ) ) {
				unset( $items[$key] );
			}
		}
		return $items;
	}


	function submenu_get_children_ids( $id, $items ) {
		$ids = wp_filter_object_list( $items, array( 'menu_item_parent' => $id ), 'and', 'ID' );

		foreach ( $ids as $id ) {
			$ids = array_merge( $ids, submenu_get_children_ids( $id, $items ) );
		}

		return $ids;
	}
	add_filter( 'wp_nav_menu_objects', 'submenu_limit', 10, 2 );


	/**
	 * function: add additional body classes by filtering the body_class function
	 */
	function add_body_classes( $classes ) {
		$qt_lang = rnr3_get_language();

		// add 'class-name' to the $classes array
		$classes[] = 'lang-'.$qt_lang['lang'];

		// return the $classes array
		return $classes;
	}
	add_filter( 'body_class', 'add_body_classes' );


	/**
	 * function: most recent posts to display on single posts (sidebar)
	 * transient: 'single_sidebar_query_posts-'. $posttype .'-'. $category_slug .'-'. $qt_lang['lang']
	 */
	function get_sidebar_recent_posts( $num_posts, $posttype = 'post', $category = '', $market = '' ) {
		$market = get_market2();

		if( $category != '' ) {
			$category_obj	= get_term_by( 'name', $category, 'category' );
			$category_slug	= $category_obj->slug;
		} else {
			$category_slug = '';
		}

		if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
			$event_info = rnr3_get_event_info( $market );
		}

		$qt_lang = rnr3_get_language();
		include 'languages.php';

		if( false === ( $single_sidebar_query = get_transient( 'single_sidebar_query_posts-'. $posttype .'-'. $category_slug .'-'. $qt_lang['lang'] ) ) ) {
			if( $posttype != 'post' ) {
				$args = array(
					'post_type'			=> $posttype,
					'posts_per_page'	=> $num_posts,
					'post_status'		=> 'publish',
					'has_password'		=> false
				);

			} elseif( $category == '' ) {
				/**
				 * grab posts categorized as promotion or news
				 * unless it's in the rock blog category, then grab ALL
				 */
				if( $market == 'virtual-run' ) {

					$args = array(
						'year'				=> date( 'Y' ),
						'post_type'			=> 'post',
						'posts_per_page'	=> $num_posts,
						'has_password'		=> false,
						'post_status'		=> 'publish'
					);

				} elseif( $market != 'rock-blog' ) {

					$promo_cat	= get_category_by_slug( 'promotion' );
					$news_cat	= get_category_by_slug( 'news' );

					$args = array(
						'post_type'			=> 'post',
						'posts_per_page'	=> $num_posts,
						'category__in'		=> array( $promo_cat->term_id, $news_cat->term_id ),
						'post_status'		=> 'publish',
						'has_password'		=> false
					);

				} else {

					$args = array(
						'post_type'			=> 'post',
						'posts_per_page'	=> $num_posts,
						'has_password'		=> false,
						'post_status'		=> 'publish'
					);

				}
			} elseif( $category != '' ) {
				$cat_obj	= get_category_by_slug( $category );
				$args		= array(
					'post_type'			=> 'post',
					'posts_per_page'	=> $num_posts,
					'category__in'		=> $cat_obj->term_id,
					'has_password'		=> false,
					'post_status'		=> 'publish'
				);

			}

			$single_sidebar_query = new WP_Query( $args );

			set_transient( 'single_sidebar_query_posts-'. $posttype .'-'. $category_slug .'-'. $qt_lang['lang'], $single_sidebar_query, 5 * MINUTE_IN_SECONDS );
		}

		if( $single_sidebar_query->have_posts() ) {
			echo '<ul>';

				while( $single_sidebar_query->have_posts() ) {

					$single_sidebar_query->the_post();

					if( $posttype == 'post' ) {

						$category = get_the_category();

						echo '<li>';

							/**
							 * if qtranslate-x is enabled, show the proper category name
							 */
							if( $qt_lang['enabled'] == 1 ) {
								$categoryname_split = qtrans_split( $category[0]->cat_name );

								echo '<a href="'. get_permalink() .'">'. get_the_title() .'</a><br>
								<span class="sidebar_postedin">'. $postedin_sidebar_txt .' <a href="'. get_category_link( $category[0]->term_id ). '">'. $categoryname_split[$qt_lang['lang']] .'</a></span>';
							} else {

								echo '<a href="'. get_permalink() .'">'. get_the_title() .'</a><br>
								<span class="sidebar_postedin">Posted in <a href="'. get_category_link( $category[0]->term_id ). '">'. $category[0]->cat_name .'</a></span>';
							}

						echo '</li>';

					} elseif( $posttype == 'schedule' ) {

						$dayofweek	= date( 'D', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
						$date		= date( 'n.d.y', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
						$start_time	= date( 'g:i A', strtotime( get_post_meta( get_the_ID(), '_rnr3_soe_start_time', TRUE ) ) );
						$end_time	= date( 'g:i A', strtotime( get_post_meta( get_the_ID(), '_rnr3_soe_end_time', TRUE ) ) );
						$timestamp	= date( 'Y-m-d', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );

						echo '<li>
							<time datetime="'. $timestamp .'T'. date( 'H:i', strtotime( $start_time ) ) .'">
								'. $dayofweek .' / '. $date;

								if( $start_time != '' && $end_time != '' ) {
									echo ' / '. $start_time .' &ndash; '. $end_time;
								}

							echo '</time><br>
							<a href="'. get_permalink() .'">'. get_the_title() .'</a>
						</li>';

					} elseif( $posttype == 'moment' ) {
						echo '<li>
							<a href="'. get_permalink() .'">'. get_the_title() .'</a>';
						echo '</li>';
					}
				}

			echo '</ul>';
		}
		wp_reset_postdata();
	}


	/**
	 * function: most recent posts to display on tag archives (sidebar)
	 */
	function get_sidebar_recent_posts_tag( $tagname ) {

		$args = array(
			'post_type'			=> 'post',
			'posts_per_page'	=> -1,
			'tag'				=> $tagname,
			'post_status'		=> 'publish',
			'has_password'		=> false
		);

		if ( false === ( $tag_sidebar_query = get_transient( 'tag_sidebar_query-'.$tagname ) ) ) {
			$tag_sidebar_query = new WP_Query( $args );
			set_transient( 'tag_sidebar_query-'.$tagname, $tag_sidebar_query, 5 * MINUTE_IN_SECONDS );
		}
		$tag_sidebar_query = new WP_Query( $args );

		if( $tag_sidebar_query->have_posts() ) {

			echo '<ul>';

				while( $tag_sidebar_query->have_posts() ) {

					$tag_sidebar_query->the_post();

					$tag_obj = get_term_by( 'name', $tagname, 'post_tag' );

					echo '<li>
						<a href="'.get_permalink().'">'.get_the_title().'</a><br>
						<span class="sidebar_postedin">Tagged as <a href="'.get_tag_link( $tag_obj->term_id ). '">'.$tagname.'</a></span>
					</li>';
				}

			echo '</ul>';

		}
		wp_reset_postdata();
	}


	/**
	 * function: surveygizmo newsletter form
	 */
	function surveygizmo_form( $market, $lang ) { ?>
		<form id="sgform" action="https://www.surveygizmo.com/s3/2124343/dd15136c5383?rnr_event=<?php echo $market; ?>&rnr_lang=<?php echo $lang; ?>" method="post" enctype="multipart/form-data">
			<input type="hidden" name="sg_navchoice" value="" />
			<input type="hidden" name="sg_currentpageid" value="1" />
			<input type="hidden" name="sg_surveyident" value="2124343" />
			<input type="hidden" name="sg_sessionid" value="" />
			<input type="hidden" name="sg_interactionlevel" value="0" />
			<input type="hidden" name="sg379d8d3cf32685dc1d45a822b34964b2" value="" />

			<input type="email" name="sgE-2124343-1-2" title="" placeholder="Email Address" />
			<button type="submit" name="sGizmoSubmitButton">Keep Me Informed</button>
		</form>
	<?php }


	/**
	 * function: get event date
	 * returns an array with keys: 'monthdate', 'monthdateeuro', 'year'
	 */
	function get_event_date( $qt_lang, $market, $usemarket = 0 ) {

		if($usemarket == 1){
			if ( false === ( $event_info = get_transient( 'event_info_'.$market ) ) ) {
				$event_info = rnr3_get_event_info( $market);
			}
		}else{
			if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
				$event_info = rnr3_get_event_info( $market );
			}
		}

		$month_day_eur = $event_day2 = '';

		if( isset($event_info->festival) && $event_info->festival == 1 ) {

			if( $event_info->festival_start_date == '1970-01-01' || $event_info->festival_start_date == '0000-00-00' ) {

				$event_monthdate	= 'TBD';
				$event_mmmdd		= 'TBD';
				$event_year			= '';
				$event_month		= '';
				$event_day			= '';
				$event_day2			= '';

			} elseif( date( 'M', strtotime( $event_info->festival_start_date ) ) != date( 'M', strtotime( $event_info->festival_end_date ) ) ) {

				$event_monthdate	= date( 'F d', strtotime( $event_info->festival_start_date ) ).'&ndash;'.date( 'F d', strtotime( $event_info->festival_end_date ) );
				$event_mmmdd		= date( 'M d', strtotime( $event_info->festival_start_date ) ).'&ndash;'.date( 'M d', strtotime( $event_info->festival_end_date ) );
				$event_year			= date( 'Y', strtotime( $event_info->festival_end_date ) );
				$event_month		= date( 'm', strtotime( $event_info->festival_start_date ) );
				$event_day			= date( 'd', strtotime( $event_info->festival_start_date ) );
				$event_day2			= date( 'd', strtotime( $event_info->festival_end_date ) );

				$pos_first			= date( 'n', strtotime( $event_info->festival_start_date ) ) -1;
				$pos_second			= date( 'n', strtotime( $event_info->festival_end_date ) ) -1;
				$month_day_eur		= date( 'd', strtotime( $event_info->festival_start_date ) )." ".get_day_or_month_by_language( $qt_lang, $pos_first, 'month' ).'&ndash;'.date( 'd', strtotime( $event_info->festival_end_date ) )." ".get_day_or_month_by_language( $qt_lang, $pos_second, 'month' );

			} else {

				$event_monthdate	= date( 'F d', strtotime( $event_info->festival_start_date ) ).'-'.date( 'd', strtotime( $event_info->festival_end_date ) );
				$event_mmmdd		= date( 'M d', strtotime( $event_info->festival_start_date ) ).'-'.date( 'd', strtotime( $event_info->festival_end_date ) );
				$event_year			= date( 'Y', strtotime( $event_info->festival_end_date ) );
				$event_month		= date( 'm', strtotime( $event_info->festival_start_date ) );
				$event_day			= date( 'd', strtotime( $event_info->festival_start_date ) );
				$event_day2			= date( 'd', strtotime( $event_info->festival_end_date ) );

				$pos				= date( 'n', strtotime( $event_info->festival_start_date ) ) -1;
				$month_day_eur		= date( 'd', strtotime( $event_info->festival_start_date ) ).'&ndash;'. date( 'd', strtotime( $event_info->festival_end_date ) )." ".get_day_or_month_by_language( $qt_lang, $pos, 'month' );
			}

		} else {

			if( isset($event_info->event_date) && $event_info->event_date == '1970-01-01' ) {

				$event_monthdate	= 'TBD';
				$event_mmmdd		= 'TBD';
				$month_day_eur		= 'TBD';
				$event_year			= '';
				$event_month		= '';
				$event_day			= '';
				$event_day2			= '';

			} else {

				if( $qt_lang != 'en' ) {

					//$month_day_eur = date( 'd', strtotime( $event_info->event_date ) )." ".$meses_short[ date( 'n', strtotime( $event_info->event_date ) ) -1 ];
					$pos				= date( 'n', strtotime( $event_info->event_date ) ) -1;
					$month_day_eur		= date( 'd', strtotime( $event_info->event_date ) )." ".get_day_or_month_by_language($qt_lang, $pos, 'month');
					$event_monthdate	= '';
					$event_mmmdd		= '';
					$event_month		= '';
					$event_day			= '';

				} else {

					$event_monthdate	= isset($event_info->event_date) ? date( 'F d', strtotime( $event_info->event_date ) ): '';
					$event_mmmdd		= isset($event_info->event_date) ? date( 'M d', strtotime( $event_info->event_date ) ): '';
					$event_month		= isset($event_info->event_date) ? date( 'm', strtotime( $event_info->event_date ) ): '';
					$event_day			= isset($event_info->event_date) ? date( 'd', strtotime( $event_info->event_date ) ): '';

				}

				$event_year = isset($event_info->event_date) ? date( 'Y', strtotime( $event_info->event_date ) ) : '';

			}
		}

		// united states: mmm dd / yyyy
		$formatted_date[ 'monthdate' ]		= $event_monthdate;
		$formatted_date[ 'mmmdd' ]			= $event_mmmdd;
		$formatted_date[ 'monthdateeuro' ]	= $month_day_eur;
		$formatted_date[ 'month' ]			= $event_month;
		$formatted_date[ 'day' ]			= $event_day;
		$formatted_date[ 'year' ]			= $event_year;
		$formatted_date[ 'event_day2' ]		= $event_day2;

		return $formatted_date;
	}


	/**
	 * function: get individual event date
	 * returns an array with keys: 'monthdate', 'monthdateeuro', 'year'
	 */
	function get_individual_date( $qt_lang, $event ) {

		if( $event->festival == 1 ) {

			if( $event->festival_start_date == '1970-01-01' || $event->festival_start_date == '0000-00-00' ) {
				$event_monthdate	= 'TBD';
				$event_year			= '';
				$month_day_eur		= '';
			} elseif( date( 'M', strtotime( $event->festival_start_date ) ) != date( 'M', strtotime( $event->festival_end_date ) ) ) {
				$event_monthdate	= date( 'M d', strtotime( $event->festival_start_date ) ).'-'.date( 'M d', strtotime( $event->festival_end_date ) );
				$event_year			= date( 'Y', strtotime( $event->festival_end_date ) );
				$month_day_eur		= '';
			} else {
				$event_monthdate	= date( 'F d', strtotime( $event->festival_start_date ) ).'-'.date( 'd', strtotime( $event->festival_end_date ) );
				$event_mmmdd		= date( 'M d', strtotime( $event->festival_start_date ) ).'-'.date( 'd', strtotime( $event->festival_end_date ) );
				$event_year			= date( 'Y', strtotime( $event->festival_end_date ) );
				$month_day_eur		= '';
			}

		} else {

			if( $event->event_date == '1970-01-01' ) {
				$event_monthdate	= 'TBD';
				$month_day_eur		= 'TBD';
				$event_year			= '';
			} else {
				if( $qt_lang != 'en' ) {
					$pos				= date( 'n', strtotime( $event->event_date ) ) -1;
					$event_monthdate	= date( 'd', strtotime( $event->event_date ) )." ".get_day_or_month_by_language($qt_lang, $pos, 'month');
					$month_day_eur		= date( 'd', strtotime( $event->event_date ) )." ".get_day_or_month_by_language($qt_lang, $pos, 'month');
				} else {
					$event_monthdate	= date( 'F d', strtotime( $event->event_date ) );
					$month_day_eur		= '';
				}
				$event_year = date( 'Y', strtotime( $event->event_date ) );
			}

		}

		// united states: mmm dd / yyyy
		$formatted_date[ 'monthdate' ]		= $event_monthdate;
		$formatted_date[ 'monthdateeuro' ]	= $month_day_eur;
		$formatted_date[ 'year' ]			= $event_year;

		return $formatted_date;

	}

	/**
	 * function: get day or month by language
	 */
	function get_day_or_month_by_language( $lang, $position, $type ) {

		switch( $lang ) {
			case 'pt':
				$dias			= array("domingo", "segunda-feira", "ter&ccedil;a-feira", "quarta-feira", "quinta-feira", "sexta-feira", "s&aacute;bado");
				$meses			= array("janeiro", "fevereiro", "mar&ccedil;o", "abril", "maio", "junho", "julho", "agosto", "setembro", "outubro", "novembro", "dezembro");
				$meses_short	= array("jan", "fev", "mar&ccedil;o", "abril", "maio", "junho", "julho", "agosto", "sept", "out", "nov", "dez");
				break;
			case 'es':
				$dias			= array("domingo", "lunes", "martes", "miercoles", "jueves", "viernes", "s&aacute;bado");
				$meses			= array("enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre");
				$meses_short	= array("enero", "feb", "marzo", "abr", "mayo", "jun", "jul", "ago", "sept", "oct", "nov", "dic");
				break;
			case 'fr':
				$dias			= array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi");
				$meses			= array("janvier", "f&#233;vrier", "mars", "avril", "mai", "juin", "juillet", "ao&#251;t", "septembre", "octobre", "novembre", "d&#233;cembre");
				// french don't like abbreviating their months!
				$meses_short	= array("janvier", "f&#233;vrier", "mars", "avril", "mai", "juin", "juillet", "ao&#251;t", "septembre", "octobre", "novembre", "d&#233;cembre");
				break;
			default:
				$dias			= array("sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday");
				$meses			= array("january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december");
				$meses_short	= array("jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sept", "oct", "nov", "dec");
				break;
			}

			switch( $type ) {
				case 'month':
					return $meses_short[$position];
					break;
				case 'month_long':
					return $meses[$position];
					break;
				case 'day':
				default:
					return $dias[$position];
					break;
			}
	}


	/**
	 * function: get the various distances an event has
	 * returns: array with key/value
	 * keys: name, label, circle, active
	 * count is used as a counter for the homepage grid
	 */
	function rnr3_get_distances( $lang, $market ) {
		if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
			$event_info = rnr3_get_event_info( $market );
		}

		$euro_races_array = get_euro_races();

		$onemile_array = $onek_array = $fivek_array = $thirteenk_array = $tenk_array = $elevenk_array = $half_array = $relay_array = $full_array = $bike_array = $kidsrock_array = $twentyk_array = $jrcb_array = $funrun_array = $fourpk_array = $mini_array = $grandparents_array = $kidsruns_array = $virtualrun_array = $happyrun_array = array();

		$count['count']	= 0;
		$distance_array	= explode( ',', $event_info->has_races );

		foreach( $distance_array as $race ) {

			if( $race == 'full' ) {
				$count['count']++;
				$full_array['name']		= 'Marathon';
				$full_array['label']	= 'full';
				$full_array['active']	= 'on';

				if( $lang == 'fr' ) {
					$full_array['circle'] = '<h3 class="mid">42,2 <strong>km</strong></h3>';
				} elseif( $lang == 'es' ) {
					$full_array['circle']	= '<h3 class="mid">42.2</h3>';
					$full_array['name']		= 'Marat&oacute;n';
				} elseif( $lang == 'pt' ) {
					$full_array['circle']	= '<h3 class="mid">42,2</h3>';
					$full_array['name']		= 'Maratona';
				} elseif( $lang == 'zh' ) {
					$full_array['circle']	= '<h3 class="mid">马拉松</h3>';
					$full_array['name']		= '马拉松';
				} else {

					if( in_array( $event_info->event_slug, $euro_races_array ) ) {
						$full_array['circle'] = '<h3 class="mid">42,2</h3>';
					} else {
						$full_array['circle'] = '<h3 class="mid">26.2</h3>';
					}
				}

				if( $market == 'lisbon' ) {
					if( $lang == 'es' ) {
						$full_array['name']   = 'EDP Marat&oacute;n de Lisboa ';
					} elseif( $lang == 'pt' ) {
						$full_array['name']   = 'EDP MARATONA DE LISBOA';
					} else {
						$full_array['name']   = 'EDP Lisbon Marathon';
					}

				}
			} elseif( $race == 'half' ) {
				$count['count']++;
				$half_array['label']	= 'half';
				$half_array['active']	= 'on';

				if( $lang == 'fr' ) {
					if( $event_info->event_slug == 'montreal' )  {
						$half_array['circle']	= '<h3 class="mid">21,1 <strong>km</strong></h3>';
						$half_array['name']		= 'Demi-marathon';
					} else {
						$half_array['circle']	= '<h3 class="mid">21,1 <strong>km</strong></h3>';
						$half_array['name']		= 'Semi Marathon';
					}
				} elseif( $lang == 'es' ) {
					$half_array['circle']	= '<div class="mid group"><h3>21.1</h3><h4>Medio</h4></div>';
					$half_array['name']		= 'Medio Marat&oacute;n';
				} elseif( $lang == 'pt' ) {
					$half_array['circle']	= '<div class="mid group"><h3>21.1</h3><h4>Meia</h4></div>';
					$half_array['name']		= 'Meia Maratona';
				} elseif( $lang == 'zh' ) {
					$half_array['circle']	= '<h3 class="mid">半程马拉松</h3>';
					$half_array['name']		= '半程马拉松';
				} else {
					$half_array['name']   = 'Half Marathon';

					if( in_array( $event_info->event_slug, $euro_races_array ) ) {
						$half_array['circle'] = '<div class="mid group"><h3>21,1</h3><h4>Half</h4></div>';
					} else {
						$half_array['circle'] = '<div class="mid group"><h3>13.1</h3><h4>Half</h4></div>';
					}
				}
				if( $market == 'lisbon' ) {
					if( $lang == 'es' ) {
						$half_array['name']   = 'Medio Marat&oacute;n Santander Totta RTP';
					} elseif( $lang == 'pt' ) {
						$half_array['name']   = 'Meia Maratona Santander Totta RTP';
					} else {
						$half_array['name']   = 'Half Marathon Santander Totta RTP';
					}

				}
			} elseif( $race == 'elevenk' ) {
				$count['count']++;
				$elevenk_array['name']		= '11K';
				$elevenk_array['label']		= 'elevenk';
				$elevenk_array['active']	= 'on';

				if( $lang == 'fr' ) {
					if( $event_info->event_slug == 'montreal' )  {
						$elevenk_array['name']		= '11 km';
						$elevenk_array['circle']	= '<h3 class="mid">11 <strong>km</strong></h3>';
					} else {
						$elevenk_array['name']	= '11K';
						$elevenk_array['circle'] = '<h3 class="mid">11 <strong>K</strong></h3>';
					}
				} else {
					$elevenk_array['circle']	= '<h3 class="mid">11<strong>K</strong></h3>';
					$elevenk_array['name']		= '11K';
				}
			} elseif( $race == 'tenk' ) {
				$count['count']++;
				$tenk_array['name']		= '10K';
				$tenk_array['label']	= 'tenk';
				$tenk_array['active']	= 'on';

				if( $lang == 'fr' ) {
					if( $event_info->event_slug == 'montreal' )  {
						$tenk_array['name']		= '10 km';
						$tenk_array['circle']	= '<h3 class="mid">10 <strong>km</strong></h3>';
					} else {
						$tenk_array['name']		= '10K';
						$tenk_array['circle']	= '<h3 class="mid">10 <strong>K</strong></h3>';
					}
				} else {
					$tenk_array['circle']	= '<h3 class="mid">10<strong>K</strong></h3>';
					$tenk_array['name']		= '10K';
				}
			} elseif( $race == 'thirteenk' ) {
				$count['count']++;
				$thirteenk_array['name']	= '13K';
				$thirteenk_array['label']	= 'thirteenk';
				$thirteenk_array['active']	= 'on';
				$thirteenk_array['circle']	= '<h3 class="mid">13<strong>K</strong></h3>';
			} elseif( $race == 'fivek' ) {
				$count['count']++;
				$fivek_array['label']	= 'fivek';
				$fivek_array['active']	= 'on';

				if( $lang == 'fr' ) {
					$fivek_array['name']	= '5 km';
					$fivek_array['circle']	= '<h3 class="mid">5 <strong>km</strong></h3>';
				} else {
					$fivek_array['name']	= '5K';
					$fivek_array['circle']	= '<h3 class="mid">5<strong>K</strong></h3>';
				}
			} elseif( $race == 'onemile' ) {
				$count['count']++;
				$onemile_array['name']		= '1 Mile';
				$onemile_array['label']		= 'onemile';
				$onemile_array['circle']	= '<h3 class="mid">1<strong>Mile</strong></h3>';
				$onemile_array['active']	= 'on';
			} elseif( $race == 'onek' ) {
				$count['count']++;
				$onek_array['label']	= 'onek';
				$onek_array['active']	= 'on';

				if( $lang == 'fr' ) {
					$onek_array['name']		= '1 km';
					$onek_array['circle']	= '<h3 class="mid">1 <strong>km</strong></h3>';
				} else {
					$onek_array['name']		= '1K';
					$onek_array['circle']	= '<h3 class="mid">1<strong>K</strong></h3>';
				}
			} elseif( $race == 'relay' ) {
				$count['count']++;
				$relay_array['name']	= 'Relay';
				$relay_array['label']	= 'relay';
				$relay_array['circle']	= '<div class="mid group"><h3>13.1</h3><h4>Relay</h4></div>';
				$relay_array['active']	= 'on';
			} elseif( $race == 'kidsrock' ) {
				$count['count']++;
				$kidsrock_array['name']		= 'KiDS ROCK';
				$kidsrock_array['label']	= 'kidsrock';
				$kidsrock_array['circle']	= '<div class="mid group"><h3>KiDS</h3><h4>Rock</h4></div>';
				$kidsrock_array['active']	= 'on';
			} elseif( $race == 'kidsruns' ) {
				// only lisbon
				// $count['count']++;     don't count it since it won't show up in the grid of circles
				$kidsruns_array['name']			= 'EDP Mini Champions';
				$kidsruns_array['label']		= 'kidsruns';
				// $kidsruns_array['circle']	= '<div class="mid group"><h3>Kids</h3><h4>Runs</h4></div>';
				$kidsruns_array['active']		= 'on';

				if( $lang == 'pt' ) {
					$kidsruns_array['name'] = 'EDP Mini Campe&otilde;es';
				} elseif( $lang == 'es' ) {
					$kidsruns_array['name'] = 'EDP Mini Campeones';
				}
			} elseif( $race == 'funrun' ) {
				$count['count']++;
				$funrun_array['label']	= 'funrun';
				$funrun_array['active']	= 'on';

				if( $lang == 'zh') {
					$funrun_array['name']	= '家庭欢乐跑';
					$funrun_array['circle']	= '<h3 class="mid">家庭欢乐跑</h3>';
				} else {
					$funrun_array['name']	= 'Family Fun Run';
					$funrun_array['circle']	= '<h3 class="mid">Fun<strong>Run</strong></h3>';
				}

			} elseif( $race == 'fourpk' ) {
				$count['count']++;
				$fourpk_array['name']	= 'Family Fun';
				$fourpk_array['label']	= 'fourpk';
				$fourpk_array['circle']	= '<h3 class="mid">4<strong>Pack</strong></h3>';
				$fourpk_array['active']	= 'on';
			} elseif( $race == 'grandparents' ) {
				// only lisbon
				// $count['count']++;     don't count it since it won't show up in the grid of circles
				$grandparents_array['name']			= 'MIMOSA Grandparents and Grandchildren Walk';
				$grandparents_array['label']		= 'grandparents';
				// $grandparents_array['circle']	= '<h3 class="mid">Grandparents<strong>Walk</strong></h3>';
				$grandparents_array['active']		= 'on';

				if( $lang == 'pt' ) {
					$grandparents_array['name'] = 'Passeio MIMOSA Av&oacute;s e Netos';
				} elseif( $lang == 'es') {
					$grandparents_array['name'] = 'Paseo MIMOSA Nietos y Abuelos';
				}
			} elseif( $race == 'twentyk' ) {
				// carlsbad 5k
				$count['count']++;
				$twentyk_array['name']		= 'All Day 20K (4 x 5K)';
				$twentyk_array['label']		= 'twentyk';
				$twentyk_array['circle']	= '<div class="mid group"><h3>All Day</h3><h4 style="padding: 0; border: none;">20K</h4></div>';
				$twentyk_array['active']	= 'on';
			} elseif( $race == 'jrcb' ) {
				// carlsbad 5k
				$count['count']++;
				$jrcb_array['name']		= 'Junior Carlsbad 5000';
				$jrcb_array['label']	= 'jrcb';
				$jrcb_array['circle']	= '<h3 class="mid">JR <strong>CB</strong></h3>';
				$jrcb_array['active']	= 'on';
			} elseif( $race == 'mini' ) {
				$count['count']++;
				$mini_array['name']		= 'Mini Marathon';
				$mini_array['label']	= 'mini';
				$mini_array['active']	= 'on';

				if( $lang == 'pt' ) {
					$mini_array['name']		= 'EDP Mini Maratona';
					$mini_array['circle']	= '<div class="mid group"><h3>6,6</h3><h4>Mini</h4></div>';
				} elseif( $lang == 'es' ) {
					$mini_array['name']		= 'Mini Marat&oacute;n';
					$mini_array['circle']	= '<div class="mid group"><h3>6.6</h3><h4>Mini</h4></div>';
				} else {
					if( in_array( $event_info->event_slug, $euro_races_array ) ) {
						$mini_array['circle'] = '<div class="mid group"><h3>6,6</h3><h4>Mini</h4></div>';
					} else {
						$mini_array['circle'] = '<h3 class="mid">Mini</h3>';
					}
				}

				if( $market == 'lisbon' ) {
					if( $lang == 'es' ) {
						$mini_array['name']   = 'EDP Mini Marat&oacute;n';
					} elseif( $lang == 'pt' ) {
						$mini_array['name']   = 'EDP MINI MARATONA';
					} else {
						$mini_array['name']   = 'EDP Mini Marathon';
					}

				}
			} elseif( $race == 'bike' ) {
				$count['count']++;
				$bike_array['name']		= 'Bike Tour';
				$bike_array['label']	= 'bike';
				$bike_array['circle']	= '<div class="mid group"><h3>Bike</h3><h4>Tour</h4></div>';
				$bike_array['active']	= 'on';
			} elseif( $race == 'virtualrun' ) {
				$count['count']++;
				$virtualrun_array['name']	= 'Virtual Run';
				$virtualrun_array['label']	= 'virtualrun';
				$virtualrun_array['circle']	= '<div class="mid group"><h3>Virtual</h3><h4>Run</h4></div>';
				$virtualrun_array['active']	= 'on';

				if( $lang == 'pt' ) {
					$virtualrun_array['name'] = 'Virtual Run';
				} elseif( $lang == 'es' ) {
					$virtualrun_array['name'] = 'Virtual Run';
				}
			} elseif( $race == 'cnhappy' ) {
				$count['count']++;
				if( $lang == 'en' ) {
					$happyrun_array['name'] = 'Happy Run 6K';
				} elseif( $lang == 'zh' ) {
					$happyrun_array['name'] = '欢乐跑';
				}
				$happyrun_array['label']	= 'cnhappy';
				$happyrun_array['circle']	= '<div class="mid group"><h3>Happy</h3><h4>Run</h4></div>';
				$happyrun_array['active']	= 'on';
			}
		}

		$sold_out_array = explode( ',', $event_info->is_sold_out );

		foreach( $sold_out_array as $sold_out ) {

			if( $sold_out == 'onemile' ) {
				$onemile_array['sold_out']  = 1;
			} elseif( $sold_out == 'onek' ) {
				$onek_array['sold_out'] = 1;
			} elseif( $sold_out == 'fivek' ) {
				$fivek_array['sold_out'] = 1;
			} elseif( $sold_out == 'thirteenk' ) {
				$thirteenk_array['sold_out'] = 1;
			} elseif( $sold_out == 'tenk' ) {
				$tenk_array['sold_out'] = 1;
			} elseif( $sold_out == 'elevenk' ) {
				$elevenk_array['sold_out'] = 1;
			} elseif( $sold_out == 'half' ) {
				$half_array['sold_out'] = 1;
			} elseif( $sold_out == 'relay' ) {
				$relay_array['sold_out']  = 1;
			} elseif( $sold_out == 'full' ) {
				$full_array['sold_out'] = 1;
			} elseif( $sold_out == 'bike' ) {
				$bike_array['sold_out'] = 1;
			} elseif( $sold_out == 'kidsrock' ) {
				$kidsrock_array['sold_out'] = 1;
			} elseif( $sold_out == 'twentyk' ) {
				$twentyk_array['sold_out'] = 1;
			} elseif( $sold_out == 'jrcb' ) {
				$jrcb_array['sold_out'] = 1;
			} elseif( $sold_out == 'funrun' ) {
				$funrun_array['sold_out'] = 1;
			} elseif( $sold_out == 'fourpk' ) {
				$fourpk_array['sold_out'] = 1;
			} elseif( $sold_out == 'mini' ) {
				$mini_array['sold_out'] = 1;
			} elseif( $sold_out == 'kidsruns' ) {
				$kidsruns_array['sold_out'] = 1;
			} elseif( $sold_out == 'grandparents' ) {
				$grandparents_array['sold_out'] = 1;
			} elseif( $sold_out == 'virtualrun' ) {
				$virtualrun_array['sold_out'] = 1;
			} elseif( $sold_out == 'cnhappy' ) {
				$happyrun_array['sold_out'] = 1;
			}
		}
		// cache it!
		if ( false === ( $event_array = get_transient( 'event_array_contents-'.$lang ) ) ) {

			$event_array = array(
				$full_array,
				$half_array,
				$elevenk_array,
				$tenk_array,
				$thirteenk_array,
				$fivek_array,
				$onemile_array,
				$onek_array,
				$relay_array,
				$kidsrock_array,
				$kidsruns_array,
				$funrun_array,
				$fourpk_array,
				$grandparents_array,
				$twentyk_array,
				$jrcb_array,
				$mini_array,
				$bike_array,
				$virtualrun_array,
				$happyrun_array,
				$count
			);
			set_transient( 'event_array_contents-'. $lang, $event_array, 5 * MINUTE_IN_SECONDS );
		}

		return $event_array;
	}


	/**
	 * function: countdown timer
	 */
	function rnr3_get_countdown_timer( $market, $lang_info ) {
		if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
			$event_info = rnr3_get_event_info( $market );
		}
		if( $lang_info['lang'] == 'es' ){
			$txt_day	= "D&#237;as";
			$txt_hour	= "Horas";
			$txt_min	= "Min";
			$txt_sec	= "Seg";
		} elseif( $lang_info['lang'] == 'fr' ) {
			$txt_day	= "Jours";
			$txt_hour	= "Heures";
			$txt_min	= "Min";
			$txt_sec	= "Sec";
		} elseif( $lang_info['lang'] == 'pt' ) {
			$txt_day	= "Dias";
			$txt_hour	= "Horas";
			$txt_min	= "Mins";
			$txt_sec	= "Seg";
		} else {
			$txt_day	= "Days";
			$txt_hour	= "Hours";
			$txt_min	= "Min";
			$txt_sec	= "Sec";
		}

		// See if date is in past
		if( !empty($event_info->event_date) && !empty($event_info->event_time) && strtotime( $event_info->event_date .' '. $event_info->event_time) >= strtotime( "now" ) ) {
			$date_and_time = strtotime( $event_info->event_date .' '. $event_info->event_time ); ?>

			<table id="counter" data-countdown="<?php echo $date_and_time; ?>" class="countdown">
				<tr class="stats">
					<td><div class="countdown-days countdown-val">--</div></td>
					<td><div class="countdown-hrs countdown-val">--</div></td>
					<td><div class="countdown-mins countdown-val">--</div></td>
					<td><div class="countdown-secs countdown-val">--</div></td>
				</tr>
				<tr class="labels">
					<td><?php echo $txt_day;?></td><td><?php echo $txt_hour;?></td><td><?php echo $txt_min;?></td><td><?php echo $txt_sec;?></td>
				</tr>
			</table>

		<?php } else {

			$date_and_time = ''; ?>

			<table id="counter" class="countdown">
				<tr class="stats">
					<td><div class="countdown-val">--</div></td>
					<td><div class="countdown-val">--</div></td>
					<td><div class="countdown-val">--</div></td>
					<td><div class="countdown-val">--</div></td>
				</tr>
				<tr class="labels">
					<td><?php echo $txt_day;?></td><td><?php echo $txt_hour;?></td><td><?php echo $txt_min;?></td><td><?php echo $txt_sec;?></td>
				</tr>
			</table>

		<?php }
	}


	/**
	 * function: display sponsor module
	 */
	function rnr3_get_sponsor_module( $market ) {
		$fz_results = is_page_template( 'finisher-zone-search-and_results.php' ) && !empty( $_GET['eventid'] ) ? true : false;

		if( $fz_results ) {
			$event_info = getFZEventInfo( $_GET['eventid'] );
			switch_to_blog( $event_info->blog_id );
		}

		$pri_sponsor_logos	= get_option( 'pri_sponsor_logos' );
		$pri_sponsor_list	= get_option( 'pri_sponsor_list' );
		$pri_sponsor_url	= get_option( 'pri_sponsor_url' );

		$sponsor_logos		= get_option( 'sponsor_logos' );
		$sponsor_list		= get_option( 'sponsor_list' );
		$sponsor_url		= get_option( 'sponsor_url' );

		if( $fz_results ) {
			restore_current_blog();
		}

		$qt_lang = rnr3_get_language();

		if( $qt_lang['lang'] == 'pt' ) {
			$txtSeriesPartners	= '';
			$txtEventPartners	= 'Parceiros Do Evento';
		} elseif( $qt_lang['lang'] == 'es' ) {
			$txtSeriesPartners	= 'Patrocinadores De La Serie';
			$txtEventPartners	= 'Patrocinadores Del Evento';
		} elseif( $qt_lang['lang'] == 'fr' ) {
			$txtSeriesPartners	= 'Partenaires';
			$txtEventPartners	= 'Partenaires';
		} elseif( $qt_lang['lang'] == 'zh' ) {
			$txtSeriesPartners	= '系列赛伙伴';
			$txtEventPartners	= '赛事伙伴';
		} else {
			$txtSeriesPartners	= 'Series Partners';
			$txtEventPartners	= 'Event Partners';
		}

		if( !empty( $pri_sponsor_list ) || ( !empty( $sponsor_list) ) ) { ?>
			<!-- partner logos -->
			<section id="partnerlogos">
				<section class="wrapper">
					<?php
						if( $market == 'series' ) {
							$partner_type = $txtSeriesPartners;
						} elseif( is_page_template( array( 'winter-promo.php', 'finisher-zone.php' ) ) ) {
							$partner_type = $txtSeriesPartners;
						} else {
							$partner_type = $txtEventPartners;
						}
					?>
					<h2><?php echo $partner_type; ?></h2>

					<?php // PRIMARY sponsor logos
						if( !empty( $pri_sponsor_list ) ) {

							$grayscale_logos_array = array( 'montreal' );
							if( in_array( $market, $grayscale_logos_array ) ) {
								$grayscale_class = ' grayscale';
							} else {
								$grayscale_class = '';
							}

							echo '<div id="highlight-carousel" class="owl-carousel'. $grayscale_class .'">';

								foreach($pri_sponsor_list as $key => $value) {

									if( isset( $pri_sponsor_url[$key] ) AND $pri_sponsor_url[$key] != '' ) {
										$sponsor_link_pri = $pri_sponsor_url[$key];
									} else {
										$sponsor_link_pri = '#';
									}
									echo '<a href="'. $sponsor_link_pri .'" target="_blank"><img src="'. $pri_sponsor_logos[$key] .'" alt="'. $value .'"></a>';

								}

							echo '</div>';
						}

						if( !empty( $sponsor_list ) ) { ?>

							<div id="sponsor-carousel" class="owl-carousel">
								<?php // REGULAR sponsor logos (carousel)
									foreach($sponsor_list as $key => $value) {

										if( isset( $sponsor_url[$key] ) AND $sponsor_url[$key] != '' ) {
											$sponsor_link = $sponsor_url[$key];
										} else {
											$sponsor_link = '#';
										}
										echo '<a href="'. $sponsor_link .'" target="_blank"><img src="'. $sponsor_logos[$key] .'" alt="'. $value .'"></a>';
									}
								?>
							</div>
						<?php }
					?>

				</section>
			</section>

		<?php }
	}


	/**
	 * new sponsor retrieval method
	 * controlled by custom post type on blog 1
	 * this will avoid the pain of having to log into each event that requires a logo update
	 * NOT ACTIVE YET
	 */
	function rnr3_get_sponsor_module2( $market ) {
		switch_to_blog( 1 );

			$qt_lang = rnr3_get_language();
			if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
				$event_info = rnr3_get_event_info( $market );
			}
			include 'languages.php';


			/**
			 * primary sponsors
			 */
			$args = array(
				'post_type'			=> 'sponsor',
				'posts_per_page'	=> -1,
				'tax_query'			=> array(
					array(
						'taxonomy'	=> 'marketlists',
						'field'		=> 'slug',
						'terms'		=> $market
					)
				),
				'orderby'		=> 'menu_order name',
				'meta_key'		=> '_rnr3_is_primary',
				'meta_value'	=> 'primary'
			);
			$primary_sponsors_query = new WP_Query( $args );


			/**
			 * secondary sponsors
			 */
			$args = array(
				'post_type'			=> 'sponsor',
				'posts_per_page'	=> -1,
				'tax_query'			=> array(
					array(
						'taxonomy'	=> 'marketlists',
						'field'		=> 'slug',
						'terms'		=> $market
					)
				),
				'orderby'		=> 'menu_order name',
				'meta_key'		=> '_rnr3_is_primary',
				'meta_value'	=> 'secondary',
			);
			$secondary_sponsors_query = new WP_Query( $args ); ?>


			<!-- partner logos -->
			<section id="partnerlogos">
				<section class="wrapper">

					<?php
						if( $market == 'series' ) {
							$partner_type = $txtSeriesPartners;
						} elseif( is_page_template( array( 'winter-promo.php', 'finisher-zone.php' ) ) ) {
							$partner_type = $txtSeriesPartners;
						} else {
							$partner_type = $txtEventPartners;
						}
					?>

					<h2><?php echo $partner_type; ?></h2>

					<?php // PRIMARY sponsor logos
						if( $primary_sponsors_query->have_posts() ) {

							$grayscale_logos_array = array( 'montreal', 'vancouver' );
							if( in_array( $market, $grayscale_logos_array ) ) {
								$grayscale_class = ' grayscale';
							} else {
								$grayscale_class = '';
							}

							echo '<div id="highlight-carousel" class="owl-carousel'. $grayscale_class .'">';

								while( $primary_sponsors_query->have_posts() ) {

									$primary_sponsors_query->the_post();

									if( has_post_thumbnail() ) {
										$pri_sponsor_url = get_post_meta( get_the_ID(), '_rnr3_sponsor_url', 1 ) != '' ? get_post_meta( get_the_ID(), '_rnr3_sponsor_url', 1 ) : '#';

										echo '<div>
											<a href="'. $pri_sponsor_url .'" target="_blank" title="'. get_the_title() .'">'. get_the_post_thumbnail() .'</a>
										</div>';
									}

								}
								wp_reset_postdata();

							echo '</div>';
						}


						// REGULAR sponsor logos (carousel)
						if( $secondary_sponsors_query->have_posts() ) {

							echo '<div id="sponsor-carousel" class="owl-carousel">';

								while( $secondary_sponsors_query->have_posts() ) {
									$secondary_sponsors_query->the_post();

									if( has_post_thumbnail() ) {
										$sec_sponsor_url = get_post_meta( get_the_ID(), '_rnr3_sponsor_url', 1 ) != '' ? get_post_meta( get_the_ID(), '_rnr3_sponsor_url', 1 ) : '#';

										echo '<div>
											<a href="'. $sec_sponsor_url .'" target="_blank" title="'. get_the_title() .'">'. get_the_post_thumbnail() .'</a>
										</div>';
									}

								}
								wp_reset_postdata();

							echo '</div>';

						}
					?>

				</section>
			</section>

		<?php restore_current_blog();
	}


	/**
	 * function: change excerpt length OF CONTENT
	 */
	function custom_excerpt_length( $length ) {
		return 20;
	}
	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


	/**
	 * function: display "find race" feature
	 * locations: home and 404
	 */
	function rnr3_findrace( $all_events, $qt_lang ) {
		global $wpdb; ?>

		<section id="findrace">
			<section class="wrapper">
				<div class="loc"><span class="icon-location"></span></div>
				<h2>Find An Event</h2>

				<?php
					$findrace_intro = ( get_post_meta( get_the_ID(), '_rnr3_findrace_intro', 1 ) != '' ) ? apply_filters( 'the_content', get_post_meta( get_the_ID(), '_rnr3_findrace_intro', 1 ) ) : '';

					if( $findrace_intro != '' ) {
						echo '<div class="narrow findrace__intro">' .

							$findrace_intro .'

						</div>';
					}
				?>

				<div class="controls">
					<label>Sort By</label>

					<div id="sort">
						<select id="sortselect">
							<option value="date:asc">Date</option>
							<option value="city:asc">Name</option>
						</select>
					</div>

					<label>Filter</label>

					<button class="filter" data-filter="all">All</button>
					<button class="filter" data-filter=".fivek">5K</button>
					<button class="filter" data-filter=".tenk">10K</button>
					<button class="filter" data-filter=".half">Half</button>
					<button class="filter" data-filter=".full">Marathon</button>
					<button class="filter" data-filter=".relay">Relay</button>
				</div>

				<?php
					$modal_count = 0;

					foreach( $all_events as $event ) {

						$event_location = explode( ',', $event->event_location );

						if( strpos( $event_location[0], '[:en]' ) !== false ) {
							$event_location_split	= preg_split( '(\\[.*?\\])', $event_location[0] );
							$event_location_display	= $event_location_split[1];
						} else {
							$event_location_display	= $event_location[0];
						}

						$event_modalname	= strtolower( str_replace( '.', '', $event_location_display ) );
						$event_modalname	= strtolower( str_replace( ' ', '-', $event_modalname ) );

						$date = get_individual_date( $qt_lang['lang'], $event );

						if( $date['monthdate'] == 'TBD' ) {
							$data_date = 'TBD';
						} else {
							$data_date = $event->event_date;
						}

						if( $event->has_races != '' ) {
							$event_races = str_replace( ',', ' ',  $event->has_races );
						}

						$resultsurl = $event->event_url.'/the-races/results/';

						echo '
							<div class="mix '.$event_races.'" data-city="'.$event->event_slug.'" data-date="'.$data_date.'" itemscope itemtype="http://schema.org/Event">
								<h5 itemprop="location"><a href="'.$event->event_url.'" itemprop="url">'.$event_location_display.'</a></h5>';

								if( $date['monthdate'] == 'TBD' ) {
									$tile_date	= '<div class="date">TBD</div>';
									$modal_date	= 'TBD';
								} else {
									$schema_date	= $date['monthdate'] .', '. $date['year'];
									$schema_date	= date( 'Y-m-d', strtotime( $schema_date ) );

									//$tile_date = '<div class="date" itemprop="startDate" datetime="'.$schema_date.'">';
									if($event->festival == 1){
										$tile_date = '<div class="date" itemprop="startDate" datetime="'.$event->festival_start_date.'">'.$date['monthdate'].', '.$date['year'].'</div><meta itemprop="endDate" datetime="'.$event->festival_end_date.'">';
									} else {
										$tile_date = '<div class="date" itemprop="startDate" datetime="'.$schema_date.'">'.$date['monthdate'].', '.$date['year'].'</div>';
									}

									//$tile_date .= $date['monthdate'].', '.$date['year'].'</div>';
									$modal_date = $date['monthdate'].', '.$date['year'];
								}
								echo $tile_date;

								if( $event->reg_open == 0 ) {
									$register_or_closed = 'More Info';
									$register_or_closed_modal = 'More Info';
								} else {
									$register_or_closed = 'Register';
									$register_or_closed_modal = 'Register Now';
								}

								if( $event->event_slug == 'virtual-run' ) {

									echo '<p>
										<a href="'. $event->event_url .'" class="register2">'. $register_or_closed .'</a>
										<a class="results" data-fancybox data-src="#modal-'. $event_modalname .'" href="javascript:;" data-rnr-event-slug="'. $event_modalname .'" data-rnr-event-id="'. $event->id .'">Get Details</a>
									</p>';

								} else {

									echo '<p>
										<a href="'.$event->event_url .'register" class="register2">'. $register_or_closed .'</a>
										<a class="results" data-fancybox data-src="#modal-'. $event_modalname .'" href="javascript:;" data-rnr-event-slug="'. $event_modalname .'" data-rnr-event-id="'. $event->id .'">Get Details</a>
									</p>';

								}

								echo '<div style="display:none;" class="findrace_modal" id="modal-'. $event_modalname .'">';

									$modal_gallery	= '';

									$column_count = ( $event->findrace_gallery1 != '' ) ? 2 : 1;

									echo '<div class="grid_'. $column_count .'_modal">
										<div class="column">

											<h2>'. $event_location_display .'</h2>
											<p>'. $modal_date .'</p>';

											if( $event->findrace_desc != '' ) {
												echo apply_filters( 'the_content', $event->findrace_desc );
											}

											if( $event->event_slug == 'virtual-run' ) {
												echo '<p><a target="_blank" href="'. $event->event_url .'" class="cta">'. $register_or_closed_modal .'</a></p>';
											} else {
												echo '<p><a target="_blank" href="'. $event->event_url .'register/" class="cta">'. $register_or_closed_modal .'</a></p>';
											}

										echo '</div>

										<div class="column">';
											if( $column_count == 2 ) {
												$modal_count++;
												echo '<div id="gallery-carousel-'. $modal_count .'" class="owl-carousel"></div>';
											}
										echo '</div>

									</div>
								</div>
							</div>
						';
					}

				?>

				<div class="gap"></div>
				<div class="gap"></div>
				<div class="gap"></div>
				<div class="gap"></div>

			</section>

		</section><!-- /find a race -->


		<!-- <script src="//cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
		<script>
			jQuery(function($){
				var $sortSelect	= $('#sortselect'),
					$container	= $('#findrace');

				$container.mixItUp();

				$sortSelect.on('change', function(){
					$container.mixItUp('sort', this.value);
				});
			});
		</script> -->
		<script src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/js/vendor/mixitup.min.js"></script>
		<script>
			var containerEl = document.querySelector('#findrace');

			var mixer = mixitup(containerEl, {
				load: {
					sort: 'date:asc'
				}
			});

			const sortSelect = document.querySelector( '#sortselect' );

			sortSelect.onchange = function() {
				mixer.sort( this.value );

			};

		</script>

	<?php }


	/**
	 * function: add favicon to head
	 */
	function rnr3_display_favicon() {
		$pathtofavicon = get_template_directory_uri() .'/img/favicon'; ?>
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $pathtofavicon; ?>/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $pathtofavicon; ?>/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $pathtofavicon; ?>/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $pathtofavicon; ?>/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $pathtofavicon; ?>/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $pathtofavicon; ?>/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $pathtofavicon; ?>/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $pathtofavicon; ?>/apple-touch-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $pathtofavicon; ?>/apple-touch-icon-180x180.png">
		<link rel="icon" type="image/png" href="<?php echo $pathtofavicon; ?>/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php echo $pathtofavicon; ?>/favicon-194x194.png" sizes="194x194">
		<link rel="icon" type="image/png" href="<?php echo $pathtofavicon; ?>/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo $pathtofavicon; ?>/android-chrome-192x192.png" sizes="192x192">
		<link rel="icon" type="image/png" href="<?php echo $pathtofavicon; ?>/favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="<?php echo $pathtofavicon; ?>/manifest.json">
		<meta name="msapplication-TileColor" content="#2d89ef">
		<meta name="msapplication-TileImage" content="<?php echo $pathtofavicon; ?>/mstile-144x144.png">
		<meta name="theme-color" content="#ffffff">
	<?php }


	/**
	 * function: get the timezone offset -- used for schedule of events on site home
	 */
	function get_timezone_offset($remote_tz, $origin_tz = null) {
		if($origin_tz === null) {
			if(!is_string($origin_tz = date_default_timezone_get())) {
				return false; // A UTC timestamp was returned -- bail out!
			}
		}
		$origin_dtz	= new DateTimeZone($origin_tz);
		$remote_dtz	= new DateTimeZone($remote_tz);
		$origin_dt	= new DateTime("now", $origin_dtz);
		$remote_dt	= new DateTime("now", $remote_dtz);
		$offset		= $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);

		return $offset;
	}

	/**
	 * function: get id by slug
	 * returns id of a page based off of slug provided
	 * used by "moment" custom post type
	 */
	function get_id_by_slug( $page_slug ) {
		$page = get_page_by_path( $page_slug );
		if ( $page ) {
			return $page->ID;
		} else {
			return null;
		}
	}


	/**
	 * function get partners sidebar
	 */
	function get_partners_sidebar() {
		if ( false === ( $partners_query = get_transient( 'partners_query_results' ) ) ) {
			$args = array(
				'post_status'		=> 'publish',
				'post_type'			=> 'partner',
				'posts_per_page'	=> -1,
				'meta_key'			=> '_rnr3_priority',
				'orderby'			=> 'meta_value_num title',
				'order'				=> 'ASC',
				'has_password'		=> false
			);
			$partners_query = new WP_Query( $args );
			set_transient( 'partners_query_results', $partners_query, 5 * MINUTE_IN_SECONDS );
		}

		if( $partners_query->have_posts() ) {
			echo '<nav>
				<ul>';

					while( $partners_query->have_posts() ) {
						$partners_query->the_post();
						echo '<li>
							<a href="'.get_permalink().'">'.get_the_title().'</a>
						</li>';
					}

				echo '</ul>
			</nav>';

		}
		wp_reset_postdata();

	}


	/**
	* function: social sharing widget
	*/
	function social_sharing( $post_pubkey, $top = 1 ) {
		if( $post_pubkey != '' ) {
			if( $top != 1 ) {
				$share_bottom_class = ' sharewidget_bottom';
			} else {
				$share_bottom_class = '';
			}

			echo '<div class="pw-widget pw-size-medium'. $share_bottom_class .'" pw:twitter-via="runrocknroll">
				<a class="pw-button-facebook pw-counter"></a>
				<a class="pw-button-twitter pw-counter"></a>
				<a class="pw-button-pinterest pw-counter"></a>
				<a class="pw-button-email"></a>
			</div>';


			if( $top != 1 ) {
				echo '<script src="//i.po.st/static/v3/post-widget.js#publisherKey='.$post_pubkey.'&retina=true" type="text/javascript"></script>

				<script>
					var pwidget_config = {
						shareQuote:false,
						i18n: {
							readMore: false
						},
						defaults: {
							retina: true,
							mobileOverlay: true,
							afterShare: false,
							sharePopups: true,
							url: "'. get_permalink() .'",
							title: "'. get_the_title() .'"
						}
					};
				</script>';
			}
		}
	}


	/**
	 * function: liveblog display
	 */
	function get_liveblog_entries() {
		// liveblog - tumblr api integration
		$liveblog_tumblr_url	= rnr_get_option( 'rnrgv_tumblr_base_hostname' );
		$liveblog_tumblr_api	= rnr_get_option( 'rnrgv_tumblr_consumer_key' );
		$liveblog_tag			= get_post_meta( get_the_ID(), '_rnr3_liveblog_tag', 1 ); ?>

		<script>
			var x = 0;
			if( window.location.search.indexOf( 'tumblrpage=' ) > -1 ) {
				var url		= $( location ).attr( 'href' );
				url			= url.split( '=' );
				x			= url[1] * 20 - 20;
				currentpage	= url[1];
			} else {
				currentpage = 1;
			}

			repeatAjax();

			function repeatAjax() {
				$.ajax({
					url: '//api.tumblr.com/v2/blog/<?php echo $liveblog_tumblr_url; ?>.tumblr.com/posts?api_key=<?php echo $liveblog_tumblr_api; ?>',
					dataType: 'jsonp',
					data: { 'tag': '<?php echo $liveblog_tag; ?>', 'offset': x, 'limit': 20 },
					success: function( posts ) {
						var postings	= posts.response.posts;
						var text		= '';

						for( var i in postings ) {
							var p = postings[i];
							text += '<li>';
								var taglength		= p.tags.length;
								var author			= ''
								var authorfound		= 0;
								var title			= '';
								var social_title	= '';
								var social_handle	= '';
								var titlefound		= 0;

								for( var i = 0; i < taglength; i++ ) {
									if( p.tags[i].search( /author/i ) != -1 ) {
										author = p.tags[i].replace( 'author:', '' );
										authorfound = 1;
									} else if( ( p.tags[i].search( /author/i ) == -1 && ( authorfound != 1 ) ) ) {
										author = 'Rock \'n\' Roll Marathon Series';
										authorfound = 0;
									}
									if( p.tags[i].search( /title/i ) != -1 ) {
										title = p.tags[i].replace( 'title:', '' );
										titlefound = 1;
									} else if( ( p.tags[i].search( /title/i ) == -1 && ( titlefound != 1 ) ) ) {
										title = '';
										titlefound = 0;
									}
								}

								if( p.type == 'photo' ) {
									text += '<!-- photo --><h3>' + title + '</h3>';

									if( p.photos.length > 1 ) {
										text += '<div class="liveblog_gallery"><figure>';
											for( var x in p.photos ) {
												text += '<a href="'+ p.photos[x].original_size.url +'" class="liveblog_pix" data-fancybox="gallery-'+ p.reblog_key +'" data-caption="'+ p.caption +'"><img alt="'+ p.caption +'" src="' + p.photos[x].alt_sizes[1].url +'"></a>';
											}
										text += '<figcaption>' + p.caption +'</figcaption></figure></div>';
									} else {
										text += '<figure><a href="'+ p.photos[0].original_size.url +'" class="liveblog_pix" data-fancybox data-caption="'+ p.caption +'"><img src="' + p.photos[0].alt_sizes[1].url +'" alt="'+ p.caption +'"></a><figcaption>' + p.caption +'</figcaption></figure>';
									}

								} else if( p.type == 'text' ) {

									text += '<!-- text --><h3>' + p.title + '</h3>' + p.body;

								} else if( p.type == 'video' ) {

									if( p.video_type == 'youtube' ) {
										text += '<!-- youtube video --><h3>' + title + '</h3><div class="video-container">' + p.player[2].embed_code + '</div>' + p.caption;
									} else {
										text += '<!-- video --><h3>' + title + '</h3><iframe width="700" height="394" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" data-height="394" data-width="700" data-can-resize="" data-can-gutter="" scrolling="no" class="embed_iframe tumblr_video_iframe" style="display: block; background-color: transparent; overflow: hidden; width: 100%; height: 394px;" src="https://www.tumblr.com/video/<?php echo $liveblog_tumblr_url; ?>/' + p.id + '/700/"></iframe>';
									}

								} else if( p.type == 'quote' ) {

									text += '<!-- quote --><blockquote><p>&ldquo;' + p.text + '&rdquo;</p><cite>&mdash; ' + p.source + '</cite></blockquote>';

								} else if( p.type == 'link' ) {

									if( !p.excerpt ) {
										var excerpt = '';
									} else {
										var excerpt = '<p>' + p.excerpt + '</p>';
									}
									if( !p.link_image ) {
										var link_image = '';
									} else {
										var link_image = '<a href="' + p.url + '" target="_blank"><img src="' + p.link_image +'"></a>';
									}
									if( p.publisher == 'instagram.com' ) {
										social_title = p.title.split( ':' );
										social_handle = p.title.split( ' ' );
										text += '<!-- instagram link --><figure>'+ link_image + '<figcaption><p>' + p.excerpt +'<br>&mdash; <a href="'+ p.url +'" target="_blank">'+ social_title[0] +'</a></p></figcaption></figure>';
									} else if( p.publisher == 'twitter.com' ) {
										social_title = p.title.split( ' on ' );
										social_handle = p.url.split( '/' );
										if( p.excerpt != null ) {
											var excerpt = p.excerpt.replace( /(https?:\/\/[^\s]+)/g, "" );
										}
										text += '<!-- twitter link --><blockquote><p>'+ excerpt +'</p><cite>&mdash; <a href="'+ p.url +'" target="_blank">' + p.title + '</a></cite></blockquote>';
									} else {
										if( !p.link_image ) {
											text += '<!-- link --><h3><a href="' + p.url + '" target="_blank">' + p.title + ' &raquo;</a></h3>' + excerpt + p.description;
										} else {
											text += '<!-- link --><figure>' + link_image + '<figcaption><h3><a href="' + p.url + '" target="_blank">' + p.title + ' &raquo;</a></h3>' + excerpt + p.description + '</figcaption></figure>';
										}
									}

								}
								var dts	= new Date( p.timestamp * 1000 );
								var n	= dts.toLocaleTimeString();
								var p	= dts.toLocaleDateString();

								text += '<div class="liveblog_timestamp">Posted by ' + author + ' on ' + p +' @ '+ n +'</div>';
							text += '</li>';

						}
						$( 'ul.liveblog_entries' ).html(text);

						var result = 0;
						result = posts.response.total_posts / 20;

						if( result <= 1 ) {
							$( '.liveblog_nav' ).remove();
						} else {
							$( '.liveblog_nav_pages' ).nextAll().remove();

							var nav = '';
							if( currentpage != 1 ) {
								nav += '<div class="navright"><a href="?tumblrpage='+ ( +currentpage - 1 ) +'">Newer &raquo;</a></div>';
							}
							if( currentpage != Math.ceil( result ) ) {
								nav += '<div class="navleft"><a href="?tumblrpage='+ ( +currentpage + 1 ) +'">&laquo; Older</a></div>';
							}
						}
						$( '.liveblog_nav' ).html( nav );

					},
					complete: function() {
						setTimeout( repeatAjax, 300000 ); // After completion of request, time to redo it after 5 minutes (in milliseconds)
					}

				});
			}

		</script>
		<ul class="liveblog_entries"></ul>
		<div class="liveblog_nav"></div>


	<?php }


	/**
	 * function: get all events dates
	 * usage: $all_events gets used in rnr3_findrace() function
	 */
	function rnr3_get_all_events_dates($qt_lang) {

		$all_event_info		= rnr3_get_all_events();
		$all_events_dates	= array();

		foreach ($all_event_info as $key => $value){
			$all_events_dates[$value->id] = get_individual_date( $qt_lang, $value );
		}
		return $all_events_dates;

	}


	/**
	 * function: get pixlee grid
	 * will display pixlee grid
	 *
	 * UPDATE: don't load images/ads on initial load
	 * waits until user scrolls down to above the section id
	 */
	function get_pixlee_grid( $pixlee_album ) { ?>
		<script type="text/javascript" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/js/advertisement.js"></script>

		<div class="grid"><div class="grid-sizer"></div> </div>

		<script>
			$( document ).ready( function() {
				var $socialfeed = $( '#socialfeed' ),
					loaded = false;

				if( $socialfeed.length > 0 ) {

					$(window).scroll(function() {
						if( loaded == false ) {
							var top_of_element = $socialfeed.offset().top,
								bottom_of_element = (top_of_element + $socialfeed.outerHeight()),
								bottom_of_screen = ($(window).scrollTop() + $(window).height() + 300);

							// Delay animation until the ratings chart comes into focus
							if((bottom_of_screen > top_of_element) && (bottom_of_screen < bottom_of_element)) {
								repeatAjax();
								loaded = true;

							}
						}


					});
				}

				function repeatAjax() {
					$.ajax({
						url: '<?php echo get_bloginfo("stylesheet_directory"); ?>/inc/pixlee-api.php',
						data: { 'albumid': '<?php echo $pixlee_album; ?>'},
						type: 'post',
						dataType: 'json',
						success: function(posts) {
							var postings	= posts.data;
							var photos		= new Array();
							// console.log(postings);
							var text = '';
							if( document.getElementById( "tester" ) == undefined ) {
								// console.log( 'adblock is active' );
								var adblock = true;
							} else {
								// console.log( 'adblock is inactive' );
								var adblock = false;
							}

							for( var i in postings ) {
								if( i == 9 && postings.length < 16 ) {
									break;
								}
								var source	= '';
								var account	= '';
								var p		= postings[i];
								if( p.source == 'instagram' ) {
									source = 'instagram';
								} else if( p.source == 'twitter' ) {
									source = 'twitter';
								} else if( p.source == 'facebook' ) {
									source = 'facebook';
								} else if( p.source == 'desktop' ) {
									source = 'desktop';
								}
								if( i == 0 || i == 5 || i == 7 || i == 10 || i == 12 ) {
									var special_class = ' grid-item--width2';
								} else {
									var special_class = '';
								}
								if( i == 0 ) {
									text += '<div id="div-600_600_1" class="grid-item grid-item-'+ i +' grid-desktop grid-mobile '+ special_class +'">';
										if( adblock == false ) {
											text += '<script>googletag.display("div-600_600_1");</scr'+'ipt>';
										} else {
											text += '<a class="'+ source +'" href="' + p.platform_link +'" target="_blank"><img style="background-image:url(' + p.pixlee_cdn_photos.medium_url +');" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/blank.gif"><span class="grid_social_icon icon-'+ source +'"></span></a>';
										}
									text += '</div>';

								} else if( i == 3 ) {
									text += '<div id="div-600_600_2" class="grid-item grid-item-'+ i + special_class +'">';
										if( adblock == false ) {
											text += '<script>googletag.display("div-600_600_2");</scr'+'ipt>';
										} else {
											text += '<a class="'+ source +'" href="' + p.platform_link +'" target="_blank"><img style="background-image:url(' + p.pixlee_cdn_photos.medium_url +');" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/blank.gif"><span class="grid_social_icon icon-'+ source +'"></span></a>';
										}
									text += '</div>';

								} else if( i == 7 ) {
									text += '<div class="grid-item grid-mobile grid-item-'+ i + special_class +'">';
										text += '<a class="'+ source +'" href="' + p.platform_link +'" target="_blank"><img class="grid-img" style="background-image:url(' + p.pixlee_cdn_photos.medium_url +');" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/blank.gif"><span class="grid_social_icon icon-'+ source +'"></span></a>';
									text += '</div>';

								} else if( i == 10 ) {
									text += '<div id="div-600_600_3" class="grid-item grid-desktop grid-item-'+ i + special_class +'">';
										if( adblock == false ) {
											text += '<script>googletag.display("div-600_600_3");</scr'+'ipt>';
										} else {
											text += '<a class="'+ source +'" href="' + p.platform_link +'" target="_blank"><img style="background-image:url(' + p.pixlee_cdn_photos.medium_url +');" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/blank.gif"><span class="grid_social_icon icon-'+ source +'"></span></a>';
										}
									text += '</div>';

								} else if( i == 12 ) {
									text += '<div id="div-600_600_4" class="grid-item grid-mobile grid-item-'+ i + special_class +'">';
										if( adblock == false ) {
											text += '<script>googletag.display("div-600_600_4");</scr'+'ipt>';
										} else {
											text += '<a class="'+ source +'" href="' + p.platform_link +'" target="_blank"><img style="background-image:url(' + p.pixlee_cdn_photos.medium_url +');" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/blank.gif"><span class="grid_social_icon icon-'+ source +'"></span></a>';
										}
									text += '</div>';

								} else {
									if( p.source == 'desktop' ) {
										var desktop_link = decodeURIComponent( p.action_link );
										text += '<div class="grid-item grid-item-'+ i + special_class +'">';
											text += '<a class="'+ source +'" href="' + desktop_link +'" target="_blank"><img class="grid-img" style="background-image:url(' + p.pixlee_cdn_photos.medium_url +');" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/blank.gif"><span class="grid_social_icon icon-'+ source +'"></span></a>';
										text += '</div>';
									} else {
										text += '<div class="grid-item grid-item-'+ i + special_class +'">';
											text += '<a class="'+ source +'" href="' + p.platform_link +'" target="_blank"><img class="grid-img" style="background-image:url(' + p.pixlee_cdn_photos.medium_url +');" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/blank.gif"><span class="grid_social_icon icon-'+ source +'"></span></a>';
										text += '</div>';
									}
								}

							}
							// console.log( i + ' items' );
							$( '.grid-sizer' ).after( text );
						},
						complete: function() {
							// setTimeout( repeatAjax, 300000 ); // After completion of request, time to redo it after 5 minutes (in milliseconds)
							setTimeout(function() {
								var container	= document.querySelector('.grid');
								var msnry		= new Masonry( container, {
									itemSelector: '.grid-item',
									columnWidth: '.grid-sizer',
									percentPosition:true
								} );
								msnry.reloadItems();
								msnry.layout();
							}, 1000);

						}

					});
				}
			});
		</script>

	<?php }


	/**
	 * function: get promo articles
	 * usage: finisher zone
	 * max 2 (in case there are 2 events at the same time
	 * look for posts categorized as promotion and tagged as featured
	 * store those in an array to be echo'd later on
	 */
	function get_finisher_zone_promo( $qt_cur_lang ) {
		$args = array(
			'category_name'		=> 'promotion',
			'tag'				=> 'featured',
			'posts_per_page'	=> '2'
		);
		if ( false === ( $featured_promo_query = get_transient( 'featured_promo_query' ) ) ) {
			$featured_promo_query = new WP_Query( $args );
			set_transient( 'featured_promo_query', $featured_promo_query, 5 * MINUTE_IN_SECONDS );
		}

		if( $qt_cur_lang == 'es' ){
			$learn_more_txt = 'Lea M&aacute;s';
		} else {
			$learn_more_txt = 'Learn More';
		}

		$i = 0;

		if( $featured_promo_query->have_posts() ) {
			while( $featured_promo_query->have_posts() ) {
				$featured_promo_query->the_post();

				$featured_promo_array[$i] = '<div class="column_special">
					<figure><a href="'. get_permalink() .'">'. get_the_post_thumbnail( get_the_ID(), 'infobox-thumbs' ) .'</a></figure>
					<h3 class="center"><a href="'. get_permalink() .'">'. get_the_title() .'</a></h3>
					<p>'. get_the_excerpt() .'</p>
					<p class="center"><a class="cta" href="'. get_permalink() .'">'. $learn_more_txt .'</a></p>
				</div>';

				$i++;
			}
		}
		wp_reset_postdata();

		return $featured_promo_array;
	}


	/**
	 * function: get share module (moments - event)
	 */
	function get_share_module( $post_id, $youtube_id, $twitter_hashtag ) {
		return '<div class="share_module">
			<span class="share_via">Share Via</span>
			<div id="ribbon" class="moments-share">
				<div class="facebook">
					<a href="https://www.facebook.com/sharer/sharer.php?u='. urlencode( 'https://www.youtube.com/watch?v='. $youtube_id ) .'" onclick="window.open(this.href, \'fbopen\', \'width=700, height=660, toolbar=0, menubar=0, location=1, status=1, scrollbars=1, resizable=1, left=0, top=0\' ); return false;"><span class="icon-facebook"></span></a>
				</div>
				<div class="twitter">
					<a href="https://twitter.com/intent/tweet?text='. urlencode( get_the_title( $post_id ) ) .'&via=RunRocknRoll&hashtags='. ltrim( $twitter_hashtag, '#' ).'&wrap_links=true&url='. urlencode( 'https://www.youtube.com/watch?v='. $youtube_id ) .'" target="_blank" class="twitter-share" data-item-shared="video"><span class="icon-twitter"></span></a>
				</div>
				<div class="pinterest">
					<a data-pin-do="buttonPin" data-pin-round="true" data-pin-tall="true" href="https://www.pinterest.com/pin/create/button/?url='. urlencode( 'https://www.youtube.com/watch?v='. $youtube_id ) .'&media='. urlencode( 'http://img.youtube.com/vi/'. $youtube_id .'/0.jpg' ).'&description='. urlencode( get_the_title( $post_id ) ) .'" onclick="window.open(this.href, \'fbopen\', \'width=700, height=660, toolbar=0, menubar=0, location=1, status=1, scrollbars=1, resizable=1, left=0, top=0\' ); return false;"><span class="icon-pinterest"></span></a>
				</div>
			</div>
		</div>';
	}


	/**
	 * function: get global overlay
	 */
	function get_global_overlay( $qt_lang = array() ){
		$text_enter_site	= 'Enter Full Site';
		$blog_name			= 'main';

		// check if qtranslate is set and if we are changing languages
		$qt_on = isset( $qt_lang['lang'] ) && $qt_lang['enabled'] == 1 ? true : false;

		if( $qt_on ) {
			if( $qt_lang['lang'] == 'es' ) {
				$text_enter_site = 'Acceder La P&aacute;gina Completa';
			} elseif( $qt_lang['lang'] == 'fr' ) {
				$text_enter_site = 'Entrer Complet Site';
			} elseif( $qt_lang['lang'] == 'pt' ) {
				$text_enter_site = 'Acesse Pagina Completa';
			}
		}

		// interstitial driver
		$driver			= get_post_meta( get_the_ID(), '_rnr3_driver_enabler', true );
		$home_driver	= '';
		$excluded_blog	= false;
		// $cta_text	= $text_enter_site;
		// $cta_link	= '#';
		$current_blog	= get_current_blog_id();
		$cookie_on		= get_post_meta( get_the_ID(), '_rnr3_driver_cookie_enabler', true );
		$cookie_name	= get_post_meta( get_the_ID(), '_rnr3_driver_cookie_name', true ) ?: 'rnr3_driver';

		// check if we are using cookies. If yes, check for it
		if( $cookie_on != '' && isset($_COOKIE[$cookie_name]) ) {
			return;
		}

		if( $driver != '' ) {
			$driver_logo	= get_post_meta( get_the_ID(), '_rnr3_driver_logo', true );
			$driver_copy	= get_post_meta( get_the_ID(), '_rnr3_driver_copy', true );
			$bckgrnd_color	= get_post_meta( get_the_ID(), '_rnr3_driver_color', true );
			$other_color	= get_post_meta( get_the_ID(), '_rnr3_driver_color-other', true );
			$link_buttons	= get_post_meta( get_the_ID(), '_rnr3_driver_link_buttons', true );
			$cta_text		= get_post_meta( get_the_ID(), '_rnr3_driver_button_text', true ) ?: $text_enter_site;
			$cta_link		= get_post_meta( get_the_ID(), '_rnr3_driver_button_link', true ) ?: '#';
			$grid_columns	= count($link_buttons) > 2 ? 4 : 2;
			$blog_name		= str_replace( '/', '', get_blog_details( get_current_blog_ID() )->path );
		}

		if( !is_main_site() ) {
			// if not on main blog, check main blog for interstitial
			switch_to_blog(1);
			$home_id			= get_page_by_title( 'Home' )->ID;
			$home_driver		= get_post_meta( $home_id, '_rnr3_driver_enabler', true );
			$excluded_blog_ids	= get_post_meta( $home_id, '_rnr3_driver_excluded_blogs', true );
			$series_home_only	= get_post_meta( $home_id, '_rnr3_driver_series_home_only', true );

			if( $home_driver ){
				$excluded_blog	= in_array( $current_blog, explode(',', $excluded_blog_ids) );
				$excluded_blog	= $series_home_only != '' ? true : $excluded_blog;
				// only override values if it is NOT an excluded blog

				if( !$excluded_blog ) {
					// override all values if values not empty
					$driver			= $home_driver;
					$driver_logo	= get_post_meta( $home_id, '_rnr3_driver_logo', true );
					$driver_copy	= get_post_meta( $home_id, '_rnr3_driver_copy', true );
					$bckgrnd_color	= get_post_meta( $home_id, '_rnr3_driver_color', true );
					$other_color	= get_post_meta( $home_id, '_rnr3_driver_color-other', true );
					$link_buttons	= get_post_meta( $home_id, '_rnr3_driver_link_buttons', true );
					$cta_text		= get_post_meta( $home_id, '_rnr3_driver_button_text', true ) ?: $text_enter_site;
					$cta_link		= get_post_meta( $home_id, '_rnr3_driver_button_link', true ) ?: '#';
					$grid_columns	= count($link_buttons) > 2 ? 4 : 2;
				}
			}
			restore_current_blog();
		}

		// remove paragraph formatting in filters for driver copy, then add again below (filters needed from qtranslate)
		remove_filter( 'the_content', 'wpautop' );
		$driver_copy = isset($driver_copy) ? apply_filters( 'the_content', $driver_copy ) : '';
		add_filter( 'the_content', 'wpautop' );

		if( $driver != '' || ( $home_driver != '' && !$excluded_blog ) || ( $home_driver == '' && $excluded_blog ) ){ ?>
			<a href="#driver" id="fancybox-driver"></a>

			<?php if( !in_array( strtolower($other_color), array('#fff', '#ffffff', '') ) ) : ?>

				<style>
					.fancybox-driver-wrap .fancybox-close:before,
					.fancybox-driver-wrap .fancybox-close:after{
						background-color: <?php echo $other_color; ?>;
					}
					#driver h2,
					#driver .enter-site{
						color: <?php echo $other_color; ?>;
					}
					#driver h2,
					.driver-link,
					#driver .enter-site{
						border-color: <?php echo $other_color; ?>;
					}
				</style>

			<?php endif; ?>

			<section id="driver" style="background-color: transparent; overflow: visible;">
				<script>
					var rnr3_driver_bg_color	= '<?php echo $bckgrnd_color != "" ? $bckgrnd_color : "#EE1C24"; ?>';
					var rnr3_cookie_name		= '<?php echo $cookie_on != "" ? $cookie_name : ""; ?>';
				</script>

				<?php if( $driver_logo != '' ) : ?>
					<img id="driver-logo" src="<?php echo $driver_logo; ?>" alt="Logo">
				<?php endif; ?>

				<?php if( $driver_copy != '' ) : ?>
					<h2><?php echo $driver_copy; ?></h2>
				<?php endif; ?>

				<?php if( !empty($link_buttons) ) : ?>

					<div class="wrapper">
						<div class="grid_<?php echo $grid_columns; ?>_special">
							<?php
								foreach($link_buttons as $k => $button):
									if($k >= 4) break; //limit to 4 until cmb2 expansion plugin added/worked out

									//check for images and their proper language equivalent
									$button_image			= $button['image'];
									$button_image			= image_translation( $qt_lang, $button_image );
									$button_mobile_image	= isset($button['image-mobile']) ? image_translation( $qt_lang, $button['image-mobile'] ) : '';
									$button_mobile_class	= !empty($button['image-mobile']) ? ' mobile-image-button' : '';
									$button_position		= $blog_name. '_position' . ($k+1);
									$button_link			= $button['link']; ?>

									<div class="column_special">
										<a class="driver-link<?php echo $button_mobile_class; ?>" data-position="<?php echo $button_position; ?>" href="<?php echo $button_link; ?>" style="background-image: url('<?php echo $button_image; ?>');";>
											<picture>
												<?php if( $button_mobile_image != '' ): ?>
													<source media="(max-width: 419px)" srcset="<?php echo $button_mobile_image; ?>">
													<source media="(min-width: 420px)" srcset="<?php echo $button_image; ?>">
												<?php endif; ?>
												<img src="<?php echo $button_image; ?>" alt="RnR Driver">
											</picture>
										</a>
									</div>
								<?php endforeach;
							?>

						</div>
					</div>

				<?php endif; ?>

				<a href="<?php echo $cta_link; ?>" class="enter-site"><?php echo $cta_text; ?></a>

			</section>
		<?php }

	}


	/**
	 * function: get non-US north american races
	 * returns array of north american races that aren't located in the usa
	 */
	function get_non_us_na_races() {
		$northamerican_array = array( 'mexico-city', 'merida', 'montreal', 'queretaro', 'vancouver' );
		return $northamerican_array;
	}


	/**
	 * function: get european races
	 * returns array of euro races, mainly to get english
	 */
	function get_euro_races() {
		$euro_array = array( 'lisbon', 'madrid', 'dublin' );
		return $euro_array;
	}


	/**
	 * function: ajax call for Expo Registration
	 */
	add_action( 'wp_ajax_nopriv_expo_post_data', 'expo_post_data_callback' );
	add_action( 'wp_ajax_expo_post_data', 'expo_post_data_callback' );
	function expo_post_data_callback(){
		global $post;
		ob_start();

		if ( !empty( $_POST['post_id'] ) ) {
			$postID			= $_POST['post_id'];
			$frontpage_id	= get_option('page_on_front');
			$post			= get_post( $postID );
			$race_info_copy	= get_post_meta( $postID, '_drop_down_race_info_copy', true ) ?: false;
			$perks_copy		= get_post_meta( $postID, '_drop_down_perks_copy', true );
			$stjude_option	= get_post_meta( $postID, '_additional_details_sj_option', true ) ?: false;
			$reg_link		= get_post_meta( $postID, '_reg_details_reg_link', true ) ?: false;
			$reg_reminder	= get_post_meta( $postID, '_reg_details_reg_reminder', true ) ?: false;
			$gallery		= get_post_meta( $postID, '_gallery_images', true ) ?: false;
			$subscription	= get_post_meta( $postID, '_gallery_form', true ) ?: false;
			$blog_id		= get_post_meta( $postID, '_additional_details_blog_id', true ) ?: false;
			$stjude_heading	= get_post_meta( $frontpage_id, '_stjude_heading', true );
			$stjude_copy	= get_post_meta( $frontpage_id, '_stjude_copy', true );

			// set up the date
			$tbd		= get_post_meta( $postID, '_additional_details_tbd', true ) ?: NULL;
			$festival	= get_post_meta( $postID, '_additional_details_festival', true ) ?: NULL;

			if ( $tbd != '' ) {

				$date_displayed = $date = 'TBD';

			} elseif ( $festival != '' ) {

				$fest_start_date	= get_post_meta( $postID, '_additional_details_start_date', true );
				$fest_end_date		= get_post_meta( $postID, '_additional_details_end_date', true );

				$date = date( 'F d', strtotime( $fest_start_date) ) . ' - ' . date( 'F d, Y', strtotime( $fest_end_date) );

			} else {

				$date = get_the_date('F d, Y', $postID);

			}


			//get event pricing and save into array
			//FIRST ELEMENT LEFT EMPTY FOR FULL MARATHON!!
			$races            = array(
				''			=> 'Marathon',
				'_half'		=> 'Half Marathon',
				'_eleven_k'	=> '11K',
				'_ten_k'	=> '10K',
				'_five_k'	=> '5K',
				'_one_mile'	=> 'One mile',
				'_relay'	=> 'Relay',
				'_custom'	=> 'Custom',
				'_discount'	=> 'Discount',
			);
			$race_prices      = array();

			foreach ( $races as $k => $race ) {
				$race_prices[$race] = get_post_meta( $postID, '_reg_details_pricing' . $k, true ) ?: false;
			} ?>

			<div class="drawer__header">
				<h2><?php echo get_the_title($postID); ?></h2>
				<div class="date"><?php echo $date; ?></div>
			</div>

			<div id="info-tabs" class="tp-tabs">
				<ul>
					<li><a href="#race-info">Race Info</a></li>

					<?php if ( $perks_copy != '' ) : ?>
						<li><a href="#participants-perks">Participants Perks</a></li>
					<?php endif;

					if ( isset( $stjude_option ) && $stjude_option == 'on' ) : ?>
						<li><a href="#stjude-perks"><?php echo $stjude_heading; ?></a></li>
					<?php endif; ?>

				</ul>

				<div id="race-info">
					<ul class="race-prices">
						<?php
							foreach ( $race_prices as $name => $price ) {
								if ( $price != '' && $name != 'Custom' ) {
									echo "<li><strong>$name - </strong> $price</li>";
								} elseif ( $price != '' && $name == 'Custom' ) {
									echo "<li>$price</li>";
								}
							}
						?>
					</ul>

					<?php echo $race_info_copy; ?>

				</div>

				<?php if ( $perks_copy != '' ) : ?>
					<div id="participants-perks">
						<?php echo apply_filters( 'the_content', $perks_copy ); ?>
					</div>
				<?php endif;

				if ( $stjude_option && $stjude_copy != '' ) :

					$stjude_copy	= apply_filters( 'the_content', $stjude_copy );
					$reg_link		= get_blog_details( $blog_id )->siteurl . '/register/'; ?>

					<div id="stjude-perks">
						<?php echo $stjude_copy;

						if ( $reg_link ) : ?>
							<a href="<?php echo $reg_link; ?>#stjude" class="cta">Learn More</a>
						<?php endif;?>

					</div>

				<?php endif;

				if ( $reg_link ): ?>

					<a href="<?php echo $reg_link; ?>" class="cta cta_big register">Register</a>

				<?php elseif ( !$reg_link && $reg_reminder): ?>

					<a href="javascript:;" class="cta cta_big register fancybox" data-fancybox data-src="<?php echo $reg_reminder; ?>" data-options="{'iframe':{'preload':'true'}}">Get Notified</a>

				<?php endif; ?>

				<!-- countdown timer -->
				<?php exporeg_get_countdown_timer( $post->post_date ); ?>
			</div>

			<div class="drawer-panel-2">

				<?php if ( $gallery != '' ) : ?>

					<div class="gallery">
						<?php foreach( $gallery as $id => $image_url ){
							echo wp_get_attachment_image( $id, 'newsletter-post' ); ?>
							<!-- <img src="<?php echo $image_url; ?>" alt="<?php echo get_the_title($postID); ?> Image"> -->
						<?php } ?>
					</div><!-- END .GALLERY -->

				<?php endif;

				if ( $subscription != '' ) : ?>

					<!-- SUBSCRIPTION FORM -->
					<div class="subscription-form">
						<?php do_shortcode( $subscription ); ?>
					</div>

				<?php endif; ?>
			</div>

		<?php }

		$output = ob_get_clean();

		echo $output;

		die();
	}


	/**
	 * countdown timer
	 */
	function exporeg_get_countdown_timer( $time = '', $lang_info = array('lang' => '') ) {

		if( $lang_info['lang'] == 'es' ){
			$txt_day	= "D&#237;as";
			$txt_hour	= "Horas";
			$txt_min	= "Min";
			$txt_sec	= "Seg";
		} elseif( $lang_info['lang'] == 'fr' ) {
			$txt_day	= "Jours";
			$txt_hour	= "Heures";
			$txt_min	= "Min";
			$txt_sec	= "Sec";
		} elseif( $lang_info['lang'] == 'pt' ) {
			$txt_day	= "Dias";
			$txt_hour	= "Horas";
			$txt_min	= "Mins";
			$txt_sec	= "Seg";
		} else {
			$txt_day	= "Days";
			$txt_hour	= "Hours";
			$txt_min	= "Min";
			$txt_sec	= "Sec";
		}

		//See if date is in past
		if( strtotime( $time ) >= strtotime( "now" ) ) {

			$date_and_time = strtotime( $time ); ?>

			<table id="counter" data-countdown="<?php echo $date_and_time; ?>" class="countdown">
				<tr class="stats">
					<td><div class="countdown-days countdown-val">--</div></td>
					<td><div class="countdown-hrs countdown-val">--</div></td>
					<td><div class="countdown-mins countdown-val">--</div></td>
					<td><div class="countdown-secs countdown-val">--</div></td>
				</tr>
				<tr class="labels">
					<td><?php echo $txt_day;?></td><td><?php echo $txt_hour;?></td><td><?php echo $txt_min;?></td><td><?php echo $txt_sec;?></td>
				</tr>
			</table>

		<?php } else {

			$date_and_time = ''; ?>

			<table id="counter" class="countdown">
				<tr class="stats">
					<td><div class="countdown-val">--</div></td>
					<td><div class="countdown-val">--</div></td>
					<td><div class="countdown-val">--</div></td>
					<td><div class="countdown-val">--</div></td>
				</tr>
				<tr class="labels">
					<td><?php echo $txt_day;?></td><td><?php echo $txt_hour;?></td><td><?php echo $txt_min;?></td><td><?php echo $txt_sec;?></td>
				</tr>
			</table>

		<?php }
	}


	/**
	 * function: get race display name (regional landing pages)
	 * returns the display name of a race from the event manager selections
	 * stripped down version of rnr3_get_distances()
	 */
	function get_race_display_name_regional( $race ) {
		switch( $race ) {
			case 'onemile':
				return '1 Mile';
				break;
			case 'onek':
				return '1K';
				break;
			case 'fivek':
				return '5K';
				break;
			case 'thirteenk':
				return '13K';
				break;
			case 'tenk':
				return '10K';
				break;
			case 'elevenk':
				return '11K';
				break;
			case 'half':
				return 'Half Marathon';
				break;
			case 'full':
				return 'Marathon';
				break;
			case 'relay':
				return 'Relay';
				break;
			case 'bike':
				return 'Bike Tour';
				break;
			case 'kidsrock':
				return 'Kids Rock';
				break;
			case 'twentyk':
				return '20K';
				break;
			case 'jrcb':
				return 'Jr. Carlsbad';
				break;
			case 'funrun':
				return 'Fun Run';
				break;
			case 'fourpk':
				return 'Family Fun';
				break;
			case 'mini':
				return 'Mini Marathon';
				break;
			case 'kidsruns':
				return 'EDP Mini Champions';
				break;
			case 'grandparents':
				return 'MIMOSA Grandparents and Grandchildren Walk';
				break;
			case 'virtualrun':
				return 'Virtual Run';
				break;
		}
	}


	/**
	 * function: get event date (regional landing pages)
	 * returns an array with keys: 'monthdate', 'monthdateeuro', 'year'
	 * stripped down version of get_event_date()
	 */
	function get_event_date_regional( $qt_lang, $market, $event_info ) {
		$month_day_eur = $event_day2 = '';

		if( $event_info->festival == 1 ) {
			if( $event_info->festival_start_date == '1970-01-01' || $event_info->festival_start_date == '0000-00-00' ) {
				$event_monthdate	= 'TBD';
				$event_mmmdd		= 'TBD';
				$event_year			= '';
				$event_month		= '';
				$event_day			= '';
				$event_day2			= '';
			} elseif( date( 'M', strtotime( $event_info->festival_start_date ) ) != date( 'M', strtotime( $event_info->festival_end_date ) ) ) {
				$event_monthdate	= date( 'M d', strtotime( $event_info->festival_start_date ) ).'-'.date( 'M d', strtotime( $event_info->festival_end_date ) );
				$event_mmmdd		= date( 'F d', strtotime( $event_info->festival_start_date ) ).'-'.date( 'M d', strtotime( $event_info->festival_end_date ) );
				$event_year			= date( 'Y', strtotime( $event_info->festival_end_date ) );
				$event_month		= date( 'm', strtotime( $event_info->festival_start_date ) );
				$event_day			= date( 'd', strtotime( $event_info->festival_start_date ) );
				$event_day2			= date( 'd', strtotime( $event_info->festival_end_date ) );
			} else {
				$event_monthdate	= date( 'F d', strtotime( $event_info->festival_start_date ) ).'-'.date( 'd', strtotime( $event_info->festival_end_date ) );
				$event_mmmdd		= date( 'M d', strtotime( $event_info->festival_start_date ) ).'-'.date( 'd', strtotime( $event_info->festival_end_date ) );
				$event_year			= date( 'Y', strtotime( $event_info->festival_end_date ) );
				$event_month		= date( 'm', strtotime( $event_info->festival_start_date ) );
				$event_day			= date( 'd', strtotime( $event_info->festival_start_date ) );
				$event_day2			= date( 'd', strtotime( $event_info->festival_end_date ) );
			}
		} else {
			if( $event_info->event_date == '1970-01-01' ) {
				$event_monthdate	= 'TBD';
				$event_mmmdd		= 'TBD';
				$event_year			= '';
				$event_month		= '';
				$event_day			= '';
				$event_day2			= '';
			} else {
				if( $qt_lang != 'en' ) {
					$pos				= date( 'n', strtotime( $event_info->event_date ) ) -1;
					$month_day_eur		= date( 'd', strtotime( $event_info->event_date ) )." ".get_day_or_month_by_language($qt_lang, $pos, 'month');
					$event_monthdate	= '';
					$event_mmmdd		= '';
					$event_month		= '';
					$event_day			= '';
				} else {
					$event_monthdate	= date( 'F d', strtotime( $event_info->event_date ) );
					$event_mmmdd		= date( 'M d', strtotime( $event_info->event_date ) );
					$event_month		= date( 'm', strtotime( $event_info->event_date ) );
					$event_day			= date( 'd', strtotime( $event_info->event_date ) );
				}
				$event_year = date( 'Y', strtotime( $event_info->event_date ) );
			}
		}

		// united states: mmm dd / yyyy
		$formatted_date[ 'monthdate' ]		= $event_monthdate;
		$formatted_date[ 'mmmdd' ]			= $event_mmmdd;
		$formatted_date[ 'monthdateeuro' ]	= $month_day_eur;
		$formatted_date[ 'month' ]			= $event_month;
		$formatted_date[ 'day' ]			= $event_day;
		$formatted_date[ 'year' ]			= $event_year;
		$formatted_date['event_day2']		= $event_day2;

		return $formatted_date;
	}


	/**
	 * function: image_translation
	 * dependable of q-translate
	 * returns a string of the image with the proper langauge extension based on language of site
	 * IF q-translate is not on or no file is found, returns regular version (should be English)
	 */
	function image_translation( $qt_lang, $image_file ) {
		$qt_status		= $qt_lang['enabled'] == 1 ? true : false;
		$qt_cur_lang	= $qt_lang['lang'];
		$extension		= strrchr($image_file, '.'); // grab extension

		// override image with extension only if qtranslate is enabled AND viewing another language other than english
		if( $qt_status && $qt_lang['lang'] != 'en' ) {
			$image_file_lang = str_replace( $extension, '_' . $qt_lang['lang'] . $extension, $image_file );
			// check if file exits by checking for imagesize. if not, default to original
			$image_file = @getimagesize($image_file_lang) ? $image_file_lang : $image_file;
		}
		return $image_file;
	}


	/**
	 * function: AJAX call for Find Race modal
	 * @return images for modal
	 */
	add_action( 'wp_ajax_nopriv_findrace_ajax_data', 'findrace_ajax_data_callback' );
	add_action( 'wp_ajax_findrace_ajax_data', 'findrace_ajax_data_callback' );

	function findrace_ajax_data_callback(){
		global $wpdb;
		ob_start();

		if( !empty( $_POST['event_slug'] ) ){

			$all_event_info = rnr3_get_all_events();

			foreach( $all_event_info as $event){

				if( $_POST['event_id'] ==  $event->id ){

					for( $i = 1; $i < 5; $i++ ) {

						if( $event->{ 'findrace_gallery' . $i } != '' ) {

							$image_url  = $event->{ 'findrace_gallery' . $i };

							if( !is_main_site() ) switch_to_blog(1);

							$image_data = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url ));

							if( !is_main_site() ) restore_current_blog();

							${ 'findrace_gallery_' . $i } = $image_data[0];
							${ 'findrace_gallery_sized_' . $i } = wp_get_attachment_image_src( ${ 'findrace_gallery_' . $i }, 'newsletter-post' );

							if( ${ 'findrace_gallery_sized_' . $i } != false ) {

								$image_url_resized	= ${ 'findrace_gallery_sized_' . $i }[0];

								// check if CDN linker is on and do it's work for it, since plugin does not take switch_to_blog() into account
								if ( is_plugin_active('CDN-Linker-master/wp-cdn-linker.php') ) {
									$image_url_resized	= str_replace( get_site_url(), get_option('ossdl_off_cdn_url'), $image_url_resized );
								}

								echo '<div><img class="lazyOwl" data-src="'. $image_url_resized .'"></div>';
							}
						}
					} // end for

				}

			}

		} // end !empty( $_POST['event_slug'] )

		$output = ob_get_clean();

		echo $output;

		die();
	}


	/**
	 * function: EU Regulation cookie set up
	 */
	function eu_regulation() {
		$cookie_name		= 'rnr_eu_regulation';
		$eu_regulation_txt	= rnr_get_option( 'eu_regulation_txt' );

		// get translated message
		$qt_lang	= rnr3_get_language();
		$qt_status	= $qt_lang['enabled'] == 1 ? true : false;

		if ( $qt_status ) {

			$qt_cur_lang		= $qt_lang['lang'];
			$eu_regulation_txt	= qtrans_split( $eu_regulation_txt )[$qt_cur_lang];

		} else {

			$split_regex = '/<\!--:(\w+?)-->([\s\S]*?)<\!--:-->/i';

			if( preg_match_all( $split_regex, $eu_regulation_txt, $matches ) ) {

				$titles	= array();
				$count	= count($matches[0]);

				for ( $i = 0; $i < $count; $i++ ) {
					$titles[$matches[1][$i]] = $matches[2][$i];
				}

				$eu_regulation_txt	= $titles['en'];

			} else {
				$eu_regulation_txt	= $eu_regulation_txt;
			}

		}


		if ( !isset( $_COOKIE[$cookie_name] ) && !empty( $eu_regulation_txt ) ) { ?>

			<div id ="cookie-popup-bar" class="cookie-popup-bar">
				<a href="#" class="close-cookie-bar"></a>
				<?php echo stripslashes( $eu_regulation_txt ); ?>
			</div>

			<script>
				// Make absolutely sure to display or not display the cookie bar (there might be no recognition due to cache)
				(function() {
					// Check for both localStorage data and cookie
					var cookieStatus	= document.cookie.indexOf( '<?php echo $cookie_name; ?>' ) !== -1 ? true : false;
					var storageStatus	= localStorage.getItem( '<?php echo $cookie_name; ?>' ) ? true : false;

					if ( cookieStatus || storageStatus ) {
						var regulations_bar	= document.getElementById('cookie-popup-bar');

						regulations_bar.style.display	= 'none';
					}
				} )();
			</script>

		<?php }
	}


	/**
	 * Author Information
	 */
	function get_author_info() {
		/**
		 * don't show author info if guest author is entered
		 */
		if( get_post_meta( get_the_ID(), 'guest_author', 1 ) == '' ) {
			$displayname	= get_the_author_meta( 'display_name' );
			$authorurl		= get_author_posts_url( get_the_author_meta( 'ID' ) );
			$gravatar		= get_avatar( get_the_author_meta( 'ID' ), 120, '', $displayname );
			$avatarurl		= get_the_author_meta( '_vn_avatar' );
			$avatarid		= get_attachment_id_from_src( $avatarurl );
			$avatarsized	= wp_get_attachment_image_src( $avatarid, 'thumbnail' );
			$bio			= apply_filters( 'the_content', get_the_author_meta( 'description' ) );
			$twitter		= get_the_author_meta( '_vn_twitter' );
			$facebook		= get_the_author_meta( '_vn_facebook' );
			$instagram		= get_the_author_meta( '_vn_instagram' );
			$pinterest		= get_the_author_meta( '_vn_pinterest' );
			$googleplus		= get_the_author_meta( '_vn_google_plus' );

			/**
			 * use gravatar as a fallback if no user profile image is uploaded
			 */
			if( $avatarsized[0] ) {
				$author_avatar = '<div class="article__author__thumbnail"><img src="'. $avatarsized[0] .'"></div>';
			} else {
				$author_avatar = '<div class="article__author__thumbnail">'. $gravatar .'</div>';
			}

			$authorbio_div = '<footer class="article__footer">
				<div class="row article__author">
					<a href="'. $authorurl .'">'. $author_avatar .'</a>';

					$authorbio_div .= '<div class="col-xs-9 article__author__bio">
						<h3>By <a href="'. $authorurl .'">'. $displayname .'</a></h3>

						'. $bio .'

						<ul class="article__author__social">';
							if( $twitter ) {
								$authorbio_div .= '<li>
									<a href="'. $twitter .'" target="_blank"><span class="fa fa-twitter"></span></a>
								</li>';
							}
							if( $facebook ) {
								$authorbio_div .= '<li>
									<a href="'. $facebook .'" target="_blank"><span class="fa fa-facebook"></span></a>
								</li>';
							}
							if( $instagram ) {
								$authorbio_div .= '<li>
									<a href="'. $instagram .'" target="_blank"><span class="fa fa-instagram"></span></a>
								</li>';
							}
							if( $pinterest ) {
								$authorbio_div .= '<li>
									<a href="'. $pinterest .'" target="_blank"><span class="fa fa-pinterest-p"></span></a>
								</li>';
							}
							if( $googleplus ) {
								$authorbio_div .= '<li>
									<a href="'. $googleplus .'" target="_blank"><span class="fa fa-google-plus"></span></a>
								</li>';
							}
						$authorbio_div .= '</ul>
					</div>
				</div>
			</footer>';

			return $authorbio_div;

		}

	}


	/**
	 * function: AJAX call for Mixitup modals
	 * @return images for modal
	 */
	add_action( 'wp_ajax_nopriv_mix_ajax_data', 'mix_ajax_data_callback' );
	add_action( 'wp_ajax_mix_ajax_data', 'mix_ajax_data_callback' );

	function mix_ajax_data_callback() {
		global $wpdb;
		ob_start();

		if ( !empty( $_POST['post_id'] ) ) {
			$post_id  = $_POST['post_id'];

			$gallery_images = get_post_meta( $post_id, '_rnr3_modal_gallery', 1 ) != '' ? get_post_meta( $post_id, '_rnr3_modal_gallery', 1 ) : false;

			if ( $gallery_images ) {

				foreach( $gallery_images as $image_id => $image ) {
					$image_data = wp_get_attachment_image_src( $image_id, 'newsletter-post' );
					echo '<div><img data-src="'. $image_data[0] .'" class="lazyOwl" alt=""></div>';
				}

			}

		} // end !empty( $_POST['post_id'] )

		$output = ob_get_clean();

		echo $output;

		die();
	}


	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 * taken from the underscores default theme setup
	 */
	function rnr3_entry_meta( $top = true, $market ) {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() && $market == 'rock-blog' ) {

			if( $top == true ) {
				/* translators: used between list items, there is a space after the comma */
				$categories_list = get_the_category_list( esc_html__( ', ', 'rnr3' ) );
				if ( $categories_list && rnr3_categorized_blog() ) {
					printf( '<p class="cat-links">' . esc_html__( 'Posted in %1$s', 'rnr3' ) . '</p>', $categories_list ); // WPCS: XSS OK.
				}
			} else {
				/* translators: used between list items, there is a space after the comma */
				$tags_list = get_the_tag_list( '', esc_html__( ', ', 'rnr3' ) );
				if ( $tags_list ) {
					printf( '<p class="tags-links">' . esc_html__( 'Tagged %1$s', 'rnr3' ) . '</p>', $tags_list ); // WPCS: XSS OK.
				}
			}

		}
	}


	/**
	 * Returns true if a blog has more than 1 category.
	 * taken from the underscores default theme setup
	 *
	 * @return bool
	 */
	function rnr3_categorized_blog() {

		if ( false === ( $all_the_cool_cats = get_transient( 'rnr3_categories' ) ) ) {
			// Create an array of all the categories that are attached to posts.
			$all_the_cool_cats = get_categories( array(
				'fields'		=> 'ids',
				'hide_empty'	=> 1,
				// We only need to know if there is more than one category.
				'number'		=> 2,
			) );

			// Count the number of categories that are attached to the posts.
			$all_the_cool_cats = count( $all_the_cool_cats );

			set_transient( 'rnr3_categories', $all_the_cool_cats );
		}

		if ( $all_the_cool_cats > 1 ) {
			// This blog has more than 1 category so rnr3_categorized_blog should return true.
			return true;
		} else {
			// This blog has only 1 category so rnr3_categorized_blog should return false.
			return false;
		}
	}


	/**
	 * Flush out the transients used in rnr3_categorized_blog.
	 * taken from the underscores default theme setup
	 */
	function rnr3_category_transient_flusher() {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}
		// Like, beat it. Dig?
		delete_transient( 'rnr3_categories' );
	}
	add_action( 'edit_category', 'rnr3_category_transient_flusher' );
	add_action( 'save_post',     'rnr3_category_transient_flusher' );


	/**
	 * Get Meta
	 *
	 * Utility function used to consolidate the quering of multiple meta values
	 * for the given object.
	 *
	 * @param int	 	$id ID of the current object.
	 * @param mixed		$fields Array/string containing meta field(s) to retrieve from database.
	 * @param string	$type Type of metadata request. Options: post/term/user
	 * @param constant	$output pre-defined constant for return type (OBJECT/ARRAY_A)
	 *
	 * @return mixed	MySQL object/Associative Array containing returned post metadata.
	 */
	function get_meta( $id = null, $fields = array(), $type = 'post', $output = ARRAY_A ) {
		global $wpdb;

		$fields		= esc_sql( $fields );
		$values_arr	= array();
		$values_obj	= new stdClass();
		$dbtable	= $wpdb->{$type.'meta'};
		$column_id	= $type . '_id';
		$id			= $id == null ? get_the_ID() : $id ;

		$query		= "SELECT meta_key, meta_value FROM {$dbtable} WHERE {$column_id} = {$id}";

		if ( !empty( $fields ) ) {

			if ( is_array( $fields ) ) {
				$query .= " AND meta_key IN ('". implode("','", $fields) ."')";
			} else {
				$query .= " AND meta_key = '{$fields}'";
			}
		}

		$results	=  $wpdb->get_results( $query, OBJECT_K );


		foreach ( $results as $key => $result ) {
			$values_arr[$key]	= $result->meta_value;
			$values_obj->{$key}	= $result->meta_value;
		}

		if ( !is_array( $fields ) && !empty( $values_arr[$fields] ) ) {

			return $output == ARRAY_A ? $values_arr[$fields] : $values_obj[$fields];

		}

		return $output == ARRAY_A ? $values_arr : $values_obj;
	}


	/**
	 * get travel sponsors
	 * @param	int	$travel_page_id	id of the event's travel page
	 */
	function travel_sponsors( $travel_page_id ) {

		$sponsors	= ( get_post_meta( $travel_page_id, '_rnr3_sponsors', 1 ) != '' ) ? get_post_meta( $travel_page_id, '_rnr3_sponsors', 1 ) : '';
		$sponsors	= ( $sponsors != '' ) ? array_filter( $sponsors ) : '';

		if( !empty( $sponsors ) && array_key_exists( 'image', $sponsors[0] ) ) {

			echo '<section class="wrapper travel_sponsors">

				<h3>'. get_post_meta( $travel_page_id, '_rnr3_sponsor_hdr', 1 ) .'</h3>
				<ul class="sponsors_list">';

					foreach( $sponsors as $sponsor ) {

						echo '<li>
							<a href="'. $sponsor['url'] .'"><img src="'. $sponsor['image'] .'" alt=""></a>
						</li>';

					}

				echo '</ul>
			</section>';

		}

	}


	/**
	 * display travel subpage columns
	 * don't show the subpage that you're on already
	 *
	 * @param	int	$travel_page_id	id of the event's travel page
	 */
	function travel_subpage_columns( $travel_page_id, $qt_lang ) {
		$market = get_market2();

		if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
			$event_info = rnr3_get_event_info( $market );
		}
		include 'languages.php';

		$travel_subpage_array = array(
			'fly'		=> 'flights',
			'stay'		=> 'hotels',
			'pack'		=> 'pack',
			'transport'	=> 'transport'
		);
		$url					= explode( '/', get_permalink() );
		$url_tail				= $url[ count($url) - 2];
		$post_meta				= get_meta( $travel_page_id );
		$count					= 0;
		$travel_column_build	= '';

		$travel_subpage_array_exclude = array_search( $url_tail, $travel_subpage_array );

		foreach( $travel_subpage_array as $key => $value ) {

			if( $key != $travel_subpage_array_exclude ) {

				if( array_key_exists( '_rnr3_' . $key . '_img', $post_meta ) && $post_meta['_rnr3_' . $key . '_img'] != '' ) {

					$travel_column_build .= '<div class="column_special">

						<figure>
							<img src="'. $post_meta['_rnr3_' . $key . '_img'] .'" alt="">
						</figure>';


						if( $qt_lang['enabled'] == 1 ) {
							$col_title_array	= qtrans_split( $post_meta['_rnr3_' . $key . '_title'] );
							$col_title			= $col_title_array[$qt_lang['lang']];
						} else {
							$col_title = $post_meta['_rnr3_' . $key . '_title'];
						}

						$travel_column_build .= '<h3>'. $col_title .'</h3>';

						/**
						 * sponsor (optional)
						 */
						if( $key == 'pack' ) {
							switch_to_blog( 1 );

								$blog1_pack_page_id = get_id_by_slug( 'travel/pack' );

								$subpage_sponsor = ( get_post_meta( $blog1_pack_page_id, '_rnr3_sponsor_img', 1 ) != '' ) ? get_post_meta( $blog1_pack_page_id, '_rnr3_sponsor_img', 1 ) : '';

								$travel_column_build .= '<p class="type_sponsor">'. $presented_by_txt .' <img src="'. $subpage_sponsor .'" alt=""></p>';

							restore_current_blog();
						}
						if( array_key_exists( '_rnr3_' . $key . '_sponsor', $post_meta ) && $post_meta['_rnr3_' . $key . '_sponsor'] != '' ) {
							$travel_column_build .=  '<p class="type_sponsor">Presented by <img src="'. $post_meta['_rnr3_' . $key . '_sponsor'] .'" alt=""></p>';
						}


						if( $qt_lang['enabled'] == 1 ) {
							$col_desc_array	= qtrans_split( $post_meta['_rnr3_' . $key . '_desc'] );
							$col_desc		= $col_desc_array[$qt_lang['lang']];
						} else {
							$col_desc = $post_meta['_rnr3_' . $key . '_desc'];
						}

						$travel_column_build .= '<p>'. $col_desc .'</p>';


						if( $qt_lang['enabled'] == 1 ) {
							$col_cta_array	= qtrans_split( $post_meta['_rnr3_' . $key . '_cta'] );
							$col_cta		= $col_cta_array[$qt_lang['lang']];
						} else {
							$col_cta = $post_meta['_rnr3_' . $key . '_cta'];
						}

						$travel_column_build .= '<p><a class="cta" href="'. get_permalink( $travel_page_id ) . $value .'">'. $col_cta .'</a></p>

					</div>';

					$count++;

				}

			}
		}

		echo '<section class="grid_'. $count .'_special travel_subpages">
			<div class="wrapper">'

				. $travel_column_build .'

			</div>
		</section>';

	}



	// add_action('init', 'confirmphone_process_request');

	// function confirmphone_process_request() {

	// 	if( isset( $_POST['send_code_action'] ) ) {

	// 		$json = file_get_contents('http://runrocknrollapp.com/ajax/tracking/sendphonecode/phone/'. $_POST['phone'] .'/code/fakecode');
	// 		$obj = json_decode($json);

	// 		print_r( $obj );

	// 	}

	// }


	/**
	 * get rnr logo
	 * @param  boolean $twenty_years_enabled if enabled, use the special logo
	 * @return string  echo logo
	 */
	function get_logo( $twenty_years_enabled, $market, $twenty_years_exception_events ) {
		$alternate_logo = ( rnr_get_option( 'rnrgv_alternate_logo' ) != '' ) ? rnr_get_option( 'rnrgv_alternate_logo' ) : '';

		$logo_url = get_site_url();
		if ( get_blog_details()->path == '/finisher-zone/' ){
			$logo_url = get_site_url(1);
		}

		echo '<div id="logo">
			<a href="'. $logo_url .'">';

				if( ( !in_array( $market, $twenty_years_exception_events) ) && $twenty_years_enabled == 'on' && $alternate_logo != '' ) {
					echo '<img src="'. $alternate_logo .'" alt="Rock \'N\' Roll Marathon Series" class="mid">';
				} else {
					echo '<img src="'. get_stylesheet_directory_uri() .'/img/logo@2x.png" alt="Rock \'N\' Roll Marathon Series" class="mid">';
				}

			echo '</a>
		</div>';
	}


	// remove nasty features of WP
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );


	/**
	 * Include header info file for IMI page
	 */
	function include_imi_header_file() {

		global $post;

		if ( is_object( $post ) ) {

			$template = get_page_template_slug( $post->ID );

			if ( $template == 'citypage.php' || $post->post_name == 'san-diego-attractions' ) {

				$source = get_template_directory() .'/header-imiinfographic.php';

				if ( file_exists($source) ) {
					include_once($source);
				}

			}

		}

	}
	add_action( 'wp_head','include_imi_header_file' );


	/**
	 * Properly displays content based on qtranslate-x being enabled/disabled
	 * When a post is broadcast from a blog with qtranslate-x to one that doesn't have it enabled,
	 * language brackets will display.
	 *
	 * Checks for language brackets and returns the proper content back
	 */
	function rnr3_content_qtx_filter( $content ) {

		$content_split = preg_split( '(\[:(\w{0,2})\])', $content );

		if( count( $content_split ) > 1 ) {

			return $content_split[1];

		} else {

			return $content;
		}

	}

	add_filter( 'pre_get_document_title', 'rnr3_content_qtx_filter' );
	add_filter( 'wpseo_title', 'rnr3_content_qtx_filter' );

	add_filter( 'the_title', 'rnr3_content_qtx_filter' );
	add_filter( 'the_content', 'rnr3_content_qtx_filter' );
	add_filter( 'the_excerpt', 'rnr3_content_qtx_filter' );

	function add_chartbeat() {

		$chartbeat	= rnr_get_option( 'rnrgv_chartbeat');

		if ( $chartbeat != '' ) { ?>

			<script type="text/javascript">
				var _sf_async_config={uid:<?php echo $chartbeat; ?>,domain:"runrocknroll.com",useCanonical:true};
				(function(){
					function loadChartbeat() {
						window._sf_endpt=(new Date()).getTime();
						var e = document.createElement('script');
						e.setAttribute('language', 'javascript');
						e.setAttribute('type', 'text/javascript');
						e.setAttribute('src', '//static.chartbeat.com/js/chartbeat.js');
						document.body.appendChild(e);
					}
					var oldonload = window.onload;
					window.onload = (typeof window.onload != 'function') ?
						 loadChartbeat : function() { oldonload(); loadChartbeat(); };
				})();

			</script>

		<?php }

	}


	add_filter( 'wp_footer', 'add_chartbeat', 999 );

	/**
	 * Get posts specifically broadcasted to a current blog
	 * We do this by targeting the table that ThreeWP created and finding the post_id's that match up with our current blog_id
	 *
	 * @param  int		$parent_blog_ID integer ID of the parent blog we want broadcasted posts from
	 * @return array					Holds the post ID on the current blog, parent blog ID, and ID of the post on that parent blog
	 */
	function get_broadcasted_posts( $parent_blog_ID = null ) {

		$broadcasted_posts	= array();

		if ( function_exists('ThreeWP_Broadcast') != false ) {

			global $wpdb;

			$current_blog_ID		= get_current_blog_id();
			$threewp_broadcast		= ThreeWP_Broadcast();
			// define the broadcast table
			$threewp_table			= is_object($threewp_broadcast) ? $threewp_broadcast->broadcast_data_table() : '';
			// get the posts that were broadcasted to this blog
			$threewp_query			= "SELECT post_id FROM $threewp_table WHERE blog_id = $current_blog_ID";
			$broadcasted_post_IDs	= $wpdb->get_results( $threewp_query );

			if ( !empty( $broadcasted_post_IDs ) ) {

				foreach ( $broadcasted_post_IDs as $broadcasted_post_ID ) {

					$post_data	= $threewp_broadcast->get_post_broadcast_data( $current_blog_ID, $broadcasted_post_ID->post_id );
					$parent		= $post_data->get_linked_parent();

					if ( $parent_blog_ID == null || $parent_blog_ID == $parent['blog_id'] ) {

						$broadcasted_posts[$post_data->post_id]	= array(
							'post_id'			=> $post_data->post_id,
							'parent_blog_id'	=> $parent['blog_id'],
							'parent_post_id'	=> $parent['post_id']
						);

					}

				}

			}

		}

		return $broadcasted_posts;

	}


	/**
	 * countdown timer for the global alertbar
	 */
	function rnr3_global_countdown_timer( $current_phase, $lang_info ) {
		if( rnr_get_option( 'rnrgv_enable_gab_enable_clock' ) == 'on' ) {

			if( $lang_info['lang'] == 'es' ) {
				$countdown_day	= "D&#237;as";
				$countdown_hour	= "Horas";
				$countdown_min	= "Min";
				$countdown_sec	= "Seg";
			} elseif( $lang_info['lang'] == 'fr' ) {
				$countdown_day	= "Jours";
				$countdown_hour	= "Heures";
				$countdown_min	= "Min";
				$countdown_sec	= "Sec";
			} elseif( $lang_info['lang'] == 'pt' ) {
				$countdown_day	= "Dias";
				$countdown_hour	= "Horas";
				$countdown_min	= "Mins";
				$countdown_sec	= "Seg";
			} else {
				$countdown_day	= "Days";
				$countdown_hour	= "Hrs";
				$countdown_min	= "Min";
				$countdown_sec	= "Sec";
			}


			/**
			 * for promo phase, we want to countdown to the end of the day (23:59)
			 * for teaser and upsell, we want to countdown to beginning of next day (midnight)
			 */
			$phase_date	= rnr_get_option( 'rnrgv_gab_day' );

			if( $current_phase == 'dayof' ) {

				// will end at 11:59pm (23:59)
				$clock_time = $phase_date + ( 23 * 60 * 60 ) + ( 59 * 60 );

			} else {

				// will start at midnight
				$clock_time = $phase_date;

			}


			$countdown_clock = '<table data-countdown="'. $clock_time .'" data-debugdate="'. date( 'Ymd', $clock_time ) .'" class="grd_countdown-table countdown">
				<tr>';

					// hide first column on promo phase (it'll only say 0 days)
					if( $current_phase != 'dayof' ) {

						$countdown_clock .= '<td><div class="grd_countdown-table-val countdown-days">--</div> <div class="grd_countdown-table-label">'. $countdown_day .'</div></td>';

					}

					$countdown_clock .= '<td><div class="grd_countdown-table-val countdown-hrs">--</div> <div class="grd_countdown-table-label">'. $countdown_hour .'</div></td>
					<td><div class="grd_countdown-table-val countdown-mins">--</div> <div class="grd_countdown-table-label">'. $countdown_min .'</div></td>';

					if( $current_phase != 'pre' ) {

						$countdown_clock .= '<td><div class="grd_countdown-table-val countdown-secs">--</div> <div class="grd_countdown-table-label">'. $countdown_sec .'</div></td>';

					}

				$countdown_clock .= '</tr>
			</table>';

			return $countdown_clock;

		} else {
			return false;
		}
	}


	/**
	 * get_meta content filter parse
	 * if using the get_meta() function and you need to apply the 'the_content' filter,
	 * will keep translations in mind!
	 *
	 * @param  [str] $content  content from the get_meta() return array
	 * @param  [arr] $qt_lang  language array (enabled, lang)
	 * @return [str]           parsed content
	 */
	function rnr3_get_meta_content_parse( $content, $qt_lang ) {
		$parsed_content_lang	= $qt_lang['enabled'] == 1 ? qtrans_split( $content ) : apply_filters( 'the_content', $content );
		$parsed_content			= $qt_lang['enabled'] == 1 ? $parsed_content_lang[$qt_lang['lang']] : $parsed_content_lang;

		return apply_filters( 'the_content', $parsed_content );
	}


/**
 * END FRONT END FUNCTIONS
 */
?>
