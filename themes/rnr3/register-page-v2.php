<?php
	/* Template Name: Register Page - RaceIT Prices */
	global $wpdb;

	// get all page meta at once
	$all_page_meta	= get_meta( get_the_ID() );
	$prefix			= '_rnr3_';

	$european_event	= array_key_exists( $prefix . 'europe', $all_page_meta ) ? 1 : 0;

	if( $european_event == 1 ) {

		// set timezone to gmt
		date_default_timezone_set('GMT' );
		$timezone = '';

		echo '<!-- gmt timezone set -->';

	} else {

		// set timezone to pacific
		date_default_timezone_set('America/Los_Angeles' );
		$timezone = 3600 * ( 8 - date( 'I' ) );

		echo '<!-- pacific timezone set -->';

	}
	$current_date = date( 'Y-m-d H:i:s' );


	get_header();

	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';


	$stj_enable			= array_key_exists( $prefix . 'stj_enable', $all_page_meta ) ? $all_page_meta[$prefix . 'stj_enable'] : '';
	$stj_mobile_link	= array_key_exists( $prefix . 'stj_mobile_link_txt', $all_page_meta ) ? $all_page_meta[$prefix . 'stj_mobile_link_txt'] : '';
?>


<!-- main content -->
<main role="main" id="main">
	<section class="wrapper grid_2 offset780left">

		<div class="column">
			<div class="content">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post();

					echo '<h2>'. get_the_title(). '</h2>';

					if( $stj_enable && $stj_mobile_link != '' ) {
						echo '<p class="showmobile"><a class="scrolly" href="#stjude">'. $stj_mobile_link .'</a></p>';
					}

					the_content();

				endwhile; endif;


				/**
				 * begin accordion
				 */
				echo '<div class="accordion">';

					$race = array(
						"days"	=> array( 1, 2, 3, 4 )
					);

					foreach( $race["days"] as $day ) {

						$race_event_date = array_key_exists( $prefix . 'day_'. $day .'_event_date', $all_page_meta ) ? $all_page_meta[$prefix . 'day_'. $day .'_event_date'] : '';

						if( $race_event_date != '' ) {

							/**
							 * translations for display date
							 */
							if( $qt_lang['lang'] == 'es' ) {

								$printed_date = $dias[date( 'w', $race_event_date )] .' '. date( 'd', $race_event_date ) .' de '. $meses[date( 'n', $race_event_date )-1] .' del '. date( 'Y', $race_event_date );

							} elseif( $qt_lang['lang'] == 'fr' ) {

								if( ( date( 'd', $race_event_date ) == '1' ) ) {
									$french_date = '1<sup>er</sup>';
								} else {
									$french_date = date( 'j', $race_event_date );
								}

								$printed_date = $journees[date( 'w', $race_event_date )] .', le '. $french_date .' '. $mois[date( 'n', $race_event_date )-1].' '. date( 'Y', $race_event_date );

							} else {

								$printed_date = date( 'l, F j, Y', $race_event_date );

							}

							echo '<h3>'. $printed_date .'</h3>';

							// get list of distances
							$distances_serialized = array_key_exists( $prefix .'day_'. $day .'_distance', $all_page_meta ) ? $all_page_meta[$prefix .'day_'. $day .'_distance'] : '';

							$distances = unserialize( $distances_serialized );

							foreach( $distances as $key => $entry ) {

								// get asterisks
								if( $entry['asterisks'] == 'single' ) {
									$show_asterisk = '*';
								} elseif( $entry['asterisks'] == 'double' ) {
									$show_asterisk = '**';
								} else {
									$show_asterisk = '';
								}

								// get properly translated type name
								$type_name = registerpage_type_translation( $entry['distance'], $qt_lang['lang'] );

								// get raceit stuffs
								$price_display = $wpdb->get_results(
									$wpdb->prepare(
										"
											SELECT current_price, currency_symbol, iso_currency
											FROM wp_registration_prices
											WHERE division_id = %d
										",
										$entry['raceit_id']
									)
								);

								if( $price_display != null ) {
									$iso_currency = ( $price_display[0]->iso_currency != 'USD' ) ? $price_display[0]->iso_currency : '';

									// $reg_tp3pk_url	= $day.'_'.$type.'_tp3pk_register_url';
									// $$reg_tp3pk_url	= get_post_meta( $page_ID, '_'.$day.'_'.$type.'_tp3pk_register_url', 1 );
									// $reg_tpg_url	= $day.'_'.$type.'_tpg_register_url';
									// $$reg_tpg_url	= get_post_meta( $page_ID, '_'.$day.'_'.$type.'_tpg_register_url', 1 );

									// create reg button
									if( $entry['status'] == 'soldout' ) {

										$reg_or_sold_out_btn = '<span class="highlight black">'. $txt_sold_out .'</span>';

									} elseif( $entry['status'] == 'atexpo' ) {

										$reg_or_sold_out_btn = '<span class="highlight black">'. $online_reg_closed_text .'</span>';

									} elseif( $entry['status'] == 'tbd' ) {

										$reg_or_sold_out_btn = '<span class="highlight black">'. $txt_TBD .'</span>';

									} else {

										$reg_or_sold_out_btn = '<a class="cta" href="'. $entry['reg_url'] .'" target="_blank">'. $txt_register .'</a>';

									}

									/**
									 * custom message accordion?
									 * if a date is entered, append 11:59:59 pm to it
									 * then convert timezone accordingly
									 */
									$custom_msg_enddate	= array_key_exists( 'custom_msg_enddate', $entry ) ? date( 'Y-m-d H:i:s', $entry['custom_msg_enddate'] + ( 23 * 60 * 60 ) + ( 59 * 60 ) + 59 + $timezone ) : '';
									$custom_msg			= array_key_exists( 'custom_msg', $entry ) ? $entry['custom_msg'] : '';

									if( $custom_msg_enddate != '' && $custom_msg_enddate > $current_date ) {
										$accordion_enable = 1;
									} else {
										$accordion_enable = 0;
									}

									$distance_chars = array( ' ', '/', '&apos;', '.' );
									$distance_name_safe = strtolower( str_replace( $distance_chars, '-', $type_name ) );

									if( $accordion_enable == 1 ) {

										echo '<div class="accordion-section accordion-section distance-'. $distance_name_safe .'" style="position: relative;">

											<div class="accordion-section-title" data-reveal="#X'. $distance_name_safe .'">'. $type_name . $show_asterisk .' &mdash; '. $price_display[0]->currency_symbol . $price_display[0]->current_price . ' ' . $iso_currency .'</div>'.
											$reg_or_sold_out_btn;

											echo '<ul id="X'. $distance_name_safe .'" class="accordion-section-content">
												<li class="tier">
													<h4>'. $entry['custom_msg'] .'</h4>
												</li>
											</ul>';

										echo '</div>';

									} else {

										echo '<div class="accordion-section accordion-section-ri distance-'. $distance_name_safe .'" style="position: relative;">

											<div class="accordion-section-title accordion-section-title-ri">'. $type_name . $show_asterisk .' &mdash; '. $price_display[0]->currency_symbol . $price_display[0]->current_price . ' ' . $iso_currency .'</div>'.
											$reg_or_sold_out_btn;

										echo '</div>';

									}

								} else {
									echo '<!-- '. $type_name .' - error importing data. please check your division id -->';
								}
							}
						}

					}

					$disable_disclaimer = array_key_exists( $prefix . 'disable_disclaimer', $all_page_meta ) ? $all_page_meta[$prefix . 'disable_disclaimer'] : '';

					if( $disable_disclaimer != 'on' ) {
						echo '<p>'. $price_disclaimer .'</p>';
					}

				echo '</div>';

				// Additional metabox 1
				$mb1_content	= array_key_exists( $prefix . 'additional_box1_content', $all_page_meta ) ? rnr3_get_meta_content_parse( $all_page_meta[$prefix . 'additional_box1_content'], $qt_lang ) : '';

				if( $mb1_content != '' ) {
					$mb1_switch		= array_key_exists( $prefix . 'additional_box1_display', $all_page_meta ) ? $all_page_meta[$prefix . 'additional_box1_display'] : '';
					$mb1_style		= empty( $mb1_switch ) ? ' style="display:none;"' : '';
					echo '<section id="additional-box-1" class="additional-box"'. $mb1_style .'>' .
						$mb1_content .'
					</section>';
				}


				/**
				 * st jude stuffs
				 */
				if( $stj_enable ) {

					if( $qt_lang['enabled'] == 1 ) {

						$stjtitle_array	= array_key_exists( $prefix . 'stj_title', $all_page_meta ) ? qtrans_split( $all_page_meta[$prefix . 'stj_title'] ) : '';
						$stjtitle		= $stjtitle_array[$qt_lang['lang']];

					} else {

						$stjtitle = array_key_exists( $prefix . 'stj_title', $all_page_meta ) ? $all_page_meta[$prefix . 'stj_title'] : '';

					}

					$stj_content = array_key_exists( $prefix . 'stj_content', $all_page_meta ) ? rnr3_get_meta_content_parse( $all_page_meta[$prefix . 'stj_content'], $qt_lang ) : '';

					echo '<section id="stjude">
						<h2>'. $stjtitle .'</h2>

						<div class="stj_content">
							<figure class="stj_alignright"><img src="'. get_bloginfo( 'stylesheet_directory' ) .'/img/stj-logo-small.svg" alt="St. Jude Heroes"></figure>'.
							$stj_content .'
						</div>

						<div class="stj_accordion">';

							$commitments_array	= array_key_exists( $prefix . 'stj_commitments', $all_page_meta ) ? unserialize( $all_page_meta[$prefix . 'stj_commitments'] ) : '';
							$raceiturl			= array_key_exists( $prefix . 'stj_register_url', $all_page_meta ) ? $all_page_meta[$prefix . 'stj_register_url'] : '';

							foreach( $commitments_array as $commitment ) {

								$commitment_pieces	= explode( '::', $commitment );
								$commit_key			= $commitment_pieces[0];
								$commit_display		= $commitment_pieces[1] . ' Hero';

								$commit_price	= str_replace( '.00', '', $all_page_meta[$prefix . 'stj_' . $commit_key . 'cost'] );
								$commit_content	= array_key_exists( $prefix . 'stj_' . $commit_key . 'content', $all_page_meta ) ? rnr3_get_meta_content_parse( $all_page_meta[$prefix . 'stj_' . $commit_key . 'content'], $qt_lang ): '';

								if( ( $market == 'montreal' ) && $qt_lang['lang'] == 'fr' ) {

									// translate on the fly ( won't disturb metaboxes on back end )
									if( strpos( $commit_display, 'Bronze' ) !== FALSE ) {
										$commit_display = 'H&eacute;ros Bronze';
									} elseif( strpos( $commit_display, 'Silver' ) !== FALSE ) {
										$commit_display = 'H&eacute;ros Argent';
									} elseif( strpos( $commit_display, 'Gold' ) !== FALSE ) {
										$commit_display = 'H&eacute;ros Or';
									} elseif( strpos( $commit_display, 'Platinum' ) !== FALSE ) {
										$commit_display = 'H&eacute;ros Platine';
									} elseif( strpos( $commit_display, 'Classic' ) !== FALSE ) {
										$commit_display = 'H&eacute;ros Classique ';
									}

								}


								if( $commit_price == '0.00' ) {

									$commit_price_display = $commit_price_open;

								} else {

									if( ( $market == 'montreal' ) && $qt_lang['lang'] == 'fr' ) {
										$commit_price			= str_replace( ',', '', $commit_price );
										$commit_price_display	= 'Financement de '. $commit_price .' $';
									} else {
										$commit_price_display = '$' . $commit_price . ' Fundraiser';
									}

								}

								echo '<div class="accordion-section" style="position: relative;">

									<div class="stj_accordion-section-title">'. $commit_display .'</div>';

									if( $commit_key != 'classic' ) {
										echo '<a class="cta" target="_blank" href="'. $raceiturl .'">'. $register_free_btn_text .'</a>';
									} else {
										echo '<a class="cta" target="_blank" href="'. $raceiturl .'">'. $txt_register .'</a>';
									}

									echo '<div class="stj_accordion-section-content" id="'. $commit_key .'">
										<div class="stj_column_wrapper">
											'. $commit_content .'
										</div>
									</div>

								</div>';
							}

						echo '</div>';

						$stj_below_tab_content = array_key_exists( $prefix . 'stj_below_tab_content', $all_page_meta ) ? rnr3_get_meta_content_parse( $all_page_meta[$prefix . 'stj_below_tab_content'], $qt_lang ): '';

						if( $stj_below_tab_content != '' ) {
							echo '<div class="stj_below_accordion">'.
								$stj_below_tab_content .'
							</div>';
						}

					echo '</section>';
				}

				// Additional metabox 2
				$mb2_content = array_key_exists( $prefix . 'additional_box2_content', $all_page_meta ) ? rnr3_get_meta_content_parse( $all_page_meta[$prefix . 'additional_box2_content'], $qt_lang ): '';

				if( $mb2_content != '' ) {

					$mb2_switch	= array_key_exists( $prefix . 'additional_box2_display', $all_page_meta ) ? $all_page_meta[$prefix . 'additional_box2_display'] : '';
					$mb2_style	= empty( $mb2_switch ) ? ' style="display:none;"' : '';

					echo '<section id="additional-box-2" class="additional-box"'. $mb2_style .'>'.
						$mb2_content .'
					</section>';

				}

				// reg notes
				$reg_notes = array_key_exists( $prefix . 'reg_notes', $all_page_meta ) ? rnr3_get_meta_content_parse( $all_page_meta[$prefix . 'reg_notes'], $qt_lang ): '';

				if( $reg_notes != '' ) {
					echo '<div class="registration_notes">'.
						$reg_notes .'
					</div>';
				} ?>

			</div>
		</div>

		<?php get_sidebar( 'register' ); ?>

	</section>

</main>


<?php
	get_footer();


	/**
	 * translate race type
	 * @param  string $type    raw type name
	 * @param  string $qt_lang language (en, es, fr, etc)
	 * @return string          translated race type
	 */
	function registerpage_type_translation( $type, $qt_lang ) {
		if( $qt_lang == 'es' ) { // spanish translations

			if( $type == '1k' ) {
				$type_name = '1 kil&oacute;metro';
			} elseif( $type == '1m' ) {
				$type_name = '1 Mile';
			} elseif( $type == '5k' ) {
				$type_name = '5 kil&oacute;metros';
			} elseif( $type == '10k' ) {
				$type_name = '10 kil&oacute;metros';
			} elseif( $type == '11k' ) {
				$type_name = '11 kil&oacute;metros';
			} elseif( $type == 'mm' ) {
				$type_name = 'Mini Marath&oacute;n';
			} elseif( $type == 'hh' ) {
				$type_name = 'Zappos.com Mitad de la Mitad';
			} elseif( $type == 'relay' ) {
				$type_name = 'Relevo medio marat&oacute;n para 2 personas';
			} elseif( $type == 'hm' ) {
				$type_name = 'Medio Marat&oacute;n';
			} elseif( $type == 'fm' ) {
				$type_name = 'Marat&oacute;n';
			} elseif( $type == 'cb-20k' ) {
				$type_name = 'All Day 20K';
			} elseif( $type == 'cb-jc' ) {
				$type_name = 'Junior Carlsbad 5000';
			} elseif( $type == 'kr' ) {
				$type_name = 'KiDS ROCK';
			} elseif( $type == 'funrun' ) {
				$type_name = 'Family Fun Run';
			} elseif( $type == 'fourpk' ) {
				$type_name = 'Family Fun Run 4-Pack';
			} elseif( $type == 'dubhalf' ) {
				$type_name = 'Athletics Ireland Half Marathon';
			} elseif( $type == 'cnhappy' ) {
				$type_name = 'Happy Run 6K';
			} elseif( $type == '5m' ) {
				$type_name = '5 Miler';
			}

		} elseif( $qt_lang == 'fr' ) {
			if( $type == '1k' ) {
				$type_name = 'P&apos;tit Marathon - 1 km';
			} elseif( $type == '1m' ) {
				$type_name = '1 mile';
			} elseif( $type == '5k' ) {
				$type_name = '5 km';
			} elseif( $type == '10k' ) {
				$type_name = '10 km';
			} elseif( $type == '11k' ) {
				$type_name = '11 km';
			} elseif( $type == 'mm' ) {
				$type_name = 'Mini marathon';
			} elseif( $type == 'hh' ) {
				$type_name = 'La demie du Demi';
			} elseif( $type == 'relay' ) {
				$type_name = 'Demi-marathon &agrave; relais- 2 personnes';
			} elseif( $type == 'hm' ) {
				$type_name = 'Demi-marathon';
			} elseif( $type == 'fm' ) {
				$type_name = 'Marathon';
			} elseif( $type == 'cb-20k' ) {
				$type_name = 'All Day 20K';
			} elseif( $type == 'cb-jc' ) {
				$type_name = 'Junior Carlsbad 5000';
			} elseif( $type == 'kr' ) {
				$type_name = 'KiDS ROCK';
			} elseif( $type == 'funrun' ) {
				$type_name = 'Family Fun Run';
			} elseif( $type == 'fourpk' ) {
				$type_name = 'Family Fun Run 4-Pack';
			} elseif( $type == 'dubhalf' ) {
				$type_name = 'Athletics Ireland Half Marathon';
			} elseif( $type == 'cnhappy' ) {
				$type_name = 'Happy Run 6 km';
			} elseif( $type == '5m' ) {
				$type_name = '5 miler';
			}

		} else {
			if( $type == '1k' ) {
				$type_name = '1K';
			} elseif( $type == '1m' ) {
				$type_name = '1 Mile';
			} elseif( $type == '5k' ) {
				$type_name = '5K';
			} elseif( $type == '10k' ) {
				$type_name = '10K';
			} elseif( $type == '11k' ) {
				$type_name = '11K';
			} elseif( $type == 'mm' ) {
				$type_name = 'Mini Marathon';
			} elseif( $type == 'hh' ) {
				$type_name = 'Zappos.com Half of the Half';
			} elseif( $type == 'relay' ) {
				$type_name = '2-Person Half Marathon Relay';
			} elseif( $type == 'hm' ) {
				$type_name = 'Half Marathon';
			} elseif( $type == 'fm' ) {
				$type_name = 'Marathon';
			} elseif( $type == 'cb-20k' ) {
				$type_name = 'All Day 20K';
			} elseif( $type == 'cb-jc' ) {
				$type_name = 'Junior Carlsbad 5000';
			} elseif( $type == 'kr' ) {
				$type_name = 'KiDS ROCK';
			} elseif( $type == 'funrun' ) {
				$type_name = 'Family Fun Run';
			} elseif( $type == 'fourpk' ) {
				$type_name = 'Family Fun Run 4-Pack';
			} elseif( $type == 'dubhalf' ) {
				$type_name = 'Athletics Ireland Half Marathon';
			} elseif( $type == 'virtual' ) {
				$type_name = 'Virtual Run';
			} elseif( $type == 'cnhappy' ) {
				$type_name = 'Happy Run 6K';
			} elseif( $type == '5m' ) {
				$type_name = '5 Miler';
			}
		}

		return $type_name;

	}

?>
