<?php
	/* Template Name: Insider - LV */

	get_header();

	$market = get_market2();
	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';

	$prefix		= '_rnr3_';
	$page_id	= get_the_ID();

	// get all metadata needed
	$post_pubkey	= rnr_get_option( 'rnrgv_post_pubkey');
	$page_intro		= get_post_meta( $page_id, $prefix .'intro_copy', true );
	$the_posts		= get_post_meta( $page_id, $prefix .'posts_group', true );

	// create navigation
	$post_navigation	= '';
	foreach ( $the_posts as $key => $single_post ) {
		$post_navigation	.= sprintf( '<li><a href="%s">%s</a></li>', '#post'. ($key + 1), strtoupper( $single_post['title'] ) );
	}
?>

	<nav id="secondary">
		<section class="wrapper">
			<div class="menu-label">Menu</div>
			<ul class="menu">
				<?php echo $post_navigation; ?>
			</ul>
		</section>
	</nav>

	<div id="announcements"></div>
	<main role="main" id="main" class="lv-announcements">
		<div id="nav-anchor"></div>
		<section class="wrapper grid_2 offset240left">
			<div class="column sidenav stickem">
				<nav class="sticky-nav">
					<ul>
						<?php echo $post_navigation; ?>
					</ul>
				</nav>
			</div> <!-- END .COLUMN.SIDENAV.STICKEM -->

			<div class="column">
				<div class="content">
					<?php
						the_title( '<h2>', '</h2>' );

						echo '<div class="social_container">';
							social_sharing( $post_pubkey );
						echo '</div>';

						if ( !empty( $page_intro ) ) {
							echo apply_filters( 'the_content', $page_intro );
						}

						foreach ( $the_posts as $key => $single_post ) {

							if ( isset( $single_post['copy'] ) ) {

								$target_id	= 'post'. ($key + 1);
								$cta_url	= isset( $single_post['url'] ) ? $single_post['url'] : ''; ?>

								<div id="<?php echo $target_id; ?>" class="lv-announcement">
									<img src="<?php echo $single_post['image']; ?>">
									<h2><?php echo $single_post['title']; ?></h2>

									<?php echo apply_filters( 'the_content', $single_post['copy'] ); ?>

									<?php if ( $cta_url != '' ): ?>
										<a class="highlight gray" href="<?php echo $cta_url ?>" target="_blank">Learn More</a>
									<?php endif; ?>
									<div class="clearfix"></div>
								</div>


							<?php }

						}

						echo '<div class="entry-footer">';
							social_sharing( $post_pubkey, 0 );
						echo '</div>';
					?>
				</div>
			</div>
		</section>
	</main>

	<script>
		$(document).ready(function(){

			/**
			 * This part causes smooth scrolling using scrollto.js
			 * We target all a tags inside the nav, and apply the scrollto.js to it.
			 */
			$(".sticky-nav li > a").click(function(evn){
				evn.preventDefault();
				$('html,body').scrollTo(this.hash, this.hash);
			});

			/*$('main').stickem({
				container: '.offset240left',
			});*/

			/**
			 * This part handles the highlighting functionality.
			 * We use the scroll functionality again, some array creation and
			 * manipulation, class adding and class removing, and conditional testing
			 */
			var aChildren = $(".sticky-nav li").children(); // find the a children of the list items
			var aArray = []; // create the empty aArray
			for ( var i=0; i < aChildren.length; i++ ) {
				var aChild = aChildren[i];
				var ahref = $(aChild).attr('href');
				aArray.push(ahref);
			} // this for loop fills the aArray with attribute href values

			$(window).scroll(function(){
				var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
				var windowHeight = $(window).height(); // get the height of the window
				var docHeight = $(document).height();

				for ( var i=0; i < aArray.length; i++ ) {
					var theID = aArray[i];
					var divPos = $(theID).offset().top; // get the offset of the div from the top of page
					var divHeight = $(theID).height(); // get the height of the div in question
					if ( windowPos >= (divPos - 1) && windowPos < (divPos + divHeight -1 ) ) {
						$("a[href='" + theID + "']").addClass("nav-active");
					} else {
						$("a[href='" + theID + "']").removeClass("nav-active");
					}
				}

				if(windowPos + windowHeight == docHeight) {
					if ( !$(".sticky-nav li:last-child a").hasClass("nav-active") ) {
						var navActiveCurrent = $(".nav-active").attr("href");
						$("a[href='" + navActiveCurrent + "']").removeClass("nav-active");
						$(".sticky-nav li:last-child a").addClass("nav-active");
					}
				}
			});
		});

	</script>


 <?php get_footer(); ?>
