<?php
	if(get_current_blog_id() <= 1){
		get_header('series-home');
	} else {
		get_header();
	}

	$parent_slug = the_parent_slug();
	rnr3_get_secondary_nav( $parent_slug );

	$market = get_market2();

?>

	<!-- main content -->
	<main role="main" id="main">
		<section class="wrapper grid_1">
			<div class="content">
				<?php
					if (have_posts()) : while (have_posts()) : the_post();
						echo '<h2>'. get_the_title(). '</h2>';
						the_content();
					endwhile; endif;
				?>
			</div>
		</section>

	</main>

<?php if( get_current_blog_id() <= 1 ) {
	get_footer( 'series' );
} elseif( $market == 'bonus-track' ) {
	get_footer('series');
} else {
	get_footer();
} ?>
