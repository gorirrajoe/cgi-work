<?php
	/**
	 * sidebars
	 */
	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';

if( is_singular( 'schedule' ) ) { ?>

	<div class="column sidenav">
		<h2>Schedule</h2>
		<?php get_sidebar_recent_posts( -1, get_post_type() ); ?>
	</div>

<?php } elseif( is_singular( 'moment' ) || is_post_type_archive( 'moment' ) ) { ?>

	<div class="column sidenav">
		<h2><a href="<?php echo site_url( '/moments/' ); ?>">&laquo; Back to Moments</a></h2>
		<?php get_sidebar_recent_posts( -1, get_post_type() ); ?>
	</div>

<?php } elseif( is_singular( 'partner' ) || is_page_template( 'partners-series.php' ) ) { ?>

	<div class="column sidenav">
		<h2><?php echo $partners_txt; ?></h2>
		<?php get_partners_sidebar(); ?>
	</div>

<?php } elseif( $market == 'rock-blog' ) { ?>

	<div class="column sidenav">
		<div class="sidebar-search">
			<?php get_search_form(); ?>
		</div>

		<h2>Recent</h2>
		<?php
			get_sidebar_recent_posts( 10, 'post', '', $market );

			echo '<h2>Categories</h2>
			<ul class="cat_list">';
				$args = array(
					'hide_title_if_empty'	=> true,
					'orderby'				=> 'name',
					'title_li'				=> ''
				);
				wp_list_categories( $args );
			echo '</ul>';
		?>
	</div>

<?php } elseif( $market == 'virtual-run' ) { ?>

	<div class="column sidenav">
		<h2><a href="<?php echo site_url(); ?>">&laquo; To Virtual Run</a></h2>
		<?php get_sidebar_recent_posts( 10, 'post', '', 'virtual-run' ); ?>
	</div>

<?php } elseif( is_single() ){ ?>

	<div class="column sidenav">
		<h2><?php echo $recent_sidebar_txt; ?></h2>
		<?php get_sidebar_recent_posts( 10 ); ?>
	</div>

<?php } elseif( is_category() ) { ?>

	<div class="column sidenav">
		<h2><?php echo $recent_sidebar_txt; ?></h2>
		<?php get_sidebar_recent_posts( 10, 'post', single_cat_title( '', false ) ); ?>
	</div>

<?php } elseif( is_tag() ) { ?>

	<div class="column sidenav">
		<h2>Recent</h2>
		<?php get_sidebar_recent_posts_tag( single_tag_title( '', false ) ); ?>
	</div>

<?php } elseif( is_page_template( 'news-n-promos.php' ) ) { ?>

	<div class="column sidenav">
		<h2>Recent</h2>
		<?php get_sidebar_recent_posts( -1 ); ?>
	</div>

<?php } ?>
