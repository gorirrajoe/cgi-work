<?php
	/**
	 * global variables
	 */
	$twitter			= rnr_get_option( 'rnrgv_twitter');
	$instagram			= rnr_get_option( 'rnrgv_instagram');
	$pinterest			= rnr_get_option( 'rnrgv_pinterest');
	$youtube			= rnr_get_option( 'rnrgv_youtube');
	$facebook_series	= 'https://www.facebook.com/RunRocknRoll';

	// check for eventid in FZ search query
	if( isset( $_GET['eventid'] ) && is_numeric( $_GET['eventid'] ) ) {

		$eventid	= $_GET['eventid'];
		$evInfo		= getFZEventInfo( $eventid );

		switch_to_blog($evInfo->blog_id);
			$market = get_market2();
		restore_current_blog();

	} else {
		$market = get_market2();
	}

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}
	$qt_lang	= rnr3_get_language();

	include 'languages.php';

	if( $market == 'finisher-zone' ) { ?>
		<section id="fz-secondary-promotions">
			<section class="wrapper">
				<div class="grid_3_special">
					<?php
						// find 3 latest posts categorized as "promotion" and tagged as "results"
						$args = array(
							'category_name'		=> 'promotion',
							'tag'				=> 'results',
							'posts_per_page'	=> '3'
						);

						// transient api
						if ( false === ( $secondary_promo_query = get_transient( 'secondary_promo_query' ) ) ) {
							$secondary_promo_query = new WP_Query( $args );
							set_transient( 'secondary_promo_query', $secondary_promo_query, 5 * MINUTE_IN_SECONDS );
						}
						if( $secondary_promo_query->have_posts() ) {
							while( $secondary_promo_query->have_posts() ) {
								$secondary_promo_query->the_post();
								echo '<div class="column_special">
									<figure><a href="'. get_permalink() .'">'. get_the_post_thumbnail( get_the_ID(), 'infobox-thumbs' ) .'</a></figure>
									<h3 class="center"><a href="'. get_permalink() .'">'. get_the_title() .'</a></h3>
									<p>'. get_the_excerpt() .'</p>
									<p class="center"><a class="cta" href="'. get_permalink() .'">'. $learn_more_txt .'</a></p>
								</div>';
							}
						}
						wp_reset_postdata();
					?>
				</div>
			</section>
		</section>

	<?php }

	$noboxes_nosponsors = array( 'medals-series-2.php', 'competitor-wireless-series.php', 'ironman.php' );
	if( !is_page_template( $noboxes_nosponsors ) ) {

		// big red boxes
		if( $event_info != '' && $event_info->big_red_boxes == 1 ) {
			rnr3_get_big_red_boxes( $market );
		}

		// sponsors
		// rnr3_get_sponsor_module2( $market ); -- don't use... yet
		rnr3_get_sponsor_module( $market );

	} ?>

	<a href="#" id="goTop">
		<span class="icon-up-open"></span>
	</a>

	<!-- combined footers -->
	<footer>

		<section class="wrapper">

			<!-- event footer -->
			<nav id="seriesfooter" role="navigation">

				<ul class="footer-nav">
					<li><a href="<?php echo network_site_url('/about-the-rock-n-roll-marathon-series/'); ?>">About</a></li>
					<li><a href="<?php echo network_site_url('/press/'); ?>">Press</a></li>
					<li><a href="<?php echo network_site_url('/results/'); ?>">Race Results</a></li>
					<li><a href="<?php echo network_site_url('/expo/'); ?>">Expo Vendor</a></li>
					<li><a href="<?php echo network_site_url('/partners/'); ?>">Partners</a></li>
					<li><a href="<?php echo network_site_url('/programs/#program-3'); ?>">Charity</a></li>
					<li><a href="<?php echo network_site_url('/contact/'); ?>">Contact</a></li>
					<li><a href="<?php echo network_site_url('/contact/faq/'); ?>">FAQ</a></li>
					<li><a href="<?php echo network_site_url('/tempo/'); ?>">Blog</a></li>
					<li><a href="<?php echo network_site_url('/elite-athlete-program/'); ?>">Elite Athletes</a></li>
					<li><a href="https://rtrt.me/app/rnra" target="_blank">Download our App</a></li>
				</ul>
			</nav>

			<!-- global footer -->
			<section id="globalfooter">
				<a href="<?php echo rnr_get_option( 'siteurl' ); ?>"><div id="footerlogo"><img class="lazy" data-original="<?php echo get_template_directory_uri(); ?>/img/logo_footer@2x.png" alt="Rock 'N' Roll Marathon Series logo"></div></a>
				<p>&copy; <?php echo date('Y'); ?> &ndash; Competitor Group, Inc. All Rights Reserved. <a href="<?php echo network_site_url('/about-competitor-group/'); ?>">About</a> | <a href="http://competitorgroup.com/privacy-policy/" target="_blank">Privacy and Cookie Policy</a> | <a href="http://competitorgroup.com/privacy-policy#third-parties" target="_blank">AdChoices</a> | <a href="http://competitorgroup.com/" target="_blank">Media Kit</a> | <a href="https://careers-competitorgroup.icims.com/jobs/intro?hashed=0" target="_blank">Careers</a></p>
			</section>

		</section>
	</footer> <!-- /combined footers -->

</div><!-- /container -->

<?php wp_footer();


// only show big hero video on home page
$youtube = get_post_meta( get_the_ID(), '_rnr3_youtube', true );

if( ( is_front_page() && ( $event_info != '' && $event_info->youtube_video != '' ) ) || $youtube != '' ) { ?>
	<script>
		$(document).ready(function(){
			$(".big_video_close").click(function(){
				$("#hero-video").hide();
				$("#hero").show();
			});
			$(".big_video").click(function(){
				$("#hero-video").show();
				$("#hero").hide();
			});
		});
		function toggleVideo(state) {
			// if state == 'hide', hide. Else: show video
			var div = document.getElementById("popupVid");
			var iframe = div.getElementsByTagName("iframe")[0].contentWindow;
			div.style.display = state == 'hide' ? 'none' : '';
			func = state == 'hide' ? 'pauseVideo' : 'playVideo';
			iframe.postMessage('{"event":"command","func":"' + func + '","args":""}','*');
		}
	</script>

<?php }

$pixlee = get_post_meta( get_the_ID(), '_rnr3_pixlee', TRUE );
if( ( $market != 'virtual-run' ) && ( $pixlee != '' ) ) {
	echo '<script type="text/javascript">
		function downloadJSAtOnload() {
			var element = document.createElement("script");
			element.src = "//assets.pixlee.com/assets/pixlee_widget_1_0_0.js";
			document.body.appendChild(element);
		}
		if (window.addEventListener)
			window.addEventListener("load", downloadJSAtOnload, false);
		else if (window.attachEvent)
			window.attachEvent("onload", downloadJSAtOnload);
		else window.onload = downloadJSAtOnload;
	</script>';
} ?>
</body>
</html>
