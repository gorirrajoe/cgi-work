<?php
	/* Template Name: Global Running Day - Landing */

	$market	= get_market2();

	require_once 'inc/grd-functions.php';

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info	= rnr3_get_event_info( $market );
	}

	$page_id		= get_the_ID();
	$current_date	= date( 'YmdH' );
	$current_phase	= '';


	/**
	 * get all post metadata
	 */
	$all_post_meta = get_meta( $page_id );


	/**
	 * get phase (based on date)
	 * convert to gmt: append '07' because san diego is PDT (gmt -7) in summer
	 */
	$phase0_date	= date( 'Ymd', $all_post_meta['_rnr3_phase0_start'] ) . '08';	// teaser
	$phase1_date	= date( 'Ymd', $all_post_meta['_rnr3_phase1_start'] ) . '08';	// day of
	$phase2_date	= date( 'Ymd', $all_post_meta['_rnr3_phase2_start'] ) . '08';	// upsell

	$phase = isset( $_GET['phase'] ) ? $_GET['phase'] : -1;

	if( isset( $_GET['phase'] ) ) {
		$current_phase_num	= $_GET['phase'];
		$current_phase		= 'phase' . $_GET['phase'];
	} elseif( $current_date >= $phase0_date && $current_date < $phase1_date ) {
		$current_phase_num	= 0;
		$current_phase		= 'tease';
	} elseif( $current_date >= $phase1_date && $current_date < $phase2_date ) {
		$current_phase_num	= 1;
		$current_phase		= 'promo';
		// set_interstitial_cookie_timeout( 1 );
	} elseif( $current_date >= $phase2_date ) {
		$current_phase_num	= 2;
		$current_phase		= 'upsell';
	}


	get_header( 'special' );

	$qt_lang = rnr3_get_language();
	include 'languages.php';
?>

<main role="main" id="main" class="no-hero grd grd-phase__<?php echo $current_phase; ?>">
	<?php /*<style>
		.grd-marquee-red {
			background-image: url( <?php echo $all_post_meta['_rnr3_marquee01_big']; ?> );
		}
	</style>*/ ?>

	<section class="wrapper">
		<div class="grd-marquee">
			<div class="grd-marquee-white">
				<div class="grd-marquee-white-group">
					<div class="grd-marquee-white-group__txt">
						<div class="kicker kicker-info2"><?php echo $grd_24hours; ?></div>
						<h1>
							<?php /*<span class="kicker bigtext-exempt kicker-info1"><?php echo $grd_title; ?></span>*/ ?>
							<div class="headline kicker-info3"><?php echo $grd_bestprices_1; ?></div>
							<div class="headline kicker-info3a"><?php echo $grd_bestprices_2; ?></div>
						</h1>
						<div class="subhead bigtext-exempt kicker-info4"><?php echo $grd_oftheyear; ?></div>
					</div>
					<div class="grd-marquee-white-group__img">
						<img src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/grd/header-runners.png">
					</div>
				</div>

				<div class="grd-marquee-white__info">
					<?php
						$fineprint = array_key_exists( '_rnr3_hdr_fineprint_txt', $all_post_meta ) ? qtrans_split( $all_post_meta['_rnr3_hdr_fineprint_txt'] ) : '';

						if( $fineprint != '' ) {
							echo apply_filters( 'the_content', $fineprint[$qt_lang['lang']] );
						}
					?>
				</div>

			</div>
			<div class="grd-marquee-red_arrow"></div>
			<div class="grd-marquee-red">

				<?php
					// special GRD countdown to start/end of sale
					$countdown_teaser	= array_key_exists( '_rnr3_countdown_teaser_txt', $all_post_meta ) ? qtrans_split( $all_post_meta['_rnr3_countdown_teaser_txt'] ) : '';
					$countdown_saleday	= array_key_exists( '_rnr3_countdown_saleday_txt', $all_post_meta ) ? qtrans_split( $all_post_meta['_rnr3_countdown_saleday_txt'] ) : '';
					$countdown_txt		= ( $current_phase == 'tease' || $current_phase_num == 0 ) ? ( $countdown_teaser != '' ? $countdown_teaser[$qt_lang['lang']] : '' ) : ( $countdown_saleday != '' ? $countdown_saleday[$qt_lang['lang']] : '' );


					if( $current_phase_num != 2 ) {

						echo '<div class="grd-countdown">';

							/**
							 * french wants only TEASE countdown text under the timer
							 */
							if( $qt_lang['lang'] != 'fr' ) {

								echo '<div class="grd-countdown-alert">'. $countdown_txt .'</div>';

							} elseif( $qt_lang['lang'] == 'fr' && ( $current_phase != 'tease' || $current_phase_num != 0 ) ) {

								echo '<div class="grd-countdown-alert">'. $countdown_txt .'</div>';

							}

					}

					echo grd_countdown_timer( $current_phase_num, $qt_lang );

					if( $current_phase_num != 2 ) {

						if( $qt_lang['lang'] == 'fr' && ( $current_phase == 'tease' || $current_phase_num == 0 ) ) {

							echo '<div class="grd-countdown-alert grd-countdown-alert-fr">'. $countdown_txt .'</div>';

						}

						echo '</div>';
					}


					// don't show surveygizz form on promo day or upsell phase
					if( $current_phase_num == 0 ) {

						echo grd_getsurveygizmo_form( 1, $all_post_meta, $qt_lang );

					} elseif( $current_phase_num == 2 ) {

						$upsell_txt = array_key_exists( '_rnr3_upsell_txt', $all_post_meta ) ? qtrans_split( $all_post_meta['_rnr3_upsell_txt'] ) : '';
						echo '<div class="grd-marquee-red__upsell">'. $upsell_txt[$qt_lang['lang']] .'</div>';

					}

				?>

			</div>
		</div>


		<?php
			/**
			 * desktop filterbar (>960px)
			 * will get sticky to the top when scrolling past it (appends .grd-filterbar__desktop--fixed)
			 * when fixed, hidden countdown list item will appear
			 */

			if( $current_phase_num != 2 ) { ?>

				<div class="grd-filterbar__desktop-placeholder"></div>

				<ul class="grd-filterbar__desktop">
					<li class="grd-filterbar-item grd-filterbar-item-countdown">

						<?php
							/**
							 * french wants only TEASE countdown text under the timer
							 */
							if( $qt_lang['lang'] != 'fr' ) {

								echo '<div class="grd-countdown-sticky-alert">'. $countdown_txt .'</div>';

							} elseif( $qt_lang['lang'] == 'fr' && ( $current_phase != 'tease' || $current_phase_num != 0 ) ) {

								echo '<div class="grd-countdown-sticky-alert">'. $countdown_txt .'</div>';

							}

							echo grd_countdown_timer( $current_phase_num, $qt_lang );

							if( $qt_lang['lang'] == 'fr' && ( $current_phase == 'tease' || $current_phase_num == 0 ) ) {

								echo '<div class="grd-countdown-sticky-alert grd-countdown-sticky-alert-fr">'. $countdown_txt .'</div>';

							}
						?>

					</li>
					<li class="grd-filterbar-item grd-filterbar-item-sortby">
						<span class="grd-filterbar__desktop-label grd-filterbar-label"><?php echo $filterby_txt; ?>: </span>
					</li>
					<li class="grd-filterbar-item">
						<a class="grd-filterbar-toggle grd-filterbar__desktop-toggle" href="#"><?php echo $txt_date; ?><img class="grd-filterbar__desktop-svg" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/grd/date.svg"><span class="icon-play"></span></a>
						<div style="display: none;" class="grd-filterbar__desktop-content grd-filterbar-content">
							<?php echo grd_get_quarters( $qt_lang, $event_info ); ?>
						</div>
					</li>
					<li class="grd-filterbar-item">
						<a class="grd-filterbar-toggle grd-filterbar__desktop-toggle" href="#"><?php echo $city_txt; ?><img class="grd-filterbar__desktop-svg" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/grd/city.svg"><span class="icon-play"></span></a>
						<div style="display: none;" class="grd-filterbar__desktop-content grd-filterbar-content__city grd-filterbar-content">
							<?php echo grd_get_cities( $qt_lang ); ?>
						</div>
					</li>
					<li class="grd-filterbar-item">
						<a class="grd-filterbar-toggle grd-filterbar__desktop-toggle" href="#"><?php echo $txt_distance; ?><img class="grd-filterbar__desktop-svg" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/grd/distance.svg"><span class="icon-play"></span></a>
						<div style="display: none;" class="grd-filterbar__desktop-content grd-filterbar-content">
							<?php echo grd_get_distances( $qt_lang, $event_info ); ?>
						</div>
					</li>
				</ul>

			<?php }

			/**
			 * events for promo
			 * fn: grdpromo_findrace will
			 */
		?>
		<div class="grd-cards">
			<?php
				// dev sanity check -- confirm current phase
				echo '<!-- '. $current_phase .' -->';

				if( $current_phase == 'tease' || $current_phase_num == 0 ) {

					$findrace_content = grdpromo_findrace( 0, $qt_lang, $event_info );
					echo $findrace_content;

				} elseif( $current_phase == 'promo' || $current_phase_num == 1 ) {

					$findrace_content = grdpromo_findrace( 1, $qt_lang, $event_info );
					echo $findrace_content;

					/*if( empty( $_GET ) && $qt_lang['lang'] == 'en' ) {
						get_interstitial( 1, 'grd_interstitial_1' );
					}*/

				} elseif( $current_phase == 'upsell' ) {

					// $findrace_content = grdpromo_findrace( 2, $qt_lang, $event_info );
					// echo $findrace_content;

					/*if( empty( $_GET ) && $qt_lang['lang'] == 'en' ) {
						get_interstitial( 2, 'grd_interstitial_2' );
					}*/

				}
			?>
		</div>


		<?php
			/**
			 * mobile countdown bar (<960px)
			 * will appear when scrolling to the top of the cards (appends .grd-countdown-sticky--fixed)
			 */
		?>
		<div class="grd-countdown-sticky">
			<div class="grd-countdown-sticky-alert"><?php echo $countdown_txt; ?></div>
			<?php echo grd_countdown_timer( $current_phase_num, $qt_lang ); ?>
		</div>


		<?php
			/**
			 * set up inline surveygizz form
			 * all events will call this form
			 * don't show it on promo day
			 */
			if( $current_phase_num != 1 ) {

				echo grd_getsurveygizmo_form( 0, $all_post_meta, $qt_lang );

			}


			/**
			 * mobile filterbar (<960px)
			 * sticks to the bottom of the screen
			 */

			if( $current_phase_num != 2 ) { ?>

				<ul class="grd-filterbar__mobile">
					<li class="grd-filterbar-item">
						<a class="grd-filterbar-toggle" href="#"><img class="grd-filterbar__mobile-svg" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/grd/date.svg"><?php echo $txt_date; ?></a>

						<div style="display: none;" class="grd-filterbar__mobile-content grd-filterbar-content">
							<?php echo grd_get_quarters( $qt_lang, $event_info ); ?>
						</div>
					</li>
					<li class="grd-filterbar-item">
						<a class="grd-filterbar-toggle" href="#"><img class="grd-filterbar__mobile-svg" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/grd/city.svg"><?php echo $city_txt; ?></a>

						<div style="display: none;" class="grd-filterbar__mobile-content grd-filterbar-content__city grd-filterbar-content">
							<?php echo grd_get_cities( $qt_lang ); ?>
						</div>
					</li>
					<li class="grd-filterbar-item">
						<a class="grd-filterbar-toggle" href="#"><img class="grd-filterbar__mobile-svg" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/grd/distance.svg"><?php echo $txt_distance; ?></a>

						<div style="display: none;" class="grd-filterbar__mobile-content grd-filterbar-content">
							<?php echo grd_get_distances( $qt_lang, $event_info ); ?>
						</div>
					</li>
				</ul>

			<?php }
		?>

	</section>
</main>


<?php
	get_footer( 'series' );
?>
