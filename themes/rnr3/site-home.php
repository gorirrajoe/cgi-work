<?php
	/*
	 * Template Name: Site Home
	 * Description: This is the event homepage template.
	 */
	get_header();

	global $wpdb;
	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';

	$blog_title = get_post_meta(  $post->ID, '_rnr3_blog_title', true ) ? get_post_meta(  $post->ID, '_rnr3_blog_title', true ) : 'FROM THE TEMPO' ;

	// Set up tempo variables
	$tempo_blog_id				= 41;
	$tempo_enabled				= ( get_post_meta( $post->ID, '_rnr3_tempo_enabled', true ) == 'on' ) ? true : false;
	$no_show_schedule_details	= get_post_meta( $post->ID, '_rnr3_no_show_schedule_details', true );
	// Get the posts broadcasted to this blog
	$tempo_broadcasted_posts	= get_broadcasted_posts( $tempo_blog_id );
?>
	<!-- three-spot panel/infoboxes -->
	<section id="features">
		<section class="wrapper grid_3">
			<?php for( $i = 1; $i < 4; $i++ ) {

				${'box' . $i . '_img'}			= get_post_meta( $post->ID, '_rnr3_box'.$i.'_img', TRUE );

				// check for image in appropriate language
				${'box' . $i . '_img'}			= image_translation( $qt_lang, ${'box' . $i . '_img'} );
				${'box' . $i . '_blurb'}		= apply_filters( 'the_content', get_post_meta( $post->ID, '_rnr3_box'.$i.'_blurb', TRUE ) );

				if( $qt_lang['enabled'] == 1 ) {

					if( get_post_meta( $post->ID, '_rnr3_box'.$i.'_url', TRUE ) != '' ) {
						${'box' . $i . '_url'} = qtrans_convertURL( get_post_meta( $post->ID, '_rnr3_box'.$i.'_url', TRUE ) );
					} else {
						${'box' . $i . '_url'} = '';
					}

				} else {

					${'box' . $i . '_url'} = get_post_meta( $post->ID, '_rnr3_box'.$i.'_url', TRUE );

				}

				if( $qt_lang['enabled'] == 1 ) {

					$box_array					= qtrans_split( get_post_meta( $post->ID, '_rnr3_box'.$i.'_title', TRUE ) );
					${'box' . $i . '_title'}	= $box_array[$qt_lang['lang']];
					$link_array					= qtrans_split( get_post_meta( $post->ID, '_rnr3_box'.$i.'_link_txt', TRUE ) );
					${'box' . $i . '_link_txt'}	= $link_array[$qt_lang['lang']];

				} else {

					${'box' . $i . '_title'}	= get_post_meta( $post->ID, '_rnr3_box'.$i.'_title', TRUE );
					${'box' . $i . '_link_txt'}	= get_post_meta( $post->ID, '_rnr3_box'.$i.'_link_txt', TRUE );

				}

				echo '<div class="column">
					<h2>'. ${'box' . $i . '_title'} .'</h2>';

					// if no url is added, don't link the image nor show the cta link
					if( ${'box' . $i . '_url'} != '' ) {

						echo '<figure><a href="'.${'box' . $i . '_url'}.'"><img class="lazy" data-original="'. ${'box' . $i . '_img'} .'" alt="'. ${'box' . $i . '_title'} .'"></a></figure>
						'. ${'box' . $i . '_blurb'} .'
						<p style="text-align: center;"><a class="cta" href="'. ${'box' . $i . '_url'} .'">'. ${'box' . $i . '_link_txt'} .'</a></p>';

					} else {

						echo '<figure><img src="'. ${'box' . $i . '_img'} .'" alt=""></figure>
						'. ${'box' . $i . '_blurb'};

					}

				echo '</div>';
			} ?>

		</section>
	</section>

	<?php
		if( $no_show_schedule_details != 'on' ): ?>

			<!-- schedule / news -->
			<section id="info">
				<section class="wrapper grid_2">
					<div class="schedule column"><!-- column 1 -->

					<?php if( $tempo_enabled && !empty( $tempo_broadcasted_posts ) ) { //for races with the blog enabled and have broadcasted_post_IDs

						$broadcasted_post_IDs	= wp_list_pluck( $tempo_broadcasted_posts, 'post_id' ); ?>

						<div class="blog icon"><span class="icon-blog_icon"></span></div>

						<h2><?php echo $blog_title;?></h2>

						<?php
							$lang		= $qt_lang['lang'];
							$blog_posts	= array();

							$common_args	= array(
								'post__in'					=> $broadcasted_post_IDs,
								'fields'					=> 'ids',
								'no_found_rows'				=> true,
								'update_post_meta_cache'	=> false,
								'update_post_term_cache'	=> false,
								'ignore_sticky_posts'		=> true,
							);

							// get the two featured posts
							$featured_args = array_merge( $common_args, array(
								'meta_key'			=> '_rnr3_featured',
								'meta_value'		=> 'on',
								'posts_per_page'	=> 2,
							) );

							$featured_post_obj	= new WP_Query( $featured_args );
							$featured_post_IDs	= $featured_post_obj->posts;

							if( !empty( $featured_post_IDs ) ) {
								// if you have featured posts that were broadcasted, add them to the array of posts to show
								$blog_posts	= array_merge( $blog_posts, $featured_post_IDs );
							}

							// lets get that video post
							$video_args = array_merge( $common_args, array(
								'category_name'				=> 'video',
								'ignore_sticky_posts'		=> true,
								'no_found_rows'				=> true,
								'update_post_meta_cache'	=> false,
								'update_post_term_cache'	=> false
							) );

							$video_post_obj	= new WP_Query( $video_args );
							$video_post_IDs	= $video_post_obj->posts;

							if( !empty( $video_post_IDs ) ) {
								// we have the posts w/video format that were broadcasted
								// now only use the ones that are not already in $blog_posts
								$duplicate_posts	= array_intersect( $blog_posts, $video_post_IDs );
								$vid_posts_to_add	= array_diff( $video_post_IDs, $duplicate_posts );
								$blog_posts			= array_merge( $blog_posts, $vid_posts_to_add );

							}

							if ( count($blog_posts) < 3 ) {
								// if not enough featured/video posts to accumulate the 3, then add how many are needed
								$num_posts_to_use	= 3 - count($blog_posts);

								$filler_post_obj	= new WP_Query( $common_args );
								$filler_post_IDs	= $filler_post_obj->posts;

								$duplicate_posts	= array_intersect( $blog_posts, $filler_post_IDs );
								$possible_posts		= array_diff( $filler_post_IDs, $duplicate_posts );
								$posts_to_add		= array_slice( $possible_posts, 0, $num_posts_to_use );
								$blog_posts			= array_merge( $blog_posts, $posts_to_add );

							}

							// get the IDs for the posts but on The Tempo
							$tempo_IDs	= array();

							foreach ( $blog_posts as $post_ID ) {
								$tempo_IDs[]	= $tempo_broadcasted_posts[$post_ID]['parent_post_id'];
							}

							switch_to_blog( $tempo_blog_id );

							// Get our posts
							$blog_post_object	= new WP_Query( array(
								'post__in'					=> $tempo_IDs,
								'orderby'					=> 'post__in',
								'no_found_rows'				=> true,
								'update_post_meta_cache'	=> false,
								'update_post_term_cache'	=> false,
								'ignore_sticky_posts'		=> true,
							) );

							$featured_count = 1;

							if ( $blog_post_object->have_posts() ) : while ( $blog_post_object->have_posts() ) : $blog_post_object->the_post();

								$blog_post_ID	= get_the_ID();
								$blog_img		= get_the_post_thumbnail_url( $blog_post_ID, 'news-promos-thumbs' );
								$auth_ID		= $blog_post_object->post->post_author;
								$author_name	= get_the_author_meta( 'display_name' );
								$author_link	= get_author_posts_url( $auth_ID );
								$blog_post_link = get_the_permalink();
								$categories		= get_the_category();
								$category		= $categories[0]->name;
								$category_ID	= $categories[0]->term_id;
								$category_link	= get_term_link( $category_ID );
								$featured		= get_post_meta( $blog_post_ID, '_rnr3_featured', true );
								$the_excerpt	= has_excerpt() ? get_the_excerpt() : wp_trim_words( strip_shortcodes( $blog_post_object->post->post_content ), 20, '...' );

								// check if CDN linker is on and do it's work for it, since plugin does not take switch_to_blog() into account
								if ( is_plugin_active( 'CDN-Linker-master/wp-cdn-linker.php' ) ) {
									$blog_img = str_replace( get_site_url(), get_option( 'ossdl_off_cdn_url' ), $blog_img );
								} ?>

								<article>
									<?php
										 if ( in_category('video') ) {
											echo '<figure class="video_blog_post"><a href="'. $blog_post_link .'"><img class="lazy" data-original="'. $blog_img .'"><img src="'. get_template_directory_uri() .'/img/play.svg" class="play_button"></a></figure>';
										 } else {
											echo '<a href="' . $blog_post_link . '"><figure><img class="lazy" data-original="'. $blog_img .'"></figure></a>';
										 }


										 if( $featured_count == 1 ) {
											if( $featured ) {
												echo '<h5 class="featured_blog">FEATURED STORY</h5>';

												$featured_count++;
											}
										}
									?>

									<h3><a href="<?php echo $blog_post_link;?>"><?php the_title(); ?></a></h3>
									<p class="blog_excerpt"><?php echo $the_excerpt; ?></p>

									<div class="article__meta">
										<span class="author"><a href="<?php echo $author_link;?>">By <?php echo $author_name;?></a> | </span><span class="category"><a href="<?php echo $category_link;?>"><?php echo $category; ?></a></span>
									</div>

									<span class="blog_read_more">
										<a href="<?php echo $blog_post_link;?>" class="more_link">
											<span class="blog_readmore"><?php echo has_post_format( 'video', $blog_post_ID ) ? 'See Video' : 'Read More'; ?></span>
										</a>
									</span>

								</article>

							<?php endwhile; endif;
							wp_reset_postdata();
							restore_current_blog();
						?>

						<p class="centered_text"><a href="<?php echo get_site_url( $tempo_blog_id ); ?>" class="more_link">See More Stories</a></p>

					<?php } else { ?>

						<div class="star"><span class="icon-star"></span></div>

						<h2><?php echo $text_schedule; ?></h2>

						<?php
							$schedule_count	= 0;
							$event_count	= 0;
							$current_date	= date( time() );

							if( ( $event_timezone = get_post_meta( get_the_ID(), '_rnr3_timezone', TRUE ) ) == '' ) {
								$event_timezone = date_default_timezone_get();
							}

							// check server timezone -- if it's set to UTC, don't add an offset
							if( date_default_timezone_get() == 'UTC' ) {
								$offset = get_timezone_offset( $event_timezone );
							} else {
								$offset = 0;
							}

							$offset_time = $current_date + $offset;

							// Get any existing copy of our transient data
							$lang = $qt_lang['lang'];

							if ( false === ( $schedule_query = get_transient( 'schedule_query_posts-'.$lang ) ) ) {
								$args = array(
									'post_type'			=> 'schedule',
									'posts_per_page'	=> -1,
									'meta_key'			=> '_rnr3_soe_date',
									'orderby'			=> 'meta_value',
									'order'				=> 'ASC'
								);
								$schedule_query = new WP_Query( $args );
								set_transient( 'schedule_query_posts-'. $lang, $schedule_query, 5 * MINUTE_IN_SECONDS );
							}

							if( $schedule_query->have_posts() ) {
								while( $schedule_query->have_posts() ) {
									$schedule_query->the_post();

									$schedule_count++;
									if( $schedule_count < 5 ) {

										$date_of_event		= date( 'Ymd', ( get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) ) ) . ' 23:59';
										$date_of_event_sec	= strtotime( $date_of_event ) + $offset;

										// if date of event happens in the past ( before "today" ), don't show it
										if( $offset_time <= $date_of_event_sec ) {

											if( $qt_lang['lang'] == 'en' ) {
												$dayofweek = date( 'D', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
											} elseif( $qt_lang['lang'] == 'fr' ) {
												$dayofweek_fr	= date( 'w', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
												$dayofweek		= get_day_or_month_by_language( 'fr', $dayofweek_fr, 'day' );
											} elseif( $qt_lang['lang'] == 'es' ) {
												$dayofweek_es	= date( 'w', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
												$dayofweek		= get_day_or_month_by_language( 'es', $dayofweek_es, 'day' );
											} elseif( $qt_lang['lang'] == 'pt' ) {
												$dayofweek_pt	= date( 'w', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
												$dayofweek		= get_day_or_month_by_language( 'pt', $dayofweek_pt, 'day' );
											}

											//$date = date( 'm.d.y', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
											$start_time		= date( 'g:i A', strtotime( get_post_meta( get_the_ID(), '_rnr3_soe_start_time', TRUE ) ) );
											$end_time		= date( 'g:i A', strtotime( get_post_meta( get_the_ID(), '_rnr3_soe_end_time', TRUE ) ) );

											$timestamp		= date( 'Y-m-d', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
											$morelinkurl	= get_post_meta( get_the_ID(), '_rnr3_more_link_url', TRUE );

											if( $morelinkurl != '' ) {
												$useURL = $morelinkurl;
											} else {
												$useURL = get_permalink();
											}

											if( $qt_lang['enabled'] == 1 ) {
												$useURL = qtrans_convertURL( $useURL );
											}

											if( $qt_lang['lang'] != 'en' ) {
												$date = date( 'd-m-y', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
											} else {
												$date = date( 'm-d-y', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
											}


											echo '<article>
												<time datetime="'. $timestamp .'T'. date( 'H:i', strtotime( $start_time ) ) .'">

													'. $dayofweek .' / '. $date;

													if( $start_time != '' && $end_time != '' ) {

														if( $qt_lang['lang'] == 'en' ) {
															echo ' / '. $start_time .' &ndash; '. $end_time;

														} elseif( $qt_lang['lang'] == 'fr' ) {
															$start_timehr   = date( 'G', strtotime( $start_time ) );
															$start_timemin  = date( 'i', strtotime( $start_time ) );
															$end_timehr     = date( 'G', strtotime( $end_time ) );
															$end_timemin    = date( 'i', strtotime( $end_time ) );
															echo ' / '. $start_timehr .' h '. $start_timemin .' &agrave; '. $end_timehr .' h '. $end_timemin;

														} elseif( $qt_lang['lang'] == 'es' || $qt_lang['lang'] == 'pt' ) {
															$start_time   = date( 'G:i', strtotime( $start_time ) );
															$end_time     = date( 'G:i', strtotime( $end_time ) );
															echo ' / '. $start_time .' &ndash; '. $end_time;
														}

													} elseif( $start_time != '' ) {
														echo ' / '. $start_time;
													}

												echo '</time>
												<h3>'. get_the_title() .'</h3>
												<p>'. get_the_excerpt() .'<br><a href="'. $useURL .'">'. $text_more_info .'</a></p>
											</article>';

											$event_count++;
										}
									}
								}
							}


							// if there are no FUTURE events
							if( $event_count == 0 ) {
								echo '<p>'. $text_stay_tuned .'</p>';
							}
							wp_reset_postdata();


							if( $qt_lang['enabled'] == 1 ) {
								echo '<p style="text-align: center;"><a href="' . qtrans_convertURL( site_url( '/the-weekend/schedule-of-events/' ) ) . '">' . $text_see_all . '</a></p>';
							} else {
								echo '<p style="text-align: center;"><a href="' . site_url( '/the-weekend/schedule-of-events/' ) .'">' . $text_see_all .  '</a></p>';
							}
						} ?>
					</div><!-- END .SCHEDULE.COLUMN (column 1) -->


					<div class="news column"><!-- column 2 -->

						<div class="list"><span class="icon-list"></span></div>
						<h2><?php echo $text_news_promo;?></h2>

						<?php
							$lang_news = $qt_lang['lang'];

							// Get any existing copy of our transient data
							if( false === ( $news_promo_query = get_transient( 'news_promo_posts-'. $lang_news .'-' . $market ) ) ) {
								// finds posts categorized as either 'news' or 'promotion'
								$news_obj	= get_category_by_slug( 'news' );
								$news_id	= $news_obj->term_id;
								$promo_obj	= get_category_by_slug( 'promotion' );
								$promo_id	= $promo_obj->term_id;

								$args = array(
									'category__in'		=> array( $news_id, $promo_id ),
									'posts_per_page'	=> 3,
									'post_status'		=> 'publish',
									'has_password'		=> false
								);

								$news_promo_query = new WP_Query( $args );
								set_transient( 'news_promo_posts-'. $lang_news .'-' . $market, $news_promo_query, 5 * MINUTE_IN_SECONDS );
							}

							if( $news_promo_query->have_posts() ) {
								while( $news_promo_query->have_posts() ) {
									$news_promo_query->the_post();

									if( has_post_thumbnail() ) {
										$post_thumb	= get_the_post_thumbnail_url( $post->ID, 'news-promos-thumbs' );
										$figure		= '<figure><a href="'. get_the_permalink() .'"><img class="lazy" data-original="'. $post_thumb .'"></a></figure>';
									} else {
										$figure = '';
									}

									$categories		= get_the_category();
									$category_id	= $categories[0]->cat_ID;
									$category_link	= get_category_link( $category_id );

									$the_excerpt	= has_excerpt() ? get_the_excerpt() : wp_trim_words( strip_shortcodes( get_the_content() ), 20, '...' );


									if( $qt_lang['enabled'] == 1 ) {
										$category_name_array	= qtrans_split( $categories[0]->cat_name );
										$category_name			= $category_name_array[$qt_lang['lang']];
									} else {
										$category_name = $categories[0]->cat_name;
									}

									if( $tempo_enabled == false ) {

										//$post_date = get_the_date( 'n.d.y' );

										if( $qt_lang['lang'] != 'en' ){
											$post_date = get_the_date( 'd-m-y' );
										} else {
											$post_date = get_the_date( 'm-d-y' );
										}

										echo '<article>'.
											$figure .'
											<time datetime="'. get_the_date( 'Y-m-d' ) .'"><a href="'. $category_link .'">'. $category_name .'</a> / '. $post_date .'</time>
											<h3><a href="'. get_the_permalink() .'">'. get_the_title() .'</a></h3>
											<p class="blog_excerpt">'. $the_excerpt .' <a href="'. get_the_permalink() .'">'. $readmore_txt .'</a></p>
										</article>';

									} else { ?>
										<article>
											<?php echo $figure; ?>

											<h3><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h3>
											<p class="blog_excerpt">
												<?php echo $the_excerpt; ?>
											</p>
											<p>
												<a href="<?php echo get_the_permalink(); ?>" class="more_link">
													<?php echo $readmore_txt; ?>
												</a>
											</p>


										</article>
									<?php }
								}
							}
							wp_reset_postdata();


							if( $tempo_enabled ) {
								if( $qt_lang['enabled'] == 1 ) {
									echo '<p class="centered_text"><a href="' . qtrans_convertURL( site_url( '/news-promotions/' ) ) . '">' . $text_see_all . '</a></p>';
								} else {
									echo '<p class="centered_text"><a href="' . site_url( '/news-promotions/' ) . '">' . $text_see_all . '</a></p>';
								}
							} else {
								if( $qt_lang['enabled'] == 1 ) {
									echo '<p style="text-align: center;"><a href="' . qtrans_convertURL( site_url( '/news-promotions/' ) ) . '">' . $text_see_all . '</a></p>';
								} else {
									echo '<p style="text-align: center;"><a href="' . site_url( '/news-promotions/' ) . '">' . $text_see_all . '</a></p>';
								}
							}
						?>

					</div><!-- END .NEWS.COLUMN (column 2) -->
				</section>
			</section>
		<?php endif;
	?>

	<!-- pixlee stuffs -->
	<?php
		$pixlee = get_post_meta( get_the_ID(), '_rnr3_pixlee_code', TRUE );

		if( $pixlee != '' ) { ?>

			<!-- pixlee content -->
			<section id="socialfeed">
				<section class="wrapper">

					<h2><?php echo $event_info->twitter_hashtag; ?></h2>
					<?php echo $pixlee; ?>

				</section>

			</section>

		<?php } else {

			$pixlee_album = get_post_meta( get_the_ID(), '_rnr3_pixlee_album_id', 1 );

			if( $pixlee_album != '' ) { ?>

				<!-- pixlee content -->
				<section id="socialfeed">
					<section class="wrapper">

						<h2><?php echo $event_info->twitter_hashtag; ?></h2>
						<?php get_pixlee_grid( $pixlee_album ); ?>

					</section>
				</section>

			<?php }
		}
	?>


	<!-- race circles -->
	<?php
		$count			= 0;
		$grid			= 0;
		$lowergrid		= 0;
		$event_array	= array();
		$race_array		= array();

		if( isset( $event_info->has_races ) ) {
			if ( false === ( $event_array = get_transient( 'event_array_contents-'.$qt_lang['lang'] ) ) ) {
				$event_array = rnr3_get_distances( $qt_lang['lang'], $market );
			}

			// pop off the last item in the array ( count )
			$pop	= end( $event_array );
			$count	= $pop['count'];
		}


		if( $count != 0 ) { ?>
			<section id="races">

				<?php // counts for grid layout
					if( $count == 1 ) {
						$grid = 1;
					} elseif( $count == 2 ) {
						$grid = 2;
					} elseif( $count == 3 ) {
						$grid = 3;
					} elseif( $count >= 4 ) {
						$grid = 4;
					}
				?>

				<section class="wrapper grid_<?php echo $grid; ?>">
					<?php
						if( $count > 0 ) {
							foreach( $event_array as $key => $value ) {

								if( isset( $value['active'] ) && $value['active'] == 'on' && !in_array( $value['label'], array( 'kidsruns', 'grandparents' ) ) ) {

									if( $qt_lang['enabled'] == 1 ) {
										$blurb_array						= qtrans_split( get_post_meta( $post->ID, '_rnr3_'.$value['label'].'_blurb', TRUE ) );
										${$value['label'] . '_blurb'}		= $blurb_array[$qt_lang['lang']];

										$link_txt_array						= qtrans_split( get_post_meta( $post->ID, '_rnr3_'.$value['label'].'_link_txt', TRUE ) );
										${$value['label'] . '_link_txt'}	= $link_txt_array[$qt_lang['lang']];
										if( get_post_meta( $post->ID, '_rnr3_'.$value['label'].'_link_url', TRUE ) != '' ) {
											${$value['label'] . '_link_url'}	= qtrans_convertURL( get_post_meta( $post->ID, '_rnr3_'.$value['label'].'_link_url', TRUE ) );
										} else {
										${$value['label'] . '_link_url'}	= '';
										}
									} else {
										${$value['label'] . '_blurb'}		= get_post_meta( $post->ID, '_rnr3_'.$value['label'].'_blurb', TRUE );
										${$value['label'] . '_link_txt'}	= get_post_meta( $post->ID, '_rnr3_'.$value['label'].'_link_txt', TRUE );
										${$value['label'] . '_link_url'}	= get_post_meta( $post->ID, '_rnr3_'.$value['label'].'_link_url', TRUE );
									}

									echo '<div class="column">
										<div class="racebadge">'.$value['circle'].'</div>';

										if( ${$value['label'] . '_blurb'} != '' ) {
											echo '<p>'.${$value['label'] . '_blurb'}.'</p>';
										}

										if( ${$value['label'] . '_link_url'} != '' ) {
											echo '<p><a href="'.${$value['label'] . '_link_url'}.'">'.${$value['label'] . '_link_txt'}.'</a></p>';
										}

									echo '</div>';
								}

							}
						}
					?>

				</section>
			</section>

		<?php }
	?>

	<!-- big red promo boxes -->
	<?php if( $event_info->big_red_boxes == 1 ) {
		rnr3_get_big_red_boxes( $market );
	}

	// echo '<!-- three featured articles -->';
	// rnr3_get_three_promos();

	// interstitial driver
	get_global_overlay( $qt_lang );

	if( $market == 'bonus-track' ) {
		get_footer( 'series' );
	} else {
		get_footer();
	}
?>
