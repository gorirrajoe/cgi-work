<?php
	/* Template Name: Charity */
	get_header();

	$parent_slug = the_parent_slug();
	rnr3_get_secondary_nav( $parent_slug );

	$market = get_market2();
	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';
	$prefix = '_rnr3_';

	$sfc_option	= get_post_meta( get_the_ID(), $prefix . 'sfc_option', 1 );
	$sfc		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'sfc_content', 1 ) );
	$efc_count	= get_post_meta( get_the_ID(), $prefix . 'efc_count', 1 );
	$efc1		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'efc_content_1', 1 ) );
	$efc2		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'efc_content_2', 1 ) );
	$fg			= get_post_meta( get_the_ID(), $prefix . 'fc_group', 1 );
	$become		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'become', 1 ) );
?>

<!-- main content -->
<main role="main" id="main">
	<div id="nav-anchor"></div>
	<section class="wrapper grid_2 offset240left">
		<div class="column sidenav stickem">
			<nav class="sticky-nav">
				<ul>
					<?php if( isset( $sfc_option ) && $sfc_option == 'on' ) {
							echo '<li><a href="#sfc">Series Featured Charity</a></li>';
					}
					if( isset( $efc_count ) && $efc_count != 0 ) {
							echo '<li><a href="#efc">Event Featured Charity</a></li>';
					}
					if( count( $fg ) > 0 && array_key_exists( 'name', $fg[0] ) ) {
							echo '<li><a href="#fg">Fundraising Groups</a></li>';
					} ?>
					<li><a href="#become">Become a Charity</a></li>
				</ul>
			</nav>
		</div>

		<div class="column">
			<div class="content">
				<?php
					/**
					 * intro blurb
					 */
					if (have_posts()) : while (have_posts()) : the_post();
						echo '<h2>'. get_the_title(). '</h2>';
						the_content();
					endwhile; endif;

					/**
					 * series featured charity
					 */
					if( isset( $sfc_option ) && $sfc_option == 'on' ) {
						echo '<section id="sfc">
							<h2>Series Featured Charity</h2>
							'. $sfc .'
						</section>';
					}

					/**
					 * event featured charity
					 */
					if( $efc_count > 0 ) {
						echo '<section id="efc">
							<h2>Event Featured Charity</h2>
							<div class="grid_'. $efc_count .'_special">';
								for( $i = 1; $i <= $efc_count; $i++ ) {
									echo '<div class="column_special">
										'. ${'efc' . $i} .'
									</div>';
								}
							echo '</div>
						</section>';
					}

					/**
					 * fundraising groups
					 */
					if( count( $fg ) > 0 && array_key_exists( 'name', $fg[0] ) ) {
						echo '<section id="fg">
							<h2>Fundraising Groups</h2>';

							foreach( $fg as $key => $entry ) {
								$charity_name	= $entry['name'];
								$charity_desc	= array_key_exists( 'description', $entry ) ? apply_filters( 'the_content', $entry['description'] ) : '';
								$charity_info	= array_key_exists( 'contact_info', $entry ) ? $entry['contact_info'] : '';
								$charity_email	= array_key_exists( 'contact_email', $entry ) ? $entry['contact_email'] : '';
								$charity_url	= array_key_exists( 'url', $entry ) ? $entry['url'] : '';
								$charity_logo	= array_key_exists( 'logo', $entry ) ? $entry['logo'] : '';

								echo '<div class="grid_fg">
									<div class="column_fg_left">
										<a href="'. $charity_url .'" target="blank"><img src="'. $charity_logo .'"></a>
									</div>
									<div class="column_fg_right">
										<h3>'. $charity_name .'</h3>
										'. $charity_desc .'
										<div class="grid_2_special">
											<div class="column_special charity_bold">
												'. $charity_info .'<br>';

												if( $charity_email != '' ) {
													echo '<a href="mailto:'. $charity_email .'">'. $charity_email .'</a><br>';
												}
												if( $charity_url != '' ) {
													echo '<a href="'. $charity_url .'" target="blank">Website</a>';
												}

											echo '</div>';

											if( $charity_url != '' ) {
												echo '<div class="column_special charity_url">
													<a class="cta" href="'. $charity_url .'" target="_blank">Learn More</a>
												</div>';
											}

										echo '</div>

									</div>
								</div>';
							}
						echo '</section>';
					}

					/**
					 * become a charity
					 */
					echo '<section id="become">
						<h2>Become a Charity</h2>
						'. $become .'
					</section>';
				?>

			</div>
		</div>
	</section>
	<script>
		$(document).ready(function(){

				/**
				 * This part causes smooth scrolling using scrollto.js
				 * We target all a tags inside the nav, and apply the scrollto.js to it.
				 */
				$(".sticky-nav a, .backtotop").click(function(evn){
					evn.preventDefault();
					$('html,body').scrollTo(this.hash, this.hash);
				});

				/*$('main').stickem({
					container: '.offset240left',
				});*/

				/**
				 * This part handles the highlighting functionality.
				 * We use the scroll functionality again, some array creation and
				 * manipulation, class adding and class removing, and conditional testing
				 */
				var aChildren = $(".sticky-nav li").children(); // find the a children of the list items
				var aArray = []; // create the empty aArray
				for (var i=0; i < aChildren.length; i++) {
					var aChild = aChildren[i];
					var ahref = $(aChild).attr('href');
					aArray.push(ahref);
				} // this for loop fills the aArray with attribute href values

				$(window).scroll(function(){
					var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
					var windowHeight = $(window).height(); // get the height of the window
					var docHeight = $(document).height();

					for (var i=0; i < aArray.length; i++) {
						var theID = aArray[i];
						var divPos = $(theID).offset().top; // get the offset of the div from the top of page
						var divHeight = $(theID).height(); // get the height of the div in question
						if (windowPos >= (divPos - 1) && windowPos < (divPos + divHeight -1 )) {
							$("a[href='" + theID + "']").addClass("nav-active");
						} else {
							$("a[href='" + theID + "']").removeClass("nav-active");
						}
					}

					if(windowPos + windowHeight == docHeight) {
						if (!$(".sticky-nav li:last-child a").hasClass("nav-active")) {
							var navActiveCurrent = $(".nav-active").attr("href");
							$("a[href='" + navActiveCurrent + "']").removeClass("nav-active");
							$(".sticky-nav li:last-child a").addClass("nav-active");
						}
					}
				});
		});

	</script>
</main>

<?php get_footer(); ?>
