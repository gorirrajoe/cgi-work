<?php
	/* Template Name: VIP (Series) */

	get_header( 'special' );

	$vip_meta = get_meta( get_the_ID() );
	// print_r( $vip_meta );
?>

<main role="main" id="main" class="vip">

	<section class="wrapper">
		<div class="grid_2_special">

			<div class="column_special">
				<div class="video-container">
					<iframe width="1280" height="720" src="https://www.youtube.com/embed/<?php echo $vip_meta['_rnr3_vip_youtube_id'] ?>?rel=0" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>

			<div class="column_special">
				<h2><?php echo $vip_meta['_rnr3_vip_intro_right_title']; ?></h2>

				<div class="vip__selectgroup">
					<label><?php echo $vip_meta['_rnr3_pre_dd']; ?></label>

					<div class="vip__selectgroup_dd">

						<?php
							$vip_array = array_key_exists( '_rnr3_vip_dropdown', $vip_meta ) ? unserialize( $vip_meta['_rnr3_vip_dropdown'] ) : '';

							if( $vip_array != '' ) { ?>

								<select id="selectrace">
									<option value="#">-- Select a race --</option>

									<?php foreach( $vip_array as $vip ) {
										$vip_pieces	= explode( '::', $vip );
										$vip_url	= site_url( $vip_pieces[0] . '/the-weekend/vip/' );

										echo '<option value="'. $vip_url .'">'. $vip_pieces[1] .'</option>';
									} ?>

								</select>

								<script>
									$(function() {
										// bind change event to select
										$( '#selectrace' ).bind( 'change', function() {
											var url = $( this ).val(); // get selected value
											if( url ) { // require a URL
												window.location = url; // redirect
											}
											return false;
										});
									});
								</script>

							<?php }
						?>
					</div>
				</div>

				<?php echo apply_filters( 'the_content', $vip_meta['_rnr3_vip_intro_right'] ); ?>

			</div>
		</div>

		<?php
			if( array_key_exists( '_rnr3_amenities_txt', $vip_meta ) ) {

				echo '<div class="vip__amenities">' .
					apply_filters( 'the_content', $vip_meta['_rnr3_amenities_txt'] ) .'
				</div>';

			}

			if( array_key_exists( '_rnr3_testimonials_txt', $vip_meta ) ) {

				echo apply_filters( 'the_content', $vip_meta['_rnr3_testimonials_txt'] );

			}
		?>

	</section>
</main>

<?php get_footer('series'); ?>
