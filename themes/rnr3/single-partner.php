<?php
  if(get_current_blog_id() <= 1){
    get_header('special');
  } else {
    get_header();
  }
?>

  <main role="main" id="main">
    <section class="wrapper grid_2 offset240left">

      <?php get_sidebar(); ?>

      <div class="column">
        <div class="content">
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
              <h2><?php the_title(); ?></h2>

              <div class="entry">

                <?php the_content(); ?>

              </div>
            </div>


          <?php endwhile; else: ?>

            <p>Sorry, no posts matched your criteria.</p>

          <?php endif; ?>
        </div>

      </div>

  	</section>
  </main>

<?php get_footer('series'); ?>
