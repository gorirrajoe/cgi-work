<?php
	/**
	 * standard header used by most templates
	 */
	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';


	/**
	 * global variables
	 */
	$pinterest			= rnr_get_option( 'rnrgv_pinterest');
	$youtube			= rnr_get_option( 'rnrgv_youtube');
	$header_code		= rnr_get_option( 'rnrgv_header');
	$google_analytics	= rnr_get_option( 'rnrgv_google_analytics');
	$gtm				= rnr_get_option( 'rnrgv_gtm');
	$optimizely			= rnr_get_option( 'rnrgv_optimizely');

	$twenty_years_enabled		= ( rnr_get_option( 'rnrgv_20_enabled' ) == 'on' ) ? 'on' : 'off';

	$twenty_years_event_list	= ( rnr_get_option( 'rnrgv_20year_events' ) != '' ) ? rnr_get_option( 'rnrgv_20year_events' ) : '';
	$twenty_years_event			= strpos( $twenty_years_event_list, ',' ) ? explode( ',', $twenty_years_event_list ) : array( $twenty_years_event_list );

	$twenty_years_exception_list	= ( rnr_get_option( 'rnrgv_20year_exception' ) != '' ) ? rnr_get_option( 'rnrgv_20year_exception' ) : '';
	$twenty_years_exception_events	= strpos( $twenty_years_exception_list, ',' ) ? explode( ',', $twenty_years_exception_list ) : array( $twenty_years_exception_list );

	$twenty_years_logo_square = ( rnr_get_option( 'rnrgv_20year_logo_square' ) != '' ) ? rnr_get_option( 'rnrgv_20year_logo_square' ) : '';

	$tempo_enabled = ( get_post_meta( get_the_id(), '_rnr3_tempo_enabled', true ) == 'on' ) ? true : false;

	$hero_small_txt_1 = ( isset($event_info->special_hero_small_txt) && $event_info->special_hero_small_txt != '' ) ? $event_info->special_hero_small_txt : '';
	$hero_small_txt_2  = ( isset($event_info->special_hero_small_txt_2) && $event_info->special_hero_small_txt_2 != '' ) ? $event_info->special_hero_small_txt_2 : '';

	// check if event has a custom instagram account
	if( isset($event_info->instagram_acct) && $event_info->instagram_acct != '' ) {
		$instagram = 'https://instagram.com/'. $event_info->instagram_acct;
	} else {
		$instagram = rnr_get_option( 'rnrgv_instagram');
	}

	// check if event has a custom twitter account
	if( isset($event_info->twitter_acct) && $event_info->twitter_acct != NULL ) {
		$twitter = 'https://twitter.com/'. $event_info->twitter_acct;
	} else {
		$twitter = rnr_get_option( 'rnrgv_twitter');
	}

	$page_location = rnr3_home_or_sub( $market, get_the_ID() );

	$travel_page_template = array( 'travel-v2.php', 'travel-flight.php', 'travel-pack.php', 'travel-transport.php', 'hotels-v2.php' );

	$onload_init = '';
	if( is_page_template( 'course-maps.php' ) ) {
		$onload_init = 'onload="initialize();"';
	}

?><!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
	<head>

		<?php if( $optimizely != '' ) {
			echo stripslashes( $optimizely );
		} ?>

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

		<?php rnr3_display_favicon();

		if( $google_analytics != '' ) { ?>
			<script type="text/javascript">
				var _gaq = _gaq || [];
				var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
				_gaq.push(
					['_require', 'inpage_linkid', pluginUrl],
					['_setAccount', '<?php echo $google_analytics;?>'],
					['_setDomainName', 'runrocknroll.com'],
					['_setAllowLinker', true],
					['_trackPageview']
				);
				(function() {
					var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
				})();
			</script>
		<?php }

		wp_head();

		if(isset($event_info->header_settings) && $event_info->header_settings != '') {
			echo $event_info->header_settings;
		}

		// setting big hero image background
		$header_image = '';

		// if you set a custom hero image for a page, use that
		if( is_page_template( $travel_page_template ) ) {

			// assumption that the travel page's slug is "travel"
			$travel_page_id	= get_id_by_slug( 'the-weekend/travel' );
			$header_image	= ( get_post_meta( $travel_page_id, '_rnr3_special_header_img', 1 ) != '' ) ? get_post_meta( $travel_page_id, '_rnr3_special_header_img', 1 ) : $event_info->header_image;

		} elseif( get_post_meta( get_the_ID(), '_rnr3_special_header_img', true) != '' ) {

			$header_image = get_post_meta( get_the_ID(), '_rnr3_special_header_img', true);

		} elseif( get_post_meta( get_the_ID(), '_rnr3_unique_header_img', 1 ) != '' ) {

			$header_image = get_post_meta( get_the_ID(), '_rnr3_unique_header_img', 1 );

		} else {

			$header_image = isset($event_info->header_image) ? $event_info->header_image: '';

		}


		/**
		 * mobile header image
		 * will appear for screen resolutions less than or equal to 640px
		 * currently, no per page specifics
		 */
		$mobile_header_image = ( !empty($event_info->mobile_header_image) ) ? $event_info->mobile_header_image : '';


		/**
		 * unique mobile header!
		 * first, check if a mobile header image is uploaded -- use regular header image if none added
		 * if a mobile header image is added, add a media query on the regular header image
		 */
		if( $mobile_header_image != '' || $header_image != '' ) { ?>

			<style>

				<?php if( $mobile_header_image != '' ) { ?>

					#hero, #new-hero {
						background: url("<?php echo $mobile_header_image; ?>") no-repeat scroll 50% 0 rgba(0, 0, 0, 0);
					}

				<?php } else { ?>

					#hero, #new-hero {
						background: url("<?php echo $header_image; ?>") no-repeat scroll 50% 0 rgba(0, 0, 0, 0);
					}

				<?php }

				if( $mobile_header_image != '' && $header_image != '' ) { ?>

					@media (min-width: 40em) {
						#hero, #new-hero {
							background: url("<?php echo $header_image; ?>") no-repeat scroll 50% 0 rgba(0, 0, 0, 0);
						}
					}

				<?php }

				if( is_front_page() && $event_info->youtube_video != '' ) { ?>
					#hero-video {
						background: linear-gradient(rgba(0,0,0,.8), rgba(0, 0, 0, 0.8)), url("<?php echo $header_image; ?>") no-repeat scroll 50% 0 ;
					}
				<?php } ?>

			</style>

		<?php }


		if( is_front_page() ) {
			$dfp_market = str_replace( '-', '_', $market ); ?>

			<script>
				(function() {
				var useSSL = 'https:' == document.location.protocol;
				var src = (useSSL ? 'https:' : 'http:') +
				'//www.googletagservices.com/tag/js/gpt.js';
				document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
				})();
			</script>

			<script>
				slot_600_600_1=googletag.defineSlot('/8221/runrocknroll/sm-<?php echo $dfp_market; ?>', [600, 600], 'div-600_600_1').addService(googletag.pubads()).setTargeting('tile', '1');
				slot_600_600_2=googletag.defineSlot('/8221/runrocknroll/sm-<?php echo $dfp_market; ?>', [600, 600], 'div-600_600_2').addService(googletag.pubads()).setTargeting('tile', '2');
				slot_600_600_3=googletag.defineSlot('/8221/runrocknroll/sm-<?php echo $dfp_market; ?>', [600, 600], 'div-600_600_3').addService(googletag.pubads()).setTargeting('tile', '3');
				slot_600_600_4=googletag.defineSlot('/8221/runrocknroll/sm-<?php echo $dfp_market; ?>', [600, 600], 'div-600_600_4').addService(googletag.pubads()).setTargeting('tile', '4');
				googletag.pubads().enableAsyncRendering();
				googletag.pubads().enableSingleRequest();
				googletag.enableServices();
			</script>
		<?php } ?>
		<meta name="twitter:widgets:theme" content="light" />
		<meta name="twitter:widgets:link-color" content="#ee1c24" />
		<meta name="twitter:widgets:border-color" content="#e5e5e5" />

	</head>


	<?php
		$body_array_class	= array();
		$body_array_class[]	= 'market-'. $market;
		$body_array_class[]	= ( $twenty_years_enabled == 'on' && !in_array( $market, $twenty_years_exception_events ) ) ? 'twenty_years' : '';
		$body_array_class[]	= ( in_array( $market, $twenty_years_event ) ) ? 'twenty_years_event' : '';
		$body_array_class[]	= $tempo_enabled ? 'tempo' : '';
	?>

	<body id="rnr-event" <?php body_class( $body_array_class ); ?> <?php echo $onload_init; ?>>

		<?php
			if( $gtm != '' ) {
				echo stripslashes( $gtm );
			}

			eu_regulation();
		?>

		<div id="container">

			<!-- alert banner -->
			<?php
				/**
				 * global alert banner
				 * managed through the global variables manager
				 * countdown clock can be toggled on/off
				 * exceptions for individual events (event slug) can be added
				 * messaging can be before or after set date
				 * translations are allowed using the language shortcode (ie [:es])
				 *
				 * will override the alert bar set in the event manager
				 */
				$gab_exceptions = strpos( rnr_get_option( 'rnrgv_gab_exceptions' ), ',' ) ? explode( ',', rnr_get_option( 'rnrgv_gab_exceptions' ) ) : array( rnr_get_option( 'rnrgv_gab_exceptions' ) );

				$current_date	= new DateTime( 'now', new DateTimeZone( 'America/Los_Angeles' ) );
				$gab_day		= date( 'Ymd', rnr_get_option( 'rnrgv_gab_day' ) );

				if( rnr_get_option( 'rnrgv_enable_gab_countdown' ) == 'on' && !in_array( $market, $gab_exceptions ) && $current_date->format( 'Ymd' ) <= $gab_day ) {

					$gab_afterday	= strtotime( '+1 day', rnr_get_option( 'rnrgv_gab_day' ) );
					$gab_afterday	= date( 'Ymd', $gab_afterday );

					if( $current_date->format( 'Ymd' ) < $gab_day ) {

						$phase = 'pre';

						if( $qt_lang['enabled'] == 1 ) {

							$gab_countdown_langs	= qtrans_split( rnr_get_option( 'rnrgv_gab_txt_pre' ) );
							$gab_countdown_txt		= $gab_countdown_langs[$qt_lang['lang']];

						} else {

							$gab_countdown_txt = rnr3_content_qtx_filter( rnr_get_option( 'rnrgv_gab_txt_pre' ) );

						}

					} elseif( $current_date->format( 'Ymd' ) >= $gab_day && $current_date->format( 'Ymd' ) < $gab_afterday ) {

						$phase = 'dayof';

						if( $qt_lang['enabled'] == 1 ) {

							$gab_countdown_langs	= qtrans_split( rnr_get_option( 'rnrgv_gab_txt_day' ) );
							$gab_countdown_txt		= $gab_countdown_langs[$qt_lang['lang']];

						} else {


							$gab_countdown_txt = rnr3_content_qtx_filter( rnr_get_option( 'rnrgv_gab_txt_day' ) );

						}

					}

					if( $current_date->format( 'Ymd' ) < $gab_afterday ) {

						echo '<section id="alert" class="grd_countdown">
							<section class="wrapper">
								<p class="mid2">'. $gab_countdown_txt . ' ' . rnr3_global_countdown_timer( $phase, $qt_lang ) .'</p>
							</section>
						</section>';

					}

				} elseif( isset( $event_info->alert_msg ) && $event_info->alert_msg != '' ) {

					if( $qt_lang['enabled'] == 1 ) {
						$alert_msg	= qtrans_split( $event_info->alert_msg );
						$alert_msg	= $alert_msg[$qt_lang['lang']];
					} else {
						$alert_msg	= $event_info->alert_msg;
					} ?>

					<section id="alert">
						<section class="wrapper">
							<p class="mid2"><?php echo $alert_msg; ?></p>
						</section>
					</section>

				<?php }
			?>


			<!-- begin main header -->
			<header>
				<section class="wrapper">
					<div id="getgoing" class="clearfix">

						<!-- countdown timer -->
						<?php rnr3_get_countdown_timer( $market, $qt_lang ); ?>


						<ul id="reg">
							<?php
								// make sure register link stays with the selected language (if multi-lang)
								if( $qt_lang['enabled'] == 1 ) {
									$register_link = qtrans_convertURL( site_url( '/register/' ) );
								} else {
									$register_link = site_url( '/register/' );
								}

								if( isset($event_info->reg_open) && $event_info->reg_open == 1 ) { // registration open!

									if ( false === ( $event_array = get_transient( 'event_array_contents' ) ) ) {
										$event_array = rnr3_get_distances( $qt_lang['lang'], $market );
									}
									$pop	= end( $event_array );
									$count	= $pop['count'];

									echo '<li>
										<a href="'. $register_link .'" class="register">'.$txt_register.'</a>';

										if( $count > 0 ) {

											echo '<ul>';
												foreach( $event_array as $key => $value ) {

													if( isset( $value['active'] ) && $value['active'] == 'on' ) {
														if( isset( $value['sold_out'] ) && $value['sold_out'] == 1 ) {
															echo '<li><a href="'. $register_link .'">'.$value['name'].' - Sold Out</a></li>';
														} else {
															echo '<li><a href="'. $register_link .'">'.$value['name'].'</a></li>';
														}
													}

												}
											echo '</ul>';

										}

									echo '</li>';

								} elseif( isset($event_info->reg_open) && $event_info->reg_open == 2 ) {	// remind me - get notified
									echo '<li><a class="register" href="'. $register_link .'">'.$txt_reg_notified.'</a></li>';
								} else {	// registration closed
									echo '<li><a class="register_closed" href="'. $register_link .'">'.$txt_reg_closed.'</a></li>';
								}
							?>
						</ul>

					</div>


					<?php
						if( $qt_lang['enabled'] == 1 ) {
							$eventhome_url = qtrans_convertURL( site_url() );
						} else {
							$eventhome_url = site_url();
						}


						/**
						 * get appropriate logo, 20 years or normal?
						 */
						get_logo( $twenty_years_enabled, $market, $twenty_years_exception_events );
					?>


					<nav>
						<div class="hamburger">
							<span></span>
							<span></span>
							<span></span>
							<span class="hamburger__label">Menu</span>
						</div>

						<?php
							$global_nav = rnr3_get_global_nav( $qt_lang['lang'] );
							echo $global_nav;

							$nav_settings = array(
								'menu'				=> 'primary-event',
								'container_id'		=> 'primary',
								'fallback_cb'		=> false,
								'theme_location'	=> '__no_such_location',
							);
							wp_nav_menu( $nav_settings );
						?>

					</nav>
				</section>


				<?php
					/**
					 * travel page exception
					 */
					if( is_page_template( $travel_page_template ) ) { ?>

						<div class="event_nav">
							<nav id="subnav">
								<section class="wrapper">

									<?php
										$url		= explode( '/', get_permalink() );
										$url_tail	= $url[ count($url) - 2];

										if( $url_tail == 'travel' ) {

											$current_class = 'current_travel_subpage ';

										} else {

											$current_class = '';

										}
									?>

									<a href="<?php echo site_url( '/the-weekend/travel/' ); ?>" class="<?php echo $current_class; ?>event_home_link"><?php echo $display_text['travel']; ?></a>

									<div class="subnav-menu-label">
										<div class="eventname_dropdown">
											<?php echo $display_text['travel']; ?>
										</div>
									</div>

									<div class="subnav_social">
										<section id="ribbon">

											<?php echo $event_info->twitter_hashtag != '' ? '<div class="hash">'.$event_info->twitter_hashtag . '  <span class="pop">/</span></div>': ''; ?>
											<div class="twitter"><a href="<?php echo $twitter; ?>" target="_blank"><span class="icon-twitter"></span></a></div>
											<div class="facebook"><a href="<?php echo $event_info->facebook_page; ?>" target="_blank"><span class="icon-facebook"></span></a></div>
											<div class="instagram"><a href="<?php echo $instagram; ?>" target="_blank"><span class="icon-instagram"></span></a></div>


										</section>
									</div>

									<?php
										$active_subpages		= array();
										if( $qt_lang['enabled'] == 1 ) {

											if( $qt_lang['lang'] == 'es' ) {

												$travel_subpage_array	= array(
													'fly::volar'			=> 'flights',
													'stay::hospedaje'		=> 'hotels',
													'pack::empacar'			=> 'pack',
													'transport::transporte'	=> 'transport'
												);

											} elseif( $qt_lang['lang'] == 'fr' ) {

												$travel_subpage_array	= array(
													'fly::vol'					=> 'flights',
													'stay::S&eacute;jour'		=> 'hotels',
													'pack::&Agrave; emporter'	=> 'pack',
													'transport::transport'		=> 'transport'
												);

											} else {

												$travel_subpage_array	= array(
													'fly::fly'				=> 'flights',
													'stay::stay'			=> 'hotels',
													'pack::pack'			=> 'pack',
													'transport::transport'	=> 'transport'
												);

											}

										} else {

											$travel_subpage_array	= array(
												'fly::fly'				=> 'flights',
												'stay::stay'			=> 'hotels',
												'pack::pack'			=> 'pack',
												'transport::transport'	=> 'transport'
											);

										}

										foreach( $travel_subpage_array as $key => $value ) {

											$english_key = explode( '::', $key);

											if( get_post_meta( $travel_page_id, '_rnr3_' . $english_key[0] . '_img', 1 ) != '' ) {

												$active_subpages[$key] = $value;

											}

										}

										if( !empty( $active_subpages ) ) {

											echo '<div id="event-subnav" class="subnav-menu-primary-event-container">
												<ul class="subnav-menu">';

													foreach( $active_subpages as $key => $value ) {

														$english_key = explode( '::', $key);

														if( $url_tail == $value ) {

															$current_class = ' class="current_travel_subpage"';

														} else {

															$current_class = '';

														}

														echo '<li><a'. $current_class .' href="'. get_permalink( $travel_page_id ) . $value .'/">'. $english_key[1] .'</a></li>';

													}

												echo '</ul>
											</div>';

										}
									?>

								</section>
							</nav>

						</div>

					<?php }
				?>

			</header>


			<?php
				$disable_hero = get_post_meta( get_the_ID(), '_rnr3_disable_hero', 1 );

				if( $disable_hero != 'on'  && !is_page_template( $travel_page_template ) ) {

					$mid_class = ( $twenty_years_enabled == 'off' || in_array( $market, $twenty_years_exception_events) ) ? 'mid' : ''; ?>

					<!-- big hero -->
					<section id="hero">

						<?php if( !in_array( $market, $twenty_years_event ) || !is_front_page() ) { ?>

							<section id="bigtext" class="wrapper <?php echo $mid_class; ?>" itemscope itemtype="http://schema.org/Event">

								<?php
									$location_array			= !empty($event_info->event_location) ? explode( ',', $event_info->event_location ) : false;
									$use_translated_event	= 0;

									if( $qt_lang['enabled'] == 1 && $location_array != false ) {
										$use_translated_event	= 1;
										$location_array			= qtrans_split( $location_array[0] );
									}

									$custom_post_type = get_post_type();

									/**
									 * set up for Rock Idol (Dublin) hero banner
									 */
									if( is_page_template( 'rock-idol.php' ) ) {

										$rock_idol_hero_logo	= get_post_meta( get_the_ID(), '_rnr3_hero_logo', true );
										$hero_small_txt			= $hero_huge_txt = ''; // defined as blank so file does not throw warnings/notices ?>

										<img class="rock-idol-logo" src="<?php echo $rock_idol_hero_logo; ?>">

									<?php } elseif( isset($event_info->special_hero_option) && $event_info->special_hero_option == 1 ) {

										/**
										 * event home: show blurb, then city
										 * event subpage: show city, then page group title
										 */
										// custom hero text
										$hero_small_txt	= '<span class="kicker bigtext-exempt">'. stripslashes( $hero_small_txt_1 ) .'</span>';
										$hero_huge_txt	= '<span class="headline">' . $event_info->special_hero_headline_txt . '</span>';
										echo '<meta itemprop="location" content="'. $event_info->event_location .'" />';

									} else {

										if( $page_location == 'event-home' || $page_location == 'event-post' || is_page_template( 'news-n-promos.php' ) ) {
											if( $event_info->event_sponsor_blurb == '' ) {
												$hero_small_txt = '<span class="kicker bigtext-exempt">Rock &lsquo;n&lsquo; Roll</span>';
											} else {
												$hero_small_txt = '<span class="kicker bigtext-exempt">' . stripslashes( $event_info->event_sponsor_blurb ) . '</span>';
											}
											if( $use_translated_event == 1 ) {
												$hero_huge_txt = '<span class="headline" itemprop="location">' . $location_array[$qt_lang['lang']] . '</span>';
											} else {
												$hero_huge_txt = '<span class="headline" itemprop="location">' . $location_array[0] . '</span>';
											}

											// if it's a custom post type single
											if( $custom_post_type != 'post' && $custom_post_type != 'page' ) {
												if( $qt_lang['enabled'] == 1 ) {
													$hero_small_txt = '<span class="kicker bigtext-exempt" itemprop="location">'. $location_array[$qt_lang['lang']] . '</span>';
													$cpt_obj = get_post_type_object( $custom_post_type );
													$hero_huge_txt = '<span class="headline">' . $cpt_obj->description . '</span>';
												} else {
													$hero_small_txt = '<span class="kicker bigtext-exempt" itemprop="location">'. $location_array[0] . '</span>';
													$cpt_obj = get_post_type_object( $custom_post_type );
													$hero_huge_txt = '<span class="headline">' . $cpt_obj->description . '</span>';
												}
											}

										} elseif( $page_location == 'event-sub' ) {
											// subpage
											if( $hero_small_txt_1 != '' && $hero_small_txt_2 !='' ) {
												$hero_small_txt = '<span class="kicker bigtext-exempt">'. stripslashes( $hero_small_txt_1 ) .'</span><span class="kicker bigtext-exempt">'. stripslashes( $hero_small_txt_2).'</span>';
											} else {
												if( $use_translated_event == 1 ) {
													$hero_small_txt = '<span class="kicker bigtext-exempt" itemprop="location">' . $location_array[$qt_lang['lang']] . '</span>';
												} else {
													$hero_small_txt = '<span class="kicker bigtext-exempt" itemprop="location">' . $location_array[0] . '</span>';
												}
											}

											if( is_post_type_archive() ) {
												$cpt_obj = get_post_type_object( $custom_post_type );
												$hero_huge_txt = '<span class="headline">' . $cpt_obj->description . '</span>';
											} elseif( is_page_template( 'register-page.php' ) || is_page_template( 'register-page-v2.php' ) || is_page( 'register' ) ) {
												// register page
												$hero_huge_txt = '<span class="headline">' . $txt_register . '</span>';
											} elseif( is_page_template( 'partners-series.php' ) || is_singular( 'partner' ) ) {
												// partner page
												$hero_huge_txt = '<span class="headline">' . $partners_txt . '</span>';
											} elseif( is_404() ) {
												// 404 page
												$hero_small_txt = '<span class="kicker bigtext-exempt">Rock &lsquo;n&lsquo; Roll</span>';
												if( $use_translated_event == 1 ) {
													$hero_huge_txt = '<span class="headline" itemprop="location">' . $location_array[$qt_lang['lang']] . '</span>';
												} else {
													$hero_huge_txt = '<span class="headline" itemprop="location">' . $location_array[0] . '</span>';
												}
											} elseif( is_category() ) {
												if( $qt_lang['enabled'] == 1 ) {
													$category_split = qtrans_split( single_cat_title('', false) );
													$hero_huge_txt = '<span class="headline">'. $category_split[$qt_lang['lang']] .'</span>';
												} else {
													$hero_huge_txt = '<span class="headline">' . single_cat_title('', false) . '</span>';
												}
											} elseif( is_tag() ) {
												if( $qt_lang['enabled'] == 1 ) {
													$tag_split = qtrans_split( single_tag_title( '', false ) );
													$hero_huge_txt = '<span class="headline">'. $tag_split[$qt_lang['lang']] .'</span>';
												} else {
													$hero_huge_txt = '<span class="headline">' .	single_tag_title( '', false ) . '</span>';
												}
											} elseif( $market == 'bonus-track' ) {
												$hero_huge_txt = '<span class="headline">' .	get_the_title( '', false ) . '</span>';
											} elseif( is_page_template( 'charity.php' ) ) {
												// translate?
												$hero_huge_txt = '<span class="headline">Charity</span>';
											} else {
												if( $qt_lang['enabled'] == 1 ) {
													$hero_huge_txt = qtrans_split( the_parent_title() );
													$hero_huge_txt = '<span class="headline">' . $hero_huge_txt[$qt_lang['lang']] . '</span>';
												} else {
													$title	= the_parent_title();
													$title	= $title != '' ? $title : get_the_title();
													$hero_huge_txt = '<span class="headline">' . $title . '</span>';
												}
											}
										}
									}

									$formatted_date = get_event_date( $qt_lang[ 'lang' ], $market );
								?>

								<meta itemprop="organizer" content="Rock 'n' Roll Marathon Series" />

								<?php if( isset($event_info->official_name) && $event_info->official_name != '' ) {
									echo '<meta itemprop="name" content="'. stripslashes( $event_info->official_name ) .'" />';
								} ?>

								<h1>
									<?php
										if($hero_small_txt_2 == ''){//if only one line of small text

											$lgthherosmall = strlen( $hero_small_txt );

											// Virginia Beach wraps for some reason trying to prevent it.
											if((strpos( $hero_small_txt, '>Virginia Beach<' ) !== FALSE) OR (strpos( $hero_small_txt, 'l Virginia Beach' ) !== FALSE)) {
												$found = 1;
												} else {
													$found = 0;
												}

											if( $lgthherosmall > 56 AND $found == 0 ) {
												if( $lgthherosmall > 95 ){
													//for Philly AMERICAN ASSOCIATION FOR CANCER RESEARCH ROCK 'N' ROLL
													echo wordwrap( $hero_small_txt, 80, '</span><span class="kicker bigtext-exempt">', false );
												} elseif( $lgthherosmall > 85 AND $lgthherosmall < 95 ) {
													//for UNITED AIRLINES ROCK 'N' ROLL SAN FRANCISCO
													echo wordwrap( $hero_small_txt, 65, '</span><span class="kicker bigtext-exempt">', false );
												} else {
													echo wordwrap( $hero_small_txt, 79, '</span><span class="kicker bigtext-exempt">', false );
												}
											}
									} else {
										echo $hero_small_txt;
									}
									echo $hero_huge_txt;


									if( isset($event_info->publish) && $event_info->publish == 1 && !is_page_template( 'rock-idol.php' ) ) { ?>
										<span class="subhead bigtext-exempt" itemprop="startDate" datetime="<?php echo $formatted_date['year'] .'-'. $formatted_date['month'] .'-'. $formatted_date['day']; ?>">

											<?php if( $event_info->festival == 1 ) {
												echo '<meta itemprop="endDate" content="'. $formatted_date['year'] .'-'. $formatted_date['month'] .'-'. $formatted_date['event_day2'] .'" />';
											} ?>

											<strong>
												<?php if( $qt_lang[ 'lang' ] == 'en' ) {
													echo $formatted_date[ 'mmmdd' ];
												} else {
													echo $formatted_date[ 'monthdateeuro' ];
												} ?>
											</strong>

											<?php if( ( $qt_lang['lang'] != 'fr' ) && ( $formatted_date[ 'mmmdd' ] != 'TBD' ) ) {
												// french do not like slashes in their dates!
												echo ' / ';
											}

											echo $formatted_date[ 'year' ]; ?>
										</span>
									<?php } ?>
								</h1>

								<?php if( is_front_page() && $event_info->youtube_video != '' ) {
									echo '<div class="play bigtext-exempt">
										<a class="big_video" href="javascript:;" onClick="toggleVideo();"><span class="icon-play"></span></a>
									</div>';

								} ?>

							</section>

						<?php } elseif( in_array( $market, $twenty_years_event ) && is_front_page() ) {

							echo '<section class="wrapper" itemscope itemtype="http://schema.org/Event">';

								if( $twenty_years_logo_square != '' ) {

									echo '<div class="twenty_years__logo bigtext-exempt">
										<img src="'. $twenty_years_logo_square .'">
									</div>';

								}

								$location_array			= explode( ',', $event_info->event_location );
								$use_translated_event	= 0;

								if( $qt_lang['enabled'] == 1 && $location_array != false ) {
									$use_translated_event	= 1;
									$location_array			= qtrans_split( $location_array[0] );
								}

								$custom_post_type = get_post_type();

								if( $event_info->special_hero_option == 1 ) {

									/**
									 * event home: show blurb, then city
									 * event subpage: show city, then page group title
									 */
									// custom hero text
									$hero_small_txt = '<span class="kicker bigtext-exempt">'. stripslashes( $event_info->special_hero_small_txt ) .'</span>';
									$hero_huge_txt = '<span class="headline">' . $event_info->special_hero_headline_txt . '</span>';
									echo '<meta itemprop="location" content="'. $event_info->event_location .'" />';

								} else {

									if( $event_info->event_sponsor_blurb == '' ) {
										$hero_small_txt = '<span class="kicker bigtext-exempt">Rock &lsquo;n&lsquo; Roll</span>';
									} else {
										$hero_small_txt = '<span class="kicker bigtext-exempt">' . stripslashes( $event_info->event_sponsor_blurb ) . '</span>';
									}
									if( $use_translated_event == 1 ) {
										$hero_huge_txt = '<span class="headline" itemprop="location">' . $location_array[$qt_lang['lang']] . '</span>';
									} else {
										$hero_huge_txt = '<span class="headline" itemprop="location">' . $location_array[0] . '</span>';
									}

								}

								$formatted_date = get_event_date( $qt_lang[ 'lang' ], $market ); ?>

								<meta itemprop="organizer" content="Rock 'n' Roll Marathon Series" />

								<?php if( $event_info->official_name != '' ) {
									echo '<meta itemprop="name" content="'. stripslashes( $event_info->official_name ) .'" />';
								} ?>



								<?php echo '<section id="bigtext">
									<h1>';

										$lgthherosmall = strlen( $hero_small_txt );

										// Virginia Beach wraps for some reason trying to prevent it.
										if(strpos( $hero_small_txt, '>Virginia Beach<' ) !== FALSE ) {
											$found = 1;
										} else {
											$found = 0;
										}

										if( $lgthherosmall > 56 AND $found == 0 ) {
											if( $lgthherosmall > 76 AND $lgthherosmall < 85 ) {
												echo wordwrap( $hero_small_txt, 65, '</span><span class="kicker bigtext-exempt">', false );
											} else {
												echo wordwrap( $hero_small_txt, 79, '</span><span class="kicker bigtext-exempt">', false );
											}
										} else {
											echo $hero_small_txt;
										}

										echo $hero_huge_txt;


										if( $event_info->publish == 1 ) { ?>

											<span class="subhead bigtext-exempt" itemprop="startDate" datetime="<?php echo $formatted_date['year'] .'-'. $formatted_date['month'] .'-'. $formatted_date['day']; ?>">

												<?php if( $event_info->festival == 1 ) {
													echo '<meta itemprop="endDate" content="'. $formatted_date['year'] .'-'. $formatted_date['month'] .'-'. $formatted_date['event_day2'] .'" />';
												} ?>

												<strong>
													<?php if( $qt_lang[ 'lang' ] == 'en' ) {
														echo $formatted_date[ 'mmmdd' ];
													} else {
														echo $formatted_date[ 'monthdateeuro' ];
													} ?>
												</strong>

												<?php if( ( $qt_lang['lang'] != 'fr' ) && ( $formatted_date[ 'mmmdd' ] != 'TBD' ) ) {
													// french do not like slashes in their dates!
													echo ' / ';
												}

												echo $formatted_date[ 'year' ]; ?>
											</span>

										<?php } ?>
									</h1>


									<?php if( is_front_page() && $event_info->youtube_video != '' ) {
										echo '<div class="play bigtext-exempt">
											<a class="big_video" href="javascript:;" onClick="toggleVideo();"><span class="icon-play"></span></a>
										</div>';

									}

								echo '</section>
							</section>';

						} ?>

					</section>


					<?php if( is_front_page() && $event_info->youtube_video != '' ) {
						echo '<section id="hero-video">
							<div id="popupVid" class="hero-video-wrapper">
								<a class="big_video_close" href="javascript:;" onClick="toggleVideo(\'hide\');"><span class="icon-cancel"></span></a>
								<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'. $event_info->youtube_video .'?enablejsapi=1&rel=0&showinfo=0&modestbranding=1&controls=0" frameborder="0" allowfullscreen></iframe>
							</div>
						</section>';
					}

				}



				/**
				 * travel page exception
				 */
				if( is_page_template( $travel_page_template ) ) { ?>

					<section id="new-hero-container">
						<section id="new-hero">
							<section class="wrapper bigtext" itemscope itemtype="http://schema.org/Event">

								<?php
									$hero_small_txt		= get_post_meta( $travel_page_id, '_rnr3_small_text', true );
									$hero_huge_txt		= get_post_meta( $travel_page_id, '_rnr3_huge_text', true );
									$hero_subline_txt	= get_post_meta( $travel_page_id, '_rnr3_sub_line', true );
								?>

								<meta itemprop="organizer" content="Rock 'n' Roll Marathon Series" />

								<div class="hero-copy">
									<?php
										echo '
											<span class="kicker bigtext-exempt">'. $hero_small_txt .'</span>
											<span class="hero_huge_txt">'. $hero_huge_txt .'</span>
											<span class="subhead bigtext-exempt">'. $hero_subline_txt .'</span>
										';
									?>
								</div> <!-- END .HERO-COPY -->

							</section>
						</section>
					</section>

				<?php }


				/**
				 * social ribbon
				 * only on event home pages
				 */
				if( is_front_page() ) { ?>

					<section id="ribbon">
						<section class="wrapper mid">

							<div class="hash"><?php echo strtoupper($event_info->twitter_hashtag); ?> <span class="pop">/</span></div>
							<div class="twitter"><a href="<?php echo $twitter; ?>" target="_blank"><span class="icon-twitter"></span></a></div>
							<div class="facebook"><a href="<?php echo $event_info->facebook_page; ?>" target="_blank"><span class="icon-facebook"></span></a></div>
							<div class="instagram"><a href="<?php echo $instagram; ?>" target="_blank"><span class="icon-instagram"></span></a></div>

							<?php
								if( !in_array( get_current_blog_id() , [1, 41, 37, 42, 38] ) ){
									if( $tempo_enabled  == true ) {

										echo '<div class="schedule">
											<a href="'. get_site_url( get_current_blog_id() ) .'/the-weekend/schedule-of-events"><span class="icon-schedule_icon"></span>SCHEDULE</a>
										</div>';
									}
								}
							?>
						</section>
					</section>

				<?php } ?>
