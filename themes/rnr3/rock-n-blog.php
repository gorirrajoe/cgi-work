<?php
/* Template Name: Rock 'N' Blog*/
get_header('oneoffs'); ?>
  <style>
    .group {
      padding:60px 0 30px;
    }
    .rnbround .attachment-rnb-ambassador-thumb {
      border-radius:50%;
      margin-bottom:30px;
    }

  </style>
  <main role="main" id="main">
    <section class="wrapper group">
      <div class="grid_2">
        <div class="column">
          <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 	
             the_content();
           endwhile; endif; ?>
         </div>
        <div class="column">
          <img src="<?php echo get_template_directory_uri(); ?>/img/rnb/rnb-hero.jpg">
        </div>
      </div>
    </section>
    <section class="wrapper group center">
      <h2>About Us</h2>
      <div class="grid_2">
        <div class="column">
          <?php echo get_post_meta(get_the_ID(), '_rnb_ambo_join_text', TRUE); ?>
        </div>
        <div class="column">
          <?php echo get_post_meta(get_the_ID(), '_rnb_ambo_meet_text', TRUE); ?>
        </div>
      </div>
    </section>
    <section id="apply" class="wrapper group center">
      <h2>Get on the Wait-List!</h2>
      <div class="aligncenter">
        <iframe src="http://www.surveygizmo.com/s3/1766748/Rock-N-Blogger-Waiting-List-2015-2016 " frameborder="0" width="400" height="600" style="display:table; margin:auto;"></iframe>
      </div>
    </section>
    <section class="wrapper group center">
      <h2>Rock 'n' Blog Team</h2>
      <p>Thank you to everyone who applied for the 2015 team!</p>
      <?php
        $query = 'SELECT * from wp_posts where post_type = "rock-n-blog" AND post_status = "publish" ORDER BY post_date DESC limit 8';
        $entries = $wpdb->get_results( $query );
        $count = 0;
        echo '<div class="grid_4">';
          foreach( $entries as $entry ) {
            /*
            if( ( $count % 4 ) == 0 ) {
              echo '</div>
              <div class="grid_4">';
            }
            */
            $ambo_market = get_post_meta( $entry->ID, '_ambo_event_readable', true );
            echo '
              <div class="column">
                <div class="rnbround"><a href="'. post_permalink( $entry->ID ) .'">'.get_the_post_thumbnail( $entry->ID, 'rnb-ambassador-thumb' ).'</a></div>
                <p><a href="'. post_permalink( $entry->ID ) .'">'.$entry->post_title.'</a></p>
              </div>
            ';
            $count++;

          }
        echo '</div>
        <div style="margin-bottom:30px; text-align:center;"><a class="cta" href="'.site_url('/rock-n-blog/').'">View All</a></div>';

      ?>
    </section>
    <section class="wrapper group center">
      <h2>#RocknBlog Tweets </h2>
      <div>
        <a class="twitter-timeline" href="https://twitter.com/hashtag/RocknBlog" data-widget-id="503884630991183873"></a> 
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
      </div>
    </section>
    <section class="wrapper group">
      <h2>FAQ's</h2>
      <div class="accordion">
        <div class="accordion-section" style="position: relative;">
          <div class="accordion-section-title" data-reveal="#rnb1">How can I apply to be a Rock 'n' Blog member??</div>
          <div class="accordion-section-content" id="rnb1">
            <p>Applications are now closed for the 2015 team, but you can sign up for our waiting list or a notification for 2016 applications above! </p>
          </div>
        </div>
        <div class="accordion-section" style="position: relative;">
          <div class="accordion-section-title" data-reveal="#rnb2">Do I have to be a professional athlete to apply to be part of the Rock 'n' Blog?</div>
          <div class="accordion-section-content" id="rnb2">
            <p>Nope. The only thing you need to apply to be a Rock 'n' Blog member is an enthusiasm for health & fitness, and to want to put it all over the Internet! (eagerness to run also encouraged) </p>
          </div>
        </div>
        <div class="accordion-section" style="position: relative;">
          <div class="accordion-section-title" data-reveal="#rnb3">Do I have to have a blog to apply for the Rock 'n' Blog Program?</div>
          <div class="accordion-section-content" id="rnb3">
            <p>We accept submissions from anyone who is active on at least one of the following social channels: blogs, YouTube, Instagram and Twitter. You will be asked to provide the URL or handle to your channel and subscribers and/or followers.</p>
          </div>
        </div>
        <div class="accordion-section" style="position: relative;">
          <div class="accordion-section-title" data-reveal="#rnb4">What will I receive as a member of Rock 'n' Blog?</div>
          <div class="accordion-section-content" id="rnb4">
            <p>As a Rock 'n' Blog member, you will receive the following:</p>
            <ul>
              <li>A Rock 'n' Roll Tour Pass 3-Pack</li>
              <li>Discount code for your audience</li>
              <li>Inside scoop on Rock 'n' Roll news</li>
              <li>Exclusive Rock 'n' Blog digital badge and swag</li>
              <li>Access to a private Facebook group to connect with other Rock 'n' Blog members</li>
            </ul>
          </div>
        </div>
        <div class="accordion-section" style="position: relative;">
          <div class="accordion-section-title" data-reveal="#rnb5">What are my duties as a Rock 'n' Blog member?</div>
          <div class="accordion-section-content" id="rnb5">
            <p>As a member of the Rock 'n' Blog team, we really just want to hang out with you on the Internet, on race weekend, and any other time you are as pumped about running as we are! But we do ask you to do a few things for us, which include but aren't limited to:</p>
            <ul>
              <li>Posting the Rock 'n' Blog digital badge to your social channels</li>
              <li>Publishing #RocknBlog in your social channel bios</li>
              <li>At least one post a month mentioning Rock 'n' Roll whether it's training, race reviews, or just how pumped you are to ROCK! </li>
            </ul>
          </div>
        </div>
        <div class="accordion-section" style="position: relative;">
          <div class="accordion-section-title" data-reveal="#rnb6">How long does a membership as a Rock 'n' Blog member last?</div>
          <div class="accordion-section-content" id="rnb6">
            <p>One year. Applications are accepted once a year and members are a part of the same community for one year. </p>
          </div>
        </div>
        <div class="accordion-section" style="position: relative;">
          <div class="accordion-section-title" data-reveal="#rnb7">Does Rock 'n' Blog offer monetary or full product sponsorships?</div>
          <div class="accordion-section-content" id="rnb7">
            <p>At this time no, Rock 'n' Blog does not offer monetary or full product sponsorships. But we will offer unlimited high fives. </p>
          </div>
        </div>
      </div>
    </section>

  </main>

<?php get_footer( 'oneoffs' ); ?>