<?php
	/*
		Template Name: City Page
		Template Post Type: post, page
	 */

	$current_blog_details	= get_blog_details();
	$current_blog_path		= $current_blog_details->path;

	$currently_in_tempo		= strpos( $current_blog_path, 'tempo' ) ? true : false;

	if ( $currently_in_tempo ) {
		get_header('oneoffs');
	} else {
		get_header();
	}

	// check language and get our languages
	$qt_lang = rnr3_get_language();
	include 'languages.php';
?>
<!-- main content -->
	<main role="main" id="main">

		<section class="wrapper grid_1">

			<div class="content">
				<?php echo $currently_in_tempo ? get_social_sharing() : ''; ?>
				<?php
					if (have_posts()) : while (have_posts()) : the_post();
						the_content();
					endwhile; endif;
				?>
			</div>

		</section>

	</main>

	<main role="main" id="rockrollinfo">

		<?php
			//include( ABSPATH . 'wp-content/rnrinfo/san-diego/index.php');
			include( ABSPATH . 'wp-content/rnrinfo/san-diego/body.php');
		?>

	</main>

	<br><br>

	<p style="text-align: center;"><a class="cta cta_big cta_shortcode" href="http://www.runrocknroll.com/san-diego/register/" target="_blank">REGISTER</a></p>

	<div style="margin:0 auto; text-align:center; clear:both;" id="info-embed">
		<p style="font-family: Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold;">Post this on your site (Embed Code):</p>
		<p style="font-family: Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold;">Disclosure: Due to the set up of this page, we do not guarantee the same display as on our site. For best results use a minimum 1200px viewpoint.</p>
		<input type="text" style="overflow:hidden; width:45%;" onclick="javascript:this.focus();this.select();" value="<iframe href='http://www.runrocknroll.com/wp-content/rnrinfo/san-diego/index.php' height='100%'' width='100%'></iframe>">
	</div>

	<?php if ( $currently_in_tempo ) : ?>
	<main role="main">

		<div class="wrapper">

			<section class="article-footer">

				<?php
					// The Related Posts
					$post_id			= get_the_ID();
					$post_tags			= get_the_tags( $post_id );

					if ( !empty( $post_tags ) ) :

						$default_image_url		= get_option( 'default-featured-image' );
						$image_data				= $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE guid='%s';", $default_image_url ) );
						$default_image_thumb	= wp_get_attachment_image( $image_data[0] );

						$post_tags_slugs		= wp_list_pluck( $post_tags, 'slug' );

						$args = array(
							'tag'						=> $post_tags_slugs,
							'posts_per_page'			=> 3,
							'post__not_in'				=> array( $post_id ),
							'ignore_sticky_posts'		=> true,
							'no_found_rows'				=> true,
							'update_post_meta_cache'	=> false,
							'update_post_term_cache'	=> false,
						);

						$related_query = new WP_Query( $args ); ?>


						<section class="article__related more-stories">

							<h3><?php echo $related_stories_txt; ?></h3>

							<?php if( $related_query->have_posts() ) : while( $related_query->have_posts() ): $related_query->the_post(); ?>

							<article class="article article_type_related">

								<a href="<?php the_permalink(); ?>" class="article__permalink">

									<figure class="article__thumbnail">
										<?php if ( has_post_thumbnail() ) { ?>

											<?php the_post_thumbnail( 'thumbnail' ); ?>

										<?php } else {
											echo $default_image_thumb;
										} ?>
									</figure>

									<div class="article__details">
										<?php the_title( '<h3 class="article__title">', '</h3>' ); ?>
										<?php echo ( has_excerpt() ) ? '<h5 class="article__subheading">'. $post->post_excerpt .'</h5>' : '' ?>
										<div class="article__meta"><span class="author"><?php echo $by_txt .' '; the_author(); ?></span></div>
									</div>

								</a>

							</article>

							<?php endwhile; endif; ?>

						</section>

						<?php
					endif;
				?>

			</section><!-- END ARTICLE-FOOTER -->

		</div>

	</main>
	<?php endif; ?>

<?php
	if ( $currently_in_tempo ) {
		get_footer('series');
	} else {
		get_footer();
	}
?>
