<?php
	/* Template Name: Past Results */
	get_header();

	$parent_slug = the_parent_slug();
	rnr3_get_secondary_nav( $parent_slug );
	$qt_lang = rnr3_get_language();

	if ( false === ( $pastresults_query = get_transient( 'pastresults_query_results' ) ) ) {
		$args = array(
			'post_type'			=> 'past-results',
			'meta_key'			=> '_rnr3_pastresult_date',
			'orderby'			=> 'meta_value_num',
			'order'				=> 'DESC',
			'posts_per_page'	=> -1
		);
		$pastresults_query = new WP_Query( $args );
		set_transient( 'pastresults_query_results', $pastresults_query, 5 * MINUTE_IN_SECONDS );
	}

?>

	<!-- main content -->
	<main role="main" id="main">
		<section class="wrapper center grid_1">
			<?php
				if (have_posts()) : while (have_posts()) : the_post();
					echo '<h2>'. get_the_title(). '</h2>';

					the_content();
				endwhile; endif;

				if ($pastresults_query->have_posts()) {
					$gallery_count = 1;

					echo '<div id="results">';
						while ($pastresults_query->have_posts()) {
							$pastresults_query->the_post();

							$year = date( 'Y', get_post_meta( get_the_ID(), '_rnr3_pastresult_date', TRUE ) );

							if( $qt_lang['enabled'] != 1 ) {
								$url = get_post_meta( get_the_ID(), '_rnr3_pastresult_url', TRUE );
							} else {
								$url = qtrans_convertURL( get_post_meta( get_the_ID(), '_rnr3_pastresult_url', TRUE ) );
							}

							echo '<div class="result">
								<div class="inner_result">
									<h2>'. $year;
										$resulttitle = get_the_title();

										if(stripos($resulttitle, "jr") >= 1 OR stripos($resulttitle, "junior") >= 1){
											echo " - JUNIOR";
										}
									echo '</h2>
									<p><a class="cta" href="'. $url .'" target="_blank">View Results</a></p>';

									$entries = get_post_meta( get_the_ID(), '_rnr3_repeat_group', true );

									if( $entries != '' ) {
										echo '<a class="expand">Photo Galleries</a>

										<div class="collapse">
											<div id="gallery-carousel-'.$gallery_count.'" class="owl-carousel">';

												foreach( $entries as $key => $entry ) {
													$image_id	= $entry['image_id'];
													$image		= wp_get_attachment_image_src( $image_id, 'past-galleries' );

													echo '<div><a href="'. $entry['url'] .'" target="_blank"><img src="'. $image[0] .'"></a><br>
													<a href="'. $entry['url'] .'" target="_blank">'. $entry['title'] . '</a></div>';

												}

											echo '</div>
										</div>';
									}
									$gallery_count++;

									edit_post_link( 'edit', '<p class="post-edit">', '</p>', get_the_ID() );

								echo '</div>
							</div>';
						}
					echo '</div>';

				} else {

					echo 'No results added yet.';

				}
				wp_reset_postdata();

			?>
		</section>

	</main>

<script>
	jQuery(document).ready(function($) {
		<?php for( $i = 1; $i <= $gallery_count; $i++ ) { ?>
			$('#gallery-carousel-<?php echo $i; ?>').owlCarousel({
				singleItem : true,
				autoPlay : false,
				navigation : true,
				navigationText : ["prev", "next"],
				pagination : true,
				lazyLoad : true
			});
		<?php } ?>
	});
</script>
<?php get_footer(); ?>
