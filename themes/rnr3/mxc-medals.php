<?php
	/* Template Name: MXC 15/16 Medals Form */
	get_header('special');

	$market = get_market2();
	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang	= rnr3_get_language();
	$qt_status	= $qt_lang['enabled'] == 1 ? true: false;
	include 'languages.php';

	if( $qt_status ) {
		if( $qt_lang['lang'] == 'es' ) {
			$months			= array("enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");
			$select_txt		= 'Selecciona';
			$birthdate_txt	= 'Fecha de Nacimiento';
			$zipcode_txt	= 'C&oacute;digo Postal';
			$country_txt	= 'Pa&iacute;s';
			$email_txt		= 'Correo Electr&oacute;nico';
			$search_again	= 'Busca de Nuevo';
		} else {
			$months			= array("january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december");
			$select_txt		= 'Select';
			$birthdate_txt	= 'Birth Date';
			$zipcode_txt	= 'Zipcode';
			$country_txt	= 'Country';
			$email_txt		= 'Email Address';
			$search_again	= 'Search Again';
		}
	}
	// medals database
	$wpdb->mxc_medals = 'mxc_medals';
	// form values
	$month_value	= 0;
	$year_min		= 1920;
	$year_max		= 2010;

	// form messages
	$page_id				= get_the_ID();
	$message_no_runners		= get_post_meta( $page_id, '_mxc_medals_no_runner', true );
	$message_flagged		= get_post_meta( $page_id, '_mxc_medals_previously_submitted', true );
	$message_confirmation	= get_post_meta( $page_id, '_mxc_medals_confirmation', true );
	$message_form			= get_post_meta( $page_id, '_mxc_medals_shipping_data', true );
	$message_sg_form		= get_post_meta( $page_id, '_mxc_medals_sg_form', true );
?>
	<style>
		form{
			margin-bottom: 60px;
		}
		#new_address textarea,
		#new_address input[type="text"]{
			width: 100%;
		}
	</style>

	<!-- main content -->
	<main role="main" id="main">
		<section class="wrapper grid_1 center">
			<?php
				if( have_posts() ): while ( have_posts() ): the_post();
					echo '<h2>'. get_the_title(). '</h2>';
					the_content();
				endwhile; endif;
			?>
		</section>

		<section class="wrapper grid_2">
			<div class="column">

				<?php if( !empty($_REQUEST['sg_sessionid']) ): //if there has been an SG submission, show reset link ?>
					<a href="<?php echo get_permalink(); ?>"><?php echo $search_again; ?>.</a>
				<?php else: //else show search form ?>

					<h2 class="center"><?php echo $runner_info_txt; ?></h2>
					<form action="" method="post" id="mxc_medals_form">
						<input type="hidden" name="mxc_medals_action" value="search">

						<?php wp_nonce_field( 'search', 'mxc_medal_search' ); ?>

						<div class="form-field">
							<label for="first_name"><?php echo $first_name_txt; ?></label>
						</div>

						<input type="text" name="first_name" required="required" pattern=".{3,}" title="3 characters minimum">

						<div class="form-field">
							<label for="last_name"><?php echo $last_name_txt; ?></label>
						</div>

						<input type="text" name="last_name">

						<!-- <div class="form-field">
							<label for="email"><?php echo $email_txt; ?></label>
						</div>
						<input type="text" name="email"> -->

						<div class="form-field">
							<label for="bib_15"><?php echo $bib_number_txt; ?> MXC 2015</label>
						</div>

						<input type="number" name="bib_15" step="1">

						<div class="form-field">
							<label for="bib_16"><?php echo $bib_number_txt; ?> MXC 2016</label>
						</div>

						<input type="number" name="bib_16" step="1">

						<div class="form-field">
							<div>
								<label for=""><?php echo $birthdate_txt; ?></label>
							</div>

							<!-- <input type="number" name="birth_day" id="birth_day" step="1" min="1" max="31" required="required"> -->

							<select name="birth_day" id="birth_day" required="required">
								<option value="" selected disabled><?php echo $select_txt; ?></option>
								<?php
									for( $day_count = 1; $day_count <= 31; $day_count++ ) {
										echo "<option value='$day_count'>$day_count</option>";
									}
								?>
							</select>

							<select name="birth_month" id="birth_month" required="required">
								<option value="" selected disabled><?php echo $select_txt; ?></option>
								<?php
									foreach( $months as $month ) {
										++$month_value;
										echo "<option value='$month_value'>$month</option>";
									}
								?>
							</select>

							<select name="birth_year" id="birth_year" required="required">
								<option value="" selected disabled><?php echo $select_txt; ?></option>
								<?php
									for( $year = $year_min; $year_max >= $year; $year++ ) {
										echo "<option value='$year'>$year</option>";
									}
								?>
							</select>

						</div>

						<div class="submit-button">
							<input type="submit" value="Submit">
						</div>

					</form>
				<?php endif; ?>

			</div>

			<?php if( !empty($_POST['mxc_medals_action']) ): ?>
				<div class="column">
					<?php
						if( $_POST['mxc_medals_action'] == 'search' ) {
							// secure form submssion
							$search_nonce		= isset($_REQUEST['mxc_medal_search']) ? $_REQUEST['mxc_medal_search'] : false;
							$search_verified	= wp_verify_nonce( $search_nonce, 'search' );

							$first_name	= sanitize_text_field($_POST['first_name']);
							$last_name	= sanitize_text_field($_POST['last_name']);
							// $email	= sanitize_text_field($_POST['email']);
							$bib_15		= sanitize_text_field($_POST['bib_15']);
							$bib_16		= sanitize_text_field($_POST['bib_16']);
							$birthdate	= $_POST['birth_month'].'/'.$_POST['birth_day'].'/'.$_POST['birth_year'];

							if( ($search_nonce && !$search_verified) || in_array('', array( $first_name, $birthdate ) ) ) { ?>

								<h2 class="center">There was an error. Please try again.</h2>

							<?php } elseif( $search_nonce && $search_verified ) {

								$query_string		= "SELECT * FROM $wpdb->mxc_medals WHERE birth_date = \"$birthdate\"";
								// $query_string	.= " AND email_address = \"$email\"";
								$query_string		.= " AND (2015_bib = \"$bib_15\" OR 2016_bib = \"$bib_16\")";
								$query_string		.= ' AND first_name LIKE "%'.$first_name.'%"';
								// $query_string	.= ' AND last_name LIKE "%'.$last_name.'%"';

								$runnerdata			= $wpdb->get_row( $query_string, ARRAY_A );

								if( null !== $runnerdata && $runnerdata['flag'] != 0 ) { //runner was found but has already submitted data
									echo wpautop( $message_flagged );
								} elseif( null !== $runnerdata ) { //runner found but has not submitted data
									$runnername	= '<strong>'.$runnerdata['first_name'].' '.$runnerdata['last_name'].'</strong>';
									echo wpautop( $message_form.' '.$runnername ); ?>

									<form action="" method="post" id="new_address">
										<input type="hidden" name="mxc_medals_action" value="save">
										<input type="hidden" name="birth_date" value="<?php echo $birthdate; ?>">
										<input type="hidden" name="bib_15" value="<?php echo $bib_15; ?>">
										<input type="hidden" name="bib_16" value="<?php echo $bib_16; ?>">
										<?php wp_nonce_field( 'save_address', 'mxc_medal_save' ); ?>
										<div class="form-field">
											<label for="new_address1"><?php echo $display_text['address']; ?> 1</label>
										</div>
										<textarea name="new_address1" row="3" required="required"></textarea>
										<div class="form-field">
											<label for="new_address2"><?php echo $display_text['address']; ?> 2</label>
										</div>
										<textarea name="new_address2" row="3"></textarea>
										<div class="form-field">
											<label for="new_city"><?php echo $city_txt; ?></label>
										</div>
										<input type="text" name="new_city" required="required">
										<div class="form-field">
											<label for="new_state_code"><?php echo $state_txt; ?></label>
										</div>
										<input type="text" name="new_state_code" required="required">
										<div class="form-field">
											<label for="new_postal_code"><?php echo $zipcode_txt; ?></label>
										</div>
										<input type="number" name="new_postal_code" step="1" required="required">
										<div class="form-field">
											<label for="new_country_code"><?php echo $country_txt; ?></label>
										</div>
										<input type="text" name="new_country_code" required="required">
										<input type="submit" value="Submit">
									</form>

								<?php } else { // no runner was found
									echo wpautop( $message_no_runners );
									echo do_shortcode( $message_sg_form );
								}
							} // end mxc_medal_search

						} elseif( $_POST['mxc_medals_action'] == 'save' ) {
							// secure form submssion
							$save_nonce		= isset($_REQUEST['mxc_medal_save']) ? $_REQUEST['mxc_medal_save'] : false;
							$save_verified	= wp_verify_nonce( $save_nonce, 'save_address' );

							if( $save_nonce && !$save_verified ) { ?>

								<h2 class="center">There was an error. Please try again.</h2>

							<?php } elseif( $save_nonce && $save_verified ) {
								// set up WHERE data
								$where_data = array(
									'birth_date' => $_POST['birth_date']
								);

								if( !empty($_POST['bib_15']) ) {
									$where_data['2015_bib'] = $_POST['bib_15'];
								}

								if( !empty($_POST['bib_16']) ) {
									$where_data['2016_bib'] = $_POST['bib_16'];
								}

								// set up save data
								$new_address = array(
									'new_address1'		=> sanitize_text_field( $_POST['new_address1'] ),
									'new_address2'		=> sanitize_text_field( $_POST['new_address2'] ),
									'new_city'			=> sanitize_text_field( $_POST['new_city'] ),
									'new_state_code'	=> sanitize_text_field( $_POST['new_state_code'] ),
									'new_postal_code'	=> sanitize_text_field( $_POST['new_postal_code'] ),
									'new_country_code'	=> sanitize_text_field( $_POST['new_country_code'] ),
									'flag'				=> 1
								);

								$result = $wpdb->update( $wpdb->mxc_medals, $new_address, $where_data );
								echo $message_confirmation;
							}
						}
					?>
				</div>
			<?php endif;

			if( !empty($_REQUEST['sg_sessionid']) ) : ?>

				<div class="column">
					<?php echo do_shortcode( $message_sg_form ); ?>
				</div>

			<?php endif; ?>

		</section>
	</main>
<?php get_footer(); ?>
