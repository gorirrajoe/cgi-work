<?php
	/**
	 * header for Expo Registration
	 */
	global $wpdb;
	$market = get_market2();

	$qt_lang = rnr3_get_language();

	/**
	 * global variables
	 */
	$header_code		= rnr_get_option( 'rnrgv_header');
	$google_analytics	= rnr_get_option( 'rnrgv_google_analytics');
	$gtm				= rnr_get_option( 'rnrgv_gtm');

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

?><!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<?php rnr3_display_favicon();

		if( $google_analytics != '' ) { ?>
			<script type="text/javascript">
				var _gaq = _gaq || [];
				var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
				_gaq.push(
					['_require', 'inpage_linkid', pluginUrl],
					['_setAccount', '<?php echo $google_analytics;?>'],
					['_setDomainName', 'runrocknroll.com'],
					['_setAllowLinker', true],
					['_trackPageview']
				);
				(function() {
					var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
				})();
			</script>
		<?php }

		wp_head();

		if($event_info->header_settings != '') {
			echo $event_info->header_settings;
		} ?>
	</head>
	<body id="expo-reg" <?php body_class( 'minimal-nav' ); ?>>
		<?php if( $gtm != '' ) {
			echo stripslashes( $gtm );
		} ?>

		<?php if($event_info->alert_msg != '') { ?>
			<!-- alert banner -->
			<section id="alert">
				<section class="wrapper">
					<p class="mid2"><?php echo $event_info->alert_msg; ?></p>
				</section>
			</section>
		<?php } ?>

		<?php
			// get data for header display
			$postid           = get_the_ID();
			$header_copy      = get_post_meta( $postid, '_header_info_mid_text', true ) ?: '';
			$subscription     = get_post_meta( $postid, '_header_info_subscription_code', true ) ?: '';
			$featured_events  = get_post_meta( $postid, '_banner_info_featured_events', true ) ?: '';
			$tourpass_box     = get_post_meta( $postid, '_banner_info_tourpass_box', true ) ?: '';
		?>

		<!-- begin main header -->
		<header>
			<div id="logo"><a href="<?php echo site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo@2x.png" alt="Rock 'N' Roll Marathon Series" class="mid"></a></div>

			<?php if( $header_copy != '' ): ?>
			<div id="header-copy"><?php echo $header_copy; ?></div>
			<?php endif; ?>

			<?php if( $subscription != '' ): ?>
			<div class="subscription-form"><?php echo do_shortcode( $subscription ); ?></div>
			<?php endif; ?>
		</header>

		<section class="banner">

			<?php
				if( $featured_events != '' ):

					//figure out which featured element to show based on the parameter
					//using -1 so that fevents start at 1
					if( !empty($_GET['fevent']) && isset( $featured_events[$_GET['fevent'] - 1] ) ){
						$event_id = $featured_events[$_GET['fevent'] - 1];
					}else{
						$event_id = $featured_events[0];
					}

					//query to get post
					$args = array(
					'p'               => $event_id,
					'post_status'     => array( 'publish', 'future' ),
					'posts_per_page'  => -1,
					);

					$query = new WP_Query( $args );

					if ( $query->have_posts() ) :while ( $query->have_posts() ) : $query->the_post();

					$time       = strtotime( get_the_date() );
					$reg_link   = get_post_meta( $event_id, '_reg_details_reg_link', true ) ?: '';
					?>

					<div class="banner-column featured-event">
						<h2><?php echo the_title(); ?></h2>
						<div class="date"><?php the_date('F d, Y'); ?></div>
						<?php echo wpautop(the_excerpt()); ?>

						<?php if( $reg_link != '' ): ?>
						<a href="<?php echo $reg_link; ?>" class="cta register">Register</a>
						<?php endif; ?>

						<a href="#" class="cta get-details" data-post-id="<?php echo $event_id; ?>">Details</a>
					</div>

					<?php
						if( has_post_thumbnail() ):
							$image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $event_id ), 'newsletter-post' );
					?>
					<div class="banner-column featured-event-image" style="background-image: url(<?php echo $image_url[0]; ?>)"></div>
					<?php endif; // ends post thumbnail ?>

					<?php endwhile; endif; wp_reset_postdata(); //end loop and reset data

				endif; //$featured_events

				if( $tourpass_box != '' ): ?>
				<div id="tourpass-box" class="banner-column"><?php echo wpautop($tourpass_box); ?></div>
				<?php endif; ?>

		</section>
