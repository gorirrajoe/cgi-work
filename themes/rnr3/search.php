<?php
	get_header('special');

	$market = get_market2();
	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';

?>

	<?php
	/*<div id="content" class="narrowcolumn" role="main">

	<?php if (have_posts()) : ?>

		<h2 class="pagetitle">Search Results</h2>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
		</div>


		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?>>
				<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
				<small><?php the_time('l, F jS, Y') ?></small>

				<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p>
			</div>

		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
		</div>

	<?php else : ?>

		<h2 class="center">No posts found. Try a different search?</h2>
		<?php get_search_form(); ?>

	<?php endif; ?>

	</div>

	<?php get_sidebar(); */?>


	<main role="main" id="main">
		<section class="wrapper grid_2 offset240left">
			<div class="column archive_list">
				<div class="content">
					<?php if( have_posts() ) {
						echo '<h2>Search Results for: '. get_search_query() .'</h2>';

						while( have_posts()) {
							the_post(); ?>

							<article <?php post_class() ?>>
								<?php $image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'news-promos-thumbs');

								if($image_array[0]) { ?>
									<a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><img class="wp-post-image alignleft" src="<?php echo $image_array[0];?>"></a>
								<?php } ?>

								<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>

								<p><?php the_excerpt(); ?></p>
								<a href="<?php the_permalink(); ?>" class="read-more-link"><?php echo $readmore_txt; ?></a>

							</article>

						<?php } ?>

						<nav class="archive_nav">
							<?php if( get_next_posts_link() ) { ?>
								<div class="alignleft"><?php next_posts_link('&laquo; Older Entries', max_num_pages) ?></div>
							<?php }

							if( get_previous_posts_link() ) { ?>
								<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
							<?php } ?>

						</nav>

						<?php wp_reset_postdata();

					} else {
						echo '
							<h2>No posts found.</h2>
							<p> Try a different search?</p>
							<div class="searchform_inpage">'. get_search_form( false ) .'</div>
						';
					} ?>
				</div>
			</div>
			<?php get_sidebar(); ?>

		</section>
	</main>

<?php get_footer( 'series' ); ?>
