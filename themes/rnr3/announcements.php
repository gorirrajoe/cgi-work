<?php
	/* Template Name: Announcements - LV */

	get_header();

	$market = get_market2();
	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';

	$prefix		= '_rnr3_';
	$page_id	= get_the_ID();

	// declare array with pre-determined announcements
	$announcements = array(
		0 => array(
			'title'	=> 'course entertainment',
			// 'icon'	=> 'lv-icon-course.svg'
		),
		1 => array(
			'title'	=> 'runner perks',
			// 'icon'	=> 'lv-icon-perks.svg'
		),
		2 => array(
			'title'	=> 'headliner',
			// 'icon'	=> 'lv-icon-headliner.svg'
		),
		3 => array(
			'title'	=> 'remix challenge',
			// 'icon'	=> 'lv-icon-remix.svg'
		),
		4 => array(
			'title'	=> 'vip experiences',
			// 'icon'	=> 'lv-icon-vip.svg'
		),
		5 => array(
			'title'	=> 'finisher medals',
			// 'icon'	=> 'lv-icon-medals.svg'
		),
		6 => array(
			'title'	=> 'marathon finisher jacket',
			// 'icon'	=> 'lv-icon-jacket.svg'
		),
		7 => array(
			'title'	=> 'participant t-shirt',
			// 'icon'	=> 'lv-icon-shirt.svg'
		)
	);
	// get all metadata needed
	$post_pubkey			= rnr_get_option( 'rnrgv_post_pubkey');
	$announcements_image	= get_post_meta( $page_id, $prefix .'announcements_image', 1 ) ?: false;
	$announcements_intro	= get_post_meta( $page_id, $prefix .'announcements_intro_copy', 1 ) ?: false;
	$announcements_nav		= '';
	$sort_order				= array();

	foreach( $announcements as $key => $announcement ){
		$data_to_merge				= get_post_meta( $page_id, $prefix .'announcement_'. $key, 1 );
		$data_to_merge[0]['order']	= isset( $data_to_merge[0]['order'] ) ? $data_to_merge[0]['order'] : 0;
		$announcements[$key]		= array_merge( $announcements[$key], $data_to_merge[0] );
		$sort_order[$key]			= $announcements[$key]['order'];
	}

	// sort the announcements by order meta or regular order
	function sort_by_order( $item1, $item2 ){
		if ($item1['order'] == $item2['order']) return 0;
		return $item1['order'] < $item2['order'] ? -1 : 1;
	}
	function sort_by_order2( $item1, $item2 ){
		$result = strnatcmp($item1['order'], $item2['order']);

		return $result;
	}
	// if any were left in default value (0), we will sort by natural order. MAKE SURE KEVIN FILLS OUT ALL FIELDS
	if( !in_array( 0, $sort_order ) ){
		uasort( $announcements, 'sort_by_order' );
	}

	foreach( $announcements as $key => $announcement ){
		if( isset( $announcement['copy'] ) ){
			$nav_icon					= $announcement['icon'];
			$target_id				= str_replace( ' ', '-', $announcement['title'] );
			$title						= strtoupper( $announcement['title'] );
			$announcements_nav .= '<li><a href="#'. $target_id .'"><img src="'. $nav_icon .'">'. $title . '</a></li>';
		}
	}

?>

	<nav id="secondary">
		<section class="wrapper">
			<div class="menu-label">Menu</div>
			<ul class="menu">
				<?php echo $announcements_nav; ?>
			</ul>
			<a href="<?php echo get_bloginfo('url') .'/register/'; ?>" class="cta">Register</a>
		</section>
	</nav>

	<div id="announcements"></div>
	<main role="main" id="main" class="lv-announcements">
		<div id="nav-anchor"></div>
		<section class="wrapper grid_2 offset240left">
			<div class="column sidenav stickem">
				<nav class="sticky-nav">
					<ul>
						<?php echo $announcements_nav; ?>
						<a href="<?php echo get_bloginfo('url') .'/register/'; ?>" class="cta">Register</a>
					</ul>
				</nav>
			</div> <!-- END .COLUMN.SIDENAV.STICKEM -->

			<div class="column">
				<div class="content">
					<?php
						the_title( '<h2>', '</h2>' );

						echo '<div class="social_container">';
							social_sharing( $post_pubkey );
						echo '</div>';

						if( $announcements_image ){
							echo '<img src="'. $announcements_image .'" class="main-image">';
						}
						if( $announcements_intro ){
							echo apply_filters( 'the_content', $announcements_intro );
						}

						foreach( $announcements as $key => $announcement ){
							if( isset( $announcement['copy'] ) ){
								$copy				= $announcement['copy'];
								$target_id	= str_replace( ' ', '-', $announcement['title']);
								$title			= strtoupper($announcement['title']);
								$cta_url		= isset($announcement['url']) ? $announcement['url'] : '';
					?>
								<div id="<?php echo $target_id; ?>" class="lv-announcement">
									<img src="<?php echo $announcement['image']; ?>">
									<h2><?php echo $title; ?></h2>

									<?php
										if( isset($announcement['distances']) ){
											$distances = array_reverse( $announcement['distances'] );
											foreach( $distances as $distance){
												echo '<div class="distance">'. strtoupper($distance) ."</div>";
											}
										}
									?>

									<?php echo apply_filters( 'the_content', $copy ); ?>

									<?php if( $cta_url != '' ): ?>
										<a class="highlight gray" href="<?php echo $cta_url ?>" target="_blank">Learn More</a>
									<?php endif; ?>
									<div class="clearfix"></div>
								</div>
					<?php
							} // end if
						} // end foreach $announcements

						echo '<div class="entry-footer">';
							social_sharing( $post_pubkey, 0 );
						echo '</div>';
					?>
				</div>
			</div>
		</section>
	</main>

	<script>
		$(document).ready(function(){

				/**
				 * This part causes smooth scrolling using scrollto.js
				 * We target all a tags inside the nav, and apply the scrollto.js to it.
				 */
				$(".sticky-nav li > a").click(function(evn){
						evn.preventDefault();
						$('html,body').scrollTo(this.hash, this.hash);
				});

				/*$('main').stickem({
					container: '.offset240left',
				});*/

				/**
				 * This part handles the highlighting functionality.
				 * We use the scroll functionality again, some array creation and
				 * manipulation, class adding and class removing, and conditional testing
				 */
				var aChildren = $(".sticky-nav li").children(); // find the a children of the list items
				var aArray = []; // create the empty aArray
				for (var i=0; i < aChildren.length; i++) {
						var aChild = aChildren[i];
						var ahref = $(aChild).attr('href');
						aArray.push(ahref);
				} // this for loop fills the aArray with attribute href values

				$(window).scroll(function(){
						var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
						var windowHeight = $(window).height(); // get the height of the window
						var docHeight = $(document).height();

						for (var i=0; i < aArray.length; i++) {
								var theID = aArray[i];
								var divPos = $(theID).offset().top; // get the offset of the div from the top of page
								var divHeight = $(theID).height(); // get the height of the div in question
								if (windowPos >= (divPos - 1) && windowPos < (divPos + divHeight -1 )) {
										$("a[href='" + theID + "']").addClass("nav-active");
								} else {
										$("a[href='" + theID + "']").removeClass("nav-active");
								}
						}

						if(windowPos + windowHeight == docHeight) {
								if (!$(".sticky-nav li:last-child a").hasClass("nav-active")) {
										var navActiveCurrent = $(".nav-active").attr("href");
										$("a[href='" + navActiveCurrent + "']").removeClass("nav-active");
										$(".sticky-nav li:last-child a").addClass("nav-active");
								}
						}
				});
		});

	</script>


 <?php get_footer(); ?>
