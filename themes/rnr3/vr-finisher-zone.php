<?php
	/* Template Name: Virtual Run: Finisher Zone */
	get_header( 'oneoffs' );

	$market = get_market2();
	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$prefix = '_rnr3_';
	$qt_lang= rnr3_get_language();
	$qt_status= $qt_lang['enabled'] == 1 ? true : false;
	$qt_cur_lang= $qt_lang['lang'];

	include 'languages.php';
?>


	<main role="main" id="main" class="virtual-run">

		<section id="events">
			<section class="wrapper">
				<div class="grid_3_special">
					<?php
						$event_array = array(
							'guitar'	=> 'Guitar Solo',
							'drum'		=> 'Drum Solo',
							'mic'		=> 'Lead Singer'
						);
						$count = 0;
						$today_unix = strtotime( '-7 hours' );

						foreach( $event_array as $key => $value ) {
							$date1_unix	= vr_get_option( $prefix . $key . '_date1' );
							$date1		= date( 'M d', $date1_unix );

							$regdate_start_unix	= vr_get_option( $prefix . $key . '_regdate1' );
							$regdate_start		= date( 'M d', $regdate_start_unix );

							$regdate_end_unix	= vr_get_option( $prefix . $key . '_regdate2' );
							$regdate_end		= date( 'M d', $regdate_end_unix );

							$date2_unix	= vr_get_option( $prefix . $key . '_date2' );
							$date2		= date( 'M d', $date2_unix );

							${$key . '_date1_ext'}		= strtotime( $date1 . ' 12:00:01 AM' );
							${$key . '_date2_ext'}		= strtotime( $date2 . ' 11:59:59 PM' );

							${$key . '_regdate_start'}	= strtotime( $regdate_start . ' 12:00:01 AM' );
							${$key . '_regdate_end'}	= strtotime( $regdate_end . ' 11:59:59 PM' );


							echo '<div class="column_special">
								<h3>'. strtoupper( $value ) .'</h3>';

								if( date( 'M', $date1_unix ) == date( 'M', $date2_unix ) ) {
									$date2_day = date( 'd', $date2_unix );
									echo '<p class="vr_upper">'. $date1 .' &ndash; '. $date2_day .'</p>';
								} else {
									echo '<p class="vr_upper">'. $date1 . ' &ndash; ' . $date2 .'</p>';
								}

								echo '<div class="solo_ctas">';
									if( ( $fzurl = vr_get_option( $prefix . $key . '_fzurl', 1 ) ) == '' ) {
										if( ( $fztxt_before = get_post_meta( get_the_ID(), $prefix . 'fz_cta_before', 1 ) ) != '' ) {
											echo '<span class="button_single highlight silver">'. $fztxt_before .'</span><br>';
										}
									} else {
										if( ( $fztxt_after = get_post_meta( get_the_ID(), $prefix . 'fz_cta_after', 1 ) ) != '' ) {
											echo '<a class="button_single highlight red" href="'. $fzurl .'">'. $fztxt_after .'</a><br>';
										}
									}

									if( $today_unix < ${$key . '_regdate_start'} ) {
										echo '<div style="text-align: center;"><span class="highlight gray">Registration Coming Soon</span></div>';
									} elseif( ( $today_unix < ${$key . '_regdate_end'} ) && ( $regurl = vr_get_option( $prefix . $key . '_regurl', 1 ) ) != '' ) {
										echo '<a class="button_single highlight red" href="'. $regurl .'">Register Now</a>';
									} else {
										echo '<span class="button_single highlight silver">Registration Closed</span>';
									}

								echo '</div>
							</div>';
							$count++;
						}
					?>
				</div>
			</section>
		</section>


		<?php
			/**
			 * finisher zone badges
			 */
			$badges = get_post_meta( get_the_ID(), $prefix . 'badges', 1 );
			if( !empty( $badges ) ) {
				echo '<section id="badges">
					<section class="wrapper">
						<h2>Finisher Badges</h2>
						<div class="grid_4_special">';

							foreach( $badges as $key => $entry ) {
								echo '<div class="column_special">
									<figure>
										<img src="'. $entry['image'] .'">
									</figure>
									<h4>'. strtoupper( $entry['name'] ) .'</h4>
									<p>'. $entry['desc'] .'</p>
								</div>';
							}
						echo '</div>
					</section>
				</section>';
			}
		?>
	</main>


<?php get_footer('series'); ?>
