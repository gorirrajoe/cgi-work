<?php
	/**
	 * Template Name: Competitor Wireless (Series)
	 */

	get_header( 'special' );

	$parent_slug = the_parent_slug();

	rnr3_get_secondary_nav( $parent_slug );
	$market = get_market2();

	/**
	 * get all post metadata
	 */
	$all_post_meta = get_meta( get_the_ID() );
	
	//$rnrappdomain = 'runrocknrollapp.com';
	$rnrappdomain = '69.2.216.133';

?>

	<!-- main content -->
	<main role="main" id="main" class="cw">
		<section class="wrapper">
			<div class="content">

				<?php
					if (have_posts()) : while (have_posts()) : the_post();

						$page_sponsor = array_key_exists( '_rnr3_sponsor', $all_post_meta ) ? $all_post_meta['_rnr3_sponsor'] : '';


						if( $page_sponsor != '' ) {
							echo '<div class="cw--sponsor">Compliments of <img src="'. $page_sponsor .'"></div>';
						}

						echo '<div class="showmobile">'.

							apply_filters( 'the_content', get_the_content() ) .'

							<div class="accordion">
								<div class="accordion-section custom" style="position: relative;">
									<div class="accordion-section-title" data-reveal="#panel-what">What is Runner Tracking?</div>
									<div class="accordion-section-content" id="panel-what">'.

										apply_filters( 'the_content', $all_post_meta['_rnr3_extra_content'] ) .'

									</div>
								</div>
							</div>
						</div>

						<div class="showdesktop">' .

							apply_filters( 'the_content', get_the_content() ) .

							apply_filters( 'the_content', $all_post_meta['_rnr3_extra_content'] ) .'

						</div>';

					endwhile; endif;
				?>

			</div>
		</section>


		<?php
			/**
			 * check if there are any events
			 * if none (failure), don't show the forms
			 */
			$events_jsonurl	= 'http://'.$rnrappdomain.'/ajax/events/getrnrevents';
			$events_json	= file_get_contents( $events_jsonurl );
			$events_output	= json_decode( $events_json );

			if( $events_output->status == 'success' ) {

				/**
				 * cell phone and confirmation forms
				 * confirmation will not show up until after cell phone checks okay
				 */ ?>

				<section class="wrapper cw--forms">
					<section class="grid_2_special">

						<div class="column_special">
							<form autocomplete="off" class="mobile_codes" id="send_code" method="POST" action="#">
								<label for="phone">Cell Phone Number</label>
								<input type="text" id="phone" name="phone" required placeholder="123-456-7890" maxlength="12" type="tel">
								<input type="hidden" name="send_code_action" value="1">
								<button id="send_code_submit" type="submit">Send Code</button>
							</form>
							<div id="error_invalid_phone" style="display: none;">Not a valid phone number!</div>
							<div id="error_phone_max_requests" style="display: none;">Maximum confirmation requests exceeded.  This will reset in 24 hours, or contact <a href="mailto:cwsupport@competitorwireless.com">cwsupport@competitorwireless.com</a> to have it reset</div>

						</div>

						<div class="column_special" id="confirmation_code" style="display: none;">
							<form autocomplete="off" class="mobile_codes" id="confirm_code" method="POST" action="#">
								<label for="usr_confirmation_code">Confirmation Code</label>
								<input type="text" id="usr_confirmation_code" name="confirmation_code" required maxlength="6">
								<button type="submit" id="confirm_code_submit">Submit</button>
							</form>
							<div id="error_invalid_code" style="display: none;">Confirmation code is not correct for this phone number.</div>

						</div>

					</section>
				</section>


				<?php
					/**
					 * find an athlete section
					 * shouldn't appear until confirmation checks okay
					 */
				?>
				<section class="wrapper cw--findathlete" id="findathlete" style="display: none;">
					<h2>Find an Athlete</h2>

					<?php
						$event_count = count( $events_output->events );
					?>

					<div class="grid_2_special">
						<div class="column_special">

							<form class="find_athletes" id="find-athlete" method="POST" action="#">
								<label for="cw_event_dropdown">Choose an Event</label>
								<div class="find_athletes--dropdown">
									<select id="cw_event_dropdown" class="cw--selectevent">
										<option value="-1">-- Select an Event --</option>

										<?php for( $i = 0; $i < $event_count; $i++ ) {

											echo '<option value="'. $events_output->events[$i]->id .'">'. $events_output->events[$i]->name .'</option>';

										} ?>

									</select>
								</div>
							</form>
						</div>

						<div class="column_special">
							<div class="find_athletes--search" style="display: none;">
								<form autocomplete="off" class="runner_search" id="name-bib-search" method="POST" action="#">
									<label for="athlete-search">Participant's Name or Bib Number</label>
									<input type="text" id="athlete-search" name="athlete-search" required maxlength="24">
									<button type="submit" id="athlete-search-submit">Submit</button>
								</form>
							</div>
						</div>
					</div>

					<div class="athlete_search_results" style="display: none;">

						<table id="personDataTable-header" cellspacing="0" cellpadding="0" border="0">
							<tr>
								<th>Name</th>
								<th>Bib Number</th>
								<th>Track?</th>
							</tr>
						</table>

						<table id="personDataTable-selected" cellspacing="0" cellpadding="0" border="0">

						</table>

						<table id="personDataTable" cellspacing="0" cellpadding="0" border="0">

						</table>

						<div id="search-confirm-init" style="display: none;">
							<div class="grid_2_special">

								<div class="column_special">
									<label><input id="agree-checkbox" type="checkbox" value="agree"> <strong>Yes, I have read and agree with the <a href="javascript:;" data-fancybox data-src="#terms">Terms & Conditions</a></strong><br>
									(Must agree to terms to complete process)</label>
								</div>

								<div class="column_special track_button_div">
									<form id="start-tracking-form" method="POST" action="#">
										<button id="start-tracking" type="submit" disabled>Start Tracking</button>
									</form>
									<div id="error_tracking" style="display: none;">There was an issue with tracking your runners.</div>
								</div>

							</div>
						</div>

					</div>
				</section>


				<section class="wrapper cw--tracking-confirmation" id="tracking-confirmation" style="display: none;">

					<?php
						$confirmation_title = array_key_exists( '_rnr3_confirmation_title', $all_post_meta ) ? $all_post_meta['_rnr3_confirmation_title'] : '';
						$confirmation_text = array_key_exists( '_rnr3_confirmation_txt', $all_post_meta ) ? apply_filters( 'the_content', $all_post_meta['_rnr3_confirmation_txt'] ) : '';

						echo '<h3>'. $confirmation_title .'</h3>'.
						$confirmation_text .

						'<div class="runner_tracking_status"></div>

						<p><a class="cta" href="'. get_permalink() .'">Finish</a></p>';
					?>

				</section>

			<?php } else {

				$no_events_txt = array_key_exists( '_rnr3_no_events', $all_post_meta ) ? apply_filters( 'the_content', $all_post_meta['_rnr3_no_events'] ) : '';

				if( $no_events_txt != '' ) {

					echo '<section class="wrapper">'.

						$no_events_txt .'

					</section>';

				}
			}


			/**
			 * texting details and restrictions section
			 */
			$restrictions_title = array_key_exists( '_rnr3_restrictions_title', $all_post_meta ) ? $all_post_meta['_rnr3_restrictions_title'] : '';
			$restrictions_content = array_key_exists( '_rnr3_restrictions_content', $all_post_meta ) ? apply_filters( 'the_content', $all_post_meta['_rnr3_restrictions_content'] ) : '';
		?>

		<section class="wrapper cw--restrictions">

			<?php
				if( $restrictions_content != '' ) { ?>

					<div class="accordion">
						<div class="accordion-section custom" style="position: relative;">
							<div class="accordion-section-title" data-reveal="#panel-restrictions"><?php echo $restrictions_title; ?></div>
							<div class="accordion-section-content" id="panel-restrictions">
								<?php echo $restrictions_content; ?>
							</div>
						</div>
					</div>

				<?php }


				$note = array_key_exists( '_rnr3_note', $all_post_meta ) ? $all_post_meta['_rnr3_note'] : '';

				if( $note != '' ) {

					echo '<div class="cw--note">
						<span>'. $note .'</span>
					</div>';

				}


				$alert = array_key_exists( '_rnr3_alert', $all_post_meta ) ? apply_filters( 'the_content', $all_post_meta['_rnr3_alert'] ) : '';

				if( $alert != '' ) {

					echo '<div class="cw--alert">'.
						$alert .'
					</div>';

				}
			?>

		</section>


		<?php
			/**
			 * faq section
			 */
		?>
		<section class="wrapper cw--faq">

			<?php
				$faq_content = array_key_exists( '_rnr3_faq', $all_post_meta ) ? apply_filters( 'the_content', $all_post_meta['_rnr3_faq'] ) : '';

				if( $faq_content != '' ) {

					echo '
						<h2>FAQs</h2>'.
						$faq_content
					;

				}
			?>

		</section>


		<?php
			/**
			 * terms and conditions
			 */

			$terms = array_key_exists( '_rnr3_terms', $all_post_meta ) ? apply_filters( 'the_content', $all_post_meta['_rnr3_terms'] ) : '';

			if( $terms != '' ) {

				echo '<div class="cw--terms" id="terms" style="display: none; max-width: 800px; min-height: 400px; max-height: 600px;">'.
					$terms .'
				</div>';

			}
		?>


		<script>
			/**
			 * retrieves carrier id, phone number, event
			 * from various submit form successes throughout the page
			 */
			var variables_fn = ( function() {
				var stored_carrierId,
					stored_phone,
					stored_event,
					stored_trackedId = [],
					pub = {};

				pub.storeID = function( id ) {
					stored_carrierId = id;
				};
				pub.getID = function() {
					return stored_carrierId;
				};
				pub.storePhone = function( phone ) {
					stored_phone = phone;
				};
				pub.getPhone = function() {
					return stored_phone;
				};
				pub.storeEvent = function( event_id ) {
					stored_event = event_id;
				};
				pub.getEvent = function() {
					return stored_event;
				};
				pub.storeTrackedID = function( pid ) {
					stored_trackedId.push( pid );
				};
				pub.getTrackedID = function() {
					return stored_trackedId;
				}
				return pub;
			})();


			/**
			 * SUBMIT FUNCTION
			 * submit function for sending code to phone number
			 */
			$( '#send_code' ).submit( function( e ) {
				/**
				 * reset all error messages
				 */
				$( '#error_invalid_phone', '#error_phone_max_requests', '#confirmation_code' ).hide();

				/**
				 * validation for phone
				 * we ask for xxx-xxx-xxxx format, but just in case they enter something crazy
				 * use regex to turn user input to xxxxxxxxxx
				 */
				var phone = $( '#phone' ).val();
				var strippedPhone = phone.replace( /[.?*+^$[\]\\(){}|-]+/g, '' );
				variables_fn.storePhone( strippedPhone );

				if( strippedPhone.match( /^\d{10}$/ ) ) {

					$.ajax({
						url : '<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/competitor-wireless-proxy.php',
						type : 'POST',
						data : {
							sendcode: 'http://<?php echo $rnrappdomain;?>/ajax/tracking/sendphonecode/phone/'+ strippedPhone +'/code/fakecode'
						},
						dataType : 'json',
						success : function( data ) {
							// console.log( 'success: ', data );

							if( data['status'] == 'failure' ) {
								$( '#error_phone_max_requests' ).effect( 'highlight' );
							} else {
								$( '#send_code_submit' ).attr( 'disabled', 'disabled' );
								$( '#confirmation_code' ).show();
								$( '#error_invalid_phone' ).hide();
								$( '#error_phone_max_requests' ).hide();
							}

							variables_fn.storeID( data['carrierId'] );

						},
						error : function( jqXHR, textStatus, errorThrown ) {
							var error = JSON.stringify( jqXHR );
							console.log( 'error: ', error );
						}
					});

				} else {
					$( '#error_invalid_phone' ).effect( 'highlight' );
					return false;
				}

				e.preventDefault();

			});


			/**
			 * SUBMIT FUNCTION
			 * submit event for confirmation code
			 */
			$( '#confirm_code' ).submit( function( e ) {
				/**
				 * validation for phone
				 * we ask for xxx-xxx-xxxx format, but just in case they enter something crazy
				 * use regex to turn user input to xxxxxxxxxx
				 */
				var confirmCode = $( '#usr_confirmation_code' ).val();
				var strippedPhone = variables_fn.getPhone();

				$.ajax({
					url : '<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/competitor-wireless-proxy.php',
					type : 'POST',
					data : {
						sendcode: 'http://<?php echo $rnrappdomain;?>/ajax/tracking/checkphonecode/phone/' + strippedPhone + '/code/' + confirmCode
					},
					dataType : 'json',
					success : function( data ) {
						//console.log( 'confirmation code success: ', data );

						if( data['status'] == 'failure' ) {
							$( '#error_invalid_code' ).effect( 'highlight' );
						} else {
							$( '#error_invalid_code' ).hide();
							$( '#confirm_code_submit' ).attr( 'disabled', 'disabled' );
							$( '#cw_event_dropdown' ).val( '-1' );
							$( '#findathlete' ).slideDown();
						}

					},
					error : function( jqXHR, textStatus, errorThrown ) {
						var error = JSON.stringify( jqXHR );
						console.log( 'error: ', error );
					}
				});

				e.preventDefault();

			});


			/**
			 * SUBMIT FUNCTION
			 * Search for athlete
			 * check if number for bib or name, sanitiz that input
			 * get the results by ajax to fill in a table that lets them add/remove alerts
			 */
			$( '#name-bib-search' ).submit( function( e ) {

				e.preventDefault();

				var inputNameBib = $( '#athlete-search' ).val(),
					nameOrBib = inputNameBib.replace( /[.?*+^$[\]\\(){}|-]+/g, '' ),
					carrier_id = variables_fn.getID(),
					eventID = variables_fn.getEvent();

				$( '#personDataTable tr' ).remove();

				//check for number bib else name
				if( !isNaN( nameOrBib )  ) {

					//Search for Runner By Bib Number
					$.ajax({
						url : '<?php echo get_bloginfo( "stylesheet_directory" ); ?>/competitor-wireless-proxy.php',
						type : 'POST',
						data : {
							sendcode: 'http://<?php echo $rnrappdomain;?>/ajax/events/searchforself/searchString/' + nameOrBib + '/eventId/' + eventID
						},
						dataType : 'json',
						success : function( data ) {

							if( data['status'] == 'failure' ) {

								console.log( 'bib search failure: ', data );

								$( '#search-results' ).append( '<tr>' +
									'<td class="no_athletes_found" colspan="3">'+ data['message'] + '</td>' +
								'</tr>' );

							} else {
								// console.log( 'bib search success: ', data );
								drawTable( data.athletes  );
							}

							$( '.athlete_search_results' ).show();
							$( '#search-confirm-init').show();

						},
						error : function( jqXHR, textStatus, errorThrown ) {
							var error = JSON.stringify( jqXHR );
							console.log( 'error: ', error );
						}
					});

				} else {

					var fullName = encodeURIComponent( nameOrBib );

					//search for Runner by name
					$.ajax({
						url : '<?php echo get_bloginfo( "stylesheet_directory" ); ?>/competitor-wireless-proxy.php',
						type : 'POST',
						data : {
							sendcode: 'http://<?php echo $rnrappdomain;?>/ajax/events/athletesearch/eventId/' + eventID + '/athleteName/' + fullName,
						},
						dataType : 'json',
						success : function( data ) {

							if( data['status'] == 'failure' ) {
								$( '' ).effect( 'highlight' );
							} else {
								//console.log( 'name search success: ', data );
								drawTable(data.athletes);
							}

							$( '.athlete_search_results' ).show();
							$( '#search-confirm-init').show();
						},
						error : function( jqXHR, textStatus, errorThrown ) {
							var error = JSON.stringify( jqXHR );
							console.log( 'error: ', error );
						}
					});

				}

			});


			/**
			 * focus functions
			 * various actions to disable buttons and hide error codes
			 */
			$( '#phone' ).focus( function() {
				$( '#send_code_submit' ).removeAttr( 'disabled' );
				$( '#confirm_code_submit' ).removeAttr( 'disabled' );
				$( '#confirmation_code' ).hide();
				$( '#error_invalid_phone' ).hide();
				$( '#error_phone_max_requests' ).hide();
				$( '#findathlete' ).slideUp();
				$( '#usr_confirmation_code' ).val( '' );
			});

			$( '#usr_confirmation_code' ).focus( function() {
				$( '#error_invalid_code' ).hide();
			});


			/**
			 * CLICK EVENT FUNCTIONS
			 * click event for the "agree with terms" checkbox
			 * when checked, enable the "start tracking" button
			 */
			$( '#agree-checkbox' ).click( function() {

				if( $( this ).is( ':checked' ) ) {

					$( '#start-tracking' ).removeAttr( 'disabled' );

				} else {

					$( '#start-tracking' ).attr( 'disabled', 'disabled' );

				}
			});


			/**
			 * ON EVENT FUNCTION
			 * choose an event dropdown
			 * only show the search box after an event has been chosen
			 * no need to show it if there is no race selected, right?
			 */
			$( '#cw_event_dropdown' ).on( 'change', function() {

				if( $( '#cw_event_dropdown' ).val() !== '-1' ) {

					$( '.find_athletes--search' ).show();
					variables_fn.storeEvent( $( '#cw_event_dropdown' ).val() );

				} else {

					$( '.find_athletes--search, .athlete_search_results' ).hide();

				}

			});


			/*
			 * ON EVENT FUNCTION
			 * click event function for the track buttons
			 */
			$( '#personDataTable' ).on( 'click', '.tracker', function() {

				$( this ).toggleClass( 'tracking' );

				if ( $( this ).hasClass( 'tracking' ) ) {
					$( this ).html( 'Tracking' );
					// $( this ).parent().parent().addClass( 'lockedRow' );

				} else {
					$( this ).html( 'Track' );
					// $( this ).parent().parent().removeClass( 'lockedRow' );

				}

			});


			/**
			 * ON EVENT FUNCTION
			 * when the track/untrack button for a runner is clicked,
			 * remove its row and append it to one of two tables:
			 * personDataTable or personDataTable-selected
			 */
			$( document ).on( 'submit', '.runner_track_btn', function( e ) {

				var $affectedTable = $( this ).closest( 'table' ).attr( 'id' );

				$( this ).parent().parent().remove();
				var row = $( '<tr />' );

				if( $affectedTable == 'personDataTable' ) {

					$( '#personDataTable-selected' ).append( row );

					row.append( $( '<td>' + $( this ).find( 'input[name=fullName]' ).val() + '</td>' ) );
					row.append( $( '<td>' + $( this ).find( 'input[name=bib]' ).val() + '</td>' ) );
					row.append( $( '<td>' +
						'<form class="runner_track_btn">' +
							'<input type="hidden" name="fullName" value="' + $( this ).find( 'input[name=fullName]' ).val() + '">' +
							'<input type="hidden" name="bib" value="' + $( this ).find( 'input[name=bib]' ).val() + '">' +
							'<input type="hidden" name="pid" value="' + $( this ).find( 'input[name=pid]' ).val() + '">' +
							'<button type="submit" class="tracking tracker">Tracking</button>' +
						'</form>' +
						'<span class="cw--untrack_txt">click to untrack</span>' +
					'</td>' ) );

				} else {

					$( '#personDataTable' ).append( row );

					row.append( $( '<td>' + $( this ).find( 'input[name=fullName]' ).val() + '</td>' ) );
					row.append( $( '<td>' + $( this ).find( 'input[name=bib]' ).val() + '</td>' ) );
					row.append( $( '<td>' +
						'<form class="runner_track_btn">' +
							'<input type="hidden" name="fullName" value="' + $( this ).find( 'input[name=fullName]' ).val() + '">' +
							'<input type="hidden" name="bib" value="' + $( this ).find( 'input[name=bib]' ).val() + '">' +
							'<input type="hidden" name="pid" value="' + $( this ).find( 'input[name=pid]' ).val() + '">' +
							'<button type="submit" class="tracker">Track</button>' +
						'</form>' +
					'</td>' ) );

				}

				e.preventDefault();

			});


			/**
			 * Use json results to fill the tracking table
			 * create a row for the search results in table
			 */
			function drawTable( data ) {
				for( var i = 0; i < data.length; i++ ) {
					drawRow( data[i] );
				}
			}

			//creates a row with a button that has a value of the pid to be tracked for that row
			function drawRow( rowData ) {

				var row = $( '<tr />' );
				$( '#personDataTable' ).append( row );

				row.append( $( '<td class="fullNameResults">' + rowData.firstName + ' ' + rowData.lastName + '</td>' ) );
				row.append( $( '<td class="bibResults">' + rowData.bib + '</td>' ) );
				row.append( $( '<td class="buttonResults">' +
					'<form class="runner_track_btn">' +
						'<input type="hidden" name="fullName" value="' + rowData.firstName + ' ' + rowData.lastName + '">' +
						'<input type="hidden" name="bib" value="' + rowData.bib + '">' +
						'<input type="hidden" name="pid" value="' + rowData.personId + '">' +
						'<button type="submit" class="tracker">Track</button>' +
					'</form>' +
				'</td>' ) );

			}


			/**
			 * ON EVENT FUNCTION
			 * Create tracking request and send via ajax
			 */
			$( '#start-tracking' ).on( 'click', function( e ) {

				e.preventDefault(); //don't submit yet

				var pId = [];

				//check for clicked track buttons save those people IDs to the variable fn
				$( '.tracker' ).each( function() {
					if ( $( this ).hasClass( 'tracking' ) ) {
						pId.push( $( this ).siblings( 'input[name=pid]' ).val() );
					}
				});

				//check for multiple racers
				if( pId.length > 1 ) {
					var trackedRacers = pId.join( '+' );
				} else {
					var trackedRacers = pId;
				}

				variables_fn.storeTrackedID( trackedRacers ); //store the racer id(s)

				//double check that we have a phone # to use for tracking
				if( $.isNumeric( variables_fn.getPhone() ) ) {
					// phone number is numeric!
				} else {
					alert( 'Phone number is required for tracking' );
				}

				$.ajax({
					url : '<?php echo get_bloginfo( "stylesheet_directory" ); ?>/competitor-wireless-proxy.php',
					type : 'POST',
					data : {
						sendcode: 'http://<?php echo $rnrappdomain;?>/ajax/tracking/addrtracker/phonenumber/' + variables_fn.getPhone() + '/pId/' + variables_fn.getTrackedID() + '/eventId/' + variables_fn.getEvent() + '/carrierId/' + variables_fn.getID(),
					},
					dataType : 'json',
					success : function( data ) {

						if( data['status'] == 'failure' ) {

							console.log( 'tracking failure: ', data );

							$( '#error_tracking' ).effect( 'highlight' );

						} else {
							var message = '';
							console.log( data )

							for( var i = 0; i < data.results.length; i++ ) {
								message += '<li>' + data.results[i].firstname + ' ' + data.results[i].lastname + ': ' + data.results[i].message + '</li>';
							}

							// console.log( 'tracker success: ', data );
							$( '.cw--forms, .cw--findathlete' ).hide();
							$( '#tracking-confirmation' ).show();
							$( '#tracking-confirmation .runner_tracking_status' ).html( '<ul>' + message + '</ul>' );
							$( window ).scrollTop( $( '#tracking-confirmation' ).offset().top );
						}

					},
					error : function( jqXHR, textStatus, errorThrown ) {
						var error = JSON.stringify( jqXHR );
						console.log( 'error: ', error );
					}
				});
			});

		</script>

	</main>

<?php get_footer( 'series' ); ?>
