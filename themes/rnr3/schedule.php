<?php
	/* Template Name: Schedule of Events */
	get_header();

	$parent_slug = the_parent_slug();
	rnr3_get_secondary_nav( $parent_slug );
	$qt_lang = rnr3_get_language();

	if( $qt_lang['lang'] == 'es' ) {
		$pagetitle = 'Programa';
	} elseif( $qt_lang['lang'] == 'pt' ) {
		$pagetitle = 'Programa';
	} else {
		$pagetitle = 'Schedule';
	}

	if ( false === ( $schedule_query = get_transient( 'schedule_query_results-' . $qt_lang['lang'] ) ) ) {
		$args = array(
			'post_type'			=> 'schedule',
			'meta_key'			=> '_rnr3_soe_date',
			'orderby'			=> 'meta_value_num',
			'order'				=> 'ASC',
			'posts_per_page'	=> -1
		);
		$schedule_query = new WP_Query( $args );
		set_transient( 'schedule_query_results-' . $qt_lang['lang'], $schedule_query, 1 * MINUTE_IN_SECONDS );
	}

?>

	<!-- main content -->
	<main role="main" id="main">
		<section class="wrapper grid_2 offset240left">
			<div class="column sidenav">

				<div id="nav-anchor"></div>
				<h2><?php echo $pagetitle; ?></h2>

				<?php
					if ( $schedule_query->have_posts() ) {

						echo '<ul>';

							while( $schedule_query->have_posts() ) {
								$schedule_query->the_post();

								if( $qt_lang['lang'] == 'en' ) {
									$dayofweek = date( 'D', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
								} elseif( $qt_lang['lang'] == 'fr' ) {
									$dayofweek_fr = date( 'w', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
									$dayofweek = get_day_or_month_by_language( 'fr', $dayofweek_fr, 'day' );
								} elseif( $qt_lang['lang'] == 'es' ) {
									$dayofweek_es = date( 'w', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
									$dayofweek = get_day_or_month_by_language( 'es', $dayofweek_es, 'day' );
								} elseif( $qt_lang['lang'] == 'pt' ) {
									$dayofweek_pt = date( 'w', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
									$dayofweek = get_day_or_month_by_language( 'pt', $dayofweek_pt, 'day' );
								}

								if( $qt_lang['lang'] != 'en' ) {
									$date = date( 'd-m-y', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
								} else {
									$date = date( 'm-d-y', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
								}

								$start_time		= get_post_meta( $post->ID, '_rnr3_soe_start_time', TRUE );
								$end_time		= get_post_meta( $post->ID, '_rnr3_soe_end_time', TRUE );
								$timestamp		= date( 'Y-m-d', get_post_meta( $post->ID, '_rnr3_soe_date', TRUE ) );
								$morelinkurl	= get_post_meta( get_the_ID(), '_rnr3_more_link_url', TRUE );

								if( $morelinkurl != '' ) {
									$useURL = $morelinkurl;
								} else {
									$useURL = get_permalink();
								}

								echo '<li>
									<time datetime="'. $timestamp .'T'. date( 'H:i', strtotime( $start_time ) ) .'">
										'. $dayofweek .' / '. $date;

										// don't show time range unless both start & end are added
										if( $start_time != '' && $end_time != '' ) {

											if( $qt_lang['lang'] == 'en' ) {
												$start_time = date( 'g:i A', strtotime( get_post_meta( $post->ID, '_rnr3_soe_start_time', TRUE ) ) );
												$end_time   = date( 'g:i A', strtotime( get_post_meta( $post->ID, '_rnr3_soe_end_time', TRUE ) ) );

												echo ' / '.$start_time.' &ndash; '.$end_time;
											} elseif( $qt_lang['lang'] == 'fr' ) {
												$start_timehr   = date( 'G', strtotime( $start_time ) );
												$start_timemin  = date( 'i', strtotime( $start_time ) );
												$end_timehr     = date( 'G', strtotime( $end_time ) );
												$end_timemin    = date( 'i', strtotime( $end_time ) );

												echo '<br>'.$start_timehr.' h '.$start_timemin.' &agrave; '.$end_timehr.' h '.$end_timemin;
											} elseif( $qt_lang['lang'] == 'pt' || $qt_lang['lang'] == 'es' ) {
												$start_time   = date( 'G:i', strtotime( $start_time ) );
												$end_time     = date( 'G:i', strtotime( $end_time ) );

												echo ' / '. $start_time .' &ndash; '.$end_time;
											}

										} elseif( $start_time != '' ) {
											echo ' / '.$start_time;
										}
									echo '</time>
									<a href="#event-'.get_the_ID().'">'.get_the_title().'</a>
								</li>';
							}
							wp_reset_postdata();

						echo '</ul>';
					}
				?>
			</div>
			<div class="column">
				<div class="content">

					<?php
						if ( $schedule_query->have_posts() ) {
							while ( $schedule_query->have_posts() ) {
								$schedule_query->the_post();

								if( $qt_lang['lang'] == 'en' ) {
									$dayofweek = date( 'D', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
								} elseif( $qt_lang['lang'] == 'fr' ) {
									$dayofweek_fr = date( 'w', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
									$dayofweek = get_day_or_month_by_language( 'fr', $dayofweek_fr, 'day' );
								} elseif( $qt_lang['lang'] == 'es' ) {
									$dayofweek_es = date( 'w', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
									$dayofweek = get_day_or_month_by_language( 'es', $dayofweek_es, 'day' );
								} elseif( $qt_lang['lang'] == 'pt' ) {
									$dayofweek_pt = date( 'w', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
									$dayofweek = get_day_or_month_by_language( 'pt', $dayofweek_pt, 'day' );
								}

								if( $qt_lang['lang'] != 'en' ) {
									$date = date( 'd-m-y', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
								} else {
									$date = date( 'm-d-y', get_post_meta( get_the_ID(), '_rnr3_soe_date', TRUE ) );
								}

								$start_time		= get_post_meta( $post->ID, '_rnr3_soe_start_time', TRUE );
								$end_time		= get_post_meta( $post->ID, '_rnr3_soe_end_time', TRUE );
								$timestamp		= date( 'Y-m-d', get_post_meta( $post->ID, '_rnr3_soe_date', TRUE ) );
								$morelinkurl	= get_post_meta( get_the_ID(), '_rnr3_more_link_url', TRUE );

								if( $morelinkurl != '' ) {
									$useURL = $morelinkurl;
								} else {
									$useURL = get_permalink();
								}

								echo '<section class="event_item" id="event-'.get_the_ID().'">

									<h2 id="post-'. get_the_ID() .'">'. get_the_title() .'</h2>
									<time datetime="'.$timestamp.'T'.date( 'H:i', strtotime( $start_time ) ).'">
										'.$dayofweek.' / '.$date;

										// don't show time range unless both start & end are added
										if( $start_time != '' && $end_time != '' ) {

											if( $qt_lang['lang'] == 'en' ) {
												$start_time = date( 'g:i A', strtotime( get_post_meta( $post->ID, '_rnr3_soe_start_time', TRUE ) ) );
												$end_time   = date( 'g:i A', strtotime( get_post_meta( $post->ID, '_rnr3_soe_end_time', TRUE ) ) );
												echo ' / '.$start_time.' &ndash; '.$end_time;
											} elseif( $qt_lang['lang'] == 'fr' ) {
												$start_timehr	= date( 'G', strtotime( $start_time ) );
												$start_timemin	= date( 'i', strtotime( $start_time ) );
												$end_timehr		= date( 'G', strtotime( $end_time ) );
												$end_timemin	= date( 'i', strtotime( $end_time ) );
												echo ' / '.$start_timehr.' h '.$start_timemin.' &agrave; '.$end_timehr.' h '.$end_timemin;
											} elseif( $qt_lang['lang'] == 'pt' || $qt_lang['lang'] == 'es' ) {
												$start_time   = date( 'G:i', strtotime( $start_time ) );
												$end_time     = date( 'G:i', strtotime( $end_time ) );
												echo ' / '. $start_time .' &ndash; '.$end_time;
											}

										} elseif( $start_time != '' ) {
											echo ' / ' . $start_time;
										}

									echo '</time>';

									the_content();

									if( ( $moreinfo_url = get_post_meta( get_the_ID(), '_rnr3_more_link_url', 1 ) ) != '' ) {

										echo '<p><a href="'. $moreinfo_url .'">More Info</a></p>';

									}

									edit_post_link( 'edit', '<p class="post-edit">', '</p>', get_the_ID() );

								echo '</section>';
							}
							wp_reset_postdata();
						} else {
							echo 'Stay Tuned...';
						}
					?>
				</div>
			</div>
		</section>


		<script>
			/*
			jQuery( function( $ ) {
				$( "#accordion" ).accordion( {
					header:'h3',
					heightStyle: 'content'
				} );
			} );
			*/
			$( document ).ready( function(){

				/**
				 * This part does the "fixed navigation after scroll" functionality
				 * We use the jQuery function scroll() to recalculate our variables as the
				 * page is scrolled/
				$( window ).scroll( function(){
					var window_top = $( window ).scrollTop() + 50; // the "12" should equal the margin-top value for nav.stick
					var div_top = $( '#nav-anchor' ).offset().top;
						if ( window_top > div_top ) {
							$( '.sticky-nav' ).addClass( 'stick' );
						} else {
							$( '.sticky-nav' ).removeClass( 'stick' );
						}
				} );
				 */


				/**
				 * This part causes smooth scrolling using scrollto.js
				 * We target all a tags inside the nav, and apply the scrollto.js to it.
				 */
				$( ".sidenav a, .backtotop" ).click( function( evn ){
					evn.preventDefault();
					$( 'html,body' ).scrollTo( this.hash, this.hash );
				} );



				/**
				 * This part handles the highlighting functionality.
				 * We use the scroll functionality again, some array creation and
				 * manipulation, class adding and class removing, and conditional testing
				var aChildren = $( ".sticky-nav li" ).children(); // find the a children of the list items
				var aArray = []; // create the empty aArray
				for ( var i=0; i < aChildren.length; i++ ) {
					var aChild = aChildren[i];
					var ahref = $( aChild ).attr( 'href' );
					aArray.push( ahref );
				} // this for loop fills the aArray with attribute href values

				$( window ).scroll( function(){
					var windowPos = $( window ).scrollTop(); // get the offset of the window from the top of page
					var windowHeight = $( window ).height(); // get the height of the window
					var docHeight = $( document ).height();

					for ( var i=0; i < aArray.length; i++ ) {
						var theID = aArray[i];
						var divPos = $( theID ).offset().top; // get the offset of the div from the top of page
						var divHeight = $( theID ).height(); // get the height of the div in question
						if ( windowPos >= ( divPos - 1 ) && windowPos < ( divPos + divHeight -1 ) ) {
							$( "a[href='" + theID + "']" ).addClass( "nav-active" );
						} else {
							$( "a[href='" + theID + "']" ).removeClass( "nav-active" );
						}
					}

					if( windowPos + windowHeight == docHeight ) {
						if ( !$( ".sticky-nav li:last-child a" ).hasClass( "nav-active" ) ) {
							var navActiveCurrent = $( ".nav-active" ).attr( "href" );
							$( "a[href='" + navActiveCurrent + "']" ).removeClass( "nav-active" );
							$( ".sticky-nav li:last-child a" ).addClass( "nav-active" );
						}
					}
				} );
				*/
			} );

		</script>

	</main>

<?php get_footer(); ?>
