<?php
	/* Template Name: VIP - Vegas */
	get_header();

	$parent_slug = the_parent_slug();
	rnr3_get_secondary_nav( $parent_slug );

	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$prefix = '_rnr3_';
	//adding back in 'high_roller::Race Weekend',
	$tier_array = array(
		'reception::Friday Night',
		'silver::Pre-Race',
		'gold::Race Day',
		'platinum::Race Weekend',
		'high_roller::Race Weekend',
	);
	$perks_array = array(
		'perk1'		=> 'Friday Night VIP Reception',
		'perk2'		=> 'Hotel',
		'perk3'		=> 'Airport Transportation',
		'perk4'		=> 'Sponsor Swag Pack',
		'perk5'		=> 'Pre-Race Transportation to the Start',
		'perk6'		=> 'Expo Packet Pick Up',
		'perk7'		=> 'Start Line Experience',
		'perk8'		=> 'Pre-Race Food',
		'perk9'		=> 'Pre-Race Private Restrooms',
		'perk10'	=> 'Pre-Race Stretching Area',
		'perk11'	=> 'Pre-Race Heated Zone',
		'perk12'	=> 'Pre-Race Concert VIP Viewing Area',
		'perk13'	=> 'Private Gear Check',
		'perk14'	=> 'Hosted Bar',
		'perk15'	=> 'Finish Line Experience',
		'perk16'	=> 'Post-Race Catered Buffet',
		'perk17'	=> 'Post-Race Indoor Restrooms',
		'perk18'	=> 'Post-Race Massage',
		'perk19'	=> 'VIP Welcome Gift',
		'perk20'	=> 'VIP Finisher\'s Gift',
		'perk21'	=> 'Start to Finish Spectator Shuttle'
	);
	$high_roller_perks = $platinum_perks = $gold_perks = $reception_perks = $silver_perks = array();

?>

	<!-- main content -->
	<main role="main" id="main">
		<section class="wrapper">
			<div class="content">
				<?php
					if (have_posts()) : while (have_posts()) : the_post();
						echo '<h2>'. get_the_title(). '</h2>';
						the_content();
					endwhile; endif;
				?>
				<table class="responsive_table" cellspacing="0" cellpadding="0" border="0">
					<thead>
						<tr>
							<th></th>
							<th class="vip_hdr"><span class="vip_spectator">Reception<br><span class="vip_subtext">Friday</span></span></th>
							<th class="vip_hdr"><span class="vip_silver">Silver<br><span class="vip_subtext">Pre-Race</span></span></th>
							<th class="vip_hdr"><span class="vip_gold">Gold<br><span class="vip_subtext">Pre- &amp; Post-Race</span></span></th>
							<th class="vip_hdr"><span class="vip_platinum">Platinum<br><span class="vip_subtext">Weekend</span></span></th>
							<th class="vip_hdr"><span class="vip_high_roller">High Roller<br><span class="vip_subtext">Weekend</span></span></th>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach( $perks_array as $key => $perk ) {
								$chosen_perks_array = get_post_meta( get_the_ID(), $prefix . $key, 1 );

								if( !empty( $chosen_perks_array ) ){

									${$key . '_vip_tooltip'} = get_post_meta( get_the_ID(), $prefix . $key . '_tooltip', 1 );

									echo '<tr>
										<td class="vip_perk">';
											if( ${$key . '_vip_tooltip'} != '' ) {
												echo $perk .'<a href="#" class="toggle"><span class="icon-help-circled"></span></a>
												<div class="vip_tooltip">'. ${$key . '_vip_tooltip'} .'</div>';
											} else {
												echo $perk;
											}
										echo '</td>';

										/*
										 * check5 = reception
										 * check4 = silver
										 * check3 = gold
										 * check1 = platinum
										 * check0 = high roller
										 * sets array for the mobile tabs
										 */
										if( in_array( 'check5', $chosen_perks_array ) ) {
											echo '<td><span class="icon-ok-circled"></span></td>';
											$reception_perks[$key] = $perk;
										} else {
											echo '<td></td>';
										}

										if( in_array( 'check4', $chosen_perks_array ) ) {
											echo '<td><span class="icon-ok-circled"></span></td>';
											$silver_perks[$key] = $perk;
										} else {
											echo '<td></td>';
										}

										if( in_array( 'check3', $chosen_perks_array ) ) {
											echo '<td><span class="icon-ok-circled"></span></td>';
											$gold_perks[$key] = $perk;
										} else {
											echo '<td></td>';
										}

										if( in_array( 'check1', $chosen_perks_array ) ) {
											echo '<td><span class="icon-ok-circled"></span></td>';
											$platinum_perks[$key] = $perk;
										} else {
											echo '<td></td>';
										}

										if( in_array( 'check0', $chosen_perks_array ) ) {
											echo '<td><span class="icon-ok-circled"></span></td>';
											$high_roller_perks[$key] = $perk;
										} else {
											echo '<td></td>';
										}

									echo '</tr>';
								} // END !empty( $chosen_perks_array )
							}
						?>
						<tr class="vip_no_shade">
							<td class="vip_no_border"></td>

							<?php
								foreach( $tier_array as $tier ) {
									$tier_pieces		= explode( '::', $tier );
									$tier				= $tier_pieces[0];
									${$tier . '_url'}	= get_post_meta( get_the_ID(), $prefix . $tier . '-url', 1 );
									${$tier . '_cost'}	= str_replace( '.00', '', get_post_meta( get_the_ID(), $prefix . $tier . '-cost', 1 ) );

									echo '<td>';
										if( ${$tier . '_cost'} == '0' ) {
											echo '<div class="vip_pricing">Sold Out</div>';
										} else {
											echo '<div class="vip_pricing">$'. ${$tier . '_cost'} .'</div>';
										}

										if( ${$tier . '_url'} != '' ) {
											echo '<div><a class="'.$tier.' highlight" href="'. ${$tier . '_url'} .'">Purchase</a></div>';
										}
									echo '</td>';
								}
							?>
						</tr>
						<tr>
							<td class="vip_no_border"></td>
							<td class="vip_footnote" colspan="5">NOTE: VIP Packages do NOT include race entry.</td>
						</tr>
					</tbody>
				</table>


				<?php /* tabs for mobile */ ?>
				<div id="vip_tabs">
					<ul class="vip_tabs_nav">
						<li class="sp_tab"><a href="#tabs-reception">Reception<br><span class="vip_subtext">Friday Night</span></a></li>
						<li class="ag_tab"><a href="#tabs-silver">Silver<br><span class="vip_subtext">Pre-Race</span></a></li>
						<li class="au_tab"><a href="#tabs-gold">Gold<br><span class="vip_subtext">Race Day</span></a></li>
						<li class="pt_tab"><a href="#tabs-platinum">Platinum<br><span class="vip_subtext">Race Weekend</span></a></li>
						<li class="hr_tab"><a href="#tabs-high_roller">High Roller<br><span class="vip_subtext">Race Weekend</span></a></li>
					</ul>

					<?php
						foreach( $tier_array as $tier ) {
							$tier_pieces	= explode( '::', $tier );
							$tier			= $tier_pieces[0];

							echo '<div id="tabs-'.$tier.'">';

								if( ${$tier . '_cost'} != '0' ) {
									echo '<h3>'. ucwords( str_replace ( "_" , " " , $tier) ) .' &mdash; $'. ${$tier . '_cost'} .'</h3>';
								} else {
									echo '<h3>'. ucwords( str_replace ( "_" , " " , $tier) ) .' &mdash; Sold Out</h3>';
								}

								echo '<p>'. $tier_pieces[1] .'</p>
								<ul>';

									foreach( ${$tier.'_perks'} as $key => $tab_perk ) {
										echo '<li>';
											// show link and toggle if a tooltip is written
											if( ${$key . '_vip_tooltip'} != '' ) {
												echo $tab_perk .'<a href="#" class="toggle"><span class="icon-help-circled"></span></a>
												<div class="vip_tooltip">'. ${$key . '_vip_tooltip'} .'</div>';
											} else {
												echo $tab_perk;
											}
										echo '</li>';
									}

								echo '</ul>';

								if( ${$tier . '_cost'} != '0' ) {
									echo '<div class="vip_purchase"><a class="'.$tier.' highlight" href="'. ${$tier . '_url'} .'">Purchase &mdash; $'. ${$tier . '_cost'} .'</a></div>';
								} else {
									echo '<div class="vip_purchase"><span class="'.$tier.' highlight">Sold Out</span></div>';
								}

							echo '</div>';

						}
					?>
				</div>
				<p class="vip_footnote_mobile">NOTE: VIP Packages do NOT include race entry.</p>
			</div>
		</section>
	</main>


	<script>
		$(function() {
			$( '#vip_tabs' ).tabs();
		});

		$(document).ready(function() {
			$(".toggle").click(function(e){
				$(this).nextAll(".vip_tooltip").first().slideToggle();
				e.preventDefault();
			});
		});

	</script>

<?php get_footer(); ?>
