<?php
	/**
	 * Template Name: FZ VR - Search & Results
	 * Notes: perhaps make `fz_results_grid` & `finisher-zone-leaderboard` & `fz_event_hdr` a function
	 */

	get_header( 'oneoffs' );
	$prefix = '_rnr3_';
	require_once 'functions_fz_vr.php';

	$redirecturl	= $_SERVER['REDIRECT_URL'];

	$bib			= $resultspage = $resultpage = '';
	$isarchive		= 0;

	if( isset( $_GET['archive'] ) && is_numeric( $_GET['archive'] ) ) {
		$isarchive = $_GET['archive'];
	}

	if( isset( $_GET['c_id'] ) && is_numeric( $_GET['c_id'] ) && strlen( $_GET['c_id'] ) == 11 ) {
		$c_id = $_GET['c_id'];
	} else {
		$c_id = '';
	}

	if( isset( $_GET['resultpage'] ) && is_numeric( $_GET['resultpage'] ) ) {
		$resultpage = $_GET['resultpage'];
	}

	if( isset( $_GET['resultspage'] ) && is_numeric( $_GET['resultspage'] ) ) {
		$resultspage = $_GET['resultspage'];

		if( isset( $_GET['perpage'] ) && is_numeric( $_GET['perpage'] ) ) {
			$perpage = $_GET['perpage'];
		}

		$firstname	= $_GET['firstname'];
		$lastname	= $_GET['lastname'];

		$criteria = array(
			'resultspage'	=> $resultspage,
			'firstname'		=> trim( $firstname ),
			'lastname'		=> trim( $lastname ),
			'perpage'		=> $perpage
		 );

	}

	$market = get_market2();
	if ( false === ( $event_info = get_transient( 'event_info_'.$market ) ) ) {
		$event_info = rnr3_get_event_info( $market, true );
	}

	// find out where this event takes place
	$mexico_race	= $american_race = false;
	$american_race	= true;

	$qt_lang		= rnr3_get_language();
	$qt_status		= $qt_lang['enabled'] == 1 ? true : false;
	$qt_cur_lang	= $qt_lang['lang'];

	include 'languages.php';

	$front_page_ID		= get_option( 'page_on_front' );
	$presenting_sponsor	= '';

	if ( ( $presenting_sponsor_img = get_post_meta( $front_page_ID, $prefix . 'sponsor_img', true ) ) != '' ) {
		$presenting_sponsor = '<img src="'. $presenting_sponsor_img .'">';

		$presenting_sponsor_url = get_post_meta( $front_page_ID, $prefix . 'sponsor_url', true );

		if ( $presenting_sponsor_url != ''  ) {
			$presenting_sponsor	= '<a href="'. $presenting_sponsor_url .'" target="_blank">'. $presenting_sponsor .'</a>';
		}

		$presenting_sponsor = '<div class="presented_by"><span>'. $presented_by_txt .'</span>'. $presenting_sponsor_img .'</div>';
	}

	/**
	 * set time to PDT so we can accurately disable the button at 11:59:59pm our time
	 */
	$timezone_offset	= ( timezone_offset_get( timezone_open( "America/Los_Angeles" ), new DateTime() ) ) / 3600;
	$today_unix			= strtotime( $timezone_offset . ' hours' );
?>

<main role="main" id="main" class="no-hero finisher_zone_search_results">

	<nav id="subnav">
		<section class="wrapper">
			<a href="<?php echo esc_url( home_url() ); ?>" class="fz-home">Finisher Zone</a>
			<div class="subnav-menu-label">Menu</div>
			<?php echo $presenting_sponsor; ?>
			<div class="subnav-menu-primary-event-container">
				<ul class="subnav-menu">
					<li><a href="<?php echo esc_url( home_url() ); ?>/#past_results"><?php echo trim( $results_txt ); ?></a></li>
					<li><a href="<?php echo esc_url( home_url() ); ?>/#badges"><?php echo $finisher_badges_txt; ?></a></li>
					<li><a href="<?php echo esc_url( home_url() ); ?>/#more_features"><?php echo $features_txt; ?></a></li>
				</ul>
			</div>
		</section>
	</nav>


	<?php if( $isarchive == 0 && $c_id != '' && $resultpage != '' ) {
		// event chosen, year chosen, runner selected ( individual result )
		$dbConnLink		= dbConnectVR( 'results' );
		$rStats			= getRunnerStatsVR( $c_id, $dbConnLink );
		$rEvents		= getRunnerEventsVR( $c_id, $dbConnLink );
		$rOInfo			= getRunnerOtherInfoVR( $c_id, $dbConnLink );
		$rIGHandle		= getRunnerInstagrmaVR( $c_id, $dbConnLink );
		$upload_dir		= wp_upload_dir(); // need only once
		$displayIG		= 0;
		$hashtag		= "#RnRVirtualRun";
		$share_counter	= 0;

		if( !empty( $rStats ) ) {
			$partOfRelay = 0;
			$rStatsC = array( $rStats );

			// loop through all
			foreach( $rStatsC as $keyRunner => $rStats ) {

				// check to see if lucky finisher
				$luckyFinisher	= isLuckyFinisherVR( $c_id );
				// what's next
				$whatsNextInfo	= getWhatsNextVR();
				// discounts and offerings
				$dandOInfo		= getDiscountOffersVR();
				// Badges
				// $badge_tally	= getTally( $subEventInfoToUse, 'fk_badges_id_' );
				$badge_tally[0]	= 1; ?>

				<section id="finisher-zone-results">
					<section class="wrapper grid_2 offset240left">
						<div class="column sidenav stickem">
							<div id="nav-anchor"></div>
							<nav class="sticky-nav">
								<ul>
									<li><a href="#finisher-info"><img src="<?php echo bloginfo( 'template_directory' ); ?>/img/finisher-zone/i-info.svg" alt="<?php echo $runner_info_txt; ?>"><?php echo $runner_info_txt; ?></a></li>

									<?php if( !empty( $rIGHandle ) AND $displayIG == 1 ) : ?>
										<li><a href="#finisher-photos"><img src="<?php echo bloginfo( 'template_directory' ); ?>/img/finisher-zone/i-photos.svg" alt="<?php echo $photos_txt; ?>"><?php echo $photos_txt; ?></a></li>
									<?php endif;

									if( $badge_tally[0] > 0 ) : ?>
										<li><a href="#finisher-badges"><img src="<?php echo bloginfo( 'template_directory' ); ?>/img/finisher-zone/i-earned-badges.svg" alt="<?php echo $earned_badges_txt; ?>"><span class="hidden-sm">My Virtual Badges</span><span class="hidden-lrg"><?php echo $badges_txt; ?></span></a></li>
									<?php endif;

									if( !empty( $whatsNextInfo ) ) { ?>
										<li><a href="#finisher-whatsnext"><img src="<?php echo bloginfo( 'template_directory' ); ?>/img/finisher-zone/i-whats-next.svg" alt="<?php echo $whats_next_txt; ?>"><?php echo $whats_next_txt; ?></a></li>
									<?php }

									if( !empty( $dandOInfo ) ) { ?>
										<li><a href="#finisher-assessment"><img src="<?php echo bloginfo( 'template_directory' ); ?>/img/finisher-zone/i-discounts.svg" alt="Rock 'n' Roll Virtual Run Discounts and Offers"><span class="hidden-sm">Discounts and Offers</span><span class="hidden-lrg">Offers</span></a></li>
									<?php } ?>

								</ul>
							</nav>
						</div>


						<div class="column">
							<div class="content">

								<div class="vr-logo">
									<img src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/finisher-zone/vr-mb-general.svg">
								</div>

								<section id="finisher-info">
									<h3><?php echo $runner_info_txt; ?></h3>
									<div class="fz_event_hdr">
										<h3><?php echo 'Rock \'n\' Roll Virtual Run Series'; ?> </h3>
										<p><?php //date display? ?></p>
									</div>


									<ul class="fz_runner_info fz_bullets">
										<li>
											<span class="fz_label"><?php echo $name_txt; ?></span>
											<span><?php echo $rStats['FIRSTNAME'].' '.$rStats['LASTNAME']; ?></span>
										</li>
									</ul>


									<ul class="fz_runner_vitals fz_bullets fz_bullets_3">
										<li class="gender">
											<span class="fz_label"><?php echo $gender_txt; ?></span>
											<span><?php echo $rStats['GENDER'];?></span>
										</li>

										<li class="age">
											<span class="fz_label"><?php echo $age_txt; ?></span>
											<span>
												<?php //determine age
													$r_b_year	= substr( $rStats['BIRTHDATE'],0, 4 );
													$r_b_month	= substr( $rStats['BIRTHDATE'],4, 2 );
													$r_b_day	= substr( $rStats['BIRTHDATE'], -2 );

													if( date( 'm' ) < $r_b_month ) {
														//no bd yet this year
														$age = date( 'Y' ) - $r_b_year - 1;
													} elseif( date( 'm' ) == $r_b_month AND date( 'd' ) > $r_b_day ) {
														//no bd yet this year
														$age = date( 'Y' ) - $r_b_year - 1;
													} else {
														$age = date( 'Y' ) - $r_b_year;
													}
													echo $age;
												?>
											</span>
										</li>

										<li class="hometown">
											<span class="fz_label"><?php echo $hometown_txt; ?></span>
											<span>
												<?php if( !empty( $rStats['CITY'] ) ) {
													echo $rStats['CITY'].', ';
												}
												if( !empty( $rStats['STATE'] )  AND $rStats['STATE'] != 'NULL') {
													echo $rStats['STATE'];
												}
												echo ' ';

												if( !empty( $rStats['PROVINCE'] ) AND $rStats['PROVINCE'] != 'NULL') {
													echo $rStats['PROVINCE'];
												}
												echo ' ';

												if( !empty( $rStats['COUNTRYCODE'] ) ) {
													if( !empty( $rStats['STATE'] ) OR !empty( $rStats['PROVINCE'] ) ) {
														echo ' ';
													}
													echo $rStats['COUNTRYCODE'];
												} ?>
											</span>
										</li>
									</ul>

									<?php
										/**
										 * Lucky Finisher
										 */
										if( is_array( $luckyFinisher ) ) { ?>
											<ul class="fz_bullets fz_winner_alert">
												<li>
													<a href="#finisher-badges" class="scrolly" style="text-decoration:underline;"><?php echo $rStats['FIRSTNAME']; ?>, you are a Virtual Run Prize Pack Winner</a>
												</li>
											</ul>
										<?php }
									?>
								</section>

								<?php
									// Use for Instagram?
									if( !empty( $rIGHandle ) ): ?>
										<section id="finisher-photos">
											<h3><?php echo $hashtag.' '.$photos_txt; ?></h3>
											<div class="fz_badgelist">
											<?php
											//$pixleeStuff = getPixleeImages( 'llldcreate' );
											$pHandle = str_replace ( "@" , "" , $rIGHandle['answer'] );
											//echo $pHandle;
											$pixleeStuff = getPixleeImages( $pHandle );
											if(isset($pixleeStuff->total) AND $pixleeStuff->total > 0){
												$displayIG = 1;
												include_once('php-emoji/emoji.php');
											}

											if( $displayIG == 1):?>
											<ul>
											<?php foreach( $pixleeStuff->data as $key => $value ){
												//echo $value->photo_title."<br/>";
												echo "<li class=\"smaller_txt fz_socialitem\">";
												echo "<a href=\"".$value->platform_link."\" target=\"_blank\"><img src=\"".$value->medium_url."\" alt=\"\"></a>";
												if(strlen($value->photo_title) <= 150) {
													echo "<p>".emoji_html_to_unified($value->photo_title)."</p>";
												} else {
													$y = substr(emoji_html_to_unified($value->photo_title),0,147) . '...';
													echo "<p>".$y."</p>";
												}
												echo "</li>";
											}?>
											</ul>
											</div>
											<?php else:?>
											<a href="https://www.instagram.com/explore/tags/rnrvirtualrun/" target="_blank">Share your journey through Instagram using <?php echo $hashtag;?></a>
											<?php endif;?>
										</section>
									 <?php endif;


									/**
									 * my virtual badges section
									 */
								?>
								<section id="finisher-badges">
									<h3>My Virtual Badges</h3>

									<?php $totalEventCount = 0;
									foreach( $rEvents as $key => $value ) :
										/**
										 * get making the band badges
										 */
										$eBadges = getFZBadgesVR( $value['pk_event_id'] );

										$nomargin = 'accordion-nomargin';
										//echo count( $eBadges );

										if( count( $eBadges ) > 0 ) :
										$making_band_badge_ids = array();?>

											<?php  if($totalEventCount >= 1){ echo "</div>"; }?>

											<div class="accordion <?php echo $nomargin; ?>">
												<div class="accordion-section custom" style="position: relative;">

													<?php
														$series_name = ( isset( $value['series_name'] ) AND !empty( $value['series_name'] ) ) ? $value['series_name'] . ' | ': '';

														/**
														 * check if event is done based on server time
														 * flag as 0 (not complete) or 1 (complete)
														 */
														if( strtotime( $value['date_end'] ) > $today_unix ) {
															$complete = 0;
														} else {
															$complete = 1;
														}

													?>

													<div class="accordion-section-title" data-reveal="#panel-<?php echo $key; ?>"><?php echo $series_name . $value['name']; ?></div>

													<div class="accordion-section-content" id="panel-<?php echo $key; ?>">
														<div class="fzvr_badgelist">
															<ul>
																<?php foreach( $eBadges as $key => $bd_value ) :
																	// if qualified for badge
																	if( isQualforBadge( $bd_value, $rOInfo, $luckyFinisher, "main", $c_id ) == 1 ) :
																	$making_band_badge_ids[]	= $bd_value['pk_vr_badage_id'];
																	?>
																		<li class="smaller_txt">
																			<figure class="badge_alignleft">
																				<img src="<?php echo ($complete == 0 ? $bd_value['image_nf_display'] : $bd_value['image_display'] ); ?>" alt="">
																			</figure>
																			<h3>
																				<?php
																					$badge_title	= $bd_value['display_title'];
																					$badge_text		= $bd_value['display_text'];
																					$badge_text		= ($complete == 0) ? $bd_value['nf_display_text'] : $bd_value['display_text'];
																					echo $badge_title;
																				?>

																			</h3>

																			<p><?php echo $badge_text; ?></p>

																			<?php
																				/**
																				 * social sharing for badges
																				 * only show if event is complete
																				 */
																				$share_url = "www.runrocknroll.com" . $_SERVER['REQUEST_URI'];
																				$share_counter++;

																				if( $complete != 0 ) : ?>

																					<div class="fz_badge_social">
																						<figure class="hidden"><img src="<?php echo $bd_value['image_social']; ?>" alt=""></figure>

																						<a href="<?php echo get_permalink( get_page_by_title( 'FB Share' )->ID ); ?>?type=badge&image=<?php echo $bd_value['image_social']; ?>&itemName=<?php echo $badge_title; ?>&event=<?php echo $value['name']; ?>" class="fb-share-badge"><span class="icon-facebook"></span></a>

																						<a href="<?php echo $bd_value['image_social']; ?>" download><span class="icon-instagram"></span></a>

																						<span class="post-widget" id="badge_<?php echo $share_counter; ?>" data-fz-url="<?php echo $share_url; ?>" data-fz-title="<?php echo shareMessage( $qt_lang, $badge_title, 'badge', $hashtag ); ?>" data-pin-image="<?php echo $bd_value['image_social']; ?>"></span>

																						<?php /*
																							<a href="https://twitter.com/intent/tweet?text=<?php //echo urlencode( shareMessage( $qt_lang, $badge_title, 'badge', $hashtag ) );?>&via=RunRocknRoll&hashtags=<?php //echo ltrim( $hashtag, '#' ); ?>&wrap_links‌​=true&url=<?php //echo $share_url; ?>" target="_blank" class="twitter-share" data-item-shared="badge"><span class="icon-twitter"></span></a>

																							<a class="pinterest-share" data-pin-do="buttonPin" data-pin-custom="true" href="//www.pinterest.com/pin/create/button/?url=<?php //echo $share_url; ?>&media=<?php //echo urlencode( $bd_value['image_social'] ); ?>&description=<?php //echo urlencode( "I earned the ".$badge_title );?>" count-layout="none"><img src="<?php //echo get_bloginfo( 'template_directory' );?>/img/40x40_pinterest.png" alt="Share on Pinterest"></a>
																						*/ ?>
																					</div>

																				<?php else: ?>

																					<p class="center"><a class="cta" href="http://www.runrocknroll.com/virtual-run" target="_blank">Learn More</a></p>

																				<?php endif;
																			?>
																		</li>

																	<?php endif; // end not qualified
																endforeach; ?>
															</ul>

														</div>
													</div>
													<?php if( $complete == 1 ) { ?>
														<form name="FinCert" method="post" action="<?php echo home_url( '/wp-content/html2pdf/fz_certs/FZVRPDFGen.php' ); ?>">
															<input type="hidden" name="raceID" value="<?php echo $value['pk_event_id']; ?>">
															<input type="hidden" name="ParticipantName" value="<?php echo $rStats['FIRSTNAME'] . ' ' . $rStats['LASTNAME']; ?>">
															<input type="hidden" name="earnedBadges" value="<?php echo implode(',', $making_band_badge_ids); ?>">

															<a class="download_cert" href="####" target="_blank">Download Certificate <img src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/finisher-zone/i-download-small.svg"></a>
														</form>
													<?php } ?>

												</div>
											</div>

											<?php $nomargin = '';

										endif;


										/**
										 * get individual event badges: guitar, drum, mic
										 */
										$otherBadges = getChildEventAndBadgesVR( $value['pk_event_id'], $dbConnLink );

										// if other badges and events - display them
										if( count( $otherBadges ) > 0 ) :


											echo '<div class="accordion '. $nomargin .'">';

												$lastEId = 0;
												$lastWasComplete = 0;

												foreach( $otherBadges as $key => $bdo_value ):

													++$totalEventCount;
													/**
													 * check if event is done based on server time
													 * flag as 0 (not complete) or 1 (complete)
													 */
													if( strtotime( $bdo_value['E_DateE'] ) > $today_unix ) {
														$complete = 0;
													} else {
														$complete = 1;
													}

													/**
													 * close or open up accordion section content divs
													 */
													if( $lastEId == 0 OR $lastEId != $bdo_value['fk_vr_event_id'] ) :

														/**
														 * close off accordion section content
														 */
														if( $lastEId != 0 AND $lastEId != $bdo_value['fk_vr_event_id']) :
																	echo '</ul>
																</div>
															</div>';
															if( $lastWasComplete == 1 ) {

																echo '<form name="FinCert" method="post" action="' . home_url( '/wp-content/html2pdf/fz_certs/FZVRPDFGen.php' ) . '">
																		<input type="hidden" name="raceID" value="' . $lastEId . '">
																		<input type="hidden" name="ParticipantName" value="' . $rStats['FIRSTNAME'] . ' ' . $rStats['LASTNAME'] . '">
																		<input type="hidden" name="earnedBadges" value="' . implode(',', $other_badges_ids) . '">

																		<a class="download_cert" href="####" target="_blank">Download Certificate <img src="'. get_bloginfo( 'stylesheet_directory' ) .'/img/finisher-zone/i-download-small.svg"></a>
																	</form>';

															}
														echo '</div>';
															//reset badges
															$other_badges_ids = array();
														endif;

														/**
														 * open up accordion section content
														 */
														$other_series_name = ( isset( $bdo_value['E_SName'] ) AND !empty( $bdo_value['E_SName'] ) ) ? $bdo_value['E_SName'] . ' | ' : '';

														echo '<div class="accordion-section" style="position: relative;">

															<div class="accordion-section-title" data-reveal="#otherpanel-'. $key . $totalEventCount.'">
																'. $other_series_name . $bdo_value['E_Title'] .'
															</div>';

															echo '<div class="accordion-section-content" id="otherpanel-'. $key . $totalEventCount.'">
																<div class="fzvr_badgelist">
																	<ul>';

													endif;


													// if qualified for badge
													if( isQualforBadge( $bdo_value, $rOInfo, $luckyFinisher, "child", $c_id ) == 1 ) :
														$other_badges_ids[]	= $bdo_value['pk_vr_badage_id'];
													?>
														<li class="smaller_txt">
															<figure class="badge_alignleft">
																<img src="<?php echo ($complete == 0 ? $bdo_value['image_nf_display'] : $bdo_value['image_display']); ?>" alt="">
															</figure>

															<h3>
																<?php
																	$badge_title	= $bdo_value['display_title'];
																	$badge_text		= ($complete == 0) ? $bdo_value['nf_display_text'] : $bdo_value['display_text'];
																	echo $badge_title;
																?>

															</h3>

															<p><?php echo $badge_text; ?></p>

															<?php
																/**
																 * social sharing for badges
																 * only show if event is complete
																 */
																$share_url = "www.runrocknroll.com" . $_SERVER['REQUEST_URI'];
																$share_counter++;

																if( $complete != 0 ) : ?>
																	<div class="fz_badge_social">
																		<figure class="hidden">
																			<img src="<?php echo $bdo_value['image_social']; ?>" alt="">
																		</figure>

																		<a href="<?php echo get_permalink( get_page_by_title( 'FB Share' )->ID ); ?>?type=badge&image=<?php echo $bdo_value['image_social']; ?>&itemName=<?php echo $badge_title; ?>&event=<?php echo $bdo_value['E_Title']; ?>" class="fb-share-badge"><span class="icon-facebook"></span></a>

																		<a href="<?php echo $bdo_value['image_social']; ?>" download><span class="icon-instagram"></span></a>

																		<span class="post-widget" id="badge_<?php echo $share_counter; ?>" data-fz-url="<?php echo $share_url; ?>" data-fz-title="<?php echo shareMessage( $qt_lang, $badge_title, 'badge', $hashtag ); ?>" data-pin-image="<?php echo $bd_value['image_social']; ?>"></span>

																		<?php /*
																			<a href="https://twitter.com/intent/tweet?text=<?php //echo urlencode( shareMessage( $qt_lang, $badge_title, 'badge', $hashtag ) );?>&via=RunRocknRoll&hashtags=<?php //echo ltrim( $hashtag, '#' ); ?>&wrap_links‌​=true&url=<?php //echo $share_url; ?>" target="_blank" class="twitter-share" data-item-shared="badge"><span class="icon-twitter"></span></a>

																			<a class="pinterest-share" data-pin-do="buttonPin" data-pin-custom="true" href="//www.pinterest.com/pin/create/button/?url=<?php //echo $share_url; ?>&media=<?php //echo urlencode( $bd_value['image_social'] ); ?>&description=<?php //echo urlencode( "I earned the ".$badge_title );?>" count-layout="none"><img src="<?php //echo get_bloginfo( 'template_directory' );?>/img/40x40_pinterest.png" alt="Share on Pinterest"></a>
																		*/ ?>
																	</div>
																<?php else : ?>

																	<p class="center"><a class="cta" href="http://www.runrocknroll.com/virtual-run" target="_blank">Learn More</a></p>

																<?php endif;
															?>
														</li>
													<?php endif; // end is qualified

													$lastEId = $bdo_value['fk_vr_event_id'];
													$lastWasComplete = $complete;

												endforeach;

											echo '</div>';
										echo '</div>';
										if( $complete == 1 ) {
												echo '<form name="FinCert" method="post" action="' . home_url( '/wp-content/html2pdf/fz_certs/FZVRPDFGen.php' ) . '">
													<input type="hidden" name="raceID" value="' . $bdo_value['fk_vr_event_id'] . '">
													<input type="hidden" name="ParticipantName" value="' . $rStats['FIRSTNAME'] . ' ' . $rStats['LASTNAME'] . '">
													<input type="hidden" name="earnedBadges" value="' . implode(',', $other_badges_ids) . '">

													<a class="download_cert" href="####" target="_blank">Download Certificate <img src="'. get_bloginfo( 'stylesheet_directory' ) .'/img/finisher-zone/i-download-small.svg"></a>
													</form>';

										}
										echo '</div>';

										endif; //end if other badges and events - display them

									endforeach; //End badges ?>

								</section>


								<?php
									/**
									 * what's next section
									 */

									if( !empty( $whatsNextInfo ) ) : ?>
										<section id="finisher-whatsnext">
											<h3><?php echo $whats_next_txt; ?></h3>

											<div class="fz_whatsnext_wrapper">
												<?php
													$wNCountMinus = count( $whatsNextInfo ) - 1;

													foreach( $whatsNextInfo as $keyWNL => $valueWNL ) {
														if( $keyWNL == 0 ): ?>
															<ul class="fz_whatsnext">
														<?php endif;

														if( $keyWNL <= 2 ): ?>

															<li class="smaller_txt">
																<figure><img src="<?php echo $valueWNL['image_display']; ?>" alt=""></figure>
																<h3><?php echo $valueWNL['display_title']; ?></h3>
																<p><?php echo $valueWNL['display_text']; ?></p>

																<?php if( !empty( $valueWNL['link_text'] ) ): ?>

																	<p class="center"><a class="cta" href="<?php echo $valueWNL['link']; ?>"><?php echo $valueWNL['link_text']; ?></a></p>

																<?php endif;?>

															</li>

														<?php endif;


														if( $keyWNL == 2 ): ?>
															</ul>
														<?php endif;


														if( $keyWNL == 3 ): ?>
															<ul class="fz_whatsnext_bottom">
														<?php endif;

															if( $keyWNL > 2 ): ?>
																<li class="smaller_txt">
																	<figure><img src="<?php echo $valueWNL['image_display']; ?>" alt=""></figure>
																	<h3><?php echo $valueWNL['display_title']; ?></h3>
																	<p><?php echo $valueWNL['display_text']; ?></p>

																	<?php if( !empty( $valueWNL['link_text'] ) ): ?>
																		<p class="center"><a class="cta" href="<?php echo $valueWNLo['link']; ?>"><?php echo $valueWNL['link_text']; ?></a></p>
																	<?php endif;?>

																</li>

															<?php endif;

														if( $keyWNL == $wNCountMinus AND $keyWNL > 2): ?>
															</ul>
														<?php endif; ?>

													<?php }
												?>
											</div>

										</section>
									<?php endif;


									/**
									 * discounts and offers section
									 */
									if( !empty( $dandOInfo ) ) : ?>
										<section id="finisher-assessment">
											<h3>Discounts and Offers</h3>
											<div class="fz_assessment_wrapper">

												<div class="fz_offers">
													<?php
														$dNOCountMinus = count( $dandOInfo ) - 1;

														foreach( $dandOInfo as $keyDNO => $valueDNO ) { ?>

															<div class="fz_offer smaller_txt">
																<figure><img src="<?php echo $valueDNO['image_display']; ?>" alt=""></figure>
																<h3><?php echo $valueDNO['display_title']; ?></h3>
																<p><?php echo $valueDNO['display_text']; ?></p>

																<?php if( !empty( $valueDNO['link_text'] ) ):?>
																	<p class="alignright"><a class="cta" href="<?php echo $valueDNO['link']; ?>"><?php echo $valueDNO['link_text']; ?></a></p>
																<?php endif;?>

															</div>

														<?php }
													?>

												</div>
											</div>
										</section>

									<?php endif;
								?>

							</div>
						</div>

					</section>

					<script>
						$( document ).ready( function(){
							/**
							 * This part causes smooth scrolling using scrollto.js
							 * We target all a tags inside the nav, and apply the scrollto.js to it.
							 */
							$( ".sticky-nav a, .scrolly" ).click( function( evn ){
								evn.preventDefault();
								$( 'html,body' ).scrollTo( this.hash, this.hash );
							} );

							/*$( 'main' ).stickem( {
								container: '.offset240left',
							} );*/

							/**
							 * This part handles the highlighting functionality.
							 * We use the scroll functionality again, some array creation and
							 * manipulation, class adding and class removing, and conditional testing
							 */
							var aChildren = $( ".sticky-nav li" ).children(); // find the a children of the list items

							var aArray = []; // create the empty aArray
							for ( var i=0; i < aChildren.length; i++ ) {
								var aChild = aChildren[i];
								var ahref = $( aChild ).attr( 'href' );
								aArray.push( ahref );
							} // this for loop fills the aArray with attribute href values

							$( window ).scroll( function(){
								var windowPos = $( window ).scrollTop(); // get the offset of the window from the top of page
								var windowHeight = $( window ).height(); // get the height of the window
								var docHeight = $( document ).height();

								for ( var i=0; i < aArray.length; i++ ) {
									var theID = aArray[i];
									var divPos = $( theID ).offset().top; // get the offset of the div from the top of page
									var divHeight = $( theID ).height(); // get the height of the div in question
									if ( windowPos >= ( divPos - 1 ) && windowPos < ( divPos + divHeight -1 ) ) {
										$( "a[href='" + theID + "']" ).addClass( "nav-active" );
									} else {
										$( "a[href='" + theID + "']" ).removeClass( "nav-active" );
									}
								}

								if( windowPos + windowHeight == docHeight ) {
									if ( !$( ".sticky-nav li:last-child a" ).hasClass( "nav-active" ) ) {
										var navActiveCurrent = $( ".nav-active" ).attr( "href" );
										$( "a[href='" + navActiveCurrent + "']" ).removeClass( "nav-active" );
										$( ".sticky-nav li:last-child a" ).addClass( "nav-active" );
									}
								}
							} );
						} );

					</script>
				</section>
			<?php }
		} else {
			echo '<section id="finisher-zone-results">
				<section class="wrapper grid_2 offset240left">
					<h2>Finisher Zone - Virtual Run</h2>
					<p>'. $no_runner_txt .'</p>
				</section>
			</section>';
		}

		dbClose( $dbConnLink );

	} elseif( $resultspage != '' ) {
		/* search results page */ ?>

		<section id="finisher-zone-search-results">
			<section class="wrapper">
				<div class="vr-logo">
					<img src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/finisher-zone/vr-mb-general.svg">
				</div>

				<table class="fz_rank_table fz_search_results_table">
					<tr>
						<th>Event</th>
						<th>Name</th>
					</tr>

					<?php
						$search1 = doSearchResultsVR( $criteria );
						if( count( $search1 ) == 0 ) {
							echo '<tr>
								<td colspan="2">No Runners Found</td>
							</tr>';
						} else {
							foreach( $search1 as $keySearch => $search_value ) {
								if( is_array( $search_value ) ) {

									$indiv_criteria = array(
										'c_id' => trim( $search_value['pk_customer_id'] ),
									);

									$searchstring = get_query_stringVR( $indiv_criteria, '', 1 );

									echo '<tr>
										<td>'. $search_value['EVENT_NAME'] .'</td>
										<td>
											<a href="'. $redirecturl .'?resultpage=1'. $searchstring .'">'. $search_value['FIRSTNAME'] .' '. $search_value['LASTNAME'] .'</a><br/>';
											if( empty( $search_value['PROVINCE'] ) ) {
												echo $search_value['CITY'].', '.$search_value['STATE'];
											} else {
												echo $search_value['PROVINCE'];
											}
											echo ' '.$search_value['COUNTRYCODE'];
										echo '</td>
									</tr>';
								}

							}
						}
					?>
				</table>


				<div class="fz_navigation">
					<?php
						// display range of runners over total runners
						$totalrunners = getTotalRunnersVR( $criteria );
					?>

					<div class="fz_nav_column">
						<?php
							echo $display_txt . ': ';

							if( $totalrunners[0] == 0 ) {
								echo '0-' . number_format( $totalrunners[0] );
							} elseif ( $totalrunners[0] <= $perpage ) {
								echo '1-' . number_format( $totalrunners[0] );
							} else {
								echo number_format( ( ( $resultspage - 1 ) * $perpage ) + 1 ) . '-';
								$lastRecord = $resultspage * $perpage;

								if ( $lastRecord > $totalrunners[0] ) {
									$lastRecord = $totalrunners[0];
								}

								echo number_format( $lastRecord );
							}
							echo ' / ' . number_format( $totalrunners[0] ) .' '. $runners_txt;
						?>
					</div>


					<div class="fz_nav_column">
						<?php
							/* display prev/next arrows based on resultspage */
							$searchstring	= get_query_stringVR( $criteria, 'resultspage' );
							$totalpages		= ceil( $totalrunners[0] / $perpage );

							if( $resultspage > $totalpages ) {
								$resultspage = $totalpages;
							}

							if( $totalpages > 0 ) {
								echo '<span>' . $page_txt .': ' . $resultspage . ' ' . $of_txt . ' ' . $totalpages . '</span>';
								// echo '<span>'. $page_txt .': '. $resultspage .' '. $of_txt .' ' . $totalpages .'</span>';

								echo '<ul class="fz_prevnext_links">';
									if( $resultspage > 1 ) {
										// prev link
										$resultspagePre	= $resultspage - 1;
										echo '<li class="fz_prev"><a href="'. $redirecturl .'?resultspage='. $resultspagePre . $searchstring .'">&laquo; '. $previous_txt .'</a></li>';
									}
									if( $resultspage < $totalpages ) {
										// next link
										$resultspageNext = $resultspage + 1;
										echo '<li class="fz_next"><a href="'. $redirecturl .'?resultspage='. $resultspageNext . $searchstring .'">'. $next_txt .' &raquo;</a></li>';
									}
								echo '</ul>';
							}
						?>

					</div>

					<?php
						$searchstring = get_query_string( $criteria, 'perpage' );
					?>

					<div class="fz_nav_column">
						<?php echo $results_per_page_txt; ?>: <a href="<?php echo $redirecturl . '?perpage=25'. $searchstring; ?>">25</a> | <a href="<?php echo $redirecturl . '?perpage=50'. $searchstring; ?>">50</a> | <a href="<?php echo $redirecturl . '?perpage=100'. $searchstring; ?>">100</a>
					</div>
				</div>


				<p class="center">
					<a href="<?php echo $redirecturl ?>" class="cta"><?php echo $search_again_txt; ?></a>
				</p>

			</section>
		</section>

	<?php } else {
		// default search away! ?>

		<section id="finisher-zone-search">
			<section class="wrapper">
				<div class="vr-logo">
					<img src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/finisher-zone/vr-mb-general.svg">
				</div>
				<h2><?php echo $search_results_txt; ?></h2>

				<div class="form_narrow fz_module">
					<form method="GET" action="" id="fz-search">
						<input type="hidden" name="resultspage" value="1">
						<input type="hidden" name="perpage" value="25">

						<fieldset>
							<ol>
								<li>
									<input type="text" placeholder="<?php echo $first_name_txt; ?>" value="" id="firstname" name="firstname" class="field" pattern=".{3,}" title="3 characters minimum">
								</li>
								<li>
									<input type="text" placeholder="<?php echo $last_name_txt; ?>" value="" id="lastname" name="lastname" class="field">
								</li>
							</ol>
						</fieldset>

						<fieldset>
							<button type="submit"><?php echo $search_txt; ?></button>
							<?php // get_correction_link( $qt_lang ); ?>
						</fieldset>
					</form>
				</div>
			</section>
		</section>

	<?php } ?>
</main>

<?php
	// script for pinterest share
	echo '<script async defer src="//assets.pinterest.com/js/pinit.js"></script>';
	//script for po.st
	$post_pubkey = rnr_get_option( 'rnrgv_post_pubkey' );
	echo '<script src="http://i.po.st/static/v3/post-widget.js#publisherKey='.$post_pubkey.'&retina=true" type="text/javascript"></script>';
	get_footer( 'series' );
?>
