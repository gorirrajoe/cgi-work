<?php
/* Template Name: TourPass - Splash Page (Series) */
  get_header('oneoffs');
?>
  <!-- main content -->
  <style>
    .logo_splash {
      margin-bottom:60px;
    }
    .container_tp {
      list-style:none;
      display:table;
      margin:auto;
      overflow:hidden;
      margin-bottom:60px;
    }
    .container_tp li {
      float:left;
      margin:0 30px;
    }
    a.cta_big {
      font-size:32px;
    }
  </style>
  <main role="main" id="main">
    <section class="wrapper center">

      <div class="logo_splash">
        <img src="<?php bloginfo('stylesheet_directory'); ?>/img/tourpass/tourpass-logo.png">
      </div>
      <div class="intro_text_splash">
        <h1>Start Your Rock 'n' Roll Tour!</h1>
        <?php echo apply_filters('the_content', get_post_meta (get_the_ID(), '_tourpass_splash_intro_txt', true)); ?>
        <h3>Select Year</h2>
        <ul class="container_tp">
          <li><a class="cta cta_big" href="<?php echo site_url('/tourpass/tourpass-2015/'); ?>">2015</a></li>
          <li><a class="cta cta_big" href="<?php echo site_url('/tourpass/tourpass-2016/'); ?>">2016</a></li>
        </ul>
      </div>


    </section>
  </main>

<?php get_footer('oneoffs'); ?>