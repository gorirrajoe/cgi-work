<?php
	/* Template Name: Register Page */

	get_header();
	$page_ID = get_the_ID();

	$european_event = get_post_meta( $page_ID, '_is_european', true );
	if( $european_event == 1 ) {
		// set timezone to gmt
		date_default_timezone_set('GMT' );
		echo '<!-- gmt timezone set -->';
	} else {
		// set timezone to pacific
		date_default_timezone_set('America/Los_Angeles' );
		echo '<!-- pacific timezone set -->';
	}


	$market = get_market2();
	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';

	$current_date	= strtotime( date( 'l, F j g:i:s A' ) );
	$prefix			= '_rnr3_stj_';
?>

	<!-- main content -->
	<main role="main" id="main">
		<section class="wrapper grid_2 offset780left">

			<div class="column">
				<div class="content">
					<?php if (have_posts()) : while (have_posts()) : the_post();

						echo '<h2>'. get_the_title(). '</h2>';

						$stj_enable			= get_post_meta( $page_ID, $prefix . 'enable', 1 );
						$stj_mobile_link	= ( get_post_meta( $page_ID, $prefix . 'mobile_link_txt', 1 ) != '' ) ? get_post_meta( $page_ID, $prefix . 'mobile_link_txt', 1 ) : '';

						if( $stj_enable && $stj_mobile_link != '' ) {
							echo '<p class="showmobile"><a class="scrolly" href="#stjude">'. $stj_mobile_link .'</a></p>';
						}

						the_content();

					endwhile; endif;

					echo '<div class="accordion">';

						$race = array(
							"days" => array(
								0	=> "first",
								1	=> "second",
								2	=> "third",
								3	=> "fourth"
							),
							"types" => array( "kr", "fm", "hm", "relay", "11k", "10k", "5k", "5m", "1m", "1k", "mm", "hh", "cb-20k", "cb-jc", "funrun", "fourpk", "dubhalf", "virtual", "cnhappy" )
						);
						$reg_btn_count = 1;

						foreach( $race[ "days" ] as $day ) {

							$race_event_date = 'race_event_date_' . $day;
							$$race_event_date = strtotime (get_post_meta( $page_ID, '_race_event_date_' . $day, true ) );

							if( $qt_lang['lang'] == 'es' ) {

								$printed_date = $dias[date('w', $$race_event_date)]." ".date('d', $$race_event_date)." de ".$meses[date('n', $$race_event_date)-1]. " del ".date('Y', $$race_event_date);

							} elseif( $qt_lang['lang'] == 'fr' ) {

								if((date('d', $$race_event_date) == '1' )) {
									$french_date = '1<sup>er</sup>';
								} else {
									$french_date = date('j', $$race_event_date);
								}

								$printed_date = $journees[date('w', $$race_event_date)].", le ".$french_date." ".$mois[date('n', $$race_event_date)-1]. " ".date('Y', $$race_event_date);

							} else {

								$printed_date = date('l, F j, Y', $$race_event_date);

							}


							if( $$race_event_date != '' ) {

								echo '<h3>'. $printed_date .'</h3>';

								foreach( $race[ "types" ] as $type ) {

									$display		= $day.'_'.$type.'_display';
									$$display		= get_post_meta( $page_ID, '_'.$display, true );

									$asterisk_type	= $day.'_'.$type.'_asterisk_type';
									$$asterisk_type	= get_post_meta( $page_ID, '_'.$asterisk_type, true );

									$show_asterisk	= '';

									if( $$asterisk_type == 'single' ) {
										$show_asterisk = '*';
									} elseif( $$asterisk_type == 'double' ) {
										$show_asterisk = '**';
									}

									$price_array = array();

									for( $i = 0; $i < 9; $i++ ) {

										$price_data = array();

										if( $$display == 'on' ) {

											$price_start_date	= 'price_start_'. $day .'_'. $type .'-'. $i;
											$$price_start_date	= strtotime( get_post_meta( $page_ID, '_price_start_'. $day .'_'. $type .'-'. $i, true ) );
											$price_end_date		= 'price_end_'. $day .'_'. $type .'-'. $i;
											$$price_end_date	= get_post_meta( $page_ID, '_price_end_'. $day .'_'. $type .'-'. $i, true );

											if( $$price_end_date != '' ) {
												$$price_end_date .= ' 11:59:59 PM';
											}

											$$price_end_date	= strtotime( $$price_end_date );
											$price				= 'price_'. $day .'_'. $type .'-'. $i;
											$$price				= apply_filters('the_content', get_post_meta( $page_ID, '_price_'. $day .'_'. $type .'-'. $i, true ) );
											$p_tags				= array('<p>', '</p>' );
											$$price				= str_replace( $p_tags, '', $$price);

											$price_verbiage		= 'price_verbiage_'. $day .'_'. $type .'-'. $i;
											$$price_verbiage	= get_post_meta( $page_ID, '_price_verbiage_'. $day .'_'. $type .'-'. $i, true );
											$custom_verbiage	= 'custom_'. $day .'_'. $type .'-'. $i;
											$$custom_verbiage	= apply_filters( 'the_content', get_post_meta( $page_ID, '_custom_'. $day .'_'. $type .'-'. $i, true ) );
											$$custom_verbiage	= strip_tags( $$custom_verbiage );

											if( $$price_verbiage != 'off' ) {

												$price_data = array(
													'type'		=> $type,
													'start'		=> $$price_start_date,
													'end'		=> $$price_end_date,
													'price'		=> $$price,
													'verbiage'	=> $$price_verbiage,
													'custom'	=> $$custom_verbiage
												);
												$price_array[] = $price_data;

											}
										}
									}

									// this can probably be cleaner (add to array)?
									if( $qt_lang['lang'] == 'es' ) { // spanish translations

										if( $type == '1k' ) {
											$type_name = '1 kil&oacute;metro';
										} elseif( $type == '1m' ) {
											$type_name = '1 Mile';
										} elseif( $type == '5k' ) {
											$type_name = '5 kil&oacute;metros';
										} elseif( $type == '10k' ) {
											$type_name = '10 kil&oacute;metros';
										} elseif( $type == '11k' ) {
											$type_name = '11 kil&oacute;metros';
										} elseif( $type == 'mm' ) {
											$type_name = 'Mini Marath&oacute;n';
										} elseif( $type == 'hh' ) {
											$type_name = 'Zappos.com Mitad de la Mitad';
										} elseif( $type == 'relay' ) {
											$type_name = 'Relevo medio marat&oacute;n para 2 personas';
										} elseif( $type == 'hm' ) {
											$type_name = 'Medio Marat&oacute;n';
										} elseif( $type == 'fm' ) {
											$type_name = 'Marat&oacute;n';
										} elseif( $type == 'cb-20k' ) {
											$type_name = 'All Day 20K';
										} elseif( $type == 'cb-jc' ) {
											$type_name = 'Junior Carlsbad 5000';
										} elseif( $type == 'kr' ) {
											$type_name = 'KiDS ROCK';
										} elseif( $type == 'funrun' ) {
											$type_name = 'Family Fun Run';
										} elseif( $type == 'fourpk' ) {
											$type_name = 'Family Fun Run 4-Pack';
										} elseif( $type == 'dubhalf' ) {
											$type_name = 'Athletics Ireland Half Marathon';
										} elseif( $type == 'cnhappy' ) {
											$type_name = 'Happy Run 6K';
										} elseif( $type == '5m' ) {
											$type_name = '5 Miler';
										}

									} elseif( $qt_lang['lang'] == 'fr' ) {

										if( $type == '1k' ) {
											$type_name = 'P&apos;tit Marathon - 1 km';
										} elseif( $type == '1m' ) {
											$type_name = '1 mile';
										} elseif( $type == '5k' ) {
											$type_name = '5 km';
										} elseif( $type == '10k' ) {
											$type_name = '10 km';
										} elseif( $type == '11k' ) {
											$type_name = '11 km';
										} elseif( $type == 'mm' ) {
											$type_name = 'Mini marathon';
										} elseif( $type == 'hh' ) {
											$type_name = 'La demie du Demi';
										} elseif( $type == 'relay' ) {
											$type_name = 'Demi-marathon &agrave; relais- 2 personnes';
										} elseif( $type == 'hm' ) {
											$type_name = 'Demi-marathon';
										} elseif( $type == 'fm' ) {
											$type_name = 'Marathon';
										} elseif( $type == 'cb-20k' ) {
											$type_name = 'All Day 20K';
										} elseif( $type == 'cb-jc' ) {
											$type_name = 'Junior Carlsbad 5000';
										} elseif( $type == 'kr' ) {
											$type_name = 'KiDS ROCK';
										} elseif( $type == 'funrun' ) {
											$type_name = 'Family Fun Run';
										} elseif( $type == 'fourpk' ) {
											$type_name = 'Family Fun Run 4-Pack';
										} elseif( $type == 'dubhalf' ) {
											$type_name = 'Athletics Ireland Half Marathon';
										} elseif( $type == 'cnhappy' ) {
											$type_name = 'Happy Run 6 km';
										} elseif( $type == '5m' ) {
											$type_name = '5 miler';
										}

									} else {

										if( $type == '1k' ) {
											$type_name = '1K';
										} elseif( $type == '1m' ) {
											$type_name = '1 Mile';
										} elseif( $type == '5k' ) {
											$type_name = '5K';
										} elseif( $type == '10k' ) {
											$type_name = '10K';
										} elseif( $type == '11k' ) {
											$type_name = '11K';
										} elseif( $type == 'mm' ) {
											$type_name = 'Mini Marathon';
										} elseif( $type == 'hh' ) {
											$type_name = 'Zappos.com Half of the Half';
										} elseif( $type == 'relay' ) {
											$type_name = '2-Person Half Marathon Relay';
										} elseif( $type == 'hm' ) {
											$type_name = 'Half Marathon';
										} elseif( $type == 'fm' ) {
											$type_name = 'Marathon';
										} elseif( $type == 'cb-20k' ) {
											$type_name = 'All Day 20K';
										} elseif( $type == 'cb-jc' ) {
											$type_name = 'Junior Carlsbad 5000';
										} elseif( $type == 'kr' ) {
											$type_name = 'KiDS ROCK';
										} elseif( $type == 'funrun' ) {
											$type_name = 'Family Fun Run';
										} elseif( $type == 'fourpk' ) {
											$type_name = 'Family Fun Run 4-Pack';
										} elseif( $type == 'dubhalf' ) {
											$type_name = 'Athletics Ireland Half Marathon';
										} elseif( $type == 'virtual' ) {
											$type_name = 'Virtual Run';
										} elseif( $type == 'cnhappy' ) {
											$type_name = 'Happy Run 6K';
										} elseif( $type == '5m' ) {
											$type_name = '5 Miler';
										}

									}


									if( !empty( $price_array ) ) {

										$price_count = count( $price_array);

										for( $i = 0; $i < $price_count; $i++ ) {

											if( $current_date < $price_array[ $i ][ 'end' ] ) {
												$price_display		= $price_array[ $i ][ 'price' ];
												$verbiage_display	= $price_array[ $i ][ 'verbiage' ];
												break;
											} else {
												$price_display		= 'TBD';
												$verbiage_display	= '';
											}

										}

										$sold_out	= $day.'_'.$type.'_sold_out';
										$$sold_out	= get_post_meta($page_ID, '_'.$sold_out, true);

										if( $$sold_out == 'on' ) {
											$reg_or_sold_out_btn = '<span class="highlight black">'.$txt_sold_out.'</span>';

										} elseif( $verbiage_display == 'At Expo If Available' ) {
											$reg_or_sold_out_btn = '<span class="highlight black">'.$online_reg_closed_text.'</span>';

										} elseif( $verbiage_display == 'TBD' ) {
											$reg_or_sold_out_btn = '<span class="highlight black">'.$txt_TBD.'</span>';

										} elseif( $type == 'relay' ) {
											$reg_url				= $day.'_'.$type.'_register_url';
											$$reg_url				= apply_filters('the_content', get_post_meta($page_ID, '_'.$day.'_'.$type.'_register_url', true));
											$$reg_url				= strip_tags( $$reg_url);

											$reg_or_sold_out_btn	= '<a class="cta" href="'.$$reg_url.'" target="_blank">'.$txt_register.'</a>';

										} else {
											$reg_url		= $day.'_'.$type.'_register_url';
											$$reg_url		= apply_filters('the_content', get_post_meta($page_ID, '_'.$day.'_'.$type.'_register_url', true));
											$$reg_url		= strip_tags( $$reg_url);

											$reg_tp3pk_url	= $day.'_'.$type.'_tp3pk_register_url';
											$$reg_tp3pk_url	= get_post_meta( $page_ID, '_'.$day.'_'.$type.'_tp3pk_register_url', true );
											$reg_tpg_url	= $day.'_'.$type.'_tpg_register_url';
											$$reg_tpg_url	= get_post_meta( $page_ID, '_'.$day.'_'.$type.'_tpg_register_url', true );

											// for( $count=1; $count<4; $count++) {

											// 	${'reg_bundle_with_'.$count}	= $day.'_'.$type.'_'.$count.'_bundle_with';
											// 	$${'reg_bundle_with_'.$count}	= get_post_meta( $page_ID, '_'.$day.'_'.$type.'_'.$count.'_bundle_with', true );
											// 	${'reg_bundle_url_'.$count}		= $day.'_'.$type.'_'.$count.'_bundle_register_url';
											// 	$${'reg_bundle_url_'.$count}	= get_post_meta( $page_ID, '_'.$day.'_'.$type.'_'.$count.'_bundle_register_url', true );

											// }

											// if(( $$reg_url != '' ) && ( $$reg_tp3pk_url == '' ) && ( $$reg_tpg_url == '' ) && ( $$reg_bundle_url_1 == '' ) && ( $$reg_bundle_url_2 == '' ) && ( $$reg_bundle_url_3 == '' )) {

												$reg_or_sold_out_btn = '
													<a class="cta" href="'.$$reg_url.'" target="_blank">'.$txt_register.'</a>
												';

											// } else {

											// 	$reg_or_sold_out_btn = '
											// 		<div class="reg_btn">
											// 			<a id="link'.$reg_btn_count.'" class="show" href="javascript:void(0)">'.$txt_register.'</a>
											// 			<div class="reg_popup" id="reg_popup_'.$reg_btn_count.'">
											// 				<span class="reg_popup_indicator"></span>
											// 				<div class="reg_popup_content">
											// 					<h5>'.$choose_1_text.'</h5>
											// 					<ul>';
											// 						if( $$reg_url != '' ) {
											// 							$reg_or_sold_out_btn .= '<li><a href="'.$$reg_url.'" target="_blank">'.$individual_text.'</a></li>';
											// 						}
											// 						if( $$reg_tp3pk_url != '' ) {
											// 							$reg_or_sold_out_btn .= '<li><a href="'.$$reg_tp3pk_url.'" target="_blank">'.$tp3p_text.'</a></li>';
											// 						}
											// 						if( $$reg_tpg_url != '' ) {
											// 							$reg_or_sold_out_btn .= '<li><a href="'.$$reg_tpg_url.'" target="_blank">'.$tpg_text.'</a></li>';
											// 						}
											// 						for( $count=1; $count<4; $count++) {
											// 							if( $${'reg_bundle_with_'.$count} != '-1' ) {
											// 								$reg_or_sold_out_btn .= '<li><a href="'.$${'reg_bundle_url_'.$count}.'" target="_blank">'.$festival_wkd_text.$${'reg_bundle_with_'.$count}.'</a></li>';
											// 							}
											// 						}
											// 					$reg_or_sold_out_btn .= '</ul>
											// 				</div>
											// 			</div>
											// 		</div>
											// 	';

											// }
										}

										$distance_chars = array( ' ', '/', '&apos;', '.' );

										echo '<div class="accordion-section distance-'.strtolower( str_replace( $distance_chars, '-', $type_name) ).'" style="position: relative;">

											<div class="accordion-section-title" data-reveal="#X'.str_replace( $distance_chars, '-', $type_name).'">'.$type_name.$show_asterisk.' &mdash; '.$price_display.'</div>'.
											$reg_or_sold_out_btn;


											/**
											 * only show current plus the next future price tier
											 * when current is shown, increment to 1
											 * when the next date is shown, increment again
											 * when $current_plus1 is over 1, break out of foreach
											 */
											$current_plus1 = 0;

											$tier_type = get_post_meta( $page_ID, '_tiertype', 1 );

											if( $tier_type == NULL ) {  // this is default
												$tier_type = 'current_one';
											}

											echo '<ul id="X'. str_replace( $distance_chars, '-', $type_name) .'" class="accordion-section-content">';

												foreach( $price_array as $single_price) {

													if( $current_plus1 > 1 && $tier_type == 'current_one' ) {
														break;
													} else {

														if( $single_price['verbiage'] == "TBD") {
															echo '<li class="tier">
																<strong>'. $single_price['price'] .'</strong>
															</li>';
														} else {

															if( $current_date > $single_price['end'] || $$sold_out == 'on' ) {
																$strike_style	= ' price_inactive';
																$donotshow		= 1;
															} else {
																$strike_style	= '';
																$donotshow		= 0;
															}

															if( $qt_lang['lang'] == 'es' ){
																$display_start_date	= date('j.n.y', $single_price['start']);
																$display_end_date	= date('j.n.y', $single_price['end']);
															} elseif( $qt_lang['lang'] == 'fr' ) {
																$display_start_date	= date('j/n/y', $single_price['start']);
																$display_end_date	= date('j/n/y', $single_price['end']);
															} else {
																$display_start_date	= strtoupper( date('M j', $single_price['start']) );
																$display_end_date	= strtoupper( date('M j', $single_price['end']) );
															}

															if( $single_price['verbiage'] == "Standard Range") {

																if( $donotshow == 0 ) {
																	echo '<li class="tier'.$strike_style.'">
																		 <h4>'.$display_start_date.' ' .$through_text . ' ' .$display_end_date.' </h4>
																		 <div class="price">'.$single_price['price'].'</div>
																	</li>';
																	$current_plus1++;
																}

															} elseif( $single_price['verbiage'] == 'Through' ) {

																if( $donotshow == 0 ) {
																	echo '<li class="tier'.$strike_style.'">
																		<h4>'.$through_text.' '.$display_end_date.' </h4>
																		<div class="price">'.$single_price['price'].'</div>
																	</li>';
																	$current_plus1++;
																}

															} elseif( $single_price['verbiage'] == 'At Expo If Available' ) {

																if( $donotshow == 0 ) {
																	echo '<li class="tier'.$strike_style.'">
																		<h4>'.$at_expo_text.'</h4>
																		<div class="price">'.$single_price['price'].'</div>
																	</li>';
																	$current_plus1++;
																}

															} elseif( $single_price['verbiage'] == 'Custom' ) {

																if( $donotshow == 0 ) {
																	echo '<li class="tier'.$strike_style.'">
																		<h4>'.$single_price['custom'].'</h4>
																		<div class="price">'.$single_price['price'].'</div>
																	</li>';
																	$current_plus1++;
																}

															}
														}
													}
												}

											echo '</ul>
										</div>';

									}
									$reg_btn_count++;
								}
							}
						}

						if( get_post_meta( $page_ID, '_disable_disclaimer', 1 ) != 1 ) {
							echo '<p>'. $price_disclaimer .'</p>';
						}

					echo '</div>';


					// Additional metabox 1
					$mb1_switch		= get_post_meta( $page_ID, '_rnr3_additional_box1_display', true );
					$mb1_content	= get_post_meta( $page_ID, '_rnr3_additional_box1_content', true );
					$mb1_content	= !empty( $mb1_content ) ? apply_filters( 'the_content', $mb1_content ) : '';
					$mb1_style		= empty( $mb1_switch ) ? ' style="display:none;"' : '';

					echo '<section id="additional-box-1" class="additional-box"'. $mb1_style .'>'.
						$mb1_content .'
					</section>';


					// st jude stuffs
					if( $stj_enable ) {

						if( $qt_lang['enabled'] == 1 ) {
							$stjtitle_array = qtrans_split( get_post_meta( $page_ID, $prefix . 'title', 1 ) );
							$stjtitle       = $stjtitle_array[ $qt_lang['lang'] ];
						} else {
							$stjtitle       = get_post_meta( $page_ID, $prefix . 'title', 1 );
						}

						echo '<section id="stjude">
							<h2>'. $stjtitle .'</h2>

							<div class="stj_content">
								<figure class="stj_alignright"><img src="'. get_bloginfo( 'stylesheet_directory' ) .'/img/stj-logo-small.svg" alt="St. Jude Heroes"></figure>
								'. apply_filters( 'the_content', get_post_meta( $page_ID, $prefix . 'content', 1 ) ) .'
							</div>

							<div class="stj_accordion">';
								$commitments_array	= get_post_meta( $page_ID, $prefix . 'commitments', 1 );
								$raceiturl			= get_post_meta( $page_ID, $prefix . 'register_url', 1 );

								foreach( $commitments_array as $commitment ) {

									$commitment_pieces	= explode( '::', $commitment );
									$commit_key			= $commitment_pieces[0];
									$commit_display		= $commitment_pieces[1] . ' Hero';
									$commit_price		= str_replace( '.00', '', get_post_meta( $page_ID, $prefix . $commit_key . 'cost', 1 ) );
									$commit_content		= apply_filters( 'the_content', get_post_meta( $page_ID, $prefix . $commit_key . 'content', 1 ) );

									if( ( $market == 'montreal' ) && $qt_lang['lang'] == 'fr' ) {

										// translate on the fly (won't disturb metaboxes on back end)
										if( strpos( $commit_display, 'Bronze' ) !== FALSE ) {
											$commit_display = 'H&eacute;ros Bronze';
										} elseif( strpos( $commit_display, 'Silver' ) !== FALSE ) {
											$commit_display = 'H&eacute;ros Argent';
										} elseif( strpos( $commit_display, 'Gold' ) !== FALSE ) {
											$commit_display = 'H&eacute;ros Or';
										} elseif( strpos( $commit_display, 'Platinum' ) !== FALSE ) {
											$commit_display = 'H&eacute;ros Platine';
										} elseif( strpos( $commit_display, 'Classic' ) !== FALSE ) {
											$commit_display = 'H&eacute;ros Classique ';
										}

									}


									if( $commit_price == '0.00' ) {
										$commit_price_display = $commit_price_open;
									} else {

										if( ( $market == 'montreal' ) && $qt_lang['lang'] == 'fr' ) {
											$commit_price			= str_replace( ',', '', $commit_price );
											$commit_price_display	= 'Financement de '. $commit_price .' $';
										} else {
											$commit_price_display = '$' . $commit_price . ' Fundraiser';
										}

									}

									echo '<div class="accordion-section" style="position: relative;">

										<div class="stj_accordion-section-title">'. $commit_display .'</div>';

										if( $commit_key != 'classic' ) {
											echo '<a class="cta" target="_blank" href="'. $raceiturl .'">'. $register_free_btn_text .'</a>';
										} else {
											echo '<a class="cta" target="_blank" href="'. $raceiturl .'">'. $txt_register .'</a>';
										}

										echo '<div class="stj_accordion-section-content" id="'. $commit_key .'">
											<div class="stj_column_wrapper">
												'. $commit_content .'
											</div>
										</div>

									</div>';
								}

							echo '<div class="stj_below_accordion">
								'. apply_filters( 'the_content', get_post_meta( $page_ID, $prefix . 'below_tab_content', 1 ) ) .'
							</div>

						</section>';
					}


					// Additional metabox 1
					$mb2_switch		= get_post_meta( $page_ID, '_rnr3_additional_box2_display', true );
					$mb2_content	= get_post_meta( $page_ID, '_rnr3_additional_box2_content', true );
					$mb2_content	= !empty( $mb2_content ) ? apply_filters( 'the_content', $mb2_content ) : '';
					$mb2_style		= empty( $mb2_switch ) ? ' style="display:none;"' : '';

					echo '<section id="additional-box-2" class="additional-box"'. $mb2_style .'>' .
						$mb2_content .'
					</section>';


					$reg_notes = apply_filters( 'the_content', get_post_meta( $page_ID, '_reg_notes', TRUE ) );

					if( $reg_notes != '' ) {

						echo '<div class="registration_notes">
							'. $reg_notes .'
						</div>';

					} ?>
				</div>
			</div>

			<?php get_sidebar( 'register' ); ?>

		</section>

	</main>

<?php get_footer(); ?>
