<?php
	/* Template Name: Travel - Pack */

	get_header();

	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';

	$prefix = '_rnr3_';

	$travel_subpage_array = array(
		'fly'		=> 'flights',
		'stay'		=> 'hotels',
		'pack'		=> 'pack',
		'transport'	=> 'transport'
	);

	$travel_page_id	= get_id_by_slug( 'the-weekend/travel' );

	/**
	 * since subpage sponsor (eagle creek) is the same for all events,
	 * retrieve from blog 1
	 */
	switch_to_blog( 1 );

		$blog1_pack_page_id = get_id_by_slug( 'travel/pack' );

		$subpage_sponsor = ( get_post_meta( $blog1_pack_page_id, '_rnr3_sponsor_img', 1 ) != '' ) ? get_post_meta( $blog1_pack_page_id, '_rnr3_sponsor_img', 1 ) : '';

	restore_current_blog();
?>

<!-- main content -->
<main role="main" id="main" class="travel">
	<section class="wrapper">
		<div class="narrow">

			<?php
				if (have_posts()) : while (have_posts()) : the_post();

					$subpage_icon = ( get_post_meta( $travel_page_id, '_rnr3_pack_img', 1 ) != '' ) ? get_post_meta( $travel_page_id, '_rnr3_pack_img', 1 ) : '';

					echo '<section class="travel_content">

						<h2 class="travel_subpage_title">

							<img class="travel_subpage_icon" src="'. $subpage_icon .'" alt="'. get_the_title() .'">' .

							get_the_title();

							if( $subpage_sponsor != '' ) {
								/**
								 * eagle creek (pack sponsor) wants to have a slightly larger logo since theirs is a teeny box
								 */
								echo '<span>Presented by <img src="'. $subpage_sponsor .'" class="pack_sponsor"></span>';
							}

						echo '</h2>';

						the_content();


						$bulleted_items = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'travel_bullets', 1 ) );

						if( $bulleted_items != '' ) {

							$bulleted_items_title = ( get_post_meta( get_the_ID(), $prefix . 'travel_bullets_title', 1 ) != '' ) ? get_post_meta( get_the_ID(), $prefix . 'travel_bullets_title', 1 ) : '';

							if( $bulleted_items_title != '' ) {
								echo '<h3 class="travel_bullet_box_hdr">'. $bulleted_items_title .'</h3>';
							}

							echo '<div class="travel_bullet_box">'
								. $bulleted_items;

								/**
								 * retrieve packing list pdf from blog 1's travel/pack page
								 */
								switch_to_blog( 1 );

									$pack_page_ID = get_id_by_slug( 'travel/pack' );

									$packing_pdf = ( get_post_meta( $pack_page_ID, $prefix . 'checklist_pdf', 1 ) != '' ) ? get_post_meta( $pack_page_ID, $prefix . 'checklist_pdf', 1 ) : '';

									if( $packing_pdf != '' ) {

										echo '<a href="'. $packing_pdf .'" target="_blank" title="Download Travel Tips & Packing Lists"><span class="icon-file-pdf"></span></a>';

									}

								restore_current_blog();

							echo '</div>';

						}

					echo '</section>';


					/**
					 * retrieve packing videos from blog 1's travel/pack page
					 */
					switch_to_blog( 1 );

						$videos = ( get_post_meta( $blog1_pack_page_id, '_rnr3_video-group', 1 ) != '' ) ? get_post_meta( $blog1_pack_page_id, '_rnr3_video-group', 1 ) : '';

						if( $videos != '' ) {

							echo '<section class="grid_3_special travel_videos">
								<div class="wrapper">';

									foreach( $videos as $key => $value ) {

										$img = wp_get_attachment_image( $value['thumb_id'], 'infobox-thumbs' );

										echo '<div class="column_special">';

											if( $value['type'] == 'youtube' ) {

												echo '<figure>
													<a href="javascript:;" data-fancybox data-src="http://www.youtube.com/embed/'. $value['url'] .'?autoplay=1" class="video-modal">
														'. $img .'
														<span class="icon-play"></span>
													</a>
												</figure>';

											} else {

												echo '<figure>
													<a href="'. $value['url'] .'" target="_blank" class="external-link">
														'. $img .'
														<span class="icon-link"></span>
													</a>
												</figure>';

											}

											echo '<p><strong>'. $value['title'] .'</strong></p>

										</div>';

									}

								echo '</wrapper>
							</section>';

						}

					restore_current_blog();

				endwhile; endif;


				travel_subpage_columns( $travel_page_id, $qt_lang );

				/**
				 * travel sponsors
				 */
				travel_sponsors( $travel_page_id );
			?>

		</div>

	</section>

	<script>
		$(document).ready(function(){
			$(".video-trigger").fancybox({
				openEffect: 'none',
				closeEffect: 'none',
				helpers: {
					overlay: {
						locked: false,
						css: {
							'background' : 'rgba(112, 112, 113, .76)'
						}
					}
				}
			});
		});
	</script>
</main>

<?php get_footer(); ?>
