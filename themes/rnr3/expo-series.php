<?php
	/* Template Name: Expo (Series) */
	get_header( 'series-home' );

	$args = array(
		'post_type'			=> 'expo_event',
		'meta_key'			=> 'expo_startdate',
		'orderby'			=> 'meta_value_num',
		'order'				=> 'ASC',
		'posts_per_page'	=> -1
	);
	$exposeries_query = new WP_Query( $args );

	if ( false === ( $exposeries_query = get_transient( 'exposeries_query_data' ) ) ) {
		$exposeries_query = new WP_Query( $args );
		set_transient( 'exposeries_query_data', $exposeries_query, 5 * MINUTE_IN_SECONDS );
	}

?>

<!-- main content -->
<main role="main" id="main">
	<section class="wrapper">
		<div class="content">
			<?php
				if (have_posts()) : while (have_posts()) : the_post();
					echo '<h2>'. get_the_title(). '</h2>';
					the_content();
				endwhile; endif;

				$index = 1;

				if( $exposeries_query->have_posts() ) {

					echo '<div class="accordion">';
						while( $exposeries_query->have_posts() ) {
							$exposeries_query->the_post();

							$sold_out		= get_post_meta( get_the_ID(), 'sold_out', true );
							$location		= get_post_meta( get_the_ID(), 'expo_location', true );
							$participation	= get_post_meta( get_the_ID(), 'expo_participation', true );
							$attendees		= get_post_meta( get_the_ID(), 'expo_attendees', true );
							$male			= get_post_meta( get_the_ID(), 'expo_male', true );
							$female			= get_post_meta( get_the_ID(), 'expo_female', true );
							$hh_income		= get_post_meta( get_the_ID(), 'expo_hh_income', true );
							$application	= get_post_meta( get_the_ID(), 'expo_application', true );
							$floorplan		= get_post_meta( get_the_ID(), 'expo_floorplan', true );
							$setup			= get_post_meta( get_the_ID(), 'expo_setup', true );
							$open			= get_post_meta( get_the_ID(), 'expo_open', true );
							$moveout		= get_post_meta( get_the_ID(), 'expo_moveout', true );
							$notes			= get_post_meta( get_the_ID(), 'expo_notes', true );
							$map			= get_post_meta( get_the_ID(), 'expo_map', true );

							if( $map != '' ) {
								$map_link = '[ <a href="'. $map .'" target="_blank">map</a> ]';
							} else {
								$map_link = '';
							}

							$startdate	= get_post_meta( get_the_ID(), 'expo_startdate', true );
							$enddate	= get_post_meta( get_the_ID(), 'expo_enddate', true );

							if( date('F', $startdate ) != date('F', $enddate ) ) {
								$daterange = date('F j', $startdate) .' &ndash; '. date('F j, Y', $enddate);
							} else {
								$daterange = date('F j', $startdate) .' &ndash; '. date('j, Y', $enddate);
							}

							echo '<div class="accordion-section" style="position: relative;">
								<div class="accordion-section-title" data-reveal="#panel'.$index.'">'.get_the_title().':  '.$daterange.'</div>
								<div class="accordion-section-content" id="panel'.$index.'">
									<p><strong>Location: </strong>'.$location. ' ' .$map_link .'</p>
									<p>
										<strong>Last Year\'s Participation:</strong> '.$participation.'
										&middot;
										<strong>Attendees:</strong> '.$attendees.'
										&middot;
										<strong>Male: </strong> '.$male.' / <strong>Female:</strong> '.$female.'
										&middot;
										<strong>Avg. HH Income:</strong> '.$hh_income.'
									</p>';

									if( $sold_out == 'on' ) {
										$application_txt = 'Expo Sold Out';
									} elseif( $application == '' ) {
										$application_txt = 'Application Coming Soon';
									} else {
										$application_txt = '<a href="'.$application.'" target="_blank">Online Application</a>';
									}

									echo '<p>'. $application_txt .'</p>';


									if( $floorplan == '' ) {
										echo '<p>Exhibitor List and Floor Plan Coming Soon</p>';
									} else {
										echo '<p><a href="'.$floorplan.'" target="_blank">Exhibitor List and Floor Plan </a></p>';
									}

									echo '<div class="grid_3">';
										if( $setup != '' ) {
											echo '<div class="column">
												<strong>Setup</strong>'.
												$setup .
											'</div>';
										}

										if( $open != '' ) {
											echo '<div class="column">
												<strong>Open to Public</strong>'.
												$open .
											'</div>';
										}

										if( $moveout != '' ) {
											echo '<div class="column">
												<strong>Move Out</strong>'.
												$moveout .
											'</div>';
										}
									echo '</div>';

									if( $notes != '' ) {
										echo $notes;
									}
								echo '</div>
							</div>';
							$index++;

						}
					echo '</div>';

				}
			?>
		</div>
	</section>

</main>

<?php get_footer( 'series' ); ?>
