<?php
  get_header('oneoffs');

  if( isset( $_GET['event'] ) ) {
    $rnb_ambo_event = $_GET['event'];
  } else {
    $rnb_ambo_event = '';
  }
?>
<style>
  .rnbround .attachment-rnb-ambassador-thumblg {
    border-radius:50%;
    margin-bottom:30px;
  }
</style>

<main role="main" id="main">
  <section class="wrapper">
      <div style="margin-bottom:30px;"><a class="cta" href="<?php echo site_url('/rocknblog/'); ?>">Back</a></div>

      <h1>The Team</h1>
      <div class="grid_4">

        <?php
          $count = 0;
          $query = 'SELECT * from wp_posts where post_type = "rock-n-blog" AND post_status = "publish" ORDER BY post_date DESC';
          $entries = $wpdb->get_results($query);

          foreach($entries as $entry) {
            /*
            if( ( $count % 4 ) == 0 && $count != 0 ) {
              echo '</div><div class="grid_4">';
            }
            */
            $ambo_market = get_post_meta( $entry->ID, '_ambo_event_readable', true );

            echo '<div class="column center">
              <div class="rnbround">
                '.get_the_post_thumbnail( $entry->ID, 'travel-hotel' ).'
              </div>
              <p><a href="'. post_permalink( $entry->ID ) .'">'.$entry->post_title.'</a></p>
            </div>';
            $count++;
          }
          echo '</div>';

        ?>
    </div>
  </section>
</main>

<?php get_footer( 'oneoffs' ); ?>
