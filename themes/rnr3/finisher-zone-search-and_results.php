<?php
	/**
	 * Template Name: Finisher Zone - Search & Results
	 * Notes: perhaps make `fz_results_grid` & `finisher-zone-leaderboard` & `fz_event_hdr` a function
	 */

	get_header('oneoffs');
	$prefix = '_rnr3_';
	require_once 'functions_fz.php';

	$redirecturl = $_SERVER['REDIRECT_URL'];

	$eventid = $yearid = $subevent_id = $bib = $resultspage = $resultpage = '';
	$isarchive = 0;

	if ( isset( $_GET['eventid'] ) && is_numeric( $_GET['eventid'] ) ) {
		$eventid		= $_GET['eventid'];
		$evInfo			= getFZEventInfo( $eventid );
	} else {
		$eventid		= get_query_var( 'eventid', '' );
		$evInfo			= getFZEventInfo( $eventid );
	}
	if ( isset( $_GET['yearid'] ) && is_numeric( $_GET['yearid'] ) ) {
		$yearid			= $_GET['yearid'];
	}
	if ( isset( $_GET['archive'] ) && is_numeric( $_GET['archive'] ) ) {
		$isarchive		= $_GET['archive'];
	}
	if ( isset( $_GET['subevent_id'] ) && is_numeric( $_GET['subevent_id'] ) ) {
		$subevent_id	= $_GET['subevent_id'];
	}
	if ( isset( $_GET['bib'] ) && is_numeric( $_GET['bib'] ) ) {
		$bib			= $_GET['bib'];
	}
	if ( isset( $_GET['resultpage'] ) && is_numeric( $_GET['resultpage'] ) ) {
		$resultpage		= $_GET['resultpage'];
	}
	if ( isset( $_GET['resultspage'] ) && is_numeric( $_GET['resultspage'] ) ) {
		$resultspage	= $_GET['resultspage'];

		if ( isset( $_GET['perpage'] ) && is_numeric( $_GET['perpage'] ) ) {
			 $perpage	= $_GET['perpage'];
		}

		$firstname	= !empty( $_GET['firstname'] ) ? $_GET['firstname'] : '';
		$lastname	= !empty( $_GET['lastname'] ) ? $_GET['lastname'] : '';
		$bib		= !empty( $_GET['bib'] ) ? $_GET['bib'] : '';
		$gender		= !empty( $_GET['gender'] ) ? $_GET['gender'] : '';
		$division	= !empty( $_GET['division'] ) ? $_GET['division'] : '';
		$city		= !empty( $_GET['city'] ) ? $_GET['city'] : '';
		$state		= !empty( $_GET['state'] ) ? $_GET['state'] : '';
		//$club      = $_GET['club'];
		if ( isset( $_GET['club'] ) ) {
			$club	= $_GET['club'];
		} else {
			//$club = NULL;
		}
		if ( isset( $_GET['military'] ) ) {
			$military	= $_GET['military'];
		} else {
			//$military = NULL;
		}

		$criteria = array(
			'resultspage' => $resultspage,
			'eventid'     => $eventid,
			'subevent_id' => $subevent_id,
			'yearid'      => $yearid,
			'firstname'   => trim( $firstname ),
			'lastname'    => trim( $lastname ),
			'bib'         => trim( $bib ),
			'gender'      => trim( $gender ),
			'division'    => trim( $division ),
			'state'       => trim( $state ),
			'city'        => trim( $city ),
			/*'club'        => trim( $club ),*/
			'perpage'     => $perpage
		);

		if ( isset( $_GET['club'] ) ) {
			$criteria['club'] = trim( $_GET['club'] );
		}
		if ( isset( $_GET['military'] ) ) {
			$criteria['military'] = trim( $_GET['military'] );
		}

	}

	if ( $eventid != '' ) {
		switch_to_blog($evInfo->blog_id);
		$market = get_market2();
		restore_current_blog();
	} else {
		$market = get_market2();
	}
	if ( false === ( $event_info = get_transient( 'event_info_'. $market ) ) ) {
		$event_info = rnr3_get_event_info( $market, true );
	}

	//find out where this event takes place
	$mexico_race	= $american_race = false;
	if ( in_array( $event_info->event_slug, get_mexico_races() ) ) {
		$mexico_race	= true;
	} else {
		$american_race	= true;
	}

	$qt_lang      = rnr3_get_language();
	$qt_status    = $qt_lang['enabled'] == 1 ? true : false;
	$qt_cur_lang  = $qt_lang['lang'];

	include 'languages.php';

	$front_page_ID		= get_option( 'page_on_front' );
	$presenting_sponsor	= '';

	if ( ( $presenting_sponsor_img = get_post_meta( $front_page_ID, $prefix . 'sponsor_img', true ) ) != '' ) {
		$presenting_sponsor = '<img src="'. $presenting_sponsor_img .'">';

		$presenting_sponsor_url = get_post_meta( $front_page_ID, $prefix . 'sponsor_url', true );

		if ( $presenting_sponsor_url != ''  ) {
			$presenting_sponsor	= '<a href="'. $presenting_sponsor_url .'" target="_blank">'. $presenting_sponsor .'</a>';
		}

		$presenting_sponsor = '<div class="presented_by"><span>'. $presented_by_txt .'</span>'. $presenting_sponsor .'</div>';
	}
?>

	<main role="main" id="main" class="no-hero finisher_zone_search_results">

		<nav id="subnav">
			<section class="wrapper">

				<a href="<?php echo esc_url( home_url() ); ?>" class="fz-home">Finisher Zone</a>

				<div class="subnav-menu-label">Menu</div>

				<div class="subnav-menu-primary-event-container">
					<ul class="subnav-menu">
						<li><a href="<?php echo esc_url( home_url() ); ?>/#past_results"><?php echo trim($results_txt); ?></a></li>
						<li><a href="<?php echo esc_url( home_url() ); ?>/#badges"><?php echo $finisher_badges_txt; ?></a></li>
						<li><a href="<?php echo esc_url( home_url() ); ?>/#personas"><?php echo $runner_personas_txt; ?></a></li>
						<li><a href="<?php echo esc_url( home_url() ); ?>/#more_features"><?php echo $features_txt; ?></a></li>
					</ul>
				</div>

				<?php echo $presenting_sponsor; ?>

			</section>
		</nav>

		<?php if ( $eventid != '' && $yearid == '' && $isarchive == 1 && $subevent_id == '' && $bib == '' ) {

			/*
			 * Event chosen, year not chosen
			 * Pull in template
			 * Note: get_template_part() not used because function doesn't pass variables
			 */
			include( locate_template( 'template-parts/fz-select-year.php' ) );

		} elseif ( $eventid == '' && $yearid != '' && $isarchive == 1 ) {

			/*
			 * Only year chosen
			 */
			include( locate_template( 'template-parts/fz-select-event.php' ) );

		} elseif ( $eventid != '' && $isarchive == 0 && $yearid == '' && $subevent_id == '' && $bib == '' && $resultpage == '' && $resultspage == '' ) {

			/*
			 * Event chosen, year chosen... so search away!
			 */
			include( locate_template( 'template-parts/fz-search.php' ) );

		} elseif ( $eventid != '' && $yearid != '' && $subevent_id != '' && $bib == '' && $resultspage == '' ) {

			/*
			 * Event chosen, year chosen, subevent chosen -- show top 10 of subevent
			 */
			include( locate_template( 'template-parts/fz-subevent.php' ) );

		} elseif ( $eventid != '' && $isarchive == 0 && $yearid == '' && $bib != '' ) {

			/*
			 * Event chosen, year chosen, runner selected (individual result)
			 */
			include( locate_template( 'template-parts/fz-single-result.php' ) );

		} elseif ( $resultspage != '' ) {

			/*
			 * Search results page
			 */
			include( locate_template( 'template-parts/fz-search-results.php' ) );

		} else {

			/*
			 * Default case -- landing with no event/year chosen
			 */
			include( locate_template( 'template-parts/fz-search-results-landing.php' ) );

		} ?>
	</main>

<?php
	// script for pinterest share
	echo '<script async defer src="//assets.pinterest.com/js/pinit.js"></script>';
	//script for po.st
	$post_pubkey  = rnr_get_option( 'rnrgv_post_pubkey');

	if ( !empty( $post_pubkey ) ) {
		echo '<script src="http://i.po.st/static/v3/post-widget.js#publisherKey='. $post_pubkey .'&retina=true" type="text/javascript"></script>';
	}

get_footer('series'); ?>
