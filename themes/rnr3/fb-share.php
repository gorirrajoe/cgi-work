<?php

	/* Template Name: FB SHARE */
	global $wpdb;

	get_header('oneoffs');
	require_once 'functions_fz.php';

	$market = get_market2();
	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang		= rnr3_get_language();
	$qt_status		= $qt_lang['enabled'] == 1 ? true : false;
	$qt_cur_lang	= $qt_lang['lang'];

	include 'languages.php';

	$type		= isset( $_REQUEST['type'] ) ? $_REQUEST['type'] : '';
	$image		= isset( $_REQUEST['image'] ) ? $_REQUEST['image'] : '';
	$itemName	= isset( $_REQUEST['itemName'] ) ? $_REQUEST['itemName'] : '';
	$event		= isset( $_REQUEST['event'] ) ? $_REQUEST['event'] : '';

?>

	<main role="main" id="main" class="finisher_zone">
		<section id="fz_fb_share" class="wrapper">

			<?php if( !in_array('', array($type, $image, $itemName, $event) ) ){ ?>
				<div
					class="fb-login-button"
					data-show-faces="true"
					data-width="200"
					data-max-rows="1"
					data-scope="publish_actions">
				</div>

				<div>
					<p><?php echo $fb_share_msg_txt; ?></p>
					<p><?php echo $suggest_msg_txt; ?>: <strong><em><?php echo shareMessage( $qt_lang, $itemName, $type, '', true ) . stripslashes($event);?></em></strong></p>
					<p class="center">
						<?php echo $msg_to_share_txt; ?>:
						<textarea name="fb-share-message" id="fb-share-message" rows="2"></textarea>
					</p>
					<figure>
						<?php echo $img_to_share_txt; ?>:<br>
						<img id="fb-share-image" src="<?php echo $image; ?>">
					</figure>
					<p class="center"><a href="#" class="cta fb-share-bttn"><?php echo $share_txt; ?></a></p>
				</div>

				<div id="result"></div>

			<?php } else { ?>

				<div>
					<p>There seems to be an error, please <a href="<?php echo get_permalink( get_page_by_title( 'Search and Results' )->ID ); ?>">find your results</a> and select a Badge/Persona to share on your timeline!</p>
				</div>

			<?php } ?>

		</section>

	</main>

<?php
	get_footer('oneoffs');
?>
