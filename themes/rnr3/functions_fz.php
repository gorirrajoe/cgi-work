<?php
if(stripos($_SERVER['SERVER_NAME'], 'cgitesting') !== false OR stripos($_SERVER['SERVER_NAME'], 'lan') !== false OR stripos($_SERVER['SERVER_NAME'], 'loc') !== false){
	define( 'RESULTSHOSTNAME', 'localhost' );
	define( 'RESULTSDBUSER', 'rnruserdb' );
	define( 'RESULTSDBPWD', 'cgi123' );
	define( 'RESULTSDBNAME', 'rnr_results' );

	define( 'EVENTHOSTNAME', 'localhost' );
	define( 'EVENTDBUSER', 'rnruserdb' );
	define( 'EVENTDBPWD', 'cgi123' );
	define( 'EVENTDBNAME', 'runrocknroll' );
}else{
	define( 'RESULTSHOSTNAME', '192.168.0.10' );
	define( 'RESULTSDBUSER', 'resultuserdb' );
	define( 'RESULTSDBPWD', 'c0mPet1tor858' );
	define( 'RESULTSDBNAME', 'rnr_results' );

	define( 'EVENTHOSTNAME', '192.168.0.10' );
	define( 'EVENTDBUSER', 'resultuserdb' );
	define( 'EVENTDBPWD', 'c0mPet1tor858' );
	define( 'EVENTDBNAME', 'runrocknroll' );
}

/**
 * function: getRunnerInfo
 */
function getRunnerInfo( $rc_appid, $wp_fz_event_id, $runner_bib ) {
	$dbConnLink = dbConnect( 'results' );
	//dbSelection( $dbConnLink, 'results' );
	$rStats = getRunnerStats( $runner_bib, $dbConnLink, $rc_appid, $wp_fz_event_id );
	if( !empty( $rStats ) ) {
		$partOfRelay = 0;
		//check to see if relay or on team
		if( !empty( $rStats['TEAMNAME'] ) ) {
			$rTeamStats = getRunnersTeam( $runner_bib, $rStats['TEAMNAME'], $dbConnLink, $rc_appid, $wp_fz_event_id );
			if( !empty( $rTeamStats ) AND count( $rTeamStats ) == 1 ) {
				$partOfRelay = 1;
			}
		}
		if( $partOfRelay != 1 ) {
			displayRunnerStats( $rStats );
		} else {
			displayTeamStats( $rStats, $rTeamStats );
		}
		//see if remix
		$remixStats = getRunnerRemix( $dbConnLink, $rc_appid, $wp_fz_event_id, $rStats );
		if( !empty( $remixStats ) ) {
			//print_r( $remixStats );
			displayRunnerStats( $remixStats );
		} else {
			echo "No Remix";
		}
	} else {
		echo 'No Runner Found';
	}
	dbClose( $dbConnLink );
}


/**
 * function: displayRunnerStats
 */
function displayRunnerStats( $runnerData ) {
	echo $runnerData['BIB'].' '.$runnerData['FIRSTNAME'].' '.$runnerData['LASTNAME'].'<br/>';
	echo $runnerData['SEX'].' '.$runnerData['AGE'].' '.$runnerData['DIVISION'].'<br/>';
	if( !empty( $runnerData['CITY'] ) ) {
		echo $runnerData['CITY'];
	}
	echo ' ';
	if( !empty( $runnerData['STATE'] ) ) {
		echo $runnerData['STATE'];
	}
	echo ' ';
	if( !empty( $runnerData['COUNTRY'] ) ) {
		echo $runnerData['COUNTRY'];
	}
	echo '<br/>';

	echo $runnerData['GUNSTART'];
	if( !empty( $runnerData['CHIPSTART'] ) ) {
		echo $runnerData['CHIPSTART'];
		$timeDiff = strtotime( $runnerData['CHIPSTART'] ) - strtotime( $runnerData['GUNSTART'] );
	} else {
		echo $runnerData['GUNSTART'];
	}
	echo ' Clocktime ';
	$ftimeEst = date ( 'H:i:s' , ( strtotime( $runnerData['FINISHTIME'] ) + $timeDiff ) );
	echo $ftimeEst;
	echo ' ';
	echo $runnerData['FINISHTIME'];
	echo ' ';
	echo $runnerData['PACE'];
	echo '<br/>';

	if( !empty( $runnerData['PLACEOALL_CHIP'] ) ) {
		echo $runnerData['PLACEOALL_CHIP'];
		echo ' ';
	}
	if( !empty( $runnerData['PLACESEX_CHIP'] ) ) {
		echo $runnerData['PLACESEX_CHIP'];
		echo ' ';
	}
	if( !empty( $runnerData['PLACEDIVISION_CHIP'] ) ) {
		echo $runnerData['PLACEDIVISION_CHIP '];
		echo ' ';
	}
	if( !empty( $runnerData['PLACEMASTER'] ) ) {
		echo $runnerData['PLACEMASTER'];
	}
	echo '<br/>';

	if( !empty( $runnerData['PR_TIME'] ) ) {
		echo $runnerData['PR_TIME'];
		echo ' ';
	}
	if( !empty( $runnerData['SPECIALDIVISION'] ) ) {
		echo $runnerData['SPECIALDIVISION'];
		echo ' ';
	}
	if( !empty( $runnerData['TEAMNAME'] ) ) {
		echo $runnerData['TEAMNAME'];
		echo ' ';
	}
	if( !empty( $runnerData['TEAMDIVISION'] ) ) {
		echo $runnerData['TEAMDIVISION'];
	}
	echo '<br/>';

	//get MatSequence
	$matsUsed = explode( ", ", $runnerData['SPLITSEQUENCE'] );
	//display time, average, and distance in order
	foreach( $matsUsed as $keyMats => $mat_value ) {
		$mat_value_t = trim( $mat_value );
		$matTime = $mat_value_t."TIME";
		$matAvgTime = $mat_value_t."_AVERAGEPACE";
		$matDistance = $mat_value_t."_DISTANCE";
		echo $mat_value_t.' '.$runnerData[$matTime].' '.$runnerData[$matAvgTime].' '.$runnerData[$matDistance].'<br/>';
	}
}


/**
 * function: displayTeamStats
 */
function displayTeamStats( $runnerData, $teamData ) {
	if( !empty( $runnerData['TEAMNAME'] ) ) {
		echo $runnerData['TEAMNAME'];
		echo ' ';
	}
	if( !empty( $runnerData['TEAMDIVISION'] ) ) {
		echo $runnerData['TEAMDIVISION'];
	}
	echo '<br/>';
	echo $runnerData['BIB'].' '.$runnerData['FIRSTNAME'].' '.$runnerData['LASTNAME'].'<br/>';
	if( sizeof( $teamData ) == 1 ) {
		$teamMate = $teamData[0];
		echo $teamMate['BIB'].' '.$teamMate['FIRSTNAME'].' '.$teamMate['LASTNAME'].'<br/>';
	} else {
		foreach( $teamData as $keyMats => $mat_value ) {
			//wait here for multiple
		}
	}

	echo $runnerData['GUNSTART'];
	if( !empty( $runnerData['CHIPSTART'] ) ) {
		echo $runnerData['CHIPSTART'];
	} else {
		echo $teamMate['CHIPSTART'];
	}
	echo ' ';
	if( !empty( $runnerData['FINISHTIME'] ) ) {
		echo $runnerData['FINISHTIME'];
	} else {
		echo $teamMate['FINISHTIME'];
	}
	echo ' ';
	if( !empty( $runnerData['PACE'] ) ) {
		echo $runnerData['PACE'];
	} else {
		echo $teamMate['PACE'];
	}
	echo '<br/>';

	if( !empty( $runnerData['PLACEOALL_CHIP'] ) ) {
		echo $runnerData['PLACEOALL_CHIP'];
	} else {
		echo $teamMate['PLACEOALL_CHIP'];
	}
	echo ' ';
	if( !empty( $runnerData['PLACEDIVISION_CHIP'] ) ) {
		echo $runnerData['PLACEDIVISION_CHIP'];
	} else {
		echo $teamMate['PLACEDIVISION_CHIP'];
	}
	echo '<br/>';

	//do not display Estimate Finish Time for Relays - next field
	if( !empty( $runnerData['SPECIALDIVISION'] ) ) {
		echo $runnerData['SPECIALDIVISION'];
	}
	echo '<br/>';

	//get MatSequence
	$matsUsed = explode( ", ", $runnerData['SPLITSEQUENCE'] );
	//display time, average, and distance in order
	foreach( $matsUsed as $keyMats => $mat_value ) {
		$mat_value_t = trim( $mat_value );
		$matTime = $mat_value_t."TIME";
		$matAvgTime = $mat_value_t."_AVERAGEPACE";
		$matDistance = $mat_value_t."_DISTANCE";
		if( !empty( $runnerData[$matTime] ) ) {
			echo 'You '. $mat_value_t.' '.$runnerData[$matTime].' '.$runnerData[$matAvgTime].' '.$runnerData[$matDistance].'<br/>';
		} else {
			echo 'Teammate '. $mat_value_t.' '.$teamMate[$matTime].' '.$teamMate[$matAvgTime].' '.$teamMate[$matDistance].'<br/>';
		}
	}
}


/**
 * function: allTopTensForEvent
 */
function allTopTensForEvent( $rc_appid, $wp_fz_event_id ) {
	echo "All Top Tens for ".$rc_appid." ".$wp_fz_event_id."<br/>";
	$dbConnLink = dbConnect( 'results' );
	//dbSelection( $dbConnLink, 'results' );
	$availableRaceIDs = getRaceCentralRAIDs( $dbConnLink, $rc_appid, $wp_fz_event_id );
	foreach( $availableRaceIDs as $keys => $rc_value ) {
		echo 'Top Tens for '.$rc_value.'<br/>';
		$topTen = getTopTenSex( $rc_value, $dbConnLink, $rc_appid, $wp_fz_event_id );
		if( !empty( $topTen ) ) {
			foreach( $topTen as $keyGender => $gender_value ) {
				echo 'Gender '.$keyGender .'<br/>';
				foreach( $gender_value as $keyPlace => $place_value ) {
					if( !empty( $place_value['TEAMNAME'] ) ) {
						$ftimeEst = date ( 'H:i:s' , ( strtotime( $place_value['FINISHTIME'] ) - $place_value['DIFFTIME'] ) );
						echo $keyPlace .' = '. $place_value['TEAMNAME'].' '.$ftimeEst.'<br/>';
					} else {
						echo $keyPlace .' = '. $place_value['FIRSTNAME'].' '.$place_value['LASTNAME']. ' '.$place_value['FINISHTIME'].'<br/>';
					}
				}
			}
		} else {
			echo 'No Top Ten';
		}
	}
	dbClose( $dbConnLink );
}


/**
 * function: allTopTenForEvent_display
 */
function allTopTenForEvent_display( $qt_lang, $rc_appid, $wp_fz_event_id, $yearid ) {
	$subinfo					= getFZSubEvents( $wp_fz_event_id);
	$dbConnLink				= dbConnect( 'results' );
	$availableRaceIDs	= getRaceCentralRAIDs( $dbConnLink, $rc_appid, $wp_fz_event_id );
	$i = $x = 1;
	$redirecturl	= $_SERVER['REDIRECT_URL'];
	$qt_status		= $qt_lang['enabled'] == 1 ? true : false;
	$qt_cur_lang	= $qt_lang['lang'];

	$female_txt			= 'Female';
	$male_txt				= 'Male';
	$gender_pl_txt	= 'Gender Pl';
	$name_txt				= 'Name';
	$time_txt				= 'Time';

	if( $qt_status ){
		if( $qt_cur_lang == 'es' ){
			$female_txt			= 'Femenil';
			$male_txt				= 'Varonil';
			$gender_pl_txt	= 'Lugar: G&eacute;nero';
			$name_txt				= 'Nombre';
			$time_txt				= 'Tiempo';
		}
	}

	echo '<div id="fz_tabs">
		<ul class="fz_racetypes">';
			foreach( $availableRaceIDs as $keys => $rc_value ) {
				if( !empty($subinfo[$rc_value]) AND $subinfo[$rc_value]['published'] == 1 ){
					$race_title = empty($subinfo[$rc_value]['display_title']) ? $rc_value : $subinfo[$rc_value]['display_title'];
					$race_title = $qt_status ? qtrans_split( $race_title )[$qt_cur_lang] : $race_title;

					echo '<li><a href="#tabs-'. $i .'">';
					echo $race_title;
					echo '</a></li>';
					$i++;
				}
			}
		echo '</ul>';

		foreach( $availableRaceIDs as $keys => $rc_value ) {
			if( !empty($subinfo[$rc_value]) AND $subinfo[$rc_value]['published'] == 1){
				echo '<div id="tabs-'. $x .'">
					<div class="grid_2_special">';
						$topTen = getTopTenSex( $rc_value, $dbConnLink, $rc_appid, $wp_fz_event_id,$subinfo[$rc_value]['fk_race_type_id'] );

						if( !empty( $topTen ) ) {
							foreach( $topTen as $keyGender => $gender_value ) {
								$keyGender = trim($keyGender);
								if( $keyGender == 'F' || $keyGender == 'Femenil') {
									$genderdisplay = $female_txt;
								} elseif(stripos($keyGender,'w') !== false OR $keyGender == 'Coed' OR stripos($keyGender,'c') === true OR $keyGender == 'Men') {
									$genderdisplay = ucfirst($keyGender);
								}else {
									$genderdisplay = $male_txt;
								}

								echo '<div class="column_special">
									<table class="fz_rank_table">
										<thead>
											<tr><th colspan="3">Top '. $genderdisplay .'</th></tr>
										</thead>
										<tbody>
										<tr><th>'. $gender_pl_txt .'</th><th>'. $name_txt .'</th><th>'. $time_txt .'</th></tr>';

										foreach( $gender_value as $keyPlace => $place_value ) {
											$indiv_criteria = array(
												'eventid'			=> $wp_fz_event_id,
												'subeventid'	=> $rc_value,
												'bib'					=> $place_value['BIB'],
											);

											$searchstring = get_query_string( $indiv_criteria, '', 1 );

											if( !empty( $place_value['TEAMNAME'] ) ) {
												if(!empty($place_value['FINISHTIME'])){
													$ftimeEst = date ( 'H:i:s' , ( strtotime( $place_value['FINISHTIME'] ) - $place_value['DIFFTIME'] ) );
												}else{
													$ftimeEst = " - ";
												}
												echo '<tr><td>'. $keyPlace .'</td><td> <a href="'. $redirecturl .'?resultpage=1'. $searchstring .'">'. $place_value['TEAMNAME'].'</td><td>'. $ftimeEst .'</td></tr>';
											} else {
												echo '<tr><td>'. $keyPlace .'</td><td><a href="'. $redirecturl .'?resultpage=1'. $searchstring .'">'. $place_value['FIRSTNAME'] .' '. $place_value['LASTNAME']. '</td><td>'. $place_value['FINISHTIME'] .'</td></tr>';
											}
										}
									echo '</tbody></table>
								</div>';
							}
						} else {
							echo 'No Top Ten';
						}
					echo '</div>
				</div>';
				$x++;
			}
		}
	echo '</div>
	<script>
		$(function() {
			$("#fz_tabs").tabs();
		});
	</script>';
	dbClose( $dbConnLink );
}


/**
 * function: dbConnect
 */
function dbConnect( $hostToUse ) {
	//connect to db
	if( $hostToUse == 'results' ) {
		$wp_host	= RESULTSHOSTNAME;
		$wp_user	= RESULTSDBUSER;
		$wp_pwd		= RESULTSDBPWD;
		$wp_db		= RESULTSDBNAME;
	} else {
		$wp_host	= EVENTHOSTNAME;
		$wp_user	= EVENTDBUSER;
		$wp_pwd		= EVENTDBPWD;
		$wp_db		= EVENTDBNAME;
	}
	// we connect to db
	$link_wrwp = mysqli_connect( $wp_host, $wp_user, $wp_pwd );
	$link_wrwp->set_charset('utf8');
	if ( !$link_wrwp ) {
		die( 'Could not connect: ' . mysqli_error( ) );
	}
	//echo 'Connected to db successfully<br/>';
	return $link_wrwp;
}


/**
 * function: dbSelection
 */
function dbSelection( $dbLinkConnection, $hostToUse ) {
	if( $hostToUse == 'results' ) {
		$wp_db = RESULTSDBNAME;
	} else {
		$wp_db = EVENTDBNAME;
	}

	if ( !mysqli_select_db( $wp_db, $dbLinkConnection ) ) {
		die( 'Could not select database: ' . mysqli_error( ) );
	}
}


/**
 * function: dbClose
 */
function dbClose( $dbLinkConnection ) {
	mysqli_close( $dbLinkConnection );
}


/**
 * function: getTopTenSex
 */
function getTopTenSex( $rc_race_id, $dbLinkConnection, $rc_appid, $wp_fz_event_id, $race_type_id = 1) {
	$table_name = $rc_appid."_".$wp_fz_event_id;
	if($race_type_id == 5){
		//Relay and teams
		$topTenstatement = 'SELECT FIRSTNAME, LASTNAME, BIB, FINISHTIME, SEX, PLACESEX_CHIP, TEAMNAME, TEAMDIVISION, RELAYPLACEOALL, RELAYPLACEDIVISION, CHIPSTART, GUNSTART FROM '.RESULTSDBNAME.'.'.$table_name.' WHERE RACEID = "'.$rc_race_id.'" AND ( ( PLACESEX_CHIP <= 10 AND TEAMNAME IS NULL AND FINISHTIME IS NOT NULL ) OR ( RELAYPLACEDIVISION IS NOT NULL AND RELAYPLACEDIVISION <= 10 ) ) ORDER BY TEAMDIVISION, RELAYPLACEDIVISION, SEX, PLACESEX_CHIP';
	}else{
		$topTenstatement = 'SELECT FIRSTNAME, LASTNAME, BIB, FINISHTIME, SEX, PLACESEX_CHIP, CHIPSTART, GUNSTART FROM '.RESULTSDBNAME.'.'.$table_name.' WHERE RACEID = "'.$rc_race_id.'" AND PLACESEX_CHIP <= 10 AND FINISHTIME IS NOT NULL ORDER BY SEX, PLACESEX_CHIP';
	}
	$result1 = mysqli_query( $dbLinkConnection, $topTenstatement );
	$values = "";
	$topTenArray = array( );
	while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
		//print_r( $row1 );
		if( empty( $row1['TEAMNAME'] ) ) {
			$topTenArray[$row1['SEX']][$row1['PLACESEX_CHIP']]['FIRSTNAME'] = $row1['FIRSTNAME'];
			$topTenArray[$row1['SEX']][$row1['PLACESEX_CHIP']]['LASTNAME'] = $row1['LASTNAME'];
			$topTenArray[$row1['SEX']][$row1['PLACESEX_CHIP']]['FINISHTIME'] = $row1['FINISHTIME'];
			$topTenArray[$row1['SEX']][$row1['PLACESEX_CHIP']]['BIB'] = $row1['BIB'];
		} else {
			$topTenArray[$row1['TEAMDIVISION']][$row1['RELAYPLACEDIVISION']]['TEAMNAME'] = $row1['TEAMNAME'];
			$topTenArray[$row1['TEAMDIVISION']][$row1['RELAYPLACEDIVISION']]['BIB'] = $row1['BIB'];
			if( !empty( $row1['FINISHTIME'] ) ) {
				$topTenArray[$row1['TEAMDIVISION']][$row1['RELAYPLACEDIVISION']]['FINISHTIME'] = $row1['FINISHTIME'];
			}
			if( !empty( $row1['CHIPSTART'] ) ) {
				$timeDiff = strtotime( $row1['CHIPSTART'] ) - strtotime( $row1['GUNSTART'] );
				$topTenArray[$row1['TEAMDIVISION']][$row1['RELAYPLACEDIVISION']]['DIFFTIME'] = $timeDiff;
			}
		}
	}
	return $topTenArray;
}


/**
 * function: getRaceCentralRAIDs
 */
function getRaceCentralRAIDs( $dbLinkConnection, $rc_appid, $wp_fz_event_id ) {
	$table_name = $rc_appid."_".$wp_fz_event_id;
	$distinctstatement = "SELECT DISTINCT RACEID FROM ".RESULTSDBNAME.".".$table_name." WHERE FINISHTIME IS NOT NULL AND RACEID IS NOT NULL";
	//echo $distinctstatement;
	$result1 = mysqli_query( $dbLinkConnection, $distinctstatement );
	$race_ids = array( );
	$ri = 0;
	while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
		$race_ids[$ri] = $row1['RACEID'];
		$ri++;
	}
	return $race_ids;
}


/**
 * function: getRunnerStats
 */
function getRunnerStats( $bib, $dbLinkConnection, $rc_appid, $wp_fz_event_id ) {
	$table_name = $rc_appid."_".$wp_fz_event_id;
	$topTenstatement = 'SELECT * FROM '.RESULTSDBNAME.'.'.$table_name.' WHERE BIB = "'.$bib.'" AND DQ <> 1 AND ( FINISHTIME IS NOT NULL OR TEAMNAME IS NOT NULL )';
	$result1 = mysqli_query( $dbLinkConnection, $topTenstatement );
	$values = "";
	$runnerStats = array( );
	while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
		$runnerStats = $row1;
	}
	return $runnerStats;
}


/**
 * function: getRunnerRemix
 */
function getRunnerRemix( $dbLinkConnection, $rc_appid, $wp_fz_event_id, $runnerData ) {
	$table_name = $rc_appid."_".$wp_fz_event_id;
	$remixStatement = 'SELECT * FROM '.RESULTSDBNAME.'.'.$table_name.' WHERE BIB <> "'.$runnerData['BIB'].'"';
	$remixStatement .= ' AND FIRSTNAME = "'.$runnerData['FIRSTNAME'].'"';
	$remixStatement .= ' AND LASTNAME = "'.$runnerData['LASTNAME'].'"';
	$remixStatement .= ' AND SEX = "'.$runnerData['SEX'].'"';
	$remixStatement .= ' AND AGE = "'.$runnerData['AGE'].'"';
	$remixStatement .= ' AND EMAIL = "'.$runnerData['EMAIL'].'"';
	$remixStatement .= ' AND DQ <> 1 AND ( FINISHTIME IS NOT NULL OR TEAMNAME IS NOT NULL )';
	$result1 = mysqli_query( $dbLinkConnection, $remixStatement );
	$remixStats = array( );
	$n = 1;
	while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
		$remixStats[$n] = $row1;
		$n++;
	}
	return $remixStats;
}


/**
 * function: getRunnersTeam
 */
function getRunnersTeam( $bib, $team_name, $dbLinkConnection, $rc_appid, $wp_fz_event_id ) {
	ini_set( 'memory_limit', '-1' );
	ini_set( 'max_execution_time', 300 );

	$table_name = $rc_appid."_".$wp_fz_event_id;
	$topTenstatement = 'SELECT * FROM '.RESULTSDBNAME.'.'.$table_name.' WHERE BIB <> "'.$bib.'" AND DQ <> 1 AND TEAMNAME = "'.$team_name.'"';
	$result1 = mysqli_query( $dbLinkConnection, $topTenstatement );
	$values = "";
	$runnerStats = array( );
	while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
		$runnerStats[] = $row1;
	}
	return $runnerStats;
}


/**
 * function: getDropdownList
 */
function getDropdownList( $rc_appid, $wp_fz_event_id, $rc_race_id = NULL ) {
	$dbConnLink = dbConnect( 'results' );
	//dbSelection( $dbConnLink, 'results' );
	$distinct = array( );
	$distinct['gender']	 = getDistinctList( $dbConnLink, $rc_appid, $wp_fz_event_id, "SEX", $rc_race_id );
	$distinct['division'] = getDistinctList( $dbConnLink, $rc_appid, $wp_fz_event_id, "DIVISION", $rc_race_id );
	$distinct['state']		= getDistinctList( $dbConnLink, $rc_appid, $wp_fz_event_id, "STATE", $rc_race_id );
	if(strpos($rc_appid, 'DUB') !== false OR strpos($rc_appid, 'LIV') !== false){
		$distinct['club']		= getDistinctList( $dbConnLink, $rc_appid, $wp_fz_event_id, "CLUB", $rc_race_id );
	}
	$distinct['military']		= getDistinctList( $dbConnLink, $rc_appid, $wp_fz_event_id, "MILITARY_BRANCH", $rc_race_id, 1 );
	dbClose( $dbConnLink );
	return $distinct;
}


/**
 * function: getDistinctList
 */
function getDistinctList( $dbLinkConnection, $rc_appid, $wp_fz_event_id, $distinct_column, $rc_race_id = NULL, $isMil = 0 ) {
	$table_name = $rc_appid."_".$wp_fz_event_id;
	if( empty( $rc_race_id ) ) {
		$distinctStmt = "SELECT DISTINCT ".$distinct_column." FROM ".RESULTSDBNAME.".".$table_name." WHERE ( FINISHTIME IS NOT NULL OR TEAMNAME IS NOT NULL )";
		if($distinct_column == "DIVISION"){
			$distinctStmt .= " AND (DIVISION NOT LIKE '%Age' AND DIVISION NOT LIKE '%01-99' AND DIVISION != 'unknown')";
		}
		if($isMil == 1){
			$distinctStmt .= " AND MILOALLPLACE IS NOT NULL";
		}
		$distinctStmt .= " ORDER BY ".$distinct_column;
	} else {
		$distinctStmt = 'SELECT DISTINCT '.$distinct_column.' FROM '.RESULTSDBNAME.'.'.$table_name.' WHERE RACEID = "'.$rc_race_id.'" AND ( FINISHTIME IS NOT NULL OR TEAMNAME IS NOT NULL )"';
		if($isMil == 1){
			$distinctStmt .= " AND MILOALLPLACE IS NOT NULL";
		}
		$distinctStmt = ' ORDER BY '.$distinct_column;
	}
	$result1 = mysqli_query( $dbLinkConnection, $distinctStmt );
	//echo mysqli_errno( $dbLinkConnection ) . ": " . mysqli_error( $dbLinkConnection ) . "\n";
	$distinct_ids = array( );
	$ri = 0;
	$affected_rows = mysqli_affected_rows( $dbLinkConnection );
	if($affected_rows > 0){
		while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
			$distinct_ids[$ri] = $row1[$distinct_column];
			$ri++;
		}
	}
	return $distinct_ids;
}


/**
 * function: doSearchResults
 */
function doSearchResults( $rc_appid, $wp_fz_event_id, $searchParameters ) {
	$dbConnLink = dbConnect( 'results' );
	$search1 = getSearchResults( $dbConnLink, $rc_appid, $wp_fz_event_id, $searchParameters );
	dbClose( $dbConnLink );
	return $search1;
}


/**
 * function: getSearchResults
 */
function getSearchResults( $dbLinkConnection, $rc_appid, $wp_fz_event_id, $searchParameters ) {
	$table_name = $rc_appid."_".$wp_fz_event_id;
	$searchStmt = "SELECT FIRSTNAME, LASTNAME, BIB, FINISHTIME, PLACEOALL_CHIP, TEAMNAME, RELAYPLACEOALL, RACEID FROM ".RESULTSDBNAME.".".$table_name." WHERE ( (FINISHTIME IS NOT NULL AND PLACEOALL_CHIP IS NOT NULL) OR ( TEAMNAME IS NOT NULL AND RELAYPLACEOALL IS NOT NULL ) ) AND DQ = 0";
	if( !empty( $searchParameters['rc_race_id'] ) ) {
		$searchStmt .= ' AND RACEID = "'.$searchParameters['rc_race_id'].'"';
	}
	if( !empty( $searchParameters['subevent_id'] ) ) {
		$searchStmt .= ' AND RACEID = "'.$searchParameters['subevent_id'].'"';
	}
	if( !empty( $searchParameters['bib'] ) ) {
		$searchStmt .= " AND BIB = ".$searchParameters['bib'];
	}
	if( !empty( $searchParameters['firstname'] ) ) {
		$searchStmt .= ' AND FIRSTNAME LIKE "'.$searchParameters['firstname'].'%"';
	}
	if( !empty( $searchParameters['lastname'] ) ) {
		$searchStmt .= ' AND LASTNAME LIKE "'.$searchParameters['lastname'].'%"';
	}
	if( !empty( $searchParameters['division'] ) ) {
		$searchStmt .= ' AND DIVISION = "'.$searchParameters['division'].'"';
	}
	if( !empty( $searchParameters['gender'] ) ) {
		$searchStmt .= ' AND SEX = "'.$searchParameters['gender'].'"';
	}
	if( !empty( $searchParameters['city'] ) ) {
		$searchStmt .= ' AND CITY LIKE "'.$searchParameters['city'].'%"';
	}
	if( !empty( $searchParameters['state'] ) ) {
		$searchStmt .= ' AND STATE = "'.$searchParameters['state'].'"';
	}
	if( !empty( $searchParameters['strTeamName'] ) ) {
		$searchStmt .= ' AND TEAMNAME = "'.$searchParameters['strTeamName'].'"';
	}
	if( !empty( $searchParameters['club'] ) ) {
		$searchStmt .= ' AND CLUB = "'.$searchParameters['club'].'"';
	}
	if( !empty( $searchParameters['military'] ) ) {
		$searchStmt .= ' AND MILITARY_BRANCH = "'.$searchParameters['military'].'"';
	}
	$searchStmt .= " ORDER BY RACEID, PLACEOALL_CHIP, RELAYPLACEOALL";

	if( !empty( $searchParameters['perpage'] ) ) {
		$searchStmt .= ' LIMIT ' . $searchParameters['perpage'];
	}
	if( !empty( $searchParameters['resultspage'] ) ) {
		$resultspage = $searchParameters['resultspage'];
		$totalrunners = getTotalRunners( $rc_appid, $wp_fz_event_id, $searchParameters );
		$totalpages = $totalrunners[0] / $searchParameters['perpage'];

		if( $totalrunners[0] != 0 ) {
			if( $resultspage > ceil( $totalpages ) ) {
				$offset = ( $totalpages * $searchParameters['perpage'] ) - $searchParameters['perpage'];
			} else {
				$offset = ( ( $resultspage - 1 ) * $searchParameters['perpage'] );
			}
			$searchStmt .= ' OFFSET ' . $offset;
		}
	}

	// echo $searchStmt;
	$result1 = mysqli_query( $dbLinkConnection, $searchStmt );
	$runnerStats = array( );
	while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
		$runnerStats[] = $row1;
	}
	return $runnerStats;
}


/**
 * function: getTotalRunners
 */
function getTotalRunners( $rc_appid, $wp_fz_event_id, $criteria ) {
	$dbConnLink = dbConnect( 'results' );

	$table_name = $rc_appid."_".$wp_fz_event_id;
	$total_runners_query = "SELECT COUNT(*) FROM ".RESULTSDBNAME.".". $table_name . " WHERE ( FINISHTIME IS NOT NULL OR ( TEAMNAME IS NOT NULL AND RELAYPLACEOALL IS NOT NULL ) ) AND DQ = 0";
	/*if( !empty( $criteria['rc_race_id'] ) ) {
		$total_runners_query .= ' AND RACEID = "'.$criteria['rc_race_id'].'"';
	}*/
	if( !empty( $criteria['subevent_id'] ) ) {
		$total_runners_query .= ' AND RACEID = "'.$criteria['subevent_id'].'"';
	}
	if( !empty( $criteria['bib'] ) ) {
		$total_runners_query .= " AND BIB = ".$criteria['bib'];
	}
	if( !empty( $criteria['firstname'] ) ) {
		$total_runners_query .= ' AND FIRSTNAME LIKE "'.$criteria['firstname'].'%"';
	}
	if( !empty( $criteria['lastname'] ) ) {
		$total_runners_query .= ' AND LASTNAME LIKE "'.$criteria['lastname'].'%"';
	}
	if( !empty( $criteria['division'] ) ) {
		$total_runners_query .= ' AND DIVISION = "'.$criteria['division'].'"';
	}
	if( !empty( $criteria['gender'] ) ) {
		$total_runners_query .= ' AND SEX = "'.$criteria['gender'].'"';
	}
	if( !empty( $criteria['city'] ) ) {
		$total_runners_query .= ' AND CITY LIKE "'.$criteria['city'].'%"';
	}
	if( !empty( $criteria['state'] ) ) {
		$total_runners_query .= ' AND STATE = "'.$criteria['state'].'"';
	}
	if( !empty( $criteria['strTeamName'] ) ) {
		$total_runners_query .= ' AND TEAMNAME = "'.$criteria['strTeamName'].'"';
	}
	if( !empty( $criteria['club'] ) ) {
		$total_runners_query.= ' AND CLUB = "'.$criteria['club'].'"';
	}
	if( !empty( $criteria['military'] ) ) {
		$total_runners_query.= ' AND MILITARY_BRANCH = "'.$criteria['military'].'"';
	}

	if( $total_runners_result = mysqli_query( $dbConnLink, $total_runners_query ) ) {
		$total_runners = mysqli_fetch_array( $total_runners_result, MYSQLI_NUM );
	}
	$total_runners_result->close();

	dbClose( $dbConnLink );
	return $total_runners;

}


/**
 * function: get_correction_link
 */
function get_correction_link($qt_lang) {
	if( $qt_lang['lang'] == 'es'){
		$submit_corr_txt			= 'Somete una correcci&oacute;n';
	}else{
		$submit_corr_txt			= 'Submit a Correction';
	}
	echo '<p class="center"><a href="http://www.runrocknroll.com/contact/corrections/">'. $submit_corr_txt .'</a></p>';
}


/**
 * function:
 */
function get_query_string( $criteria, $exclude = NULL, $individual = 0 ) {
	if( $individual != 1 ) {
		/* default criteria
			$criteria = array(
				'firstname' => trim( $firstname ),
				'lastname'	=> trim( $lastname ),
				'bib'			 => trim( $bib ),
				'gender'		=> trim( $gender ),
				'division'	=> trim( $division ),
				'state'		 => trim( $state ),
				'city'			=> trim( $city )
			);
		*/
		$query_string = '';
		foreach( $criteria as $key => $value ) {
			if( $key != $exclude ) {
				$query_string .= '&' . $key . '=' . $value;
			}
		}
	} else {
		$query_string = '';
		foreach( $criteria as $key => $value ) {
			if( $key != $exclude ) {
				$query_string .= '&' . $key . '=' . $value;
			}
		}

	}
	return $query_string;
}


/**
 * function getBars
 */
function getBars( $speed ) {
	$speed_array = explode( '.', $speed );
	//echo $speed;
	$singlebar = '';
	$color_array = array(
		'red',
		'orange',
		'yellow',
		'green',
		'ltblue',
	);

	$color_index = 5 - $speed_array[0];
	$eq_color = $color_array[$color_index];
	// $blank_space = 3 - $speed_array[1] + ( $color_index * 4 );
	$blank_space2 = 15 - ( ( $speed_array[0] * 3 ) - ( 3 - $speed_array[1] ) );
	for( $a = 0; $a < $blank_space2; $a++ ) {
		$singlebar .= '<span class="fz_eqbar"><span class="fz_block"><span class="fz_eqbar_color fz_eqbar_clear"></span></span><span class="fz_block"><span class="fz_eqbar_color fz_eqbar_clear"></span></span><span class="fz_block"><span class="fz_eqbar_color fz_eqbar_clear"></span></span></span>';
	}
	for( $i = 0; $i < $speed_array[0]; $i++ ) {
		$eq_color = $color_array[$color_index++];
		if( $i == 0 ) {
			for( $x = 0; $x < $speed_array[1]; $x++ ) {
				$singlebar .= '<span class="fz_eqbar"><span class="fz_block"><span class="fz_eqbar_color	fz_eqbar_'. $eq_color .'"></span></span><span class="fz_block"><span class="fz_eqbar_color	fz_eqbar_'. $eq_color .'"></span></span><span class="fz_block"><span class="fz_eqbar_color	fz_eqbar_'. $eq_color .'"></span></span></span>';
			}
		} else {
			$singlebar .= '<span class="fz_eqbar"><span class="fz_block"><span class="fz_eqbar_color	fz_eqbar_'. $eq_color .'"></span></span><span class="fz_block"><span class="fz_eqbar_color	fz_eqbar_'. $eq_color .'"></span></span><span class="fz_block"><span class="fz_eqbar_color	fz_eqbar_'. $eq_color .'"></span></span></span><span class="fz_eqbar"><span class="fz_block"><span class="fz_eqbar_color	fz_eqbar_'. $eq_color .'"></span></span><span class="fz_block"><span class="fz_eqbar_color	fz_eqbar_'. $eq_color .'"></span></span><span class="fz_block"><span class="fz_eqbar_color	fz_eqbar_'. $eq_color .'"></span></span></span><span class="fz_eqbar"><span class="fz_block"><span class="fz_eqbar_color	fz_eqbar_'. $eq_color .'"></span></span><span class="fz_block"><span class="fz_eqbar_color	fz_eqbar_'. $eq_color .'"></span></span><span class="fz_block"><span class="fz_eqbar_color	fz_eqbar_'. $eq_color .'"></span></span></span>';

		}
	}
	return $singlebar;
}


/**
 * function getPaceRange
 * 5 colors, 20 second intervals
 */
function getPaceRange( $avgpace ) {
	switch( $avgpace ) {

		// less than 8:40 and you're a speedster!
		case( $avgpace <= strtotime( '00:08:40' ) ) :
			$displayeq = getBars( 5.3 );
			return $displayeq;
			break;

		case( $avgpace >= strtotime( '00:08:41' ) && $avgpace <= strtotime( '00:09:00' ) ) :
			$displayeq = getBars( 5.2 );
			return $displayeq;
			break;

		case( $avgpace >= strtotime( '00:09:01' ) && $avgpace <= strtotime( '00:09:20' ) ) :
			$displayeq = getBars( 5.1 );
			return $displayeq;
			break;

		case( $avgpace >= strtotime( '00:09:21' ) && $avgpace <= strtotime( '00:09:40' ) ) :
			$displayeq = getBars( 4.3 );
			return $displayeq;
			break;

		case( $avgpace >= strtotime( '00:09:41' ) && $avgpace <= strtotime( '00:10:00' ) ) :
			$displayeq = getBars( 4.2 );
			return $displayeq;
			break;

		case( $avgpace >= strtotime( '00:10:01' ) && $avgpace <= strtotime( '00:10:20' ) ) :
			$displayeq = getBars( 4.1 );
			return $displayeq;
			break;

		case( $avgpace >= strtotime( '00:10:21' ) && $avgpace <= strtotime( '00:10:40' ) ) :
			$displayeq = getBars( 3.3 );
			return $displayeq;
			break;

		case( $avgpace >= strtotime( '00:10:41' ) && $avgpace <= strtotime( '00:11:00' ) ) :
			$displayeq = getBars( 3.2 );
			return $displayeq;
			break;

		case( $avgpace >= strtotime( '00:11:01' ) && $avgpace <= strtotime( '00:11:20' ) ) :
			$displayeq = getBars( 3.1 );
			return $displayeq;
			break;

		case( $avgpace >= strtotime( '00:11:21' ) && $avgpace <= strtotime( '00:11:40' ) ) :
			$displayeq = getBars( 2.3 );
			return $displayeq;
			break;

		case( $avgpace >= strtotime( '00:11:41' ) && $avgpace <= strtotime( '00:12:00' ) ) :
			$displayeq = getBars( 2.2 );
			return $displayeq;
			break;

		case( $avgpace >= strtotime( '00:12:01' ) && $avgpace <= strtotime( '00:12:20' ) ) :
			$displayeq = getBars( 2.1 );
			return $displayeq;
			break;

		case( $avgpace >= strtotime( '00:12:21' ) && $avgpace <= strtotime( '00:12:40' ) ) :
			$displayeq = getBars( 1.3 );
			return $displayeq;
			break;

		case( $avgpace >= strtotime( '00:12:41' ) && $avgpace <= strtotime( '00:13:00' ) ) :
			$displayeq = getBars( 1.2 );
			return $displayeq;
			break;

		/*
		case( $avgpace >= strtotime( '00:13:01' ) && $avgpace <= strtotime( '00:13:20' ) ) :
			$displayeq = getBars( 1.1 );
			return $displayeq;
			break;
		case( $avgpace >= strtotime( '00:13:21' ) && $avgpace <= strtotime( '00:13:40' ) ) :
			$displayeq = getBars( 2.1 );
			return $displayeq;
			break;

		case( $avgpace >= strtotime( '00:13:41' ) && $avgpace <= strtotime( '00:14:00' ) ) :
			$displayeq = getBars( 1.4 );
			return $displayeq;
			break;

		case( $avgpace >= strtotime( '00:14:01' ) && $avgpace <= strtotime( '00:14:20' ) ) :
			$displayeq = getBars( 1.3 );
			return $displayeq;
			break;

		case( $avgpace >= strtotime( '00:14:21' ) && $avgpace <= strtotime( '00:14:40' ) ) :
			$displayeq = getBars( 1.2 );
			return $displayeq;
			break;

		// more than 14:41 for your pace and you get only 1 bar
		case( $avgpace >= strtotime( '00:14:41' ) ) :
		default:
			$displayeq = getBars( 1.1 );
			return $displayeq;
			break;
		*/

		// more than 13:01 for your pace and you get only 1 bar
		case( $avgpace >= strtotime( '00:13:01' ) ) :
		default:
			$displayeq = getBars( 1.1 );
			return $displayeq;
			break;


	}

}

function getFZSubEvents( $event_id, $active = NULL ){

	global $wpdb;
	//$query = 'SELECT * FROM wp_fz_sub_event WHERE fk_event_id = '.$event_id;
	$query = 'SELECT s.*,r.distance FROM wp_fz_sub_event s, wp_fz_race_types r WHERE s.fk_event_id = '.$event_id.' AND r.pk_race_type_id = s.fk_race_type_id';
	if($active == 1){
		$query .= ' AND published = 1';
	}
	$subevents = $wpdb->get_results( $query, ARRAY_A);
	if ( count($subevents) == 0 ) {
		$wpdb->print_error();
		return null;
	} else {
		//rearrange subevents by raceid
		$subEventInfo = array();
		foreach($subevents as $keySE => $subEI){
			$subEventInfo[$subEI['ra_id']] = $subEI;
		}
		return $subEventInfo;
	}
}

function getFZSubEventInfo( $event_id, $race_id ){

	global $wpdb;
	//$query = 'SELECT * FROM wp_fz_sub_event WHERE fk_event_id = '.$event_id;
	$query = 'SELECT s.*,r.distance FROM wp_fz_sub_event s, wp_fz_race_types r WHERE s.fk_event_id = '.$event_id.' AND s.ra_id = '.$race_id.' AND r.pk_race_type_id = s.fk_race_type_id';
	$subevents = $wpdb->get_results( $query, ARRAY_A);
	if ( count($subevents) == 0 ) {
		$wpdb->print_error();
		return null;
	} else {
		//rearrange subevents by raceid
		$subEventInfo = array();
		foreach($subevents as $keySE => $subEI){
			$subEventInfo[$subEI['ra_id']] = $subEI;
		}
		return $subEventInfo;
	}
}


function getFZOtherResults( $event_id ){

	global $wpdb;
	$query = 'SELECT * FROM wp_fz_other_results WHERE fk_event_id = '.$event_id;
	$oresults = $wpdb->get_results( $query, ARRAY_A);

	if ( count($oresults) == 0 ) {
		// $wpdb->print_error();
		return null;
	} else {
		return $oresults;
	}

}

function getCourseHighlights( $sub_event_id){

	global $wpdb;
	$query = 'SELECT * FROM wp_fz_course_highlights WHERE fk_sub_event_id = '.$sub_event_id.' ORDER BY distance';
	$chighlights = $wpdb->get_results( $query, ARRAY_A);
	if ( count($chighlights) == 0 ) {
		$wpdb->print_error();
		return null;
	} else {
		return $chighlights;
	}
}

function getFZEventInfo( $event_id, $active = NULL	){

	global $wpdb;
	$query = 'SELECT * FROM wp_fz_event WHERE pk_event_id = '.$event_id;
	if($active == 1){
		$query .= ' AND published = 1';
	}
	$eventInfo = $wpdb->get_results( $query);
	if ( count($eventInfo) == 0 ) {
		$wpdb->print_error();
		return null;
	} else {
		if(count($eventInfo) == 1){
			$eventInfoClean = $eventInfo[0];
		}else{
			$eventInfoClean = null;
		}
		return $eventInfoClean;
	}
}

//SELECT pk_event_id,display_title FROM wp_fz_event WHERE DATE_SUB(NOW(),INTERVAL 1 YEAR) <= date_start ORDER BY date_start DESC

function getLast12MonthsEvents($active = NULL){

	global $wpdb;
	$query = 'SELECT pk_event_id,display_title,name FROM wp_fz_event WHERE DATE_SUB(NOW(),INTERVAL 1 YEAR) <= date_start';
	if($active == 1){
		$query .= ' AND published = 1';
	}
	$query .= ' ORDER BY date_start DESC';
	$eventList = $wpdb->get_results( $query, ARRAY_A);
	if ( count($eventList) == 0 ) {
		$wpdb->print_error();
		return null;
	} else {
		return $eventList;
	}
}

function getFZWhatsNext( $whatsNextid ){

	global $wpdb;
	if(is_array($whatsNextid)){
		$comma_separated = implode(",", $whatsNextid);
		$query = 'SELECT * FROM wp_fz_whats_next WHERE pk_whats_next_id IN ('.$comma_separated.')';
	}else{
		$query = 'SELECT * FROM wp_fz_whats_next WHERE pk_whats_next_id = '.$whatsNextid;
	}
	$nextInfo = $wpdb->get_results( $query, ARRAY_A);
	if ( count($nextInfo) == 0 ) {
		$wpdb->print_error();
		return null;
	} else {
		$whatsNextInfo = array();
		foreach($nextInfo as $keyWN => $subWN){
					$whatsNextInfo[$subWN['pk_whats_next_id']] = $subWN;
		}
		return $whatsNextInfo;
	}
}

function getFZBadges( $badgeid ){

	global $wpdb;
	if(is_array($badgeid)){
		$comma_separated = implode(",", $badgeid);
		$query = 'SELECT s.*,r.argument FROM wp_fz_badges s, wp_fz_qualifications r WHERE pk_ID IN ('.$comma_separated.') AND r.pk_qual_id = s.fk_qualification_default AND r.type = "badge"';
	}else{
		$query = 'SELECT s.*,r.argument FROM wp_fz_badges s, wp_fz_qualifications r WHERE pk_ID = '.$badgeid.' AND r.pk_qual_id = s.fk_qualification_default AND r.type = "badge"';
	}
	$badgesInfo = $wpdb->get_results( $query, ARRAY_A);
	if ( count($badgesInfo) == 0 ) {
		$wpdb->print_error();
		return null;
	} else {
		$badgeInfo = array();
		foreach($badgesInfo as $keyWN => $subWN){
			if(is_array($badgeid)){
				foreach($badgeid as $keyBID => $subBID){
					if($subBID == $subWN['pk_ID']){
						$badgeInfo[$keyBID] = $subWN;
					}
				}
			}else{
						$badgeInfo[$subWN['pk_ID']] = $subWN;
			}
		}
		return $badgeInfo;
	}
}

function getFZPersona( $personaid ){

	global $wpdb;
	if(is_array($personaid)){
		$comma_separated = implode(",", $personaid);
		//$query = 'SELECT s.*,r.argument FROM wp_fz_persona s, wp_fz_qualifications r WHERE s.pk_persona_id IN ('.$comma_separated.') AND r.pk_qual_id = s.fk_qualification_default AND r.type = "persona"';
		$query = 'SELECT s.*,r.argument,p.pk_persona_offers_id as po_id,p.display_title as p_d_title,p.display_text as p_d_text, p.image_display as p_image, p.link_text as p_link_text, p.link as p_link FROM wp_fz_persona s, wp_fz_qualifications r, wp_fz_persona_offers p WHERE s.pk_persona_id IN ('.$comma_separated.') AND r.pk_qual_id = s.fk_qualification_default AND r.type = "persona" AND (p.pk_persona_offers_id = s.fk_persona_offers_1 OR p.pk_persona_offers_id = s.fk_persona_offers_2 OR p.pk_persona_offers_id = s.fk_persona_offers_3)';
	}else{
		$query = 'SELECT s.*,r.argument,p.pk_persona_offers_id as po_id,p.display_title as p_d_title,p.display_text as p_d_text, p.image_display as p_image, p.link_text as p_link_text, p.link as p_link FROM wp_fz_persona s, wp_fz_qualifications r, wp_fz_persona_offers p WHERE s.pk_persona_id = '.$personaid.' AND r.pk_qual_id = s.fk_qualification_default AND r.type = "persona" AND (p.pk_persona_offers_id = s.fk_persona_offers_1 OR p.pk_persona_offers_id = s.fk_persona_offers_2 OR p.pk_persona_offers_id = s.fk_persona_offers_3)';
	}
	$personasInfo = $wpdb->get_results( $query, ARRAY_A);
	if ( count($personasInfo) == 0 ) {
		$wpdb->print_error();
		return null;
	} else {
		$personaInfo = array();
		$lastPId = 0;
		foreach($personasInfo as $keyWN => $subWN){
			if($subWN['pk_persona_id'] != $lastPId){
						$personaInfo[$subWN['pk_persona_id']] = $subWN;
						$iLoop = 1;
						$useValue = 0;
						while($iLoop <= 3) {
							if($subWN['fk_persona_offers_'.$iLoop] == $subWN['po_id']){
								$useValue = $iLoop;
							}
							$iLoop++;
						}
						$personaInfo[$subWN['pk_persona_id']]['po_title_'.$useValue] = $subWN['p_d_title'];
						$personaInfo[$subWN['pk_persona_id']]['po_text_'.$useValue] = $subWN['p_d_text'];
						$personaInfo[$subWN['pk_persona_id']]['po_image_'.$useValue] = $subWN['p_image'];
						$personaInfo[$subWN['pk_persona_id']]['po_link_text_'.$useValue] = $subWN['p_link_text'];
						$personaInfo[$subWN['pk_persona_id']]['po_link_'.$useValue] = $subWN['p_link'];
			}else{
				//add in other offers
				$iLoop = 1;
						$useValue = 0;
						while($iLoop <= 3) {
							if($subWN['fk_persona_offers_'.$iLoop] == $subWN['po_id']){
								$useValue = $iLoop;
							}
							$iLoop++;
						}
						$personaInfo[$subWN['pk_persona_id']]['po_title_'.$useValue] = $subWN['p_d_title'];
						$personaInfo[$subWN['pk_persona_id']]['po_text_'.$useValue] = $subWN['p_d_text'];
						$personaInfo[$subWN['pk_persona_id']]['po_image_'.$useValue] = $subWN['p_image'];
					$personaInfo[$subWN['pk_persona_id']]['po_link_text_'.$useValue] = $subWN['p_link_text'];
						$personaInfo[$subWN['pk_persona_id']]['po_link_'.$useValue] = $subWN['p_link'];
			}
					$lastPId = $subWN['pk_persona_id'];
		}
		return $personaInfo;
	}
}


/**
 * function getTally
 * returns an array:
 * index 0 = count of possiblities for this runner
 * index 1 = array of possiblities
 */
function getTally( $subEventInfoToUse, $field = NULL) {
	$i = 1;
	$bdCount = 0;
	$badgeArray = array();
	if($field == null){
		$field = 'fk_badges_id_';
	}
	while($i <= 20) {
		if( $i < 10 ) {
			$badgeId = $field.'0' . $i;
		} else {
			$badgeId = $field . $i;
		}
		if( !empty( $subEventInfoToUse[$badgeId] ) ) {
			$badgeArray[$i] = $subEventInfoToUse[$badgeId];
			$bdCount++;
		}
		$i++;
	}
	$badgeTally[0] = $bdCount;
	$badgeTally[1] = $badgeArray;
	return $badgeTally;
}

/**
 * function bostonQualification
 * returns an integer:
 * 1 = qualified
 * 0 = not qualified
 */
function bostonQualification( $finishTime, $age, $gender, $eventDate ) {
	$qBoston = 0;
	switch($gender){
		case "F":
			switch($age){
				case ($age >= 18 AND $age <= 34):
					if(strtotime($finishTime) < strtotime('03:35:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 35 AND $age <= 39):
					if(strtotime($finishTime) < strtotime('03:40:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 40 AND $age <= 44):
					if(strtotime($finishTime) < strtotime('03:45:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 45 AND $age <= 49):
					if(strtotime($finishTime) < strtotime('03:55:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 50 AND $age <= 54):
					if(strtotime($finishTime) < strtotime('04:00:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 55 AND $age <= 59):
					if(strtotime($finishTime) < strtotime('04:10:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 60 AND $age <= 64):
					if(strtotime($finishTime) < strtotime('04:25:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 65 AND $age <= 69):
					if(strtotime($finishTime) < strtotime('04:40:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 70 AND $age <= 74):
					if(strtotime($finishTime) < strtotime('04:55:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 75 AND $age <= 79):
					if(strtotime($finishTime) < strtotime('05:10:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 80):
					if(strtotime($finishTime) < strtotime('05:25:00')){
						$qBoston = 1;
					}
					break;
				default:
					break;
			}
			break;
		case "M":
			switch($age){
				case ($age >= 18 AND $age <= 34):
					if(strtotime($finishTime) < strtotime('03:05:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 35 AND $age <= 39):
					if(strtotime($finishTime) < strtotime('03:10:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 40 AND $age <= 44):
					if(strtotime($finishTime) < strtotime('03:15:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 45 AND $age <= 49):
					if(strtotime($finishTime) < strtotime('03:25:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 50 AND $age <= 54):
					if(strtotime($finishTime) < strtotime('03:30:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 55 AND $age <= 59):
					if(strtotime($finishTime) < strtotime('03:40:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 60 AND $age <= 64):
					if(strtotime($finishTime) < strtotime('03:55:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 65 AND $age <= 69):
					if(strtotime($finishTime) < strtotime('04:10:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 70 AND $age <= 74):
					if(strtotime($finishTime) < strtotime('04:25:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 75 AND $age <= 79):
					if(strtotime($finishTime) < strtotime('04:40:00')){
						$qBoston = 1;
					}
					break;
				case ($age >= 80):
					if(strtotime($finishTime) < strtotime('04:55:00')){
						$qBoston = 1;
					}
					break;
				default:
					break;
			}
			break;
		default:
			break;
	}
	return $qBoston;
}

function qualificationTimeOrPercent($infoArray){
	$returnV = 0;
	switch($infoArray['param']){
		case "percent":
			$pctValue = 100 - $infoArray['value'];
			switch($infoArray['operator']){
				case "==":
					if($infoArray['pct'] == $pctValue){ $returnV = 1; }
					break;
				case ">=":
					if($infoArray['pct'] >= $pctValue){ $returnV = 1; }
					break;
				case ">":
					if($infoArray['pct'] > $pctValue){ $returnV = 1; }
					break;
				case "<=":
					if($infoArray['pct'] <= $pctValue){ $returnV = 1; }
					break;
				case "<":
					if($infoArray['pct'] < $pctValue){ $returnV = 1; }
					break;
				default:
					break;
			}
			break;
		case "finish":
			$returnV = 1;
			break;
		case "remix":
			if($infoArray['remix'] == 1){
				$returnV = 1;
			}
			break;
		case "lucky":
			if(is_array($infoArray['luckyfinisher'])){
				foreach($infoArray['luckyfinisher'] as $keyLFA => $subLFA){
					if($subLFA == $infoArray['badge_id']){
						$returnV = 1;
					}
				}
			}
			break;
		case "special":
			switch($infoArray['operator']){
				case "==":
					if($infoArray['bib'] == $infoArray['value']){ $returnV = 1; }
					break;
				default:
					break;
			}
			break;
		case "military":
			if($infoArray['mil'] != '123456'){
				$returnV = 1;
			}
			break;
		case "boston":
			$qualBoston = bostonQualification( $infoArray['ft'], $infoArray['age'], $infoArray['sex'], $infoArray['eventdate'] );
			if($qualBoston == 1){
				$returnV = 1;
			}
			break;
		case "time":
			$timeFinish = strtotime($infoArray['ft']);
			$compareTime = strtotime($infoArray['value']);
			switch($infoArray['operator']){
				case "==":
					if($timeFinish == $compareTime){ $returnV = 1; }
					break;
				case ">=":
					if($timeFinish >= $compareTime){ $returnV = 1; }
					break;
				case ">":
					if($timeFinish > $compareTime){ $returnV = 1; }
					break;
				case "<=":
					if($timeFinish <= $compareTime){ $returnV = 1; }
					break;
				case "<":
					if($timeFinish < $compareTime){ $returnV = 1; }
					break;
				default:
					break;
			}
			break;
		default:
			break;
	}
	return $returnV;
}

/**
 * function: get day or month by language
 */
function get_month_by_language( $lang, $position) {
	--$position;
	switch( $lang ){
		case 'pt':
			$meses_short = array("jan", "fev", "mar&ccedil;o", "abril", "maio", "junho", "julho", "agosto", "set", "out", "nov", "dez");
			break;
		case 'es':
			$meses_short = array("enero", "feb", "marzo", "abr", "mayo", "jun", "jul", "agosto", "set", "oct", "nov", "dic");
			break;
		case 'fr':
			$meses_short = array("janv", "fevr", "mars", "avril", "mai", "juin", "juil", "aout", "sept", "oct", "nov", "dec");
			break;
		default:
			$meses_short = array("jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sept", "oct", "nov", "dec");
			break;
	}

	return $meses_short[$position];
}

function isLuckyFinisher( $bib, $subEventId ){

	global $wpdb;

	$query = 'SELECT * FROM wp_fz_lucky_winners WHERE bib = '.$bib.' AND fk_sub_event_id = '.$subEventId;

	$luckyFInfo = $wpdb->get_results( $query, ARRAY_A);
	if ( count($luckyFInfo) == 0 ) {
		//$wpdb->print_error();
		return 0;
	} else {
		//Result found is lucky finisher
		$lFInfo = array();
		foreach($luckyFInfo as $keyWN => $subWN){
					$lFInfo[$keyWN] = $subWN['fk_n_wp_fz_badges_pk_ID'];
		}
		return $lFInfo;
	}
}

function getAllBadgeIDsForEvent( $eventid ){

	global $wpdb;

	$query = 'SELECT fk_badges_id_01,fk_badges_id_02,fk_badges_id_03,fk_badges_id_04,fk_badges_id_05,fk_badges_id_06,fk_badges_id_07,fk_badges_id_08,fk_badges_id_09,fk_badges_id_10,fk_badges_id_11,fk_badges_id_12,fk_badges_id_13,fk_badges_id_14,fk_badges_id_15,fk_badges_id_16,fk_badges_id_17,fk_badges_id_18,fk_badges_id_19,fk_badges_id_20
	FROM wp_fz_sub_event WHERE fk_event_id = '.$eventid.' AND published = 1';

	$allBadgeInfo = $wpdb->get_results( $query, ARRAY_A);
	if ( count($allBadgeInfo) == 0 ) {
		//$wpdb->print_error();
		return 0;
	} else {
		//Result found all badges for event
		$aBInfo = array();
		foreach($allBadgeInfo as $keyWN => $subWN){
			//loop through badges
			$i = 1;
			$field = 'fk_badges_id_';
			while($i <= 20) {
				if( $i < 10 ) {
					$badgeId = $field.'0' . $i;
				} else {
					$badgeId = $field . $i;
				}
					if( !empty( $subWN[$badgeId] ) ) {
						//$badgeArray[$i] = $subEventInfoToUse[$badgeId];
						$aBInfo[$subWN[$badgeId]] = $subWN[$badgeId];
					}
					$i++;
			}
		}
		return $aBInfo;
	}
}

function getFZBadgesNoQual( $badgeid, $excludeGlobal = NULL ){

	global $wpdb;
	if(is_array($badgeid)){
		$comma_separated = implode(",", $badgeid);
		$query = 'SELECT * FROM wp_fz_badges WHERE pk_ID IN ('.$comma_separated.')';
	}else{
		$query = 'SELECT * FROM wp_fz_badges WHERE pk_ID = '.$badgeid;
	}
	if($excludeGlobal >= 1){
		$query .= ' AND fk_event_id = "'.$excludeGlobal.'"';
	}
	$badgesInfo = $wpdb->get_results( $query, ARRAY_A);
	if ( count($badgesInfo) == 0 ) {
		$wpdb->print_error();
		return null;
	} else {
		$badgeInfo = array();
		foreach($badgesInfo as $keyWN => $subWN){
			if(is_array($badgeid)){
				foreach($badgeid as $keyBID => $subBID){
					if($subBID == $subWN['pk_ID']){
						$badgeInfo[$keyBID] = $subWN;
					}
				}
			}else{
						$badgeInfo[$subWN['pk_ID']] = $subWN;
			}
		}
		return $badgeInfo;
	}
}

/**
 * function: shareMessage
 * Translating function
 * returns string to print/manipulate
 */

function shareMessage( $qt_lang, $item_title, $item_type, $hashtag, $fb = false ){
	$message = '';

	if( $qt_lang['lang'] == 'es' ){
		if( $item_type == 'badge' ){
			$message .= "Gan&eacute; el distintivo $item_title";
		}elseif( $item_type == 'persona' ){
			$message .= "Mi Evaluac&iacute;on de Rock 'n' Roll fue $item_title";
		}
		$message .= !$fb ? ". &iexcl;Checa mis estad&iacute;sticas! $hashtag" : " en ";
	}else{
		if( $item_type == 'badge' ){
			$message .= "I earned the $item_title badge.";
		}elseif( $item_type == 'persona' ){
			$message .= "My Rock 'n' Roll Assessment was $item_title";
		}
		$message .= !$fb ? ". Check out my stats! $hashtag" : " at ";
	}

	return $message;
}

/**
 * function: shareMessage
 * Translating function
 * returns string to print/manipulate
 */
function viewItemMessage( $qt_lang, $item_type, $runner_name ){
	$qt_status		= $qt_lang['enabled'] == 1 ? true : false;
	$qt_cur_lang	= $qt_lang['lang'];

	$item					= ($item_type == 'badge') ? 'Badges' : 'Rock \'n\' Roll Assessment';
	$message			= "See $runner_name's $item";

	if( $qt_status ){
		if( $qt_cur_lang == 'es' ){
			$item			= ($item_type == 'badge') ? 'Distintivos de' : 'Evaluac&iacute;on de Rock \'n\' Roll de';
			$message	= "Ver $item $runner_name";
		}
	}
	return $message;
}

/**
 * function: get_mexico_races
 */
function get_mexico_races(){
	$mexico_array = array( 'mexico-city', 'merida', 'queretaro');
	return $mexico_array;
}

/**
 * function: getGDropdownList
 */
function getGDropdownList() {
	$dbConnLink = dbConnect( 'results' );
	$distinct = array( );
	$distinct['gender']	 = getGDistinctList( $dbConnLink, "SEX");
	$distinct['state']		= getGDistinctList( $dbConnLink, "STATE");
	dbClose( $dbConnLink );
	return $distinct;
}

/**
 * function: getGDistinctList
 */
function getGDistinctList( $dbLinkConnection, $distinct_column ) {
	$table_name = "wp_fz_runners";
	$distinctStmt = 'SELECT DISTINCT '.$distinct_column.' FROM '.RESULTSDBNAME.'.'.$table_name.' WHERE 1 ORDER BY '.$distinct_column;
	$result1 = mysqli_query( $dbLinkConnection, $distinctStmt );
	$distinct_ids = array( );
	$ri = 0;
	while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
		$distinct_ids[$ri] = $row1[$distinct_column];
		$ri++;
	}
	return $distinct_ids;
}

/**
 * function: doGSearchResults
 */
function doGSearchResults( $searchParameters ) {
	$dbConnLink = dbConnect( 'results' );
	$search1 = getGSearchResults( $dbConnLink, $searchParameters );
	dbClose( $dbConnLink );
	return $search1;
}


/**
 * function: getGSearchResults
 */
function getGSearchResults( $dbLinkConnection, $searchParameters ) {

	/*Initial Search maybe not include any results and just find the person?*/
	/*$searchStmt = "SELECT r.FIRSTNAME, r.LASTNAME, r.SEX, r.CITY, r.STATE, s.fk_customer_id, s.fk_event_id, s.date_start, s.FINISHTIME,s.BIB, s.RACEID, s.race_table_id, s.race_types_id FROM rnr_results.wp_fz_runners r, rnr_results.wp_fz_runner_races s WHERE 1";*/
	$searchStmt = "SELECT r.* FROM rnr_results.wp_fz_runners r WHERE 1";
	if( !empty( $searchParameters['bib'] ) ) {
		$searchStmt .= " AND r.EMAIL = ".$searchParameters['email'];
	}
	if( !empty( $searchParameters['firstname'] ) ) {
		$searchStmt .= ' AND r.FIRSTNAME = "'.$searchParameters['firstname'].'"';
	}
	if( !empty( $searchParameters['lastname'] ) ) {
		$searchStmt .= ' AND r.LASTNAME = "'.$searchParameters['lastname'].'"';
	}
	if( !empty( $searchParameters['gender'] ) ) {
		$searchStmt .= ' AND r.SEX = "'.$searchParameters['gender'].'"';
	}
	if( !empty( $searchParameters['city'] ) ) {
		$searchStmt .= ' AND r.CITY LIKE "'.$searchParameters['city'].'%"';
	}
	if( !empty( $searchParameters['state'] ) ) {
		$searchStmt .= ' AND r.STATE = "'.$searchParameters['state'].'"';
	}
	/*$searchStmt .= " AND s.fk_customer_id = r.pk_customer_id";*/
	$searchStmt .= " ORDER BY r.LASTNAME, r.FIRSTNAME";
	/*$searchStmt .= ", s.date_start";*/

	if( !empty( $searchParameters['perpage'] ) ) {
		$searchStmt .= ' LIMIT ' . $searchParameters['perpage'];
	}
	if( !empty( $searchParameters['resultspage'] ) ) {
		$resultspage = $searchParameters['resultspage'];
		$totalrunners = getGTotalRunners( $searchParameters );
		$totalpages = $totalrunners[0] / $searchParameters['perpage'];

		if( $totalrunners[0] != 0 ) {
			if( $resultspage > ceil( $totalpages ) ) {
				$offset = ( $totalpages * $searchParameters['perpage'] ) - $searchParameters['perpage'];
			} else {
				$offset = ( ( $resultspage - 1 ) * $searchParameters['perpage'] );
			}
			$searchStmt .= ' OFFSET ' . $offset;
		}
	}

	//echo $searchStmt;
	$result1 = mysqli_query( $dbLinkConnection, $searchStmt );
	$runnerStats = array( );
	while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
		$runnerStats[] = $row1;
	}
	return $runnerStats;
}

/**
 * function: getGTotalRunners
 */
function getGTotalRunners( $criteria ) {
	$dbConnLink = dbConnect( 'results' );

	/*Initial Search maybe not include any results and just find the person?*/
	/*$total_runners_query = "SELECT COUNT(*) FROM rnr_results.wp_fz_runners r, rnr_results.wp_fz_runner_races s WHERE 1";*/
	$total_runners_query = "SELECT COUNT(*) FROM rnr_results.wp_fz_runners r WHERE 1";

	if( !empty( $criteria['bib'] ) ) {
		$total_runners_query .= " AND r.EMAIL = ".$criteria['email'];
	}
	if( !empty( $criteria['firstname'] ) ) {
		$total_runners_query .= ' AND r.FIRSTNAME = "'.$criteria['firstname'].'"';
	}
	if( !empty( $criteria['lastname'] ) ) {
		$total_runners_query .= ' AND r.LASTNAME = "'.$criteria['lastname'].'"';
	}
	if( !empty( $criteria['gender'] ) ) {
		$total_runners_query .= ' AND r.SEX = "'.$criteria['gender'].'"';
	}
	if( !empty( $criteria['city'] ) ) {
		$total_runners_query .= ' AND r.CITY LIKE "'.$criteria['city'].'%"';
	}
	if( !empty( $criteria['state'] ) ) {
		$total_runners_query .= ' AND r.STATE = "'.$criteria['state'].'"';
	}
	/*$total_runners_query .= " AND s.fk_customer_id = r.pk_customer_id";*/

	if( $total_runners_result = mysqli_query( $dbConnLink, $total_runners_query ) ) {
		$total_runners = mysqli_fetch_array( $total_runners_result, MYSQLI_NUM );
	}
	$total_runners_result->close();

	dbClose( $dbConnLink );
	return $total_runners;

}

function getRaceTypes( ){

	global $wpdb;
	//$query = 'SELECT * FROM wp_fz_sub_event WHERE fk_event_id = '.$event_id;
	$query = 'SELECT pk_race_type_id,distance FROM wp_fz_race_types WHERE 1';
	$raceTypes = $wpdb->get_results( $query, ARRAY_A);
	if ( count($raceTypes) == 0 ) {
		$wpdb->print_error();
		return null;
	} else {
		//rearrange subevents by raceid
		$raceTypeInfo = array();
		foreach($raceTypes as $keySE => $subEI){
			$raceTypeInfo[$subEI['pk_race_type_id']] = $subEI['distance'];
		}
		return $raceTypeInfo;
	}
}

/**
 * function: doGIndividualInfo
 */
function doGIndividualInfo( $global_customer_id) {
	$dbConnLink = dbConnect( 'results' );
	$search1 = getGIndividualInfo( $dbConnLink, $global_customer_id);
	dbClose( $dbConnLink );
	return $search1;
}

/**
 * function: getGIndividualInfo
 */
function getGIndividualInfo( $dbLinkConnection, $global_customer_id) {
	$searchStmt = "SELECT * FROM rnr_results.wp_fz_runners WHERE 1";
	$searchStmt .= " AND pk_customer_id = ".$global_customer_id;

	//echo $searchStmt;
	$result1 = mysqli_query( $dbLinkConnection, $searchStmt );
	$runnerStats = array( );
	while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
		$runnerStats[] = $row1;
	}
	return $runnerStats;
}

/**
 * function: doGIndividualResults
 */
function doGIndividualResults( $global_customer_id) {
	$dbConnLink = dbConnect( 'results' );
	$search1 = getGIndividualResults( $dbConnLink, $global_customer_id);
	dbClose( $dbConnLink );
	return $search1;
}

/**
 * function: getGIndividualResults
 */
function getGIndividualResults( $dbLinkConnection, $global_customer_id) {
	$searchStmt = "SELECT * FROM rnr_results.wp_fz_runner_races WHERE 1";
	$searchStmt .= " AND fk_customer_id = ".$global_customer_id." ORDER BY race_types_id, date_start DESC";
	//echo $searchStmt;
	$result1 = mysqli_query( $dbLinkConnection, $searchStmt );
	$runnerStats = array( );
	while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
		$runnerStats[] = $row1;
	}
	return $runnerStats;
}

/**
 * function: doGIndividualEvents
 */
function doGIndividualEvents( $global_customer_id) {
	$dbConnLink = dbConnect( 'results' );
	$search1 = getGIndividualEvents( $dbConnLink, $global_customer_id);
	dbClose( $dbConnLink );
	return $search1;
}

/**
 * function: getGIndividualEvents
 */
function getGIndividualEvents( $dbLinkConnection, $global_customer_id) {
	$searchStmt = "SELECT fk_event_id FROM rnr_results.wp_fz_runner_races WHERE 1";
	$searchStmt .= " AND fk_customer_id = ".$global_customer_id;

	echo $searchStmt;
	$result1 = mysqli_query( $dbLinkConnection, $searchStmt );
	$runnerStats = array( );
	while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
		$runnerStats[] = $row1;
	}
	return $runnerStats;
}

/*SELECT COUNT(pk_registration_id) as eTotal, fk_customer_id, FIRSTNAME, LASTNAME FROM rnr_results.wp_fz_runner_races, rnr_results.wp_fz_runners WHERE fk_customer_id <> '13339' AND pk_customer_id = fk_customer_id  group by fk_customer_id ORDER BY eTotal DESC LIMIT 25;*/
?>
