<?php
  /* Template Name: Volunteer */
  get_header();

  $parent_slug = the_parent_slug();
  rnr3_get_secondary_nav( $parent_slug );
  $market = get_market2();

  // meta boxes!
  $prefix                 = '_rnr3_';

  // titles
  $overview['title']  = 'Overview';
  $overview['id']     = 'overview';
  $racecrew['title']  = 'Race Crew';
  $racecrew['id']     = 'race_crew';
  $medical['title']   = 'Medical';
  $medical['id']      = 'medical';
  $faqs['title']      = 'FAQs';
  $faqs['id']         = 'faqs';
  $contact['title']   = 'Contact Us';
  $contact['id']      = 'contact';
  $ty_page['title']   = 'Thank You';
  $ty_page['id']      = 'ty';
  $perks['title']     = 'Benefits';
  $perks['id']        = 'perks';
  $pos['title']       = 'Positions Guide';
  $pos['id']          = 'pos';
  $rights['title']    = 'Rights & Responsibilites';
  $rights['id']       = 'rights';
  $id = get_the_id();


  // contents
  //check to see if the race is over and show either thank you or the other sections
  $is_ty                    = get_post_meta( $id, $prefix . 'is_over', true ) == 'on' ? true : false;

  if( $is_ty ){
    //display thank you if event is over
    $overview['content'] = $racecrew['content'] = $medical['content'] = $perks['content'] = '';
    $ty_page['content']      = apply_filters( 'the_content', get_post_meta( $id, $prefix . 'volunteer_ty', true) );

  } else {
    //display these sections if event is not over
    $overview['content']      = apply_filters( 'the_content', get_post_meta( $id, $prefix . 'overview', true) );
    $racecrew['content']      = apply_filters( 'the_content', get_post_meta( $id, $prefix . 'racecrew', true ) );
    $medical['content']       = apply_filters( 'the_content', get_post_meta( $id, $prefix . 'medical', true ) );
    $perks['content']         = apply_filters( 'the_content', get_post_meta( $id, $prefix . 'perks', true ) );
    $ty_page['content']       = '';
  }

  if(strtolower($market) == 'liverpool'){
  $faqs['content']      = '
    <div class="accordion">
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_1">What happens after I sign up?</div>
        <div id="faq_1" class="accordion-section-content">
          <p>You will receive a confirmation e-mail from the Volunteer Coordinator. Some further information exchange may be needed, to ensure that we understand your requirements fully.  This is where providing your (mobile) phone number is important, as sometimes it\'s easier to get finalise details by phone. Nearer to race day, you will get all the info about your tasks, joining instructions etc.</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_2">May I bring a friend?</div>
        <div id="faq_2" class="accordion-section-content">
          <p>Yes you can.  There are many occasions when friends sign up together, and wish to work in the same area.  When you sign up, please include your friend\'s information, or encourage them to sign up as well.   You will receive joint e-mails about your involvement.</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_3">What if I cannot attend?</div>
        <div id="faq_3" class="accordion-section-content">
          <p>Please contact the Volunteer Coordinator as soon as possible by e-mail or phone.  There are many reasons why volunteers find they cannot attend. The most important thing is to tell us as soon as possible, as we can then put contingencies in place. We always appreciate that you volunteered in the first place, even though circumstances may dictate otherwise.</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_4">What if I cannot stay for the entire shift?</div>
        <div id="faq_4" class="accordion-section-content">
          <p>Once you sign up, we are counting on you to stay for the full shift.  Sometimes the shift may seem over-staffed and slow at the beginning, but often we are preparing for a rush.  We count on each volunteer to stay until the end of the shift.</p>
          <p>However, we realise there could be an emergency, and if this occurs, we would appreciate if you contact the volunteer coordinator by phone or text before leaving.</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_5">Can I split the shift with a friend?</div>
        <div id="faq_5" class="accordion-section-content">
          <p>No, sorry.  Shift times are predetermined because it is difficult to arrive or leave in the middle of a shift.  Volunteers must remain for the full length of the shift: we cannot replace you if you leave. However, some shift times can be flexible - for example at a water station, when the rush eases off, it may be possible to leave early. </p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_6">What do I do if I do not receive my Race Crew confirmation e-mail or phone-call?</div>
        <div id="faq_6" class="accordion-section-content">
          <p>Final confirmation e-mails are sent 7-14 days prior to race day.  If it is less than a week to the race, and you still don\'t know all the information you need, please e-mail the volunteer coordinator with your query.  At the worst case, on race day, come to the volunteer check-in at the Echo Arena - there will always be tasks to help with.</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_7">Are meals provided?</div>
        <div id="faq_7" class="accordion-section-content">
          <p>Volunteers near the Start/Finish area will be provided with food vouchers.  Other volunteers on the course will be provided with a snack and bottled water.  However, bringing your own snack and drink is also highly recommended.</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_8">How should I dress?</div>
        <div id="faq_8" class="accordion-section-content">
          <p>The race is in late May,  so the weather can still be quite cold, or it can be raining, or very hot!  So you should bring layers and waterproofs if required.  If you are on an outside shift, a check of the weather forecast beforehand is adviseable.  Remember, that standing outside for a long time is normally unusual, so  be prepared for any cooling effect of wind etc. Please wear shoes that you are comfortable with for standing for long duration . You will receive a Race Crew T-shirt that you should wear, that identifies you as a part of the official Rock \'n\' Roll Race Crew.</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_9">What should I bring with me?</div>
        <div id="faq_9" class="accordion-section-content">
          <p>As little as possible.  There will be no storage for personal items on-site.  Almost all areas will have a corner where items can be left, but it will not be secure.  So we highly recommend bringing as few personal items as possible.  We cannot  be responsible for lost or stolen items.</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_10">Can children volunteer?</div>
        <div id="faq_10" class="accordion-section-content">
          <p>Yes, but there are restrictions.  Normally, children will be part of an organised group, such as the Scouts, who will have their own supervision.  Other children must be supervised by an adult volunteer that he/she knows, such as a relative.</p>
          <p>The tasks performed will be with the group that includes the supervising adult in close proximity, such as at a water station.</p>
          <p>The minimum age for someone to work on their own, or as a marshal on the race route, is 17years.</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_11">Do volunteers receive a discount on race entry fees?</div>
        <div id="faq_11" class="accordion-section-content">
          <p>Sorry, no.</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_12">What if I\'m unable to stand for long periods of time?</div>
        <div id="faq_12" class="accordion-section-content">
          <p>Please provide the Volunteer Coordinator if you have any special needs.  We will do our best to accommodate you.</p>
        </div>
      </div>

    </div>

  ';
  }else{
  $faqs['content']      = '
    <div class="accordion">
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_1">What happens after I sign up online?</div>
        <div id="faq_1" class="accordion-section-content">
          <p>After you register, you will immediately receive an automated email confirmation. One to Two weeks prior to the event, you will receive confirmation materials via mail with specific instructions about your volunteer shift(s). All volunteers will also receive emailed shift reminders during race week.</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_2">May I bring a friend?</div>
        <div id="faq_2" class="accordion-section-content">
          <p>Yes! On certain occasions shifts do fill up, which will be noted online. Please encourage friends to sign up online to ensure their spot. This also lets us know how many volunteers will be helping out. We can always use extra help!</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_3">What if I cannot attend?</div>
        <div id="faq_3" class="accordion-section-content">
          <p>Please contact our Volunteer Coordinator as soon as possible. Send an email (preferred) to the email address specified on the Volunteer website with your full name, event city, and volunteer shifts you need to cancel. If you do not have access to email, please call 1-800-311-1255.</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_4">What if I cannot stay for the entire shift?</div>
        <div id="faq_4" class="accordion-section-content">
          <p>Once you sign up, we are counting on you to stay for the full shift. We cannot replace you if you leave early. Sometimes the shift may seem over-staffed and slow at the beginning, but often we are preparing for a rush. We count on each volunteer to stay until the end of the shift.</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_5">Can I split the shift with a friend?</div>
        <div id="faq_5" class="accordion-section-content">
          <p>No, sorry. Shift times are pre-determined because it is difficult to arrive or leave in the middle of a shift. It is crucial that volunteers remain for the full length of the shift; we cannot replace you if you leave. Please support your fellow volunteers by staying for your entire assignment. Volunteer with your friend instead!</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_6">What do I do if I did not receive my Race Crew confirmation letter?</div>
        <div id="faq_6" class="accordion-section-content">
          <p>Confirmation letters are mailed 7-14 days prior to race day. If it is less than a week to the race and you still have not received your volunteer packet, contact the Volunteer Coordinator or come to the Health & Fitness Expo (see website for times and location). The Volunteer Check In booth has extra parking passes for the expo, start and finish lines, if needed.</p>
        </div>
      </div>
      <p><strong>**NOTE:</strong> Water station volunteers must contact their team captain for parking passes and instructions.</p>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_7">Are meals provided?</div>
        <div id="faq_7" class="accordion-section-content">
          <p>Meals are provided for the long or \'All Day\' shifts. All volunteer shifts will include snacks and bottled water. Bringing snacks, a sack lunch, or a reusable water bottle, is highly recommended.</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_8">How should I dress?</div>
        <div id="faq_8" class="accordion-section-content">
          <p>Wear comfortable, close-toed shoes. Bring layers of comfortable clothing for cool to hot temperatures, for both indoor and outdoor shifts. You will receive a Race Crew T-shirt that identifies you as a part of the Official Rock \'n\' Roll Race Crew.</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_9">What should I bring with me?</div>
        <div id="faq_9" class="accordion-section-content">
          <p>As little as possible! There is no storage for personal items on-site. You will receive a Race Crew drawstring backpack upon checking in, which you can use to store small items. Almost all areas will also have a corner where everyone can set their coat or lunch, but it will not be secure. We highly recommend bringing as few personal items as possible. We are not responsible for lost or stolen items.</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_10">Can children volunteer?</div>
        <div id="faq_10" class="accordion-section-content">
          <p>The official minimum age for an unaccompanied child to volunteer is 14 years of age. With supervision, adults may bring slightly younger children. We are concerned about safety and the ability to stay focused on a task for the full shift time. We have many positions that are good for children as long as the ratio of adults-to-children is adequate. The children must be old enough to participate for the entire shift. Please direct any questions on this issue to Volunteer Coordinator.</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_11">Do volunteers receive a discount on race entry fees?</div>
        <div id="faq_11" class="accordion-section-content">
          <p>Sorry, for most markets volunteers will not receive discounts on race entry fees.</p>
        </div>
      </div>
      <div class="accordion-section" style="position: relative;">
        <div class="accordion-section-title" data-reveal="#faq_12">What if I am unable to stand for long periods of time?</div>
        <div id="faq_12" class="accordion-section-content">
          <p>The Registration / Participant Check In area at the Health & Fitness Expo provides seated positions. There are very limited seated positions at the start and finish lines; please plan accordingly and alert the Volunteer Coordinator ahead of time if you have any special needs. We will do our best to accommodate you!</p>
        </div>
      </div>

    </div>

  ';
  }

  //display before and after event
  $contact['content']   = apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'contact', true ) );
  $resp['content']      = apply_filters( 'the_content', get_post_meta( $id, $prefix . 'responsibilties', true ) );
  $rights['content']    = apply_filters( 'the_content', get_post_meta( $id, $prefix . 'rights', true ) );
  $pos['content']       = apply_filters( 'the_content', get_post_meta( $id, $prefix . 'positions', true ) );

  $volunteer_array = array(
    $ty_page,
    $overview,
    $racecrew,
    $medical,
    $perks,
    $contact,
    $faqs,
    $pos,
    $rights
  );
   $tabs_array = ['contact', 'faqs', 'info', 'rights', 'pos'];

?>

  <!-- main content -->
  <main role="main" id="main">
    <div id="nav-anchor"></div>
    <section class="wrapper grid_2 offset240left">
      <div class="column sidenav stickem">
        <div id="nav-anchor"></div>
        <nav class="sticky-nav">
          <h2>Volunteer</h2>
          <ul>
            <?php
              foreach( $volunteer_array as $key => $value ) {
                if( $value['content'] != '' ) {
                  if( in_array( $value['id'], $tabs_array ) ) {
                    echo '<li><a href="#vol-info" class="' . $value['id'] . '">'.$value['title'].'</a></li>';

                  } else{

                    echo '<li><a href="#'.$value['id'].'">'.$value['title'].'</a></li>';
                  }
                  
                }
              }
            ?>
          </ul>

        </nav>
      </div>

      <div class="column">
        <div class="content">
          <?php 
         
          foreach( $volunteer_array as $key => $value ) {
            //if not in the tabs section, spit it out
           if( !in_array($value['id'], $tabs_array) ){
              if( $value['content'] != '' ) {
                echo '<section id="'.$value['id'].'">
                  <h2>'.$value['title'].'</h2>
                  '. $value['content'] .
                '</section>        
          ';
              }
            } 
          }
          //TABS SECTION
          echo '<section id="vol-info">
                  <section class="wrapper group">
                    <div id="volunteer-tabs" class="tp-tabs">
                      <ul>
                        <li><a href="#contact">Contact Us</a></li>
                        <li><a href="#faqs">FAQs</a></li>
                        <li><a href="#pos">Positions Guide</a></li>
                        <li><a href="#rights">Rights & Responsibilites</a></li>

                      </ul>
                      <div id="contact">' . 
                        $contact['content'] . '
                      </div>
                      <div id="faqs">' . 
                        $faqs['content'] . '
                      </div>
                      <div id="pos">
                        '. $pos['content'].'
                        
                      </div>
                      <div id="rights">
                      '. $rights['content']. $resp['content'] . '
                        
                      </div>
                    </div>
                  </section>
                </section>
                  </div>
          </section>
        </div>';

          ?>

    <script>
      jQuery( function( $ ) {
        $( '#volunteer-tabs' ).tabs();
      } );


      $(document).ready(function(){


        /**
         * This part causes smooth scrolling using scrollto.js
         * We target all a tags inside the nav, and apply the scrollto.js to it.
         */
        $(".sticky-nav a, .backtotop").click(function(evn){
            evn.preventDefault();
            $('html,body').scrollTo(this.hash, this.hash);
        });

        $(document).on("scroll", onScroll);
            
        //smoothscroll
        $('.sticky-nav a[href^="#"]').on('click', function (e) {
          e.preventDefault();

          $('.sticky-nav a').each(function () {
            $(this).removeClass('nav-active');
            });
            $(this).addClass('nav-active');
            

            $('html,body').scrollTo(this.hash, this.hash);
            });

        function onScroll(event){

            var scrollPos = $(document).scrollTop();

            $('.sticky-nav a').each(function () {
                var currLink = $(this);
                var refElement = $(currLink.attr("href"));
                var tabClasses = $(this).attr("class");
                
                if (refElement.position().top  <= scrollPos && refElement.position().top + refElement.height()  > scrollPos) {

                    $('.sticky-nav ul li a').removeClass("nav-active");
                    currLink.addClass("nav-active");
                    if( ( tabClasses = 'contact' ) || ( tabClasses = 'rights') || ( tabClasses = 'faqs' ) || (tabClasses = 'pos') ) {
                        if( $('[aria-controls="contact"]').hasClass('ui-state-active') ){
                          $('.contact').addClass("nav-active");
                          $('.rights').removeClass('nav-active');
                          $('.faqs').removeClass("nav-active");
                          $('.pos').removeClass("nav-active");
                        } else if( $('[aria-controls="rights"]').hasClass('ui-state-active') ) {
                          $('.rights').addClass("nav-active");
                          $('.contact').removeClass("nav-active");
                          $('.faqs').removeClass("nav-active");
                          $('.pos').removeClass("nav-active");
                        }else if( $('[aria-controls="pos"]').hasClass('ui-state-active') ) {
                          $('.pos').addClass("nav-active");
                          $('.contact').removeClass("nav-active");
                          $('.faqs').removeClass("nav-active");
                          $('.rights').removeClass("nav-active");
                        } else {
                          $('.faqs').addClass("nav-active");
                          $('.contact').removeClass("nav-active");
                          $('.rights').removeClass('nav-active');
                          $('.pos').removeClass('nav-active');
                        }
                    } 
                  } else{
                   // alert('all else');
                    currLink.removeClass("nav-active");
                }
            });
          }

          //trigger the tabs to align with the menus
          $('.contact').on('click', function(){
              if( $('[aria-controls="contact"]').hasClass('ui-state-active') ){
               
              } else {
              $('#volunteer-tabs li').each(function(){
                $(this).removeClass('ui-state-active ui-tabs-active');
              });
              $('[aria-controls="contact"] a').trigger('click');
            }

          });

        });
        $('.rights').on('click', function(){
          if( $('[aria-controls="rights"]').hasClass('ui-state-active')){
                
          } else {
          $('#volunteer-tabs li').each(function(){
              $(this).removeClass('ui-state-active ui-tabs-active');
            });
            $('[aria-controls="rights"] a').trigger('click');
          }  
        });
        $('.faqs').on('click', function(){
           if( $('[aria-controls="faqs"]').hasClass('ui-state-active')){
                
          } else {
            $('#volunteer-tabs li').each(function(){
              $(this).removeClass('ui-state-active ui-tabs-active');
            });
            $('[aria-controls="faqs"] a').trigger('click');
          }
        });
        $('.pos').on('click', function(){
           if( $('[aria-controls="pos"]').hasClass('ui-state-active')){
                
          } else {
            $('#volunteer-tabs li').each(function(){
              $(this).removeClass('ui-state-active ui-tabs-active');
            });
            $('[aria-controls="pos"] a').trigger('click');
          }
        });
    </script>
  </main>

<?php get_footer(); ?>
