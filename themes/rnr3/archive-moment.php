<?php
  /**
   * archive page for the moment custom post type
   */
  get_header('series-home');
?>

  <!-- main content -->
  <main role="main" id="main">
    <section class="wrapper grid_2 offset240left">

      <?php get_sidebar(); ?>
      
      <div class="column">
        <div class="content">
          <?php if (have_posts()) : ?>

            <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
            <?php /* If this is a category archive */ if (is_category()) { ?>
              <h2 class="pagetitle">Archive for the '<?php single_cat_title(); ?>' Category</h2>
            <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
              <h2 class="pagetitle">Posts Tagged '<?php single_tag_title(); ?>'</h2>
            <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
              <h2 class="pagetitle">Archive for <?php the_time('F jS, Y'); ?></h2>
            <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
              <h2 class="pagetitle">Archive for <?php the_time('F, Y'); ?></h2>
            <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
              <h2 class="pagetitle">Archive for <?php the_time('Y'); ?></h2>
            <?php /* If this is an author archive */ } elseif (is_author()) { ?>
              <h2 class="pagetitle">Author Archive</h2>
            <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
              <h2 class="pagetitle">Blog Archives</h2>
            <?php } ?>


          <?php while (have_posts()) : the_post(); ?>
            <article <?php post_class() ?>>
              <?php $image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'news-promos-thumbs');
              
              if($image_array[0]) { ?>
                <a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><img class="wp-post-image alignleft" src="<?php echo $image_array[0];?>"></a>
              <?php } ?>

              <h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
              
              <p><?php the_excerpt(); ?></p>
              <a href="<?php the_permalink(); ?>" class="read-more-link">Read Full Story</a>

            </article>

          <?php endwhile; ?>

            <div class="navigation">
              <div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
              <div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
            </div>

          <?php else :

            if ( is_category() ) { // If this is a category archive
              printf("<h2 class='center'>Sorry, but there aren't any posts in the %s category yet.</h2>", single_cat_title('',false));
            } else if ( is_date() ) { // If this is a date archive
              echo("<h2>Sorry, but there aren't any posts with this date.</h2>");
            } else if ( is_author() ) { // If this is a category archive
              $userdata = get_userdatabylogin(get_query_var('author_name'));
              printf("<h2 class='center'>Sorry, but there aren't any posts by %s yet.</h2>", $userdata->display_name);
            } else {
              echo("<h2 class='center'>No posts found.</h2>");
            }

          endif; ?>
        </div>
      </div>
    </section>
  </main>

<?php if(get_current_blog_id() <= 1){
  get_footer('series');
} else {
  get_footer();
} ?>
