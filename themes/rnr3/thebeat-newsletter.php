<?php 
  /* Template Name: The Beat Newsletter (Series) */
  
  $logo = get_bloginfo('stylesheet_directory') . '/img/newsletter/rnr-logo.png';
  $logobeat = get_bloginfo('stylesheet_directory') . '/img/newsletter/the-beat.png';
  $facebook = 'RunRocknRoll';
  $twitter = 'runrocknroll';
  $twitter_share = get_bloginfo( 'stylesheet_directory' ).'/img/newsletter/twitter-inverse.png';
  $facebook_share = get_bloginfo( 'stylesheet_directory' ).'/img/newsletter/facebook-inverse.png';
  $brand_color = '#0010a6';
  $campaign_var = 'newsletter_thebeat';
  
  echo '<!DOCTYPE html>
  <html lang="en">
  <head>
    <title>'.get_the_title().'</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
      /*<style>
        body { width:100%!important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0; }
        .ExternalClass {width:100%;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
        span { font-family:Arial, sans-serif; font-size:16px; line-height:20px; color:#555; }
        table, td { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
        img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
        a img {border:none;}
        .image_fix {display:block;}
      
        @media only screen and (max-width: 480px) {
          body,table,td,p,a,li,blockquote{ -webkit-text-size-adjust:none !important; }
          table[class=content_wrap] { width: 94%!important; }
          table[class=full_width] { width: 100%!important; }
          img[class=max-size] { width: 100%!important; height:auto; }
          table[class=hide] { display: none!important; }
          img[class=hide] { display: none!important; }
          td[class=hide] { display: none!important; }
          table[class=show] { display:inline!important; }
          img[class=show] { display:inline!important; }
          td[class=text-center] { text-align: center!important; }
          td[class=text-left] { text-align: left!important; }
          a[class=button] { border-radius:2px; background-color:<?php  // echo $brand_color; ?>; color:#fff!important; padding: 5px; text-align:center; display:block; text-decoration: none; text-transform: uppercase; margin: 0 0 10px 0; }
        }
      </style>*/
      echo '<style>
        body {
          width:100%!important;
          -webkit-text-size-adjust:100%;
          -ms-text-size-adjust:100%;
          margin:0;
          padding:0;
        }
        .ExternalClass {
          width: 100%;
        }
        span {
          font-family:Arial, sans-serif;
          font-size:16px;
          line-height:20px;
          color:#555;
        }
        table, td {
          border-collapse:collapse;
          mso-table-lspace:0pt;
          mso-table-rspace:0pt;
        }
        img{
            border:0;
            height:auto !important;
            line-height:100%;
            outline:none;
            text-decoration:none;
        }
        img#header-logo {
          margin-right: 0px;
        }
        img#sponsor-logo {
          padding: 2px 0px;
          margin: 2px 0px;
        }
        @media only screen and (max-width: 480px) {
          body,table,td,p,a,li,blockquote{
            -webkit-text-size-adjust:none !important;
          }
        }
        @media only screen and (max-width: 480px) {
          table[class=content_wrap] {
            width: 94%!important;
          }
        }
        @media only screen and (max-width: 480px) {
          table[class=full_width] {
            width: 100%!important;
          }
        }
        @media only screen and (max-width: 480px) {
          img[class=max-size] {
            width: 100%!important;
            height:auto;
          }
        }
        @media only screen and (max-width: 480px) {
          table[class=hide] {
            display: none!important;
          }
        }
        @media only screen and (max-width: 480px) {
          img[class=hide] {
            display: none!important;
          }
        }
        @media only screen and (max-width: 480px) {
          td[class=hide] {
            display: none!important;
          }
        }
        @media only screen and (max-width: 480px) {
          table[class=show] {
            display:inline!important;
          }
        }
        @media only screen and (max-width: 480px) {
          img[class=show] {
            display:inline!important;
          }
        }
        @media only screen and (max-width: 480px) {
          td[class=text-center] {
            text-align: center!important;
          }
        }
        @media only screen and (max-width: 480px) {
          td[class=text-left] {
            text-align: left!important;
          }
        }
        @media only screen and (max-width: 480px) {
          a[class=button] {
            border-radius:2px;
            background-color:<?php echo $brand_color; ?>;
            color:#fff!important;
            padding: 5px;
            text-align:center;
            display:block;
            text-decoration: none;
            text-transform: uppercase;
            margin: 0 0 10px 0;
          }
        }
      </style>
    </head>
  ';

  if ( $facebook != '' ) {
    $facebook = '<a href="https://facebook.com/' . $facebook . '" style="text-decoration: none;color: '.$brand_color.';"><img src="' . get_bloginfo( 'stylesheet_directory' ). '/img/newsletter/facebook.png" width="32" height="32" alt="Facebook" /></a>';
  }
  
  if ( $twitter != '' ) {
    $twitter = '<a href="https://twitter.com/' . $twitter . '" style="text-decoration: none; color: '.$brand_color.';"><img src="' . get_bloginfo( 'stylesheet_directory' ). '/img/newsletter/twitter.png" width="32" height="32" alt="Twitter" /></a>';
  }
  
  // this array requires 7 post ids, a query will run for
  // each to populate the newsletter
  $newsletter_posts = '';
  if ( get_post_meta( get_the_ID(), '_rnr3_beatids', true ) != '' ) {
    $newsletter_posts = explode( ',' , get_post_meta( get_the_ID(), '_rnr3_beatids', true ) );
  }
  
  // Get 7 Stories in order which gives editors control over the main stories in Newsletter
  $args = array(
    'post__in' => $newsletter_posts,
    'post_type' => 'thebeat'
  );
  $count = 0;

  $beatnl_query = new WP_Query( $args );

  if ($beatnl_query->have_posts()) :
    while ($beatnl_query->have_posts()) :
      $beatnl_query->the_post();
      $title_link = '<a href="'. wp_get_shortlink() .'" title="Read full story" style="font-family: Arial, sans-serif; font-size:21px; line-height:24px; color:#e52122; margin:0; font-weight:bold; text-decoration:none;">' .  get_the_title() . '</a>';

      $sub_title_link = '<a href="'. get_permalink() .'" title="Read full story" style="font-family: Arial, sans-serif; font-size:18px; line-height:22px; color:#e52122; margin:0; font-weight:bold; text-decoration:none;">' .  get_the_title() . '</a>';
      
      $excerpt = get_the_excerpt();
      $posttag_name = "Get the full story";

      if ( $count == 0 ) {
        $image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'newsletter-splash');
        $image_link = '<a href="'. get_permalink() .'" title="Read full story" style="text-decoration: none;"><img src="' . $image_array[0] . '" class="max-size" width="620" height="400" /></a>';

        $stories = '
                <!-- content '.$count.' -->
                <tr>
                  <td width="100%" class="text-center" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">' . $image_link . '</td>
                </tr>
                <tr>
                  <td width="100%" bgcolor="#eaeaea" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" valign="top">
                    <table width="100%" align="center" cellpadding="15" cellspacing="0" border="0" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                      <tr>
                        <td width="100%" style="font-family:Arial, sans-serif; font-size:15px; line-height:20px; color:#555; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                          ' . $title_link . '<br><p>' . $excerpt . '</p>
                          <div style="background-color: #ee1c24; font-size:13px; display: inline-block; padding: 10px; text-transform: uppercase;"><a href="'.get_permalink().'" style="color:#ffffff; font-weight: bold; font-family: Arial, sans-serif; line-height:19px; text-decoration: none;">'.$posttag_name.'</a></div>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td width="100%" height="20" bgcolor="#ffffff" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><br /></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <!-- end content '.$count.' -->';
      } elseif ($count < 8) {
      
        $image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'newsletter-image');
        $image_link = '<a href="'. get_permalink() .'" title="Read full story" style="text-decoration: none;"><img src="' . $image_array[0] . '" class="max-size" width="300" height="200" /></a>';
        
        if ($count == 1 || $count == 3 || $count == 5) {
          $stories .= '
            <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="font-family:Arial, sans-serif; font-size:15px; line-height:20px; color:#555;">
              <tr>
                <td width="100%" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                  <table width="620" cellpadding="0" cellspacing="0" border="0" align="center" class="content_wrap" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                    <tr>
                      <td width="100%" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                        <!-- content '.$count.' -->
                        <table class="full_width" width="300" align="left" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                          <tr>
                            <td width="100%" class="text-center" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">'.$image_link.'</td>
                          </tr>
                          <tr>
                            <td bgcolor="#eaeaea" width="100%" valign="top" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                              <table width="300" cellpadding="15" cellspacing="0" border="0" class="full_width" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                                <tr>
                                  <td width="100%" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-family:Arial, sans-serif; font-size:15px; line-height:20px; color:#555;">'.$sub_title_link.'<br /><br />
                                    <div style="background-color: #ee1c24; font-size:13px; display: inline-block; padding: 10px; text-transform: uppercase;"><a href="'.get_permalink().'" style="color:#ffffff; font-weight: bold; font-family: Arial, sans-serif; line-height:19px; text-decoration: none;">'.$posttag_name.'</a></div>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td width="100%" height="20" bgcolor="#ffffff" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><br /></td>
                          </tr>
                        </table>
                        <!-- end content '.$count.' -->
          ';
        } elseif ($count == 2 || $count == 4 || $count == 6) {
          $stories .= '
                    <!-- content '.$count.' -->
                    <table class="full_width" width="300" align="right" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                      <tr>
                        <td width="100%" class="text-center" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                          '.$image_link.'
                          </td>
                        </tr>
                        <tr>
                        <td bgcolor="#eaeaea" width="100%" valign="top" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                          <table width="300" cellpadding="15" cellspacing="0" border="0" class="full_width" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                            <tr>
                              <td width="100%" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-family:Arial, sans-serif; font-size:15px; line-height:20px; color:#555;">'.$sub_title_link.'<br /><br />
                              <div style="background-color: #ee1c24; font-size:13px; display: inline-block; padding: 10px; text-transform: uppercase;"><a href="'.get_permalink().'" style="color:#ffffff; font-weight: bold; font-family: Arial, sans-serif; line-height:19px; text-decoration: none;">'.$posttag_name.'</a></div>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                    <!-- end content '.$count.' -->
                </td>
              </tr>
              <tr>
                <td width="100%" height="20" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><br /></td>
              </tr>
            </table>
          ';
        }

      }
      $count++;
    endwhile; else :
      $stories .= '<tr><td>Sorry, no posts match that category</td></tr>';
    endif;

  $stories .= '</table>';
  wp_reset_postdata(); ?> 

  <body bgcolor="#ffffff" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="-webkit-font-smoothing:antialiased; width:100% !important; background-color:#ffffff; background-image:none; background-repeat:repeat; background-position:top left; background-attachment:scroll; -webkit-text-size-adjust:none;">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="font-family:Arial, sans-serif; font-size:15px; line-height:20px; color:#555;">
      <tr>
        <td width="100%" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
          <!--Head banner-->
          <!--Content wrapper-->
          <table width="620" cellpadding="0" cellspacing="0" border="0" align="center" class="content_wrap" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
            <tr>
              <td width="100%" bgcolor="#2F353E" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                <table align="center" cellpadding="5" cellspacing="0" border="0" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                  <tr>
                    <td class="text-center" bgcolor="#2F353E" width="100%" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size:14px; font-family:Arial,sans-serif;">
                      <a href="[webversionurl]" style="color:#f2f2f2; text-decoration:none; font-weight:bold;">View in browser</a>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td width="100%" height="20" bgcolor="#ffffff" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><br /></td>
            </tr>
            <tr>
              <td width="100%" bgcolor="#ffffff" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                <!-- header -->
                <table align="left" width="165" class="full_width" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                  <tr>
                    <!--Logo-->
                    <td bgcolor="#ffffff" width="104" class="text-center" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                      <a href="<?php echo site_url('/thebeat/'); ?>">
                        <img src="<?php echo $logo; ?>" width="104" height="77" title="<?php echo get_bloginfo('name'); ?> Newsletter" alt="Rock 'n' Roll Marathon Series Logo" border="0" />
                      </a>
                    </td>
                  </tr>
                </table>
                <table align="left" width="290" class="full_width" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                  <tr>
                    <!--Logo-->
                    <td bgcolor="#ffffff" width="290" class="text-center" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" align="center">
                      <a href="<?php echo site_url('/thebeat/'); ?>">
                        <img src="<?php  echo $logobeat; ?>" width="290" height="81" title="<?php  echo get_bloginfo('name'); ?> Newsletter" alt="The Beat Logo" border="0" />
                      </a>
                    </td>
                  </tr>
                </table>
                <table class="full_width" width="100" height="44" cellpadding="0" cellspacing="0" border="0" align="right" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                  <tr>
                    <td colspan="2" height="10"></td>
                  </tr>
                  <tr>
                    <td width="8" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"></td>
                    <td width="32" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                      <?php  echo $twitter; ?>
                    </td>
                    <td width="8" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"></td>
                    <td width="32" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                      <?php  echo $facebook; ?>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="hide" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                  <tr>
                    <td width="100%" height="10" bgcolor="#ffffff" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><br /></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td bgcolor="#ffffff" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="hide" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                  <tr>
                    <!--ad-->
                    <td bgcolor="#ffffff" width="100%" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                      <?php  //echo $leaderboard_ad; ?>
                    </td>
                  </tr>
                </table>  
              </td>
            </tr>
            <!-- spacer -->
            <tr>
              <td width="100%" height="10" bgcolor="#ffffff" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><br /></td>
            </tr>
            <!-- / header -->
            <?php echo $stories; ?>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>