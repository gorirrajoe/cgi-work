<section id="finisher-zone-year">
	<section class="wrapper">
		<h2>Finisher Zone</h2>

		<div class="fz_module fz_dropdown_module">
			<h3>YEAR ONLY - Choose from a <?php echo $yearid; ?> Event</h3>
			<p><?php echo $select_event; ?></p>
			<form action="">
				<select onchange="window.location.href=this.form.URL.options[this.form.URL.selectedIndex].value" name="URL">
					<?php /* hardcoded stuffs */ ?>
					<option value="">Select Rock 'n' Roll Event</option>
					<option value="/rnrresults?eId=10&eiId=249">wp_fz_event->display_title</option>
				</select>
			</form>
		</div>

	</section>
</section>
