<section id="finisher-zone-leaderboard">
	<section class="wrapper">

		<h2><?php echo $leaderboards_txt; ?></h2>

		<div class="fz_event_hdr">

			<h3>EVENT, YEAR, SUBEVENT CHOSEN <?php echo stripslashes( $evInfo->display_title ); ?></h3>

			<p><?php
			if ( $qt_lang['lang'] != 'en' ) {

				$startDateEx = explode( '-',$evInfo->date_start );
				echo $startDateEx[2] .' '. ucfirst( get_month_by_language( $qt_lang['lang'], ltrim( $startDateEx[1], '0' ) ) ) .' '. $startDateEx[0];

				if ( $evInfo->date_start != $evInfo->date_end ) {
					$endDateEx = explode( '-',$evInfo->date_end );
					echo ' &ndash; '. $endDateEx[2] .' '. ucfirst( get_month_by_language( $qt_lang['lang'], ltrim( $endDateEx[1], '0' ) ) ) .' '. $endDateEx[0];
				}

			} else {

				echo date( 'M d, Y', strtotime( $evInfo->date_start ) );

				if ( $evInfo->date_start != $evInfo->date_end ) {
					echo ' &ndash; '. date( 'M d, Y', strtotime( $evInfo->date_end ) );
				}

			}?></p>

		</div>
		<?php
			//Get Top Tens
			allTopTenForEvent_display( $qt_lang, $evInfo->rc_appid, $eventid, $yearid );
		?>
	</section>
</section>

