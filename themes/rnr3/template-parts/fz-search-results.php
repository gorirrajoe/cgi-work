<section id="finisher-zone-search-results">
	<section class="wrapper">
		<div class="fz_event_hdr">
			<h3><?php echo stripslashes($evInfo->display_title);?> &ndash; <?php echo $search_results_txt; ?></h3>
			<p><?php
				//date formatting - display
				if ( $qt_lang['lang'] != 'en' ) {

					$startDateEx	= explode( '-', $evInfo->date_start );
					echo $startDateEx[2] .' '. ucfirst( get_month_by_language( $qt_lang['lang'], ltrim( $startDateEx[1], '0' ) ) ) .' '. $startDateEx[0];

					if ( $evInfo->date_start != $evInfo->date_end ) {
						$endDateEx	= explode( '-', $evInfo->date_end );
						echo ' &ndash; '. $endDateEx[2].' '.ucfirst( get_month_by_language( $qt_lang['lang'], ltrim( $endDateEx[1], '0' ) ) ).' '.$endDateEx[0];
					}

				} else {

					echo date( 'M d, Y', strtotime( $evInfo->date_start ) );

					if ( $evInfo->date_start != $evInfo->date_end ) {
						echo ' &ndash; '. date( 'M d, Y', strtotime( $evInfo->date_end ) );
					}

				}
			?></p>
		</div>

		<table class="fz_rank_table fz_search_results_table">
			<tr>
				<th><?php echo $overall_txt; ?></th>
				<th>Bib</th>
				<th><?php echo $name_txt; ?></th>
				<th><?php echo $time_txt; ?></th>
			</tr>
			<?php
				$search1 = doSearchResults( $evInfo->rc_appid, $eventid, $criteria );

				foreach ( $search1 as $keySearch => $search_value ) {

					if ( is_array( $search_value ) ) {

						$indiv_criteria = array(
							'eventid'		=> $eventid,
							// 'yearid'		=> $yearid,
							'subevent_id'	=> $search_value['RACEID'],
							'bib'			=> trim( $search_value['BIB'] ),
						);

						$searchstring = get_query_string( $indiv_criteria, '', 1 );
						// print_r($searchstring);

						echo '<tr><td>';
							if (
								( empty( $search_value['TEAMNAME'] ) && empty( $search_value['RELAYPLACEOALL'] ) ) ||
								( !empty( $search_value['TEAMNAME'] ) && is_null( $search_value['RELAYPLACEOALL'] ) )
							) {
								echo $search_value['PLACEOALL_CHIP'];
							} else {
								echo $search_value['RELAYPLACEOALL'];
							}
						echo '</td>
						<td>
							'. $search_value['BIB'] .'
						</td><td>
							<a href="'. $redirecturl .'?resultpage=1'. $searchstring .'">'. $search_value['FIRSTNAME'] .' '. $search_value['LASTNAME'] .'</a>
						</td><td>';
							if (
								( empty( $search_value['TEAMNAME'] ) && empty( $search_value['RELAYPLACEOALL'] ) ) ||
								( !empty( $search_value['TEAMNAME'] ) && is_null( $search_value['RELAYPLACEOALL'] ) )
							) {
								echo $search_value['FINISHTIME'];
							} else {
								echo $search_value['TEAMNAME'];
							}
						echo '</td></tr>';
					}
				}
			?>
		</table>

		<div class="fz_navigation">
			<?php
				/* display range of runners over total runners */
				$totalrunners	= getTotalRunners( $evInfo->rc_appid, $eventid, $criteria );
			?>
			<div class="fz_nav_column"><?php echo $display_txt; ?>:
				<?php
					if ( $totalrunners[0] <= $perpage ) {
						echo '1-' . number_format( $totalrunners[0] );
					} else {
						echo number_format( ( ( $resultspage - 1 ) * $perpage ) + 1 ) . '-';
						$lastRecord	= $resultspage * $perpage;

						if ( $lastRecord > $totalrunners[0] ) {
							$lastRecord	= $totalrunners[0];
						}
						echo number_format( $lastRecord );
					}
					echo ' / '. number_format( $totalrunners[0] ) .' '. $runners_txt;
				?>
			</div>

			<div class="fz_nav_column">
				<?php
					/* display prev/next arrows based on resultspage */
					$searchstring	= get_query_string( $criteria, 'resultspage' );
					$totalpages		= ceil( $totalrunners[0] / $perpage );

					if ( $resultspage > $totalpages ) {
						$resultspage	= $totalpages;
					}

					if ( $totalpages > 0 ) {
						echo "<span>$page_txt: $resultspage $of_txt $totalpages</span>";
						//echo '<span>'. $page_txt .': '. $resultspage .' '. $of_txt .' ' . $totalpages .'</span>';

						echo '<ul class="fz_prevnext_links">';
							if ( $resultspage > 1 ) {
								// prev link
								$resultspagePre		= $resultspage - 1;
								echo '<li class="fz_prev"><a href="'. $redirecturl .'?resultspage='. $resultspagePre . $searchstring .'">&laquo; '. $previous_txt .'</a></li>';
							}
							if ( $resultspage < $totalpages ) {
								// next link
								$resultspageNext	= $resultspage + 1;
								echo '<li class="fz_next"><a href="'. $redirecturl .'?resultspage='. $resultspageNext . $searchstring .'">'. $next_txt .' &raquo;</a></li>';
							}
						echo '</ul>';
					}
				?>

			</div>

			<?php $searchstring = get_query_string( $criteria, 'perpage' ); ?>
			<div class="fz_nav_column">
				<?php echo $results_per_page_txt; ?>: <a href="<?php echo $redirecturl . '?perpage=25'. $searchstring; ?>">25</a> | <a href="<?php echo $redirecturl . '?perpage=50'. $searchstring; ?>">50</a> | <a href="<?php echo $redirecturl . '?perpage=100'. $searchstring; ?>">100</a>
			</div>
		</div>

		<p class="center"><a href="<?php echo $redirecturl . '?eventid=' . $eventid; ?>" class="cta"><?php echo $search_again_txt; ?></a></p>

		<?php get_correction_link( $qt_lang ); ?>

	</section>
</section>

