<section id="finisher-zone-search-results">
	<section class="wrapper">

		<table class="fz_rank_table fz_search_results_table">
			<tr>
				<!-- <th>Date</th>
				<th>Distance/Type</th> -->
				<th><?php echo $name_txt; ?></th>
				<th>Location</th>
				<!-- <th><?php echo $time_txt; ?></th> -->
			</tr>
			<?php
				$search1 = doGSearchResults( $criteria );

				//$arrayRaceTypes = getRaceTypes();


				foreach ( $search1 as $keySearch => $search_value ) {

					if ( is_array( $search_value ) ) {
						
						/*$eventid = substr ( $search_value['race_table_id'], strpos ( $search_value['race_table_id'], "_" )+1 );*/
						/*$eventid = $search_value['fk_event_id'];

						$indiv_criteria = array(
							'eventid'		=> $eventid,
							'subevent_id'	=> $search_value['RACEID'],
							'bib'			=> trim( $search_value['BIB'] ),
						);

						$searchstring = get_query_string( $indiv_criteria, '', 1 );*/
						
						$indiv_s_criteria = array(
								'gl_a_id'	=> $search_value['pk_customer_id']
						);
						$individualstring = get_query_string($indiv_s_criteria, '', 1);
						// print_r($searchstring);
						
						echo '<tr>';
						/*echo '<td><a href="'.site_url('/search-and-results/?resultpage=1'. $searchstring).'" target="_blank">';
							if ( $qt_lang['lang'] != 'en' ) {
								$startDateEx = explode( '-',$search_value['date_start'] );
								echo $startDateEx[2] .' '. ucfirst(get_month_by_language( $qt_lang['lang'], ltrim( $startDateEx[1], '0' ) ) ) .' '. $startDateEx[0];
							} else {
								echo date( 'M d, Y', strtotime( $search_value['date_start'] ) );
							}
						echo '</a></td><td>'.$arrayRaceTypes[$search_value['race_types_id']].' KM </td>';*/
						echo '<td><a href="'.$redirecturl.'?resultpage=1'. $individualstring.'" target="_blank">'. $search_value['FIRSTNAME'] .' '. $search_value['LASTNAME'] .'</a></td>';
						echo '<td>'. $search_value['CITY'] .', '. $search_value['STATE'] .' '. $search_value['COUNTRY'] .'</td>';
						/*echo '<td><a href="'.site_url('/search-and-results/?resultpage=1'. $searchstring).'" target="_blank">'.$search_value['FINISHTIME'].'</a></td>';*/
						echo '</tr>';
					}
				}
			?>
		</table>

		<div class="fz_navigation">
			<?php
				/* display range of runners over total runners */
				$totalrunners	= getGTotalRunners( $criteria );
			?>
			<div class="fz_nav_column"><?php echo $display_txt; ?>:
				<?php
					if ( $totalrunners[0] <= $perpage ) {
						echo '1-' . number_format( $totalrunners[0] );
					} else {
						echo number_format( ( ( $resultspage - 1 ) * $perpage ) + 1 ) . '-';
						$lastRecord	= $resultspage * $perpage;

						if ( $lastRecord > $totalrunners[0] ) {
							$lastRecord	= $totalrunners[0];
						}
						echo number_format( $lastRecord );
					}
					echo ' / '. number_format( $totalrunners[0] ) .' '. $runners_txt;
				?>
			</div>

			<div class="fz_nav_column">
				<?php
					/* display prev/next arrows based on resultspage */
					$searchstring	= get_query_string( $criteria, 'resultspage' );
					$totalpages		= ceil( $totalrunners[0] / $perpage );

					if ( $resultspage > $totalpages ) {
						$resultspage	= $totalpages;
					}

					if ( $totalpages > 0 ) {
						echo "<span>$page_txt: $resultspage $of_txt $totalpages</span>";
						//echo '<span>'. $page_txt .': '. $resultspage .' '. $of_txt .' ' . $totalpages .'</span>';

						echo '<ul class="fz_prevnext_links">';
							if ( $resultspage > 1 ) {
								// prev link
								$resultspagePre		= $resultspage - 1;
								echo '<li class="fz_prev"><a href="'. $redirecturl .'?resultspage='. $resultspagePre . $searchstring .'">&laquo; '. $previous_txt .'</a></li>';
							}
							if ( $resultspage < $totalpages ) {
								// next link
								$resultspageNext	= $resultspage + 1;
								echo '<li class="fz_next"><a href="'. $redirecturl .'?resultspage='. $resultspageNext . $searchstring .'">'. $next_txt .' &raquo;</a></li>';
							}
						echo '</ul>';
					}
				?>

			</div>

			<?php $searchstring = get_query_string( $criteria, 'perpage' ); ?>
			<div class="fz_nav_column">
				<?php echo $results_per_page_txt; ?>: <a href="<?php echo $redirecturl . '?perpage=25'. $searchstring; ?>">25</a> | <a href="<?php echo $redirecturl . '?perpage=50'. $searchstring; ?>">50</a> | <a href="<?php echo $redirecturl . '?perpage=100'. $searchstring; ?>">100</a>
			</div>
		</div>

		<p class="center"><a href="<?php echo $redirecturl; ?>" class="cta"><?php echo $search_again_txt; ?></a></p>

		<?php get_correction_link( $qt_lang ); ?>

	</section>
</section>
