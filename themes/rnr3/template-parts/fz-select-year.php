<section id="finisher-zone-event">
	<section class="wrapper">

		<h2>Finisher Zone</h2>

		<div class="fz_module fz_results_grid">
			<h3>EVENT ONLY</h3>
			<p>Choose a year to view the Rock 'n' Roll event.</p>

			<?php /* hardcoded stuffs */ ?>
			<div class="grid_4_special">
				<div class="column_special">
					<div class="inner_column">
						<h3>2015</h3>
						<p><a href="#" class="cta"><?php echo $view_results_txt; ?></a></p>
					</div>
				</div><div class="column_special">
					<div class="inner_column">
						<h3>2014</h3>
						<p><a href="#" class="cta"><?php echo $view_results_txt; ?></a></p>
					</div>
				</div><div class="column_special">
					<div class="inner_column">
						<h3>2013</h3>
						<p><a href="#" class="cta"><?php echo $view_results_txt; ?></a></p>
					</div>
				</div><div class="column_special">
					<div class="inner_column">
						<h3>2012</h3>
						<p><a href="#" class="cta"><?php echo $view_results_txt; ?></a></p>
					</div>
				</div>
			</div>
		</div>

	</section>
</section>
