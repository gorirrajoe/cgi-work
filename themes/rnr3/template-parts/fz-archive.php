<?php
	/**
	 * Template part for the event archives in the Finisher Zone
	 */

	if ( false === ( $archive_results = get_transient( 'archive_results' ) ) ) {
		$archive_results_query	= 'SELECT * FROM `wp_fz_event` WHERE published = 1 ORDER BY `date_start` DESC';
		$archive_results		= $wpdb->get_results( $archive_results_query );
		set_transient( 'archive_results', $archive_results, '15 * MINUTE_IN_SECONDS' );
	}
	// initiate variable for cities drop down filter
	$displayed_cities	= array();

	// get the years that exist for the year drop down filter (this way we don't have to add them every year)
	$events_dates	= wp_list_pluck( $archive_results, 'date_start' );

	// set up only years
	foreach ( $events_dates as $key => $event_date ) {
		$events_dates[$key] = date( 'Y', strtotime( $event_date ) );
	}

	// remove duplicates
	$events_years	= array_unique( $events_dates );
?>
<section id="finisher-zone-archive">
	<section class="wrapper">
		<h2><?php echo $past_results_txt; ?></h2>
		<div class="grid_3_special">

			<?php if ( $archive_results != '' ) { ?>

				<div class="controls">
					<label>Sort By</label>

					<div id="sort">
						<select id="sortselect">
							<option value="date:desc">Date Desc</option>
							<option value="date:asc">Date Asc</option>
							<option value="city:asc">City Name</option>
						</select>
					</div>

					<label>Filter</label>

					<button class="all-filter" data-filter="all">All</button>

					<select id="select-year" class="results-filter">
						<option value="">Year</option>
						<?php
							foreach ( $events_years as $year ) {
								echo '<option value=".year-'. $year .'">'. $year .'</option>';
							}
						?>
					</select>

					<select id="select-city" class="results-filter">
						<option value="">City</option>
						<?php
							// create select option for each city
							foreach ( $archive_results as $key => $past_event ) {

								$event_page	= get_blog_details( $past_event->blog_id );
								$event_city	= str_replace( '/', '', $event_page->path );

								$archive_results[$key]->event_slug	= $event_city;

								if ( !in_array( $event_city, $displayed_cities  ) ) {
									$displayed_cities[]	= $event_city;
									echo '<option value=".'. $event_city .'">'. $past_event->location .'</option>';
								}

							}
						?>
					</select>

				</div>

				<?php foreach ( $archive_results as $past_event ) {

					// lets keep everything organized and save what we need into variables
					$event_page		= get_blog_details( $past_event->blog_id );
					$event_blog_url	= $event_page->siteurl;
					$event_url		= get_permalink() .'search-and-results?eventid='. $past_event->pk_event_id;
					$event_img_src	= !empty( $past_event->image_display ) ? $past_event->image_display : $past_event->image_logo;
					$event_title	= stripslashes( $past_event->display_title );
					$event_date		= '';
					$event_filters	= array();

					// determine the dates
					if ( $qt_lang['lang'] != 'en' ) {

						$start_date_ex	= explode( '-', $past_event->date_start );

						$event_date		.= $start_date_ex[2] .' '. ucfirst( get_month_by_language( $qt_lang['lang'], ltrim( $start_date_ex[1], '0' ) ) ) .' '. $start_date_ex[0];

						if ($past_event->date_start != $past_event->date_end) {
							$end_date_ex = explode( '-', $past_event->date_end );
							$event_date .= ' &ndash; '. $end_date_ex[2] .' '. ucfirst( get_month_by_language( $qt_lang['lang'], ltrim( $end_date_ex[1], '0' ) ) ) .' '. $end_date_ex[0];
						}

					} else {

						$event_date	.= date( 'M d, Y', strtotime( $past_event->date_start ) );

						if ( $past_event->date_start != $past_event->date_end ) {
							$event_date	.= ' &ndash; '. date( 'M d, Y', strtotime( $past_event->date_end ) );
						}

					}

					// set up our sort and filter arrays
					$event_city			= $past_event->event_slug;
					$event_filters[]	= $event_city;
					$event_filters[]	= 'year-'. date( 'Y', strtotime( $past_event->date_start ) );
					?>

					<div class="mix column_special <?php echo implode( ' ', $event_filters ); ?>" data-city="<?php echo $event_city; ?>" data-date="<?php echo $past_event->date_start; ?>">
						<figure>
							<a href="<?php echo $event_url; ?>"><img class="lazy" data-original="<?php echo $event_img_src; ?>"></a>
						</figure>

						<h3><a href="<?php echo $event_url; ?>"><?php echo $event_title; ?></a></h3>

						<p><?php echo $event_date; ?></p>

						<p><a href="<?php echo $event_url; ?>"><?php echo $view_results_txt; ?></a> | <a href="<?php echo $event_blog_url; ?>"><?php echo $event_page_txt; ?></a></p>
					</div>

					<?php
				}

				// echo out a few gaps for the mixitup
				echo '<div class="gap"></div>';
				echo '<div class="gap"></div>';
				echo '<div class="gap"></div>';
				echo '<div class="gap"></div>';

			} else { ?>

				<div class="column_special">
					<h3>Sorry, no past events were found</h3>
				</div>

			<?php } ?>

		</div>

	</section>
</section>
