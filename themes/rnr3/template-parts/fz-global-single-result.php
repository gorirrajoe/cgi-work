<section id="finisher-zone-search-results">
	<section class="wrapper">
		<?php
		$custInfo = doGIndividualInfo($gl_a_id);
		foreach ( $custInfo as $keyCustInfo => $custInfo_value ) {
			$cust_info = $custInfo_value;
		}
		//print_r($cust_info);
		echo '<h2>'.$cust_info['FIRSTNAME'].' '.$cust_info['LASTNAME'].'</h2>';
		?>
		<table class="fz_rank_table fz_search_results_table">
			<!-- <tr>
				<th>Pace</th>
				<th>Finish Time</th>
				<th>Event</th>
				<th>Date</th>
			</tr> -->
			<?php
				$search1 = doGIndividualResults($gl_a_id);
				//print_r($search1);
				//print_r($_SERVER);
				$total_miles = array();
				$current_distance = '';
				$n_distances = 0;
				$e_date = array();
				$e_distance = array();
				$e_ftime = array();
				$e_pace = array();
				$e_name = array();
				$n = 0; //index arrays to start
				foreach ( $search1 as $keySearch => $search_value ) {

					if ( is_array( $search_value ) ) {

						$indiv_criteria = array(
							'eventid'		=> $search_value['fk_event_id'],
							'subevent_id'	=> $search_value['RACEID'],
							'bib'			=> trim( $search_value['BIB'] ),
						);

						$searchstring = get_query_string( $indiv_criteria, '', 1 );
						
						$subInfo = getFZSubEventInfo( $search_value['fk_event_id'], $search_value['RACEID']);
						foreach ( $subInfo as $keySubInfo => $subevent_value ) {
							$subEventInfo = $subevent_value;
						}
						//print_r($subEventInfo);
						if($current_distance == '' OR $current_distance != $subEventInfo['distance']){
							echo '<tr><th colspan="4">'.$subEventInfo['distance'].' KM </th></tr>';
							echo '<tr><th>Pace</th><th>Finish Time</th><th>Event</th><th>Date</th></tr>';
						}
						echo '<tr>';
						/*echo '<td>'.$subEventInfo['distance'].' KM </td>';*/
						echo '<td>'.$search_value['PACE'].'</td>';
						echo '<td>'.$search_value['FINISHTIME'].'</td>';
						echo '<td><a href="'.site_url('/search-and-results/?resultpage=1'. $searchstring).'" target="_blank">'.$subEventInfo['name'].'</a></td>';
						echo '<td>';
						$startDateEx = explode( '-',$search_value['date_start'] );
						if ( $qt_lang['lang'] != 'en' ) {
							echo $startDateEx[2] .' '. ucfirst(get_month_by_language( $qt_lang['lang'], ltrim( $startDateEx[1], '0' ) ) ) .' '. $startDateEx[0];
						} else {
							echo date( 'M d, Y', strtotime( $search_value['date_start'] ) );
						}
						echo '</td>';
						echo '</tr>';
						$e_date[$n] = $search_value['date_start'];
						$e_distance[$n]= $subEventInfo['distance'];
						$e_ftime[$n]= $search_value['FINISHTIME'];
						$e_pace[$n]= $search_value['PACE'];
						$e_name[$n]= $subEventInfo['name'];
						if (!isset($total_miles[$startDateEx[0]])) {
							$total_miles[$startDateEx[0]] = array();
						}
						if (!isset($total_miles[$startDateEx[0]][$subEventInfo['distance']])) {
							$total_miles[$startDateEx[0]][$subEventInfo['distance']]= 0;
						}
						$total_miles[$startDateEx[0]][$subEventInfo['distance']] += $subEventInfo['distance'];
						$n++;
						if($current_distance == '' OR $current_distance != $subEventInfo['distance']){
							$current_distance = $subEventInfo['distance'];
							$n_distances++;
						}
					}
				}
			?>
		</table>
		<?php 
		krsort($total_miles);
		$n = 1;
		foreach ( $total_miles as $keyTM => $valueTM ) :?>
		<section id="finisher-results">
			<h3><a onClick="myFunction('<?php echo $n;?>');"><?php echo $keyTM; ?> Total KM Breakdown</a></h3>
			<?php 
			$arrS = count($valueTM);
			krsort($valueTM);
			$totalKMForYear = 0;?>
			<ul class="fz_mat_times_<?php echo $n;?> fz_bullets" id="<?php echo $n;?>">
			<?php
			$dWidth = 100 / $arrS;
			echo '<style>
				.fz_mat_times_'.$n.'{
					display:none;
					width:100%;
				}
				.fz_mat_times_'.$n.' li {
					width:'. $dWidth .'%;
					display:table-cell;
				}
			</style>';
			foreach ( $valueTM as $keyTMD => $valueTMD ) :?>
				<li>
					<span class="fz_label"><?php echo $keyTMD;?> KM Events</span>
					<span><?php echo $valueTMD;?> KM</span>
				</li>
			<?php 
			$totalKMForYear += $valueTMD;
			endforeach;?>
			</ul>
			<ul class="fz_bullets fz_bullets_1">
				<li><?php echo $keyTM; ?> Total KM - <?php echo $totalKMForYear;?></li>
			</ul>
		</section>
		<?php $n++;
		endforeach; ?>
		
		<section id="finisher-course">
			<h3>Stats</h3>
			<div class="fz_speedometer">
				<h4 class="big_hdr">vs. Themself</h4>
				<?php 
				//testing data sort and graph display
				$u_distances = array_unique ( $e_distance);
				asort($e_date);
				rsort($u_distances);
				 ?>
				<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
				<script type="text/javascript">
					google.charts.load('current', {'packages':['corechart']});
					google.charts.setOnLoadCallback(drawChart);

					function drawChart() {
						var data = google.visualization.arrayToDataTable([
							['Dates'
							<?php 
							foreach ( $u_distances as $keyU => $valueU ) {
								echo ",'".$valueU."K'";
							}
							?>]
							<?php 
							foreach ( $e_date as $keyD => $valueD ) {
								$startDateEx = explode( '-',$valueD);
								//print_r($startDateEx);
								$paceFExplode = explode(':', $e_pace[$keyD]);
								//Get array location
								$s_month = $startDateEx[1] - 1;
								if(!empty($e_pace[$keyD]) AND $e_pace[$keyD] != ''){
									echo ",[new Date(".$startDateEx[0].",".$s_month.",".$startDateEx[2].")";
									$i = 1;
									$aKey = array_search ( $e_distance[$keyD], $u_distances);
									while ($i <= $n_distances) {
										if($aKey !== FALSE AND ($aKey == $i - 1)){
											//if key = 0 position = 1
											//if key = 1 position = 2, etc...
											if(sizeof($paceFExplode) < 3){
												echo ", [0,".$paceFExplode[0].",".$paceFExplode[1]."]";
											}else{
												echo ", [".$paceFExplode[0].",".$paceFExplode[1].",0]";
											}
										}else{
											echo ", null";
										}
										$i++;
									}
									echo " ]";
								}
							}
							?> 
						]);

						var options = {
							title: 'Performance',
							curveType: 'function',
							legend: { position: 'bottom' },
							interpolateNulls: true,
							vAxis: {format: 'mm:ss', minValue: 0}
						};

						var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

						chart.draw(data, options);
					}
				</script>
				
				<div id="curve_chart" style="width: 100%; height: 350px;"></div>

			</div>

		</section>

		<p class="center"><a href="<?php echo $redirecturl; ?>" class="cta"><?php echo $search_again_txt; ?></a></p>

		<?php get_correction_link( $qt_lang ); ?>

	</section>
</section>
<script>
function myFunction(idname) {
	var x = document.getElementById(idname);
	if (x.style.display === 'table') {
		x.style.display = 'none';
	} else {
		x.style.display = 'table';
	}
}
</script>
