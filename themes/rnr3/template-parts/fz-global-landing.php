<?php
	$dropdownlist = getGDropdownList();
	$genderList   = $dropdownlist['gender'];
	$stateList    = $dropdownlist['state'];
?>
	<section id="finisher-zone-search">
		<section class="wrapper">

			<h2><?php echo $search_results_txt; ?></h2>
			
			<div class="fz_event_hdr">
				<?php
				if ( have_posts() ) : while ( have_posts() ) : the_post();
					the_content();
				endwhile; endif;
				?>
				<p><?php echo $quick_results_txt; ?></p>
			</div>
			<div class="form_narrow fz_module">
				<form method="GET" action="" id="fz-search">

					<input type="hidden" name="resultspage" value="1">
					<input type="hidden" name="perpage" value="25">
					<fieldset>
						<ol>
							<li><input type="text" placeholder="<?php echo $first_name_txt; ?>" value="" id="firstname" name="firstname" class="field" pattern=".{3,}" title="3 characters minimum"></li>
							<li><input type="text" placeholder="<?php echo $last_name_txt; ?>" value="" id="lastname" name="lastname" class="field"></li>
							<li><input type="text" placeholder="Email" value="" id="email" name="email" class="field"></li>
							<li><input type="text" placeholder="<?php echo $city_txt; ?>" value="" id="city" name="city" class="field"></li>
							<li>
								<select name="gender" id="gender">
									<option value="">- <?php echo $gender_txt; ?> -</option>
									<?php
									foreach ( $genderList as $keyG => $valueG ) {
										echo '<option value="'. $valueG .'">'. $valueG .'</option>';
									}
									?>
								</select>
							</li>
							<li>
								<select name="state" id="state">
									<option value="">- <?php echo $state_txt; ?> -</option>
									<?php
									foreach ( $dropdownlist['state'] as $keyS => $valueS ) {
										echo '<option value="'. $valueS .'">'. $valueS .'</option>';
									}
									?>
								</select>
							</li>
						</ol>
					</fieldset>
					<fieldset>
						<button type="submit"><?php echo $search_txt; ?></button>
						<?php get_correction_link($qt_lang); ?>
					</fieldset>
				</form>
			</div>
		</section>
	</section>