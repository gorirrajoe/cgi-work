<?php
	$subEInfo	= getFZSubEvents( $eventid );
	$dbConnLink	= dbConnect( 'results' );
	$rStats		= getRunnerStats( $bib, $dbConnLink, $evInfo->rc_appid, $eventid );
	$upload_dir	= wp_upload_dir(); /* need only once */

	// Get event hashtag
	global $wpdb;

	$hash_blog_id	= $evInfo->blog_id;
	$hashtag		= $wpdb->get_var( "SELECT `twitter_hashtag` FROM wp_rocknroll_events WHERE blog_id = $hash_blog_id" );

	if ( !empty( $rStats ) ) {

		//check to see if relay or on team
		$partOfRelay	= 0;

		if ( !empty( $rStats['TEAMNAME'] ) && $rStats['RELAYPLACEOALL'] > 0 && !is_null( $rStats['RELAYPLACEOALL'] ) ) {

			$rTeamStats = getRunnersTeam( $bib, $rStats['TEAMNAME'], $dbConnLink, $evInfo->rc_appid, $eventid );

			if ( !empty( $rTeamStats ) && count( $rTeamStats ) == 1 ) {
				$partOfRelay	= 1;
				$teamMate		= $rTeamStats[0];
			}

		}

		//see if remix
		$remixStats = getRunnerRemix( $dbConnLink, $evInfo->rc_appid, $eventid, $rStats );
		/*if ( !empty( $remixStats ) ) {

			$rStatsC	= array();
			$rStatsC[0]	= $rStats;

			foreach ( $remixStats as $key => $value ) {
				$rStatsC[$key]	= $remixStats[$key];
			}

			//print_r(array_merge ( $rStats,$remixStats ) );
		} else {*/

			$rStatsC	= array( $rStats );

		/*}*/

		//loop through all
		foreach ( $rStatsC as $keyRunner => $rStats) {

			//find subevent
			foreach ( $subEInfo as $keySE => $subEI ) {

				if ( $subEI['ra_id'] == $rStats['RACEID'] )  {
					$subEventInfoToUse	= $subEI;
					//print_r($subEventInfoToUse);
				}

			}

			//check if Virtual Run/Virtual Race
			$race_type		= get_fz_data( 'race-type', $subEventInfoToUse['fk_race_type_id'] );
			$virtual_run	= in_array( $race_type['name'], array('Virtual Run', 'Virtual Race') ) ? true : false;

			//check to see if lucky finisher
			$luckyFinisher	= isLuckyFinisher( $bib, $subEventInfoToUse['pk_sub_event_id'] );
			$badge_tally	= getTally( $subEventInfoToUse, 'fk_badges_id_' );
			$persona_tally	= getTally( $subEventInfoToUse, 'fk_persona_id_' ); ?>

			<section id="finisher-zone-results">
				<section class="wrapper grid_2 offset240left">
					<div class="column sidenav stickem">
						<div id="nav-anchor"></div>
						<nav class="sticky-nav">
							<ul>
								<li><a href="#finisher-info"><img src="<?php echo bloginfo('template_directory'); ?>/img/finisher-zone/i-info.svg" alt="<?php echo $runner_info_txt; ?>"><?php echo $runner_info_txt; ?></a></li>
								<li><a href="#finisher-results"><img src="<?php echo bloginfo('template_directory'); ?>/img/finisher-zone/i-info.svg" alt="Race Results"><span class="hidden-sm"><?php echo $results_txt; ?></span><span class="hidden-lrg">Results</span></a></li>
								<?php if ( $subEventInfoToUse['mfoto_link'] != '' ) : ?>
									<li><a href="#finisher-photos"><img src="<?php echo bloginfo('template_directory'); ?>/img/finisher-zone/i-photos.svg" alt="<?php echo $photos_txt; ?>"><?php echo $photos_txt; ?></a></li>
								<?php endif;
								if ( $badge_tally[0] > 0 ) : ?>
									<li><a href="#finisher-badges"><img src="<?php echo bloginfo('template_directory'); ?>/img/finisher-zone/i-earned-badges.svg" alt="<?php echo $earned_badges_txt; ?>"><span class="hidden-sm"><?php echo $earned_badges_txt; ?></span><span class="hidden-lrg"><?php echo $badges_txt; ?></span></a></li>
								<?php endif;
								if ( $partOfRelay != 1 && !empty( $rStats['CHIPSTART'] ) && !empty($rStats['PACE']) ) : ?>
									<li><a href="#finisher-course"><img src="<?php echo bloginfo('template_directory'); ?>/img/finisher-zone/i-course-stats.svg" alt="<?php echo $course_stats_txt; ?>"><?php echo $course_stats_txt; ?></a></li>
								<?php endif;
								if ( $partOfRelay != 1 && !empty( $rStats['CHIPSTART'] ) && !empty($rStats['PACE']) ) : ?>
									<li><a href="#finisher-speedometer"><img src="<?php echo bloginfo('template_directory'); ?>/img/finisher-zone/i-speedometer.svg" alt="<?php echo $speedometer_txt; ?>"><?php echo $speedometer_txt; ?></a></li>
								<?php endif; ?>
								<?php if ( !empty( $subEventInfoToUse['fk_whats_next_id_1'] ) ) { ?>
								<li><a href="#finisher-whatsnext"><img src="<?php echo bloginfo('template_directory'); ?>/img/finisher-zone/i-whats-next.svg" alt="<?php echo $whats_next_txt; ?>"><?php echo $whats_next_txt; ?></a></li>
								<?php } ?>
								<?php if ( $persona_tally[0] > 0 ) : ?>
								<li><a href="#finisher-assessment"><img src="<?php echo bloginfo('template_directory'); ?>/img/finisher-zone/i-assessment.svg" alt="Rock 'n' Roll Assessment"><span class="hidden-sm"><?php echo $rnr_assessment_txt; ?></span><span class="hidden-lrg"><?php echo $assessment_txt; ?></span></a></li>
								<?php endif;
								if ( $subEventInfoToUse['mfoto_id'] != '' || $subEventInfoToUse['image_logo'] != '' || $evInfo->image_logo != '' ) : ?>
								<li><a href="#finisher-downloads"><img src="<?php echo bloginfo('template_directory'); ?>/img/finisher-zone/i-digital-cert.svg" alt="<?php echo $digital_certificate_txt; ?>"><span class="hidden-sm"><?php echo $digital_certificate_txt; ?></span><span class="hidden-lrg"><?php echo $digital_certificate_txt; ?></span></a></li>
								<?php endif; ?>
							</ul>
						</nav>
					</div>

				<div class="column">
					<div class="content">

						<section id="finisher-info">
							<h3><?php echo $runner_info_txt; ?></h3>
							<div class="fz_event_hdr">
								<h3>
									<?php
										$event_title	= ( !empty( $subEventInfoToUse['display_text'] ) ) ? $subEventInfoToUse['display_text'] : stripslashes( $evInfo->display_title ).' '.$subEventInfoToUse["display_title"];
										$event_title	= $qt_status ? qtrans_split( $event_title )[$qt_cur_lang] : $event_title;
										echo $event_title;
										// virtual run? add distance
										echo $virtual_run ? ' - ' . $rStats['SPECIALDIVISION'] : '';
									?>
								</h3>
								<p><?php
									if ( $qt_lang['lang'] != 'en' ) {
										$startDateEx = explode( '-',$subEventInfoToUse['date_start'] );
										echo $startDateEx[2] .' '. ucfirst(get_month_by_language( $qt_lang['lang'], ltrim( $startDateEx[1], '0' ) ) ) .' '. $startDateEx[0];
										if ( $subEventInfoToUse['date_start'] != $subEventInfoToUse['date_end'] ) {
											$endDateEx = explode( '-', $subEventInfoToUse['date_end'] );
											echo ' &ndash; '. $endDateEx[2] .' '. ucfirst( get_month_by_language( $qt_lang['lang'], ltrim( $endDateEx[1], '0' ) ) ).' '.$endDateEx[0];
										}
									} else {
										echo date( 'M d, Y', strtotime( $subEventInfoToUse['date_start'] ) );
										if ( $subEventInfoToUse['date_start'] != $subEventInfoToUse['date_end'] ) {
											echo ' &ndash; '. date( 'M d, Y', strtotime( $subEventInfoToUse['date_end'] ) );
										}
									}?></p>
							</div>
							<ul class="fz_runner_info fz_bullets fz_bullets_2">
								<li>
									<span class="fz_label"><?php echo $name_txt; ?></span>
									<span><?php echo $rStats['FIRSTNAME'].' '.$rStats['LASTNAME']; ?></span>
									<?php if ( !empty( $rStats['CLUB'] ) ) : ?>
										<span class="fz_label"><?php echo $rStats['CLUB'];?></span>
									<?php endif; ?>
								</li>
								<li>
									<span class="fz_label"><?php echo $bib_number_txt; ?></span>
									<span><?php echo $rStats['BIB'];?></span>
								</li>
							</ul>
							<?php
								if ( $partOfRelay == 1 ) { ?>
									<ul class="fz_runner_info fz_bullets fz_bullets_2">
										<li>
											<span class="fz_label"><?php echo $name_txt; ?></span>
											<span><?php echo $teamMate['FIRSTNAME'].' '.$teamMate['LASTNAME'];?></span>
										</li>
										<li>
											<span class="fz_label"><?php echo $bib_number_txt; ?></span>
											<span><?php echo $teamMate['BIB'];?></span>
										</li>
									</ul>
									<ul class="fz_team_info fz_bullets fz_bullets_2">
										<li>
											<span class="fz_label"><?php echo $team_txt; ?></span>
											<span><?php echo $rStats['TEAMNAME'];?></span>
										</li>
										<li>
											<span class="fz_label"><?php echo $division_txt; ?></span>
											<span><?php echo $rStats['TEAMDIVISION'];?></span>
										</li>
									</ul>
								<?php } else {
									// Build Hometown
									$hometown	= '';
									$hometown	.=  !empty( $rStats['CITY'] ) ? $rStats['CITY'].', ' : '';
									$hometown	.=  !empty( $rStats['STATE'] ) ? $rStats['STATE']. ' ' : ' ';
									$hometown	.=  !empty( $rStats['COUNTRY'] ) ? $rStats['COUNTRY'] : '';
									?>
									<ul class="fz_runner_vitals fz_bullets fz_bullets_3">
										<li class="gender">
											<span class="fz_label"><?php echo $gender_txt; ?></span>
											<span><?php echo $rStats['SEX'];?></span>
										</li>
										<li class="age">
											<span class="fz_label"><?php echo $age_txt; ?></span>
											<span><?php echo $rStats['AGE'];?></span>
										</li>
										<li class="hometown">
											<span class="fz_label"><?php echo $hometown_txt; ?></span>
											<span><?php echo $hometown; ?></span>
										</li>
									</ul>
								<?php }
								if ( is_array( $luckyFinisher ) ) { ?>
									<ul class="fz_bullets fz_winner_alert">
										<li><a href="#finisher-badges" class="scrolly" style="text-decoration:underline;"><?php echo $rStats['FIRSTNAME']; ?>, <?php echo $selected_lucky_txt; ?></a></li>
										<a href="http://www.runrocknroll.com/share-rock-n-roll-moment/" target="_blank" style="color: white;"><li style="background: #ee1c24; color: white;">Share Your Story &#62;&#62;</li></a>
									</ul>
								<?php } else { ?>
									<ul class="fz_bullets fz_winner_alert">
										<a href="http://www.runrocknroll.com/share-rock-n-roll-moment/" target="_blank" style="color: white;"><li style="background: #ee1c24; color: white;">Share Your Story &#62;&#62;</li></a>
									</ul>
								<?php }
								get_correction_link($qt_lang);
							?>
						</section>

						<section id="finisher-results">
							<h3><?php echo $results_txt; ?></h3>
							<ul class="runner_info fz_bullets fz_bullets_2<?php echo $virtual_run ? ' fz_runner_placing_alt' : '' ?>">
								<?php
								$beatExpected = 0;
								if ( $partOfRelay != 1 && !empty( $rStats['PR_TIME'] ) && $rStats['PR_TIME'] > $rStats['FINISHTIME'] ) {

									$pr_text		= $beat_time_txt;
									$pr_class		= 'class="fz_bullet_highlight"';
									$beatExpected	= 1;

								} else {

									$pr_text	= '';
									$pr_class	= '';

								} ?>
								<li <?php echo $pr_class; ?>>
									<span class="fz_label"><?php echo $finish_time_txt; ?></span>
									<span>
										<?php
											if ( !empty( $rStats['FINISHTIME'] ) && $partOfRelay != 1 ) {
												echo $rStats['FINISHTIME'] . $pr_text;
											} elseif ( $partOfRelay == 1 ) {
												$timeDiff	= 0;
												$chipStart	= 0;

												if ( !empty( $rStats['CHIPSTART'] ) ) {
													$timeDiff	= strtotime( $rStats['CHIPSTART'] ) - strtotime( $rStats['GUNSTART'] );
													$chipStart	= strtotime( $rStats['CHIPSTART'] );
												} else {
													$timeDiff	= strtotime( $teamMate['CHIPSTART'] ) - strtotime( $teamMate['GUNSTART'] );
													$chipStart	= strtotime( $teamMate['CHIPSTART'] );
												}

												if ( !empty( $rStats['FINISHTIME'] ) ) {
													$ftimeEst	= date( 'H:i:s', ( strtotime( $rStats['FINISHTIME'] ) - $timeDiff ) );
												} elseif ( $partOfRelay == 1 ) {
													$ftimeEst	= date( 'H:i:s', ( strtotime( $teamMate['FINISHTIME'] ) - $timeDiff ) );
												}

												echo $ftimeEst;

												if ( !empty( $rStats['PR_TIME'] ) && $rStats['PR_TIME'] > $ftimeEst && $partOfRelay != 1 ) {
													echo ' PR';
												}
											} else {
												echo ' - ';
											}
										?>
									</span>
								</li>
								<li>
									<?php if ( !$virtual_run ) : ?>
									<span class="fz_label"><?php echo $expected_time_txt; ?></span>
									<?php if ( $partOfRelay != 1 ) { ?>
										<span><?php if ( !empty( $rStats['PR_TIME'] ) ) { echo $rStats['PR_TIME']; } else { echo '-'; } ?></span>
									<?php } else { ?>
										<span><?php echo '-';?></span>
									<?php } ?>
									<?php endif; ?>
									<?php if ( $badge_tally[0] > 0 && $virtual_run ) : ?>
									<figure><a class="scrolly" href="#finisher-badges"><img id="pBadge" class="pBadge" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/generic-badge.jpg" alt=""></a></figure>
									<a class="cta-alt scrolly" href="#finisher-badges">See <?php if ( $partOfRelay != 1 ) { echo $rStats['FIRSTNAME']; } else { echo $rStats['TEAMNAME']; } ?>'s Badges</a>
									<?php endif; ?>
								</li>
							</ul>

							<?php if ( $beatExpected == 100 ) : ?>
							<ul class="fz_bullets">
								<li style="color: black;"><?php echo $beat_expected_txt; ?></li>
							</ul>
							<?php endif;?>

							<!-- Promote another event -->
							<ul class="fz_bullets">

								<?php
									$tdiff	= time() - strtotime( $subEventInfoToUse['date_start'] );

									//adding presale exception for mexico city
									if ( $tdiff > 604800 || $tdiff < 0 || $evInfo->presale != 1 ) {
										echo '<a href="http://www.runrocknroll.com/#findrace" target="_blank" style="color: white;"><li style="background: #ee1c24; color: white; font-weight: bold;text-transform: uppercase;">FIND YOUR NEXT RACE &#62;&#62;</li></a>';
									} else {
										//presale link
										echo '<a href="'. get_site_url( $hash_blog_id ) .'/presale" target="_blank" style="color: white;"><li style="background: #ee1c24; color: white; font-weight: bold;text-transform: uppercase;">SIGN UP FOR NEXT YEAR &#62;&#62;</li></a>';
									}
								?>

							</ul>

							<?php
								// Set up some defaults
								// Actual mats that have a time recorded
								$matCount	= 0;
								$matsUsed	= array();

								if ( !empty( $rStats['SPLITSEQUENCE'] ) ) {
							?>
								<ul class="fz_mat_times_<?php echo $keyRunner;?> fz_bullets">
									<?php
										/* count the mats then use it to find width of each li (inline style) */
										$matsUsed = explode( ',', $rStats['SPLITSEQUENCE'] );
										$dWidth = 100 / count( $matsUsed );
										echo '<style>
												.fz_mat_times_'.$keyRunner.' {
													display:table;
													width:100%;
												}
												.fz_mat_times_'.$keyRunner.' li {
													width:'. $dWidth .'%;
													display:table-cell;
												}
										</style>';

										//display time, average, and distance in order
										foreach ( $matsUsed as $keyMats => $mat_value ) {

											$mat_value_t  = trim( $mat_value );

											if ( $mexico_race ) {
												$matTime      = 'MP'. zeroise( $keyMats + 1, 2 ) .'TIME';
												$matAvgTime   = 'MP'. zeroise( $keyMats + 1, 2 ) .'_AVERAGEPACE';
												$matDistance  = 'MP'. zeroise( $keyMats + 1, 2 ) .'_DISTANCE';
											} else {
												$matTime      = $mat_value_t .'TIME';
												$matAvgTime   = $mat_value_t .'_AVERAGEPACE';
												$matDistance  = $mat_value_t .'_DISTANCE';
											}

											if ( $partOfRelay != 1 ) {

												if ( !empty( $rStats[$matTime] ) ) {

													$matCount++;

													if ( empty( $rStats['CHIPSTART'] ) ) {

														$spTime = date ( 'H:i:s', ( strtotime($rStats[$matTime]) - strtotime( $rStats['GUNSTART'] ) ) );
														if ( strpos ( $spTime , '0:') == 1 ) { $spTime = substr ( $spTime , 3 ); }
														echo '<li><span class="fz_label">'. $rStats[$matDistance] .'</span><span>'. $spTime .'</span></li>';

													} else {

														echo '<li><span class="fz_label">'. $rStats[$matDistance] .'</span><span>' .$rStats[$matTime] .'</span>
														<span class="fz_label fz_label2">'. $pace_txt .'</span><span>'. $rStats[$matAvgTime] .'</span></li>';

													}
												} else {

													echo '<li><span class="fz_label">'. $rStats[$matDistance] .'</span><span> - </span>
													<span class="fz_label fz_label2">'. $pace_txt .'</span><span>-</span></li>';

												}

											} else {

												if ($rStats['SPLITSEQUENCE'] == $teamMate['SPLITSEQUENCE']) {

													if ( !empty( $rStats[$matTime] ) ) {

														$matCount++;

														if ( empty( $rStats['CHIPSTART'] ) && empty( $teamMate[$matTime] ) ) {
															//other person started the event
															$teamMatMMStart = gmdate ( 'H:i:s', ( strtotime($rStats[$matTime]) - $chipStart));
															if (strpos ( $teamMatMMStart , '0:') == 1) { $teamMatMMStart = substr ( $teamMatMMStart , 3);}
															echo '<li class="fz_bullet_highlight"><span class="fz_label">'.$rStats[$matDistance].'</span><span>'.$teamMatMMStart.'</span></li>';
														} elseif ( empty( $rStats['CHIPSTART'] ) && !empty( $teamMate[$matTime] ) ) {
															//Display Teammate's time
															echo '<li><span class="fz_label">'.$rStats[$matDistance].'</span><span>'.$teamMate[$matTime].'</span></li>';
														} else {
															echo '<li class="fz_bullet_highlight"><span class="fz_label">'.$rStats[$matDistance].'</span><span>'.$rStats[$matTime].'</span></li>';
														}

													} else {

														if ( empty( $rStats['CHIPSTART'] ) ) {
															//other person started the event
															//Display Teammate's time
															echo '<li><span class="fz_label">'.$rStats[$matDistance].'</span><span>'.$teamMate[$matTime].'</span></li>';
														} elseif (!empty($teamMate[$matTime])) {
															//Display Teammate's time
															$teamMatMMStart = gmdate( 'H:i:s', ( strtotime( $teamMate[$matTime] ) - $chipStart ) );
															if ( strpos ( $teamMatMMStart , '0:') == 1 ) { $teamMatMMStart = substr ( $teamMatMMStart , 3 ); }
															echo '<li><span class="fz_label">'.$rStats[$matDistance].'</span><span>'.$teamMatMMStart.'</span></li>';
														} else {
															echo '<li><span class="fz_label">'.$rStats[$matDistance].'</span><span> </span></li>';
														}

													} // End if ( !empty( $rStats[$matTime] ) )
												}

											}

										}
									?>
								</ul>
							<?php }

							if ( $partOfRelay != 1 && ( !empty( $rStats['PACE'] ) || !empty( $rStats['CHIPSTART'] ) || !empty( $rStats['CLOCKFINISHTIME'] ) ) ) { ?>
								<ul class="fz_runner_times fz_bullets fz_bullets_3">
									<li>
										<span class="fz_label"><?php echo $pace_txt; ?></span>
										<span><?php echo $rStats['PACE']; ?></span>
									</li>
									<li>
										<span class="fz_label"><?php echo $chip_time_txt; ?></span>
										<span><?php if ( !empty( $rStats['FINISHTIME'] ) ) { echo $rStats['FINISHTIME']; } elseif ( $partOfRelay == 1 ) { echo $teamMate['FINISHTIME']; } ?></span>
									</li>
									<li>
										<span class="fz_label"><?php echo $clock_time_txt; ?></span>
										<span>
											<?php
											if ( $mexico_race ) {
												echo !empty( $rStats['CLOCKFINISHTIME'] ) ? $rStats['CLOCKFINISHTIME'] : '';
											} else {

												if ( !empty( $rStats['CHIPSTART'] ) ) {
													$timeDiff = strtotime( $rStats['CHIPSTART'] ) - strtotime( $rStats['GUNSTART'] );
												} else {
													$timeDiff = 0;
												}

												if ( !empty( $rStats['FINISHTIME'] ) ) {

													if ( strlen( $rStats['FINISHTIME'])  < 6 ) {
														$displayFT	= "00:".$rStats['FINISHTIME'];
														$ftimeEst	= date( 'H:i:s', ( ( strtotime( $displayFT ) ) + $timeDiff ) );
													} else {
														$ftimeEst	= date ( 'H:i:s', ( ( strtotime( $rStats['FINISHTIME'] ) ) + $timeDiff ) );
													}

												} else {

													$ftimeEst	= ' - ';

												}
												echo $ftimeEst;
											}
											?>
										</span>
									</li>
								</ul>
							<?php } ?>

							<?php
								if ( !empty( $rStats['MILITARY_BRANCH'] ) && !empty( $rStats['MILOALLPLACEBYBRANCH'] ) ):?>
							<ul class="runner_info_data fz_bullets fz_bullets_3" style="margin-bottom: 0;border-bottom: 0;border-top: 1px solid #e5e5e5;">
								<li>
									<span class="fz_label"><?php echo $military_branch_txt; ?></span>
									<span><?php echo $rStats['MILITARY_BRANCH']; ?></span>
								</li>
								<li>
									<span class="fz_label"><?php echo $rStats['MILITARY_BRANCH'].' '.$overall_txt; ?></span>
									<span><?php

									$militaryTots	= unserialize( $subEventInfoToUse['MILITARY'] );
									
									//find all keys totals for that branch
									$branchTotal = 0;
									foreach ( $militaryTots as $keyMilTots => $milTot_value ) {
										if(strpos ( $keyMilTots, $rStats['MILITARY_BRANCH']) !== FALSE){
											$branchTotal += $milTot_value;
										}
									}

									echo $rStats['MILOALLPLACEBYBRANCH'] .' / '. $branchTotal;

									?></span>
								</li>
								<li>
									<span class="fz_label"><?php echo $rStats['MILITARY_BRANCH'].' '.$gender_txt; ?></span>
									<span><?php

									$genderBranchTotal = $rStats['MILITARY_BRANCH'].'_'.$rStats['SEX'];

									echo $rStats['MILSEXPLACEBYBRANCH'] .' / '. $militaryTots[$genderBranchTotal];

									?></span>
								</li>
							</ul>
							<?php
								endif;
								$pct = 100;
								if($partOfRelay == 1){
									if(empty($rStats['RELAYPLACEOALL'])){
										$rStats['RELAYPLACEOALL'] = $teamMate['RELAYPLACEOALL'];
									}
									$rStats['PLACEOALL_CHIP'] = $rStats['RELAYPLACEOALL'];
									if(empty($rStats['RELAYPLACEDIVISION'])){
										$rStats['RELAYPLACEDIVISION'] = $teamMate['RELAYPLACEDIVISION'];
									}
									$rStats['PLACEDIVISION_CHIP'] = $rStats['RELAYPLACEDIVISION'];
								}
								if ( $rStats['PLACEOALL_CHIP'] != 0 ||  !empty( $rStats['PLACEDIVISION_CHIP'] ) ):
							?>
							<ul class="<?php if ( $partOfRelay == 1 ) : ?>fz_runner_placing fz_bullets fz_bullets_2<?php else : ?>runner_info_data fz_bullets fz_bullets_3<?php endif; ?>"
							<?php if ( $persona_tally[0] <= 0 && $badge_tally[0] <= 0) { /*echo 'style="margin-bottom: 30px;border-bottom: 1px solid #e5e5e5;"';*/}elseif ( $partOfRelay == 1 ) { echo 'style="margin-bottom: 0;border-bottom: 0;"';}?>>
								<li>
									<span class="fz_label"><?php echo $overall_txt; ?></span>
									<span><?php

									if ( $partOfRelay != 1 ) {

										echo $rStats['PLACEOALL_CHIP'] .' / '. ( $subEventInfoToUse['OVERALLFEMALE'] + $subEventInfoToUse['OVERALLMALE'] );

										if ( $rStats['PLACEOALL_CHIP'] != 0 ) {
											$pct	= 100 - round( ( $rStats['PLACEOALL_CHIP'] / ($subEventInfoToUse['OVERALLFEMALE'] + $subEventInfoToUse['OVERALLMALE'] ) ) * 100, 1 );
										}

									} else {
										$divisionTots2	= unserialize( $subEventInfoToUse['DIVISION'] );
										$relayTotal		= array_sum( $divisionTots2 );

										echo $rStats['RELAYPLACEOALL'] .' / '. $relayTotal;

										$pct	= 100 - round( ( $rStats['RELAYPLACEOALL'] / $relayTotal ) * 100, 1 );
									} ?></span>
								</li>
								<li>
									<span class="fz_label"><?php echo $division_txt; ?></span>
									<span>
										<?php if ( $partOfRelay != 1 && !empty( $rStats['PLACEDIVISION_CHIP'] ) ) {
											echo $rStats['PLACEDIVISION_CHIP'];
										} elseif ( $partOfRelay == 1) {
											echo $rStats['RELAYPLACEDIVISION'];
										} else {
											echo ' - ';
										}
										echo ' / ';

										$divisionTots	= unserialize( $subEventInfoToUse['DIVISION'] );
										if ( $partOfRelay != 1 ) {
											echo $divisionTots[$rStats['DIVISION']];
										} else {
											echo $divisionTots[$rStats['TEAMDIVISION']];
										} ?>
									</span>
								</li>
								<?php if ( $partOfRelay != 1 ) { ?>
									<li>
										<span class="fz_label"><?php echo $gender_txt; ?></span>
										<span><?php echo $rStats['PLACESEX_CHIP']; ?> / <?php if ( stripos(  $rStats['SEX'], 'f' ) !== false) { echo $subEventInfoToUse['OVERALLFEMALE']; } else { echo $subEventInfoToUse['OVERALLMALE']; } ?></span>
									</li>
								<?php } ?>
							</ul>
							<?php endif; ?>

							<?php
								if ( ( $persona_tally[0] > 0 || $badge_tally[0] > 0 ) && !$virtual_run ) :

									$runner_name	= ($partOfRelay != 1) ? $rStats['FIRSTNAME'] : $rStats['TEAMNAME'];
								?>
								<ul class="fz_runner_placing fz_bullets fz_bullets_2">
									<li>
										<?php if ( $persona_tally[0] > 0 ) : ?>
											<figure><a class="scrolly" href="#finisher-assessment"><img id="pPersona" class="pPersona" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/generic-persona.jpg" alt=""></a></figure>
											<a class="cta-alt scrolly" href="#finisher-assessment"><?php echo viewItemMessage( $qt_lang, 'persona', $runner_name ); ?></a>
										<?php else: ?>
										-
										<?php endif; ?>
									</li>
									<li>
										<?php if ( $badge_tally[0] > 0 ) : ?>
											<figure><a class="scrolly" href="#finisher-badges"><img id="pBadge" class="pBadge" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/generic-badge.jpg" alt=""></a></figure>
											<a class="cta-alt scrolly" href="#finisher-badges"><?php echo viewItemMessage( $qt_lang, 'badge', $runner_name ); ?></a>
										<?php else: ?>
										-
										<?php endif; ?>
									</li>
								</ul>
							<?php endif; ?>
							<?php
							//multiple events
							if ( !empty( $remixStats ) ) :?>
							<ul class="fz_bullets fz_multiple_events_msg">
								<li>
								<?php echo $mutiple_events_txt; ?>
								<?php
									foreach ($remixStats as $key => $value) {
										$multi_criteria	= array(
											'eventid'		=> $eventid,
											'subevent_id'	=> $value['RACEID'],
											'bib'			=> trim( $value['BIB'] ),
										);

										$remixstring	= get_query_string( $multi_criteria, '', 1 );
										echo '<br/><a href="'. $redirecturl .'?resultpage=1'. $remixstring .'" target="_blank">'. $other_events_txt .'</a>';
									}
								?>
								</li>
							</ul>
							<?php endif;?>

							<?php get_correction_link($qt_lang); ?>
						</section>

						<?php if ( $subEventInfoToUse['mfoto_link'] != '' ) { ?>

							<section id="finisher-photos">
								<h3><?php echo $photos_txt; ?></h3>
								<iframe width="100%" height="335" frameborder="0" id="asiframe" scrolling="no" src="<?php echo $subEventInfoToUse['mfoto_link'];?>&amp;LastName=<?php echo $rStats['LASTNAME'];?>&amp;BibNumber=<?php echo $rStats['BIB'];?>">&lt;a href="http://www.marathonfoto.com"&gt;Visit Marathonfoto.com to view and order your photos.&lt;/a&gt;</iframe>
							</section>

						<?php }

						if ( $badge_tally[0] > 0 ) {

							$badgeRInfo			= getFZBadges( $badge_tally[1] );
							ksort($badgeRInfo);
							$earned_badges_ids	= array();
							?>

							<section id="finisher-badges">
								<h3><?php echo $earned_badges_txt; ?></h3>
								<div class="fz_badgelist">
									<div class="fz_badgelist_hdr">
										<h4 id="badgeH4" class="big_hdr"><?php echo $partOfRelay != 1 ? $rStats['FIRSTNAME'] .' '. $rStats['LASTNAME'] : $rStats['TEAMNAME']; ; ?>'s Earned Badge</h4>
									</div>
									<ul>
										<?php
										$earnedCourseBadges	= 0;
										$infoToChange		= array();
										$infoToChange['bc']	= 0;
										$share_counter		= 0;
										$specMilBadge 		= 0;

										foreach ( $badgeRInfo as $keyBd => $bd_value ) :

											$qualForBadges				= unserialize( $bd_value['argument'] );
											$earnB						= 0;
											$infoArray					= array();
											$infoArray['param']			= $qualForBadges[0]['param'];
											$infoArray['value']			= $qualForBadges[0]['value'];
											$infoArray['operator']		= $qualForBadges[0]['operator'];
											$infoArray['pct']			= $pct;
											$infoArray['ft']			= $rStats['FINISHTIME'];
											$infoArray['age']			= $rStats['AGE'];
											$infoArray['sex']			= $rStats['SEX'];
											$infoArray['eventdate']		= $subEventInfoToUse['date_start'];
											$infoArray['remix']			= !empty( $remixStats ) ? 1 : 0;
											$infoArray['luckyfinisher']	= $luckyFinisher;
											$infoArray['badge_id']		= $bd_value['pk_ID'];
											$infoArray['bib']			= $rStats['BIB'];
											$infoArray['mil']			= !empty( $rStats['MILITARY_BRANCH'] ) ? $rStats['MILITARY_BRANCH'] : '123456';

											if ( count( $qualForBadges ) == 1 ) {

												$returnQualValue	= qualificationTimeOrPercent( $infoArray );
												$earnB				= $returnQualValue;
												if($infoArray['param'] == 'military'){
													$specMilBadge = 1;
												}

											} elseif ( $qualForBadges >= 1 ) {
												$returnQualValue				= qualificationTimeOrPercent( $infoArray );
												$infoArray2						= array();
												$infoArray2['param']			= $qualForBadges[1]['param'];
												$infoArray2['value']			= $qualForBadges[1]['value'];
												$infoArray2['operator']			= $qualForBadges[1]['operator'];
												$infoArray2['pct']				= $pct;
												$infoArray2['ft']				= $rStats['FINISHTIME'];
												$infoArray2['age']				= $rStats['AGE'];
												$infoArray2['sex']				= $rStats['SEX'];
												$infoArray2['remix']			= !empty( $remixStats ) ? 1 : 0;
												$infoArray2['luckyfinisher']	= $luckyFinisher;
												$infoArray2['badge_id']			= $bd_value['pk_ID'];
												$infoArray2['bib']				= $rStats['BIB'];
												$infoArray2['mil']				= !empty( $rStats['MILITARY_BRANCH'] ) ? $rStats['MILITARY_BRANCH'] : '123456';
												$returnQualValue2				= qualificationTimeOrPercent( $infoArray2 );

												if ( $returnQualValue == 1 && $returnQualValue2 == 1 ) {
													$earnB = $returnQualValue;
												}
												//$earnB	= 1;
											} else {
												//no qualification - automatic earn
												$earnB	= 1;
											}
											if ( $earnB == 1 ) :
												if ( !empty( $luckyFinisher ) && in_array( $bd_value['lucky_course'], $luckyFinisher) && !empty( $bd_value['lucky_course'] ) ) {
													$cFZBadge		= getFZBadges( $bd_value['lucky_course']);
													$courseFZBadge	= $cFZBadge[$bd_value['lucky_course']];

													$filename_badge						= '/'.$subEventInfoToUse['pk_sub_event_id'].'_badge_'.$courseFZBadge['pk_ID'];

													$badge_spec_persona_image			= $upload_dir['basedir'] .'/'. date( 'Y/m', strtotime( $subEventInfoToUse['date_start'] ) ) . $filename_badge .'.jpg';
													$badge_spec_persona_image_display	= $upload_dir['baseurl'] .'/'. date( 'Y/m', strtotime( $subEventInfoToUse['date_start'] ) ) . $filename_badge .'.jpg';

													if ( file_exists( $badge_spec_persona_image ) ) {
														$courseFZBadge['image_social']	= str_replace ( 'www', 'cdn', $badge_spec_persona_image_display);
													}
													// check for image in appropriate language
													$courseFZBadge['image_social']	= image_translation( $qt_lang, $courseFZBadge['image_social'] );

													++$infoToChange['bc'];
													if ( $infoToChange['bc'] == 1 ) {
														$infoToChange['bi'] = $courseFZBadge['image_display'];
													}

													$earned_badges_ids[]	= $courseFZBadge['pk_ID'];

													//$share_url = urlencode("http://".$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
													$share_url	= 'www.runrocknroll.com'. $_SERVER['REQUEST_URI'];
													if ( ( !empty( $courseFZBadge['distance'] ) && $courseFZBadge['distance'] > 0 ) || !empty( $bd_value['lucky_course'] ) ) { ++$earnedCourseBadges; }
													$share_counter++;

													$badge_title	= $qt_status ? qtrans_split( $courseFZBadge['display_title'] )[$qt_cur_lang] : $courseFZBadge['display_title'];
													$badge_text		= $qt_status ? qtrans_split( $courseFZBadge['display_text'] )[$qt_cur_lang] : $courseFZBadge['display_text'];

													$bd_value['image_social']	= $courseFZBadge['image_social'];
													$bd_value['image_display']	= $courseFZBadge['image_display'];

												} else {
													if ( $specMilBadge = 1 && !empty( $rStats['MILITARY_BRANCH'] ) ) {
														$strbranch = str_replace (' ', '_', strtolower($rStats['MILITARY_BRANCH']));

														$filename_badge				= '/'.$subEventInfoToUse['pk_sub_event_id'].'_badge_'.$bd_value['pk_ID'].'_'.$strbranch;

														$badge_spec_persona_image	= $upload_dir['basedir'] .'/'. date( 'Y/m', strtotime( $subEventInfoToUse['date_start'] ) ) . $filename_badge .'.jpg';

														//Use Default if not specific military badge
														if ( !file_exists( $badge_spec_persona_image ) ) {
															$filename_badge					= '/'.$subEventInfoToUse['pk_sub_event_id'].'_badge_'.$bd_value['pk_ID'];
														}else{
															$badge_spec_persona_image_display	= $upload_dir['baseurl'] .'/'. date( 'Y/m', strtotime( $subEventInfoToUse['date_start'] ) ) . $filename_badge .'.jpg';
															$bd_value['image_display'] = $badge_spec_persona_image_display;
														}
													}else{
														$filename_badge						= '/'.$subEventInfoToUse['pk_sub_event_id'].'_badge_'.$bd_value['pk_ID'];
													}

													$badge_spec_persona_image			= $upload_dir['basedir'] .'/'. date( 'Y/m', strtotime( $subEventInfoToUse['date_start'] ) ) . $filename_badge .'.jpg';
													$badge_spec_persona_image_display	= $upload_dir['baseurl'] .'/'. date( 'Y/m', strtotime( $subEventInfoToUse['date_start'] ) ) . $filename_badge .'.jpg';


													if ( file_exists( $badge_spec_persona_image ) ) {
														$bd_value['image_social']	= str_replace ( 'www', 'cdn', $badge_spec_persona_image_display);
													}

													/*if ( $bd_value['fk_event_id'] == 'global' ) {
														$event_filename_badge = '/'.$subEventInfoToUse["fk_event_id"].'_badge_'.$bd_value['pk_ID'].'_event';
														$event_badge_spec_persona_image = $upload_dir['basedir'] .'/'. date( 'Y/m', strtotime($subEventInfoToUse["date_start"])) .$event_filename_badge.'.jpg';
														$event_badge_spec_persona_image_display = $upload_dir['baseurl'] .'/'. date( 'Y/m', strtotime($subEventInfoToUse["date_start"])).$event_filename_badge.'.jpg';
														if (file_exists($event_badge_spec_persona_image)) {
															$bd_value['image_social'] = str_replace ( 'www', 'cdn', $event_badge_spec_persona_image_display);
														}
													}*/

													// check for image in appropriate language
													$bd_value['image_social']	= image_translation( $qt_lang, $bd_value['image_social'] );

													++$infoToChange['bc'];
													if ( $infoToChange['bc'] == 1 ) {
														$infoToChange['bi'] = $bd_value['image_display'];
													}

													$earned_badges_ids[]	= $bd_value['pk_ID'];

													//$share_url = urlencode("http://".$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
													$share_url	= 'www.runrocknroll.com'. $_SERVER['REQUEST_URI'];
													if ( !empty( $bd_value['distance'] ) && $bd_value['distance'] > 0 ) { ++$earnedCourseBadges; }
													$share_counter++;

													$badge_title	= $qt_status ? qtrans_split( $bd_value['display_title'] )[$qt_cur_lang] : $bd_value['display_title'];
													$badge_text		= $qt_status ? qtrans_split( $bd_value['display_text'] )[$qt_cur_lang] : $bd_value['display_text'];
												}
												?>
											<li class="smaller_txt">
												<figure class="badge_alignleft"><img src="<?php echo $bd_value['image_display']; ?>" alt=""></figure>
												<h3><?php echo $badge_title; ?></h3>
												<p><?php echo $badge_text; ?></p>
												<div class="fz_badge_social">
													<figure class="hidden"><img src="<?php echo $bd_value['image_social']; ?>" alt=""></figure>
													<a href="<?php echo get_permalink( get_page_by_title( 'FB Share' )->ID ); ?>?type=badge&image=<?php echo $bd_value['image_social']; ?>&itemName=<?php echo $badge_title; ?>&event=<?php echo $event_title; ?>" class="fb-share-badge"><span class="icon-facebook"></span></a>
													<a href="<?php echo $bd_value['image_social']; ?>" download><span class="icon-instagram"></span></a>
													<span class="post-widget" id="badge_<?php echo $share_counter; ?>" data-fz-url="<?php echo $share_url; ?>" data-fz-title="<?php echo shareMessage( $qt_lang, $badge_title, 'badge', $hashtag ); ?>" data-pin-image="<?php echo $bd_value['image_social']; ?>"></span>
													<!-- <a href="https://twitter.com/intent/tweet?text=<?php echo urlencode( shareMessage( $qt_lang, $badge_title, 'badge', $hashtag ) );?>&via=RunRocknRoll&hashtags=<?php echo ltrim($hashtag, '#'); ?>&wrap_links‌​=true&url=<?php echo $share_url; ?>" target="_blank" class="twitter-share" data-item-shared="badge"><span class="icon-twitter"></span></a>
													<a class="pinterest-share" data-pin-do="buttonPin" data-pin-custom="true" href="//www.pinterest.com/pin/create/button/?url=<?php echo $share_url; ?>&media=<?php echo urlencode($bd_value['image_social']); ?>&description=<?php echo urlencode("I earned the ".$badge_title);?>" count-layout="none"><img src="<?php echo get_bloginfo('template_directory');?>/img/40x40_pinterest.png" alt="Share on Pinterest"></a> -->
												</div>
											</li>
											<?php endif;?>
										<?php endforeach; //endforeach?>
									</ul>
								</div>
							</section>
						<?php } else {
							//no badges to show
							// $badgeRInfo == null;

						}

						/* COURSE HIGHLIGHTS */
						if ( $partOfRelay != 1 && ( !empty( $rStats['CHIPSTART'] ) ) && !empty( $rStats['PACE'] ) ) { ?>
							<section id="finisher-course">
								<h3><?php echo $course_stats_txt; ?></h3>
								<div class="fz_speedometer">
									<h4 class="big_hdr"><?php echo $rStats['FIRSTNAME'].' '.$rStats['LASTNAME']; ?> vs. <?php echo $field_txt; ?></h4>
									<script type="text/javascript" src="https://www.google.com/jsapi"></script>
									<script type="text/javascript">
										google.load("visualization", '1.1', {packages: ['corechart']});
										google.setOnLoadCallback(drawChart<?php echo $keyRunner;?>);
										function drawChart<?php echo $keyRunner;?>() {
											var data<?php echo $keyRunner;?> = google.visualization.arrayToDataTable([[
											{label: 'Distance'},
											{label: '<?php echo $rStats['FIRSTNAME'];?>', id: '<?php echo $rStats['FIRSTNAME'];?>', type: 'timeofday'},
											{label: '<?php echo $field_txt; ?>', id: 'Field', type: 'timeofday'},
											<?php if ( isset( $earnedCourseBadges ) && $earnedCourseBadges >= 1 ) : ?>
											{label: '<?php echo $course_highlights_txt; ?>', id: 'Field', type: 'timeofday'},
											{type: 'string', role: 'tooltip','p': { 'html': true} }
											<?php endif;?>
											],
												[0, 0, 0<?php if ( isset( $earnedCourseBadges ) && $earnedCourseBadges >= 1 ) : ?>, null, null<?php endif; ?>]
												<?php
												$highestpoint = 0;
												foreach ( $matsUsed as $keyMats => $mat_value ) {
													$mat_value_t	= trim( $mat_value );
													if ( $mexico_race ) {
														$matTime		= 'MP'. zeroise( $keyMats + 1, 2 ) .'TIME';
														$matAvgTime		= 'MP'. zeroise( $keyMats + 1, 2 ) .'_AVERAGEPACE';
														$matDistance	= 'MP'. zeroise( $keyMats + 1, 2 ) .'_DISTANCE';
													} else {
														$matTime		= $mat_value_t .'TIME';
														$matAvgTime		= $mat_value_t .'_AVERAGEPACE';
														$matDistance	= $mat_value_t .'_DISTANCE';
													}

													/* need to set up different pace ranges (here, sub 6 is fastest) */
													if ( !empty( $rStats[$matTime] ) ) {
														$paceExplode	= explode( ':', $rStats[$matAvgTime] );
														$paceFldExplode	= explode( ':', $subEventInfoToUse[$matAvgTime] );

														//Need to convert miles
														if ( stripos( $rStats[$matDistance], 'k' ) === false ) {
															if ( stripos( $rStats[$matDistance], 'half' ) === false ) {
																$distValue	= round( floatval( $rStats[$matDistance] ) * 1.60934, 1 );
															} else {
																$distValue	= '21.1';
															}
														} else {
															$distValue	= floatval($rStats[$matDistance]);
														}


														if(sizeof($paceExplode) < 3){
															echo ",['". $distValue ."', [0,". $paceExplode[0] .",". $paceExplode[1] ."], [00,". $paceFldExplode[0] .",". $paceFldExplode[1] ."]";
														}else{
															echo ",['". $distValue ."', [". $paceExplode[0] .",". $paceExplode[1] .",0], [". $paceFldExplode[0] .",". $paceFldExplode[1] .",00]";
														}

														//echo ",['". $distValue ."', [". $paceExplode[0] .",". $paceExplode[1] .",0], [". $paceFldExplode[0] .",". $paceFldExplode[1] .",00]";
														if ( isset($earnedCourseBadges) && $earnedCourseBadges >= 1 ) {
															echo ', null, null';
														}
														echo ']';

														if ( $highestpoint < $paceExplode[0] ) { $highestpoint = $paceExplode[0]; }
														if ( $highestpoint < $paceFldExplode[0] ) { $highestpoint = $paceFldExplode[0]; }
													}
												}
												if ( !empty( $rStats['PACE'] ) ) {
													$paceFExplode		= explode(':', $rStats['PACE'] );
													$paceFFldExplode	= explode(':', $subEventInfoToUse['PACE_AVG'] );

													if(sizeof($paceFExplode) < 3){
														echo ",[".$subEventInfoToUse["distance"].", [0,".$paceFExplode[0].",".$paceFExplode[1]."], [0,".$paceFFldExplode[0].",".$paceFFldExplode[1]."]";
													}else{
														echo ",[".$subEventInfoToUse["distance"].", [".$paceFExplode[0].",".$paceFExplode[1].",0], [".$paceFFldExplode[0].",".$paceFFldExplode[1].",0]";
													}

													//echo ",[".$subEventInfoToUse["distance"].", [".$paceFExplode[0].",".$paceFExplode[1].",0], [".$paceFFldExplode[0].",".$paceFFldExplode[1].",0]";
													if ( isset($earnedCourseBadges) && $earnedCourseBadges >= 1 ) {
														echo ', null, null';
													}
													echo ']';

													if ( $highestpoint < $paceFExplode[0] ) { $highestpoint = $paceFExplode[0]; }
													if ( $highestpoint < $paceFFldExplode[0] ) { $highestpoint = $paceFFldExplode[0]; }
												}
												//getCourseHighlights
												//$cHiglights = getCourseHighlights( $subEventInfoToUse["pk_sub_event_id"]);
												$highestpoint	= $highestpoint + 3;
												if ( isset( $earnedCourseBadges ) && $earnedCourseBadges >= 1 ) {
													foreach ( $badgeRInfo as $keyCH => $valueCH ) {

														// overwrite info except distance if lucky finisher situation happens (in a hurry! no time to explain!)
														if ( !empty( $luckyFinisher ) && !empty( $valueCH['lucky_course'] ) && $luckyFinisher[0] == $valueCH['lucky_course'] ) {
															$cFZBadges			= getFZBadges( $bd_value['lucky_course']);
															$main_badge_info	= $cFZBadges[$bd_value['lucky_course']];

															foreach ( $main_badge_info as $key => $value ) {
																if ( $key !== 'distance' ) {
																	$valueCH[$key]	= $value;
																}
															}

														}

														if ( !empty( $valueCH['distance'] ) && $valueCH['distance'] > 0 ) {
															echo ",[".$valueCH['distance'].", null,null, [0,".$highestpoint.",0], '";

															if ( !empty( $valueCH['image_display'] ) ) {
																$ch_text = $qt_status ? qtrans_split( $valueCH['display_text'] )[$qt_cur_lang] : $valueCH['display_text'];
																$ch_text = htmlspecialchars( $ch_text, ENT_QUOTES );
																echo "<center><img src=\"" . $valueCH['image_display'] . "\" alt=\"\" width=\"150\"><br/>" . $ch_text . "</center>']";
															} else {
																$ch_text = $qt_status ? qtrans_split( $valueCH['display_text'] )[$qt_cur_lang] : $valueCH['display_text'];
																$ch_text = htmlspecialchars( $ch_text, ENT_QUOTES );
																echo $ch_text . "']";
															}

														}
													}
												}
												?>
											]);
											var options = {
												legend: {position: 'bottom', maxLines: 2},
												hAxis: {title: '<?php echo trim($txt_distance); ?> (km)',  titleTextStyle: {color: '#333'}},
												vAxis: {title: '<?php echo $pace_txt; ?>', minValue: 0, format: 'mm:ss'},
												pointSize: 7,
												interpolateNulls: true,
												tooltip: {isHtml: true},
												series: {2: { pointSize: 12, pointShape: 'star', lineWidth: 0, areaOpacity: 0}}
											};

											var chart = new google.visualization.AreaChart(document.getElementById('chart_div<?php echo $keyRunner;?>'));
											chart.draw(data<?php echo $keyRunner;?>, options);
										}
									</script>
									<div id="chart_div<?php echo $keyRunner; ?>" style="width: 100%; height: 350px;"></div>
								</div>

							</section>
						<?php }

						/* SPEEDOMETER */
						if ( $partOfRelay != 1 && !empty( $rStats['CHIPSTART'] ) && !empty( $rStats['PACE'] ) ) { ?>

							<section id="finisher-speedometer">
								<h3><?php echo $speedometer_txt; ?></h3>
								<div class="fz_speedometer">
									<h4 class="big_hdr"><?php echo $how_fast_txt .' '. $rStats['FIRSTNAME'] .' '. $rStats['LASTNAME']; ?>?</h4>
									<div class="speed_graph">
										<div class="speed_graph_container">
											<ul class="fz_y_axis">
												<li><?php echo $screamin_txt; ?></li>
												<li><?php echo $jammin_txt; ?></li>
												<li><?php echo $strummin_txt; ?></li>
												<li><?php echo $on_beat_txt; ?></li>
												<li><?php echo $rippin_txt; ?></li>
											</ul>
											<?php
												// $matsCount = count( $matsUsed );
												$matsCount_plus1	= $matCount + 1;
											?>
											<ul class="fz_mats fz_mats_<?php echo $matsCount_plus1; ?>">
												<?php
													foreach ( $matsUsed as $keyMats => $mat_value ) {
														$mat_value_t	= trim( $mat_value );

														if ( $mexico_race ) {
															$matTime		= 'MP'. zeroise( $keyMats + 1, 2 ) .'TIME';
															$matAvgTime		= 'MP'. zeroise( $keyMats + 1, 2 ) .'_AVERAGEPACE';
															$matDistance	= 'MP'. zeroise( $keyMats + 1, 2 ) .'_DISTANCE';
														} else {
															$matTime		= $mat_value_t .'TIME';
															$matAvgTime		= $mat_value_t .'_AVERAGEPACE';
															$matDistance	= $mat_value_t .'_DISTANCE';
														}

														/* need to set up different pace ranges (here, sub 6 is fastest) */
														if ( !empty( $rStats[$matTime] ) ) {
															if ( strlen($rStats[$matAvgTime])  < 6 ) {
																$avgpace	= strtotime( '00:'. $rStats[$matAvgTime] );
															} else {
																$avgpace	= strtotime( $rStats[$matAvgTime] );
															}

															$displayeq	= getPaceRange( $avgpace );
															echo '<li>'. $displayeq .'</li>';

														}
													}

													if ( strlen( $rStats['PACE'] )  < 6 ) {
														$displayeq	= getPaceRange( strtotime( '00:'. $rStats['PACE'] ) );
													} else {
														$displayeq	= getPaceRange( strtotime( $rStats['PACE'] ) );
													}
													echo '<li>'. $displayeq .'</li>';

												?>
											</ul>
											<ul class="fz_mats fz_mats_<?php echo $matsCount_plus1; ?>">
												<?php
													foreach ( $matsUsed as $keyMats => $mat_value ) {

														$mat_value_t	= trim( $mat_value );

														if ( $mexico_race ) {
															$matTime		= 'MP'. zeroise( $keyMats + 1, 2 ) .'TIME';
															$matAvgTime		= 'MP'. zeroise( $keyMats + 1, 2 ) .'_AVERAGEPACE';
															$matDistance	= 'MP'. zeroise( $keyMats + 1, 2 ) .'_DISTANCE';
														} else {
															$matTime		= $mat_value_t .'TIME';
															$matAvgTime		= $mat_value_t .'_AVERAGEPACE';
															$matDistance	= $mat_value_t .'_DISTANCE';
														}

														if ( !empty( $rStats[$matTime] ) ) {
															echo '<li><span class="fz_label">'. $rStats[$matDistance] .'</span></li>';
														}
													}
												?><li><span class="fz_label">F</span></li>
											</ul>
											<ul class="fz_speedo_legend">
												<li><?php echo $screamin_txt; ?> <span class="fz_eqbar_legend fz_eqbar_red"></span></li>
												<li><?php echo $jammin_txt; ?> <span class="fz_eqbar_legend fz_eqbar_orange"></span></li>
												<li><?php echo $strummin_txt; ?> <span class="fz_eqbar_legend fz_eqbar_yellow"></span></li>
												<li><?php echo $on_beat_txt; ?> <span class="fz_eqbar_legend fz_eqbar_green"></span></li>
												<li><?php echo $rippin_txt; ?> <span class="fz_eqbar_legend fz_eqbar_ltblue"></span></li>
											</ul>

										</div>
									</div>
								</div>

							</section>
						<?php }

						//Get What's Next Info
						$usedWhatsNext	= array();

						if ( !empty( $subEventInfoToUse['fk_whats_next_id_1'] ) ) {
							$usedWhatsNext[1]	= $subEventInfoToUse['fk_whats_next_id_1'];
						}
						if ( !empty( $subEventInfoToUse['fk_whats_next_id_2'] ) ) {
							$usedWhatsNext[2]	= $subEventInfoToUse['fk_whats_next_id_2'];
						}
						if ( !empty( $subEventInfoToUse['fk_whats_next_id_3'] ) ) {
							$usedWhatsNext[3]	= $subEventInfoToUse['fk_whats_next_id_3'];
						}
						if ( !empty( $subEventInfoToUse['fk_whats_next_id_4'] ) ) {
							$usedWhatsNext[4]	= $subEventInfoToUse['fk_whats_next_id_4'];
						}

						$whatsNextInfo	= getFZWhatsNext($usedWhatsNext);
						//print_r($whatsNextInfo); ?>

						<?php if ( !empty( $whatsNextInfo ) ) : ?>
						<section id="finisher-whatsnext">
							<h3><?php echo $whats_next_txt; ?></h3>

							<div class="fz_whatsnext_wrapper">
								<ul class="fz_whatsnext">
								<?php
									if ( !empty( $subEventInfoToUse['fk_whats_next_id_1'] ) ) :
										$wn_1_info		= $whatsNextInfo[$subEventInfoToUse['fk_whats_next_id_1']];
										$wn_1_title		= $qt_status ? qtrans_split( $wn_1_info['display_title'] )[$qt_cur_lang] : $wn_1_info['display_title'];
										$wn_1_txt		= $qt_status ? qtrans_split( $wn_1_info['display_text'] )[$qt_cur_lang] : $wn_1_info['display_text'];
										$wn_1_link_txt	= $qt_status ? qtrans_split( $wn_1_info['link_text'] )[$qt_cur_lang] : $wn_1_info['link_text'];
								?>
									<li class="smaller_txt">
										<figure><img src="<?php echo $wn_1_info['image_display']; ?>" alt=""></figure>
										<h3><?php echo $wn_1_title; ?></h3>
										<p><?php echo $wn_1_txt; ?></p>
										<?php if ( !empty( $wn_1_info['link_text'] ) ) : ?>
										<p class="center"><a class="cta" href="<?php echo $wn_1_info['link']; ?>"><?php echo $wn_1_link_txt; ?></a></p>
										<?php endif;?>
									</li>
								<?php
									endif;
									if ( !empty( $subEventInfoToUse['fk_whats_next_id_2'] ) ) :
										$wn_2_info		= $whatsNextInfo[$subEventInfoToUse['fk_whats_next_id_2']];
										$wn_2_title		= $qt_status ? qtrans_split( $wn_2_info['display_title'] )[$qt_cur_lang] : $wn_2_info['display_title'];
										$wn_2_txt		= $qt_status ? qtrans_split( $wn_2_info['display_text'] )[$qt_cur_lang] : $wn_2_info['display_text'];
										$wn_2_link_txt	= $qt_status ? qtrans_split( $wn_2_info['link_text'] )[$qt_cur_lang] : $wn_2_info['link_text'];
								?>
									<li class="smaller_txt">
										<figure><img src="<?php echo $wn_2_info['image_display']; ?>" alt=""></figure>
										<h3><?php echo $wn_2_title; ?></h3>
										<p><?php echo $wn_2_txt; ?></p>
										<?php if ( !empty( $wn_2_info['link_text'] ) ) : ?>
										<p class="center"><a class="cta" href="<?php echo $wn_2_info['link']; ?>"><?php echo $wn_2_link_txt; ?></a></p>
										<?php endif;?>
									</li>
								<?php
									endif;
									if ( !empty( $subEventInfoToUse['fk_whats_next_id_3'] ) ) :
										$wn_3_info		= $whatsNextInfo[$subEventInfoToUse['fk_whats_next_id_3']];
										$wn_3_title		= $qt_status ? qtrans_split( $wn_3_info['display_title'] )[$qt_cur_lang] : $wn_3_info['display_title'];
										$wn_3_txt		= $qt_status ? qtrans_split( $wn_3_info['display_text'] )[$qt_cur_lang] : $wn_3_info['display_text'];
										$wn_3_link_txt	= $qt_status ? qtrans_split( $wn_3_info['link_text'] )[$qt_cur_lang] : $wn_3_info['link_text'];
								?>
									<li class="smaller_txt">
										<figure><img src="<?php echo $wn_3_info['image_display']; ?>" alt=""></figure>
										<h3><?php echo $wn_3_title; ?></h3>
										<p><?php echo $wn_3_txt; ?></p>
										<?php if ( !empty( $wn_2_info['link_text'] ) ) : ?>
										<p class="center"><a class="cta" href="<?php echo $wn_3_info['link']; ?>"><?php echo $wn_3_link_txt; ?></a></p>
										<?php endif;?>
									</li>
								<?php endif;?>
								</ul>
								<?php
									if ( !empty( $subEventInfoToUse['fk_whats_next_id_4'] ) ) :
										$wn_4_info		= $whatsNextInfo[$subEventInfoToUse['fk_whats_next_id_4']];
										$wn_4_title		= $qt_status ? qtrans_split( $wn_4_info['display_title'] )[$qt_cur_lang] : $wn_4_info['display_title'];
										$wn_4_txt		= $qt_status ? qtrans_split( $wn_4_info['display_text'] )[$qt_cur_lang] : $wn_4_info['display_text'];
										$wn_4_link_txt	= $qt_status ? qtrans_split( $wn_4_info['link_text'] )[$qt_cur_lang] : $wn_4_info['link_text'];
								?>
								<ul class="fz_whatsnext_bottom">
									<li class="smaller_txt">
										<figure><img src="<?php echo $wn_4_info['image_display']; ?>" alt=""></figure>
										<h3><?php echo $wn_4_title; ?></h3>
										<p><?php echo $wn_4_txt; ?></p>
										<?php if ( !empty( $whatsNextInfo[$subEventInfoToUse['fk_whats_next_id_4']]['link_text'] ) ) : ?>
										<p class="center"><a class="cta" href="<?php echo $wn_4_info['link']; ?>"><?php echo $wn_4_link_txt; ?></a></p>
										<?php endif;?>
									</li>
								</ul>
								<?php endif; ?>
							</div>

						</section>
						<?php endif; ?>

						<?php if ( $persona_tally[0] > 0 ) {
							//$badgeRInfo = getFZBadges( $badge_tally[1] );
							$personaRInfo = getFZPersona( $persona_tally[1] );

							if ( $persona_tally[0] == 1 ) {
								//$badges_txt = ' Badge(s)';
							} else {
								//$badges_txt = ' Badge(s)';
							}
							?>

						<section id="finisher-assessment">
							<h3><?php echo $rnr_assessment_txt; ?></h3>
							<div class="fz_assessment_wrapper">
							<?php
								$earned_persona_id	= '';
								$displayPa			= 0;

								foreach ( $personaRInfo as $keyPa => $pa_value ) :

									$qualForPersona			= unserialize( $pa_value['argument'] );
									$earnPa					= 0;
									$infoArray				= array();
									$infoArray['param']		= $qualForPersona[0]['param'];
									$infoArray['value']		= $qualForPersona[0]['value'];
									$infoArray['operator']	= $qualForPersona[0]['operator'];
									$infoArray['pct']		= $pct;
									$infoArray['ft']		= $rStats['FINISHTIME'];
									$infoArray['age']		= $rStats['AGE'];
									$infoArray['sex']		= $rStats['SEX'];
									$infoArray['eventdate']	= $subEventInfoToUse['date_start'];

									if ( count( $qualForPersona ) == 1) {

										$returnQualValue	= qualificationTimeOrPercent( $infoArray );
										$earnPa				= $returnQualValue;

									} elseif ( $qualForPersona >= 1 ) {

										$returnQualValue 		= qualificationTimeOrPercent( $infoArray );
										$infoArray2				= array();
										$infoArray2['param']	= $qualForPersona[1]['param'];
										$infoArray2['value']	= $qualForPersona[1]['value'];
										$infoArray2['operator']	= $qualForPersona[1]['operator'];
										$infoArray2['pct']		= $pct;
										$infoArray2['ft']		= $rStats['FINISHTIME'];
										$infoArray2['age']		= $rStats['AGE'];
										$infoArray2['sex']		= $rStats['SEX'];
										$returnQualValue2		= qualificationTimeOrPercent( $infoArray2 );

										if ( $returnQualValue == 1 && $returnQualValue2 == 1 ) {
											$earnPa	= $returnQualValue;
										}

									} else {
										//no qualification - automatic earn
										$earnPa	= 1;
									}

									if ( $earnPa == 1 && $displayPa == 0 ) :
										//print_r($pa_value);
										// check for social image based on naming convention
										$filename_persona					= '/'.$subEventInfoToUse['pk_sub_event_id'].'_persona_'.$pa_value['pk_persona_id'];
										$event_spec_persona_image			= $upload_dir['basedir'] .'/'. date( 'Y/m', strtotime( $subEventInfoToUse['date_start'] ) ) . $filename_persona .'.jpg';
										$event_spec_persona_image_display	= $upload_dir['baseurl'] .'/'. date( 'Y/m', strtotime( $subEventInfoToUse['date_start'] ) ) . $filename_persona .'.jpg';
										if ( file_exists( $event_spec_persona_image ) ) {
											//if file exists, overwrite social image
											$pa_value['image_social']	= str_replace ( 'www', 'cdn', $event_spec_persona_image_display);
										}

										/*$event_filename_persona = '/'.$subEventInfoToUse["fk_event_id"].'_persona_'.$bd_value['pk_ID'].'_event';
										$event_spec_persona_image2 = $upload_dir['basedir'] .'/'. date( 'Y/m', strtotime($subEventInfoToUse["date_start"])) .$event_filename_persona.'.jpg';
										$event_spec_persona_image_display2 = $upload_dir['baseurl'] .'/'. date( 'Y/m', strtotime($subEventInfoToUse["date_start"])).$event_filename_persona.'.jpg';
										if (file_exists($event_spec_persona_image2)) {
											$bd_value['image_social'] = str_replace ( 'www', 'cdn', $event_spec_persona_image_display2);
										}*/

										// check for image in appropriate language
										$pa_value['image_social']	= image_translation( $qt_lang, $pa_value['image_social'] );

										$infoToChange['pi']		= $pa_value['image_display'];
										$earned_persona_id		= $pa_value['pk_persona_id'];
										$earned_persona_title	= $qt_status ? qtrans_split( $pa_value['display_title'] )[$qt_cur_lang] : $pa_value['display_title'];
										$earned_persona_txt		= $qt_status ? qtrans_split( $pa_value['display_text'] )[$qt_cur_lang] : $pa_value['display_text'];
									?>
									<div class="fz_assessment smaller_txt">
										<figure><img src="<?php echo $pa_value['image_display']; ?>" alt=""></figure>
										<p>
										<?php
											echo $partOfRelay != 1 ? $rStats['FIRSTNAME'] .' '. $rStats['LASTNAME'] : $rStats['TEAMNAME'];

											if ( $qt_lang['lang'] ==  'es' ) {
												echo stripos($rStats['SEX'], 'f') !== false ? ' es una' : ' es un';
											} else {
												echo ' is a';
											}

										?></p>
										<h4 class="big_hdr"><?php echo $earned_persona_title; ?></h4>
										<p><?php echo $earned_persona_txt; ?></p>
										<div class="fz_badge_social">
											<figure class="hidden"><img src="<?php echo $pa_value['image_social']; ?>" alt=""></figure>
											<a href="<?php echo get_permalink( get_page_by_title( 'FB Share' )->ID ); ?>?type=persona&image=<?php echo $pa_value['image_social']; ?>&itemName=<?php echo $earned_persona_title; ?>&event=<?php echo $event_title; ?>" class="fb-share-persona"><span class="icon-facebook"></span></a>
											<a href="<?php echo $pa_value['image_social']; ?>" download><span class="icon-instagram"></span></a>
											<span class="post-widget" id="persona_share" data-fz-url="<?php echo $share_url; ?>" data-fz-title="<?php echo shareMessage( $qt_lang, $badge_title, 'persona', $hashtag ); ?>" data-pin-image="<?php echo $pa_value['image_social']; ?>"></span>
											<!-- <a href="https://twitter.com/intent/tweet?text=<?php echo urlencode( shareMessage( $qt_lang, $badge_title, 'persona', $hashtag ) );?>&via=RunRocknRoll&hashtags=<?php echo ltrim($hashtag, '#'); ?>&wrap_links‌​=true&url=<?php echo $share_url; ?>" target="_blank" class="twitter-share" data-item-shared="persona"><span class="icon-twitter"></span></a>
											<a class="pinterest-share" data-pin-do="buttonPin" data-pin-custom="true" href="//www.pinterest.com/pin/create/button/?url=<?php echo urlencode("http://".$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']); ?>&media=<?php echo urlencode($pa_value['image_social']); ?>&description=<?php echo urlencode( shareMessage( $qt_lang, $badge_title, 'persona', $hashtag ) );?>"><img src="<?php echo get_bloginfo('template_directory');?>/img/40x40_pinterest.png" alt="Share on Pinterest"></a> -->
										</div>
									</div>
									<div class="fz_offers">
									<?php
										$poCount	= 1;

										while ( $poCount <= 3 ) :
											$offer_title		= 'po_title_'.$poCount;
											$offer_text			= 'po_text_'.$poCount;
											$offer_image		= 'po_image_'.$poCount;
											$offer_link_text	= 'po_link_text_'.$poCount;
											$offer_link			= 'po_link_'.$poCount;

											if ( !empty( $pa_value[$offer_text] ) ) :
												// overwrite fields with translations
												$offer_title		= $qt_status ? qtrans_split( $pa_value[$offer_title] )[$qt_cur_lang] : $pa_value[$offer_title];
												$offer_text			= $qt_status ? qtrans_split( $pa_value[$offer_text] )[$qt_cur_lang] : $pa_value[$offer_text];
												$offer_link_text	= $qt_status ? qtrans_split( $pa_value[$offer_link_text] )[$qt_cur_lang] : $pa_value[$offer_link_text];
												?>
												<div class="fz_offer smaller_txt">
													<?php if ( !empty( $offer_link_text ) ) {
														echo "<a href='". $pa_value[$offer_link] ."''>";
													} ?>
													<figure><img src="<?php echo $pa_value[$offer_image];?>" alt=""></figure>
													<?php if (!empty($offer_link_text)) {
														echo "</a>";
													} ?>
													<h3><?php echo $offer_title;?></h3>
													<p><?php echo $offer_text;?></p>
													<?php if (!empty($offer_link_text)):?>
													<p><a href="<?php echo $pa_value[$offer_link];?>"><?php echo $offer_link_text;?></a></p>
													<?php endif;?>
												</div>
												<?php
											endif;

											$poCount++;

										endwhile;

									echo '</div>';
									$displayPa++;

									endif; // if ( $earnPa == 1 && $displayPa == 0 )

								endforeach; // endforeach
							?>

							</div>

						</section>
						<?php } ?>
						<?php
						//update text and images for badges and personas
						echo '<script>';
						if ( !empty( $infoToChange['pi'] ) ) {
							echo '$("#pPersona").attr(\'src\', \''.$infoToChange['pi'].'\');';
						}
						if ( isset( $infoToChange['bc'] ) && $infoToChange['bc'] > 1 ) {
							$badge_display_text = '';
							$badge_display_text = $partOfRelay != 1 ? $rStats['FIRSTNAME'] . ' ' . $rStats['LASTNAME'] : $rStats['TEAMNAME'];

							if ( $qt_cur_lang == 'es' ) {
								$badge_display_text	.= ' Gan&oacute; ';
							} else {
								$badge_display_text	.= ' Earned ';
							}

							$badge_display_text	.= $infoToChange['bc'] .' '. $badges_txt;

							echo '$("#badgeH4").text( "'.$badge_display_text.'" );
							$("#pBadge").attr( "src", "'.$infoToChange['bi'].'");';

						} elseif ( isset( $infoToChange['bc'] ) && $infoToChange['bc'] == 1 ) {
							//only update the badge image
							echo '$("#pBadge").attr( "src", "'. $infoToChange['bi'] .'" );';
						}
						echo '</script>';?>

						<?php
							$fz_image_logo	= '';

							if ( $subEventInfoToUse['image_logo'] != '' ) {
								$fz_image_logo	= $subEventInfoToUse['image_logo'];
							} elseif ( $evInfo->image_logo != '' ) {
								$fz_image_logo	= $evInfo->image_logo;
							}

							if ( $subEventInfoToUse['mfoto_id'] != '' || $fz_image_logo != '' ) :

								if ( isset($earned_badges_ids) ) $earned_badges_ids = implode(', ', $earned_badges_ids);
								?>
								<section id="finisher-downloads">
									<h3><?php echo $finisher_certificate_txt; ?></h3>
									<div class="fz-downloads">

										<?php if ( $fz_image_logo != '' && $subEventInfoToUse['pdf_link'] != '' ) { ?>

											<form name="FinCert" id="FinCert" method="post" action="<?php echo $subEventInfoToUse['pdf_link'];?>">
												<img src="<?php echo bloginfo('template_directory'); ?>/img/finisher-zone/icon-download.png" alt="Download your certificate">
												<h4 class="big_hdr"><?php echo $customize_cert_txt; ?></h4>
												<div class="fz-cert-checkboxes">
													<div class="fz-cert-checkbox"><input type="checkbox" name="finisher-results" checked="checked"> <?php echo $results_txt; ?></div>
													<?php if ( $badge_tally[0] > 0 ) { ?>
													<div class="fz-cert-checkbox"><input type="checkbox" name="finisher-badges" checked="checked"> <?php echo $earned_badges_txt; ?></div>
													<input type="hidden" name="earnedBadges" value="<?php echo $earned_badges_ids; ?>" />
													<?php }
														if ( $partOfRelay != 1 && !empty( $rStats['CHIPSTART'] ) ) { ?>
													<!-- <div class="fz-cert-checkbox"><input type="checkbox" name="finisher-course" checked="checked"> Course Stats</div> -->
													<!-- <div class="fz-cert-checkbox"><input type="checkbox" name="finisher-speedometer" checked="checked"> Speedometer</div> -->
													<?php }
														if ( $persona_tally[0] > 0 ) : ?>
													<div class="fz-cert-checkbox"><input type="checkbox" name="finisher-assessment" checked="checked"> <?php echo $rnr_assessment_txt; ?></div>
													<input type="hidden" name="earnedPersona" value="<?php echo $earned_persona_id; ?>" />
													<?php endif; ?>
													<div class="fz-cert-checkbox"><input type="checkbox" name="finisher-all" checked="checked"> <?php echo $all_txt; ?></div>
													<div class="clearfix"></div>
												</div>
												<input type="hidden" name="bib" value="<?php echo $bib; ?>">
												<input type="hidden" name="eventid" value="<?php echo $eventid; ?>">
												<input type="hidden" name="ParticipantName" value="<?php echo $rStats['FIRSTNAME'] . ' ' . $rStats['LASTNAME']; ?>" />
												<input type="hidden" name="DivisionPlace" value="<?php echo ($rStats['PLACEDIVISION_CHIP'] != '' ? $rStats['PLACEDIVISION_CHIP'] : '-');?>" />
												<input type="hidden" name="GenderPlace" value="<?php echo $rStats['PLACESEX_CHIP']; ?>" />
												<input type="hidden" name="OverallPlace" value="<?php echo $rStats['PLACEOALL_CHIP']; ?>" />
												<input type="hidden" name="FinishTime" value="<?php echo $rStats['FINISHTIME']; ?>" />
												<input type="hidden" name="BackgroundLocation" value="<?php echo $fz_image_logo; ?>" />
												<input type="hidden" name="language" value="<?php echo $qt_cur_lang; ?>" />
												<input type="hidden" name="language_status" value="<?php echo $qt_status; ?>" />
												<p class="center">
													<a href="#" class="cta"><?php echo $download_cert_txt; ?></a>
												</p>
											</form>

										<?php } else { ?>

											<!-- OLD METHOD, FUNCTIONAL -->
											<form name="FinCert" method="post" action="http://cert.marathonfoto.com/index.cfm?action=cert.rockitRequest">
												<input type="hidden" name="ParticipantName" value="<?php echo $rStats['FIRSTNAME'] .' '. $rStats['LASTNAME'];?>" size="75" />
												<input type="hidden" name="DivisionPlace" value="<?php echo $rStats['PLACEDIVISION_CHIP'];?>" size="75" />
												<input type="hidden" name="GenderPlace" value="<?php echo $rStats['PLACESEX_CHIP'];?>" size="75" />
												<input type="hidden" name="OverallPlace" value="<?php echo $rStats['PLACEOALL_CHIP'];?>" size="75" />
												<input type="hidden" name="FinishTime" value="<?php echo $rStats['FINISHTIME'];?>" size="75" />
												<input type="hidden" name="BackgroundLocation" value="<?php echo $subEventInfoToUse['mfoto_id'];?>" size="75" />
												<p class="center">
													<a href="#" class="cta" onclick="javascript:return false;">
														<input type="button" value="<?php echo $download_cert_txt; ?>" onclick="javascript:document.FinCert.submit();" />
													</a>
												</p>
											</form>

										<?php } ?>
									</div>

								</section>
								<?php

							endif;
						?>

					</div>
				</div>
			</section>
			<script>
				$(document).ready(function() {

					/**
					 * This part causes smooth scrolling using scrollto.js
					 * We target all a tags inside the nav, and apply the scrollto.js to it.
					 */
					$(".sticky-nav a, .scrolly").click(function( evn ) {
						evn.preventDefault();
						$('html, body').scrollTo( this.hash, this.hash );
					});

					/*$('main').stickem({
						container: '.offset240left',
					});*/

					/**
					 * This part handles the highlighting functionality.
					 * We use the scroll functionality again, some array creation and
					 * manipulation, class adding and class removing, and conditional testing
					 */
					var aChildren = $(".sticky-nav li").children(); // find the a children of the list items

					var aArray = []; // create the empty aArray
					for (var i=0; i < aChildren.length; i++) {
							var aChild = aChildren[i];
							var ahref = $(aChild).attr('href');
							aArray.push(ahref);
					} // this for loop fills the aArray with attribute href values

					$(window).scroll(function() {
						var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
						var windowHeight = $(window).height(); // get the height of the window
						var docHeight = $(document).height();

						for ( var i=0; i < aArray.length; i++ ) {
							var theID = aArray[i];
							var divPos = $(theID).offset().top; // get the offset of the div from the top of page
							var divHeight = $(theID).height(); // get the height of the div in question
							if (windowPos >= (divPos - 1) && windowPos < (divPos + divHeight -1 )) {
								$("a[href='" + theID + "']").addClass("nav-active");
							} else {
								$("a[href='" + theID + "']").removeClass("nav-active");
							}
						}

						if ( windowPos + windowHeight == docHeight ) {
							if ( !$(".sticky-nav li:last-child a").hasClass("nav-active") ) {
								var navActiveCurrent = $(".nav-active").attr("href");
								$("a[href='" + navActiveCurrent + "']").removeClass("nav-active");
								$(".sticky-nav li:last-child a").addClass("nav-active");
							}
						}
					});

				});

			</script>
		</section>
		<?php }

	} else {

		// No such runner
		echo '<section id="finisher-zone-results">
			<section class="wrapper grid_2 offset240left">
				<h2>Finisher Zone</h2>
				<p>'. $no_runner_txt .'</p>
			</section>
		</section>';
		get_correction_link( $qt_lang );

	}
	dbClose( $dbConnLink );
?>
