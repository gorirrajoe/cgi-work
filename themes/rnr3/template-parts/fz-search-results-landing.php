<section id="finisher-zone-landing">
	<section class="wrapper">

		<div class="fz_module fz_dropdown_module">

			<h3><?php $default_txt; ?></h3>

			<p><?php echo $select_event_txt; ?></p>

			<form action="">
				<select onchange="window.location.href=this.form.URL.options[this.form.URL.selectedIndex].value" name="URL">
					<option value="">Select Rock 'n' Roll Event</option>
					<?php
						$last12List = getLast12MonthsEvents();

						foreach ( $last12List as $keyL12 => $valueL12 ) {
							echo '<option value="?eventid='. $valueL12['pk_event_id'] .'">'. stripslashes( $valueL12['display_title'] ) .'</option>';
						}
					?>
				</select>
			</form>

		</div>

		<!-- <div class="fz_module fz_results_grid">
			<h3>Results By Year</h3>
			<p>Choose a year to view the Rock 'n' Roll event.</p>
			<div class="grid_4_special">
				<div class="column_special">
					<div class="inner_column">
						<h3>2015</h3>
						<p><a href="#" class="cta"><?php echo $view_results_txt; ?></a></p>
					</div>
				</div><div class="column_special">
					<div class="inner_column">
						<h3>2014</h3>
						<p><a href="#" class="cta"><?php echo $view_results_txt; ?></a></p>
					</div>
				</div><div class="column_special">
					<div class="inner_column">
						<h3>2013</h3>
						<p><a href="#" class="cta"><?php echo $view_results_txt; ?></a></p>
					</div>
				</div><div class="column_special">
					<div class="inner_column">
						<h3>2012</h3>
						<p><a href="#" class="cta"><?php echo $view_results_txt; ?></a></p>
					</div>
				</div>
			</div>
		</div>  -->

	</section>
</section>
