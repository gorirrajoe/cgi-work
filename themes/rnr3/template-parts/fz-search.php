<?php
	$dropdownlist = getDropdownList( $evInfo->rc_appid, $eventid );
	$genderList   = $dropdownlist['gender'];
	$divisionList = $dropdownlist['division'];
	$stateList    = $dropdownlist['state'];

	if ( isset( $dropdownlist['club'] ) && ( strpos( $evInfo->rc_appid, 'DUB' ) !== false || strpos( $evInfo->rc_appid, 'LIV' ) !== false ) ) {
		$clubList    = $dropdownlist['club'];
	}
	if ( isset( $dropdownlist['military'] ) ) {
		$militaryList    = $dropdownlist['military'];
	}
	$subEInfo     = getFZSubEvents( $eventid, 1 );

	//$allBadges = getAllBadgeIDsForEvent($eventid);
	//$allBadgeInfo = getFZBadgesNoQual($allBadges, $eventid);
	$liveResultsOn = '';
	if ( property_exists( $evInfo, 'liveresults' ) ) {
		$liveResultsOn = $evInfo->liveresults;
	}
	if ( $liveResultsOn > 0 ) :
		// Determine the Live Results JS src
		if ( $liveResultsOn == 1 ) {
			$liveResultsJsSrc	= 'http://track.rtrt.me/js/embed.js?appid=4ce6c15a7c91c45c23ae58d0&amp;immediate=1';
		} elseif ( $liveResultsOn == 2 ) {
			$liveResultsJsSrc	= 'http://track.rtrt.me/js/embed.js?appid=524cebacf3f6a412b81a8e00';
		} elseif ( $liveResultsOn == 3 ) {
			$liveResultsJsSrc	= 'http://track.rtrt.me/js/embed.js?appid=504f6ef9ac0fd063be485260';
		}
		?>
		<section id="finisher-zone-search">
			<section class="wrapper">

				<h2>Live Results</h2>

				<div id="rt-app">The live race results view requires Javascript</div>
				<script type="text/javascript" src="<?php echo $liveResultsJsSrc; ?>"></script>

			</section>
		 </section>

		<?php else : ?>

		<section id="finisher-zone-search">
			<section class="wrapper">

				<h2><?php echo $search_results_txt; ?></h2>

				<?php if ( $evInfo->published == 1 ) { ?>
					<div class="fz_event_hdr">
						<?php
							if ( have_posts() ) : while ( have_posts() ) : the_post();
								the_content();
							endwhile; endif;
						?>
						<?php
						if ( !empty( $evInfo->image_logo ) ) :
							echo '<figure><img src="'.$evInfo->image_logo.'"></figure>';
						else :
							?>
						<h3><?php echo stripslashes($evInfo->display_title);?></h3>
						<?php endif;?>
						<p><?php
						//date formatting - display
						if ( $qt_lang['lang'] != 'en' ) {
							$startDateEx	= explode( '-', $evInfo->date_start );

							echo $startDateEx[2] .' '. ucfirst( get_month_by_language( $qt_lang['lang'], ltrim( $startDateEx[1], '0' ) ) ) .' '. $startDateEx[0];

							if ( $evInfo->date_start != $evInfo->date_end ) {
								$endDateEx = explode( '-', $evInfo->date_end );
								echo ' &ndash; '. $endDateEx[2].' '.ucfirst( get_month_by_language( $qt_lang['lang'], ltrim( $endDateEx[1], '0' ) ) ) .' '. $endDateEx[0];
							}
						} else {
							echo date( 'M d, Y', strtotime( $evInfo->date_start ) );
							if ( $evInfo->date_start != $evInfo->date_end ) {
								echo ' &ndash; '. date( 'M d, Y', strtotime( $evInfo->date_end ) );
							}
						}?></p>
						<p><?php echo $quick_results_txt; ?></p>
					</div>
					<div class="form_narrow fz_module">

						<form method="GET" action="" id="fz-search">

							<input type="hidden" name="resultspage" value="1">
							<input type="hidden" name="perpage" value="25">
							<input type="hidden" name="eventid" value="<?php echo $eventid; ?>">
							<?php /* <input type="hidden" name="yearid" value="<?php echo $yearid; ?>"> */ ?>

							<fieldset>
								<ol>
									<li><input type="text" placeholder="<?php echo $first_name_txt; ?>" value="" id="firstname" name="firstname" class="field" pattern=".{3,}" title="3 characters minimum"></li>
									<li><input type="text" placeholder="<?php echo $last_name_txt; ?>" value="" id="lastname" name="lastname" class="field"></li>
									<li><input type="text" placeholder="<?php echo $runner_number_txt; ?>" value="" id="bib" name="bib" class="field"></li>
									<li><input type="text" placeholder="<?php echo $city_txt; ?>" value="" id="city" name="city" class="field"></li>
									<li>
										<select name="gender" id="gender">
											<option value="">- <?php echo $gender_txt; ?> -</option>
											<?php
												foreach ( $genderList as $keyG => $valueG ) {
													echo '<option value="'. $valueG .'">'. $valueG .'</option>';
												}
											?>
										</select>
									</li>
									<li>
										<select name="state" id="state">
											<option value="">- <?php echo $state_txt; ?> -</option>
											<?php
												foreach ( $dropdownlist['state'] as $keyS => $valueS ) {
													echo '<option value="'. $valueS .'">'. $valueS .'</option>';
												}
											?>
										</select>
									</li>
									<li>
										<?php /* query subevents table */ ?>
										<select name="subevent_id" id="subevent_id">
											<option value="">- <?php echo trim( $txt_distance ); ?> -</option>
											<?php
											foreach ($subEInfo as $keySE => $subEI) {
												$distance = $qt_status ? qtrans_split( $subEI['display_title'] )[$qt_cur_lang] : $subEI['display_title'];
												echo '<option value="'.$subEI['ra_id'].'">'.$distance.'</option>';
											}
											?>
										</select>
									</li>
									<li>
										<?php /* query distinct divisions from results database */ ?>
										<select name="division" class="ageclass" id="division">
											<option value="">- <?php echo $division_txt; ?> -</option>
											<?php
												foreach ( $dropdownlist['division'] as $keyD => $valueD ) {
													echo '<option value="'. $valueD .'">'. $valueD .'</option>';
												}
											?>
										</select>
									</li>
									<?php if ( isset( $clubList ) && !empty( $clubList ) ) :?>
									<li>
										<select name="club" id="club">
											<option value="">- <?php echo $club_txt; ?> -</option>
											<?php
												foreach ( $clubList as $keyC => $valueC ) {
													echo '<option value="'. $valueC .'">'. $valueC .'</option>';
												}
											?>
										</select>
									</li>
									<?php endif;?>
									<?php if ( isset( $militaryList) && !empty( $militaryList) ) :?>
									<li>
										<select name="military" id="military">
											<option value="">- Military -</option>
											<?php
												foreach ( $militaryList as $keyM => $valueM ) {
													echo '<option value="'. $valueM .'">'. $valueM .'</option>';
												}
											?>
										</select>
									</li>
									<?php endif;?>
								</ol>
							</fieldset>
							<fieldset>
								<button type="submit"><?php echo $search_txt; ?></button>
								<?php get_correction_link($qt_lang); ?>
							</fieldset>
						</form>
						<?php
						$otherRLinks = getFZOtherResults($eventid);
						if ( isset( $otherRLinks ) && !empty( $otherRLinks ) ) {
							echo 'Other Results<br/>';

							foreach ( $otherRLinks as $oresult ) {
								echo '<a href="'. $oresult['link'] .'" target="_blank">'. stripslashes( $oresult['link_text'] ) .'</a><br/>';
							}

						}
						/*if ( false === ( $badges_results = get_transient( 'badges_'.$eventid ) ) ) {
							$badges_query   = 'SELECT DISTINCT(display_title),display_text,image_display,pk_ID FROM wp_fz_badges WHERE fk_event_id = '.$eventid.' GROUP BY display_title ORDER BY CASE WHEN display_order = 0 THEN 1 ELSE 0 END, display_order LIMIT 4';
							$badges_results  = $wpdb->get_results( $badges_query );
							$transient_expiration = '60 * MINUTE_IN_SECONDS';
							set_transient( 'badges_'.$eventid, $badges_results, $transient_expiration );
						}

						if ( $badges_results != '' ) {
							echo 'What badges did you earned?<br/><br/>';
							echo '<div class="grid_4_special">';
							foreach ( $badges_results as $badge ) {
								echo '<div class="column_special">
									<figure><img src="'. $badge->image_display .'" alt=""></figure>
									<h3>'. stripslashes( $badge->display_title ) .'</h3>
									</div>';
							}
							echo '</div>';
						}*/
					?>
					</div>
				<?php } else { // else if results are not published/available ?>

					<div class="fz_event_hdr">
						<p><?php echo $results_not_available; ?></p>
					</div>

				<?php } // end if ( $evInfo->published == 1 ) ?>
			</section>
		</section>

	<?php endif;?>

	<?php if ( $evInfo->published == 1 ) : ?>

		<section id="finisher-zone-leaderboard">
			<section class="wrapper">

				<h2><?php echo $leaderboards_txt; ?></h2>

				<?php
					// Get Top Tens
					allTopTenForEvent_display( $qt_lang, $evInfo->rc_appid, $eventid, $yearid );
				?>

			</section>
		</section>

	<?php endif; ?>
