<?php
	/* Template Name: Welcome Locations - Landing */

	$market	= get_market2();

	require_once 'inc/grd-functions.php';

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info	= rnr3_get_event_info( $market );
	}

	$page_id		= get_the_ID();
	$current_date	= date( 'YmdH' );
	$current_phase	= '';


	/**
	 * get all post metadata
	 */
	$all_post_meta = get_meta( $page_id );
	// print_r( $all_post_meta );


	/**
	 * get phase (based on date)
	 * convert to gmt: append '07' because san diego is PDT (gmt -7) in summer
	 */
	// $phase0_date	= date( 'Ymd', $all_post_meta['_rnr3_phase0_start'] ) . '07';	// teaser
	// $phase1_date	= date( 'Ymd', $all_post_meta['_rnr3_phase1_start'] ) . '07';	// day of
	// $phase2_date	= date( 'Ymd', $all_post_meta['_rnr3_phase2_start'] ) . '07';	// upsell

	// $phase = isset( $_GET['phase'] ) ? $_GET['phase'] : -1;

	// if( isset( $_GET['phase'] ) ) {
	// 	$current_phase_num	= $_GET['phase'];
	// 	$current_phase		= 'phase' . $_GET['phase'];
	// } elseif( $current_date >= $phase0_date && $current_date < $phase1_date ) {
	// 	$current_phase_num	= 0;
	// 	$current_phase		= 'tease';
	// } elseif( $current_date >= $phase1_date && $current_date < $phase2_date ) {
	// 	$current_phase_num	= 1;
	// 	$current_phase		= 'promo';
	// 	// set_interstitial_cookie_timeout( 1 );
	// } elseif( $current_date >= $phase2_date ) {
	// 	$current_phase_num	= 2;
	// 	$current_phase		= 'upsell';
	// }

	$current_phase_num	= 1;
	$current_phase		= 'promo';


	get_header( 'special' );

	$qt_lang = rnr3_get_language();
	include 'languages.php';

	// get the rest of the page metadata needed
	/*if( $current_phase	== 'tease' ) {
		$marquee_desktop	= $all_post_meta['_rnr3_marquee01_big'];
		$marquee_mobile		= $all_post_meta['_rnr3_marquee01_small'];
	} else {
		$marquee_desktop	= $all_post_meta['_rnr3_marquee2_big'];
		$marquee_mobile		= $all_post_meta['_rnr3_marquee2_small'];
	}*/

?>

<main role="main" id="main" class="no-hero grd grd-phase__<?php echo $current_phase; ?>">
	<style>
		.grd-marquee-red {
			background-image: url( <?php echo $all_post_meta['_rnr3_marquee01_big']; ?> );
		}
	</style>

	<section class="wrapper">
		<div class="grd-marquee">
			<div class="grd-marquee-white">
				<h1>
					<span class="kicker bigtext-exempt kicker-info2">We invite you to explore</span>
					<span class="headline kicker-info3">the Rock 'n' Roll<br>Marathon Series</span>
				</h1>

				<div class="grd-marquee-white__info bigtext-exempt">
					<?php
						$fineprint = array_key_exists( '_rnr3_hdr_fineprint_txt', $all_post_meta ) ? qtrans_split( $all_post_meta['_rnr3_hdr_fineprint_txt'] ) : '';

						if( $fineprint != '' ) {
							echo apply_filters( 'the_content', $fineprint[$qt_lang['lang']] );
						}
					?>
				</div>
			</div>
			<div class="grd-marquee-red_arrow"></div>
			<div class="grd-marquee-red"></div>
		</div>


		<?php
			/**
			 * desktop filterbar (>960px)
			 * will get sticky to the top when scrolling past it (appends .grd-filterbar__desktop--fixed)
			 * when fixed, hidden countdown list item will appear
			 */

			$aboutremix_txt = array_key_exists( '_rnr3_about_remix_txt', $all_post_meta ) ? qtrans_split( $all_post_meta['_rnr3_about_remix_txt'] ) : '';


			if( $current_phase_num != 2 ) { ?>

				<div class="grd-filterbar__desktop-placeholder"></div>

				<ul class="grd-filterbar__desktop">

					<li class="grd-filterbar-item grd-filterbar-item-sortby">
						<span class="grd-filterbar__desktop-label grd-filterbar-label"><?php echo $filterby_txt; ?>: </span>
					</li>
					<li class="grd-filterbar-item">
						<a class="grd-filterbar-toggle grd-filterbar__desktop-toggle" href="#"><?php echo $txt_date; ?><img class="grd-filterbar__desktop-svg" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/grd/date.svg"><span class="icon-play"></span></a>
						<div style="display: none;" class="grd-filterbar__desktop-content grd-filterbar-content">
							<?php echo grd_get_quarters( $qt_lang, $event_info ); ?>
						</div>
					</li>
					<li class="grd-filterbar-item">
						<a class="grd-filterbar-toggle grd-filterbar__desktop-toggle" href="#"><?php echo $city_txt; ?><img class="grd-filterbar__desktop-svg" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/grd/city.svg"><span class="icon-play"></span></a>
						<div style="display: none;" class="grd-filterbar__desktop-content grd-filterbar-content__city grd-filterbar-content">
							<?php echo grd_get_cities( $qt_lang, false ); ?>
						</div>
					</li>
					<li class="grd-filterbar-item">
						<a class="grd-filterbar-toggle grd-filterbar__desktop-toggle" href="#"><?php echo $txt_distance; ?><img class="grd-filterbar__desktop-svg" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/grd/distance.svg"><span class="icon-play"></span></a>
						<div style="display: none;" class="grd-filterbar__desktop-content grd-filterbar-content">
							<?php echo grd_get_distances( $qt_lang, $event_info ); ?>
						</div>
					</li>
					<li class="grd-filterbar-item">
						<a class="grd-filterbar-toggle grd-filterbar__desktop-toggle grd-filterbar-toggle__remix" href="#"><span><?php echo $grd_about_remix_txt; ?></span><img class="grd-filterbar__desktop-svg" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/grd/aboutremix.svg"><span class="icon-play"></span></a>
						<div style="display: none;" class="grd-filterbar__desktop-content grd-filterbar-content grd-filterbar__desktop-content__remix">
							<?php
								if( $aboutremix_txt != '' ) {

									echo apply_filters( 'the_content', $aboutremix_txt[$qt_lang['lang']] );

								}
							?>
						</div>
					</li>
				</ul>

			<?php }

			/**
			 * events for promo
			 * fn: grdpromo_findrace will
			 */
		?>
		<div class="grd-cards">
			<?php
				// dev sanity check -- confirm current phase
				echo '<!-- '. $current_phase .' -->';

				if( $current_phase == 'tease' || $current_phase_num == 0 ) {

					$findrace_content = grdpromo_findrace( 0, $qt_lang, $event_info, false );
					echo $findrace_content;

				} elseif( $current_phase == 'promo' || $current_phase_num == 1 ) {

					$findrace_content = grdpromo_findrace( 1, $qt_lang, $event_info );
					echo $findrace_content;

					/*if( empty( $_GET ) && $qt_lang['lang'] == 'en' ) {
						get_interstitial( 1, 'grd_interstitial_1' );
					}*/

				} elseif( $current_phase == 'upsell' ) {

					// $findrace_content = grdpromo_findrace( 2, $qt_lang, $event_info );
					// echo $findrace_content;

					/*if( empty( $_GET ) && $qt_lang['lang'] == 'en' ) {
						get_interstitial( 2, 'grd_interstitial_2' );
					}*/

				}
			?>
		</div>


		<?php
			/**
			 * mobile countdown bar (<960px)
			 * will appear when scrolling to the top of the cards (appends .grd-countdown-sticky--fixed)
			 */
		?>
		<div class="grd-countdown-sticky"></div>


		<?php
			/**
			 * set up inline surveygizz form
			 * all events will call this form
			 * don't show it on promo day
			 */
			if( $current_phase_num != 1 ) {

				echo grd_getsurveygizmo_form( 0, $all_post_meta, $qt_lang );

			}


			/**
			 * mobile filterbar (<960px)
			 * sticks to the bottom of the screen
			 */

			if( $current_phase_num != 2 ) { ?>

				<ul class="grd-filterbar__mobile">
					<li class="grd-filterbar-item">
						<a class="grd-filterbar-toggle grd-filterbar-toggle__remix" href="#"><img class="grd-filterbar__mobile-svg" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/grd/aboutremix.svg"><?php echo $grd_about_remix_short_txt; ?></a>

						<div style="display: none;" class="grd-filterbar__mobile-content grd-filterbar-content">
							<?php
								if( $aboutremix_txt != '' ) {

									echo apply_filters( 'the_content', $aboutremix_txt[$qt_lang['lang']] );

								}
							?>
						</div>
					</li>
					<li class="grd-filterbar-item">
						<a class="grd-filterbar-toggle" href="#"><img class="grd-filterbar__mobile-svg" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/grd/date.svg"><?php echo $txt_date; ?></a>

						<div style="display: none;" class="grd-filterbar__mobile-content grd-filterbar-content">
							<?php echo grd_get_quarters( $qt_lang, $event_info ); ?>
						</div>
					</li>
					<li class="grd-filterbar-item">
						<a class="grd-filterbar-toggle" href="#"><img class="grd-filterbar__mobile-svg" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/grd/city.svg"><?php echo $city_txt; ?></a>

						<div style="display: none;" class="grd-filterbar__mobile-content grd-filterbar-content__city grd-filterbar-content">
							<?php echo grd_get_cities( $qt_lang, false ); ?>
						</div>
					</li>
					<li class="grd-filterbar-item">
						<a class="grd-filterbar-toggle" href="#"><img class="grd-filterbar__mobile-svg" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/grd/distance.svg"><?php echo $txt_distance; ?></a>

						<div style="display: none;" class="grd-filterbar__mobile-content grd-filterbar-content">
							<?php echo grd_get_distances( $qt_lang, $event_info ); ?>
						</div>
					</li>
				</ul>

			<?php }
		?>

	</section>
</main>


<?php
	get_footer( 'series' );
?>
