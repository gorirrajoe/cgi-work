<?php
	/* Template Name: Partners */
	if(get_current_blog_id() <= 1){
		get_header('special');
	} else {
		get_header();
	}

	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	// translations
	$qt_lang = rnr3_get_language();
	include 'languages.php';

?>
	<main role="main" id="main" class="partners">
		<section class="wrapper grid_2 offset240left">
			<?php get_sidebar(); ?>

			<div class="column">
				<?php
					if (have_posts()) : while (have_posts()) : the_post();
						if( $post->post_content != '' ) {
							echo '<h2>'. $overview_txt .'</h2>
							<div class="content">';
								the_content();
							echo '</div>';
						}
					endwhile; endif;

					if ( false === ( $partners_query = get_transient( 'partners_query_results' ) ) ) {
						$args = array(
							'post_status'		=> 'publish',
							'post_type'			=> 'partner',
							'posts_per_page'	=> -1,
							'meta_key'			=> '_rnr3_priority',
							'orderby'			=> 'meta_value_num title',
							'order'				=> 'ASC',
							'has_password'		=> false
						);
						$partners_query = new WP_Query( $args );
						set_transient( 'partners_query_results', $partners_query, 5 * MINUTE_IN_SECONDS );
					}

					if( $partners_query->have_posts() ) {
						echo '<div class="grid_3_special">';
							while( $partners_query->have_posts() ) {
								$partners_query->the_post();

								echo '<div class="column_special">
									<figure>
										<a href="'.get_permalink().'">'.get_the_post_thumbnail( get_the_ID(), 'full' ).'</a>
									</figure>
								</div>';

							}
						echo '</div>';
					}
					wp_reset_postdata();
				?>
			</div>
		</section>

	</main>

	<script>
		$(document).ready(function(){
			/**
			 * This part causes smooth scrolling using scrollto.js
			 * We target all a tags inside the nav, and apply the scrollto.js to it.
			 */
			$("#secondary a, .scrolly").click(function(evn){
				evn.preventDefault();
				$('html,body').scrollTo(this.hash, this.hash);
			});
		});
	</script>

<?php get_footer('series'); ?>
