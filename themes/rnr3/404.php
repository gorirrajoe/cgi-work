<?php
  if ( get_current_blog_id() <= 1 ) {
    get_header('series-home');
  } elseif ( '/tempo/' === get_blog_details()->path ) {
    get_header('oneoffs');
  } else {
    get_header();
  }
  if ( false === ( $all_events = get_transient( 'all_events_data' ) ) ) {
    $all_events = rnr3_get_all_events();
  }
  $qt_lang = rnr3_get_language();

?>

  <!-- main content -->
  <main role="main" id="main">
    <section class="wrapper">
      <h2>Page Not Found</h2>
      <p>The content or page you requested does not exist or has moved to a new location. Please use the current event list below to locate it.</p>
      <p>Thank You,<br />Run Rock 'n' Roll Marathon Series</p>

    </section>

    <?php rnr3_findrace( $all_events, $qt_lang ); ?>

  </main>
<?php if ( get_current_blog_id() <= 1 ) {
  get_footer('series');
} else {
  get_footer();
} ?>
