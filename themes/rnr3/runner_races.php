<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	ini_set('memory_limit', '-1');
	require_once 'functions_fz.php';
	
	//ini_set('max_execution_time', 300);
	
	//RaceIt request Info
	if(isset($_REQUEST['whodoyoulove']) AND $_REQUEST['whodoyoulove'] == 'manchesterunited'){
		if(isset($_REQUEST['evil']) AND is_numeric($_REQUEST['evil']) AND $_REQUEST['evil'] > 0){
			$event_available = getPublishedEvent($_REQUEST['evil']);
			//print_r($event_available);
			if(!empty($event_available)){
				//loop through array
				foreach ($event_available as $eid => $rcaid){
					echo $rcaid."_".$eid." Begin<br/><br/><br/>";
					getAllFinishers($eid,$rcaid);
					echo "<br/><br/><br/>".$rcaid."_".$eid." End<br/><br/><br/>";
				}
			}
		}elseif(isset($_REQUEST['last']) AND $_REQUEST['last'] = '1' AND (!isset($_REQUEST['allP']) OR $_REQUEST['allP'] != 'Y')){
			$event_available = getLastPublishedEvent();
			//print_r($event_available);
			if(!empty($event_available)){
				//loop through array
				foreach ($event_available as $eid => $rcaid){
					echo $rcaid."_".$eid." Begin<br/><br/><br/>";
					getAllFinishers($eid,$rcaid);
					echo "<br/><br/><br/>".$rcaid."_".$eid." End<br/><br/><br/>";
				}
			}
		}elseif(isset($_REQUEST['allP']) AND $_REQUEST['allP'] == 'Y'){
			$event_available = getAllPublishedEvents();
			//print_r($event_available);
			if(!empty($event_available)){
				//loop through array
				foreach ($event_available as $eid => $rcaid){
					echo $rcaid."_".$eid." Begin<br/><br/><br/>";
					getAllFinishers($eid,$rcaid);
					echo "<br/><br/><br/>".$rcaid."_".$eid." End<br/><br/><br/>";
				}
			}
		}else{
			echo 'Nothing to add';
		}
	}else{
		echo 'Nothing to see here';
	}
	
	
	
function getAllFinishers($eventid,$rcappid){
	$subEInfo = getSubEventInfo($eventid);
	print_r($subEInfo);
	$match_fields = array("FIRSTNAME","LASTNAME","EMAIL","DOB","SEX");
	$insert_fields = array("FIRSTNAME","LASTNAME","EMAIL","DOB","SEX","AGE","CITY","STATE","COUNTRY","ZIP");
	//connect to db
	$table_name = $rcappid."_".$eventid;
	$selectStatement = 'SELECT BIB,FIRSTNAME,LASTNAME,SEX,AGE,DOB,CITY,STATE,COUNTRY,ZIP,EMAIL,RACEID,FINISHTIME,PACE FROM '.RESULTSDBNAME.'.'.$table_name .' WHERE ( (FINISHTIME IS NOT NULL AND PLACEOALL_CHIP IS NOT NULL) OR ( TEAMNAME IS NOT NULL AND RELAYPLACEOALL IS NOT NULL ) ) AND DQ == 0';
	$dbLinkConnection = dbConnect('results');
	$result1 = mysqli_query( $dbLinkConnection, $selectStatement );
	$affected_rows = mysqli_affected_rows( $dbLinkConnection );
	if($affected_rows == 0){
		//no records
		echo 'no records';
	}else{
		//get customer
		while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
			//print_r($row1);
			$custSearch = '';
			$cInsert = '';
			$values = '';
			$runnerbib = 0;
			$eInfoArray = array();
			$isEmail = 0;
			foreach ($row1 as $key => $value){
				//echo $key . " = ".$value;
				if(in_array($key,$insert_fields) AND !empty($value)){
					if(strlen($cInsert) > 1){
						$cInsert .= ",";
					}
					if(strlen($values) > 1){
						$values .= ",";
					}
					if(in_array($key,$match_fields)){
						if(strlen($custSearch) > 1){
							$custSearch .= " AND ";
						}
						$custSearch .= $key ." = ".sprintf('"%s"',$value);
				    }
				    $values .= sprintf('"%s"',$value);
				    $cInsert .= $key;
				    if($key == 'EMAIL'){
				    	$isEmail = 1;
				    }
				}
				if($key == "BIB"){
					$runnerbib = $value;
				}
				if($key == "RACEID"){
					$rcsubid = $value;
				}
				if($key == "FINISHTIME"){
					$rcfinish = $value;
				}
				if($key == "PACE"){
					$rcpace = $value;
				}
			}
			//echo $custSearch."<br/>\n";
			if($isEmail != 1){
				$fakeEmail = $rcappid."@defaultemail.com";
				if(strlen($cInsert) > 1){
					$cInsert .= ",";
				}
				if(strlen($values) > 1){
					$values .= ",";
				}
				if(strlen($custSearch) > 1){
					$custSearch .= " AND ";
				}
				$custSearch .= "EMAIL = ".sprintf('"%s"',$fakeEmail);
				
				$values .= sprintf('"%s"',$fakeEmail);
				$cInsert .= "EMAIL";
			}
			$customerInfo = array();
		    $customerInfo['info'] = $custSearch;
		    $customerInfo['keys'] = $cInsert;
		    $customerInfo['values'] = $values;
		    $customer_id = seeIfCustomerExistsOrInsert($customerInfo,$dbLinkConnection);
		    //echo $customer_id;
		    //$subEInfo['distance']
		    //$subEInfo['date_start']
		    if(array_key_exists ($rcsubid, $subEInfo)){
		    	$rSubEInfo = $subEInfo[$rcsubid];
		    }else{
		    	$rSubEInfo['distance'] = 0;
		    	$rSubEInfo['start'] = '';
		    	$rSubEInfo['end'] = '';
		    	$rSubEInfo['ft_avg'] = '';
		    	$rSubEInfo['p_avg'] = '';
		    }
		    if($customer_id != 0 AND $runnerbib > 0){
		    	$regId = seeIfAlreadyRegOrInsert($customer_id,$eventid,$runnerbib,$rcsubid,$rSubEInfo,$table_name,$dbLinkConnection,$rcfinish,$rcpace);
		    }
		}
	}
	dbClose( $dbLinkConnection );
}


function seeIfCustomerExistsOrInsert($customerInfo, $dbConnection){
	//connect to db
	$table_name = "wp_fz_runners";
	$selectStatement = 'SELECT pk_customer_id FROM '.RESULTSDBNAME.'.'.$table_name .' WHERE '.$customerInfo['info'];
	//echo $selectStatement;
	//$dbLinkConnection = dbConnect();
	$dbLinkConnection = $dbConnection;
	$result1 = mysqli_query( $dbLinkConnection, $selectStatement );
	$affected_rows = mysqli_affected_rows( $dbLinkConnection );
	if($affected_rows == 1){
		//return customer_id
		while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
			echo "Customer exists <br/>";
			$customer_id = $row1['pk_customer_id'];
		}
	}elseif($affected_rows == 0){
		//add customer and return id
		echo "Customer does not exist <br/>";
		$create_query = "INSERT INTO ".RESULTSDBNAME.".".$table_name ." (".$customerInfo['keys'].") VALUES (".$customerInfo['values'].")";
		//echo $create_query;
		if(mysqli_query($dbLinkConnection,$create_query)) {
    		//insert id
    		$customer_id = mysqli_insert_id ( $dbLinkConnection );
    		echo "Customer created - ".$customer_id."<br>";
    	}else{
    		echo "Insert failed<br>";
    		echo mysql_errno($dbLinkConnection) . ": " . mysql_error($dbLinkConnection) . "\n";
    	}
	}else{
		//too many records to determine or error
		$customer_id = 0;
	}
	//dbClose( $dbLinkConnection );
	return $customer_id;
}

function seeIfAlreadyRegOrInsert($custId,$eventId,$bib,$subeventRaceId,$subEventRaceInfo,$table_id,$dbConnection,$finishtime = NULL, $pace = NULL){
	//connect to db
	//print_r($subEventRaceInfo);
	$table_name = "wp_fz_runner_races";
	$selectStatement = 'SELECT pk_registration_id FROM '.RESULTSDBNAME.'.'.$table_name .' WHERE fk_customer_id = "'.$custId.'" AND fk_event_id = "'.$eventId.'" AND BIB = "'.$bib.'"';
	//$dbLinkConnection = dbConnect();
	$dbLinkConnection = $dbConnection;
	$result1 = mysqli_query( $dbLinkConnection, $selectStatement );
	$affected_rows = mysqli_affected_rows( $dbLinkConnection );
	if($affected_rows == 1){
		//return customer_id
		while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
			echo "Registration exists <br/>";
			$reg_id = $row1['pk_registration_id'];
			
			/*$rSubEInfo['distance'] = 0;
			$rSubEInfo['start'] = '';
			$rSubEInfo['end'] = '';
			$rSubEInfo['ft_avg'] = '';
			$rSubEInfo['p_avg'] = '';*/
			
			$create_query = "UPDATE ".RESULTSDBNAME.".".$table_name ." SET FINISHTIME = \"".$finishtime."\", PACE = \"".$pace."\", RACEID = \"".$subeventRaceId."\", race_types_id = ".$subEventRaceInfo['distance'].", date_start = \"".$subEventRaceInfo['start']."\", finishtime_avg = \"".$subEventRaceInfo['ft_avg']."\", pace_avg = \"".$subEventRaceInfo['p_avg']."\" WHERE pk_registration_id = \"".$reg_id."\"";
			if(mysqli_query($dbLinkConnection,$create_query)) {
				//insert id
				$reg_id = mysqli_insert_id ( $dbLinkConnection );
				echo "Registration updated - ".$reg_id."<br>";
			}else{
				echo "Update failed<br>";
				echo mysql_errno($dbLinkConnection) . ": " . mysql_error($dbLinkConnection) . "\n";
			}
		}
	}elseif($affected_rows == 0){
		//add customer and return id
		//$customerInfo['subEInfo'] = $subEventRaceInfo;
		echo "Registration does not exist <br/>";
		$create_query = "INSERT INTO ".RESULTSDBNAME.".".$table_name ." (fk_event_id,fk_customer_id,BIB,RACEID,FINISHTIME,PACE,race_table_id,race_types_id,date_start,finishtime_avg,pace_avg) VALUES (\"".$eventId."\",\"".$custId."\",\"".$bib."\",\"".$subeventRaceId."\",\"".$finishtime."\",\"".$pace."\",\"".$table_id."\",\"".$subEventRaceInfo['distance']."\",\"".$subEventRaceInfo['start']."\",\"".$subEventRaceInfo['ft_avg']."\",\"".$subEventRaceInfo['p_avg']."\")";
		if(mysqli_query($dbLinkConnection,$create_query)) {
    		//insert id
    		$reg_id = mysqli_insert_id ( $dbLinkConnection );
    		echo "Registration created - ".$reg_id."<br>";
    	}else{
    		echo "Insert failed<br>";
    		echo mysql_errno($dbLinkConnection) . ": " . mysql_error($dbLinkConnection) . "\n";
    	}
	}else{
		//too many records to determine or error
	}
	//dbClose( $dbLinkConnection );
	return $reg_id;
}

function getAllPublishedEvents(){
	//connect to db
	$eArray = array();
	$table_name = "wp_fz_event";
	$selectStatement = 'SELECT pk_event_id,rc_appid FROM '.DBNAME2.'.'.$table_name .' WHERE published = 1 AND date_end < NOW() AND pk_event_id >= 3 and pk_event_id <= 4 LIMIT 4';
	$dbLinkConnection = dbConnect('results');
	$result1 = mysqli_query( $dbLinkConnection, $selectStatement );
	$affected_rows = mysqli_affected_rows( $dbLinkConnection );
	if($affected_rows == 0){
		//no records
		echo 'no events';
	}else{
		//get events
		while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
			$eArray[$row1['pk_event_id']] = $row1['rc_appid'];
		}
	}
	dbClose( $dbLinkConnection );
	return $eArray;
}

function getLastPublishedEvent(){
	//connect to db
	$eArray = array();
	$table_name = "wp_fz_event";
	$selectStatement = 'SELECT pk_event_id,rc_appid FROM '.DBNAME2.'.'.$table_name .' WHERE published = 1 AND date_end < NOW() ORDER BY date_end DESC, pk_event_id DESC LIMIT 1';
	$dbLinkConnection = dbConnect('results');
	$result1 = mysqli_query( $dbLinkConnection, $selectStatement );
	$affected_rows = mysqli_affected_rows( $dbLinkConnection );
	if($affected_rows == 0){
		//no records
		echo 'no events';
	}else{
		//get events
		while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
			$eArray[$row1['pk_event_id']] = $row1['rc_appid'];
		}
	}
	dbClose( $dbLinkConnection );
	return $eArray;
}

function getPublishedEvent($ev_id){
	//connect to db
	$eArray = array();
	$table_name = "wp_fz_event";
	$selectStatement = 'SELECT pk_event_id,rc_appid FROM '.EVENTDBNAME.'.'.$table_name .' WHERE published = 1 AND date_end < NOW() AND pk_event_id = '.$ev_id.' ORDER BY date_end DESC, pk_event_id DESC LIMIT 1';
	$dbLinkConnection = dbConnect('events');
	$result1 = mysqli_query( $dbLinkConnection, $selectStatement );
	$affected_rows = mysqli_affected_rows( $dbLinkConnection );
	if($affected_rows == 0){
		//no records
		echo 'no events';
	}else{
		//get events
		while ( $row1 = mysqli_fetch_array( $result1, MYSQL_ASSOC ) ) {
			$eArray[$row1['pk_event_id']] = $row1['rc_appid'];
		}
	}
	dbClose( $dbLinkConnection );
	return $eArray;
}

function getSubEventInfo($ev_id){
	//connect to db
	$eArray = array();
	$table_name = "wp_fz_sub_event";
	$selectStatement = 'SELECT fk_race_type_id,ra_id,date_start,date_end,FINISHTIME_AVG,PACE_AVG FROM '.EVENTDBNAME.'.'.$table_name .' WHERE fk_event_id = '.$ev_id.' ORDER BY date_end DESC, ra_id DESC';
	$dbLinkConnection = dbConnect('events');
	$result1a = mysqli_query( $dbLinkConnection, $selectStatement );
	$affected_rowsS = mysqli_affected_rows( $dbLinkConnection );
	if($affected_rowsS == 0){
		//no records
		echo 'no sub events';
	}else{
		//get events
		while ( $row1a = mysqli_fetch_array( $result1a, MYSQL_ASSOC ) ) {
			$arraystuff = array();
			$arraystuff['distance'] = $row1a['fk_race_type_id'];
			$arraystuff['start'] = $row1a['date_start'];
			$arraystuff['end'] = $row1a['date_end'];
			$arraystuff['ft_avg'] = $row1a['FINISHTIME_AVG'];
			$arraystuff['p_avg'] = $row1a['PACE_AVG'];
			$eArray[$row1a['ra_id']] = $arraystuff;
		}
	}
	dbClose( $dbLinkConnection );
	return $eArray;
}

//tables
//Runners that have finished a race
/*CREATE TABLE `wp_fz_runners` (
  `pk_customer_id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `FIRSTNAME` varchar(35) DEFAULT NULL,
  `LASTNAME` varchar(80) DEFAULT NULL,
  `SEX` varchar(10) DEFAULT NULL,
  `AGE` tinyint(3) DEFAULT NULL,
  `DOB` int(11) DEFAULT NULL,
  `CITY` varchar(60) DEFAULT NULL,
  `STATE` varchar(60) DEFAULT NULL,
  `COUNTRY` varchar(10) DEFAULT NULL,
  `ZIP` varchar(20) DEFAULT NULL,
  `EMAIL` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`pk_customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE rnr_results.wp_fz_runners (
  `pk_customer_id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `FIRSTNAME` varchar(35) DEFAULT NULL,
  `LASTNAME` varchar(80) DEFAULT NULL,
  `SEX` varchar(10) DEFAULT NULL,
  `AGE` tinyint(3) DEFAULT NULL,
  `DOB` int(11) DEFAULT NULL,
  `CITY` varchar(60) DEFAULT NULL,
  `STATE` varchar(60) DEFAULT NULL,
  `COUNTRY` varchar(10) DEFAULT NULL,
  `ZIP` varchar(20) DEFAULT NULL,
  `EMAIL` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`pk_customer_id`),
  KEY (`EMAIL`),
  KEY (`DOB`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;*/


//Races runner has run
/*CREATE TABLE `wp_fz_runner_races` (
  `pk_registration_id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `fk_event_id` int(10) NOT NULL,
  `fk_customer_id` int(11) NOT NULL,
  `BIB` int(11)  DEFAULT NULL,
  `RACEID` VARCHAR(11)  DEFAULT NULL,
  `FINISHTIME` VARCHAR(11)  DEFAULT NULL,
  `PACE` VARCHAR(11)  DEFAULT NULL,
  `race_table_id` varchar(32) DEFAULT NULL,
  `race_types_id` INT(10) DEFAULT NULL,
  `date_start` DATE NOT NULL,
  `finishtime_avg` VARCHAR(11) DEFAULT NULL,
  `pace_avg` VARCHAR(11) DEFAULT NULL,
  PRIMARY KEY (`pk_registration_id`),
  KEY `fk_event_id` (`fk_event_id`),
  KEY `fk_customer_id` (`fk_customer_id`),
  KEY `BIB` (`BIB`),
  KEY `RACEID` (`RACEID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE rnr_results.wp_fz_runner_races (
  `pk_registration_id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `fk_event_id` int(10) NOT NULL,
  `fk_customer_id` int(11) NOT NULL,
  `BIB` int(11) DEFAULT NULL,
  `RACEID` varchar(11) DEFAULT NULL,
  `FINISHTIME` varchar(11) DEFAULT NULL,
  `PACE` varchar(11) DEFAULT NULL,
  `race_table_id` varchar(32) DEFAULT NULL,
  `race_types_id` int(10) DEFAULT NULL,
  `date_start` date NOT NULL,
  `finishtime_avg` varchar(11) DEFAULT NULL,
  `pace_avg` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`pk_registration_id`),
  UNIQUE KEY `NODUPS` (`BIB`,`fk_event_id`),
  KEY `fk_event_id` (`fk_event_id`),
  KEY `fk_customer_id` (`fk_customer_id`),
  KEY `BIB` (`BIB`),
  KEY `RACEID` (`RACEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;*/
?>