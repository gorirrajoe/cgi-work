<?php
	/* Template Name: Course Maps */

	get_header();
	global $wpdb;

	$parent_slug = the_parent_slug();
	rnr3_get_secondary_nav( $parent_slug );

	$prefix = '_rnr3_';

	$market = get_market2();
	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';
?>

	<!-- main content -->
	<main role="main" id="main">
		<section class="wrapper grid_1">
			<div class="content">

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
					echo '<h2>'. get_the_title(). '</h2>';
					the_content();
				endwhile; endif;

				$event_whole		= get_post_meta( get_the_ID(), $prefix . 'coursemap_event_number', 1 );
				$eventnumbersplit	= explode ( "_", $event_whole );
				$eventnumber		= $eventnumbersplit[0];
				$wm					= $eventnumbersplit[1];

				// default selections
				$zoom = ( get_post_meta( get_the_ID(), $prefix . 'coursemap_zoom', 1 ) != '' ) ? get_post_meta( get_the_ID(), $prefix . 'coursemap_zoom', 1 ) : 8;

				$coursemap_miles_kilometers = ( get_post_meta( get_the_ID(), $prefix . 'coursemap_miles_kilometers', 1 ) != '' ) ? get_post_meta( get_the_ID(), $prefix . 'coursemap_miles_kilometers', 1 ) : 'miles';

				$coursemap_options = get_post_meta( get_the_ID(), '_rnr3_coursemap_options', 1 );
				if( $coursemap_options != '' ) {

					foreach( $coursemap_options as $key => $value ) {
						$value = str_replace( $prefix, '', $value );
						${$value} = 'on';
					}

				}

				if ( isset( $_POST['wmoption'] ) ) { // logic for dropdown
					$wmoption		= $_POST['wmoption'];
					$wmoptionevent	= explode ( "_", $wmoption );
					$eventnumber	= $wmoptionevent[0];
					$wm				= $wmoptionevent[1];
				}

				$siteurl	= 'http://runrocknroll.com';

				$kmlurl		= $siteurl ."/maps/kmlbuilder.php?eventid=". $eventnumber ."&wmid=". $wm ."&cachebuster=". date('dG');

				$filterarray = array();

				if( $coursemap_options != '' ) {
					// filters -- must update when waypoints are added/removed
					if ( isset( $coursemap_firstaid ) && $coursemap_firstaid == 'on' ) {
						$kmlurl .= "&FirstAidStation-1=1";
						$filterarray[] = "FirstAidStation-1";
					}
					if ( isset( $coursemap_bandstop ) && $coursemap_bandstop == 'on' ) {
						$kmlurl .= "&BandStop-2=1";
						$filterarray[] = "BandStop-2";
					}
					/*if ( isset( $coursemap_gustop ) && $coursemap_gustop == 'on' ) {
						$kmlurl .= "&GLUKOSStop-6=1";
						$filterarray[] = "GLUKOSStop-6";
					}*/
					if ( isset( $coursemap_gustop ) && $coursemap_gustop == 'on' ) {
						$kmlurl .= "&GELStop-6=1";
						$filterarray[] = "GELStop-6";
					}
					if ( isset( $coursemap_hotel ) && $coursemap_hotel == 'on' ) {
						$kmlurl .= "&Hotel-7=1";
						$filterarray[] = "Hotel-7";
					}
					if ( isset( $coursemap_parking ) && $coursemap_parking == 'on' ) {
						$kmlurl .= "&Parking-8=1";
						$filterarray[] = "Parking-8";
					}
					if ( isset( $coursemap_relay ) && $coursemap_relay == 'on' ) {
						$kmlurl .= "&Relay-9=1";
						$filterarray[] = "Relay-9";
					}
					if ( isset( $coursemap_water ) && $coursemap_water == 'on' ) {
						$kmlurl .= "&WaterStation-12=1";
						$filterarray[] = "WaterStation-12";
					}
					if ( isset( $coursemap_water ) && $coursemap_water == 'on' ) {
						$kmlurl .= "&HydrationStation-12=1";
						$filterarray[] = "HydrationStation-12";
					}
					if ( isset( $coursemap_expo ) && $coursemap_expo == 'on' ) {
						$kmlurl .= "&Expo-13=1";
						$filterarray[] = "Expo-13";
					}
					if ( isset( $coursemap_info ) && $coursemap_info == 'on' ) {
						$kmlurl .= "&Information-14=1";
						$filterarray[] = "Information-14";
					}
					if ( isset( $coursemap_distancemarkers ) && $coursemap_distancemarkers == 'on' ) {
						$kmlurl .= "&milemarkers=1";
						$filterarray[] = "milemarkers";
					}
					if ( isset( $coursemap_intune ) && $coursemap_intune == 'on' ) {
						$kmlurl .= "&InTuneSpectatorViewingArea-19=1";
						$filterarray[] = "InTuneSpectatorViewingArea-19";
					}
					if ( isset( $coursemap_gatorade ) && $coursemap_gatorade == 'on' ) {
						$kmlurl .= "&GatoradeEndurance-21=1";
						$filterarray[] = "GatoradeEndurance-21";
					}
					if ( isset( $coursemap_restroom ) && $coursemap_restroom == 'on' ) {
						$kmlurl .= "&Restroom-22=1";
						$filterarray[] = "Restroom-22";
					}
					if ( isset( $coursemap_miles_kilometers ) && $coursemap_miles_kilometers == 'kilometers' ) {
						$kmlurl .= "&kilometers=1";
						$filterarray[] = "kilometers";
					}
				}


				if ( isset( $_POST['filter_event'] ) ) {
					$eventnumber	= $_POST['filter_event'];
					$wm				= $_POST['filter_wm'];
					$filterarray	= array();
					$kmlurl			= $siteurl ."/maps/kmlbuilder.php?eventid=".$eventnumber."&wmid=".$wm;
					$filter			= $_POST["filters"];
					$count			= count ( $filter );

					for ( $i = 0; $i < $count; $i++ ) {

						if ( $filter[$i] == 'kilometers' ) {
							$kilometer_flag	= 1;
							$kmlurl			.= "&kilometers=1";
							$filterarray[]	= "kilometers";
						} else {
							$kmlurl			.= "&". $filter[$i] ."=1";
							$filterarray[]	= $filter[$i];
						}

					}
					if ( $kilometer_flag == 1 ) {
						$coursemap_miles_kilometers = 'kilometers';
					} else {
						$coursemap_miles_kilometers = 'miles';
					}
				}

				$siteurl = get_permalink();

				$courses = $wpdb->get_results( "SELECT e.wm_event_race_id AS event_id, e.wm_event_name AS event_name, m.wm_id AS course_id, m.wm_course_name AS course_name FROM wp_waypoint_event e, wp_waypoint_map m WHERE e.wm_event_race_id = m.wm_event_id ORDER BY e.wm_event_name, m.wm_course_name" );

				foreach( $courses as $row ) {
					if( ( $eventnumber == $row->event_id ) && ( $wm == $row->course_id ) ) {
						//$displayRaceName = stripslashes( $row->event_name )." ( ".$row->course_name." )";
						$displayRaceName = $row->course_name;
					}
				}

				// elevation
				if( isset( $coursemap_elevation ) && $coursemap_elevation == 'on' ) {
					if( isset( $eventnumber ) AND ( $eventnumber != '' ) ) {

						$markPoints = $wpdb->get_results( "SELECT wm_id, wm_event_id, wm_status, wm_json_course, wm_map_center FROM wp_waypoint_map WHERE wm_id = '". $wm ."'" );

						$wmEventId		= array();
						$wmpIds			= array();
						$wmpLat			= array();
						//$pathPoints	= '';
						//$count		= 0;
						$lon			= array();
						$lat			= array();

						//calculate distances
						$totalDistanceKM		= array();
						$totalDistanceMiles		= array();
						$distances				= array();
						$previousLon			= 0;
						$previousLat			= 0;
						$ratio					= 1.609344;
						$distanceMilesFloored	= 0;
						$lastMMarker			= 0;
						$addOnDistance			= 0;

						//Loop through results
						foreach( $markPoints as $rowMP ) {

							$jsonCenter = $rowMP->wm_map_center;
								$jsonCenter2				= str_replace( '}, {', "}; {", $jsonCenter );
								$jsonCenter_needed_decode	= explode( '; ', $jsonCenter2 );

								// foreach ( $jsonCenter_needed_decode as $value ) {
								// 	$center = json_decode( $value,true );
								// 	$clon = $coordinates["lon"];
								// 	$clat = $coordinates["lat"];
								// }

								$json				= $rowMP->wm_json_course;
								$json2				= str_replace( '}, {', "}; {", $json );
								$json_needed_decode	= explode( '; ', $json2 );
								$dataPoints			= sizeof( $json_needed_decode );

								foreach ( $json_needed_decode as $key=>$value ) {
									$usedForElevation = 0;

									if( $dataPoints <= 300 ) {
										$coordinates		= json_decode( $value,true );
										$lon[]				= $coordinates["lon"];
										$lat[]				= $coordinates["lat"];
										$usedForElevation	= 1;
									} elseif( $dataPoints > 300 AND $dataPoints <= 600 ) {

										if( $key%2==0 OR $dataPoints == $key+1 ) {
											$coordinates		= json_decode( $value,true );
											$lon[]				= $coordinates["lon"];
											$lat[]				= $coordinates["lat"];
											$usedForElevation	= 1;
										} else {
											$coordinates = json_decode( $value,true );
										}
									} elseif( $dataPoints > 600 AND $dataPoints <= 900 ) {

										if( $key%3==0 OR $dataPoints == $key+1 ) {
											$coordinates		= json_decode( $value,true );
											$lon[]				= $coordinates["lon"];
											$lat[]				= $coordinates["lat"];
											$usedForElevation	= 1;
										} else {
											$coordinates = json_decode( $value,true );
										}
									} elseif( $dataPoints > 900 AND $dataPoints <= 1200 ) {

										if( $key%4==0 OR $dataPoints == $key+1 ) {
											$coordinates		= json_decode( $value,true );
											$lon[]				= $coordinates["lon"];
											$lat[]				= $coordinates["lat"];
											$usedForElevation	= 1;
										} else {
											$coordinates = json_decode( $value,true );
										}
									} elseif( $dataPoints > 1200 AND $dataPoints <= 1500 ) {

										if( $key%5==0 OR $dataPoints == $key+1 ) {
											$coordinates		= json_decode( $value,true );
											$lon[]				= $coordinates["lon"];
											$lat[]				= $coordinates["lat"];
											$usedForElevation	= 1;
										} else {
											$coordinates = json_decode( $value,true );
										}
									}
									if( $usedForElevation == 1 ) {

										end( $lon );
										$keyCurrent = key( $lon );

										if( !$previousLon AND !$previousLat ) {
											$previousLon						= deg2rad( $coordinates["lon"] );
											$previousLat						= deg2rad( $coordinates["lat"] );
											$distances[$keyCurrent]				= 0 + $addOnDistance;
											$totalDistanceKM[$keyCurrent]		= 0;
											$totalDistanceMiles[$keyCurrent]	= 0;
											$lastMMarker						= 0;
										} else {

											$radiusOfEarth	= 6371;
											// Apply the Haversine formula to calculate the distance.
											$diffLatitude	= deg2rad( $coordinates["lat"] ) - $previousLat;
											$diffLongitude	= deg2rad( $coordinates["lon"] ) - $previousLon;

											$a = sin( $diffLatitude / 2 ) * sin( $diffLatitude / 2 ) +
											cos( deg2rad( $coordinates["lat"] ) ) * cos( $previousLat ) *
											sin( $diffLongitude / 2 ) * sin( $diffLongitude / 2 );

											$c				= 2 * atan2( sqrt( $a ), sqrt( 1 - $a ) );
											$distances[]	= $radiusOfEarth * $c;

											$previousLon	= deg2rad( $coordinates["lon"] );
											$previousLat	= deg2rad( $coordinates["lat"] );

											// default is kilometers
											$totalDistanceKM[$keyCurrent]	= round( $totalDistanceKM[$keyCurrent - 1] + $distances[$keyCurrent] + $addOnDistance, 2 );
											$distanceKMFloored[$keyCurrent]	= floor( $totalDistanceKM[$keyCurrent] );

											if( $distanceKMFloored > 0 and $distanceKMFloored != $lastMMarker ) {
												$lastMMarker = $distanceKMFloored;
											}

											// convert to miles
											$distanceMilesFloored = floor( round( ( $totalDistanceKM[$keyCurrent] / $ratio ), 3 ) );

											$totalDistanceMiles[$keyCurrent] = round( ( $totalDistanceKM[$keyCurrent] / $ratio ), 3 );
											if( $distanceMilesFloored > 0 and $distanceMilesFloored != $lastMMarker ) {
												$lastMMarker = $distanceMilesFloored;
											}
											$addOnDistance = 0;
										}
									} else {

										if( !$previousLon AND !$previousLat ) {
											$previousLon	= deg2rad( $coordinates["lon"] );
											$previousLat	= deg2rad( $coordinates["lat"] );
											$addOnDistance	= 0;
										} else {
											$radiusOfEarth	= 6371;
											// Apply the Haversine formula to calculate the distance.
											$diffLatitude	= deg2rad( $coordinates["lat"] ) - $previousLat;
											$diffLongitude	= deg2rad( $coordinates["lon"] ) - $previousLon;
											$a				= sin( $diffLatitude / 2 ) * sin( $diffLatitude / 2 ) +
											cos( deg2rad( $coordinates["lat"] ) ) * cos( $previousLat ) *
											sin( $diffLongitude / 2 ) * sin( $diffLongitude / 2 );

											$c				= 2 * atan2( sqrt( $a ), sqrt( 1 - $a ) );
											$addOnDistance	= ( $radiusOfEarth * $c ) + $addOnDistance;
											$previousLon	= deg2rad( $coordinates["lon"] );
											$previousLat	= deg2rad( $coordinates["lat"] );
										}
									}
								}
						} ?>

						<script>
							var elevator;
							var chart;
							var infowindow = new google.maps.InfoWindow();
							var polyline;

							// The following path marks a general path from Mt.
							// Whitney, the highest point in the continental United
							// States to Badwater, Death Vallet, the lowest point.
							<?php
								$total = 1;
								foreach ( $lon as $key => $value ) {
									echo "var total".$total." = new google.maps.LatLng( ".$lat[$key].",".$value." );\n\t";
									$total++;
								}
							?>

							// Load the Visualization API and the columnchart package.
							//google.load( 'visualization', '1', {packages: ['columnchart']} );
							google.load( 'visualization', '1', {packages: ['corechart']} );

							function initialize() {
								// Create an ElevationService.
								elevator = new google.maps.ElevationService();

								// Draw the path, using the Visualization API and the Elevation service.
								drawPath();
							}

							function drawPath() {

								// Create a new chart in the elevation_chart DIV.
								//chart = new google.visualization.ColumnChart( document.getElementById( 'elevation_chart' ) );
								chart = new google.visualization.LineChart( document.getElementById( 'elevation_chart' ) );

								var path = [<?php $i = 1; while ( $i < $total ) { echo "total".$i; if( $i != $total ) { echo ","; } $i++; }?> ];

								// Create a PathElevationRequest object using this array.
								// Ask for 256 samples along that path.
								var pathRequest = {
									'path': path,
									'samples': <?php echo $total-1;?>
								}

								// Initiate the path request.
								elevator.getElevationAlongPath( pathRequest, plotElevation );
							}

							// Takes an array of ElevationResult objects, draws the path on the map
							// and plots the elevation profile on a Visualization API ColumnChart.
							function plotElevation( results, status ) {
								if ( status == google.maps.ElevationStatus.OK ) {
									elevations = results;

									// Extract the elevation samples from the returned results
									// and store them in an array of LatLngs.
									var elevationPath = [];
									for ( var i = 0; i < results.length; i++ ) {
										elevationPath.push( elevations[i].location );
									}

									// Display a polyline of the elevation path.
									var pathOptions = {
										path: elevationPath,
										strokeColor: '#0000CC',
										opacity: 0.4
									}
									polyline = new google.maps.Polyline( pathOptions );

									<?php if( $coursemap_miles_kilometers == 'miles' ) {
										$titleY	= 'Elevation ( ft )';
										$titleX	= 'Distance ( miles )';
										echo "var distances = ".json_encode( $totalDistanceMiles ).";\n\t";
									} else {
										$titleY	= 'Elevation ( meters )';
										$titleX	= 'Distance ( kilometers )';
										echo "var distances = ".json_encode( $totalDistanceKM ).";\n\t";
									}
									?>

									// Extract the data from which to populate the chart.
									// Because the samples are equidistant, the 'Sample'
									// column here does double duty as distance along the
									// X axis.
									var highestEl = 0;
									var data = new google.visualization.DataTable();
									data.addColumn( 'number', 'Distance' );
									data.addColumn( 'number', 'Elevation' );

									for ( var i = 0; i < results.length; i++ ) {
										//data.addRow( ['', elevations[i].elevation] );
										<?php if ( $coursemap_miles_kilometers == 'miles' ) { // ft
											echo 'data.addRow( [distances[i], Math.round( elevations[i].elevation* 3.2808399 )] );';
										} else {  // meters
											echo 'data.addRow( [distances[i], Math.round( elevations[i].elevation )] );';
										} ?>
										if( elevations[i].elevation > highestEl ) {
											highestEl = elevations[i].elevation;
										}
									}

									// Draw the chart using the data within its DIV.
									document.getElementById( 'elevation_chart' ).style.display = 'block';
									chart.draw( data, {
										legend: 'none',
										titleY: '<?php echo $titleY; ?>',
										titleX: '<?php echo $titleX; ?>'
										} );
								}
							}
						</script>
					<?php }
				}

				if( $qt_lang['lang'] == 'en' ) { ?>
					<h3 class="eventName"><?php echo $displayRaceName; ?></h3>
				<?php } ?>


				<div class="multipleWM">
					<?php
						$wmcount	= $wpdb->get_var( "SELECT COUNT( * ) FROM wp_waypoint_map WHERE wm_event_id = $eventnumber" );

						$wmcourses	= $wpdb->get_results( "SELECT e.wm_event_race_id AS event_id, e.wm_event_name AS event_name, m.wm_id AS course_id, m.wm_course_name AS course_name, m.wm_event_id AS course_event_id FROM wp_waypoint_event e, wp_waypoint_map m WHERE e.wm_event_race_id = m.wm_event_id AND m.wm_event_id = $eventnumber" );

						$coursenumber = $eventnumber .'_'. $wm;
						if ( $wmcount > 1 ) {
							echo '<form action="'. $siteurl .'" method="post">
								<select name="wmoption" onchange="form.submit()">
									<option value="">'. $txt_select_course .'</option>';

									foreach( $wmcourses as $row ) {
										$row_comparison = $row->event_id."_".$row->course_id;
											echo "<option value=\"".$row_comparison."\">".$row->course_name."</option>\n";
									}

								echo '</select>
							</form>';
						}
					?>
				</div>


				<?php if( $qt_lang['lang'] == 'en' ) { ?>

					<div class="filterMap">
						<a href="#" class="show_hide"><?php echo $txt_filter_map;?> &#9660;</a>

						<div class="filterDiv">

							<form action="<?php echo $siteurl; ?>" method="post">
								<fieldset>
									<legend><?php echo $txt_select_filter;?></legend>

									<?php
										$waypointNames = $wpdb->get_results( "SELECT DISTINCT s.wm_id, s.wmp_id AS wmp_id, t.wmp_sub_type AS wmp_sub_type, t.wmp_image AS wmp_image FROM wp_waypoint_details s, wp_waypoint_map_points t WHERE s.wm_id = $wm and t.wmp_id = s.wmp_id AND s.wmp_id NOT IN ( 11,4 ) ORDER BY t.wmp_title" );
									?>

									<ol>
										<?php
											$checked1 = "";
											if( isset( $filterarray ) ) {
												foreach( $filterarray as $filteritem ) {
													if ( $filteritem == "milemarkers" ) {
														$checked1 = " checked=\"checked\"";
													}
												}
											}
											echo "<li>
												<input". $checked1 ." id=\"milemarkers\" value=\"milemarkers\" name=\"filters[]\" type=\"checkbox\" class=\"chk_boxes1\" />
												<label for=\"milemarkers\">Distance Markers <img src=\"/wp-content/themes/rnr3/img/distance_marker.png\" class=\"filter_img\"></label>
												<ul class=\"miles_or_km\">";
													if ( $coursemap_miles_kilometers == 'miles' ) { ?>
														<li><input type="radio" name="filters[]" value="miles" id="miles" checked="checked"><label for="miles">Miles</label></li>
														<li><input type="radio" name="filters[]" id="kilometers" value="kilometers"><label for="kilometers">Kilometers</labels></li>
													<?php } else { ?>
														<li><input type="radio" name="filters[]" id="miles" value="miles"><label for="miles">Miles</label></li>
														<li><input type="radio" name="filters[]" id="kilometers" value="kilometers" checked="checked"><label for="kilometers">Kilometers</labels></li>
													<?php }
												echo "</ul>
											</li>";

											foreach( $waypointNames as $row ) {
												$cleanName	= str_replace( " ", "", $row->wmp_sub_type );
												$cleanName	.= "-".$row->wmp_id;

												$checked = "";
												if ( isset( $filterarray ) ) {
													foreach( $filterarray as $filteritem ) {
														if ( $cleanName == $filteritem ) {
															$checked = " checked=\"checked\"";
														}
													}
												}
												echo "<li>
													<input". $checked ." id=\"".$row->wmp_id."\" value=\"".$cleanName."\" name=\"filters[]\" type=\"checkbox\" class=\"chk_boxes1\" />
													<label for=\"".$row->wmp_id."\">".$row->wmp_sub_type." <img class=\"filter_img\" src=\"".$row->wmp_image."\"></label>
												</li>";
											}
										?>
									</ol>
								</fieldset>

								<input id="checkall" type="checkbox" class="chk_boxes" label="Check/Uncheck All" />
								<input type="hidden" name="filter_wm" value="<?php echo $wm; ?>">
								<input type="hidden" name="filter_event" value="<?php echo $eventnumber; ?>">
								<label for="checkall">Check/Uncheck All</label>
								<input type="submit" value="Submit" />
							</form>
						</div>
					</div>
				<?php } ?>


				<div id="map_canvas"></div>

				<script>
					jQuery( document ).ready( function( $ ) {
						$( ".b_opacity" ).click( function() {
							//this will find all the images in the map canvas container. Normally you would want to target specific images by the value of src
							$( "#map_canvas" ).find( "img" ).css( "opacity","0.4" )
						} )

						//Set center of the map
						var myOptions = {
							zoom: <?php echo $zoom; ?>,
							zoomControl: true,
							zoomControlOptions: {
								style: google.maps.ZoomControlStyle.DEFAULT
							},
							mapTypeId: google.maps.MapTypeId.ROADMAP
						}
						var map = new google.maps.Map( document.getElementById( "map_canvas" ), myOptions );
						var ctaLayer = new google.maps.KmlLayer( "<?php echo $kmlurl; ?>" );
						ctaLayer.setMap( map );

						$( ".filterDiv" ).hide();
						$( ".show_hide" ).show();
						$( ".show_hide" ).click( function() {
							$( ".filterDiv" ).slideToggle();
							return false;
						} );

						jQuery( "#milemarkers" ).change( function() {
							if ( jQuery( this ).is( ":checked" ) ) {
								jQuery( ".miles_or_km" ).show();
							} else {
								jQuery( ".miles_or_km" ).hide();
							}
						} );
						jQuery( "#milemarkers" ).change();

					} );
					jQuery( function( $ ) {
						$( ".chk_boxes" ).click( function() {
							$( ".chk_boxes1" ).prop( "checked", this.checked );
						} );
					} );
				</script>


				<?php // elevation chart
					if ( isset( $coursemap_elevation ) && $coursemap_elevation == "on" ) {
						echo '<div id="elevation_chart"></div>';
					}

					// download link
					if ( $coursemap_download == "on" ) {
						$kmlicon		= get_bloginfo( 'stylesheet_directory' ) . "/img/ico-kml.png";
						$downloadKML	= '<p><img src="'. $kmlicon .'" class="kmlicon" /> <a href="'. $kmlurl . '&download=1">'. $txt_dwn_kml .'</a></p>';
						echo $downloadKML;
					}
				?>
				<p class="map_disclaimer">* <?php echo $txt_disclaimer;?></p>
				<div><?php echo apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'coursemap_below_text', 1 ) ); ?></div>
			</div>
		</section>

	</main>

<?php get_footer(); ?>

