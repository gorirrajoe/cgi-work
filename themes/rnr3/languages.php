<?php
	/* Languages */
	$northamerican_array	= get_non_us_na_races();
	$runner_tracking_array	= array( 'liverpool', 'dublin', 'lisbon', 'madrid', 'mexico-city', 'merida', 'queretaro' );
	$awards_array			= array( 'liverpool', 'dublin', 'lisbon' );

	/**
	 * entertainment
	 */
	if( $qt_lang['lang']	== 'es' ) {
		$headliner['title']	= 'Actuaci&oacute;n estrella en la serie de conciertos Toyota Rock \'n\' Roll';
		$schedule['title']	= 'Horario';
		$bands['title']		= 'Ocio en la carrera';
		$spirit['title']	= 'Esp&iacute;ritu de la carrera';
	} elseif( $qt_lang['lang']	== 'fr' ) {
		$headliner['title']	= 'Artiste en vedette de la s&eacute;rie Concerts Rock \'n\' Roll Toyota';
		$schedule['title']	= 'Programme';
		$bands['title']		= 'Divertissement sur le parcours';
		$spirit['title']	= 'Esprit sur le parcours';
	} elseif( $qt_lang['lang']	== 'pt' ) {
		$headliner['title']	= 'Atua&ccedil;&atilde;o da atra&ccedil;&atilde;o principal na s&eacute;rie de concertos de Rock \'n\' Roll da Toyota';
		$schedule['title']	= 'Hor&aacute;rio';
		$bands['title']		= 'Entretenimento no Curso';
		$spirit['title']	= 'Esp&iacute;rito sobre o Curso';
	} elseif( $qt_lang['lang'] == 'zh' ) {
		$headliner['title']	= '丰田摇滚音乐会系列的主打表演';
		$schedule['title']	= '时间表';
		$bands['title']		= '途中娱乐';
		$wedding['title']	= 'Run Through Wedding';
		$elvis['title']		= 'Run as Elvis';
		$perks['title']		= '跑者打扮';
		$spirit['title']	= '此过程中的精神';
	} else {
		$headliner['title']	= 'Headliner Act at the Toyota Rock \'n\' Roll Concert Series';
		$schedule['title']	= 'Schedule';
		$bands['title']		= 'Entertainment on the Course';
		$wedding['title']	= 'Run Through Wedding';
		$elvis['title']		= 'Run as Elvis';
		$perks['title']		= 'Runner Perks';
		$spirit['title']	= 'Spirit on the Course';
	}
	// NOT toyota concert series
	if( get_post_meta( get_the_ID(), '_rnr3_toyota', 1 )	== 'on' ) {
		if( $qt_lang['lang']	== 'es' ) {
			$headliner['title']	= 'Actuaci&oacute;n estrella';
		} elseif( $qt_lang['lang']	== 'fr' ) {
			$headliner['title']	= 'Artiste en vedette';
		} elseif( $qt_lang['lang']	== 'pt' ) {
			$headliner['title']	= 'Atua&ccedil;&atilde;o da atra&ccedil;&atilde;o principal';
		} elseif( $qt_lang['lang'] == 'zh' ) {
			$headliner['title']	= '主打音乐会';
		} else {
			$headliner['title']	= 'Headliner Act';
		}
	}


	/**
	 * read more
	 */
	if( $qt_lang['lang']	== 'es' ) {
		$readmore_txt		= 'Lea m&aacute;s';
		$postnav_previous	= 'Anterior';
		$postnav_next		= 'Siguiente';
	} elseif( $qt_lang['lang']	== 'pt' ) {
		$readmore_txt	= 'Ler Mais';
		$postnav_previous	= 'Anterior';
		$postnav_next		= 'Seguinte';
	} elseif( $qt_lang['lang']	== 'fr' ) {
		$readmore_txt	= 'En savoir plus';
		$postnav_previous	= 'Pr&eacute;c&eacute;dent ';
		$postnav_next		= 'Suivant';
	} else {
		$readmore_txt	= 'Read More';
		$postnav_previous	= 'Previous';
		$postnav_next		= 'Next';
	}


	/**
	 * course maps
	 */
	if( $qt_lang['lang']	== 'es' ) {
		if( !empty( $event_info ) && in_array( $event_info->event_slug, $northamerican_array ) ) {
			$txt_select_course	= 'Rutas';
			$txt_select_filter	= 'Filtro';
			$txt_filter_map		= 'Filtro Rutas';
			$txt_disclaimer		= 'La Ruta sujetos a cambios. Ubicaciones y elevaciones de marcadores de distancias son aproximadas en base a datos de mapas disponibles de Google.';
			$txt_dwn_kml		= 'Descargar archivo KML';
		} else {	// euro spanish
			$txt_select_course	= 'Selecciona un subevento';
			$txt_select_filter	= 'Elige tus filtros';
			$txt_filter_map		= 'Filtrar plano';
			$txt_disclaimer		= 'El plano de la carrera est&aacute; sujeto a cambios. Las localizaciones y altitudes de los marcadores de distancias son aproximados y se basan en los datos cartogr&aacute;ficos de Google.';
			$txt_dwn_kml		= 'Descargar archivo KML';
		}
	} elseif( $qt_lang['lang']	== 'fr' ) {
		if( in_array( $event_info->event_slug, $northamerican_array ) ) {
			$txt_select_course	= 'Parcours';
			$txt_select_filter	= 'filtre';
			$txt_filter_map		= 'filtre parcours';
			$txt_disclaimer		= 'Sous r&#233;serve de modifications. Les marqueurs de distance et les &#233;l&#233;vations sont estim&#233;es selon les donn&#233;es de cartographie de Google.';
			$txt_dwn_kml		= 'T&#233;l&#233;charger fichier KML';
		} else {	// euro french
			$txt_select_course	= 'S&eacute;lectionner un sous-&eacute;v&eacute;nement';
			$txt_select_filter	= 'Choisir vos filtres';
			$txt_filter_map		= 'Filtrer le plan';
			$txt_disclaimer		= 'Le plan de parcours n\'est pas d&eacute;finitif. Les emplacements des marqueurs de distance et les &eacute;l&eacute;vations sont indiqu&eacute;s de fa&ccedil;on approximative sur la base des donn&eacute;es disponibles sur Google.';
			$txt_dwn_kml		= 'T&#233;l&#233;charger le fichier KML';
		}
	} elseif( $qt_lang['lang']	== 'pt' ) {
		$txt_select_course	= 'Selecione um subevento';
		$txt_select_filter	= 'Escolha os seus filtros';
		$txt_filter_map		= 'Filtrar Mapa';
		$txt_disclaimer		= 'Mapa do Percurso sujeito a altera&ccedil&otilde;es. Os locais de marcadores de dist&acirc;ncias e as eleva&ccedil;&otilde;es s&atilde;o aproximados com base em dados dispon&iacute;veis a partir do mapeamento do Google.';
		$txt_dwn_kml		= 'Baixar o ficheiro KML';
	} elseif( $qt_lang['lang'] == 'zh' ) {
		$txt_select_course	= '选择一个子赛事';
		$txt_select_filter	= '选择您的筛选器';
		$txt_filter_map		= '筛选地图';
		$txt_disclaimer		= '路线图可能更改。距离标志位置和海拔是基于 Google 提供的可用测绘数据的近似值';
		$txt_dwn_kml		= '下载 KML 文件';
	} else {
		$txt_select_course	= 'Choose Your Distance';
		$txt_select_filter	= 'Choose Your Filters';
		$txt_filter_map		= 'Filter Map';
		$txt_disclaimer		= 'Course map subject to change. Distance marker locations and elevations are approximated based on available mapping data from Google.';
		$txt_dwn_kml		= 'Download KML file';
	}


	/**
	 * distances
	 */
	if( $qt_lang['lang']	== 'es' ) {
		if( isset($event_info->event_slug) && in_array( $event_info->event_slug, $northamerican_array ) ) {
			$txt_distances_title	= "Distancias";
			$txt_about_course		= "Sobre la ruta";
			$txt_finisher_jacket	= "Chamarra de Marat&oacute;n Finisher";
			$txt_part_perk			= "Perks Participante";
			$txt_at_a_glance		= "Datos del evento:";
			$txt_date				= "Fecha";
			$txt_start_time			= "Hora de inicio";
			$txt_course_map			= "La Ruta";
			$txt_start_line			= "Lugar de salida";
			$txt_finish_line		= "Lugar de meta";
			$txt_time_limit			= "Tiempo limite de competencia";
			$txt_view				= "Ver mapa aqui";
			$txt_TBD				= "TBD";
			$txt_distance			= 'Distancia ';
		} else {	// euro spanish
			$txt_distances_title	= "Distancias";
			$txt_about_course		= "Sobre la carrera";
			$txt_finisher_jacket	= "Chaqueta de Finisher";
			$txt_part_perk			= "Ventajas para los participantes";
			$txt_at_a_glance		= "De un vistazo";
			$txt_date				= "Fecha";
			$txt_start_time			= "Hora de salida";
			$txt_course_map			= "Plano de la carrera";
			$txt_start_line			= "L&iacute;nea de salida";
			$txt_finish_line		= "L&iacute;nea de meta";
			$txt_time_limit			= "L&iacute;mite de tiempo";
			$txt_view				= "Ver";
			$txt_TBD				= "Por determinar";
			$txt_distance			= 'Distancia ';
		}
	} elseif( $qt_lang['lang']	== 'fr' ) {
		if( in_array( $event_info->event_slug, $northamerican_array ) ) {
			$txt_distances_title	= "Distances";
			$txt_about_course		= "&#192; propos du marathon ";
			$txt_finisher_jacket	= "Manteau de Finissant du Marathon";
			$txt_part_perk			= "Avantages li&#233;s &#224; la participation ";
			$txt_at_a_glance		= "Aper&#231;u ";
			$txt_date				= "Date ";
			$txt_start_time			= "Heure de d&#233;part ";
			$txt_course_map			= "Parcours ";
			$txt_start_line			= "Lieu du d&#233;part ";
			$txt_finish_line		= "Site d&#8217;arriv&#233;e ";
			$txt_time_limit			= "Temps limite pour terminer ";
			$txt_view				= "Acc&#233;der ";
			$txt_TBD				= "&#224; venir";
			$txt_distance			= 'Distances ';
		} else { // euro french
			$txt_distances_title	= "Distances";
			$txt_about_course		= "&Agrave; propos du parcours";
			$txt_finisher_jacket	= "Manteau de Finissant du Marathon";
			$txt_part_perk			= "Petits plus pour les participants";
			$txt_at_a_glance		= "Aper&#231;u ";
			$txt_date				= "Date ";
			$txt_start_time			= "Heure de d&eacute;part";
			$txt_course_map			= "Plan du parcours ";
			$txt_start_line			= "Ligne de d&eacute;part";
			$txt_finish_line		= "Ligne d'arriv&eacute;e";
			$txt_time_limit			= "Barri&egrave;re horaire";
			$txt_view				= "Afficher";
			$txt_TBD				= "&Agrave; d&eacute;terminer";
			$txt_distance			= 'Distance ';
		}
	} elseif( $qt_lang['lang']	== 'pt' ) {
		$txt_distances_title	= "Dist&acirc;ncias";
		$txt_about_course		= "Sobre o Percurso";
		$txt_finisher_jacket	= "Corta vento de finalista da Maratona";
		$txt_part_perk			= "Regalias dos participantes";
		$txt_at_a_glance		= "Uma breve vis&atilde;o";
		$txt_date				= "Data";
		$txt_start_time			= "Hora de In&iacute;cio";
		$txt_course_map			= "Mapa do Percurso";
		$txt_start_line			= "Linha de Partida";
		$txt_finish_line		= "Linha de Chegada";
		$txt_time_limit			= "Limite de Tempo";
		$txt_view				= "Ver";
		$txt_TBD				= "A confirmar";
		$txt_distance			= 'Dist&acirc;ncia ';
	} elseif( $qt_lang['lang'] == 'zh' ) {
		$txt_distances_title	= "距离";
		$txt_about_course		= "关于路线";
		$txt_finisher_jacket	= "Finisher Jacket";
		$txt_part_perk			= "参加者特惠";
		$txt_at_a_glance		= "一览表";
		$txt_date				= "日期";
		$txt_start_time			= "开始时间";
		$txt_course_map			= '路线图';
		$txt_start_line			= "起跑线";
		$txt_finish_line		= "终点线";
		$txt_time_limit			= "时间限制";
		$txt_view				= "查看";
		$txt_TBD				= "待定";
		$txt_distance			= '距离';
	} else {
		$txt_distances_title	= "Distances";
		$txt_about_course		= "About the Course";
		$txt_finisher_jacket	= "Finisher Jacket";
		$txt_part_perk			= "Participant Perks";
		$txt_at_a_glance		= "At a Glance";
		$txt_date				= "Date";
		$txt_start_time			= "Start Time";
		$txt_course_map			= "Course Map";
		$txt_start_line			= "Start Line";
		$txt_finish_line		= "Finish Line";
		$txt_time_limit			= "Time Limit";
		$txt_view				= "View";
		$txt_TBD				= "TBD";
		$txt_distance			= 'Distance';
	}


	/**
	 * expo
	 */
	if( $qt_lang['lang']	== 'es' ) {
		$participant['title']	= 'Informaci&oacute;n para participantes';
		$donation['title']		= 'Informaci&oacute;n sobre donaciones';
		$exhibitor['title']		= 'Informaci&oacute;n sobre exhibidores';
	} elseif( $qt_lang['lang']	== 'fr' ) {
		$participant['title']	= 'Informations pour les participants';
		$donation['title']		= 'Informations pour les dons';
		$exhibitor['title']		= 'Informations pour les exposants';
	} elseif( $qt_lang['lang']	== 'pt' ) {
		$participant['title']	= 'Informa&ccedil;&otilde;es de Participantes';
		$donation['title']		= 'Informa&ccedil;&otilde;es de Doa&ccedil;&otilde;es';
		$exhibitor['title']		= 'Informa&ccedil;&otilde;es de Expositores';
	} elseif( $qt_lang['lang'] == 'zh' ) {
		$participant['title']	= '参加者信息';
		$donation['title']		= '捐款信息';
		$exhibitor['title']		= '出展者信息';
	} else {
		$participant['title']	= 'Participant Info';
		$donation['title']		= 'Donation Info';
		$exhibitor['title']		= 'Exhibitor Info';
	}


	/**
	 * footer
	 */
	if( $qt_lang['lang']	== 'es' ) {
		$txtSeriesPartners	= 'Patrocinadores De La Serie';
		$txtEventPartners	= 'Patrocinadores Del Evento';
		$series_txt			= "Circuito";
		$races_txt			= "Las Carreras";
		$weekend_txt		= "El Fin De Semana";
		$connect_txt		= "Conecta";
		$about_txt			= 'Acerca ';
		$press_txt			= 'Prensa ';
		$results_txt		= 'Resultados ';
		$expo_txt			= 'Expositores SportExpo ';
		$partners_txt		= 'Socios ';
		$charity_txt		= 'Caridad ';
		$elite_txt			= 'Atletas de &Eacute;lite ';
		$contact_txt		= 'Contacto ';
		$blog_txt			= 'Blog';
	} elseif( $qt_lang['lang']	== 'fr' ) {
		if( in_array( $event_info->event_slug, $northamerican_array ) ) {
			$connect_txt	= "Contact";
			$about_txt		= '&Agrave; propos';
			$results_txt	= 'R&eacute;sultats des courses';
			$expo_txt		= 'Marchands de l\'Expo';
			$charity_txt	= '&OElig;uvres caritatives';
			$elite_txt		= 'Coureurs &eacute;lites';
			$contact_txt	= 'Coordonn&eacute;es';
		} else { // euro french
			$connect_txt	= 'Se connecter';
			$about_txt		= '&Agrave;propos de';
			$results_txt	= 'R&eacute;sultats de la course';
			$expo_txt		= 'Vendeur de l\'expo';
			$charity_txt	= '&#156;uvres caritative';
			$elite_txt		= 'Athl&egrave;tes d\'&eacute;lite';
			$contact_txt	= 'Coordonn&eacute;es';
		}
		$txtSeriesPartners	= 'Partenaires';
		$txtEventPartners	= 'Partenaires';
		$series_txt			= "S&eacute;rie";
		$races_txt			= "Les Courses";
		$weekend_txt		= "Le Week-End";
		$press_txt			= 'M&eacute;dias';
		$partners_txt		= 'Partenaires';
		$blog_txt			= 'Blogue';
	} elseif( $qt_lang['lang']	== 'pt' ) {
		$txtSeriesPartners	= '';
		$txtEventPartners	= 'Parceiros Do Evento';
		$series_txt			= "S&eacute;ries";
		$races_txt			= "As corridas";
		$weekend_txt		= "O fim de semana";
		$connect_txt		= "Siga-nos";
		$about_txt			= 'Acerca';
		$press_txt			= 'Imprensa';
		$results_txt		= 'Resultados';
		$expo_txt			= 'Expositores SportExpo';
		$partners_txt		= 'Parceiros';
		$charity_txt		= 'Caridade';
		$elite_txt			= 'Atletas Elite';
		$contact_txt		= 'Contacto';
		$blog_txt			= 'Blog';
	} elseif( $qt_lang['lang']	== 'zh' ) {
		$txtSeriesPartners	= '系列赛伙伴';
		$txtEventPartners	= '赛事伙伴';
		$series_txt			= "系列赛";
		$races_txt			= "比赛";
		$weekend_txt		= "周末";
		$connect_txt		= "联系";
		$about_txt			= '关于';
		$press_txt			= '媒体';
		$results_txt		= '赛事成绩';
		$expo_txt			= '展会商家';
		$partners_txt		= '合作伙伴';
		$charity_txt		= '慈善';
		$elite_txt			= '精英运动员';
		$contact_txt		= '联系';
		$blog_txt			= '博客';
	} else {
		$txtSeriesPartners	= 'Series Partners';
		$txtEventPartners	= 'Event Partners';
		$series_txt			= "Series";
		$races_txt			= "The Races";
		$weekend_txt		= "The Weekend";
		$connect_txt		= "Connect";
		$about_txt			= 'About';
		$press_txt			= 'Press';
		$results_txt		= 'Race Results';
		$expo_txt			= 'Expo Vendor';
		$partners_txt		= 'Partners';
		$charity_txt		= 'Charity';
		$elite_txt			= 'Elite Athletes';
		$contact_txt		= 'Contact';
		$blog_txt			= 'Blog';
	}


	/**
	 * general info
	 */
	if( $qt_lang['lang']	== 'es' ) {
		if( isset($event_info->event_slug) && in_array( $event_info->event_slug, $northamerican_array ) ) {
			$marathon_jacket['title']		= 'Chamarra de Marat&oacute;n Finisher';
		} else {	// euro spanish
			$marathon_jacket['title']		= 'Chaqueta de Finisher ';
		}
		$how_to_register['title']		= 'C&oacute;mo registrarse';
		$confirmation['title']			= 'Confirmaci&oacute;n';
		$personalize_bib['title']		= 'Personaliza tu dorsal';
		$packet_pickup['title']			= 'Recogida de dorsal y bolsa del corredor';
		$corral_changes['title']		= 'Cambios de caj&oacute;n de salida';
		$deferral['title']				= 'Prorrogar';
		$cancellations['title']			= 'Cancelaciones';
		$race_rules['title']			= 'Reglas de la carrera';
		$directions['title']			= 'Instrucciones';
		$parking['title']				= 'Aparcamiento y transporte';
		$allday_20k['title']			= 'All Day 20K Lounge';
		$road_closures['title']			= 'Cierres de carreteras';
		$gear_check['title']			= 'Guardarropa';
		$wave_start['title']			= 'Salida en oleada';
		$time_limits['title']			= 'L&iacute;mites de tiempo';
		$course_support['title']		= 'Asistencia en carrera';
		$race_timing['title']			= 'Cronometraje de carrera';
		$charity['title']				= 'Organizaci&oacute;n ben&eacute;fica';
		$volunteer['title']				= 'Voluntario';
		$competitor_wireless['title']	= 'Runner Tracking';
		$secure_zone['title']			= 'Zona de seguridad';
		$family_reunion['title']		= 'Zona de reencuentro familiar';
		$medal_info['title']			= 'Informaci&oacute;n de medallas';
		$age_group['title']				= 'Premios por grupos de edad';
		$medical['title']				= 'Asistencia m&eacute;dica';
		$info_booth['title']			= 'Puestos de informaci&oacute;n y de objetos perdidos';
		$race_changes['title']			= 'Cambios en la carrera';
		$pace_team['title']				= 'Liebres';
		$txt_before_race				= 'ANTES DE LA CARRERA';
		$txt_race_day					= 'EL D&Iacute;A DE LA CARRERA';
		$txt_after_race					= 'DESPU&Eacute;S DE LA CARRERA';
		$wedding['title']				= 'Boda en plena carrera';
		$trainingplans['title']			= 'Planes de Entrenamiento';
		$grand_prix['title']			= 'Medio Marat&oacute;n Grand Prix ';
		$general_info_txt				= 'Informaci&oacute;n B&aacute;sica';

	} elseif( $qt_lang['lang']	== 'fr' ) {
		if( in_array( $event_info->event_slug, $northamerican_array ) ) {
			$how_to_register['title']		= 'Modalit&#233;s d&#8217;inscription';
			$confirmation['title']			= 'Confirmer votre inscription';
			$personalize_bib['title']		= 'Personnaliser votre dossard';
			$packet_pickup['title']			= 'R&eacute;cup&eacute;rer votre dossard';
			$corral_changes['title']		= 'Changement de corral';
			$race_rules['title']			= 'R&#232;glements de la course';
			$directions['title']			= 'Instructions';
			$parking['title']				= 'Stationnement';
			$road_closures['title']			= 'Fermetures de rues';
			$gear_check['title']			= 'D&#233;p&#244;t des sacs';
			$wave_start['title']			= 'D&#233;part par vagues/les corrals';
			$time_limits['title']			= 'Limites de temps';
			$course_support['title']		= 'Stations d&#8217;aide &#224; la course';
			$charity['title']				= 'Organismes de bienfaisance';
			$volunteer['title']				= 'B&#233;n&#233;volat';
			$competitor_wireless['title']	= 'Runner Tracking';
			$secure_zone['title']			= 'Zone s&#233;curis&#233;e';
			$family_reunion['title']		= 'Aire de retrouvailles';
			$medal_info['title']			= 'Renseignements sur les services m&#233;dicaux';
			$age_group['title']				= 'Prix';
			$medical['title']				= 'Premiers soins';
			$info_booth['title']			= 'Kiosques d&#8217;information et objets trouv&#233;s';
			$race_changes['title']			= 'Changement de distance';
			$pace_team['title']				= '&Eacute;quipe des meneurs d\'allure';
			$trainingplans['title']			= 'Training Plans';
		} else {	// euro french
			$how_to_register['title']		= 'Comment s\'inscrire?';
			$confirmation['title']			= 'Confirmation';
			$personalize_bib['title']		= 'Personnalisez votre dossard';
			$packet_pickup['title']			= 'Distribution des kits';
			$corral_changes['title']		= 'Changements de sas de d&eacute;part';
			$race_rules['title']			= 'R&egrave;gles de la course';
			$directions['title']			= 'Itin&eacute;raires';
			$parking['title']				= 'Parking et transports';
			$road_closures['title']			= 'Fermetures de routes';
			$gear_check['title']			= 'Consigne pour bagages et &eacute;quipements ';
			$wave_start['title']			= 'D&eacute;part par vagues';
			$time_limits['title']			= 'Barri&egrave;res horaires';
			$course_support['title']		= 'Assistance sur le parcours';
			$charity['title']				= '&OElig;uvre caritative';
			$volunteer['title']				= 'B&eacute;n&eacute;vole';
			$competitor_wireless['title']	= 'Communication sans fil Competitor';
			$secure_zone['title']			= 'Zone s&eacute;curis&eacute;e';
			$family_reunion['title']		= 'Regroupement des familles';
			$medal_info['title']			= 'Informations sur les m&eacute;dailles';
			$age_group['title']				= 'Prix par cat&eacute;gorie d\'&acirc;ge';
			$medical['title']				= 'Services m&eacute;dicaux';
			$info_booth['title']			= 'Stands d\'information et Objets trouv&eacute;s';
			$race_changes['title']			= 'Changements de course';
			$pace_team['title']				= 'Meneurs d\'allure';
			$trainingplans['title']			= 'Programmes d\'entra&icirc;nement';
		}
		$deferral['title']				= 'Report d\'inscription';
		$cancellations['title']			= 'Annulations';
		$race_timing['title']			= 'Chronom&#233;trage de la course';
		$marathon_jacket['title']		= 'Manteau de Finissant du Marathon';
		$txt_before_race				= "AVANT LA COURSE";
		$txt_race_day					= "LE JOUR DE LA COURSE";
		$txt_after_race					= "APR&#200;S LA COURSE";
		$wedding['title']				= 'Course au mariage';
		$grand_prix['title']			= 'Grand Prix semi-marathon';
		$general_info_txt				= 'Renseignements g&eacute;n&eacute;raux';
		$allday_20k['title']			= 'All Day 20K Lounge';

	} elseif( $qt_lang['lang']	== 'pt' ) {
		$how_to_register['title']		= 'Como se inscrever';
		$confirmation['title']			= 'Confirma&ccedil;&atilde;o';
		$personalize_bib['title']		= 'Personalizar a sua Bib';
		$packet_pickup['title']			= 'Levantamento do kit de participa&ccedil;&atilde;o';
		$corral_changes['title']		= 'Altera&ccedil;&otilde;es do curral';
		$deferral['title']				= 'Adiamento';
		$cancellations['title']			= 'Cancelamentos';
		$race_rules['title']			= 'Regras da corrida';
		$directions['title']			= 'Dire&ccedil;&otilde;es';
		$parking['title']				= 'Estacionamento e transporte';
		$road_closures['title']			= 'Encerramento de estradas';
		$allday_20k['title']			= 'All Day 20K Lounge';
		$gear_check['title']			= 'Bagagem/Verifica&ccedil;&atilde;o de material';
		$wave_start['title']			= 'Iniciar Onda';
		$time_limits['title']			= 'Limites de Tempo';
		$course_support['title']		= 'Apoio ao Curso';
		$race_timing['title']			= 'Cronometragem da corrida';
		$charity['title']				= 'Caridade';
		$volunteer['title']				= 'Volunt&aacute;rio';
		$competitor_wireless['title']	= 'Concorrente sem fios';
		$secure_zone['title']			= 'Zona de seguran&ccedil;a';
		$family_reunion['title']		= 'Reuni&atilde;o familiar';
		$medal_info['title']			= 'Informa&ccedil;&atilde;o sobre medalhas';
		$age_group['title']				= 'Pr&eacute;mios por grupo et&aacute;rio';
		$marathon_jacket['title']		= 'Corta vento de finalista da Maratona';
		$medical['title']				= 'M&eacute;dico';
		$info_booth['title']			= 'Cabines de informa&ccedil;&atilde;o e Perdidos e Achados';
		$race_changes['title']			= 'Altera&ccedil;&otilde;es de corridas';
		$pace_team['title']				= 'Equipa de ritmo';
		$txt_before_race				= 'ANTES DA CORRIDA';
		$txt_race_day					= 'DIA DA CORRIDA';
		$txt_after_race					= 'DEPOIS DA CORRIDA';
		$wedding['title']				= 'Casamento';
		$txt_before_race				= "ANTES DA CORRIDA";
		$txt_race_day					= "DIA DA CORRIDA";
		$txt_after_race					= "DEPOIS DA CORRIDA";
		$trainingplans['title']			= 'Planos de Treino';
		$grand_prix['title']			= 'Grande Pr&eacute;mio Meia Maratona';
		$general_info_txt				= 'Informa&ccedil;&atilde;o Geral';

	} elseif( $qt_lang['lang'] == 'zh' ) {
		$how_to_register['title']		= '如何登记';
		$confirmation['title']			= '确认';
		$personalize_bib['title']		= '个性化您的围兜';
		$packet_pickup['title']			= '领取参赛包';
		$corral_changes['title']		= '围栏更改';
		$deferral['title']				= 'Deferral';
		$cancellations['title']			= '取消';
		$race_rules['title']			= '比赛规则';
		$directions['title']			= '方向';
		$parking['title']				= '停车与交通';
		$road_closures['title']			= '道路封闭';
		$allday_20k['title']			= 'All Day 20K Lounge';
		$gear_check['title']			= '行李/设备检查';
		$wave_start['title']			= '波开始';
		$time_limits['title']			= '时间限制';
		$course_support['title']		= '路线支持';
		$race_timing['title']			= '赛事计时';
		$charity['title']				= '慈善';
		$volunteer['title']				= '志愿者';
		$competitor_wireless['title']	= 'Runner Tracking';
		$secure_zone['title']			= '安全区';
		$family_reunion['title']		= '家庭团聚';
		$medal_info['title']			= '奖章信息';
		$age_group['title']				= '年龄组奖励';
		$marathon_jacket['title']		= 'Marathon Finisher Jacket';
		$medical['title']				= '医疗';
		$info_booth['title']			= '询问处和失物招领处';
		$race_changes['title']			= '比赛更改';
		$pace_team['title']				= '步行团队';
		$txt_before_race				= "赛前";
		$txt_race_day					= "比赛日";
		$txt_after_race					= "赛后";
		$wedding['title']				= '跑道上的婚礼';
		$trainingplans['title']			= '训练计划';
		$grand_prix['title']			= 'Half Marathon Grand Prix';
		$general_info_txt				= '般信息';
	} else {
		$how_to_register['title']		= 'How To Register';
		$confirmation['title']			= 'Confirmation';
		$personalize_bib['title']		= 'Personalize Your Bib';
		$packet_pickup['title']			= 'Packet Pickup';
		$corral_changes['title']		= 'Corral Changes';
		$deferral['title']				= 'Deferral';
		$cancellations['title']			= 'Cancellations';
		$race_rules['title']			= 'Race Rules';
		$directions['title']			= 'Directions';
		$parking['title']				= 'Parking &amp; Transportation';
		$road_closures['title']			= 'Road Closures';
		$allday_20k['title']			= 'All Day 20K Lounge';
		$gear_check['title']			= 'Baggage/Gear Check';
		$wave_start['title']			= 'Wave Start';
		$time_limits['title']			= 'Time Limits';
		$course_support['title']		= 'Course Support';
		$race_timing['title']			= 'Race Timing';
		$charity['title']				= 'Charity';
		$volunteer['title']				= 'Volunteer';
		$competitor_wireless['title']	= 'Runner Tracking';
		$secure_zone['title']			= 'Secure Zone';
		$family_reunion['title']		= 'Family Reunion';
		$medal_info['title']			= 'Medal Info';
		$age_group['title']				= 'Age Group Awards';
		$marathon_jacket['title']		= 'Marathon Finisher Jacket';
		$medical['title']				= 'Medical';
		$info_booth['title']			= 'Information Booths and Lost &amp; Found';
		$race_changes['title']			= 'Race Changes';
		$pace_team['title']				= 'Pace Team';
		$txt_before_race				= "BEFORE RACE";
		$txt_race_day					= "RACE DAY";
		$txt_after_race					= "AFTER RACE";
		$wedding['title']				= 'Run Through Wedding';
		$trainingplans['title']			= 'Training Plans';
		$grand_prix['title']			= 'Half Marathon Grand Prix';
		$general_info_txt				= 'General Info';

		if( !empty( $event_info ) && in_array( $event_info->event_slug, $runner_tracking_array ) ) {
			$competitor_wireless['title']	= 'Runner Tracking';
		}
		if( !empty( $event_info ) && in_array( $event_info->event_slug, $awards_array ) ) {
			$age_group['title']	= 'Awards';
		}

	}


	/**
	 * headers
	 */
	if( $qt_lang['lang']	== 'es' ) {
		$txt_register		= "Inscripci&#243;n";
		$txt_reg_closed		= "Inscripciones Cerradas";
		$txt_find_a_race	= "Encontrar Carrera";
		$run_txt			= "Corre";
		$sortby_txt			= "Ordenar Por";
		$txt_presale		= "Inscripci&#243;n";

		if( isset($event_info->event_slug) && in_array( $event_info->event_slug, $northamerican_array ) ) {
			$txt_reg_notified	= "Notif&iacute;quenme";
		} else {
			$txt_reg_notified	= "Record&aacute;dmelo";
		}

	} elseif( $qt_lang['lang']	== 'fr' ) {
		$txt_register		= "Inscription";
		$txt_reg_closed		= "Inscription close";
		$txt_find_a_race	= "Trouvez une course";
		$txt_find_an_event	= "Trouver un &#233;v&#233;nement";
		$txt_reg_notified	= "Get Notified";
		$run_txt			= "Run";
		$sortby_txt			= "Sort By";
		$txt_presale		= "Inscription";
	} elseif( $qt_lang['lang']	== 'pt' ) {
		$txt_register		= "Inscri&ccedil;&atilde;o";
		$txt_reg_closed		= "Registration Closed";
		$txt_find_a_race	= "Find A Race";
		$txt_find_an_event	= "Encontre um evento";
		$txt_reg_notified	= "Quero ser informado";
		$run_txt			= "Run";
		$sortby_txt			= "Sort By";
		$txt_presale		= "Inscri&ccedil;&atilde;o";
	} elseif( $qt_lang['lang'] == 'zh' ) {
		$txt_register		= "登记";
		$txt_reg_closed		= "注册关闭";
		$txt_find_a_race	= "赛事寻找";
		$txt_find_an_event	= "赛事寻找";
		$txt_reg_notified	= "得到通知";
		$run_txt			= "Run";
		$sortby_txt			= "分类";
		$txt_presale		= "预售";
	} else {
		$txt_register		= "Register";
		$txt_reg_closed		= "Registration Closed";
		$txt_find_a_race	= "Find A Race";
		$txt_find_an_event	= "Find An Event";
		$txt_reg_notified	= "Get Notified";
		$run_txt			= "Run";
		$sortby_txt			= "Sort By";
		$txt_presale		= "Presale";
	}


	/**
	 * hotels
	 */
	if( $qt_lang['lang']	== 'es' ) {
		if( isset($event_info->event_slug) && in_array( $event_info->event_slug, $northamerican_array ) ) {
			$display_text['hotel_hq']			= "Hotel sede";
			$display_text['hotels_hq']			= "Hotel sede";
			$display_text['dst_rate']			= "Tarifas de descuento";
			$display_text['rsv_onl']			= "Reserva Ahora";
			$display_text['hotels_offic']		= "Hoteles Oficiales";
			$display_text['hotel_offic']		= "Hoteles Oficiales";
			$display_text['hotels_feat']		= "Hoteles Relevantes";
			$display_text['hotel_feat']			= "Hotel Relevante";
			$display_text['more_details']		= "m&#225;s Detalles";
			$display_text['hotel_description']	= "Sobre el Hotel";
			$display_text['headquarters']		= "Sede";
		} else { // euro spanish
			$display_text['hotel_hq']			= "Hotel sede central ";
			$display_text['hotels_hq']			= "Hoteles sedes centrales";
			$display_text['dst_rate']			= "Tarifas con descuento";
			$display_text['rsv_onl']			= "Reserva online";
			$display_text['hotels_offic']		= "Hotel Oficiales";
			$display_text['hotel_offic']		= "Hoteles Oficial";
			$display_text['hotels_feat']		= "Hoteles Destacados";
			$display_text['hotel_feat']			= "Hotel Destacado";
			$display_text['more_details']		= "M&aacute;s informaci&oacute;n";
			$display_text['hotel_description']	= "Descripci&oacute;n del hotel";
			$display_text['headquarters']		= "Sede central";
		}
		$display_text['official']			= "Oficial";
		$display_text['sold_out']			= "Agotado";
		$display_text['dst_from']			= "Distancia desde";
		$display_text['westin']				= "Westin VIP Marathon Package";
		$display_text['westin_btn']			= "Book Now";
		$display_text['phone']				= "Tel&#233;fono";
		$display_text['aslowas']			= "as low as";
		$display_text['expo']				= "Expo";
		$display_text['price']				= "Precio";
		$display_text['low_high']			= "Menos a M&aacute;s";
		$display_text['high_low']			= "M&aacute;s a Menos";
		$display_text['fivek_start_line']	= "L&iacute;nea de Salida 5K";
		$display_text['fivek_finish_line']	= "L&iacute;nea de Meta 5K";
		$display_text['tenk_start_line']	= "L&iacute;nea de Salida 10K";
		$display_text['tenk_finish_line']	= "L&iacute;nea de Meta 10K";
		$display_text['half_start_line']	= "L&iacute;nea de Salida Medio Marat&oacute;n";
		$display_text['half_finish_line']	= "L&iacute;nea de Meta Medio Marat&oacute;n";
		$display_text['full_start_line']	= "L&iacute;nea de Salida Marat&oacute;n";
		$display_text['full_finish_line']	= "L&iacute;nea de Meta Marat&oacute;n";
		$display_text['show_map']			= "Ense&ntilde;ar el Mapa";
		$display_text['westin_featured']	= "Westin Featured";

		$headquarter_initials	= 'H';
		$westin_initials		= 'F';
		$official_initials		= 'O';
		$startline_initials		= 'SL';
		$finishline_initials	= 'FL';
		$halfmarathon_initials	= 'hm';

	} elseif( $qt_lang['lang']	== 'fr' ) {
		if( in_array( $event_info->event_slug, $northamerican_array ) ) {
			$display_text['hotel_hq']			= "H&#244;tel Headquarter";
			$display_text['hotels_hq']			= "H&#244;tels Headquarter";
			$display_text['dst_from']			= "Distance de ";
			$display_text['dst_rate']			= "Tarif r&#233;duit";
			$display_text['sold_out']			= "&#233;puis&#233;";
			$display_text['hotels_feat']		= "H&#244;tels partenaires";
			$display_text['hotel_feat']			= "&Agrave; propos de l'H&#244;tel partenaire";
			$display_text['more_details']		= "D&#233;tails";
			$display_text['hotel_description']	= "A propos de l'H&#244;tel ";
			$display_text['headquarters']		= "Headquarter";
		} else {	// euro french
			$display_text['hotel_hq']			= "H&ocirc;tel d'accueil officiel";
			$display_text['hotels_hq']			= "H&ocirc;tels d'accueil officiels";
			$display_text['dst_from']			= "Distance depuis";
			$display_text['dst_rate']			= "Tarifs r&#233;duits ";
			$display_text['sold_out']			= "Complet";
			$display_text['hotels_feat']		= "H&ocirc;tels choisis";
			$display_text['hotel_feat']			= "H&circ;tel choisi";
			$display_text['more_details']		= "Plus de d&#233;tails";
			$display_text['hotel_description']	= "Description de l'h&ocirc;tel";
			$display_text['headquarters']		= "d'accueil officiel";
		}
		$display_text['rsv_onl']		= "R&eacute;servez en ligne";
		$display_text['hotels_offic']	= "H&#244;tels officiels";
		$display_text['hotel_offic']	= "H&#244;tel officiel";
		$display_text['phone']			= "T&#233;l&#233;phone ";
		$display_text['westin']			= "Forfait marathon VIP Westin";
		$display_text['westin_btn']		= "R&eacute;server maintenant";
		$display_text['aslowas']		= "pour aussi peu que";
		$display_text['official']		= "Officiel";
		$display_text['expo']				= "Expo";
		$display_text['price']				= "Prix";
		$display_text['low_high']			= "Bas &agrave; &eacute;lev&eacute;";
		$display_text['high_low']			= "&Eacute;lev&eacute; &agrave; bas";
		$display_text['fivek_start_line']	= "Ligne de d&eacute;part du 5 km";
		$display_text['fivek_finish_line']	= "Ligne d'arriv&eacute;e du 5 km";
		$display_text['tenk_start_line']	= "Ligne de d&eacute;part du 10 km";
		$display_text['tenk_finish_line']	= "Ligne d'arriv&eacute;e du 10 km";
		$display_text['half_start_line']	= "Ligne de d&eacute;part du demi-marathon";
		$display_text['half_finish_line']	= "Ligne d'arriv&eacute;e du demi-marathon";
		$display_text['full_start_line']	= "Ligne de d&eacute;part du marathon";
		$display_text['full_finish_line']	= "Ligne d'arriv&eacute;e du marathon";
		$display_text['show_map']			= "Voir le plan";
		$display_text['westin_featured']	= "Par Westin";

		$headquarter_initials	= 'QG';
		$westin_initials		= 'W';
		$official_initials		= 'O';
		$startline_initials		= 'D';
		$finishline_initials	= 'A';
		$halfmarathon_initials	= 'dm';

	} elseif( $qt_lang['lang']	== 'pt' ) {
		$display_text['hotel_hq']			= "Sede do Hotel";
		$display_text['hotels_hq']			= "Sede dos Hot&eacute;is ";
		$display_text['dst_from']			= "Dist&acirc;ncia de";
		$display_text['dst_rate']			= "Taxas de desconto";
		$display_text['sold_out']			= "Esgotado";
		$display_text['rsv_onl']			= "Reservar Online";
		$display_text['hotels_offic']		= "Hot&eacute;is oficiais";
		$display_text['hotel_offic']		= "Hotel oficial";
		$display_text['hotels_feat']		= "Hot&eacute;is indicados";
		$display_text['hotel_feat']			= "Hotel indicado";
		$display_text['more_details']		= "Mais detalhes";
		$display_text['hotel_description']	= "Descri&ccedil;&atilde;o do hotel";
		$display_text['phone']				= "Telefone";
		$display_text['westin']				= "Westin VIP Marathon Package";
		$display_text['westin_btn']			= "Book Now";
		$display_text['aslowas']			= "as low as";
		$display_text['headquarters']		= "Sede";
		$display_text['official']			= "Oficial";
		$display_text['expo']				= "Expo";
		$display_text['price']				= "Pre&ccedil;o";
		$display_text['low_high']			= "Do mais baixo para o mais alto ";
		$display_text['high_low']			= "Do mais alto para o mais baixo";
		$display_text['fivek_start_line']	= "Partida dos 5Km";
		$display_text['fivek_finish_line']	= "Meta dos 5Km";
		$display_text['tenk_start_line']	= "Partida dos 10Km";
		$display_text['tenk_finish_line']	= "Meta dos 10Km";
		$display_text['half_start_line']	= "Partida da Meia Maratona";
		$display_text['half_finish_line']	= "Meta da Meia Maratona";
		$display_text['full_start_line']	= "Partida da Maratona";
		$display_text['full_finish_line']	= "Meta da Maratona";
		$display_text['show_map']			= "Mostrar Mapa";
		$display_text['westin_featured']	= "Westin Featured";

		$headquarter_initials	= 'H';
		$westin_initials		= 'F';
		$official_initials		= 'O';
		$startline_initials		= 'SL';
		$finishline_initials	= 'FL';
		$halfmarathon_initials	= 'hm';

	} elseif( $qt_lang['lang'] == 'zh' ) {
		$display_text['hotel_hq']			= "总部酒店";
		$display_text['hotels_hq']			= "总部酒店";
		$display_text['dst_from']			= "离...的距离";
		$display_text['dst_rate']			= "折扣价";
		$display_text['sold_out']			= "售罄";
		$display_text['rsv_onl']			= "在线预订";
		$display_text['hotels_offic']		= "官方酒店";
		$display_text['hotel_offic']		= "官方酒店";
		$display_text['hotels_feat']		= "特色酒店";
		$display_text['hotel_feat']			= "特色酒店";
		$display_text['more_details']		= "更多详情";
		$display_text['hotel_description']	= "酒店描述";
		$display_text['phone']				= "电话";
		$display_text['westin']				= "Westin VIP Marathon Package";
		$display_text['westin_btn']			= "现在预定";
		$display_text['aslowas']			= "尽量低";
		$display_text['headquarters']		= "Headquarter";
		$display_text['official']			= "Official";
		$display_text['expo']				= "展会";
		$display_text['price']				= "价格";
		$display_text['low_high']			= "低到高";
		$display_text['high_low']			= "高到低";
		$display_text['fivek_start_line']	= "5公里起点";
		$display_text['fivek_finish_line']	= "5公里终点";
		$display_text['tenk_start_line']	= "10公里起点";
		$display_text['tenk_finish_line']	= "10公里终点";
		$display_text['half_start_line']	= "半马起点";
		$display_text['half_finish_line']	= "半马终点";
		$display_text['full_start_line']	= "全马起点";
		$display_text['full_finish_line']	= "全马终点";
		$display_text['show_map']			= "地图展示";
		$display_text['westin_featured']	= "威斯汀特点的";

		$headquarter_initials	= 'H';
		$westin_initials		= 'F';
		$official_initials		= 'O';
		$startline_initials		= 'SL';
		$finishline_initials	= 'FL';
		$halfmarathon_initials	= 'hm';

	} else {
		$display_text['hotel_hq']			= "Headquarter Hotel";
		$display_text['hotels_hq']			= "Headquarter Hotels";
		$display_text['dst_from']			= "Distance from";
		$display_text['dst_rate']			= "Discounted Rates";
		$display_text['sold_out']			= "Sold Out";
		$display_text['rsv_onl']			= "Reserve Online";
		$display_text['hotels_offic']		= "Official Hotels";
		$display_text['hotel_offic']		= "Official Hotel";
		$display_text['hotels_feat']		= "Featured Hotels";
		$display_text['hotel_feat']			= "Featured Hotel";
		$display_text['more_details']		= "More Details";
		$display_text['hotel_description']	= "Hotel Description";
		$display_text['phone']				= "Phone";
		$display_text['westin']				= "Westin VIP Marathon Package";
		$display_text['westin_btn']			= "Book Now";
		$display_text['aslowas']			= "as low as";
		$display_text['headquarters']		= "Headquarter";
		$display_text['official']			= "Official";
		$display_text['expo']				= "Expo";
		$display_text['price']				= "Price";
		$display_text['low_high']			= "Low to High";
		$display_text['high_low']			= "High to Low";
		$display_text['fivek_start_line']	= "5K Start Line";
		$display_text['fivek_finish_line']	= "5K Finish Line";
		$display_text['tenk_start_line']	= "10K Start Line";
		$display_text['tenk_finish_line']	= "10K Finish Line";
		$display_text['half_start_line']	= "Half Marathon Start Line";
		$display_text['half_finish_line']	= "Half Marathon Finish Line";
		$display_text['full_start_line']	= "Marathon Start Line";
		$display_text['full_finish_line']	= "Marathon Finish Line";
		$display_text['show_map']			= "Show Map";
		$display_text['westin_featured']	= "Westin Featured";

		$headquarter_initials	= 'H';
		$westin_initials		= 'F';
		$official_initials		= 'O';
		$startline_initials		= 'SL';
		$finishline_initials	= 'FL';
		$halfmarathon_initials	= 'hm';
	}


	/**
	 * humana event
	 */
	if( is_page_template( 'humana-event.php' ) || is_page_template( 'humana-home.php' ) ) {
		if( $qt_lang['lang']	== 'es' ) {
			$registration['title']	= 'Registration / Details';
			$tuneup['title']			= 'Tune Up for '.get_post_meta($post->ID, '_rnr3_tune_up_for', TRUE);
			$training['title']			= 'Training';
			$coursemap['title']			= 'Course Map';
			$parking['title']			= 'Parking Information';
			$postrace['title']			= 'Post-Race Festival';
			$contact['title']			= 'Contact';
			$startwithhealthy['title']	= '#StartWithHealthy';
			$packet['title']			= 'Packet Pickup';
			$txt_start_line				= "Inicio L&#237;nea / L&#237;nea de meta";
			// $txt_finish_line	= "L&#237;nea de meta";
			$txt_sold_out				= "Agotado";
			$txt_view					= "Vista";
			$txt_title					= "Event Information";
		} elseif( $qt_lang['lang']	== 'fr' ) {
			if( in_array( $event_info->event_slug, $northamerican_array ) ) {
				$txt_start_line			= "Lieu du d&#233;part / Site d&#8217;arriv&#233;e";
			} else {
				$txt_start_line			= "Ligne de d&eacute;part/d'arriv&eacute;e";
			}
			$registration['title']		= 'Inscription/D&eacute;tails';
			$tuneup['title']			= 'Pr&eacute;parez-vous pour '.get_post_meta($post->ID, '_rnr3_tune_up_for', TRUE);;
			$training['title']			= 'Entra&icirc;nement';
			$coursemap['title']			= 'Plan du parcours';
			$parking['title']			= 'Informations sur le parking';
			$postrace['title']			= 'Festival de fin de course';
			$contact['title']			= 'Contact';
			$startwithhealthy['title'] 	= '#StartWithHealthy';
			$packet['title']			= 'Distribution des kits';
			// $txt_finish_line	= "Site d&#8217;arriv&#233;e ";
			$txt_sold_out				= "Complet";
			$txt_title					= "Informations sur l'&eacute;v&eacute;nement";
		} elseif( $qt_lang['lang']	== 'pt' ) {
			$registration['title']		= 'Registration / Details';
			$tuneup['title']			= 'Tune Up for '.get_post_meta($post->ID, '_rnr3_tune_up_for', TRUE);
			$training['title']			= 'Training';
			$coursemap['title']			= 'Course Map';
			$parking['title']			= 'Parking Information';
			$postrace['title']			= 'Post-Race Festival';
			$contact['title']			= 'Contact';
			$startwithhealthy['title']	= '#StartWithHealthy';
			$packet['title']			= 'Packet Pickup';
			$txt_start_line				= "Linha de Partida / Linha de Chegada";
			// $txt_finish_line	= "Linha de Chegada";
			$txt_sold_out				= "Sold Out";
			$txt_title					= "Event Information";
		} else {
			$registration['title']		= 'Registration / Details';
			$custom_content['title']	= get_post_meta( $post->ID, '_rnr3_custom_title', 1 );
			$tuneup['title']			= 'Tune Up for '.get_post_meta($post->ID, '_rnr3_tune_up_for', TRUE);
			$training['title']			= 'Training';
			$coursemap['title']			= 'Course Map';
			$parking['title']			= 'Parking Information';
			$postrace['title']			= 'Post-Race Festival';
			$contact['title']			= 'Contact';
			$startwithhealthy['title']	= '#StartWithHealthy';
			$packet['title']			= 'Packet Pickup';
			$txt_start_line				= "Start/Finish Line";
			// $txt_finish_line	= "Finish Line";
			$txt_sold_out				= "Sold Out";
			$txt_title					= "Event Information";
		}
	}


	/**
	 * partners series
	 */
	if( $qt_lang['lang']	== 'es' ) {
		$overview_txt	= 'Panorama';
	} elseif( $qt_lang['lang']	== 'fr' ) {
		$overview_txt	= 'Aper&ccedil;u';
	} elseif( $qt_lang['lang']	== 'pt' ) {
		$overview_txt	= 'Vis&atilde;o global';
	} else {
		$overview_txt	= 'Overview';
	}


	/**
	 * register
	 */
	if( $qt_lang['lang']	== 'es' ) {	// spanish
		$dias					= array( "domingo", "lunes", "martes", "miercoles", "jueves", "viernes", "s&aacute;bado" );
		$meses					= array( "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" );
		$choose_1_text			= 'Elegir uno';
		$individual_text		= 'Individual';
		$through_text			= 'A trav&eacute;s de';
		$price_disclaimer		= '* El precio es susceptible de incrementarse en cualquier momento y sin previo aviso.';
		$register_free_btn_text	= 'Register Free';
		$commit_price_open		= 'Open Fundraiser';
		$youll_get_txt			= 'You&rsquo;ll get';
		$txt_sold_out			= "Agotado";
		$txt_pr	= "Los participantes reciben";

		if( isset($event_info->event_slug) && in_array( $event_info->event_slug, $northamerican_array ) ) {
			$online_reg_closed_text	= 'Registro en l&iacute;nea est&aacute; cerrado';
			// $tpna_text			= 'Pase tour norte Am&eacute;rica';
			// $tp3p_text			= 'Pase tour triple';
			// $tpg_text			= 'Pase tour global';
			$festival_wkd_text		= 'Paquete festival fin de semana: con ';
			$at_expo_text			= 'En la Expo (si hay disponibilidad)';
		} else {	// euro spanish
			$online_reg_closed_text	= 'Registro en l&iacute;nea cerrado';
			// $tpna_text				= 'Portador de TourPass North America';
			// $tp3p_text				= 'Portador de TourPass 3-Pack';
			// $tpg_text				= 'Portador de TourPass Global';
			$festival_wkd_text		= 'Paquete para fiesta de fin de semana: Con ';
			$at_expo_text			= 'En la Expo si est&aacute; disponible';
		}
	} elseif( $qt_lang['lang']	== 'fr' ) {	// french
		if( in_array( $event_info->event_slug, $northamerican_array ) ) {
			$choose_1_text		= 'S&eacute;lectionner une option';
			$individual_text	= 'Inscription individuelle';
			// $tpna_text		= 'D&eacute;tenteur d\'un laisser-passer/ Am&eacute;rique du nord';
			// $tp3p_text		= 'D&eacute;tenteur d\'un laisser-passer valide pour 3 &eacute;v&eacute;nements';
			// $tpg_text		= 'D&eacute;tenteur d\'un passeport global';
			$festival_wkd_text	= 'Festival Weekend Bundle: avec ';
			$at_expo_text		= '&agrave; l\'Expo, si l\'activit&eacute; n\'affiche pas compl&egrave;te';
			$price_disclaimer	= '* Les tarifs sont sujets &agrave; changement sans pr&eacute;avis.';
			$txt_pr				= "Services pour les participants";
		} else {	// euro french
			$choose_1_text		= 'S&eacute;lectionnez';
			$individual_text	= 'Individuelle';
			// $tpna_text		= 'D&eacute;tenteur du TourPass Am&eacute;rique du Nord';
			// $tp3p_text		= 'D&eacute;tenteur du TourPass triple';
			// $tpg_text		= 'D&eacute;tenteur du TourPass international';
			$festival_wkd_text	= 'Forfait weekend festival : avec ';
			$at_expo_text		= '&Agrave; l\'expo si disponible';
			$txt_pr				= 'Les participants re&ccedil;oivent';
		}
		$journees				= array( 'dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi' );
		$mois					= array( 'janvier', 'f&eacute;vrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'ao&ucirc;t', 'septembre', 'octobre', 'novembre', 'd&eacute;cembre' );
		$online_reg_closed_text	= 'L\'inscription en ligne est ferm&eacute;e';
		$through_text			= 'jusqu\'au';
		$register_free_btn_text	= 'Inscription gratuite';
		$commit_price_open		= 'Collecte de fonds ouverte';
		$youll_get_txt			= 'Ce que vous obtiendrez';
		$txt_sold_out			= "Complet";
		$price_disclaimer		= '* Le prix est susceptible d\'augmenter &agrave; tout moment sans pr&eacute;avis.';
	} elseif( $qt_lang['lang']	== 'pt' ) {	// portuguese
		$price_disclaimer		= '* Sujeito a altera&ccedil;&otilde;es sem aviso pr&eacute;vio.';
		$register_free_btn_text	= 'Register Free';
		$commit_price_open		= 'Open Fundraiser';
		$youll_get_txt			= 'You&rsquo;ll get';
		$txt_sold_out			= "Sold Out";
		$through_text			= 'atrav&eacute;s';
		$at_expo_text			= 'Na Expo se dispon&iacute;vel';
	} elseif( $qt_lang['lang'] == 'zh' ) {
		$online_reg_closed_text	= '在线登记已关闭';
		$choose_1_text			= '选择一个';
		$individual_text		= '个人';
		// $tpna_text			= 'TourPass North America Holder';
		// $tp3p_text			= 'TourPass 3-Pack Holder';
		// $tpg_text			= 'TourPass Global Holder';
		$festival_wkd_text		= '节日周末套票：和 ';
		$through_text			= '通过';
		$at_expo_text			= '在展览会上（如果可用）';
		$price_disclaimer		= '* 价格会根据客观条件增长';
		$register_free_btn_text	= '注册费用';
		$commit_price_open		= 'Open Fundraiser';
		$youll_get_txt			= '你将会得到';
		$txt_sold_out			= "售罄";
		$txt_pr					= "参与者会收到";
	} else {	// english
		$online_reg_closed_text	= 'Online Registration Closed';
		$choose_1_text			= 'Choose One';
		$individual_text		= 'Individual';
		// $tpna_text			= 'TourPass North America Holder';
		// $tp3p_text			= 'TourPass 3-Pack Holder';
		// $tpg_text			= 'TourPass Global Holder';
		$festival_wkd_text		= 'Festival Weekend Bundle: With ';
		$through_text			= 'through';
		$at_expo_text			= 'At Expo If Available';
		$price_disclaimer		= '* Price is subject to increase at any time without notice.';
		$register_free_btn_text	= 'Register Free';
		$commit_price_open		= 'Open Fundraiser';
		$youll_get_txt			= 'You&rsquo;ll get';
		$txt_sold_out			= "Sold Out";
		$txt_pr					= "Participants Receive";
	}



	/**
	 * sidebar
	 */
	if( $qt_lang['lang'] == 'es' ) {
		$recent_sidebar_txt		= 'Recent';
		$postedin_sidebar_txt	= 'Posted in';
	} elseif( $qt_lang['lang'] == 'fr' ) {
		$recent_sidebar_txt		= 'R&eacute;cent';
		$postedin_sidebar_txt	= 'Affich&eacute; dans';
	} elseif( $qt_lang['lang'] == 'pt' ) {
		$recent_sidebar_txt		= 'Recent';
		$postedin_sidebar_txt	= 'Posted in';
	} else {
		$recent_sidebar_txt		= 'Recent';
		$postedin_sidebar_txt	= 'Posted in';
	}


	/**
	 * site home
	 */
	if( $qt_lang['lang']	== 'es' ) {
		$text_promo				= "Promociones";
		$text_news				= "Noticias";
		$text_news_promo		= "Noticias y promociones";
		$text_news_goto			= "NOTICIAS M&#225;s";
		$text_news_goto_promo	= "PROMCIONES M&#225;s";
		$text_connect			= "Con&#233;ctate";
		$text_subscribe			= "Suscr&#237;bete";
		$text_choose_lang		= "Elegir idioma";
		$text_subscribe_h3		= "Para recibir las &#250;ltimas noticas sobre Rock 'n' Roll ciudad de M&#233;xico";
		$text_subscribe_sign_up	= "registra tu direcci&#243;n de correo electr&#243;nico";
		$text_schedule			= "PROGRAMA DE EVENTOS";
		$text_see_all			= "Ver Todo";
		$text_stay_tuned		= "pr&oacute;ximamente...";
		$text_more_info			= "m&#225;s Informaci&#243;n";
		$text_enter_site		= "Acceder La P&aacute;gina Completa";
	} elseif( $qt_lang['lang']	== 'fr' ) {
		if( in_array( $event_info->event_slug, $northamerican_array ) ) {
			$text_promo				= "Actualit&#233;s";
			$text_news_promo		= "Nouvelles et actualit&#233;s";
			$text_news_goto			= "Consulter toutes les actualit&#233;s";
			$text_news_goto_promo	= "Consulter toutes les promotions";
			$text_choose_lang		= "Choisir la langue";
			$text_subscribe_sign_up	= "S'inscrire maintenant";
			$text_schedule			= "HORAIRE";
			$text_see_all			= "Voir plus";
			$text_stay_tuned		= "&#192; Venir...";
			$text_more_info			= "Renseignements";
			$text_enter_site		= "Entrer Complet Site";
		} else {
			$text_promo				= "Promotions";
			$text_news_promo		= "Nouvelles et promotions";
			$text_news_goto			= "Aller &agrave; NOUVELLES";
			$text_news_goto_promo	= "Aller &agrave; PROMOTIONS";
			$text_choose_lang		= "S&eacute;lectionner une langue";
			$text_subscribe_sign_up	= "S'inscrire d&egrave;s maintenant";
			$text_schedule			= "Programme des &eacute;v&eacute;nements";
			$text_see_all			= "Tout afficher";
			$text_stay_tuned		= "Restez &agrave; l'&eacute;coute...";
			$text_more_info			= "En savoir plus";
			$text_enter_site		= "Acc&eacute;der au site complet";
		}
		$text_news			= "Nouvelles";
		$text_connect		= "Se connecter";
		$text_subscribe		= "S'abonner";
		$text_subscribe_h3	= "Je souhaite &#234;tre inform&#233;(e) des nouveaut&#233;s sur le Marathon Rock'n'Roll de Montr&#233;al";
	} elseif( $qt_lang['lang']	== 'pt' ) {
		$text_promo				= "Promo&ccedil;&otilde;es";
		$text_news				= "Not&iacute;cias";
		$text_news_promo		= "Not&iacute;cias E Promo&ccedil;&otilde;es";
		$text_news_goto			= "Ir para NOT&Iacute;CIAS";
		$text_news_goto_promo	= "Ir para PROMO&Ccedil;&Otilde;ES";
		$text_connect			= "Ligar";
		$text_subscribe			= "Subscrever";
		$text_choose_lang		= "Escolha a L&iacute;ngua";
		$text_schedule			= "Programa Das Provas";
		$text_see_all			= "Ver Tudo";
		$text_stay_tuned		= "Fique atento...";
		$text_more_info			= "Mais Informa&ccedil;&atilde;o";
		$text_enter_site		= "Acesse Pagina Completa";
	} elseif( $qt_lang['lang'] == 'zh' ) {
		$text_promo				= "促销";
		$text_news				= "新闻";
		$text_news_promo		= "新闻和推广";
		$text_news_goto			= "前往新闻";
		$text_news_goto_promo	= "前往推广";
		$text_connect			= "联系";
		$text_subscribe			= "订阅";
		$text_choose_lang		= "选择语言";
		$text_subscribe_h3		= "STAY UP TO DATE ON ROCK 'N' ROLL MEXICO CITY";
		$text_subscribe_sign_up	= "现在注册";
		$text_schedule			= "赛事日程";
		$text_see_all			= "全部浏览";
		$text_stay_tuned		= "保留音乐";
		$text_more_info			= "更多消息";
		$text_enter_site		= "进入主页面";
	} else {
		$text_promo				= "Promotions";
		$text_news				= "News";
		$text_news_promo		= "News &amp; Promotions";
		$text_news_goto			= "Go to NEWS";
		$text_news_goto_promo	= "Go to PROMOTIONS";
		$text_connect			= "Connect";
		$text_subscribe			= "Subscribe";
		$text_choose_lang		= "Choose Language";
		$text_subscribe_h3		= "STAY UP TO DATE ON ROCK 'N' ROLL MEXICO CITY";
		$text_subscribe_sign_up	= "Sign Up Now";
		$text_schedule			= "Schedule of Events";
		$text_see_all			= "See All";
		$text_stay_tuned		= "Stay Tuned...";
		$text_more_info			= "More Info";
		$text_enter_site		= "Enter Full Site";
	}


	/**
	 * travel
	 */
	if( $qt_lang['lang']	== 'es' ) {
		$display_text['address']				= "Direcci&oacute;n";
		$display_text['phone']					= "Tel&eacute;fono";
		$display_text['raceday_parking']		= "Transporte &amp; Estacionamiento el d&iacute;a de la carrera";
		$display_text['getting_around']			= "Alrrededores de la Ciudad";
		$display_text['choose_race_location']	= "Elige Lugar de la Carrera";
		$display_text['headquarter']			= "Oficinas";
		$display_text['breakfast']				= "Desayuno";
		$display_text['shuttle']				= "Transporte Complementario";
		$display_text['travel']					= "Viaje";

	} elseif( $qt_lang['lang']	== 'fr' ) {
		$display_text['address']				= "Adresse ";
		$display_text['phone']					= "T&eacute;l&eacute;phone ";
		$display_text['raceday_parking']		= "Stationnement et transports le jour de la course";
		$display_text['getting_around']			= "Se d&eacute;placer dans la ville";
		$display_text['choose_race_location']	= "Choisissez un lieu pour votre course";
		$display_text['headquarter']			= "Quartier g&eacute;n&eacute;ral";
		$display_text['breakfast']				= "D&eacute;jeuner offert";
		$display_text['shuttle']				= "Navette gratuite";
		$display_text['travel']					= "Voyage";

	} elseif( $qt_lang['lang']	== 'pt' ) {
		$display_text['address']				= "Endere&ccedil;o ";
		$display_text['phone']					= "Telefone ";
		$display_text['raceday_parking']		= "Parque dia da prova e transporte";
		$display_text['getting_around']			= "Visite a cidade";
		$display_text['choose_race_location']	= "Escolha localiza&ccedil;&atilde;o da prova";
		$display_text['headquarter']			= "Sede";
		$display_text['breakfast']				= "Pequeno Almo&ccedil;o inclu&iacute;do";
		$display_text['shuttle']				= "Transporte Gratuito";
		$display_text['travel']					= "Travel";

	} elseif( $qt_lang['lang'] == 'zh' ) {
		$display_text['address']				= "地址";
		$display_text['phone']					= "电话";
		$display_text['raceday_parking']		= "赛事当天停车&交通";
		$display_text['getting_around']			= "城市周边";
		$display_text['choose_race_location']	= "选择赛事地";
		$display_text['headquarter']			= "Headquarter";
		$display_text['breakfast']				= "早餐提供";
		$display_text['shuttle']				= "免费大巴";
		$display_text['travel']					= "旅行";
	} else {
		$display_text['address']				= "Address";
		$display_text['phone']					= "Phone";
		$display_text['raceday_parking']		= "Race Day Parking &amp; Transportation";
		$display_text['getting_around']			= "Getting Around the City";
		$display_text['choose_race_location']	= "Choose Race Location";
		$display_text['headquarter']			= "Headquarter";
		$display_text['breakfast']				= "Breakfast Offered";
		$display_text['shuttle']				= "Complimentary Shuttle";
		$display_text['travel']					= "Travel";

	}


	/**
	 * vip
	 */

	/**
	 * exception for montreal vip page
	 */
	if( !empty( $event_info->event_slug ) && $event_info->event_slug == 'montreal' ) {
		if( $qt_lang['lang'] == 'fr' ) {
			$vip_program_title		= 'Espace confort Oasis';
			$participant_title		= 'L\'Espace confort &agrave; la ligne d\'arriv&eacute;e inclut';
			$overview_title			= 'Description';
			$vip_overview_title		= 'Description';
			$terms['title']			= 'Termes et conditions';
			$vip_type_title['vip']	= 'Forfait Espace confort Oasis ';
			$purchase['title']		= 'Ajouter Le Forfait';

			// amenities
			$vip_amenities_lang['buffet']		= 'Buffet avec traiteur';
			$vip_amenities_lang['restrooms']	= 'Toilettes r&eacute;serv&eacute;es';
			$vip_amenities_lang['bar']			= 'Bar';
			$vip_amenities_lang['gearcheck']	= 'Vestiaire r&eacute;serv&eacute;';
			$vip_amenities_lang['indoor']		= 'Acc&egrave;s &agrave; l\'Espace confort Oasis';
			$vip_amenities_lang['massage']		= 'Massage d\'apr&egrave;s course';

		} else {
			$vip_program_title		= 'Hospitality Package';
			$participant_title		= 'Forfait Espace confort Oasis';
			$overview_title			= 'Overview';
			$vip_overview_title		= 'Overview';
			$terms['title']			= 'Terms &amp; Conditions';
			$vip_type_title['vip']	= 'Espace confort Oasis Hospitality Package';
			$purchase['title']		= 'Purchase';

			// amenities
			$vip_amenities_lang['gearcheck']	= 'Private Gear Check';
			$vip_amenities_lang['buffet']		= 'Catered Buffet';
			$vip_amenities_lang['restrooms']	= 'Private Restrooms';
			$vip_amenities_lang['bar']			= 'Hospitality Package Bar';
			$vip_amenities_lang['massage']		= 'Post-Race Massage';
			$vip_amenities_lang['indoor']		= 'Private Hospitality Package';

		}
		// titles
		$overview['title']	= 'Overview';

		// participants
		$parking['title']	= 'Parking &amp; Transportation';
		$lounge['title']	= 'Private Hospitality Package ';
		$breakfast['title']	= 'Continental Breakfast';
		$brunch['title']	= 'Catered Buffet';
		$massage['title']	= 'Post-Race Massage';
		$yoga['title']		= 'Yoga Stretching Area';
		$restrooms['title']	= 'Private Restrooms';
		$gearcheck['title']	= 'Private Gear Check';
		$bar['title']		= 'Hospitality Package Bar';
		$view['title']		= 'View Overlooking the Concert Venue';
		$shuttle['title']	= 'Hospitality Package Shuttle';
		$tents['title']		= 'Post-Race Changing Tents';
		$heaters['title']	= 'Heaters';
		$swag['title']		= 'Branded Swag';

		// prerace
		$pre_private_area['title']	= 'Private Hospitality Package';
		$pre_breakfast['title']		= 'Continental Breakast';
		$pre_restrooms['title']		= 'Private Restrooms';
		$pre_gear_check['title']	= 'Private Gear Check';
		$pre_parking['title']		= 'Reserved Parking';
		$pre_yoga['title']			= 'Yoga Stretching Area';
		$pre_shuttle['title']		= 'Hospitality Package Shuttle';

		// postrace
		$post_private_area['title']		= 'Private Hospitality Package';
		$post_restrooms['title']		= 'Private Restrooms ';
		$post_buffet['title']			= 'Catered Buffet';
		$post_bar['title']				= 'Hospitality Package Bar';
		$post_parking['title']			= 'Reserved Parking';
		$post_massage['title']			= 'Massage';
		$post_changing_tents['title']	= 'Changing Tents';
		$post_shuttle['title']			= 'Hospitality Package Shuttle';
		$post_view['title']				= 'View Overlooking The Concert Venue';
		$post_concert['title']			= 'Hospitality Package Concert Party Access';
		$txt_purchase					= 'Purchase Hospitality Package Package';

		$prerace_only_title			= 'Pre-Race Only Package';
		$postrace_only_title		= 'Post-Race Only Package';
		$postrace_concert_title		= 'Post-Race Concert';
		$vip_type_title['platinum']	= 'Platinum Package';
		$vip_type_title['prerace']	= 'Pre-Race VIP Experience';
		$vip_type_title['postrace']	= 'Post-Race VIP Experience';

		$vip_amenities_lang['parking']			= 'Parking &amp; Transportation';
		$vip_amenities_lang['breakfast']		= 'Continental Breakfast';
		$vip_amenities_lang['yoga']				= 'Yoga Stretching Area';
		$vip_amenities_lang['view']				= 'View Overlooking the Concert Venue';
		$vip_amenities_lang['shuttles']			= 'Hospitality Package Shuttle';
		$vip_amenities_lang['changingtents']	= 'Post-Race Changing Tents';
		$vip_amenities_lang['heaters']			= 'Heaters';
		$vip_amenities_lang['swag']				= 'Branded Swag';
		$vip_amenities_lang['charging']			= 'VIP Charging Station';
		$vip_amenities_lang['party']			= 'Hospitality Package Concert Party Access';

	} else {
		if( $qt_lang['lang'] == 'fr' ) {
			if( in_array( $event_info->event_slug, $northamerican_array ) ) {
				$overview['title']		= 'Description';
				$overview_title			= 'Description';
				$purchase['title']		= 'Acheter l\'Espace confort';
				$terms['title']			= 'Conditions d\'achat';
				$lounge['title']		= 'L\'espace de confort &agrave; la ligne d\'arriv&eacute;e';
				$restrooms['title']		= 'Toilettes &agrave; la ligne d\'arriv&eacute;e';
				$brunch['title']		= 'Repas';
				$bar['title']			= 'Service de bar';
				$massage['title']		= 'Massage apr&egrave;s course';
				$pre_restrooms['title']	= 'Toilettes &agrave; la ligne d\'arriv&eacute;e';
				$txt_purchase			= 'Acheter l\'espace confort';
			} else {	// euro french
				$overview['title']				= 'Aper&ccedil;u';
				$overview_title					= 'Aper&ccedil;u';
				$purchase['title']				= 'Achat';
				$terms['title']					= 'Conditions g&eacute;n&eacute;rales';
				$parking['title']				= 'Parking r&eacute;serv&eacute;';
				$lounge['title']				= 'Espace VIP';
				$breakfast['title']				= 'Petit d&eacute;jeuner continental';
				$brunch['title']				= 'Buffet';
				$massage['title']				= 'Massage d\'apr&egrave;s course';
				$yoga['title']					= '&Eacute;tirements de yoga';
				$restrooms['title']				= 'Toilettes r&eacute;serv&eacute;es';
				$gearcheck['title']				= 'Consigne pour &eacute;quipement r&eacute;serv&eacute;e';
				$bar['title']					= 'Bar VIP';
				$view['title']					= 'Vue surplombant le concert';
				$shuttle['title']				= 'Navette VIP';
				$tents['title']					= 'Tentes-vestiaires d\'apr&egrave;s course';
				$heaters['title']				= 'Appareils de chauffage';
				$swag['title']					= 'Cadeaux promotionnels';
				$pre_private_area['title']		= 'Espace VIP';
				$pre_breakfast['title']			= 'Petit d&eacute;jeuner continental';
				$pre_restrooms['title']			= 'Toilettes r&eacute;serv&eacute;es';
				$pre_gear_check['title']		= 'Consigne pour &eacute;quipement r&eacute;serv&eacute;e';
				$pre_parking['title']			= 'Parking r&eacute;serv&eacute;';
				$pre_yoga['title']				= '&Eacute;tirements de yoga';
				$pre_shuttle['title']			= 'Navette VIP';
				$post_private_area['title']		= 'Espace VIP';
				$post_restrooms['title']		= 'Toilettes r&eacute;serv&eacute;es';
				$post_buffet['title']			= 'Buffet';
				$post_bar['title']				= 'Bar VIP';
				$post_parking['title']			= 'Parking r&eacute;serv&eacute;';
				$post_massage['title']			= 'Massage d\'apr&egrave;s course';
				$post_changing_tents['title']	= 'Tentes-vestiaires';
				$post_shuttle['title']			= 'Navette VIP';
				$post_view['title']				= 'Vue surplombant le concert';
				$post_concert['title']			= 'Acc&egrave;s soir&eacute;e concert VIP';
				$txt_purchase					= 'Acheter le forfait VIP';

				// amenities
				$vip_amenities_lang['parking']			= 'Parking r&eacute;serv&eacute;';
				$vip_amenities_lang['indoor']			= 'Espace VIP';
				$vip_amenities_lang['breakfast']		= 'Petit d&eacute;jeuner continental';
				$vip_amenities_lang['buffet']			= 'Buffet';
				$vip_amenities_lang['massage']			= 'Massage d\'apr&egrave;s course';
				$vip_amenities_lang['yoga']				= '&Eacute;tirements de yoga';
				$vip_amenities_lang['restrooms']		= 'Toilettes r&eacute;serv&eacute;es';
				$vip_amenities_lang['gearcheck']		= 'Consigne pour &eacute;quipement r&eacute;serv&eacute;e';
				$vip_amenities_lang['bar']				= 'Bar VIP';
				$vip_amenities_lang['view']				= 'Vue surplombant le concert';
				$vip_amenities_lang['shuttles']			= 'Navette VIP';
				$vip_amenities_lang['changingtents']	= 'Tentes-vestiaires d\'apr&egrave;s course';
				$vip_amenities_lang['heaters']			= 'Appareils de chauffage';
				$vip_amenities_lang['swag']				= 'Cadeaux promotionnels';
				$vip_amenities_lang['charging']			= 'VIP Charging Station';
				$vip_amenities_lang['party']			= 'Acc&egrave;s soir&eacute;e concert VIP';

			}
			$vip_type_title['platinum']	= 'Platinum Package';
			$vip_type_title['vip']		= 'Espace confort';
			$vip_type_title['prerace']	= 'Pre-Race VIP Experience';
			$vip_type_title['postrace']	= 'Post-Race VIP Experience';

			$vip_program_title		= 'L\'espace confort';
			$participant_title		= 'L\'Espace confort &agrave; la ligne d\'arriv&eacute;e inclut';
			$vip_overview_title		= 'Overview';
			$prerace_only_title		= 'Pre-Race Only VIP Package';
			$postrace_only_title	= 'Post-Race Only VIP Package';
			$postrace_concert_title	= 'Post-Race Concert VIP';

		} elseif( $qt_lang['lang'] == 'es' ) {
			// titles
			$overview['title']	= 'Descripci&oacute;n general';
			$overview_title		= 'Descripci&oacute;n general';
			$purchase['title']	= 'Comprar';
			$terms['title']		= 'T&eacute;rminos y condiciones';

			// participants
			$parking['title']	= 'Aparcamiento reservado';
			$lounge['title']	= '&Aacute;rea VIP privada';
			$breakfast['title']	= 'Desayuno continental';
			$brunch['title']	= 'Servicio de buf&eacute;';
			$massage['title']	= 'Masaje para despu&eacute;s de la carrera';
			$yoga['title']		= 'Estiramientos de yoga';
			$restrooms['title']	= 'Aseos privados';
			$gearcheck['title']	= 'Control de equipaci&oacute;n privado';
			$bar['title']		= 'Bar VIP';
			$view['title']		= 'Vistas al lugar de celebraci&oacute;n del concierto';
			$shuttle['title']	= 'Transporte VIP';
			$tents['title']		= 'Vestuarios para despu&eacute;s de la carrera';
			$heaters['title']	= 'Calentadores';
			$swag['title']		= 'Art&iacute;culos promocionales de marca';

			// prerace
			$pre_private_area['title']	= '&Aacute;rea VIP privada';
			$pre_breakfast['title']		= 'Desayuno continental';
			$pre_restrooms['title']		= 'Aseos privados';
			$pre_gear_check['title']	= 'Control de equipaci&oacute;n privado';
			$pre_parking['title']		= 'Aparcamiento reservado';
			$pre_yoga['title']			= 'Estiramientos de yoga';
			$pre_shuttle['title']		= 'Transporte VIP';

			// postrace
			$post_private_area['title']		= '&Aacute;rea VIP privada';
			$post_restrooms['title']		= 'Aseos privados';
			$post_buffet['title']			= 'Servicio de buf&eacute;';
			$post_bar['title']				= 'Bar VIP';
			$post_parking['title']			= 'Aparcamiento reservado';
			$post_massage['title']			= 'Masaje para despu&eacute;s de la carrera';
			$post_changing_tents['title']	= 'Vestuarios para despu&eacute;s de la carrera';
			$post_shuttle['title']			= 'Transporte VIP';
			$post_view['title']				= 'Vistas al lugar de celebraci&oacute;n del concierto';
			$post_concert['title']			= 'Acceso VIP a la fiesta del concierto';
			$txt_purchase					= 'Comprar paquete VIP';

			$vip_program_title = 'VIP';

			// amenities
			$vip_amenities_lang['parking']			= 'Aparcamiento reservado';
			$vip_amenities_lang['indoor']			= '&Aacute;rea VIP privada';
			$vip_amenities_lang['breakfast']		= 'Desayuno continental';
			$vip_amenities_lang['buffet']			= 'Servicio de buf&eacute;';
			$vip_amenities_lang['massage']			= 'Masaje para despu&eacute;s de la carrera';
			$vip_amenities_lang['yoga']				= 'Estiramientos de yoga';
			$vip_amenities_lang['restrooms']		= 'Aseos privados';
			$vip_amenities_lang['gearcheck']		= 'Control de equipaci&oacute;n privado';
			$vip_amenities_lang['bar']				= 'Bar VIP';
			$vip_amenities_lang['view']				= 'Vistas al lugar de celebraci&oacute;n del concierto';
			$vip_amenities_lang['shuttles']			= 'Transporte VIP';
			$vip_amenities_lang['changingtents']	= 'Vestuarios para despu&eacute;s de la carrera';
			$vip_amenities_lang['heaters']			= 'Calentadores';
			$vip_amenities_lang['swag']				= 'Art&iacute;culos promocionales de marca';
			$vip_amenities_lang['charging']			= 'VIP Charging Station';
			$vip_amenities_lang['party']			= 'Acceso VIP a la fiesta del concierto';

			$vip_overview_title			= 'Overview';
			$participant_title			= 'VIP Participant Amenities';
			$prerace_only_title			= 'Pre-Race Only VIP Package';
			$postrace_only_title		= 'Post-Race Only VIP Package';
			$postrace_concert_title		= 'Post-Race Concert VIP';
			$vip_type_title['platinum']	= 'Platinum Package';
			$vip_type_title['vip']		= 'VIP Experience';
			$vip_type_title['prerace']	= 'Pre-Race VIP Experience';
			$vip_type_title['postrace']	= 'Post-Race VIP Experience';

		} elseif( $qt_lang['lang'] == 'pt' ) {
			// titles
			$overview['title']	= 'Vis&atilde;o global';
			$overview_title		= 'Vis&atilde;o global';
			$purchase['title']	= 'Comprar';
			$terms['title']		= 'Termos e Condi&ccedil;&otilde;es';

			// participants
			$parking['title']	= 'Transportes';
			$lounge['title']	= '&Aacute;rea VIP privada';
			$breakfast['title']	= 'Pequeno-almo&ccedil;o continental';
			$brunch['title']	= 'Buffet servido';
			$massage['title']	= 'Massagem p&oacute;s-corrida';
			$yoga['title']		= 'Alongamento Yoga ';
			$restrooms['title']	= 'Lavabos privados';
			$gearcheck['title']	= 'Verifica&ccedil;&atilde;o privada do material';
			$bar['title']		= 'Bar VIP ';
			$view['title']		= 'Vista para o local do concerto';
			$shuttle['title']	= 'Transporte VIP ';
			$tents['title']		= 'Troca das tendas ap&oacute;s corrida';
			$heaters['title']	= 'Aquecedores';
			$swag['title']		= 'Promo&ccedil;&otilde;es da marca';

			// prerace
			$pre_private_area['title']	= '&Aacute;rea VIP privada';
			$pre_breakfast['title']		= 'Pequeno-almo&ccedil;o continental';
			$pre_restrooms['title']		= 'Lavabos privados';
			$pre_gear_check['title']	= 'Verifica&ccedil;&atilde;o privada do material';
			$pre_parking['title']		= 'Estacionamento reservado';
			$pre_yoga['title']			= 'Alongamento Yoga ';
			$pre_shuttle['title']		= 'Transporte VIP ';

			// postrace
			$post_private_area['title']		= '&Aacute;rea VIP privada';
			$post_restrooms['title']		= 'Lavabos privados';
			$post_buffet['title']			= 'Buffet servido';
			$post_bar['title']				= 'Bar VIP ';
			$post_parking['title']			= 'Estacionamento reservado';
			$post_massage['title']			= 'Massagem p&oacute;s-corrida';
			$post_changing_tents['title']	= 'Troca das tendas ap&oacute;s corrida';
			$post_shuttle['title']			= 'Transporte VIP ';
			$post_view['title']				= 'Vista para o local do concerto';
			$post_concert['title']			= 'Acesso &agrave; Festa de Concerto VIP';
			$txt_purchase					= 'Comprar Pacote VIP';

			$vip_program_title = 'VIP';

			// amenities
			$vip_amenities_lang['parking']			= 'Transportes';
			$vip_amenities_lang['indoor']			= '&Aacute;rea VIP privada';
			$vip_amenities_lang['breakfast']		= 'Pequeno-almo&ccedil;o continental';
			$vip_amenities_lang['buffet']			= 'Buffet servido';
			$vip_amenities_lang['massage']			= 'Massagem p&oacute;s-corrida';
			$vip_amenities_lang['yoga']				= 'Alongamento Yoga';
			$vip_amenities_lang['restrooms']		= 'Lavabos privados';
			$vip_amenities_lang['gearcheck']		= 'Verifica&ccedil;&atilde;o privada do material';
			$vip_amenities_lang['bar']				= 'Bar VIP ';
			$vip_amenities_lang['view']				= 'Vista para o local do concerto';
			$vip_amenities_lang['shuttles']			= 'Transporte VIP ';
			$vip_amenities_lang['changingtents']	= 'Troca das tendas ap&oacute;s corrida';
			$vip_amenities_lang['heaters']			= 'Aquecedores';
			$vip_amenities_lang['swag']				= 'Promo&ccedil;&otilde;es da marca';
			$vip_amenities_lang['charging']			= 'VIP Charging Station';
			$vip_amenities_lang['party']			= 'Acesso &agrave; Festa de Concerto VIP';

			$vip_overview_title			= 'Overview';
			$participant_title			= 'VIP Participant Amenities';
			$prerace_only_title			= 'Pre-Race Only VIP Package';
			$postrace_only_title		= 'Post-Race Only VIP Package';
			$postrace_concert_title		= 'Post-Race Concert VIP';
			$vip_type_title['platinum']	= 'Platinum Package';
			$vip_type_title['vip']		= 'VIP Experience';
			$vip_type_title['prerace']	= 'Pre-Race VIP Experience';
			$vip_type_title['postrace']	= 'Post-Race VIP Experience';
		} elseif( $qt_lang['lang'] == 'zh' ) {
			// titles
			$overview['title']	= '概述';
			$overview_title		= '概述';
			$purchase['title']	= '购买';
			$terms['title']		= '条款与条件';

			// participants
			$parking['title']	= '专用车位';
			$lounge['title']	= '私人贵宾区域';
			$breakfast['title']	= '欧式早餐';
			$brunch['title']	= '自助餐';
			$massage['title']	= '赛后消息';
			$yoga['title']		= '瑜伽伸展';
			$restrooms['title']	= '私人洗手间';
			$gearcheck['title']	= '私人设备检查';
			$bar['title']		= '贵宾酒吧';
			$view['title']		= '音乐会场地的俯瞰图';
			$shuttle['title']	= '贵宾接驳车';
			$tents['title']		= '赛后换衣帐篷';
			$heaters['title']	= '取暖器';
			$swag['title']		= '带商标的礼品';

			// prerace
			$pre_private_area['title']	= '私人贵宾区域';
			$pre_breakfast['title']		= '欧式早餐';
			$pre_restrooms['title']		= '私人洗手间';
			$pre_gear_check['title']	= '私人设备检查';
			$pre_parking['title']		= '专用车位';
			$pre_yoga['title']			= '瑜伽伸展';
			$pre_shuttle['title']		= '贵宾接驳车';

			// postrace
			$post_private_area['title']		= '私人贵宾区域';
			$post_restrooms['title']		= '私人洗手间';
			$post_buffet['title']			= '自助餐';
			$post_bar['title']				= '贵宾酒吧';
			$post_parking['title']			= '专用车位';
			$post_massage['title']			= '赛后消息';
			$post_changing_tents['title']	= '赛后换衣帐篷';
			$post_shuttle['title']			= '贵宾接驳车';
			$post_view['title']				= '音乐会场地的俯瞰图';
			$post_concert['title']			= '贵宾音乐会派对门票';
			$txt_purchase					= '购买贵宾套票';

			$vip_program_title				= 'VIP Program';

			// amenities
			$vip_amenities_lang['parking']			= '专用车位';
			$vip_amenities_lang['indoor']			= '私人贵宾区域';
			$vip_amenities_lang['breakfast']		= '欧式早餐';
			$vip_amenities_lang['buffet']			= '自助餐';
			$vip_amenities_lang['massage']			= '赛后消息';
			$vip_amenities_lang['yoga']				= '瑜伽伸展';
			$vip_amenities_lang['restrooms']		= '私人洗手间';
			$vip_amenities_lang['gearcheck']		= '私人设备检查';
			$vip_amenities_lang['bar']				= '贵宾酒吧';
			$vip_amenities_lang['view']				= '音乐会场地的俯瞰图';
			$vip_amenities_lang['shuttles']			= '贵宾接驳车';
			$vip_amenities_lang['changingtents']	= '赛后换衣帐篷';
			$vip_amenities_lang['heaters']			= '取暖器';
			$vip_amenities_lang['swag']				= '带商标的礼品';
			$vip_amenities_lang['charging']			= 'VIP充电站';
			$vip_amenities_lang['party']			= '贵宾音乐会派对门票';

			$vip_amenities_lang['hotel']			= 'Hotel';
			$vip_amenities_lang['swagpack']			= 'Swag Pack';
			$vip_amenities_lang['transport']		= 'Pre-Race Transportation';
			$vip_amenities_lang['airporttransport']	= 'Airport Transportation';
			$vip_amenities_lang['expopickup']		= 'Expo Packet Pick Up';
			$vip_amenities_lang['reception']		= 'VIP Reception';

			$vip_overview_title			= 'Overview';
			$participant_title			= 'VIP参与者的便利设施';
			$prerace_only_title			= '赛前VIP包';
			$postrace_only_title		= '赛后VIP包';
			$postrace_concert_title		= '赛后音乐会VIP';
			$vip_type_title['platinum']	= 'Platinum Package';
			$vip_type_title['vip']		= 'VIP体验';
			$vip_type_title['prerace']	= '赛前VIP体验';
			$vip_type_title['postrace']	= '赛后VIP体验';
		} else {
			// titles
			$overview['title']	= 'Overview';
			$overview_title		= 'Overview';
			$purchase['title']	= 'Purchase';
			$terms['title']		= 'Terms &amp; Conditions';

			// participants
			$parking['title']	= 'Parking &amp; Transportation';
			$lounge['title']	= 'Private VIP Area';
			$breakfast['title']	= 'Continental Breakfast';
			$brunch['title']	= 'Catered Buffet';
			$massage['title']	= 'Post-Race Massage';
			$yoga['title']		= 'Yoga Stretching';
			$restrooms['title']	= 'Private Restrooms';
			$gearcheck['title']	= 'Private Gear Check';
			$bar['title']		= 'VIP Bar';
			$view['title']		= 'View Overlooking the Concert Venue';
			$shuttle['title']	= 'VIP Shuttle';
			$tents['title']		= 'Post-Race Changing Tents';
			$heaters['title']	= 'Heaters';
			$swag['title']		= 'Branded Swag';

			// prerace
			$pre_private_area['title']	= 'Private VIP Area';
			$pre_breakfast['title']		= 'Continental Breakast';
			$pre_restrooms['title']		= 'Private Restrooms';
			$pre_gear_check['title']	= 'Private Gear Check';
			$pre_parking['title']		= 'Reserved Parking';
			$pre_yoga['title']			= 'Yoga Stretching Area';
			$pre_shuttle['title']		= 'VIP Shuttle';

			// postrace
			$post_private_area['title']		= 'Private VIP Area';
			$post_restrooms['title']		= 'Private Restrooms ';
			$post_buffet['title']			= 'Catered Buffet';
			$post_bar['title']				= 'VIP Bar';
			$post_parking['title']			= 'Reserved Parking';
			$post_massage['title']			= 'Massage';
			$post_changing_tents['title']	= 'Changing Tents';
			$post_shuttle['title']			= 'VIP Shuttle';
			$post_view['title']				= 'View Overlooking The Concert Venue';
			$post_concert['title']			= 'VIP Concert Party Access';
			$txt_purchase					= 'Purchase VIP Package';

			$vip_program_title				= 'VIP Program';

			// amenities
			$vip_amenities_lang['parking']			= 'Parking &amp; Transportation';
			$vip_amenities_lang['indoor']			= 'Private Hospitality Package';
			$vip_amenities_lang['breakfast']		= 'Continental Breakfast';
			$vip_amenities_lang['buffet']			= 'Catered Buffet';
			$vip_amenities_lang['massage']			= 'Post-Race Massage';
			$vip_amenities_lang['yoga']				= 'Yoga Stretching Area';
			$vip_amenities_lang['restrooms']		= 'Private Restrooms';
			$vip_amenities_lang['gearcheck']		= 'Private Gear Check';
			$vip_amenities_lang['bar']				= 'Hospitality Package Bar';
			$vip_amenities_lang['view']				= 'View Overlooking the Concert Venue';
			$vip_amenities_lang['shuttles']			= 'Hospitality Package Shuttle';
			$vip_amenities_lang['changingtents']	= 'Post-Race Changing Tents';
			$vip_amenities_lang['heaters']			= 'Heaters';
			$vip_amenities_lang['swag']				= 'Branded Swag';
			$vip_amenities_lang['charging']			= 'VIP Charging Station';
			$vip_amenities_lang['party']			= 'VIP Concert Party Access';

			$vip_amenities_lang['hotel']			= 'Hotel';
			$vip_amenities_lang['swagpack']			= 'Swag Pack';
			$vip_amenities_lang['transport']		= 'Pre-Race Transportation';
			$vip_amenities_lang['airporttransport']	= 'Airport Transportation';
			$vip_amenities_lang['expopickup']		= 'Expo Packet Pick Up';
			$vip_amenities_lang['reception']		= 'VIP Reception';

			$vip_overview_title			= 'Overview';
			$participant_title			= 'VIP Participant Amenities';
			$prerace_only_title			= 'Pre-Race Only VIP Package';
			$postrace_only_title		= 'Post-Race Only VIP Package';
			$postrace_concert_title		= 'Post-Race Concert VIP';
			$vip_type_title['platinum']	= 'Platinum Package';
			$vip_type_title['vip']		= 'VIP Experience';
			$vip_type_title['prerace']	= 'Pre-Race VIP Experience';
			$vip_type_title['postrace']	= 'Post-Race VIP Experience';
		}
	}


	/**
	 * Finisher Zone
	 */
	if( $qt_lang['lang'] == 'es' ) {
		$presented_by_txt			= 'Patrocinado Por';
		$runner_personas_txt		= 'Calificaci&oacute;n de Corredores';
		$features_txt				= 'Caracteristicas';
		$learn_more_txt				= 'Aprende Mas';
		$latest_results_txt			= 'Ultimos Resultados';
		$view_results_txt			= 'Ver Resultados';
		$past_results_txt			= 'Resultado Previo';
		$event_page_txt				= 'P&aacute;gina del Evento';
		$see_all_events_txt			= 'Ver Todos Los Eventos';
		$finisher_badges_txt		= 'Distintivos Finisher';
		$rnr_personas_txt			= 'Calificacion de Corredores Rock \'n\' Roll';
		$more_features_txt			= 'Mas Caracter&iacute;sticas';

		$search_results_txt			= 'Buscar Resultados';
		$quick_results_txt			= 'Para encontrar tus resultados m&aacute;s r&aacute;pido, escribe tu n&uacute;mero de competidor.';

		$first_name_txt				= 'Nombre';
		$last_name_txt				= 'Apellido';
		$runner_number_txt			= 'N&uacute;mero de Corredor (bib)';
		$team_txt					= 'Equipo';
		$city_txt					= 'Ciudad';
		$gender_txt					= 'G&eacute;nero';
		$state_txt					= 'Estado';
		$division_txt				= 'Divisi&oacute;n';
		$club_txt					= 'Club';
		$search_txt					= 'Buscar';
		$submit_corr_txt			= 'Somete una correcci&oacute;n.';
		$leaderboards_txt			= 'Tabla de Lideres';
		$default_txt				= 'Defecto';
		$select_event_txt			= 'Elije evento desde el men&uacute; desplegable para acceder resultado m&aacute;s reciente.';
		$no_runner_txt				= 'No encuentra al corredor';

		$page_txt					= 'P&aacute;gina';
		$results_per_page_txt		= 'Resultados Por P&aacute;gina';
		$next_txt					= 'M&aacute;s';
		$previous_txt				= 'Anterior';
		$display_txt				= 'Viendo';
		$runners_txt				= 'corredores';
		$search_again_txt			= 'Buscar De Nuevo';
		$of_txt						= 'de';

		$runner_info_txt			= 'Informaci&oacute;n del Corredor';
		$photos_txt					= 'Fotos';
		$earned_badges_txt			= 'Distintivos Ganados';
		$badges_txt					= 'Distintivos';
		$course_stats_txt			= 'Estad&iacute;sticas de la Ruta';
		$speedometer_txt			= 'Veloc&iacute;metro';
		$whats_next_txt				= 'Que Sigue';
		$rnr_assessment_txt			= 'Evaluac&iacute;on de Rock \'n\' Roll';
		$assessment_txt				= 'Evaluac&iacute;on';
		$digital_certificate_txt	= 'Certificado Digital';
		$finisher_certificate_txt	= 'Certificado de Finisher';

		$name_txt					= 'Nombre';
		$bib_number_txt				= 'N&uacute;mero de competidor';
		$age_txt					= 'Edad';
		$hometown_txt				= 'Ciudad de Origen';
		$finish_time_txt			= 'Tiempo final';
		$expected_time_txt			= 'Tiempo esperado';
		$pace_txt					= 'Ritmo';
		$chip_time_txt				= 'Tiempo Chip';
		$clock_time_txt				= 'Tiempo Reloj';
		$overall_txt				= 'General';
		$military_branch_txt		= 'Military Branch';
		$mb_place_txt				= 'Place';
		$see_txt					= 'Ver';
		$time_txt					= 'Tiempo';

		$course_highlights_txt		= 'Bellezas en la Ruta';
		$field_txt					= 'Promedio total';
		$how_fast_txt				= '&iquest;Que tan r&aacute;pido fue';
		$screamin_txt				= 'Screamin\'';
		$jammin_txt					= 'Jammin\'';
		$strummin_txt				= 'Strummin\'';
		$on_beat_txt				= 'En ritmo\'';
		$rippin_txt					= 'Rippin\'';

		$customize_cert_txt			= 'Personaliza tu Certificado Digital';
		$all_txt					= 'Todas';
		$download_cert_txt			= 'Descarga Certificado';
		$cert_txt					= 'Certificado';

		$selected_lucky_txt			= 'has sido seleccionado(a) como uno de los Finalizador Afortunados!';
		$beat_expected_txt			= '&iexcl;Acabas de vencer tu tiempo esperado! &iquest;Has tenido un momento incre&iacute;ble mientras que corr&iacute;as o entrenabas, que te gustar&iacute;a compartir? <a href="http://www.runrocknroll.com/moments/#share-your-moment" target="_blank">Has clic aqu&iacute;</a> para decirnos.';
		$mutiple_events_txt			= 'Participaste en mas de un evento durante el fin de semana del evento.';
		$other_events_txt			= 'Has clic aqu&iacute; para ver otros resultados';
		$beat_time_txt				= ' Venciste tu tiempo esperado.';

		$fb_share_msg_txt			= '&iexcl;Por favor llena tu mensaje para compartir, o copea/pega el siguiente mensaje en el &aacute;rea de mensajes!';
		$suggest_msg_txt			= 'Mensaje sugerido';
		$msg_to_share_txt			= 'Mensaje para compartir';
		$img_to_share_txt			= 'Imagen para compartir';
		$share_txt					= 'Comparte';

		$results_not_available		= 'Los resultados para este evento no estan disponibles en este momento';
		$select_event_dropdown		= 'Selecciona un evento de la lista para acceder los resultados de la carrera.';
	} elseif( $qt_lang['lang'] == 'fr' ) {
		$city_txt			= 'Ville';
		$presented_by_txt	= 'pr&eacute;sent&eacute; par ';
	} else {
		$presented_by_txt			= 'Presented by';
		$runner_personas_txt		= 'Runner Personas';
		$features_txt				= 'Features';
		$learn_more_txt				= 'Learn More';
		$latest_results_txt			= 'Latest Results';
		$view_results_txt			= 'View Results';
		$past_results_txt			= 'Past Results';
		$event_page_txt				= 'Event Page';
		$see_all_events_txt			= 'See All Events';
		$finisher_badges_txt		= 'Finisher Badges';
		$rnr_personas_txt			= 'Rock \'n\' Roll Personas';
		$more_features_txt			= 'More Features';

		$search_results_txt			= 'Search Results';
		$quick_results_txt			= 'To find your results the quickest, please enter your bib number.';
		$first_name_txt				= 'First Name';
		$last_name_txt				= 'Last Name';
		$runner_number_txt			= 'Runner Number (bib)';
		$team_txt					= 'Team';
		$city_txt					= 'City';
		$gender_txt					= 'Gender';
		$state_txt					= 'State';
		$division_txt				= 'Division';
		$club_txt					= 'Club';
		$search_txt					= 'Search';
		$submit_corr_txt			= 'Submit a Correction';
		$leaderboards_txt			= 'Leaderboards';
		$default_txt				= 'Default';
		$select_event_txt			= 'Choose event from the dropdown to access most recent result.';
		$no_runner_txt				= 'No Runner Found';

		$page_txt					= 'Page';
		$results_per_page_txt		= 'Results Per Page';
		$next_txt					= 'Next';
		$previous_txt				= 'Previous';
		$display_txt				= 'Display';
		$runners_txt				= 'runners';
		$search_again_txt			= 'Search Again';
		$of_txt						= 'of';

		$runner_info_txt			= 'Runner Information';
		$photos_txt					= 'Photos';
		$earned_badges_txt			= 'Earned Badges';
		$badges_txt					= 'Badges';
		$course_stats_txt			= 'Course Stats';
		$speedometer_txt			= 'Speedometer';
		$whats_next_txt				= 'What\'s Next';
		$rnr_assessment_txt			= 'Rock \'n\' Roll Assessment';
		$assessment_txt				= 'Assessment';
		$digital_certificate_txt	= 'Digital Certificate';
		$finisher_certificate_txt	= 'Finisher Certificate';

		$name_txt					= 'Name';
		$bib_number_txt				= 'Bib Number';
		$age_txt					= 'Age';
		$hometown_txt				= 'Hometown';
		$finish_time_txt			= 'Finish Time';
		$expected_time_txt			= 'Expected Time';
		$pace_txt					= 'Pace';
		$chip_time_txt				= 'Chip Time';
		$clock_time_txt				= 'Clock Time';
		$overall_txt				= 'Overall';
		$military_branch_txt		= 'Military Branch';
		$mb_place_txt				= 'Place';
		$see_txt					= 'See';
		$time_txt					= 'Time';

		$course_highlights_txt		= 'Course Highlights';
		$field_txt					= 'Field';
		$how_fast_txt				= 'How Fast was';
		$screamin_txt				= 'Screamin\'';
		$jammin_txt					= 'Jammin\'';
		$strummin_txt				= 'Strummin\'';
		$on_beat_txt				= 'On Beat\'';
		$rippin_txt					= 'Rippin\'';

		$customize_cert_txt			= 'Customize Your Digital Certificate';
		$all_txt					= 'All';
		$download_cert_txt			= 'Download Certificate';
		$cert_txt					= 'Certificate';

		$selected_lucky_txt			= 'you have been randomly selected as a Lucky Finisher!';
		$beat_expected_txt			= 'You just beat your expected time! Did you have an incredible moment while racing or training, you would like to share? <a href="http://www.runrocknroll.com/moments/#share-your-moment" target="_blank">Click here</a> to tell us about it.';
		$mutiple_events_txt			= 'You participated in multiple events, during the event weekend.';
		$other_events_txt			= 'Click here to see other results';
		$beat_time_txt				= ' You beat your expected time.';

		$fb_share_msg_txt			= 'Please fill out your message below to share, or copy/paste the message below into the Message area!';
		$suggest_msg_txt			= 'Suggested message';
		$msg_to_share_txt			= 'Message to share';
		$img_to_share_txt			= 'Image to share';
		$share_txt					= 'Share';

		$results_not_available		= 'Results not currently available for selected event';
		$select_event				= 'Select an event from the dropdown to access race results.';
	}


	/**
	 * GRD
	 */
	if( $qt_lang['lang'] == 'es' ) {
		$aslowas_txt		= 'Desde solo';
		$savings_txt		= 'ahorros';
		$limitedsupply_txt	= 'Cupo Limitado';
		$atexpo_txt			= 'Registration for 2018 is available at the Health & Fitness Expo';
		$getdetails_txt		= 'Detalles';
		$registernow_txt	= 'Registrate';
		$remindme_txt		= 'Recu&eacute;rdamelo';
		// $grd_title			= 'D&iacute;a internacional del running';
		$grd_24hours		= 'Descuentos 24 horas';
		$grd_bestprices_1	= 'Los Mejores';
		$grd_bestprices_2	= 'precios';
		$grd_oftheyear		= 'Del a&ntilde;o';

		// "find a race" distances
		$all_txt			= 'Carreras';
		$marathon_txt		= 'Marat&oacute;n';
		$halfmarathon_txt	= 'Medio Marat&oacute;n';
		$tenk_txt			= '10K';
		$fivek_txt			= '5K';
		$relay_txt			= 'Relevo';
		$onemile_txt		= '1 Milla';
		$onek_txt			= '1K';
		$elevenk_txt		= '11K';
		$twentyk_txt		= '20 K';

		$marathon_distancelist		= '42.2';
		$halfmarathon_distancelist	= '21.1';
		$tenk_distancelist			= '10K';
		$fivek_distancelist			= '5K';
		$thirteenk_distancelist		= '13K';
		$relay_distancelist			= 'Relevo';
		$onemile_distancelist		= '1 Mi';
		$onek_distancelist			= '1K';
		$elevenk_distancelist		= '11K';
		$twentyk_distancelist		= '20K';
		$kidsrock_distancelist		= 'Kids';

		$marathon_distancename		= 'Marat&oacute;n';
		$halfmarathon_distancename	= 'Medio Marat&oacute;n';
		$tenk_distancename			= '10K';
		$fivek_distancename			= '5K';
		$thirteenk_distancename		= '13K';
		$relay_distancename			= 'Medio Marat&oacute;n Relevo';
		$onemile_distancename		= '1 Milla';
		$onek_distancename			= '1K';
		$elevenk_distancename		= '11K';
		$twentyk_distancename		= 'All Day 20K';
		$kidsrock_distancename		= 'KiDS Rock';

		$grd_event_reg_closed		= 'Las inscripciones a&uacute;n no est&aacute;n abiertas. Reg&iacute;strate para ser el primero en recibir la alerta de su apertura.';
		$grd_event_reg_soldout		= 'Todos espacios para este evento han sideo vendido. Te invitamos juntarnos por un otra distancia o Rock \'n\' Roll Marathon Series evento.';
		$grd_about_remix_txt		= 'Acerca<br> del remix';
		$grd_about_remix_short_txt	= 'Remix';
		$grd_race_amenities_txt		= 'Ventajas del corredor';
		$grd_race_features_txt		= 'Caracter&iacute;sticas de la carrera';

		$filterby_txt				= 'Filtrar por';
		$alldates_txt				= 'Todas las fechas';
		$alldistances_txt			= 'Todas las distancias';
		$season_summer_txt			= 'Verano';
		$season_fall_txt			= 'Oto&ntilde;o';
		$season_winter_txt			= 'Invierno';
		$season_spring_txt			= 'Primavera';

		$grd_amenity_1	= 'Camiseta';
		$grd_amenity_10	= 'Camiseta conmemorativa de Brooks';
		$grd_amenity_11	= 'Camiseta conmemorativa';
		$grd_amenity_2	= 'Medalla de Finisher';
		$grd_amenity_3	= 'T&iacute;tulo representativo';
		$grd_amenity_4	= 'M&uacute;sica y entretenimiento en el recorrido';
		$grd_amenity_5	= 'Expo';
		$grd_amenity_8	= 'Chaqueta de finisher';
		$grd_amenity_9	= 'M&uacute;sica y entretenimiento en el recorrido';

		$grd_amenity_6			= 'Experiencia VIP';
		$grd_amenity_6_extra	= '&iexcl;Mejora tu experiencia con ba&ntilde;os privados, guardarropa, comida, y mucho m&aacute;s!';
		$grd_amenity_7			= 'Remix Challenge';
		$grd_amenity_7_extra	= 'Corre dos d&iacute;as y gana tres medallas';
		$grd_amenity_12			= 'Nuevas experiencias en tu carrera';
		$grd_amenity_12_extra	= 'En 2018 te ofrecemos una experiencia modernizada de principio a fin. Con nuevos kil&oacute;metros de entretenimiento, m&aacute;s m&uacute;sica que nunca, estaciones de asistencia mejoradas y muchas sorpresas m&aacute;s. Te sentir&aacute;s como una Rockstar cuando corriendo con nosotros.';

		$grd_feature_miles	= 'Descripci&oacute;n del recorrido';
		$grd_miles_1		= 'Plano';
		$grd_miles_2		= 'R&aacute;pido';
		$grd_miles_3		= 'Esc&eacute;nico';
		$grd_miles_4		= 'Todo el nuevo recorrido';
		$grd_miles_5		= 'Todos los nuevos recorridos';
		$grd_miles_6		= 'Nuevo recorrido';
		$grd_miles_7		= 'Nuevos recorridos';
		$grd_miles_8		= 'Desafiante';
		$grd_miles_9		= 'Monta&ntilde;oso';
		$grd_miles_10		= 'Centro de la ciudad';
		$grd_miles_11		= 'Punto de referencia';
		$grd_miles_12		= 'Recorrido hist&oacute;rico';
		$grd_miles_13		= 'Las calles de la ciudad';
		$grd_miles_14		= 'Carrera nocturna';
		$grd_miles_15		= 'Certificado de oro de la IAAF';
		$grd_miles_16		= 'servicios para el espectador';
		$grd_miles_17		= 'Nuevo recorrido de 5K';
		$grd_miles_18		= 'Nuevo recorrido de 10K';
		$grd_miles_19		= 'Nuevo recorrido de media marat&oacute;n';
		$grd_miles_20		= 'Nuevo recorrido de marat&oacute;n';
		$grd_miles_21		= 'vistas del parque';
		$grd_miles_22		= 'parques';

		$grd_feature_views	= 'Vistas';
		$grd_views_1		= 'Costero';
		$grd_views_2		= 'Orilla';
		$grd_views_3		= 'Frente al mar';
		$grd_views_4		= 'Parques';
		$grd_views_5		= 'Paisaje urbano';
		$grd_views_6		= 'Centro de la ciudad';
		$grd_views_7		= 'Distritos hist&oacute;ricos';
		$grd_views_8		= 'Puntos ic&oacute;nicos';
		$grd_views_9		= 'Pa&iacute;s';
		$grd_views_10		= 'Los barios de moda';
		$grd_views_11		= 'Frente a la playa';
		$grd_views_12		= 'horizonte';
		$grd_views_13		= 'vistas de la ciudad';
		$grd_views_14		= 'vistas costeras';
		$grd_views_15		= 'vistas al horizonte';
		$grd_views_16		= 'tour por el barrio';

		$grd_feature_qualifier	= 'Limited Edition Medal Qualifier';
		$grd_quals_1			= 'Marathon Challenge Qualifier<br>Termina 3 maratones';
		$grd_quals_2			= 'Bucket List Challenge Qualifier<br>Corre San Francisco y Brooklyn en 2018';
		$grd_quals_3			= 'World Rocker Qualifier<br>Corre dos pa&iacute;ses en 2018';
		$grd_quals_4			= 'Beach to Beach Qualifier<br>Corre cualquier distancia en San Diego y Virginia Beach en 2018';
		$grd_quals_5			= 'Cali Combo Qualifier<br>Corre cualquier tres maraton or media maratones en California en 2018';

		$grd_years_running	= ' a&ntilde;os corriendo';
		$grd_website		= 'P&aacute;gina Web del evento';

	} elseif( $qt_lang['lang'] == 'fr' ) {
		$aslowas_txt		= 'Pour aussi peu que';
		$savings_txt		= '&Eacute;conomies de ';
		$limitedsupply_txt	= 'Quantit&eacute; limit&eacute;e';
		$atexpo_txt			= 'Registration for 2018 is available at the Health & Fitness Expo';
		$getdetails_txt		= 'D&eacute;tails';
		$registernow_txt	= 'S\'inscrire';
		$remindme_txt		= 'Rappelez-moi';
		// $grd_title			= 'Journ&eacute;e mondiale de la course &agrave; pied';
		$grd_24hours		= 'Vente de 24 heures';
		$grd_bestprices_1	= 'Meilleurs';
		$grd_bestprices_2	= 'tarifs';
		$grd_oftheyear		= 'de l\'ann&eacute;e';

		// "find a race" distances
		$all_txt			= 'Tout';
		$marathon_txt		= 'Marathon';
		$halfmarathon_txt	= 'Demi-Marathon';
		$tenk_txt			= '10 km';
		$fivek_txt			= '5 km';
		$relay_txt			= 'Relais';
		$onemile_txt		= '1 mille';
		$onek_txt			= '1 km';
		$elevenk_txt		= '11 km';
		$twentyk_txt		= '20 km';

		$marathon_distancelist		= '42,2 KM';
		$halfmarathon_distancelist	= '21,1 KM';
		$tenk_distancelist			= '10 KM';
		$fivek_distancelist			= '5 KM';
		$thirteenk_distancelist		= '13 KM';
		$relay_distancelist			= 'Relais';
		$onemile_distancelist		= '1 Mi';
		$onek_distancelist			= '1 KM';
		$elevenk_distancelist		= '11 KM';
		$twentyk_distancelist		= '20 KM';
		$kidsrock_distancelist		= 'Kids';

		$marathon_distancename		= 'Marathon';
		$halfmarathon_distancename	= 'Demi-Marathon';
		$tenk_distancename			= '10 KM';
		$fivek_distancename			= '5 KM';
		$thirteenk_distancename		= '13 KM';
		$relay_distancename			= 'Demi-Marathon Relais';
		$onemile_distancename		= '1 Mille';
		$onek_distancename			= '1 KM';
		$elevenk_distancename		= '11 KM';
		$twentyk_distancename		= 'All Day 20K';
		$kidsrock_distancename		= 'KiDS Rock';

		$grd_event_reg_closed		= 'La p&eacute;riode d\'inscription n\'est pas encore commenc&eacute;e. Veuillez inscrire votre courriel afin d\'&ecirc;tre pr&eacute;venu de l\'ouverture de la p&eacute;riode d\'inscription.';
		$grd_event_reg_soldout		= 'Cette course est maintenant compl&egrave;te. Veuillez choisir une autre distance ou un autre &eacute;v&eacute;nement de la S&eacute;rie des Marathons Rock \'n\' Roll.';
		$grd_about_remix_txt		= '&Agrave; propos<br> de Remix';
		$grd_about_remix_short_txt	= 'Remix';
		$grd_race_amenities_txt		= 'LES EXTRAS';
		$grd_race_features_txt		= 'Caract&eacute;ristiques des course';

		$filterby_txt		= 'Trier par';
		$alldates_txt		= 'Toutes les dates';
		$alldistances_txt	= 'Toutes les distances';
		$season_summer_txt	= '&Eacute;t&eacute;';
		$season_fall_txt	= 'Automne';
		$season_winter_txt	= 'Hiver';
		$season_spring_txt	= 'Printemps';

		$grd_amenity_1	= 'T-Shirt';
		$grd_amenity_10	= 'T-shirt Brooks pour les participants';
		$grd_amenity_11	= 'T-shirt pour les participants';
		$grd_amenity_2	= 'M&eacute;daille du finissant';
		$grd_amenity_3	= 'Spectacle de musique sur la sc&egrave;ne principale';
		$grd_amenity_4	= 'Musique et divertissements sur le parcours';
		$grd_amenity_5	= 'Expo';
		$grd_amenity_8	= 'Manteau des finissants du Marathon';
		$grd_amenity_9	= 'Am&eacute;lioration de la musique et de l\'animation sur le parcours';

		$grd_amenity_6			= 'Exp&eacute;rience confort Oasis';
		$grd_amenity_6_extra	= 'B&eacute;n&eacute;ficiez de plusieurs avantages comme des toilettes r&eacute;serv&eacute;es, un espace d&eacute;sign&eacute; pour y laisser votre objets de valeur, un repas de traiteur et plus encore!';
		$grd_amenity_7			= 'D&eacute;fi Remix';
		$grd_amenity_7_extra	= 'Courez deux jours et obtenez trois m&eacute;dailles';
		$grd_amenity_12			= 'Une exp&eacute;rience de course renouvel&eacute;e';
		$grd_amenity_12_extra	= 'En 2018, nous vous offrirons une exp&eacute;rience de course am&eacute;lior&eacute;e de la ligne de d&eacute;part jusqu\'au fil d\'arriv&eacute;e. Avec encore plus de divertissement par kilom&egrave;tre, plus de musique, des stations d\'aide aux coureurs am&eacute;lior&eacute;es et plus encore, vous vous sentirez comme une star de courir avec nous.';

		$grd_feature_miles	= 'Description du parcours';
		$grd_miles_1		= 'Plat';
		$grd_miles_2		= 'Vite';
		$grd_miles_3		= 'Pittoresque';
		$grd_miles_4		= 'Tout nouveau parcours';
		$grd_miles_5		= 'Tout nouveaux parcours';
		$grd_miles_6		= 'Nouveau parcours';
		$grd_miles_7		= 'Nouveaux parcours';
		$grd_miles_8		= 'Stimulant';
		$grd_miles_9		= 'Vallonn&eacute;';
		$grd_miles_10		= 'Centre-ville';
		$grd_miles_11		= 'Plusieurs rep&egrave;res historiques et culturels sur le parcours';
		$grd_miles_12		= 'Parcours historique';
		$grd_miles_13		= 'Les rues de la ville';
		$grd_miles_14		= 'Course du Nuit';

		$grd_miles_15		= 'Label d\'or de l\'IAAF';
		$grd_miles_16		= 'Appui de spectateurs';
		$grd_miles_17		= 'Nouveau parcours pour la course de 5 km';
		$grd_miles_18		= 'Nouveau parcours pour la course de 10 km';
		$grd_miles_19		= 'Nouveau parcours pour le demi-marathon';
		$grd_miles_20		= 'Nouveau parcours pour le marathon';
		$grd_miles_21		= 'Vues de parcs';
		$grd_miles_22		= 'parcs';

		$grd_feature_views	= '&Agrave; voir le long du parcours';
		$grd_views_1		= 'Le parcours longe la c&ocirc;te';
		$grd_views_2		= 'Au bord de la rivi&egrave;re';
		$grd_views_3		= 'Bord de l\'eau';
		$grd_views_4		= 'Parcs';
		$grd_views_5		= 'Paysage urbain';
		$grd_views_6		= 'Centre-ville';
		$grd_views_7		= 'Quartier historiques';
		$grd_views_8		= 'Sites embl&eacute;matiques';
		$grd_views_9		= 'Campagne';
		$grd_views_10		= 'Quartiers branch&eacute;s';
		$grd_views_11		= 'Au bord de la mer';
		$grd_views_12		= 'Ligne d\'horizon';
		$grd_views_13		= 'Vues de la ville';
		$grd_views_14		= 'Vues c&ocirc;ti&egrave;res';
		$grd_views_15		= 'Vues panoramiques';
		$grd_views_16		= 'Tour du quartier';

		$grd_feature_qualifier	= 'Limited Edition Medal Qualifier';
		$grd_quals_1			= 'D&eacute;fi Marathon<br>Compl&eacute;tez 3 marathons';
		$grd_quals_2			= 'D&eacute;fi Bucket List <br>En 2018, courez &agrave; San Francisco et &agrave; Brooklyn';
		$grd_quals_3			= 'D&eacute;fi World Rocker<br>Courez dans deux pays en 2018';
		$grd_quals_4			= 'D&eacute;fi Beach to Beach<br>En 2018, compl&eacute;tez la distance de votre choix &agrave; San Diego et &agrave; Virginia Beach';
		$grd_quals_5			= 'D&eacute;fi Cali Combo<br>Participez &agrave; trois de nos marathons ou demi-marathons en Californie.';

		$grd_years_running	= ' ans de course';
		$grd_website		= 'Site web de l\'&eacute;v&eacute;nement';

	} elseif( $qt_lang['lang'] == 'zh' ) {
		$marathon_txt		= '马拉松';
		$halfmarathon_txt	= '半程马拉松';
		$tenk_txt			= '10K';
	} else {
		$aslowas_txt		= 'As Low As';
		$savings_txt		= 'Savings';
		$limitedsupply_txt	= 'Limited Supply';
		$atexpo_txt			= 'Registration for 2018 is available at the Health & Fitness Expo';
		$getdetails_txt		= 'Details';
		$registernow_txt	= 'Register Now';
		$remindme_txt		= 'Remind Me';
		// $grd_title			= 'Global Running Day';
		$grd_24hours		= '24-Hour Sale';
		$grd_bestprices_1	= 'Best';
		$grd_bestprices_2	= 'Prices';
		$grd_oftheyear		= 'Of The Year';

		// "find a race" distances
		$all_txt			= 'All';
		$marathon_txt		= 'Marathon';
		$halfmarathon_txt	= 'Half Marathon';
		$tenk_txt			= '10k';
		$fivek_txt			= '5k';
		$relay_txt			= 'Relay';
		$onemile_txt		= '1 Mile';
		$onek_txt			= '1k';
		$elevenk_txt		= '11k';
		$twentyk_txt		= 'All Day 20k';

		$marathon_distancelist		= '26.2';
		$halfmarathon_distancelist	= '13.1';
		$tenk_distancelist			= '10K';
		$fivek_distancelist			= '5K';
		$thirteenk_distancelist		= '13K';
		$relay_distancelist			= 'Relay';
		$onemile_distancelist		= '1 Mi';
		$onek_distancelist			= '1K';
		$elevenk_distancelist		= '11K';
		$twentyk_distancelist		= '20K';
		$kidsrock_distancelist		= 'Kids';
		$funrun_distancelist		= 'Fun';
		$happyrun_distancelist 		= 'Happy';

		$marathon_distancename		= 'Marathon';
		$halfmarathon_distancename	= 'Half Marathon';
		$tenk_distancename			= '6.2 Miles';
		$fivek_distancename			= '3.1 Miles';
		$thirteenk_distancename		= '13K';
		$relay_distancename			= 'Half Marathon Relay';
		$onemile_distancename		= '1 Mile';
		$onek_distancename			= '1 K';
		$elevenk_distancename		= '6.8 Miles';
		$twentyk_distancename		= 'All Day 20K';
		$kidsrock_distancename		= 'KiDS Rock';
		$funrun_distancename		= 'Fun Run';
		$happyrun_distancename		= 'Happy Run';

		$grd_event_reg_closed		= 'Registration is not currently open. Sign up to be the first to know when it does.';
		$grd_event_reg_soldout		= 'This event is sold out. We invite you to join us for another distance or Rock \'n\' Roll Marathon Series event.';
		$grd_about_remix_txt		= 'About Remix';
		$grd_about_remix_short_txt	= 'About Remix';
		$grd_race_amenities_txt		= 'Race Amenities';
		$grd_race_features_txt		= 'Race Features';

		$filterby_txt				= 'Filter By';
		$alldates_txt				= 'All Dates';
		$alldistances_txt			= 'All Distances';
		$season_summer_txt			= 'Summer';
		$season_fall_txt			= 'Fall';
		$season_winter_txt			= 'Winter';
		$season_spring_txt			= 'Spring';

		$grd_amenity_1	= 'T-Shirt';
		$grd_amenity_10	= 'Brooks Participant T-Shirt';
		$grd_amenity_11	= 'Participant T-Shirt';
		$grd_amenity_2	= 'Finisher Medal';
		$grd_amenity_3	= 'Headliner Concert';
		$grd_amenity_4	= 'On Course Music & Entertainment';
		$grd_amenity_9	= 'Upgraded on-course music & entertainment';
		$grd_amenity_5	= 'Expo';
		$grd_amenity_8	= 'Marathon Finisher Jacket';

		$grd_amenity_6			= 'VIP Experience';
		$grd_amenity_6_extra	= 'Upgrade your race day with private restrooms, gear check, food, and more!';
		$grd_amenity_7			= 'Remix Challenge';
		$grd_amenity_7_extra	= 'Run two days and earn three medals';
		$grd_amenity_12			= 'Refreshed Race Experience';
		$grd_amenity_12_extra	= 'In 2018, we are bringing you an improved experience from start to  finish. With miles of new entertainment, music every mile, upgraded aid  stations and more, we’ll make you feel like a Rockstar when you run with  us.';

		$grd_feature_miles	= 'Course Description';
		$grd_miles_1		= 'Flat';
		$grd_miles_2		= 'Fast';
		$grd_miles_3		= 'Scenic';
		$grd_miles_4		= 'All New Course';
		$grd_miles_5		= 'All New Courses';
		$grd_miles_6		= 'New Course';
		$grd_miles_7		= 'New Courses';
		$grd_miles_8		= 'Challenging';
		$grd_miles_9		= 'Hilly';
		$grd_miles_10		= 'Downtown';
		$grd_miles_11		= 'Landmark Lined';
		$grd_miles_12		= 'Historic Course';
		$grd_miles_13		= 'City Streets';
		$grd_miles_14		= 'Night Race';
		$grd_miles_15		= 'IAAF Gold Certified';
		$grd_miles_16		= 'Spectator Supported';
		$grd_miles_17		= 'New 5K Course';
		$grd_miles_18		= 'New 10K Course';
		$grd_miles_19		= 'New 1/2 Marathon Course';
		$grd_miles_20		= 'New Marathon Course';
		$grd_miles_21		= 'Park Views';
		$grd_miles_22		= 'Parks';

		$grd_feature_views	= 'Views';
		$grd_views_1		= 'Coastal';
		$grd_views_2		= 'Riverside';
		$grd_views_3		= 'Waterfront';
		$grd_views_4		= 'Parks';
		$grd_views_5		= 'Cityscape';
		$grd_views_6		= 'Downtown';
		$grd_views_7		= 'Historic Districts';
		$grd_views_8		= 'Iconic Landmarks';
		$grd_views_9		= 'Country';
		$grd_views_10		= 'Hip Neighborhoods';
		$grd_views_11		= 'Beachfront';
		$grd_views_12		= 'Skyline';
		$grd_views_13		= 'City Views';
		$grd_views_14		= 'Coastal Views';
		$grd_views_15		= 'Skyline Views';
		$grd_views_16		= 'Neighborhood Tour';

		$grd_feature_qualifier	= 'Limited Edition Medal Qualifier';
		$grd_quals_1			= 'Marathon Challenge Qualifier:<br>Finish any 3 Marathons';
		$grd_quals_2			= 'Bucket List Challenge Qualifier:<br>Run San Francisco and Brooklyn in 2018';
		$grd_quals_3			= 'World Rocker Qualifier:<br>Run in any two countries in 2018';
		$grd_quals_4			= 'Beach to Beach Qualifier:<br>Run any distance in San Diego and Virginia Beach in 2018';
		$grd_quals_5			= 'Cali Combo Qualifier:<br>Run any three marathon or half marathons in California races in 2018';

		$grd_years_running	= ' Years Running';
		$grd_website		= 'Event Website';
	}


	/**
	 * travel
	 */
	$travel_quickfacts['title']		= 'Quick Facts';
	// $travel_airport['title']		= 'Airports';
	$travel_transportation['title']	= 'Airport Transportation &amp; Parking';
	$travel_nearby['title']			= 'Nearby Airports';
	$travel_booking['title']		= 'Booking Tips';


	/**
	 * Blog/Tempo
	 */
	if( $qt_lang['lang'] == 'es' ) {
	} elseif( $qt_lang['lang'] == 'fr' ) {
	} elseif( $qt_lang['lang']	== 'pt' ) {
	} else {
		// home
		$welcome_txt				= 'Welcome to The Tempo';
		$more_featured_stories_txt	= 'MORE FEATURED STORIES';
		$inspire_you_txt			= 'Stories that\'ll inspire you';
		$featured_videos_txt		= 'FEATURED VIDEOS';
		$motivate_you_txt			= 'Videos that\'ll motivate you';
		$latest_posts_txt			= 'LATEST POSTS';
		$by_txt						= 'By';

		// sidebar
		$cant_find_it_txt			= 'Can\'t find it';
		$browse_by_cat_txt			= 'Browse by category';
		$to_our_newsletter_txt		= 'to our newsletter';

		// single
		$related_stories_txt		= 'RELATED STORIES';
		$author_txt					= 'Author';
		$author_posts_by_txt		= 'View all posts by ';

	}

?>
