<?php
	/* Template Name: Runner Perks - Vegas */
	get_header();

	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$prefix = '_rnr3_';

	$parent_slug = the_parent_slug();
	rnr3_get_secondary_nav( $parent_slug );

	$args = array(
		'post_type' => 'runner_perk_vegas',
		'orderby' => 'meta_value',
		'order' => 'ASC',
		'posts_per_page' => -1,
		'meta_key' => $prefix . 'featured',
		'post_status' => 'publish'
	);
	if ( false === ( $runner_perks_lv_query = get_transient( 'runner_perks_lv_results' ) ) ) {
		$runner_perks_lv_query = new WP_Query( $args );
		set_transient( 'runner_perks_lv_results', $runner_perks_lv_query, 1 * MINUTE_IN_SECONDS );
	}


?>

	<!-- main content -->
	<main role="main" id="main">
		<section id="runner_perks_lv">
			<section class="wrapper center">
				<?php
					// allowing custom text if wanted
					if (have_posts()) : while (have_posts()) : the_post();
						echo '<h2>'. get_the_title(). '</h2>';
						the_content();
					endwhile; endif;

					$mix_grid = '';
					$property_array = array();

					if( $runner_perks_lv_query->have_posts() ) {
						while( $runner_perks_lv_query->have_posts() ) {
							$runner_perks_lv_query->the_post();

							$property = get_post_meta( get_the_ID(), $prefix . 'property', 1 );

							$remove_array = array( "'", ' ' );  // remove these symbols to make a nice classname
							$property_class = strtolower( str_replace( $remove_array, '', $property ) );
							$property_array[ $property_class ] = $property;

							$category_filters = get_post_meta( get_the_ID(), $prefix . 'categories', 1 );
							if( $category_filters != '' ) {
								$cat_array = array();
								foreach( $category_filters as $filter ) {
									$filter_pieces = explode( '::', $filter );
									$cat_array[] = 'perk-' . $filter_pieces[0];
								}
								$filter_string = implode( ' ', $cat_array );
							}

							$mix_grid .= '<div class="mix column_special '. $filter_string . ' ' . $property_class . '">
								<div class="inner_column_special">
									<h2>'. get_the_title() .'</h2>';
									if ( has_post_thumbnail() ) {
										$mix_grid .= '<figure>'. get_the_post_thumbnail( get_the_ID(), 'full' ) . '</figure>';
									} else {
										$mix_grid .= '<figure><img src="'. get_bloginfo( 'stylesheet_directory' ) .'/img/perks-default.jpg"></figure>';
									}
									$mix_grid .= '<h3>'. get_post_meta( get_the_ID(), $prefix . 'property', 1 ) .'</h3>';

									$perks_group = get_post_meta( get_the_ID(), $prefix . 'perk_group', 1 );
									$mix_grid .= '<div class="column_special_content">';
										foreach( $perks_group as $key => $entry ) {
											$mix_grid .= '<p>
												<strong>'. $entry['perk'] .'</strong>';
												if( isset( $entry['extra_info'] ) && $entry['extra_info'] != '' ) {
													$mix_grid .= '<br> <span class="perk_extra">&ndash; '. esc_html( $entry['extra_info'] ) . '</span>';
												}
											$mix_grid .= '</p>';
										}
									$mix_grid .= '</div>';

									if( get_post_meta( get_the_ID(), $prefix . 'url', 1 ) != '' ) {
										$mix_grid .= '<p><a class="cta" href="'. get_post_meta( get_the_ID(), $prefix . 'url', 1 ) .'" target="_blank">Details</a></p>';
									}
									if( is_user_logged_in() ) {
										$mix_grid .= '<p class="post-edit"><a href="'. get_edit_post_link( get_the_ID() ) . '">edit</a></p>';
									}
								$mix_grid .= '</div>
							</div>';
						}
					}
					wp_reset_postdata();

					echo '<form class="controls" id="filters">
						<fieldset id="sort">
							<label>Show</label>
							<select id="sortselect">
								<option value="">All Hotels</option>';
								foreach( $property_array as $key => $value ) {
									echo '<option value=".'. $key .'">'. $value .'</option>';
								}
							echo '</select>
						</fieldset>

						<fieldset>
							<label class="filterlabel">Filter</label>
							<button class="filter active show-all" data-filter="">All</button>
							<button class="filter" data-filter=".perk-featured">Featured</button>
							<button class="filter" data-filter=".perk-dining">Dining</button>
							<button class="filter" data-filter=".perk-entertainment">Entertainment</button>
							<button class="filter" data-filter=".perk-nightlife">Nightlife</button>
							<button class="filter" data-filter=".perk-transportation">Transportation</button>
							<button class="filter" data-filter=".perk-shopping">Shopping</button>
							<button class="filter" data-filter=".perk-spa">Spa</button>
						</fieldset>
					</form>

					<div class="grid_3_special">
						'. $mix_grid .'
					</div>

					<div class="gap"></div>
					<div class="gap"></div>
					<div class="gap"></div>';
				?>
				<script src="//cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
				<script>
					// To keep our code clean and modular, all custom functionality will be contained inside a single object literal called "buttonFilter".

					var buttonFilter = {

						// Declare any variables we will need as properties of the object

						$filters: null,
						$reset: null,
						groups: [],
						outputArray: [],
						outputString: '',

						// The "init" method will run on document ready and cache any jQuery objects we will need.

						init: function(){
							var self = this; // As a best practice, in each method we will asign "this" to the variable "self" so that it remains scope-agnostic. We will use it to refer to the parent "buttonFilter" object so that we can share methods and properties between all parts of the object.

							self.$filters = $('#filters');
							self.$reset = $('#reset');
							self.$container = $('.grid_3_special');

							self.$filters.find('fieldset').each(function(){
								var $this = $(this);

								self.groups.push({
									$buttons: $this.find('.filter'),
									$dropdown: $this.find('select'),
									active: ''
								});
							});

							self.bindHandlers();
						},

						// The "bindHandlers" method will listen for whenever a button is clicked.

						bindHandlers: function(){
							var self = this;

							// Handle filter clicks

							self.$filters.on('click', '.filter', function(e){
								e.preventDefault();

								var $button = $(this);

								// If the button is active, remove the active class, else make active and deactivate others.

								$button.hasClass('active') ?
									$button.removeClass('active') :
									$button.addClass('active').siblings('.filter').removeClass('active');

								self.parseFilters();
							});

							// Handle dropdown change

							self.$filters.on('change', function(){
								self.parseFilters();
							});

							// Handle reset click

							self.$reset.on('click', function(e){
								e.preventDefault();

								self.$filters.find('.filter').removeClass('active');
								self.$filters.find('.show-all').addClass('active');
								self.$filters.find('select').val('');

								self.parseFilters();
							});
						},

						// The parseFilters method checks which filters are active in each group:

						parseFilters: function(){
							var self = this;

							// loop through each filter group and grap the active filter from each one.

							for(var i = 0, group; group = self.groups[i]; i++){
								group.active = group.$buttons.length ?
									group.$buttons.filter('.active').attr('data-filter') || '' :
									group.$dropdown.val();
							}

							self.concatenate();
						},

						// The "concatenate" method will crawl through each group, concatenating filters as desired:

						concatenate: function(){
							var self = this;

							self.outputString = ''; // Reset output string

							for(var i = 0, group; group = self.groups[i]; i++){
								self.outputString += group.active;
							}

							// If the output string is empty, show all rather than none:

							!self.outputString.length && (self.outputString = 'all');

							console.log(self.outputString);

							// ^ we can check the console here to take a look at the filter string that is produced

							// Send the output string to MixItUp via the 'filter' method:

							if(self.$container.mixItUp('isLoaded')){
								self.$container.mixItUp('filter', self.outputString);
							}
						}
					};

					// On document ready, initialise our code.

					$(function(){

						// Initialize buttonFilter code

						buttonFilter.init();

						// Instantiate MixItUp

						$('.grid_3_special').mixItUp({
							controls: {
								enable: false // we won't be needing these
							},
						});
					});
				</script>

			</section>
		</section>

	</main>

<?php get_footer(); ?>
