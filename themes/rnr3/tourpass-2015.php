<?php
  /* Template Name: TourPass - 2015 (Series) */
  get_header('oneoffs');

  $toupass_slug = basename(get_permalink());
  /* if( $toupass_slug == 'tourpass-2015' ) {
    $tourpass_year = '2015';
  } elseif( $toupass_slug == 'tourpass-2016' ) {
  } */
  $tourpass_year = '2016';
  $new_year_2016_seconds = strtotime('2016-01-01');

?>
  <style>
    .group {
      padding:60px 0 30px;
    }
    .tp-tabs .ui-tabs-nav {
      border-bottom:1px solid #e5e5e5;
      margin:0 0 30px 0;
      text-align:center;
    }
    .tp-tabs .ui-tabs-nav li:first-child {
      margin-left:0;
      border-left:1px solid #e5e5e5;
    }
    .tp-tabs .ui-tabs-nav li {
      border-right:1px solid #e5e5e5;
      border-top:1px solid #e5e5e5;
      display:inline-block;
      margin:0 0 -1px 0;

    }
    .tp-tabs .ui-tabs-nav li a,
    .tp-tabs .ui-tabs-nav li.ui-state-active span {
      background-color:#333;
      color:#fff;
      display:block;
      padding:10px 20px;
    }
    .tp-tabs .ui-tabs-nav li.ui-state-active a,
    .tp-tabs .ui-tabs-nav li.ui-state-active span {
      color:#333;
      background-color:#fff;
      padding:10px 20px;
      display:block;

    }
    .tourpass_video,
    .narrow {
      width:75%;
      margin:auto;
    }
    .steps {
      background:url(<?php bloginfo('stylesheet_directory'); ?>/img/tourpass/1-2-3-sprite.png) no-repeat top left;
      width:40px;
      height:40px;
    }
    .circle2 {
      background-position:-40px 0;
    }
    .circle3 {
      background-position:-80px 0;
    }
    .tour_stops_list {
      text-align:center;
    }
    .tour_stops_table {
      vertical-align:top;
      margin:0 10px 60px 10px;
      display:inline-block;
    }
    .tour_stops_table td {
      padding:10px;
      text-align:center;
      border-bottom:1px solid #e5e5e5;
    }
    .europe_stops {
      margin-bottom:60px;
    }
    .owl-carousel {
      border-top:1px solid #e5e5e5;
    }
    .owl-carousel.oc-noborder {
      border:none;
    }
    .tp_nav {
      display:table;
      margin:auto;
      background-color:#e5e5e5;
    }
    .tp_nav li {
      float:left;
    }
    .tp_nav ul {
      margin:0;
      list-style:none;
    }
    .tp_nav li {
      margin:0;
    }
    .tp_nav .bullet-title {
      padding:30px;
    }
    .tp_nav a {
      padding:30px;
      display:block;
    }
    .tp_nav a:hover,
    .tp_nav .bling-current-page {
      background-color:#eee;
    }

  </style>
  <main role="main" id="main">
    <section class="wrapper">
      <div class="tp_nav" id="tp_top">
        <ul>
          <li><a href="#start">Start</a></li>
          <li><a href="#purchase">Purchase</a></li>
          <li><a href="#info2">Info</a></li>
          <li><a href="#perks">Perks</a></li>
          <li><a href="#social">Social</a></li>
        </ul>
      </div>

    </section>
    <section class="wrapper group" id="start">

      <div class="grid_2 center">
        <div class="column">
          <figure>
            <img src="<?php bloginfo('stylesheet_directory'); ?>/img/tourpass/tourpass-logo.png">
          </figure>
        </div>
        <div class="column">
          <h1 class="tp_title">Start Your Rock 'n' Roll Tour!</h1>
          <?php echo apply_filters('the_content', get_post_meta (get_the_ID(), '_tourpass_intro', true)); ?>
          <p><a class="cta" href="#purchase">Get Your Tourpass</a></p>
        </div>
      </div>
    </section>

    <section class="wrapper group center" id="purchase">

      <?php
        $tourpass_3_pack_text_2015 = apply_filters('the_content', get_post_meta (get_the_ID(), '_tourpass_3_pack_text_2015', true));
        $tourpass_unlimited_text_2015 = apply_filters('the_content', get_post_meta (get_the_ID(), '_tourpass_unlimited_text_2015', true));
        $tourpass_global_text_2015 = apply_filters('the_content', get_post_meta (get_the_ID(), '_tourpass_global_text_2015', true));
        $tourpass_faq = apply_filters('the_content', get_post_meta (get_the_ID(), '_tourpass_faq', true));
        $tourpass_terms_text = apply_filters('the_content', get_post_meta (get_the_ID(), '_tourpass_terms_text', true));
        $tourpass_credential = apply_filters('the_content', get_post_meta (get_the_ID(), '_tourpass_credential', true));
        $tourpass_wristband = apply_filters('the_content', get_post_meta (get_the_ID(), '_tourpass_wristband', true));
        $tourpass_early_updates = apply_filters('the_content', get_post_meta (get_the_ID(), '_tourpass_early_updates', true));
        $tourpass_vip = apply_filters('the_content', get_post_meta (get_the_ID(), '_tourpass_vip', true));
        $tourpass_heavy_medals = apply_filters('the_content', get_post_meta (get_the_ID(), '_tourpass_heavy_medals', true));
      ?>
      <h2>Choose Your TourPass</h1>
      <div id="pkg-tabs" class="tp-tabs">
        <ul class="ui-tabs-nav">
          <li class="ui-state-active"><span><?php echo $tourpass_year; ?></span></li>
        </ul>

        <?php
          if( $tourpass_year == '2016' ) {
            echo '<div class="grid_2">';
          } else {
            echo '<div class="grid_3">';
          }
        ?>
        
          <div class="column">
            <figure>
              <img src="<?php bloginfo('stylesheet_directory'); ?>/img/tourpass/package-3-pack.png">
            </figure>
            <h3>TourPass 3-Pack</h3>
            <?php echo $tourpass_3_pack_text_2015; ?>
          </div><?php if($tourpass_year != '2016') { ?><div class="column">
              <figure>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/img/tourpass/package-unlimited-pack.png">
              </figure>
              <h3>TourPass North America</h3>
              <?php echo $tourpass_unlimited_text_2015; ?>
            </div><?php } ?><div class="column">
            <figure>
              <img src="<?php bloginfo('stylesheet_directory'); ?>/img/tourpass/tourpass-global.png">
            </figure>
            <h3>TourPass Global</h3>
            <?php echo $tourpass_global_text_2015; ?>
          </div>

        </div>
      </div>
    </section>


    <section class="wrapper group" id="info2">

      <h2 class="center">Information</h2>
      <div id="info-tabs" class="tp-tabs">
        <ul>
          <li><a href="#info-how">How</a></li><li><a href="#info-where">Where</a></li><li><a href="#info-faqs">FAQs</a></li><li><a href="#info-terms">Terms</a></li>
        </ul>
        <div id="info-how">
          <?php
            $tourpass_youtube_id = get_post_meta (get_the_ID(), '_tourpass_youtube_id', true);
            $tourpass_choose_text = apply_filters('the_content', get_post_meta (get_the_ID(), '_tourpass_choose_text', true));
            $tourpass_register_text = apply_filters('the_content', get_post_meta (get_the_ID(), '_tourpass_register_text', true));
            $tourpass_reap_text = apply_filters('the_content', get_post_meta (get_the_ID(), '_tourpass_reap_text', true));

            if ($tourpass_youtube_id) { ?>
              <div class="tourpass_video">
              <h3>How Tourpass Works</h2>
                <div class="video-container">
                  <iframe width="1280" height="720" src="//www.youtube.com/embed/<?php echo $tourpass_youtube_id; ?>" frameborder="0" allowfullscreen></iframe>
                </div>
              </div>
            <?php }
          ?>
          <div class="grid_3 center">
            <div class="column">
              <figure><img src="<?php bloginfo('stylesheet_directory'); ?>/img/blank.gif" class="steps circle1"></figure>
              <h3>Choose your TourPass</h2>
              <?php echo $tourpass_choose_text; ?>
              <p><a href="#purchase">See TourPass options</a></p>
            </div><div class="column">
              <figure><img src="<?php bloginfo('stylesheet_directory'); ?>/img/blank.gif" class="steps circle2"></figure>
              <h3>Register To Rock</h2>
              <?php echo $tourpass_register_text; ?>
            </div><div class="column">
              <figure><img src="<?php bloginfo('stylesheet_directory'); ?>/img/blank.gif" class="steps circle3"></figure>
              <h3>Reap The Rewards</h2>
              <?php echo $tourpass_reap_text; ?>
            </div>
          </div>
          <p class="note center">Still have questions? <a href="#info-faqs">Check out the TourPass FAQ</a></p>
        </div>

        <div id="info-where">
          <div class="module_fourth">
            <h3>Where Will You Rock?</h3>
            <?php
              $myrows = $wpdb->get_results( "SELECT event_location, event_url, event_date, international_event, event_slug, event_logo, event_name, event_sold_out, RemixChallenge FROM wp_rocknroll_events WHERE event_slug != 'series' AND (event_location LIKE '%Canada%' OR international_event = 0) AND publish = 1 ORDER BY CASE WHEN event_date < NOW() THEN 1 ELSE 0 END, event_date" );

              $event_amount = count($myrows);
              $odd_event = $event_amount % 2;
              if ($odd_event == 1) {
                $half_event_amount = ($event_amount + 1) / 2;
              } else {
                $half_event_amount = $event_amount / 2;
              }

              echo '<div class="tour_stops_list">
                <h3>North American Tour Stops</h3>';
                /* how many events are there in north america? */
                $numbNAEvents = count($myrows);
                $remainder = $numbNAEvents % 2;
                if($remainder == 1) {
                  // do offset
                  $array1Size = floor($numbNAEvents / 2) + $remainder;
                  $array2Size = floor($numbNAEvents / 2);
                } else {
                  // no offset
                  $array1Size = $numbNAEvents / 2;
                  $array2Size = $array1Size;
                }
                $i = 0;
                foreach($myrows as $row) {
                  if($i == 0){
                    //start first table
                    echo '<table class="tour_stops_table" cellspacing="0" cellpadding="0" border="0">';
                  }
                  if($i == $array1Size){
                    //end one table and start the next
                    echo '</table>';
                    echo '<table class="tour_stops_table" cellspacing="0" cellpadding="0" border="0">';
                  }
                  $event_date_seconds = strtotime($row->event_date);

                  /* determine the subpage (2015 or 2016), then show appropriate dates,
                  "tba" for dates out of range */
                  $date = "";
                  if($tourpass_year == '2015') {
                    if($event_date_seconds > $new_year_2016_seconds) {
                      $date = "---";
                    } else {
                      $date = date('M d, Y', strtotime($row->event_date));
                    }
                  } elseif( $tourpass_year == '2016' ) {
                    if($event_date_seconds < $new_year_2016_seconds) {
                      $date = "TBA";
                    } else {
                      $date = date('M d, Y', strtotime($row->event_date));
                    }
                  }
                  if($row->event_date == '0000-00-00' || $row->event_date == '1970-01-01'){
                    $date = "TBA";
                  }

                  if($row->RemixChallenge == 1) {
                    $remix_star = '<span class="red">*</span> ';
                  } else {
                    $remix_star = '';
                  }
                  echo '
                    <tr class="'.$row->event_slug.'">
                      <td>' . $remix_star;
                  if($row->event_sold_out == 1){
                    echo '<del class="so_tp_d_d"><span class="so_tp_s_d">'.$date .'</span></del>';
                  } else {
                    echo $date;
                  }
                  echo '</td>
                      <td><a href="'.$row->event_url.'">';
                  if($row->event_sold_out == 1){
                    echo '<del class="so_tp_d"><span class="so_tp_s">'.$row->event_location .'</span></del>';
                  } else {
                    echo $row->event_location;
                  }
                  echo '</a></td>
                    </tr>
                  ';
                  //increase count by 1
                  $i++;
                  if($i == $numbNAEvents){
                    //close final table, as all results have been looped through
                    echo '</table>';
                  }
                }
              echo '</div>';
              /* international evenets - non-canada */
              /* get list of events into one array */
              $tourstops0 = $wpdb->get_results( "SELECT event_location, event_slug, event_url, event_date, international_event, event_name, event_sold_out, RemixChallenge FROM wp_rocknroll_events WHERE event_slug != 'series' AND event_location NOT LIKE '%Canada%' AND international_event = 1 and publish = 1 ORDER BY CASE WHEN event_date < NOW() THEN 1 ELSE 0 END, event_date" );

              echo '<div class="tour_stops_list europe_stops">
                <h3>European Tour Stops</h3>';
                /* how many events are there in europe? */
                $numbInterEvents = sizeof($tourstops0);
                $remainder = $numbInterEvents % 2;
                if($remainder == 1){
                  //do offset
                  $array1Size = ($numbInterEvents / 2) + $remainder;
                  $array2Size = $numbInterEvents / 2;
                }else{
                  //no offset
                  $array1Size = $numbInterEvents / 2;
                  $array2Size = $array1Size;
                }
                $i = 0;
                foreach($tourstops0 as $tourstop0) {
                  if($i == 0){
                    //start first table
                    echo '<table class="tour_stops_table" cellspacing="0" cellpadding="0" border="0">';
                  }
                  if($i == $array1Size){
                    //end one table and start the next
                    echo '</table>';
                    echo '<table class="tour_stops_table" cellspacing="0" cellpadding="0" border="0">';
                  }
                  $event_date_seconds = strtotime($tourstop0->event_date);

                  /* determine the subpage (2015 or 2016), then show appropriate dates,
                  "tba" for dates out of range */
                  $date = "";
                  if($tourpass_year == '2015') {
                    if($event_date_seconds > $new_year_2016_seconds) {
                      $date = "---";
                    } else {
                      $date = date('M d, Y', strtotime($tourstop0->event_date));
                    }
                  } elseif( $tourpass_year == '2016' ) {
                    if($event_date_seconds < $new_year_2016_seconds) {
                      $date = "TBA";
                    } else {
                      $date = date('M d, Y', strtotime($tourstop0->event_date));
                    }
                  }
                  if($tourstop0->event_date == '0000-00-00' || $tourstop0->event_date == '1970-01-01'){
                    $date = "TBA";
                  }

                  if($tourstop0->event_slug == 'nice') {
                    $nice_exception = ' *';
                  } else {
                    $nice_exception = '';
                  }
                  if($tourstop0->RemixChallenge == 1) {
                    $remix_star = '<span class="red">*</span>';
                  } else {
                    $remix_star = '';
                  }

                  echo '
                    <tr class="'.$tourstop0->event_slug.'">
                      <td>'.$remix_star .$date.'</td>
                      <td><a href="'.$tourstop0->event_url.'">'.$tourstop0->event_location . $nice_exception .'</a></td>
                    </tr>
                  ';
                  //increase count by 1
                  $i++;
                  if($i == $numbInterEvents){
                    //close final table, as all results have been looped through
                    echo '</table>';
                  }
                }
              echo '</div>';
            ?>
            <p class="center"><span class="red">*</span> <strong>Remix Challenge Events</strong></p>
          </div>
        </div>
        <div class="module_sixth" id="info-faqs">
          <div class="narrow">
           <h3>Frequently Asked Questions</h3>
           <?php echo $tourpass_faq; ?>
          </div>
        </div>
        <div class="module_seventh_container" id="info-terms">
          <div class="module_seventh">
            <div class="narrow">
              <h3>Terms And Conditions</h3>
              <?php echo $tourpass_terms_text; ?>
            </div>
          </div>
        </div>
      </div>

    </section>

    <section class="wrapper group" id="perks">


      <h2 class="center">Bling + Perks</h2>
      <div id="perks-tabs" class="tp-tabs">
        <ul>
          <li><a href="#perks-perks">Perks</a></li><li><a href="#perks-remix">Remix</a></li><li><a href="#perks-medals">Heavy Medals</a></li>
        </ul>
        <div id="perks-perks">
          <h3>Member Benefits and Perks</h2>
          <div class="grid_3 center">
            <div class="column">
              <figure><img src="<?php bloginfo('stylesheet_directory'); ?>/img/tourpass/tourpass-badge.jpg" alt=""></figure>
              <h3 class="subheader">Tourpass Credential</h3>
              <div class="perks-desc">
                <?php echo $tourpass_credential; ?>
              </div>
            </div>
            <div class="column">
              <?php if($tourpass_year == '2015') { ?>
                <figure><img src="<?php bloginfo('stylesheet_directory'); ?>/img/tourpass/wristbands.jpg" alt=""></figure>
              <?php } elseif( $tourpass_year == '2016' ) { ?>
                <figure><img src="<?php bloginfo('stylesheet_directory'); ?>/img/tourpass/wristbands-2016.jpg" alt=""></figure>
              <?php } ?>
              <h3 class="subheader">Tourpass Wristband</h3>
              <div class="perks-desc">
                <?php echo $tourpass_wristband; ?>
              </div>
            </div>
            <div class="column">
              <figure><img src="<?php bloginfo('stylesheet_directory'); ?>/img/tourpass/email.jpg" alt=""></figure>
              <h3 class="subheader">Early Updates</h3>
              <div class="perks-desc">
                <?php echo $tourpass_early_updates; ?>
              </div>
            </div>
          </div>
        </div>
        <div id="perks-remix">
          <h3>Remix Challenge</h2>
          <div class="grid_3">
            <div class="column">
              <figure><img src="<?php bloginfo('stylesheet_directory'); ?>/img/tourpass/remix-shirt.jpg" alt=""></figure>
              <h3 class="subheader">This is the remix</h3>
            </div><div class="column">
              <h3 class="subheader">Where to Earn It</h3>
              <?php $myrows = $wpdb->get_results( "SELECT event_location, event_url, event_date, international_event, event_slug, event_name, event_sold_out, RemixChallenge, festival, festival_start_date, festival_end_date FROM wp_rocknroll_events WHERE event_slug != 'series' AND (event_location LIKE '%Canada%' OR international_event = 0) AND publish = 1 AND international_event = 0 AND RemixChallenge = 1 ORDER BY event_date" ); ?>

              <table class="tour_stops_table remix_table" cellspacing="0" cellpadding="0" border="0">
                <?php foreach($myrows as $row) {
                  echo '<tr><td>';
                      if($row->festival == 1){
                        if($row->festival_start_date == '0000-00-00' || $row->festival_start_date == '1970-01-01'){
                          echo "TBA";
                        } elseif(date('M', strtotime($row->festival_start_date)) != date('M', strtotime($row->festival_end_date))) {
                          echo date('M d', strtotime($row->festival_start_date))."-".date('M d, Y', strtotime($row->festival_end_date));
                        } else {
                          echo date('M d', strtotime($row->festival_start_date))."-".date('d, Y', strtotime($row->festival_end_date));
                        }
                      } else {
                        echo date('M d, Y', strtotime($row->event_date));
                      }
                    echo '</td>
                    <td>
                      <a href="'.$row->event_url.'">';
                        if($row->event_sold_out == 1){
                          echo '<span class="strikeout">'.$row->event_location .'</span> *';
                        } else{
                          echo $row->event_location;
                        }
                      echo '</a>
                    </td>
                  </tr>';
                } ?>
              </table>
            </div><div class="column">
              <figure><img src="<?php bloginfo('stylesheet_directory'); ?>/img/tourpass/remix-medals.jpg" alt=""></figure>
              <h3 class="subheader">Remix Medals</h3>
            </div>
          </div>
          <p class="center"><a href="http://runrocknroll.lan/news/2015/04/complete-the-remix-challenge-earn-up-to-3-medals/" class="cta">Get Details</a></p>
        </div>
        <div id="perks-medals">
          <div class="grid_2 center">
            <div class="column">
              <img src="<?php bloginfo('stylesheet_directory'); ?>/img/tourpass/heavy-medals-logo.png" alt="">
            </div>
            <div class="column">
              <?php echo $tourpass_heavy_medals; ?>
            </div>
          </div>
          <section class="group center">
            <div class="hm-limited-edition">
              <h3>Limited Edition</h2>
              <div id="hm-limited" class="owl-carousel">
                <div><img src="<?php bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015_world_rocker.png" alt=""></div>
                <div><img src="<?php bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2016_desert_double_down.png" alt=""></div>
                <div><img src="<?php bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015-mex-combo.png" alt=""></div>
                <div><img src="<?php bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015_cascadia2.png" alt=""></div>
                <div><img src="<?php bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015_cali_combo1.png" alt=""></div>
                <div><img src="<?php bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015_southern_charm.png" alt=""></div>
                <div><img src="<?php bloginfo('stylesheet_directory'); ?>/img/heavy-medals/2015_beast_east.png" alt=""></div>
              </div>
            </div>
            <p><a href="<?php echo site_url( '/medals/?section=heavy-medals' ); ?>" class="cta" target="_blank">View All Limited Edition Medals</a></p>
          </section>
          <section class="group center">
            <div class="hm-remix-challenge">
              <h3>Remix Challenge</h2>
              <div id="hm-remix" class="owl-carousel">
                <div><img src="<?php bloginfo('stylesheet_directory'); ?>/img/heavy-medals/remix-vb.png" alt=""></div>
                <div><img src="<?php bloginfo('stylesheet_directory'); ?>/img/heavy-medals/remix-sj.png" alt=""></div>
                <div><img src="<?php bloginfo('stylesheet_directory'); ?>/img/heavy-medals/remix-stl.png" alt=""></div>
                <div><img src="<?php bloginfo('stylesheet_directory'); ?>/img/heavy-medals/remix-van.png" alt=""></div>
                <div><img src="<?php bloginfo('stylesheet_directory'); ?>/img/heavy-medals/remix-sav.png" alt=""></div>
                <div><img src="<?php bloginfo('stylesheet_directory'); ?>/img/heavy-medals/remix-sa.png" alt=""></div>
              </div>
            </div>
            <p class="center"><a href="<?php echo site_url( '/medals/?section=remix-challenge' ); ?>" class="cta" target="_blank">View All Remix Challenge Medals</a></p>
          </section>

        </div>
      </div>
    </section>

    <section class="wrapper group" id="social">

      <h2 class="center">#RNRTourPass</h2>
      <?php 
        $count = 0;
        for ( $x = 1; $x < 11; $x++ ) {
          if (get_post_meta (get_the_ID(), '_tourpass_quote_'.$x.'', true)) {
            $count++;
          }
        }
      ?>
      <div id="tp-quotes" class="owl-carousel oc-noborder">
        <?php
          for ( $x = 1; $x <= $count; $x++ ) {
            $tourpass_quote = get_post_meta (get_the_ID(), '_tourpass_quote_'.$x.'', true);
            $tourpass_quote_attrib = get_post_meta (get_the_ID(), '_tourpass_quote_'.$x.'_attrib', true);
            echo '
              <div>
                <p class="quote">"'.$tourpass_quote.'"</p>
                <p class="attrib">'.$tourpass_quote_attrib.'</p>
              </div>
            ';
          }
        ?>
      </div>
      <div class="ig_gallery">
        <div id="pixlee_container"></div><script type="text/javascript">window.PixleeAsyncInit = function() {Pixlee.init({apiKey:"WQoUPrRWZqiM92HGW5X"});Pixlee.addSimpleWidget({albumId:6073,recipeId:220,displayOptionsId:4888,type:"horizontal",accountId:501});};</script><script src="//assets.pixlee.com/assets/pixlee_widget_1_0_0.js"></script>
      </div>
    </section>


  </main>

        <script>
          jQuery(function($) {
            $('#info-tabs').tabs();
            $('.info-tab-open').click(function (event) {
                var info_tab = $(this).attr('href');
                $('#info-tabs').tabs('select', info_tab);
            });
            $('#perks-tabs').tabs();
            $('.perks-tab-open').click(function (event) {
                var perks_tab = $(this).attr('href');
                $('#perks-tabs').tabs('select', perks_tab);
            });
          });

          $(document).ready(function() {
            $(".tp_nav a, .backtotop").click(function(evn){
                evn.preventDefault();
                $('html,body').scrollTo(this.hash, this.hash); 
            });

            $("#hm-limited").owlCarousel({
              autoPlay:true,
              navigation:true,
              navigationText:false,
              pagination:false
            });
            $("#hm-remix").owlCarousel({
              autoPlay:true,
              navigation:true,
              navigationText:false,
              pagination:false
            });
            $("#tp-quotes").owlCarousel({
              autoPlay:true,
              navigation:false,
              pagination:false,
              singleItem:true
            });
          });

        </script>


<?php get_footer('oneoffs'); ?>