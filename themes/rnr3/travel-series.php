<?php
	/* Template Name: Travel ( Series ) */

	get_header( 'special' );

	$market		= get_market2();
	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$prefix		= '_rnr3_';
	$qt_lang	= rnr3_get_language();
	include 'languages.php';
?>

<!-- main content -->
<main role="main" id="main" class="travel">

	<?php
		/**
		 * columns section
		 * cmb2 group that'll show the various subpages under travel
		 */
		$columns = get_post_meta( get_the_ID(), $prefix . 'columns', 1 );

		if( !empty( $columns ) && array_key_exists( 'image', $columns[0] ) ) {

			echo '<section class="grid_4_special travel_subpages">
				<div class="wrapper">';

				foreach( $columns as $column ) {

					echo '<div class="column_special">
						<figure><img src="'. $column['image'] .'" alt=""></figure>
						<h3>'. $column['title'] .'</h3>
						<p>'. $column['description'] .'</p>
					</div>';

				}

				echo '</div>
			</section>';

		}


		$events_dropdown	= get_post_meta( get_the_ID(), $prefix . 'choose_rnr_dropdown', 1 );
		asort( $events_dropdown );
		$events_dropdown	= ( $events_dropdown != '' ) ? array_filter( $events_dropdown ) : '';

		if( !empty( $events_dropdown ) ) {

			$race_dropdown_hdr = get_post_meta( get_the_ID(), $prefix . 'choose_rnr_hdr', 1 );

			/**
			 * use this method to parse the text since qtranslate-x isn't enabled on blog 1
			 */
			if( strpos( $race_dropdown_hdr, '[:en]' ) !== false ) {
				$race_dropdown_hdr_split	= preg_split( '(\\[.*?\\])', $race_dropdown_hdr );
				$race_dropdown_hdr_display	= $race_dropdown_hdr_split[1];
			} else {
				$race_dropdown_hdr_display = $race_dropdown_hdr;
			}

			echo '<section class="wrapper travel_events">

				<h3>'. $race_dropdown_hdr_display .'</h3>

				<select id="selectrace">
					<option value="box">-- '. $display_text['choose_race_location'] .' --</option>';

					foreach( $events_dropdown as $event_solo ) {

						$event_pieces	= explode( '::', $event_solo );

						if( strpos( $event_pieces[1], '[:en]' ) !== false ) {
							$event_location_split	= preg_split( '(\\[.*?\\])', $event_pieces[1] );
							$event_location_display	= $event_location_split[1];
						} else {
							$event_location_display = $event_pieces[1];
						}

						$event_url = site_url( $event_pieces[0] . '/the-weekend/travel/' );

						echo '<option value="'. $event_url .'">'. $event_location_display .'</option>';
					}
				echo '</select>'; ?>

				<script>
					$(function(){
						// bind change event to select
						$('#selectrace').bind('change', function () {
							var url = $(this).val(); // get selected value
							if (url) { // require a URL
								window.location = url; // redirect
							}
							return false;
						});
					});
				</script>

			<?php echo '</section>';

		}


		/**
		 * sponsors section
		 */
		$sponsors = get_post_meta( get_the_ID(), $prefix . 'sponsors', 1 );

		if( !empty( $sponsors ) && array_key_exists( 'image', $sponsors[0] ) ) {

			$sponsors = array_filter( $sponsors );

			if( !empty( $sponsors ) ) {

				echo '<section class="wrapper travel_sponsors">

					<h3>'. get_post_meta( get_the_ID(), $prefix . 'sponsor_hdr', 1 ) .'</h3>

					<ul class="sponsors_list">';

						foreach( $sponsors as $sponsor ) {

							echo '<li>
								<a href="'. $sponsor['url'] .'"><img src="'. $sponsor['image'] .'" alt=""></a>
							</li>';

						}

					echo '</ul>

				</section>';
			}
		}
	?>

</main>


<?php get_footer( 'series' ); ?>
