<?php
  /* Template Name: Latest Promo */
  get_header('oneoffs');

  global $wpdb;

  $latest_promo_events = $wpdb->get_results("SELECT event_location, event_slug, latest_promo, latest_promo_image, latest_promo_txt FROM wp_rocknroll_events WHERE latest_promo = 1 ORDER BY event_date");
  $banner = get_post_meta( $post->ID, '_rnr3_latest_promo_banner', TRUE );
  $bottom_txt = apply_filters( 'the_content', get_post_meta( $post->ID, '_rnr3_latest_promo_bottom_txt', TRUE ) );

?>
<main role="main" id="main">
  <section class="wrapper">
    <div class="content">
      <img class="banner15" src="<?php echo $banner; ?>" alt="Latest Promo" />
      <?php
        if ( have_posts() ) : while ( have_posts() ) : the_post();
          echo '<div class="intro15">';
            the_content();
          echo '</div>';
        endwhile;
        endif;
        
        $count = 0;
        if($latest_promo_events) {
          echo '<div class="grid_3_fifteen">';
            foreach($latest_promo_events as $event) {
              echo '<div class="column15 center">
                <div class="box15">
                  <h3>'.$event->event_location.'</h3>';
              	if(!empty($event->latest_promo_image)){
              		echo '<img src="'.stripslashes($event->latest_promo_image).'" alt="'.$event->event_slug.' Promo">';
              	}
                  echo '<p>'.stripslashes($event->latest_promo_txt).'</p>
                  <a class="cta" href="'.site_url('/' . $event->event_slug .'/register').'">REGISTER NOW</a>
                </div>
              </div>';

              $count++;
            }
          echo '</div>';
        }
        echo '<div class="bottom15">
          '.$bottom_txt.'
        </div>
      </div>

    </section>
  </main>';


get_footer('oneoffs'); ?>