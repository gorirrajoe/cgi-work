<?php
/**
 * Template Name: Registration Prices
 */

// error_reporting( E_ALL );
// ini_set( 'display_errors', 1 );
// ini_set( 'memory_limit', '-1' );
require_once 'functions_fz.php';

date_default_timezone_set( 'America/Los_Angeles' );

// check to make sure it's a POST
if( isset( $_SERVER['REQUEST_METHOD'] ) AND $_SERVER['REQUEST_METHOD'] == 'POST' ) {
	// determine credentials value
	$cal_credentials	= md5( 'virginia' . date( 'Ymd' ) );
	$match_fields		= array( "sub_event_id","division_id" );

	if( isset( $_POST['credentials'] ) AND $_POST['credentials'] == $cal_credentials ) {
		$regInsert	= '';
		$regSearch	= '';
		$values		= '';
		$regUpdate	= '';

		foreach( $_POST as $key => $value ) {
			// don't need to save credentials in table
			// ignore credentials field for insert

			if( $key != 'credentials' ) {

				if( strlen( $regInsert ) > 1 ) {
					$regInsert .= ",";
				}

				if( strlen( $values ) > 1 ) {
					$values .= ",";
				}

				if( in_array( $key, $match_fields ) ) {

					if( strlen( $regSearch ) > 1 ) {
						$regSearch .= " AND ";
					}

					$regSearch .= $key ." = ". sprintf( '"%s"', $value );

				} else {

					if( strlen( $regUpdate ) > 1 ) {
						$regUpdate .= " , ";
					}

					$regUpdate .= $key ." = ". sprintf( '"%s"', $value );
				}
				$values		.= sprintf( '"%s"', $value );
				$regInsert	.= $key;

			}
		}
		$regInfo = array();

		$regInfo['keys']			= $regInsert;
		$regInfo['values']			= $values;
		$regInfo['searchstring']	= $regSearch;
		$regInfo['updatestring']	= $regUpdate;
		$statusPrice				= regPriceInsert( $regInfo );
		// echo $statusPrice;

	} else {

		echo "failure";

	}

} else {
	// not a post request ignore
	// do we return failure message here?
	echo "failure";
}


/**
 * either add or update row to wp_registration_prices
 * possibly use wordpress database functions instead?
 */
function regPriceInsert( $regPriceInfo ) {
	// connect to db
	$table_name			= "wp_registration_prices";
	$selectStatement	= 'SELECT sub_event_id FROM '. EVENTDBNAME .'.'. $table_name .' WHERE '. $regPriceInfo['searchstring'];

	if( stripos($_SERVER['SERVER_NAME'], 'loc') !== false ) {
		$dbLinkConnection	= dbConnect( 'rnr' );
	} else {
		$dbLinkConnection	= dbConnect( 'events' );
	}

	$result1			= mysqli_query( $dbLinkConnection, $selectStatement );
	$affected_rows		= mysqli_affected_rows( $dbLinkConnection );

	// initially set return value to failure
	$reg_price_id = 'failure';

	if( $affected_rows == 1 ) {
		// return customer_id
		$update_query = 'UPDATE '. EVENTDBNAME .'.'. $table_name .' SET '. $regPriceInfo['updatestring'] .' WHERE '. $regPriceInfo['searchstring'];

		if( mysqli_query( $dbLinkConnection, $update_query ) ) {
			$reg_price_id = "success";
		} else {
			$reg_price_id = "failure";
		}

	} elseif( $affected_rows == 0 ) {
		// do insert since record does not exist
		$create_query = 'INSERT INTO '. EVENTDBNAME .'.'. $table_name .' ( '. $regPriceInfo['keys'] .' ) VALUES ( '. $regPriceInfo['values'] .' )';

		if( mysqli_query( $dbLinkConnection, $create_query ) ) {
			// $reg_price_id = mysqli_insert_id ( $dbLinkConnection );
			$reg_price_id = "success";
		} else {
			/*echo "Insert failed<br>";
			echo mysqli_errno( $dbLinkConnection ) . ": " . mysqli_error( $dbLinkConnection ) . "\n";*/
			$reg_price_id = "failure";
		}
	}

	dbClose( $dbLinkConnection );
	return $reg_price_id;

}
