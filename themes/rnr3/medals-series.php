<?php
  /* Template Name: M's - Medals (Series) */
  get_header('special');
  $prefix = '_rnr3_m_medals_';

?>
  <main role="main" id="main" class="m_medals">
    <section id="medals-panel1">
      <section class="wrapper">
        <section class="grid_2">
          <h2 class="center">
            <?php echo get_post_meta( get_the_ID(), $prefix . 'intro_hdr', 1 ); ?>
          </h2>
          <div class="column">
            <?php echo apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'intro_txt_l', 1 ) ); ?>
          </div>
          <div class="column">
            <?php echo apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'intro_txt_r', 1 ) ); ?>
          </div>
        </section>
      </section>
    </section>
    <section id="medals-panel2">
      <section class="wrapper">
        <div class="burst">
          <img src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/img/mmmm/burst-clean.svg">
        </div>
        <?php echo apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'medals_gallery', 1 ) ); ?>
        <div class="controls">
          <label>Sort By</label>

          <button class="sort" data-sort="city:asc">A-Z</button>
          <button class="sort" data-sort="date:asc">Date</button>
        </div>

        <?php
          if ( false === ( $medal_query = get_transient( 'medal_query_results' ) ) ) {
            $args = array(
              'post_type' => 'medal',
              'post_status' => 'publish',
              'posts_per_page' => -1,
              'orderby' => 'title',
              'order' => 'ASC'
            );
            $medal_query = new WP_Query( $args );
            set_transient( 'medal_query_results', $medal_query, 5 * MINUTE_IN_SECONDS );
          }


          $gallery_count = 0;

          if( $medal_query->have_posts() ) {
            echo '<div class="mix_group">';
              while( $medal_query->have_posts() ) {
                $medal_query->the_post();
                $gallery_count++;

                $event_location = get_the_title();
                $event_url = get_post_meta( get_the_ID(), '_rnr3_event_url', 1 );
                $event_date = get_post_meta( get_the_ID(), '_rnr3_event_date', 1 );
                $event_enddate = get_post_meta( get_the_ID(), '_rnr3_event_enddate', 1 );
                $event_medals = get_post_meta( get_the_ID(), '_rnr3_racetype_group', 1 );

                if( $event_date != $event_enddate && $event_enddate != '' ) {
                  if( date( 'M', $event_date ) != date( 'M', $event_enddate ) ) {
                    $event_monthdate  = date( 'M d', $event_date ).'-'.date( 'M d', $event_enddate );
                    $event_year       = date( 'Y', $event_enddate );
                  } else {
                    $event_monthdate  = date( 'M d', $event_date ).'-'.date( 'd', $event_enddate );
                    $event_year       = date( 'Y', $event_enddate );
                  }
                } else {
                  $event_monthdate  = date( 'M d', $event_date );
                  $event_year       = date( 'Y', $event_date );
                }

                if( count( $event_medals ) == 1 ) {
                  $carousel_or_no = ' class="no_carousel"';
                } else {
                  $carousel_or_no = '';
                }

                echo '<div class="mix" data-city="'. str_replace( ' ', '-', strtolower( $event_location ) ) .'" data-date="'.$event_enddate.'">
                  <div class="inner_mix">
                    <h3>
                      '.$event_location.'
                      <span>'.$event_monthdate.', '.$event_year.'</span>
                    </h3>';
                    if( $event_medals != '' ) {
                      echo '<div class="owl-carousel" id="gallery-carousel-'.$gallery_count.'">';
                        foreach( $event_medals as $key => $entry ) {
                          if( isset( $entry['image'] ) ) {
                            echo '<div>
                              <figure><img src="'.$entry['image'].'"></figure>
                              <p'.$carousel_or_no.'>'.$entry['_rnr3_race_type'].'</p>
                            </div>';

                          }
                        }
                      echo '</div>';
                    }
                    echo '<p><a class="cta" href="'.$event_url.'">Go To Race</a></p>
                  </div>
                </div>';
              }
            echo '</div>';
          }
          wp_reset_postdata();
          echo '<div class="gap"></div>
          <div class="gap"></div>
          <div class="gap"></div>
          <div class="gap"></div>';
        ?>

        <script src="//cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
        <script>
          jQuery(function($){
            var $container = $('#medals-panel2');

            $container.mixItUp();

          });

        </script>

      </section>
    </section>
  </main>
  <script>
    $(document).ready(function(){
        
        /**
         * This part causes smooth scrolling using scrollto.js
         * We target all a tags inside the nav, and apply the scrollto.js to it.
         */
        $("#secondary a").click(function(evn){
            evn.preventDefault();
            $('html,body').scrollTo(this.hash, this.hash); 
        });

        <?php for( $i = 1; $i <= $gallery_count; $i++ ) { ?>
          $('#gallery-carousel-<?php echo $i; ?>').owlCarousel({
            singleItem : true,
            autoPlay : false,
            navigation : true,
            navigationText : ["prev", "next"],
            pagination : true,
            lazyLoad : true
          });
        
        <?php } ?>

    });

  </script>
<?php get_footer('series'); ?>