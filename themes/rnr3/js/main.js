/*
$( '#bigtext-2' ).bigtext( {
	maxfontsize: 180, // default is 528 ( in px )
	minfontsize: 24
} );
*/

$( window ).on( 'load', function() {
	$( '.hero-copy' ).bigtext( { maxfontsize: 180, minfontsize: 24 } );
} );

$( '#bigtext-lower' ).bigtext( {
	maxfontsize: 180, // default is 528 ( in px )
	minfontsize: 32
} );

$( '#sponsor-carousel' ).owlCarousel( {
	items : 7,
	autoPlay : true,
	navigation : true,
	navigationText : ["prev", "next"],
	pagination : false,
	lazyLoad : true,
	itemsScaleUp : true
} );

$( "#reg_testimonial" ).owlCarousel( {
	autoPlay : 10000,
	navigation:false,
	pagination:false,
	singleItem:true
} );

$( "#reg_testimonial_navi" ).owlCarousel( {
	autoPlay : 10000,
	navigation:true,
	pagination:true,
	singleItem:true
} );

$( '#highlight-carousel' ).owlCarousel( {
	items : 6,
	itemsCustom: [[0, 1], [480, 2], [768, 3], [960, 4], [1200, 6]],
	autoPlay : true,
	navigation : true,
	navigationText : ["prev", "next"],
	pagination : false,
	lazyLoad : true,
	itemsScaleUp : true
} );

$( '#bling-carousel' ).owlCarousel( {
	items : 3,
	autoPlay : false,
	navigation : true,
	navigationText : ["prev", "next"],
	pagination : true,
	lazyLoad : true
} );

$( '#viptestimonial-carousel' ).owlCarousel( {
	singleItem : true,
	autoPlay : 10000,
	stopOnHover : true,
	navigation : true,
	navigationText : ["prev", "next"]
} );

$( '.gallery-carousel' ).owlCarousel( {
	singleItem : true,
	autoPlay : false,
	slideSpeed : 300,
	navigation : true,
	navigationText : ["prev", "next"],
	pagination : true
} );


$( '.owl-singleposts' ).owlCarousel( {
	singleItem : true,
	autoPlay : false,
	slideSpeed : 300,
	pagination : true,
	afterAction: afterAction,
	lazyLoad: true
} );

function afterAction() {
	var target = '#' + $( this.owl.baseElement ).attr( 'id' ) + '-nav';
	$( target ).find( ".current-gallery-item" ).text( this.owl.currentItem + 1 );
	$( target ).find( ".total-gallery-items" ).text( this.owl.owlItems.length );
}

// Custom Navigation Events
$( ".owl-next" ).click( function() {
	var target = '#' + $( this ).parents( '.owl-singlepost-controls' ).attr( 'id' ).replace( '-nav', '' );
	$( target ).trigger( 'owl.next' );
} )
$( ".owl-prev" ).click( function() {
	var target = '#' + $( this ).parents( '.owl-singlepost-controls' ).attr( 'id' ).replace( '-nav', '' );
	$( target ).trigger( 'owl.prev' );
} )


$( '.owl-autoplay' ).owlCarousel( {
	singleItem : true,
	autoPlay : 10000,
	slideSpeed : 300,
	pagination : true,
	afterAction: afterAction
} );

// var owl2 = $( ".owl-autoplay" ),
//     status = $( ".owl-buttons" );

// function updateResult3( value ) {
//   $( ".owl-buttons" ).find( ".current-gallery-item" ).text( value );
// }
// function updateResult4( value ) {
//   $( ".owl-buttons" ).find( ".total-gallery-items" ).text( value );
// }

// function afterAction2() {
//   updateResult3( ( this.owl.currentItem +1 ) );
//   updateResult4( this.owl.owlItems.length );
// }

// Custom Navigation Events
// var owl2 = $( '.owl-autoplay' );
// $( ".owl-next" ).click( function() {
//   owl2.trigger( 'owl.next' );
// } )
// $( ".owl-prev" ).click( function() {
//   owl2.trigger( 'owl.prev' );
// } )



// Countdown timer
if( $( '.countdown' ).length ) {
$( '.countdown' ).each( function() {
	var $countdown = $( this );
	var raceDatetime = new Date( $countdown.data( 'countdown' ) * 1000 + 25200000 );
	setInterval( function () {
	var timespan = countdown( null, raceDatetime, -2018 ); // -2018 is days, hrs, mins, secs for this plugin
	$countdown.find( '.countdown-days' ).text( timespan.days < 10 ? '0' + timespan.days : timespan.days );
	$countdown.find( '.countdown-hrs' ).text( timespan.hours < 10 ? '0' + timespan.hours : timespan.hours );
	$countdown.find( '.countdown-mins' ).text( timespan.minutes < 10 ? '0' + timespan.minutes : timespan.minutes );
	$countdown.find( '.countdown-secs' ).text( timespan.seconds < 10 ? '0' + timespan.seconds : timespan.seconds );
	}, 1000 );
} );
}

$( document ).ready( function() {

	/**
	 * sticky alert bar
	 */
	if( $( '#alert' ).length ) {
		var $adminBar = $( '#wpadminbar' ),
			stickyNavTop = $( '#alert' ).offset().top;

		var stickyNav = function() {
			if( $(window).width() > 600 ) {
				$adminBarHeight = $adminBar.outerHeight();
			} else {
				$adminBarHeight = 0;
			}
			// console.log($adminBarHeight);
			var scrollTop = $( window ).scrollTop() + $adminBarHeight;

			if ( scrollTop > stickyNavTop ) {
				$( '#alert' ).addClass( 'sticky' );
				// $( 'header' ).addClass( 'non-sticky' );
				$( 'header' ).css( 'padding-top', $( '#alert' ).outerHeight() );
			} else {
				$( '#alert' ).removeClass( 'sticky' );
				// $( 'header' ).removeClass( 'non-sticky' );
				$( 'header' ).css( 'padding-top', '0' );
			}
		};

		stickyNav();
		$( window ).scroll( function() {
			stickyNav();
		} );
	}

	$( '#bigtext' ).bigtext( {
		maxfontsize: 180, // default is 528 ( in px )
		minfontsize: 24
	} );

	// Hamburger menu
	$( '.hamburger, .menulabel' ).click( function() {
		$( '.hamburger, .menulabel' ).toggleClass( 'open' );
		$( "#global-rnr-menu" ).toggleClass( "menuOpen" );
		$( "header" ).toggleClass( "menuIsOpen" );
		$( 'header .search-nav' ).removeClass('active');
		$( 'header .subscribe' ).removeClass('active');
	} );


	// Submenu menu for mobile
	$( '.menu-label' ).click( function() {
		$( this ).toggleClass( 'open' );
		$( "#secondary .menu" ).toggleClass( "menuOpen" );
	} );


	// Submenu menu for mobile
	$( '.subnav-menu-label' ).click( function() {
		$( this ).toggleClass( 'open' );
		$( "#subnav .subnav-menu" ).toggleClass( "menuOpen" );
	} );


	// Accordion functionality ( used on reg page, etc. )
	function close_accordion_section() {
		$( '.accordion .accordion-section-title' ).removeClass( 'active' );
		$( '.accordion .accordion-section-content' ).slideUp( 300 ).removeClass( 'open' );
	}

	$( '.accordion-section-title' ).click( function( e ) {
		// Grab current anchor value
		var currentAttrValue = $( this ).data( 'reveal' );

		if( $( e.target ).is( '.active' ) ) {
			close_accordion_section();
		}else {
			close_accordion_section();

			// Add active class to section title
			$( this ).addClass( 'active' );
			// Open up the hidden content panel
			$( '.accordion ' + currentAttrValue ).slideDown( 300 ).addClass( 'open' );
		}

		e.preventDefault();
	} );


	// event nav dropdown
	$( '.eventname_dropdown a' ).click( function( e ) {
		$( this ).toggleClass( 'open' );
		$( "#event-subnav" ).toggleClass( "menuOpen" );
		e.preventDefault();
	} );


	/**
	 * open video in modal
	 * anchor classed as "video-modal"
	 */
	/*$( ".video-modal" ).fancybox({
		openEffect: 'none',
		closeEffect: 'none',
		helpers: {
			overlay: {
				locked: false,
				css: {
					'background' : 'rgba(0, 0, 0, .8)'
				}
			}
		}
	});*/
	$( ".video-modal" ).fancybox({
		fullScreen: false,
		iframe: {
			css: {
				width: '800px',
				height: '600px',
				'max-width': '90%',
				'max-height': '80%'
			},
		}
	});

} );


// Show/hide functionality for hotels detail
$( '.show' ).click( function() {
	var link = $( this );
	$( this ).siblings( '.hide' ).slideToggle( 'fast', function() {
		if ( $( this ).is( ':visible' ) ) {
			link.text( 'Less details' );
		} else {
			link.text( 'More details' );
		}
	} );
} );

// Show/hide functionality for past resuls page
$( '.expand' ).click( function() {
	var link = $( this );
	$( this ).siblings( '.collapse' ).slideToggle( 'fast', function() {
		if ( $( this ).is( ':visible' ) ) {
			link.text( 'Collapse' );
		} else {
			link.text( 'Photo Galleries' );
		}
	} );
} );

// Show/hide functionality for moments-event
$( '.morevideos' ).click( function() {
	var link = $( this );
	$( '.lessvideos' ).slideToggle( 'fast', function() {
		if ( $( this ).is( ':visible' ) ) {
			link.text( 'View Less' );
		} else {
			link.text( 'View More' );
		}
	} );
} );

// Show/hide functionality for moments-event
$( '.morevideos2' ).click( function() {
	var link = $( this );
	$( '.lessvideos2' ).slideToggle( 'fast', function() {
		if ( $( this ).is( ':visible' ) ) {
			link.text( 'View Less' );
		} else {
			link.text( 'View More' );
		}
	} );
} );




// lazy load for series home and event hero bg's
$( function() {
	$( "section.lazy, div.lazy, img.lazy" ).lazyload( {
		effect : "fadeIn"
	} );
} );

$( ".wp-caption" ).removeAttr( 'style' );

$( window ).scroll( function() {
	if( $( this ).scrollTop() > 100 ) {
		$( '#goTop' ).stop().animate( {
			bottom: '20px'
		}, 500 );
	} else {
		$( '#goTop' ).stop().animate( {
			 bottom: '-100px'
		}, 500 );
	}
} );

$( '#goTop' ).on( 'click', function( e ) {
	$( 'html, body' ).stop().animate( {
		 scrollTop: 0
	}, 500, function() {
		$( '#goTop' ).stop().animate( {
			bottom: '-100px'
		}, 500 );
	} );
	e.preventDefault();
} );

window.twttr = ( function( d, s, id ) {
	var js, fjs = d.getElementsByTagName( s )[0],
		t = window.twttr || {};
	if ( d.getElementById( id ) ) return;
	js = d.createElement( s );
	js.id = id;
	js.src = "https://platform.twitter.com/widgets.js";
	fjs.parentNode.insertBefore( js, fjs );

	t._e = [];
	t.ready = function( f ) {
		t._e.push( f );
	};

	return t;
}( document, "script", "twitter-wjs" ) );

// ajax calls for home page "find a race" boxes
$( function() {
	$( '[data-rnr-event-id]' ).each( function() {
		var $this         = $( this ),
			carousel_test = false;

		$this.on( 'click', function( e ) {
			e.preventDefault();

			var originalText		= $this.text(),
				event_slug			= $this.data( 'rnr-event-slug' ),
				event_id			= $this.data( 'rnr-event-id' ),
				$target_modal		= $( '#modal-' + event_slug ),
				$carousel			= $target_modal.find( '.owl-carousel' ),
				ajax_data			= {
					'action': 'findrace_ajax_data',
					'event_slug': event_slug,
					'event_id': event_id
				};
				/*fancyBoxParams		= {
					padding: 0,
					width: '100%',
					autoSize: false,
					lazyLoad: true,
					scrolling: 'hidden',
					href: '#modal-' + event_slug,
					wrapCSS: 'fancybox-wp-wrap',
					helpers: {
						overlay: {
							locked: false,
							css: {
								'background': 'rgba( 0, 0, 0, .8 )'
							}
						}
					}
				}*/

			// replace original text to "Loading..."
			$this.text( 'Loading...' );

			if( $carousel.length && carousel_test == false ) {
				$.ajax( {
					url: findrace_ajax_data.ajax_url,
					type: 'POST',
					data: ajax_data,

					success: function( response ) {
						$this.text( originalText );
						if( response != '' ) {
							$carousel
								.html( response )
								.owlCarousel( {
									singleItem: true,
									lazyLoad : true
								} );

							carousel_test   = $carousel.data( 'owlCarousel' );
						}
						// $.fancybox( fancyBoxParams );
					},
					error: function( xhr, ajaxOptions, thrownError ) {
						$this.text( originalText );
						// dear ajax, bite my shiny metal a**
						alert( 'There was an error with your request. Please try again.' );
						// console.log( xhr );
					}
				} );
			} else {
				$this.text( originalText );
				// $.fancybox( fancyBoxParams );
			}
		} );

	} );
} );


/**
 * mixitup grids
 * ajax call for galleries
 *
 * requirements
 * cta anchor link that opens up the modal:
 *  - classed as "mix-cta fancybox"
 *  - data-post-id: {postid}
 *  - data-rnr-event-slug: {event city}
 *  - data-src: #modal-{event city}
 */
$( function() {
	var $mixboxes = $( '.mix-cta.fancybox' );

	if( $mixboxes.length > 0 ) {
		$mixboxes.each( function() {
			var $this 			= $( this ),
				carousel_test	= false;

			$this.on( 'click', function( e ) {
				e.preventDefault();

				var originalText	= $this.text(),
					event_slug		= $this.data( 'rnr-event-slug' ),
					$target_modal	= $( '#modal-' + event_slug ),
					$carousel		= $target_modal.find( '.owl-carousel' ),
					post_id			= $this.data( 'post-id' ),
					ajax_data		= {
						'action': 'mix_ajax_data',
						'post_id': post_id
					};

				// replace original text to "Loading..."
				$this.text( 'Loading...' );

				if( $carousel.length && carousel_test == false ) {
					$.ajax( {
						url: mix_ajax_data.ajax_url,
						type: 'POST',
						data: ajax_data,

						success: function( response ) {
							$this.text( originalText );
							if( response != '' ) {
								$carousel
									.html( response )
									.owlCarousel( {
										singleItem: true,
										lazyLoad: true
									} );

								carousel_test = $carousel.data( 'owlCarousel' );
							}
							// $.fancybox( fancyBoxParams );
						},
						error: function( xhr, ajaxOptions, thrownError ) {
							$this.text( originalText );
							// dear ajax, bite my shiny metal a**
							alert( 'There was an error with your request. Please try again.' );
							// console.log( xhr );
						}
					} );
				} else {
					$this.text( originalText );
					// $.fancybox( fancyBoxParams );
				}
			} );

		} )
	}
} );


/**
 * hotel page function
 * takes a name for a class, will slide open/close that div
 */
function toggleByClass( className ) {

	$( "." + className ).slideToggle( 'fast' );

	if( !( mapInit.called ) ) {
		// call the mapInit function after 1 second to make sure the div is fully open!
		setTimeout( function() {

			mapInit();

		}, 1000 );
	}

}


/**
 * hotel page function
 * with two select options on hotel page,
 * this will reset one when the other is changed
 */
$( '#sort1' ).change( function() {
	$( '#sort2' ).prop( 'selectedIndex', 0 );
});
$( '#sort2' ).change( function() {
	$( '#sort1' ).prop( 'selectedIndex', 0 );
});

/**
 * liveblog fancybox
 */
$(".liveblog_pix").fancybox({
	fullScreen: false
});


/**
 * generic fancybox
 */
$(".fancy").fancybox({
	fullScreen: false,
	caption: function( instance, item ) {
		var caption;
		if( item.type === 'image' ) {
			caption = $( 'img', this ).attr( 'alt' );
			return caption;
		}
	}
});
