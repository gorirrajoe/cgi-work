/**
 * create cookie
 */
function createCookie(name,value,days) {
	var expires = "";
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days*24*60*60*1000));
		expires = "; expires=" + date.toUTCString();
	}
	document.cookie = name + "=" + value + expires + "; path=/";
}


/**
 * read cookie
 */
function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}


/**
 * if cookie ("grd-presale-form") exists,
 * only show success message
 * hide form and intro
 */
( function( $ ) {

	var x = readCookie( 'grd-presale-form' );
	if( x ) {

		// marquee form
		$( '.grd-sg-success' ).show();
		$( '.grd-sg-intro' ).hide();
		$( '.grd-sg-datacap' ).hide();

		// inline form
		// $( '.grd-sg__inline-success' ).show();
		// $( '.grd-sg__inline-intro' ).hide();
		// $( '#grd-sg__inline-datacap' ).hide();

	}

} )( jQuery );
