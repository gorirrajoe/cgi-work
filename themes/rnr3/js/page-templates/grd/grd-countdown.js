/**
 * Logic for displaying the "sticky" social sharing icons on the site.
 */
jQuery(document).ready(function($) {
	var $header 			= $('.grd-marquee'),
		$adminBar 			= $('#wpadminbar'),

		$topBar					= $('.grd-countdown-sticky'),
		$cards					= $('.grd-cards'),
		$desktopBar				= $( '.grd-filterbar__desktop' ),
		$desktopBarPlaceholder	= $( '.grd-filterbar__desktop-placeholder' ),
		topBoundry				= 0,
		desktopTopBoundry		= 0,
		positionTop				= 0,
		desktopPositionTop		= 0,
		windowWidth				= 0,
		adminBarSticky			= false,
		fixed					= false,
		desktopFixed			= false,
		fixedBottom				= false,
		desktopFixedTop			= false,
		longForm				= false;

	/**
	 * Init
	 *
	 * Constructor method which calls all the utility methods used to
	 * calculate the relevant element dimensions.
	 */
	function init() {

		windowWidth = ( $(window).width() + calcScrollbarWidth() );

		if( windowWidth >= 601 && $adminBar.length ) {

			adminBarSticky = true;

		}

		if( windowWidth < 960 ) {
			topBoundry	= calcTopBoundry();
			positionTop	= calcPositionTop();
		} else {
			desktopTopBoundry	= calcTopBoundry( 'desktop' );
			desktopPositionTop	= calcPositionTop( 'desktop' );
		}
	}


	/**
	 * Calculate Top Boundry
	 *
	 * "topBoundry" defines the point at which the social widget should
	 * become "fixed" when scrolling down, or "relative" when scrolling up.
	 *
	 * Note: This is the highest we want the widget to go on the page.
	 *
	 * @return int Position of the "top" scroll boundry.
	 */
	function calcTopBoundry( position ) {

		if( adminBarSticky == true ) {

			if( position == 'desktop' ) {

				return $desktopBarPlaceholder.offset().top - $adminBar.outerHeight();

			} else {

				return $cards.offset().top - $adminBar.outerHeight();

			}


		} else {

			if( position == 'desktop' ) {

				return $desktopBarPlaceholder.offset().top;

			} else {

				return $cards.offset().top;

			}

		}
	}


	/**
	 * Calculate Position Top
	 *
	 * "positionTop" defines the y-coordinate at which the social widget
	 * should remained fixed at while the widget is "fixed".
	 *
	 * @return int Position of the widget while "fixed".
	 */
	function calcPositionTop(position ) {

		if( position == 'desktop' ) {

			return ($header.outerHeight() + $adminBar.height() + 45);

		} else {

			return ($header.outerHeight() + $adminBar.height() + 15);

		}


	}


	/**
	 * Position Mobile Widget
	 *
	 * Controls the positioning of the social widget for screens that
	 * are <= 991px wide.
	 *
	 * @param int position Current position of the scrollbar
	 */
	function positionMobileWidget(position) {

		if( true === fixed ) {

			if( true === fixedBottom && position <= topBoundry ) {

				fixedBottom = false;

			}

			// top-boundry logic
			if( position <= topBoundry) {

				$topBar.removeClass('grd-countdown-sticky--fixed');

				fixed = false;

			}

		} else {

			// top-boundry logic
			if(position >= topBoundry ) {

				$topBar.addClass('grd-countdown-sticky--fixed');

				fixed = true;

			}

		}

		if( true === desktopFixed ) {

			if( true === desktopFixedTop && position <= desktopTopBoundry ) {

				desktopFixedTop = false;

			}

			if( position <= desktopTopBoundry ) {

				$desktopBar.removeClass('grd-filterbar__desktop--fixed');
				desktopFixed = false;
				$cards.removeClass('grd-cards--fixed');

			}

		} else {

			if( position >= desktopTopBoundry ) {

				$desktopBar.addClass('grd-filterbar__desktop--fixed');
				desktopFixed = true;
				$cards.addClass('grd-cards--fixed');

			}
		}

	}


	/**
	 * Note: wait until "load" event so element heights are set.
	 */
	$(window).on('load', function() {

		if( $topBar.length ) {

			init();

			// bind window events logic
			$(window).scroll(function() {

				positionMobileWidget( $(this).scrollTop() );

			}).resize(function() {

				var newWidth = ( $( window ).width() + calcScrollbarWidth() );

				// don't call init unless screen width has changed
				if( 0 === windowWidth || newWidth !== windowWidth ) {

					init();

				}

			});

		}

	});

});
