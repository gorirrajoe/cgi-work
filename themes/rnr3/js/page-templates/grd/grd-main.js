/**
 * initiate lazyloading on images
 */
$( function() {
	$( "img.lazy" ).lazyload({
		effect: "fadeIn"
	});
});


/**
 * document ready function
 *
 * start page off with highlighting 13.1 (add "active" class)
 * or 5k in carlsbad's case
 */
$( document ).ready( function() {
	$( '.grd-marquee-white h1' ).bigtext( {
		maxfontsize: 150, // default is 528 ( in px )
		minfontsize: 35
	} );

	var $half = $( '.mix-distancelist-item[data-distancelist-distance="mix-price-halfmarathon"]' );
	$half.addClass( 'mix-distancelist-item--active' );

	var $cbad = $( '.carlsbad-5000 .mix-distancelist-item[data-distancelist-distance="mix-price-fivek"]' );
	$cbad.addClass( 'mix-distancelist-item--active' );
});


/**
 * click OR hover function
 *
 * prices
 * on hover/click, of distance, change the price accordingly
 */
$( '.mix-distancelist-item' ).on( 'click mouseover', function( e ) {
	// e.preventDefault();
	var $city = $( this ).attr( 'data-distancelist-city' );

	$( '[data-city="'+ $city +'"]' ).find( '.mix-price-group .mix-price-span' ).css( 'display', 'none' );
	$( '[data-city="'+ $city +'"]' ).find( '.mix-cta-group .mix-price-span' ).css( 'display', 'none' );
	$( '[data-city="'+ $city +'"]' ).find( '.mix-distancelist div' ).removeClass( 'mix-distancelist-item--active' );


	var $distance = $( this ).attr( 'data-distancelist-distance' );

	// find the distance within that particular city and show it
	$( '[data-city="'+ $city +'"]' ).find( '.' + $distance ).css( 'display', 'flex' );

	// add "active" class to button
	$( this ).addClass( 'mix-distancelist-item--active' );

});


/**
 * click function
 *
 * toggle for details expansion within event cards
 */
$( '.mix-details-toggle' ).on( 'click', function( e ) {
	e.preventDefault();

	$("img.lazy").trigger( "sporty" );

	var $city = $( this ).attr( 'data-toggle-div' );
	$toggleId = $( '#' + $city );
	$( this ).find( '.icon-play' ).toggleClass( 'rotate' );

	$toggleId.slideToggle();
});


/**
 * scroll function
 *
 * move "go to top" arrow up a bit
 * to give room to the sticky filter menu
 */
$( window ).scroll( function() {
	if( $( this ).scrollTop() > 100 ) {
		$( '#goTop' ).stop().animate( {
			bottom: '70px'
		}, 500 );
	} else {
		$( '#goTop' ).stop().animate( {
			 bottom: '-100px'
		}, 500 );
	}
} );


/**
 * click function
 *
 * ajax load carousel when the toggle link is clicked (mobile)
 */
$( function() {
	var $mixboxes = $( '.grd_mobile.mix-details-toggle' );

	if( $mixboxes.length > 0 ) {
		$mixboxes.each( function() {
			var $this 			= $( this ),
				carousel_test	= false;

			$this.on( 'click', function( e ) {
				e.preventDefault();

				var event_slug		= $this.data( 'toggle-div' ),
					$target_modal	= $( '#' + event_slug ),
					$carousel		= $target_modal.find( '.owl-carousel' ),
					post_id			= $this.data( 'post-id' ),
					ajax_data		= {
						'action': 'mix_ajax_data',
						'post_id': post_id
					};

				if( $carousel.length && carousel_test == false ) {
					$.ajax( {
						url: mix_ajax_data.ajax_url,
						type: 'POST',
						data: ajax_data,

						success: function( response ) {
							if( response != '' ) {
								$carousel
									.html( response )
									.owlCarousel( {
										singleItem: true,
										lazyLoad: true
									} );

								carousel_test = $carousel.data( 'owlCarousel' );
							}
							// $.fancybox( fancyBoxParams );
						},
						error: function( xhr, ajaxOptions, thrownError ) {
							// dear ajax, bite my shiny metal a**
							alert( 'There was an error with your request. Please try again.' );
							// console.log( xhr );
						}
					} );
				} else {
					// $.fancybox( fancyBoxParams );
				}
			} );

		} )
	}
} );


/**
 * owl carousel init for tablet and up (>=720px)
 */
$( '.mix-modal-carousel-tabletup' ).owlCarousel( {
	singleItem : true,
	autoPlay : false,
	slideSpeed : 300,
	pagination : true,
	afterAction: afterAction,
	lazyLoad: true
} );


/**
 * if clicking outside of filterbar, collapse all dropdowns and reset triangles
 */
$( window ).click( function( e ) {
	$container = $( '.grd-filterbar__desktop' );
	$toggle = $( '.grd-filterbar__desktop .grd-filterbar-toggle' );

	if( !$container.is( e.target ) && $container.has( e.target ).length === 0 ) {
		$toggle.parentsUntil( 'ul' ).siblings().children().removeClass( 'grd-filterbar-toggle-active' );

		$toggle.siblings( '.grd-filterbar-content' ).slideUp();

		$toggle.find( '.icon-play' ).toggleClass( 'rotate' );
		$toggle.parent().siblings().find( '.icon-play' ).removeClass( 'rotate' );
	}


});


/**
 * click function
 *
 * filterbar  actions
 */
$( '.grd-filterbar-toggle' ).on( 'click', function( e ) {
	e.preventDefault();
	e.stopPropagation();

	$( this ).parentsUntil( 'ul' ).siblings().children().next( '.grd-filterbar-content' ).slideUp();
	$( this ).parentsUntil( 'ul' ).siblings().children().removeClass( 'grd-filterbar-toggle-active' );

	$( this ).toggleClass( 'grd-filterbar-toggle-active' );

	$( this ).siblings( '.grd-filterbar-content' ).slideToggle();

	$( this ).find( '.icon-play' ).toggleClass( 'rotate' );
	$( this ).parent().siblings().find( '.icon-play' ).removeClass( 'rotate' );
});


/**
 * click function
 *
 * for distance item in filterbar
 * collapse/expand each distance to reveal price ranges
 * also rotates triangle icon
 */
$( '.grd-filterbar-content-label' ).on( 'click', function() {

	$( this ).next( 'ul' ).slideToggle();
	$( this ).find( '.icon-play' ).toggleClass( 'rotate' );

	$( this ).parent().siblings().children( 'ul' ).slideUp();
	$( this ).parent().siblings().find( '.icon-play' ).removeClass( 'rotate' );

});
