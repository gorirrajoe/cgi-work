/**
 * mixitup
 */
var containerEl = document.querySelector( '.mix_group' );

var mixer = mixitup( containerEl, {
	load: {
		sort: 'featured:desc date:asc'
	},
	callbacks: {
		/**
		 * resolves issue when going from distance filter to city toggle
		 * if the active filter contains any of the distance filters,
		 * remove it and then proceed with toggle
		 */
		onMixClick: function(state, originalEvent) {
			if( $( this ).attr( 'data-toggle' ) ) {

				var datafilterarray = ['.fivek', '.tenk', '.halfmarathon', '.marathon',
				'.fivek_30', '.fivek_50', '.fivek_more',
				'.tenk_40', '.tenk_50', '.tenk_more',
				'.halfmarathon_60', '.halfmarathon_100', '.halfmarathon_more',
				'.marathon_80', '.marathon_100', '.marathon_more',
				'.fall17', '.winter18', '.spring18', '.summer18', '.fall18'];

				if( datafilterarray.indexOf( mixer.getState().activeFilter.selector ) != -1 ) {
					mixer.toggleOff( mixer.getState().activeFilter.selector );
				}
			}
		},
		onMixEnd: function( state ) {
			// bail if it was the "all" button that was triggered (will load all the images)
			if ( state.activeFilter.selector === '.mix' ) {
				return;
			}

			var matches = state.matching;

			for ( var i = 0; i < matches.length; i++ ) {
				var $element	= $( matches[i] );
				var $lazyImage	= $element.find('img.lazy');

				$lazyImage.trigger('appear');
			}

		}
	}

} );


/**
 * click function
 *
 * when distance is filtered, make sure correct prices and distances are shown
 */
$( '.grd-filterbar-content-control' ).on( 'click', function() {

	var $datafilter = $( this ).data( 'basedistance' ),
		$allcards = $( '.mix' );

	if( typeof $datafilter != 'undefined' ) {

		var $dataseason = $( this ).data( 'season-distance' );

		if( typeof $dataseason != 'undefined' ) {

			distance = 'halfmarathon';

		} else {

			if( $datafilter == 'all' ) {
				distance = 'halfmarathon';
			} else {
				distance = $datafilter;
			}
		}

	} else {

		distance = 'halfmarathon';

	}


	$allcards.each( function() {

		if( distance == 'halfmarathon' && $( this ).hasClass( 'carlsbad-5000' ) ) {
			// don't do anything for carlsbad since it doesn't have a half
		} else {
			$( this ).find( '.mix-distancelist-item[data-distancelist-distance]' ).removeClass( 'mix-distancelist-item--active' );
			$( this ).find( '.mix-distancelist-item[data-distancelist-distance="mix-price-'+ distance +'"]' ).addClass( 'mix-distancelist-item--active' );

			$( this ).find( '.mix-price-group .mix-price-span' ).hide();
			$( this ).find( '.mix-cta-group .mix-price-span' ).hide();
			$( this ).find( '.mix-price-' + distance ).show();
		}

	});
});
