/**
 * data capture
 * uses surveygizmo api
 */
( function( $ ) {
	/**
	 * hide empty error message when user clicks email field
	 */
	$( '.grd-sg-email' ).click( function() {
		$( this ).closest( '.grd-sg' ).find( '.grd-sg-empty' ).hide();
	} );


	/**
	 * when clicking cta button (get notified),
	 * get data from cta and set to form's hidden input value
	 */
	$( '.grd-sg-cta' ).on( 'click', function() {
		$event_from_cta = $( this ).data( 'sg-event' );
		$( '#grd-sg__inline-event' ).val( $event_from_cta );
	});


	/**
	 * form submission processing
	 * will handle both marquee and inline (modal) forms
	 *
	 * sets cookie upon success, which will prevent
	 * form from being submitted from same browser
	 */
	$( '.grd-sg-datacap' ).submit( function( e ) {
		var $button = $( this ).find( 'button' );

		if( $button.val() == 'marquee' ) {

			$hidden		= $( '#grd-sg-event' ).val();
			$fail		= $( '.grd-sg-failure' );

		} else {

			$hidden		= $( '#grd-sg__inline-event' ).val();
			$fail		= $( '.grd-sg__inline-failure' );

		}

		$form		= $( '.grd-sg-datacap' );
		$intro		= $( '.grd-sg-intro' );
		$email		= $( this ).find( '.grd-sg-email' ).val();
		$success	= $( '.grd-sg-success' );
		$empty		= $( '.grd-sg-empty' );

		if( $email == '' ) {

			$( this ).closest( '.grd-sg' ).find( $empty ).show();

		} else {

			var email_regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
			var is_email = email_regex.test( $email );

			if( is_email ) {
				$.ajax({
					url : window.location.href + '/wp-content/themes/rnr3/inc/grd-sg-proxy.php',
					type : 'POST',
					data : {
						email: $email,
						event: $hidden
					},
					dataType : 'json',
					success : function( data ) {
						console.log( 'success: ', data );
						$intro.hide();
						$form.hide();
						$success.show();

						// set cookie
						createCookie( 'grd-presale-form', 'subscribed', 10 );

					},
					error : function( jqXHR, textStatus, errorThrown ) {
						var error = JSON.stringify( jqXHR );
						console.log( 'error: ', error );
					},
				});
			} else {

				$fail.show();

			}

		}
		e.preventDefault();

	});

} )( jQuery );
