//@prepros-prepend vendor/mixitup.min.js
//@prepros-prepend modules/finisher-zone/social-sharing.js
//@prepros-prepend modules/finisher-zone/certificate.js
//@prepros-prepend modules/finisher-zone/carousels.js
//@prepros-prepend modules/finisher-zone/archive.js

twttr.ready(function (twttr) {
    twttr.events.bind(
        'tweet',
        function (event) {
            dataLayer.push({
                'event':  'fz_twitter_share',
                'type':   event.target.dataset.itemShared
            });
        }
    );
});
