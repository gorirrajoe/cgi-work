/**
 * Calculate Scrollbar Width
 *
 * Calculate the scrollbar width (if exists).
 *
 * @return int Width of scrollbars
 */
function calcScrollbarWidth() {
    var inner, outer, w1, w2;

    inner = document.createElement('p');
        inner.style.width = "100%";
        inner.style.height = "200px";

    outer = document.createElement('div');
        outer.style.position = "absolute";
        outer.style.top = "0px";
        outer.style.left = "0px";
        outer.style.visibility = "hidden";
        outer.style.width = "200px";
        outer.style.height = "150px";
        outer.style.overflow = "hidden";
        outer.appendChild(inner);

    document.body.appendChild(outer);

    w1 = inner.offsetWidth;
    outer.style.overflow = 'scroll';

    w2 = inner.offsetWidth;

    if(w1 == w2) {
        w2 = outer.clientWidth;
    }

    document.body.removeChild(outer);

    return (w1 - w2);
}

/**
 * Logic for displaying the "sticky" social sharing icons on the site.
 */
jQuery(document).ready(function($) {
	var $leftWidget = $('#social-widget-left'),
		$topWidget = $('#social-widget-top');

	var $topWidget	= $('#social-widget-top');

	if( $topWidget.length > 0 ) {

		/**
		 * Note: wait until "load" event so element heights are set.
		 */
		$(window).on('load', function() {
			var $header			= $('header'),
				$article		= $('#article'),
				$articleLeft	= $('#article-left'),
				$articleRight	= $('.article_type_single '),
				topBoundary		= 0,
				bottomBoundary	= 0,
				paddingLeft		= 0,
				positionTop		= 0,
				positionBottom	= 0,
				widgetWidth		= 0,
				windowWidth		= 0,
				activeWidget	= '',
				fixed			= false,
				fixedBottom		= false;

			/**
			 * Calculate Top Boundary
			 *
			 * "topBoundary" defines the point at which the social widget should
			 * become "fixed" when scrolling down, or "relative" when scrolling up.
			 *
			 * Note: This is the highest we want the widget to go on the page.
			 *
			 * @return int Position of the "top" scroll boundary.
			 */
			function calcTopBoundary() {

				if('mobile' === activeWidget) {
					return ($articleRight.offset().top + $articleRight.find('.article__thumbnail').height() - $header.height());
				} else {
					// return ($leftWidget.offset().top - $header.height() - 15);
				}
			}

			/**
			 * Calculate Bottom Boundary
			 *
			 * "bottomBoundary" defines the point at which the social widget
			 * should become "absolute" when scrolling down, or "fixed" when
			 * scrolling up.
			 *
			 * Note: This is the lowest we want the widget to go on the page.
			 *
			 * @return int Position of the "bottom" scroll boundary.
			 */
			function calcBottomBoundary() {

				if('mobile' === activeWidget) {
					return -($(window).height() - $articleRight.offset().top - $articleRight.outerHeight() - 25);
				} else {
					// return ($articleLeft.position().top + $articleLeft.height() - 15 );
				}
			}

			/**
			 * Calculate Position Top
			 *
			 * "positionTop" defines the y-coordinate at which the social widget
			 * should remained fixed at while the widget is "fixed".
			 *
			 * @return int Position of the widget while "fixed".
			 */
			function calcPositionTop() {
				return ($header.height() + 15);
			}

			/**
			 * Calculate Position Bottom
			 *
			 * "positionBottom" defines the lowest y-coordinate at which the
			 * social widget should remained fixed so that it doesn't escape
			 * it's container.
			 *
			 * @return int Position of the widget while "absolute".
			 */
			function calcPositionBottom() {
				return ($articleRight.height() - $leftWidget.height());
			}

			/**
			 * Calculate Widget Dimensions
			 *
			 * Calculates the necessary dimensions of the mobile "fixed" widget.
			 */
			function generateMobileWidgetStyles() {
				/*var style = '';

				style += '@media screen and (min-width:768px) {';
					style += '.social-widget--fixed {';
						style += 'padding-left:'+ ($article.position().left + 15) +'px;';
						style += 'width:'+ ($article.outerWidth() + $article.position().left) +'px;';
					style += '}';
				style += '}';

				if($('#triathlete-sw-css').length < 1) {
					$('<style id="triathlete-sw-css">'+ style +'</style>').appendTo('head');
				} else {
					$('#triathlete-sw-css').html(style);
				}*/

			}

			/**
			 * Init
			 *
			 * Constructor method which calls all the utility methods used to
			 * calculate the relevant element dimensions.
			 */
			function init() {

				$articleLeft.css('height', $articleRight.height());

				windowWidth = ($(window).width() + calcScrollbarWidth());
				activeWidget = (windowWidth <= 960 ? 'mobile' : 'desktop');

				topBoundary = calcTopBoundary();
				bottomBoundary = calcBottomBoundary();
				positionTop = calcPositionTop();
				positionBottom = calcPositionBottom();

				// if longform template, switch "activeWidget" to "desktop"
				if('mobile' === activeWidget) {

					/*if($('#content').hasClass('template--single-longform') && windowWidth >= 768) {
						activeWidget = 'desktop';
					}*/

				}

				if('mobile' === activeWidget) {
					generateMobileWidgetStyles();
				}

				/*if('mobile' === activeWidget && windowWidth <= 767) {
					$('.copyright').css('padding-bottom', '62px');
				} else {
					$('.copyright').css('padding-bottom', '');
				}*/

			}

			/**
			 * Position Mobile Widget
			 *
			 * Controls the positioning of the social widget for screens that
			 * are <= 991px wide.
			 *
			 * @param int position Current position of the scrollbar
			 */
			function positionMobileWidget(position) {

				if(true === fixed) {

					if(windowWidth >= 960) {

						// bottom boundary logic
						if(false === fixedBottom && position >= bottomBoundary) {

							$topWidget.css({
								'bottom': '-75px',
								'left': -($articleRight.offset().left),
								'position': 'absolute'
							});

							fixedBottom = true;

						} else if(true === fixedBottom && position <= bottomBoundary) {

							$topWidget.css({
								'bottom': '',
								'left': '',
								'position': ''
							});

							fixedBottom = false;

						}

					}

					// top-boundary logic
					if(position <= topBoundary) {

						$topWidget.removeClass('social-widget--fixed');

						fixed = false;

					}

				} else {

					// top-boundary logic
					if(position >= topBoundary) {

						$topWidget.addClass('social-widget--fixed');

						fixed = true;

					}

				}

			}

			/**
			 * Position Desktop Widget
			 *
			 * Controls the positioning of the social widget for screens that
			 * are >= 992px wide.
			 *
			 * @param int position Current position of the scrollbar
			 */
			function positionDesktopWidget(position) {

				if(true === fixed) {

					// bottom boundary logic
					if(false === fixedBottom && position >= bottomBoundary) {

						$leftWidget.css({
							'bottom': 0,
							'position': 'absolute',
							'top': ''
						});

						fixedBottom = true;

					} else if(true === fixedBottom && position <= bottomBoundary) {

						$leftWidget.css({
							'position': 'fixed',
							'top': positionTop
						});

						fixedBottom = false;

					}

					// top-boundary logic
					if(position <= topBoundary && $('body').scrollTop() <= topBoundary) {

						$leftWidget.css({
							'position': 'relative',
							'top': '0'
						});

						fixed = false;

					}

				} else {

					// top-boundary logic
					if(position >= topBoundary) {

						$leftWidget.css({
							'position': 'fixed',
							'top': positionTop
						});

						fixed = true;

					}

				}

			}

			/**
			 * Assert that '#header', '#article-left', and '#article-right' are
			 * present before adding the social widget event listener.
			 */
			if($header.length > 0 && $articleRight.length > 0) {

				// calculate element sizes
				init();

				// bind window events logic
				$(window).scroll(function() {

					if('mobile' === activeWidget) {
						positionMobileWidget($(this).scrollTop());
					} else {
						// positionDesktopWidget($(this).scrollTop());
					}

				}).resize(function() {

					var newWidth = ($(window).width() + calcScrollbarWidth());

					// don't call init unless screen width has changed
					if(0 === windowWidth || newWidth !== windowWidth) {
						init();
					}

				});
			}

		});

	}

});
