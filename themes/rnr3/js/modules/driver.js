// Activate fancyBox for driver
$( function() {
    // if driver is present
    if ( $( '#driver' ).length > 0 ) {
        rnr3_driver_bg_color = hex2rgb( rnr3_driver_bg_color );

        /*$( "#fancybox-driver" ).fancybox( {
            padding:  0,
            //autoSize: true,
            width: "100%",
            autoSize: false,
            scrolling: 'hidden',
            wrapCSS:  'fancybox-driver-wrap',
            helpers:  {
                overlay: {
                    locked: true,
                    css: {
                        'background': rnr3_driver_bg_color,
                    }
                }
            },
            afterLoad: function( current, previous ) {
                if( rnr3_cookie_name != '' ) {
                    var days = 5;
                    var d = new Date();
                    d.setTime( d.getTime() + ( days*24*60*60*1000 ) );
                    document.cookie = rnr3_cookie_name + '=1; expires=' + d.toUTCString();
                }
            }
        } );*/
        $( "#fancybox-driver" ).fancybox( {
            buttons: false,
            baseTpl: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
                '<div class="fancybox-bg" style="background-color: '+ rnr3_driver_bg_color +'"></div>' +
                '<div class="fancybox-controls">' +
                    '<div class="fancybox-infobar">' +
                        '<button data-fancybox-previous class="fancybox-button fancybox-button--left" title="Previous"></button>' +
                        '<div class="fancybox-infobar__body">' +
                            '<span class="js-fancybox-index"></span>&nbsp;/&nbsp;<span class="js-fancybox-count"></span>' +
                        '</div>' +
                        '<button data-fancybox-next class="fancybox-button fancybox-button--right" title="Next"></button>' +
                    '</div>' +
                    '<div class="fancybox-buttons">' +
                        '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="Close (Esc)"></button>' +
                    '</div>' +
                '</div>' +
                '<div class="fancybox-slider-wrap fancybox-driver-wrap">' +
                    '<div class="fancybox-slider"></div>' +
                '</div>' +
                '<div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div>' +
            '</div>',
            closeTpl: '',
            afterLoad: function( current, previous ) {
                if ( rnr3_cookie_name != '' ) {
                    var days = 5;
                    var d = new Date();
                    d.setTime( d.getTime() + ( days*24*60*60*1000 ) );
                    document.cookie = rnr3_cookie_name + '=1; expires=' + d.toUTCString();
                }
            }
        } );

        // use on load or overlay doesnt work for some reason
        $( window ).on( 'load', function() {
            $( "#fancybox-driver" ).trigger( 'click' );

            var instance = $.fancybox.getInstance();

            $( '.fancybox-driver-wrap' ).on( 'click', function( e ) {
                e.preventDefault();
            } );

            $( '#driver a, .enter-site' ).on( 'click', function( e ) {

                instance.close();

                var target = $(this).attr( 'href' );

                if ( target != '#' ) {
                    e.stopPropagation();

                    if ( /^#/.test(target) === true ) {
                        e.preventDefault();

                        $('body, html').animate({
                          scrollTop: $( target ).offset().top
                        }, 600);

                    }
                }

            } );
        } );

    }

} );

// credit where credit is due:
// modified from  https://css-tricks.com/snippets/php/convert-hex-to-rgb/
function hex2rgb( color ) {
    var r,g,b;
    color = color.charAt( 0 ) == "#" ? color.substring( 1,7 ) : color;

    r = parseInt( color.substring( 0,2 ), 16 );
    g = parseInt( color.substring( 2,4 ), 16 );
    b = parseInt( color.substring( 4,6 ), 16 );

    return "rgba( " + r + "," + g + "," + b + ", 0.9 )";
}
