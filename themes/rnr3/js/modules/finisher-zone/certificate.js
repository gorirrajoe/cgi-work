$(function(){

	var base_url    = window.location.origin;

    //handle cert actions
    $('form[name="FinCert"] a').on('click',function(e){
        e.preventDefault();
        var $form   = $(this).parents('form');
        var $data   = $form.serialize();
        var $url    = $form.attr('action');
        var $button = $form.find('.cta');
        var $loader = $('<img />', { src: base_url + '/wp-admin/images/loading.gif' }).addClass('loader');
        var $error  = $('<span />').text('Something seems to have gone wrong with your request. Please try again.');

        $.ajax({
            type: 'POST',
            url:  $url,
            data: $data,
            beforeSend: function(){
                $button.addClass('hidden');
                $form.find('p').append($loader);
            },
            error: function( jqXHR, textStatus, errorThrown ){
                $button.removeClass('hidden');
                //$form.find('p').append($error);
            },
            success: function(dataresp){
                $button.removeClass('hidden');
                $form.find('.loader').remove();
            }
        });

        $form.submit();
    });

    //alternate certificate checkboxes
    $('input[name="finisher-all"]').on('click',function(e){
        if(this.checked){
            $('.fz-cert-checkbox input').each(function(){
                this.checked = true;
            });
        }else{
            $('.fz-cert-checkbox input').each(function(){
                this.checked = false;
            });
        }
    });

    $('.fz-cert-checkbox input').on('click', function(e){
        if($('.fz-cert-checkbox input:checked').length == $('.fz-cert-checkbox input').length){
                $('input[name="finisher-all"]').prop('checked',true);
        }else{
                $('input[name="finisher-all"]').prop('checked',false);
        }
    });

});
