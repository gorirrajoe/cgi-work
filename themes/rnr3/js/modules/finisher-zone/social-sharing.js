$(function(){

	var base_url    = window.location.origin;

    //handle social sharing options
    $('.post-widget').each(function(){
        var share_url   = $(this).data('fz-url'),
                this_title  = $(this).data('fz-title'),
                this_image  = $(this).data('pin-image'),
                this_id     = '#' + $(this).attr('id');

        post_widget(this_id, {
            buttons:        ['pinterest', 'twitter'],
            url:            share_url,
            title:          this_title,
            image:          this_image,
            sharePopups:    true,
            afterShare:     false,
            counter:        false,
            services:       {
                twitter:        {
                    via: 'RunRocknRoll'
                }
            }
        })
    });

    //facebook share code
    $.ajaxSetup({ cache: true });
    $.getScript('//connect.facebook.net/en_US/sdk.js', function(){
        FB.init({
            appId:    '882391795212412',
            version:  'v2.5',
            oauth:    true,
            status:   true,
            xfbml:    true
        });
    });

    var pageEvent;

    if( $('#finisher-info').length > 0 ){
        pageEvent = $('#finisher-info').find('.fz_event_hdr > h3').text();

        $('.fb-share-badge, .fb-share-persona').each(function(){
            var $this       = $(this),
                $image      = $this.prevAll('.hidden').find('img').attr('src'),
                $badgeName  = $this.parent().prevAll('h3').text(),
                $url        = $this.attr('href');

            $this.on('click',function(e){
                e.preventDefault();

                window.open(
                    $url,
                    '_blank',
                    'width=700,height=660,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');
            });

        });
    }

    $('.fb-share-bttn').on('click',function(e){
        var $this     = $(this),
            $image    = $this.parents('#fz_fb_share').find('#fb-share-image').attr('src'),
            $message  = $this.parents('#fz_fb_share').find('#fb-share-message').val();

        postToWall($message, $image);
    });

    function postToWall($message, $image){
        FB.login(function(response){
            if( response.authResponse ){
                var fb_token = FB.getAuthResponse()['accessToken'];
                var $loader  = $('<img />', { src: base_url + '/wp-admin/images/loading.gif' })

                $.ajax({
                    type: 'POST',
                    url:  'https://graph.facebook.com/me/photos',
                    data: {
                        message:      $message,
                        url:          $image,
                        access_token: fb_token,
                        format:       'json',
                        scope:        'publish_actions'
                    },
                    beforeSend: function(){
                        $('.cta.fb-share-bttn').addClass('hidden');
                        $('#result').html($loader);
                    },
                    error: function( jqXHR, textStatus, errorThrown ){
                        $('.cta.fb-share-bttn').removeClass('hidden');
                        $('#result').html('Something seems to have gone wrong with your request. Please try again.');
                    },
                    success: function(dataresp){
                        var $resultMessage =
                            '<a href=\"https://www.facebook.com/photo.php?fbid=' + dataresp.id + '\">' +
                            'Thank you for sharing on your timeline! Click here to View it </a>' +
                            'or Click <a href="#" onclick="javascript:window.close();">here to close</a> this window!';

                        $('#result').html($resultMessage);
                    }
                });
            }else{
                $('#result').html('Something seems to have gone wrong with your request. Please try again.');
            }
        }, {scope: 'publish_actions'})
    }

});
