$(function(){

    var container   = document.querySelector( '#finisher-zone-archive' ),
        $sortSelect = $( '#sortselect' ),
        $yearSelect = $( '#select-year' ),
        $citySelect = $( '#select-city' );

    if ( container !== null ) {

        // Start up our mixitup
        var mixer = mixitup( container,{
            load: {
                sort: 'date:desc'
            },
            callbacks: {
                // Set up lazy loading at the end of each mixitup interaction
                onMixEnd: function( state ) {

                    // have to hook the images to lazyload again
                    $('img.lazy').lazyload();

                    // bail if it was the "all" button that was triggered (will load all the images)
                    if ( state.activeFilter.selector === '.mix' ) {
                        return;
                    }

                    var matches = state.matching;

                    for ( var i = 0; i < matches.length; i++ ) {
                        var $element    = $( matches[i] );
                        var $lazyImage  = $element.find('img.lazy');

                        $lazyImage.trigger('appear');
                    }

                }
            }
        } );

        // Set up the sort drop down
        $sortSelect.on( 'change', function() {
            mixer.sort( this.value );
        });

        // Set up our filter select drop downs
        $('#select-year, #select-city').on( 'change', function(){

            // Filter needs to be a combination of the select drop downs
            var yearValue       = $yearSelect.val();
            var cityValue       = $citySelect.val();
            var filterString    = yearValue + cityValue

            filterString    = filterString == '' ? 'all' : filterString;

            mixer.filter( filterString);

        });

        // Reset the select drop downs when the "all" button is clicked
        $('.all-filter').on( 'click', function() {

            $('.results-filter').each(function(){
               $(this).prop('selectedIndex', 0);
            });

        });

        // Make sure our images are lazy loaded
        $('img.lazy').lazyload();

    }


});
