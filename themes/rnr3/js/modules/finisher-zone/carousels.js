$(function(){

    if ( $(window).width() < 420 ){
        owldata = $('.fz_badgelist ul, .fz_whatsnext, .fz_offers,#personas .grid_3_special, #badges .grid_4_special').owlCarousel({
            autoPlay:       true,
            navigation:     true,
            navigationText:
                [
                    "<span class='icon-left-open'></span>",
                    "<span class='icon-right-open'></span>"
                ],
            pagination:     true,
            lazyLoad:       true,
            itemsScaleUp:   true,
            items:          1,
            itemsCustom:    [[0,1]],
            afterAction:    function(){
                var $prev     = $(this.buttonPrev);
                var $next     = $(this.buttonNext);

                $prev.removeClass('disabled');
                $next.removeClass('disabled');

                if( this.currentItem == 0 ){
                    $prev.addClass('disabled');
                }
                if( this.itemsAmount-1 == this.currentItem ){
                    $next.addClass('disabled');
                }
            }
        });
    }

});
