/**
 * Logic for displaying the "sticky" sidebar on the site.
 */
jQuery(document).ready(function($) {
	var $stickyHeader		= $('#alert'),
		$adminBar			= $('#wpadminbar'),
		$content			= $('.column:last-child'),
		$stickyBarColumn	= $('.stickem'),
		$stickyBar			= $('.sticky-nav'),
		$firstSection		= $('#finisher-info'),
		$wrapper			= $('.offset240left'),
		topBoundry			= 0,
		bottomBoundry		= 0,
		paddingLeft			= 0,
		positionTop			= 0,
		positionBottom		= 0,
		widgetWidth			= 0,
		windowWidth			= 0,
		adminBarSticky		= false,
		activeWidget		= '',
		fixed				= false,
		mobileFixed			= false,
		mobileFixedTop		= false,
		fixedBottom			= false;

	/**
	 * Init
	 *
	 * Constructor method which calls all the utility methods used to
	 * calculate the relevant element dimensions.
	 */
	function init() {

		windowWidth = ( $(window).width() + calcScrollbarWidth() );

		if( windowWidth >= 601 && $adminBar.length ) {

			adminBarSticky = true;

		}

		if( windowWidth < 960 ) {

			$stickyBarColumn.css( 'height', 'auto' );
			topBoundry			= calcTopBoundry( 'mobile' );
			positionTop			= calcPositionTop( 'mobile' );

		} else {

			$stickyBarColumn.css( 'height', $content.height() );
			topBoundry			= calcTopBoundry();
			bottomBoundry		= calcBottomBoundry();
			positionTop			= calcPositionTop();

		}

	}


	/**
	 * Calculate Top Boundry
	 *
	 * "topBoundry" defines the point at which the social widget should
	 * become "fixed" when scrolling down, or "relative" when scrolling up.
	 *
	 * Note: This is the highest we want the widget to go on the page.
	 *
	 * @return int Position of the "top" scroll boundry.
	 */
	function calcTopBoundry( position ) {

		if( position == 'mobile' ) {

			return $wrapper.offset().top - $adminBar.outerHeight();

		} else {

			if( $( 'main' ).hasClass( 'finisher_zone_search_results' ) ) {

				return $wrapper.offset().top - $stickyHeader.height() - $adminBar.outerHeight() + 45;

			} else {

				return $wrapper.offset().top - $stickyHeader.height() - $adminBar.outerHeight() - 15;

			}


		}

	}


	/**
	 * Calculate Bottom Boundry
	 *
	 * "bottomBoundry" defines the point at which the social widget
	 * should become "absolute" when scrolling down, or "fixed" when
	 * scrolling up.
	 *
	 * Note: This is the lowest we want the widget to go on the page.
	 *
	 * @return int Position of the "bottom" scroll boundry.
	 */
	function calcBottomBoundry( position ) {

		if( position != 'mobile' ) {

			if( adminBarSticky == true ) {

				return $content.outerHeight() + $content.offset().top - ( $stickyHeader.height() + $adminBar.height() + $stickyBar.outerHeight() + 55 );

			} else {

				return $content.outerHeight() + $content.offset().top - ( $stickyHeader.height() + $stickyBar.outerHeight() + 55 );

			}

		}

	}


	/**
	 * Calculate Position Top
	 *
	 * "positionTop" defines the y-coordinate at which the social widget
	 * should remained fixed at while the widget is "fixed".
	 *
	 * @return int Position of the widget while "fixed".
	 */
	function calcPositionTop( position ) {
		if( position == 'mobile' ) {

			return $stickyHeader.outerHeight() + $adminBar.height();

		} else {

			return ($stickyHeader.outerHeight() + $adminBar.height() + 15 );

		}
	}


	/**
	 * Position Desktop Widget
	 *
	 * Controls the positioning of the social widget for screens that
	 * are >= 992px wide.
	 *
	 * @param int position Current position of the scrollbar
	 */
	function positionDesktopWidget(position) {
		// $stickyBar.removeClass('stickit');
		windowWidth = ( $(window).width() + calcScrollbarWidth() );

		if( windowWidth >= 960 ) {

			$stickyBar.removeClass('sticky-nav__stickit-mobile');
			$firstSection.css({
				'padding-top': '0'
			});
			mobileFixed = false;

			if(true === fixed) {

				// bottom boundry logic
				if(false === fixedBottom && position >= bottomBoundry) {

					$stickyBar.addClass('sticky-nav__stickit-desktop-bottom');
					$stickyBar.css({
						'top': 'auto'
					});

					fixedBottom = true;

				} else if(true === fixedBottom && position <= bottomBoundry) {

					$stickyBar.css({
						'top': positionTop,
					});
					$stickyBar.removeClass('sticky-nav__stickit-desktop-bottom');
					fixedBottom = false;

				}

				// top-boundry logic
				if( position <= topBoundry ) {

					$stickyBar.removeClass('sticky-nav__stickit-desktop');

					// $stickyBar.css({
					// 	'position': 'relative',
					// 	'top': '0'
					// });

					fixed = false;

				}

			} else {

				// top-boundry logic
				if( position >= topBoundry ) {

					$stickyBar.addClass('sticky-nav__stickit-desktop');

					$stickyBar.css({
						'top': positionTop
					});

					fixed = true;

				}

			}

		} else {

			$stickyBar.removeClass('sticky-nav__stickit-desktop');
			fixed = false;

			/*if( true === mobileFixed ) {

				if( true === mobileFixedTop && position <= topBoundry ) {

					mobileFixedTop = false;

				}

				if( position <= topBoundry ) {

					$stickyBar.removeClass('sticky-nav__stickit-mobile');

					$stickyBar.css({
						'top': '0'
					});
					$firstSection.css({
						'padding-top': '0'
					});

					mobileFixed = false;

				}

			} else {

				if( position >= topBoundry ) {

					$stickyBar.addClass('sticky-nav__stickit-mobile');

					$stickyBar.css({
						'top': positionTop
					});
					$firstSection.css({
						'padding-top': '177px'
					});

					mobileFixed = true;

				}

			}*/

		}

	}

	/**
	 * Note: wait until "load" event so element heights are set.
	 */
	$(window).on('load', function() {

		if( $stickyBarColumn.length ) {

			init();

			// bind window events logic
			$(window).scroll(function() {

				positionDesktopWidget( $(this).scrollTop() );


			}).resize(function() {

				var newWidth = ( $( window ).width() + calcScrollbarWidth() );

				// don't call init unless screen width has changed
				if( 0 === windowWidth || newWidth !== windowWidth ) {

					if( windowWidth < 960 && newWidth >= 960 ) {

						$stickyBar.removeClass('sticky-nav__stickit-mobile');

					} else if( windowWidth >= 960 && newWidth < 960 ) {

						$stickyBar.removeClass('sticky-nav__stickit-desktop');

					}

					init();

				}

			});

		}

	});

});
