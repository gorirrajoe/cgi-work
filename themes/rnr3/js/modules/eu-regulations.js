$(function(){
    var $privacyBar = $('#cookie-popup-bar');
    var objectName  = 'rnr_eu_regulation';

    if ( $privacyBar.length > 0 ) {

        $('.close-cookie-bar').on('click', function(e){
            e.preventDefault();

            var d       = new Date();
            var days    = 180;
            var time    = d.getTime() + ( days * 24 * 60 * 60 * 1000 );

            // if browser supports localStorage use it (IE8<)
            if ( typeof( Storage ) !== 'undefined' && !localStorage.getItem( objectName ) ) {

                // iOS 8.3+ Safari Private Browsing mode throws a quota exceeded JS error with localStorage.setItem
                try {
                    var regulationObject   = { timestamp: time };
                    localStorage.setItem( objectName, JSON.stringify( regulationObject ) );
                } catch ( error ) {
                    // Do nothing with localStore if iOS 8.3+ Safari Private Browsing mode, we are still using the cookie.
                }

            }

            d.setTime( time );
            document.cookie = objectName + '=1; expires=' + d.toUTCString();

            $privacyBar.slideUp(400, function(){
                $(this).remove();
            });
        });

    } else {

        // Check localStorage for our data
        if ( typeof( Storage ) !== 'undefined' && localStorage.getItem( objectName ) ) {

            // Delete if the date is too old (we want a friendly reminder every 180 days)
            var regulationObject    = JSON.parse( localStorage.getItem( objectName ) ),
                dateTime            = regulationObject.timestamp,
                now                 = new Date().getTime().toString();

            if ( now > dateTime) {
                localStorage.removeItem( objectName );
            }

        }

    }

});
