$(function(){

	var $tempo_fancybox_links	= $('.the-tempo-blog .fancybox'),
		$cat_nav				= $('#menu-sidebar-cat-nav, #menu-header-cat-nav');


	/**
	 * General Modal on the Tempo
	 */
	if ( $tempo_fancybox_links.length > 0 ) {

		/*$tempo_fancybox_links.each(function(){
			$( this ).fancybox({
				openEffect: 'none',
				closeEffect: 'none',
				helpers: {
					overlay: {
						locked: false,
						css: {
							'background' : 'rgba(0, 0, 0, .8)'
						}
					}
				}
			});
		});*/
		$tempo_fancybox_links.each(function(){
			$( this ).fancybox({
				fullScreen: false,
				iframe: {
					scrolling: 'yes'
				}
			});
		});

	}

	/**
	 * Sidebar Category Nav
	 */
	if ( $cat_nav.length ) {
		var $sub_nav_items	= $cat_nav.find('.menu-item-has-children');

		$sub_nav_items.each( function(i){
			var $this	= $(this);

			$this.find('> a').on('click', function(e) {
				e.preventDefault();

				$(this).toggleClass('open');
				$(this).next('.sub-menu').slideToggle();

				return false;
			});
		});
	}

	/**
	 * Legacy Runners Page - Tempo SD
	 */
	$('.legacy_runner__link').fancybox({
		caption : function( instance, item ) {
			return $(this).next('.legacy_runner__caption').html();
		}
	});

});
