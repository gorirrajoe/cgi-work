$(function(){

	var $hamburger			= $('.hamburger'),
		$search_nav			= $('header .search-nav'),
		$search_nav_toggle	= $('header .search-nav__toggle'),
		$header				= $('header'),
		$subscribe			= $('header .subscribe'),
		$subscribe_toggle	= $('header .subscribe__toggle'),
		$global_menu_toggle	= $('.global_menu__anchor');

	/**
	 * Header scroll effect
	 */
	$(window).scroll(function(){

		// Add class to header
		if ( $(this).scrollTop() > 50 ) {
			$header.addClass('shrink');
		} else {
			$header.removeClass('shrink');
		}

		// close all menus if we scroll
		$( '.hamburger, .menulabel' ).removeClass( 'open' );
		$( '#global-rnr-menu' ).removeClass( 'menuOpen' );
		$( 'header' ).removeClass( 'menuIsOpen' );
		$subscribe.removeClass('active');
		$search_nav.removeClass('active');

	});

	/**
	 * Main navigation for the hamburger
	 * We're doing some different stuff here in the tempo
	 * so need to unbind the original click function stuff
	 */
	$hamburger
		.unbind('click')
		.on( 'click', function(e) {
			e.preventDefault();

			$hamburger.toggleClass( 'open' );
			$header.toggleClass( 'menuIsOpen' );
			$search_nav.removeClass('active');
			$subscribe.removeClass('active');

			// reset a few things if the menu is being closed
			if ( !$header.hasClass('menuIsOpen') ) {

				$('#global-rnr-menu').removeClass('menuOpen');

				var $sub_nav_items	= $('#menu-header-cat-nav').find('.menu-item-has-children > a');

				$sub_nav_items.each( function(i){
					var $this	= $(this);
					$this.removeClass('open');
					$this.next('.sub-menu').slideUp();
				});

			}

			return false;
		} );

	$global_menu_toggle
		.unbind('click')
		.on('click', function(e) {
			e.preventDefault();

			var $this	= $(this);

			$this.toggleClass('open');

			$('#global-rnr-menu').toggleClass( 'menuOpen' );

			return false;
		});

	/**
	 * Subscribe link in header
	 */
	if ( $subscribe_toggle ) {

		$subscribe_toggle.on( 'click', function(e) {
			e.preventDefault();
			e.stopPropagation();

			$subscribe.toggleClass('active');

			// close other menu items
			$( '.hamburger, .menulabel' ).removeClass( 'open' );
			$( '#global-rnr-menu' ).removeClass( 'menuOpen' );
			$( 'header' ).removeClass( 'menuIsOpen' );
			$search_nav.removeClass('active');

			return false;
		});

	}

	/**
	 * Search hover/active states
	 */
	if ( $search_nav_toggle ) {

		$search_nav_toggle.on( 'click', function(e) {
			e.preventDefault();
			e.stopPropagation();

			$search_nav.toggleClass('active');

			// close other menu items
			$( '.hamburger, .menulabel' ).removeClass( 'open' );
			$( '#global-rnr-menu' ).removeClass( 'menuOpen' );
			$( 'header' ).removeClass( 'menuIsOpen' );
			$subscribe.removeClass('active');

			if ( $search_nav.hasClass('active') ) {
				$search_nav.find('input').focus();
			}

			return false;
		} );

		// close if clicking elsewhere
		/*$(document).on( 'click', function(e) {
			if ( $(e.target).is('.search-nav__wrap') === false ) {
				$('.search-nav').removeClass('active');
			}
		} );*/

	}

});
