$(function(){

	/**
	 * hide empty error message when user clicks email field
	 */
	$( '.subscribe-sg__field' ).on( 'focus', function() {
		$( '.subscribe-sg__msg' ).hide();
	} );

	var $form	= $('.subscribe-sg__datacap');

	/**
	 * handle form submission
	 */
	$form.on( 'submit', function(e) {
		e.preventDefault();

		var $this		= $(this),
			$intro		= $this.find('.subscribe-sg__intro'),
			$email		= $this.find('input[name="subscribe-sg__email"]').val(),
			$firstname	= $this.find('input[name="subscribe-sg__firstname"]').val(),
			$lastname	= $this.find('input[name="subscribe-sg__lastname"]').val(),
			$success	= $this.find('.subscribe-sg__msg--success'),
			$empty		= $this.find('.subscribe-sg__msg--empty'),
			$fail		= $this.find('.subscribe-sg__msg--failure'),
			$inputs		= $this.find('input'),
			$button		= $this.find('button'),
			validate	= true;

		// check if any of the inputs are empty
		$inputs.each(function(i){

			if ( $(this).val() == '' ) {
				$empty.show();

				validate	= false;

				return false;
			}

		});

		if ( validate ) {

			var email_regex	= /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
			var is_email	= email_regex.test( $email );

			if ( is_email ) {

				$.ajax({
					url: window.location.href + '/wp-content/themes/rnr3-tempo/inc/sg-proxy-newsletter.php',
					type: 'POST',
					data: {
						email: $email,
						firstname: $firstname,
						lastname: $lastname,
					},
					dataType: 'json',
					success: function( data ) {
						$intro.hide();
						$inputs.hide();
						$button.hide();
						$success.show();
					},
					error: function( jqXHR, textStatus, errorThrown ) {
						var error = JSON.stringify( jqXHR );
						console.log( 'error: ', error );
						$fail.show();
					},
				});

			} else {
				$fail.show();
			}

		}

		return false;
	} );



});
