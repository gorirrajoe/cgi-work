jQuery(document).ready(function($){

    $(window).on('load', function() {
        $('.event-entries').cgiDrawer({
            itemsClass: 'event',
            drawerTrigger: 'get-details',
            triggerText: true,
            actionName: 'expo_post_data',
            ajaxURL: expo_post_data.ajax_url,
            scrollTo: 'item',
            onDrawerShow: function() {
                startComponents();
            }
        });

    });

    $('.featured-event').on('click', '.get-details', function(e) {
        e.preventDefault();

        var post_id = $(this).data('post-id');
        $('#findrace')
            .find('[data-post-id="' + post_id + '"]')
            .find('.get-details')
            .trigger('click');
    });

    //set up fancybox for information forms and auto close
    var check_complete;

    $('.fancybox.register2').fancybox({

        fullScreen: false,
        iframe: {
            css: {
                width: '70%',
                height: '90%',
            },
        },
        afterLoad: function() {
            check_complete = setInterval(check_close, 500);
        }

    });

    function check_close() {
        // Create IE + others compatible event handler
        var eventMethod = window.addEventListener ? 'addEventListener' : 'attachEvent';
        var eventer = window[eventMethod];
        var messageEvent = eventMethod == 'attachEvent' ? 'onmessage' : 'message';

        // Listen to message from child window
        eventer( messageEvent, function(e) {
            if ( e.data == 'form complete' ) {
                clearInterval(check_complete);

                var close_fancybox = setInterval( function() {
                    clearInterval(close_fancybox);

                    $.fancybox.close();
                }, 3000 ) ;
            }
        }, false );
    }

});


function startComponents(){
    // gallery carousel
    $('.gallery').owlCarousel({
        singleItem: true,
        autoPlay: true,
        slideSpeed: 300,
        pagination: true,
    });

    // initiate tabs
    $('#info-tabs').tabs();

    // Countdown timer
    if ( $('.countdown').length ) {
        $('.countdown').each( function() {
            var $countdown      = $(this);
            var raceDatetime    = new Date( $countdown.data('countdown') * 1000 + 25200000 );

            setInterval(function () {
                var timespan = countdown( null, raceDatetime, -2018 ); // -2018 is days, hrs, mins, secs for this plugin
                $countdown.find('.countdown-days').text(timespan.days < 10 ? '0' + timespan.days : timespan.days);
                $countdown.find('.countdown-hrs').text(timespan.hours < 10 ? '0' + timespan.hours : timespan.hours);
                $countdown.find('.countdown-mins').text(timespan.minutes < 10 ? '0' + timespan.minutes : timespan.minutes);
                $countdown.find('.countdown-secs').text(timespan.seconds < 10 ? '0' + timespan.seconds : timespan.seconds);
            }, 1000 );
        });
    }

    // reinitiate fancybox on elements since AJAX loaded elements don't seem to be supported in fancybox 3
    var check_complete;
    $('.drawer .fancybox').fancybox({

        fullScreen: false,
        iframe: {
            css: {
                width: '70%',
                height: '90%',
            },
        },
        afterLoad: function() {
            check_complete = setInterval(check_close, 500);
        }

    });

    function check_close() {
        // Create IE + others compatible event handler
        var eventMethod = window.addEventListener ? 'addEventListener' : 'attachEvent';
        var eventer = window[eventMethod];
        var messageEvent = eventMethod == 'attachEvent' ? 'onmessage' : 'message';

        // Listen to message from child window
        eventer( messageEvent, function(e) {
            if ( e.data == 'form complete' ) {
                clearInterval(check_complete);

                var close_fancybox = setInterval( function() {
                    clearInterval(close_fancybox);

                    $.fancybox.close();
                }, 3000 ) ;
            }
        }, false );
    }

}

//our little custom jQuery plugin
(function($, window, document){
    $.fn.cgiDrawer = function( options ) {
        // replace the defaults with options selected
        var settings = $.extend({}, $.fn.cgiDrawer.defaults, options );

        // in case theres more than one grid (WHY WE WOULD DO THIS?! no idea)
        return this.each( function() {
            var $this           = $(this),
                    drawerVisible   = false,
                    triggerText     = '',
                    theDrawer;

            // do onDrawerSetup callback before anything
            settings.onGridLoad( $this );

            // set up a few functions that we will need
            function beforeOpen( gridItem ) {

                // do onDrawerSetup callback
                settings.onDrawerSetup( $this );

                /* checking which trigger we are using  */
                if ( !gridItem.hasClass('current-gridItem') ) {
                    $this.find('.current-gridItem').removeClass('current-gridItem');
                    if ( settings.triggerText ) {
                        var grid_item_trigger = gridItem.find('.get-details');
                        triggerText = grid_item_trigger.text();
                        grid_item_trigger.text('Loading...');
                    }

                } else {
                    // else if trigger is same then drawer is opened, close it
                    closeDrawer( $this );
                    return;
                }

                // remove any previously existing drawer
                if ( !drawerVisible ) {
                    $this.find('.drawer').remove();
                    // create drawer
                    var drawerhtmlstart  = $("<div class=\"drawer loading\"></div>");
                    theDrawer = drawerhtmlstart.appendTo(gridItem);
                }

                // get proper data to send AJAX request
                var post_id     = gridItem.data('post-id'),
                    ajax_data   = {
                        'action':   settings.actionName,
                        'post_id':  post_id
                    };

                // make request
                $.ajax({
                    url: settings.ajaxURL,
                    type: 'POST',
                    data: ajax_data,

                    success: function( response ) {
                        var thecontent = response;
                        setupDrawer( thecontent, gridItem );
                        if( settings.triggerText ){
                            gridItem.find('.get-details').text(triggerText);
                        }

                    },
                    error: function( xhr, ajaxOptions, thrownError ){
                        console.log( xhr );
                    }
                });

            }

            function closeDrawer() {
                // SCROLL TO CORRECT POSITION FIRST
                if ( settings.scroll ) {
                    $('html, body').animate({
                        scrollTop: $this.find('.current-gridItem').offset().top - settings.scrollOffset
                    }, {
                        duration: 200,
                        easing: settings.animationEasing
                    });
                }

                // REMOVES EXPAND AREA
                drawerVisible = false;
                $this.find('.current-gridItem').removeClass('current-gridItem');

                $this.find('.drawer').slideUp(settings.animationSpeed, function() {
                    $this.find('.drawer').remove();
                    settings.onDrawerClosed($this);
                });

            }

            function setupDrawer( thecontent, gridItem ) {

                var drawerhtml = '<div class="drawer__inner">';

                if(settings.showNav){
                    drawerhtml += '<div class="drawer__close"></div>';
                    drawerhtml += '<div class="drawer__previous"></div>';
                    drawerhtml += '<div class="drawer__next"></div>';
                }
                drawerhtml += thecontent;
                drawerhtml += '</div>';

                resizeDrawer();

                if ( !drawerVisible ) {
                    theDrawer
                        .html(drawerhtml);

                    positionDrawer();
                    settings.onDrawerShow(theDrawer);

                    theDrawer
                        .slideDown(settings.animationSpeed, function(){
                            gridItem.addClass('current-gridItem');
                        });

                    drawerVisible = true;
                } else {
                    theDrawer
                        .html(drawerhtml)
                        .appendTo(gridItem);

                    positionDrawer();
                    settings.onDrawerShow(theDrawer);

                    gridItem.addClass('current-gridItem');
                    drawerVisible = true;
                }

                /* SCROLL TO CORRECT POSITION AFTER */
                if ( settings.scroll ) {
                    var offset = (settings.scrollTo == 'panel' ? gridItem.offset().top + gridItem.height() - settings.scrollOffset : gridItem.offset().top - settings.scrollOffset);
                    $('html, body').animate({
                        scrollTop: offset
                    }, {
                        duration: settings.animationSpeed
                    });
                }

                $this.find('.drawer').removeClass('loading');

            }

            function calculateRowsColumns() {
                var rowNumber   = 1;
                var colNumber   = 1;

                $('.event').each( function() {
                    var $this = $(this),
                            $previous = $this.prev();

                    if ( $previous.length > 0 ){
                        colNumber++;
                        if ( $this.position().top != $previous.position().top ){
                            rowNumber++;
                            colNumber = 1;
                        }
                    }

                    $this.attr('data-row', rowNumber);
                    $this.attr('data-column', colNumber);
                });

            }

            function positionDrawer() {
                var $eventBox   = theDrawer.parents('.'+settings.itemsClass);
                var left        = $eventBox.offset().left;

                theDrawer.css('margin-left', -left+'px');

            }

            //from: http://www.alexandre-gomes.com/?p=115
            function getScrollBarWidth() {
                var inner = document.createElement('p');
                inner.style.width = "100%";
                inner.style.height = "200px";

                var outer = document.createElement('div');
                outer.style.position    = "absolute";
                outer.style.top         = "0px";
                outer.style.left        = "0px";
                outer.style.visibility  = "hidden";
                outer.style.width       = "200px";
                outer.style.height      = "150px";
                outer.style.overflow    = "hidden";
                outer.appendChild(inner);

                document.body.appendChild(outer);
                var w1 = inner.offsetWidth;
                outer.style.overflow = 'scroll';
                var w2 = inner.offsetWidth;
                if (w1 == w2) w2 = outer.clientWidth;

                document.body.removeChild (outer);

                return (w1 - w2);

            }

            function resizeDrawer() {
                var width = window.innerWidth - getScrollBarWidth();
                if ( theDrawer ){
                    theDrawer.outerWidth(width);
                }

            }

            // run functions on window resize
            $(window).on('resize', function() {
                resizeDrawer();
                calculateRowsColumns();
            });

            // click event for grid items (to open drawer)
            $this.find('.'+settings.drawerTrigger).on( 'click', function(e) {
                var $this_trigger = $(this);
                e.preventDefault();
                var gridItem = $this_trigger.parents('.'+settings.itemsClass);
                beforeOpen(gridItem);
            });

            // click event for closing drawer
            $this.on('click', '.drawer__close', function(e) {
                e.preventDefault();
                closeDrawer($this);
            });

            // click event for next button
            $this.on('click', '.drawer__next', function(e) {
                e.preventDefault();
                $(this).parents('.current-gridItem').next().find('.'+settings.drawerTrigger).trigger('click');
            });

            // click event for previous button
            $this.on('click', '.drawer__previous', function(e) {
                e.preventDefault();
                $(this).parents('.current-gridItem').prev().find('.'+settings.drawerTrigger).trigger('click');
            });

        });
    }

    // lets set up some defaults
    $.fn.cgiDrawer.defaults = {
        itemsClass: 'cgiGrid-item',
        drawerTrigger: 'drawer__open',
        triggerText: false,
        actionName: 'cgi_get_data',
        ajaxURL:  'cgi_get_data.ajax_url',
        scrollTo: 'panel',
        animationSpeed:   600,
        animationEasing: 'swing',
        scroll: true,
        scrollOffset: 30,
        showNav: true,
        calculateGrid: true,
        onGridLoad: function(){},
        onDrawerSetup: function(){},
        onDrawerShow: function(){},
        onDrawerClosed: function(){},
    };
})(jQuery, window, document);
