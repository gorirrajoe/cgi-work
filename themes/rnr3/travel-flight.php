<?php
	/* Template Name: Travel - Flight */

	get_header();

	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';

	$prefix = '_rnr3_';

	$travel_subpage_array = array(
		'fly'		=> 'flights',
		'stay'		=> 'hotels',
		'pack'		=> 'pack',
		'transport'	=> 'transport'
	);

	// $post_meta = get_meta( get_the_ID() );

	$travel_flight_group	= get_post_meta( get_the_ID(), $prefix . 'travel-flight', 1 );
	$travel_flight_group	= array_filter( $travel_flight_group );

	$travel_page_id	= get_id_by_slug( 'the-weekend/travel' );
?>

<!-- main content -->
<main role="main" id="main" class="travel">
	<div id="nav-anchor"></div>

	<section class="wrapper grid_2 offset240left">
		<div class="column sidenav stickem">
			<nav class="sticky-nav">
				<ul>
					<?php foreach( $travel_flight_group as $key => $value ) {

						if( $value['content'] != '' ) {
							echo '<li><a href="#before'. $key .'">'. $value['title'] .'</a></li>';
						}

					} ?>
				</ul>
			</nav>
		</div>


		<div class="column">
			<div class="content">

				<?php if (have_posts()) : while (have_posts()) : the_post();

					$subpage_icon = ( get_post_meta( $travel_page_id, '_rnr3_fly_img', 1 ) != '' ) ? get_post_meta( $travel_page_id, '_rnr3_fly_img', 1 ) : '';
					$subpage_sponsor = ( get_post_meta( $travel_page_id, '_rnr3_fly_sponsor', 1 ) != '' ) ? get_post_meta( $travel_page_id, '_rnr3_fly_sponsor', 1 ) : '';

					echo '<section>

						<h2 class="travel_subpage_title">

							<img class="travel_subpage_icon" src="'. $subpage_icon .'" alt="'. get_the_title() .'">' .
							get_the_title();

							if( $subpage_sponsor != '' ) {
								echo '<span>'. $presented_by_txt .' <img src="'. $subpage_sponsor.'"></span>';
							}

						echo '</h2>';
						the_content();

					echo '</section>';

				endwhile; endif;


				foreach( $travel_flight_group as $key => $value ) {

					if( $value['content'] != '' ) {

						echo '<section id="before'. $key .'">

							<h2>'. $value['title'] .'</h2>'
							. apply_filters( 'the_content', $value['content'] ) .'

						</section>';

					}

				}


				travel_subpage_columns( $travel_page_id, $qt_lang );

				/**
				 * travel sponsors
				 */
				travel_sponsors( $travel_page_id ); ?>

			</div>
		</div>
	</section>




	<script>
		$(document).ready(function(){

			/**
			 * This part causes smooth scrolling using scrollto.js
			 * We target all a tags inside the nav, and apply the scrollto.js to it.
			 */
			$(".sticky-nav a, .backtotop").click(function(evn){
				evn.preventDefault();
				$('html,body').scrollTo(this.hash, this.hash);
			});

			/*$('main').stickem({
				container: '.offset240left',
				offset: 60
			});*/

			/**
			 * This part handles the highlighting functionality.
			 * We use the scroll functionality again, some array creation and
			 * manipulation, class adding and class removing, and conditional testing
			 */
			var aChildren = $(".sticky-nav li").children(); // find the a children of the list items
			var aArray = []; // create the empty aArray
			for (var i=0; i < aChildren.length; i++) {
				var aChild = aChildren[i];
				var ahref = $(aChild).attr('href');
				aArray.push(ahref);
			} // this for loop fills the aArray with attribute href values

			$(window).scroll(function(){
				var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
				var windowHeight = $(window).height(); // get the height of the window
				var docHeight = $(document).height();

				for (var i=0; i < aArray.length; i++) {
					var theID = aArray[i];
					var divPos = $(theID).offset().top; // get the offset of the div from the top of page
					var divHeight = $(theID).height(); // get the height of the div in question
					if (windowPos >= (divPos - 1) && windowPos < (divPos + divHeight -1 )) {
						$("a[href='" + theID + "']").addClass("nav-active");
					} else {
						$("a[href='" + theID + "']").removeClass("nav-active");
					}
				}

				if(windowPos + windowHeight == docHeight) {
					if (!$(".sticky-nav li:last-child a").hasClass("nav-active")) {
						var navActiveCurrent = $(".nav-active").attr("href");
						$("a[href='" + navActiveCurrent + "']").removeClass("nav-active");
						$(".sticky-nav li:last-child a").addClass("nav-active");
					}
				}
			});
		});

	</script>


</main>

<?php get_footer(); ?>
