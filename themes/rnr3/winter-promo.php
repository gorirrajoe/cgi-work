<?php
	/* Template Name: Winter Promo */

	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$prefix			= '_rnr3_';
	$current_date	= date('YmdH');
	$current_phase	= '';


	$phase0_date	= date( 'Ymd', get_post_meta( get_the_ID(), $prefix . 'startdate0', 1 ) ) . '08';
	$phase1_date	= date( 'Ymd', get_post_meta( get_the_ID(), $prefix . 'startdate1', 1 ) ) . '08';
	$phase2_date	= date( 'Ymd', get_post_meta( get_the_ID(), $prefix . 'startdate2', 1 ) ) . '08';
	$phase3_date	= date( 'Ymd', get_post_meta( get_the_ID(), $prefix . 'startdate3', 1 ) ) . '08';

	if( $current_date >= $phase0_date && $current_date < $phase1_date ) {
		$current_phase = 'phase0';
	} elseif( $current_date >= $phase1_date && $current_date < $phase2_date ) {
		$current_phase = 'phase1';
		set_interstitial_cookie_timeout( 1 );
	} elseif( $current_date >= $phase2_date && $current_date < $phase3_date ) {
		$current_phase = 'phase2';
		set_interstitial_cookie_timeout( 2 );
	} elseif( $current_date >= $phase3_date ) {
		$current_phase = 'phase3';
	}
	// echo $current_phase;

	get_header( 'special' );

	// $market = get_market2();
	// if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
	// 	$event_info = rnr3_get_event_info( $market );
	// }

	$twitter_hashtag	= $event_info->twitter_hashtag;
	$qt_lang			= rnr3_get_language();

?>
	<!-- alert banner -->
	<?php
		if( $qt_lang['enabled'] == 1 ) {
			$alertbar01_array	= qtrans_split( get_post_meta( get_the_ID(), $prefix . 'alertbar01_txt', 1 ) );
			$alertbar01			= $alertbar01_array[ $qt_lang['lang'] ];
			$alertbar2_array	= qtrans_split( get_post_meta( get_the_ID(), $prefix . 'alertbar2_txt', 1 ) );
			$alertbar2			= $alertbar2_array[ $qt_lang['lang'] ];
			$alertbar3_array	= qtrans_split( get_post_meta( get_the_ID(), $prefix . 'alertbar3_txt', 1 ) );
			$alertbar3			= $alertbar3_array[ $qt_lang['lang'] ];
		} else {
			$alertbar01	= get_post_meta( get_the_ID(), $prefix . 'alertbar01_txt', 1 );
			$alertbar2	= get_post_meta( get_the_ID(), $prefix . 'alertbar2_txt', 1 );
			$alertbar3	= get_post_meta( get_the_ID(), $prefix . 'alertbar3_txt', 1 );
		}

		if( ( $current_phase == 'phase0' || $current_phase == 'phase1' ) && $alertbar01 != '' ) {

			echo '<section id="alert">
				<section class="wrapper">
					<p>'. $alertbar01 .'</p>
				</section>
			</section>';

		} elseif( $current_phase == 'phase2' && $alertbar2 != '' ) {

			echo '<section id="alert">
				<section class="wrapper">
					<p>'. $alertbar2 .'</p>
				</section>
			</section>';

		} elseif( $current_phase == 'phase3' && $alertbar3 != '' ) {

		}
	?>

	<!-- main content -->
	<main role="main" id="main" class="no-hero winter-promo">
		<section id="deals">

			<?php if( $current_phase == 'phase0' || $current_phase == 'phase1' ) {

				$marquee_desktop	= get_post_meta( get_the_ID(), $prefix . 'marquee01_big', 1 );
				$marquee_mobile		= get_post_meta( get_the_ID(), $prefix . 'marquee01_small', 1 );

				if( $qt_lang['enabled'] == 1 ) {
					if( $qt_lang['lang'] != 'en' ) {
						$marquee_desktop	= str_replace( '.jpg', '_' . $qt_lang['lang'] . '.jpg', $marquee_desktop );
						$marquee_mobile		= str_replace( '.jpg', '_' . $qt_lang['lang'] . '.jpg', $marquee_mobile );
					}
				}

			} elseif( $current_phase == 'phase2' ) {

				$marquee_desktop	= get_post_meta( get_the_ID(), $prefix . 'marquee2_big', 1 );
				$marquee_mobile		= get_post_meta( get_the_ID(), $prefix . 'marquee2_small', 1 );

				if( $qt_lang['enabled'] == 1 ) {
					if( $qt_lang['lang'] != 'en' ) {
						$marquee_desktop = str_replace( '.jpg', '_' . $qt_lang['lang'] . '.jpg', $marquee_desktop );
						$marquee_mobile = str_replace( '.jpg', '_' . $qt_lang['lang'] . '.jpg', $marquee_mobile );
					}
				}

			} else {

				$marquee_desktop	= get_post_meta( get_the_ID(), $prefix . 'marquee3_big', 1 );
				$marquee_mobile		= get_post_meta( get_the_ID(), $prefix . 'marquee3_small', 1 );

				if( $qt_lang['enabled'] == 1 ) {
					if( $qt_lang['lang'] != 'en' ) {
						$marquee_desktop = str_replace( '.jpg', '_' . $qt_lang['lang'] . '.jpg', $marquee_desktop );
						$marquee_mobile = str_replace( '.jpg', '_' . $qt_lang['lang'] . '.jpg', $marquee_mobile );
					}
				}

			}

			echo '<div class="marquee">
				<div class="showdesktop"><img src="'. $marquee_desktop .'"></div>
				<div class="showmobile"><img src="'. $marquee_mobile .'"></div>
			</div>'; ?>

			<section class="wrapper">
				<?php
					if( $current_phase == 'phase0' ) {

						// leadup
						echo '<!-- phase 0 -->' .
						apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'intro01', 1 ) );

						winterpromo_leadgen();

					} elseif( $current_phase == 'phase1' ) {

						// leadup + findarace + interstitialA
						echo '<!-- phase 1 -->' .
						apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'intro01', 1 ) );

						winterpromo_leadgen();

						$findrace_content = winterpromo_findrace( 1 );
						echo $findrace_content['text'];

						if( empty( $_GET ) ) {
							get_interstitial( 1, 'wp_interstitial_1' );
						}

					} elseif( $current_phase == 'phase2' ) {

						// showtime + interstitialB
						echo '<!-- phase 2 -->' .
						apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'intro2', 1 ) );
						$findrace_content = winterpromo_findrace( 2 );

						echo $findrace_content['text'];

						if( empty( $_GET ) ) {
							get_interstitial( 2, 'wp_interstitial_2' );
						}

					} elseif( $current_phase == 'phase3' ) {

						// donezo
						echo '<!-- phase 3 -->' .
						apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'intro3', 1 ) );
					}
				?>

			</section>
		</section>

		<script src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/js/vendor/mixitup.min.js"></script>
		<script>
			$(document).ready(function(){
				$(".interstitial_fancybox").fancybox();
			});

			// use on load or overlay doesnt work for some reason
			$(window).on('load', function(){
				$(".interstitial_fancybox").trigger('click');
				$('#wp_interstitial').on('click', function(e){
					e.preventDefault();
					$.fancybox.close();
				});
				// $("#wp_interstitial").style.display = 'block';
			});

			var containerEl = document.querySelector('.mix_group');

			var mixer = mixitup(containerEl, {
				load: {
					sort: 'date:asc'
				}
			});

			const sortSelect = document.querySelector( '#sortselect' );

			sortSelect.onchange = function() {
				mixer.sort( this.value );

			};

		</script>

	</main>

<?php
	get_footer( 'series' );


	/* template specific functions */

	/**
	 * FUNCTION: template-specific
	 * display leadgen form (surveygizz)
	 */
	function winterpromo_leadgen() {

		echo '<section id="leadgen">
			'. get_post_meta( get_the_ID(), '_rnr3_leadgen', 1 ) .'
		</section>';

	}


	/**
	 * FUNCTION: template-specific
	 * returns find a race mixitup grid ( index: 'text' )
	 * will also return count of grid items ( index: 'count' )
	 * transient: 5 minute cache
	 */
	function winterpromo_findrace( $phase ) {

		$market = get_market2();

		if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
			$event_info = rnr3_get_event_info( $market );
		}

		$prefix				= '_rnr3_';
		$content			= array();
		$content['text']	= '';
		$qt_lang			= rnr3_get_language();
		include 'languages.php';

		// if ( false === ( $promo_locations = get_transient( 'promo_locations_results' ) ) ) {
			$args = array(
				'post_type'			=> 'winter_promo',
				'posts_per_page'	=> -1,
				'post_status'		=> 'publish',
				// 'meta_key'			=> '_rnr3_startdate',
				// 'orderby'			=> 'meta_value_num',
				// 'order'				=> 'ASC'
			);
			$promo_locations = new WP_Query( $args );
			// set_transient( 'promo_locations_results', $promo_locations, 5 * MINUTE_IN_SECONDS );
		// }

		if( $promo_locations->have_posts() ) {

			$content['text'] .= '<section id="findarace">
				<h3 class="subtitle">'. $txt_find_a_race .'</h3>
				<div class="controls">
					<label>'. $sortby_txt .'</label>
					<div id="sort">
						<select id="sortselect">
							<option value="date:asc">'. $txt_date .'</option>
							<option value="city:asc">'. $city_txt .'</option>
						</select>
					</div>

					<label>'. $txt_distance .'</label>
					<button class="filter" data-filter="all">'. $all_txt .'</button>
					<button class="filter" data-filter=".fivek">'. $fivek_txt .'</button>
					<button class="filter" data-filter=".tenk">'. $tenk_txt .'</button>
					<button class="filter" data-filter=".half-marathon">'. $halfmarathon_txt .'</button>
					<button class="filter" data-filter=".marathon">'. $marathon_txt .'</button>
					<button class="filter" data-filter=".relay">'. $relay_txt .'</button>
				</div>


				<div class="mix_group">';

					$modal_count = 1;

					while( $promo_locations->have_posts() ) {
						$promo_locations->the_post();

						// set distance and price arrays to be used as keys for mixitup and display in modal
						$distance_display = $distance_key = $price_array = $savings_array = array();

						$entries = get_post_meta( get_the_ID(), $prefix . 'race_group', 1 );

						if( !empty( $entries ) ) {

							foreach( $entries as $key => $entry ) {
								$distance_pieces	= isset( $entry['type'] ) ? explode( '::', $entry['type'] ) : '';
								$distance_key[]		= ( $distance_pieces != '' ) ? $distance_pieces[0] : '';
								$distance_display[]	= ( $distance_pieces != '' ) ? $distance_pieces[1] : '';

								$price_array[]		= isset( $entry['price'] ) ? $entry['price'] : '';
								$savings_array[]	= isset( $entry['savings'] ) ? $entry['savings'] : '';
							}

						}

						$event_distances		= implode( ' ', $distance_key );
						$festival				= get_post_meta( get_the_ID(), $prefix . 'festival', 1 );
						$start_date_timestamp	= get_post_meta( get_the_ID(), $prefix . 'startdate', 1 );
						$is_tbd					= get_post_meta( get_the_ID(), $prefix . 'is_tbd', 1 );

						if( $qt_lang['enabled'] == 1 ) {
							// qt translate on!
							if( $is_tbd != 'on' ) {
								$start_date_d			= date( 'j', get_post_meta( get_the_ID(), $prefix . 'startdate', 1 ) );
								$start_date_m_foreign	= get_day_or_month_by_language( $qt_lang['lang'], ( date( 'n', get_post_meta( get_the_ID(), $prefix . 'startdate', 1 ) ) - 1 ), 'month_long' );
								$start_date				= $start_date_d . ' ' . $start_date_m_foreign;
							}

							if( $festival == 'on' ) {

								$end_date_timestamp	= get_post_meta( get_the_ID(), $prefix . 'enddate', 1 );
								$end_date_d			= date( 'j', get_post_meta( get_the_ID(), $prefix . 'enddate', 1 ) );
								$end_date_m_foreign	= get_day_or_month_by_language( $qt_lang['lang'], ( date( 'n', get_post_meta( get_the_ID(), $prefix . 'enddate', 1 ) ) - 1 ), 'month_long' );

								if( date( 'M', $start_date_timestamp ) != ( date( 'M', $end_date_timestamp) ) ) {
									// diff months
									$end_date		= $end_date_d . ' ' . $end_date_m_foreign;
									$display_date	= $start_date . '-' . $end_date;
								} else {
									// same month
									$display_date = $start_date_d . '-' . $end_date_d . ' ' . $end_date_m_foreign;
								}

							} elseif( $is_tbd == 'on' ) {

								$display_date			= 'TBD';
								$start_date_timestamp	= '1591747200';

							} else {

								$display_date = $start_date;

							}

						} else {
							if( $is_tbd != 'on' ) {
								// qt translate off
								$start_date = date( 'M d', get_post_meta( get_the_ID(), $prefix . 'startdate', 1 ) );
							}

							if( $festival == 'on' ) {
								$end_date_timestamp = get_post_meta( get_the_ID(), $prefix . 'enddate', 1 );

								if( date( 'M', $start_date_timestamp ) != ( date( 'M', $end_date_timestamp) ) ) {

									// diff months
									$end_date		= date( 'M d', $end_date_timestamp );
									$display_date	= $start_date . '-' . $end_date;

								} else {

									// same month
									$display_date = $start_date . '-' . date( 'd', $end_date_timestamp );

								}

							} elseif( $is_tbd == 'on' ) {

								$display_date = 'TBD';
								// jun 10, 2020 -- arbitrary date because it NEEDS a date, but they want tbd
								$start_date_timestamp	= '1591747200';

							} else {

								$display_date = $start_date;

							}
						}

						/**
						 * set virtual run timestamp to june 11, 2020
						 * so it shows up AFTER the tbd events (june 10, 2020)
						 */
						if( get_the_title() == 'Virtual Run' ) {

							$start_date_timestamp = '1591876800';

						}

						$display_city	= str_replace( array( ' ', '.' ), '-', get_the_title() );
						$can_mex		= get_post_meta( get_the_ID(), $prefix . 'canada_mexico', 1 );
						$saveupto		= get_post_meta( get_the_ID(), $prefix . 'saveupto', 1 );

						if( $can_mex == 'canada' ) {
							$can_mex_class		= ' canmex_class';
							$can_mex_currency	= '<span class="canmex_price">CAD</span>';
						} elseif( $can_mex == 'mexico' ) {
							if( strlen( $saveupto ) > 3 ) {
								$can_mex_class		= ' canmex_class big_price_class';
							} else {
								$can_mex_class		= ' canmex_class';
							}
							$can_mex_currency	= '<span class="canmex_price">MXN</span>';

						} else {
							$can_mex_class		= '';
							$can_mex_currency	= '';
						}

						$content['text'] .= '<div class="mix '. $event_distances .' event-group-'. $display_city .'" data-city="'. $display_city .'" data-date="'. $start_date_timestamp .'">
							<div class="inner_mix">

								<h3 class="mix-title">'. get_the_title() .'</h3>

								<div class="mix-bottom">
									<p class="mix-date">'. $display_date .'</p>
									<div class="tag_cta">';

										$is_limited		= get_post_meta( get_the_ID(), $prefix . 'limited', 1 );

										if( $is_tbd == 'on' ) {

											// los angeles price tag (generic)
											$content['text'] .= '<div class="pricetag_la">
												<span class="tag_price">'. get_post_meta( get_the_ID(), $prefix . 'saveupto', 1 ) .' </span>
											</div>';

										} elseif( $is_limited == 'on' ) {

											// limited price tag treatment (show "limited supply")
											$content['text'] .= '<div class="pricetag">
												<span class="tag_head">'. $aslowas_txt .'</span>
												<span class="tag_price'. $can_mex_class .'">'. get_post_meta( get_the_ID(), $prefix . 'saveupto', 1 ) . $can_mex_currency .' </span>
												<span class="tag_limited">'. $limitedsupply_txt .'</span>
											</div>';

										} else {

											// all other price tags (show savings)
											$content['text'] .= '<div class="pricetag">
												<span class="tag_head">'. $aslowas_txt .'</span>
												<span class="tag_price'. $can_mex_class .'">'. get_post_meta( get_the_ID(), $prefix . 'saveupto', 1 ) . $can_mex_currency .' </span>';

												if( ( $tilesavings = get_post_meta( get_the_ID(), $prefix . 'tilesavings', 1 ) ) != '' ) {
													$content['text'] .= '<span class="tag_savings">'. $savings_txt .' '. $tilesavings .'</span>';
												}
											$content['text'] .= '</div>';

										}

										// always show "get details" on tile cta
										$cta_txt	= ( $phase == 1 ) ? $getdetails_txt : $registernow_txt;

										$cta_html	= '<a data-fancybox class="mix-cta fancybox" data-post-id="'. get_the_ID() .'" data-rnr-event-slug="'. $display_city .'" data-src="#modal-'. $display_city .'" href="javscript:;">'. $cta_txt .'</a>';

										$content['text'] .= '<div class="mix-get_deets">'. $cta_html .'</div>
									</div>

									<div class="mix-img">
										'. get_the_post_thumbnail() .'
									</div>

								</div>
							</div>


							<div style="display:none; max-width: 90%; width: 800px;" class="mix-modal" id="modal-'. $display_city .'">
								<div class="mix-modal_inside">';

									if( get_post_meta( get_the_ID(), $prefix . 'modal_gallery', 1 ) == '' && get_post_meta( get_the_ID(), $prefix . 'modal_bulleted_txt', 1 ) == '' ) {

										$content['text'] .= '<div class="grid_1_modal">';
										$modal_column_count = 1;

									} else {

										$content['text'] .= '<div class="grid_2_modal">';
										$modal_column_count = 2;

									}
										$content['text'] .= '<div class="column">';

											$display_date_year = ( get_post_meta( get_the_ID(), $prefix . 'enddate', 1 ) != '' ) ? get_post_meta( get_the_ID(), $prefix . 'enddate', 1 ) : get_post_meta( get_the_ID(), $prefix . 'startdate', 1 );
											$display_date_year = ( $is_tbd ) ? '' : date( 'Y', $display_date_year );

											$content['text'] .= '<h2 class="mix-modal-title">'. get_the_title() .'</h2>

											<p class="mix-modal-date">'. $display_date . ' ' . $display_date_year .'</p>
											'. apply_filters( 'the_content', get_the_content() );

											if( $distance_display[0] != '' ) {
												$content['text'] .= '<ul class="mix-modal_prices">';
													// show distances & prices, which were set up above
													for( $x = 0; $x < count( $distance_display ); $x++ ) {

														switch( $distance_display[$x] ) {
															case '1 Mile':
																$distance_translated = $onemile_txt;
																break;
															case '1K';
																$distance_translated = $onek_txt;
																break;
															case '5K';
																$distance_translated = $fivek_txt;
																break;
															case '10K';
																$distance_translated = $tenk_txt;
																break;
															case '11K';
																$distance_translated = $elevenk_txt;
																break;
															case '20K';
																$distance_translated = $twentyk_txt;
																break;
															case 'Mini';
																$distance_translated = 'Mini';
																break;
															case 'Fun Run';
																$distance_translated = 'Fun Run';
																break;
															case 'Relay';
																$distance_translated = $relay_txt;
																break;
															case 'Half Marathon';
																$distance_translated = $halfmarathon_txt;
																break;
															case 'Marathon';
																$distance_translated = $marathon_txt;
																break;
															case 'Winter Runningland';
																$distance_translated = 'Winter Runningland';
																break;
															case 'KiDS ROCK';
																$distance_translated = 'KiDS ROCK';
																break;
														}

														$content['text'] .= '<li>' .
															$distance_translated . ' - <span class="price">' . $price_array[$x] . '</span>';
															if( isset( $savings_array[$x] ) && $savings_array[$x] != '' ) {
																$content['text'] .= '<div class="mix-modal-savings">'. $savings_array[$x] .' '. $savings_txt .'</div>';
															}
														$content['text'] .= '</li>';
													}

												$content['text'] .= '</ul>';
											}


											if( $phase == 1 ) {
												$cta_modaltxt = $getdetails_txt;
											} elseif( $phase == 2 ) {
												$cta_modaltxt = $registernow_txt;
											}

											if( $is_tbd == 'on' ) {
												$cta_modaltxt = $getdetails_txt;
											}

											$content['text'] .= '<p><a target="_blank" href="'. get_post_meta( get_the_ID(), $prefix . 'register_url', 1 ) .'" class="mix-modal-cta">'. $cta_modaltxt .'</a></p>
										</div>';


										if( $modal_column_count == 2 ) {
											$content['text'] .= '<div class="column">

												<div id="gallery-carousel-'. $modal_count .'" class="mix-modal-carousel owl-carousel"></div>

												<div class="moreinfo">'.
													get_post_meta( get_the_ID(), $prefix . 'modal_bulleted_txt', 1 ) .'
												</div>

											</div>';
										}

									$content['text'] .= '</div>
								</div>
							</div>
						</div>';

						$modal_count++;
					}

					wp_reset_postdata();

				$content['text'] .= '<div class="gap"></div>
					<div class="gap"></div>
					<div class="gap"></div>
					<div class="gap"></div>
				</div>
			</section>';

			wp_reset_postdata();
		}

		$content['count'] = $modal_count;
		return $content;
	}


	/**
	 * FUNCTION: template-specific
	 * sets cookie timeout
	 * must be set in header BEFORE any output
	 * accepts phase number (int) to change the name of the cookie
	 */
	function set_interstitial_cookie_timeout( $phase ) {
		$cookie_name	= 'wp_interstitial_' . $phase;
		$cookie_path	= '/';
		$cookie_domain	= ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;

		if( !isset( $_COOKIE[$cookie_name] ) ) {
			$successcookie = setcookie( $cookie_name, 'set', time() + 60*60*24*2, $cookie_path, $cookie_domain, false );
		}

	}


	/**
	 * FUNCTION: template-specific
	 * displays interstitial
	 * accepts phase number (int) and cookie name (string)
	 */
	function get_interstitial( $phase, $cookiename ) {

		if( isset( $_COOKIE[$cookiename] ) && $_COOKIE[$cookiename] != '' ) {
			$qt_lang	= rnr3_get_language();
			$int_img	= '';

			if( $phase == 1 ) {
				$int_img = get_post_meta( get_the_ID(), '_rnr3_interstitial1_img', 1 );
			} elseif( $phase == 2 ) {
				$int_img = get_post_meta( get_the_ID(), '_rnr3_interstitial2_img', 1 );
			}

			if( $qt_lang['enabled'] == 1 ) {
				// qtranslate on
				if( $qt_lang['lang'] != 'en' ) {
					$int_img = str_replace( '.jpg', '_' . $qt_lang['lang'] . '.jpg', $int_img );
				}
			}

			if( $int_img != '' ) {
				echo '<a href="javascript:;" data-src="#wp_interstitial" class="interstitial_fancybox" style="display:none;" id="wp_interstitial_link"></a>
				<div id="wp_interstitial" style="display:none; padding: 0;">
					<a href="#"><img src="'. $int_img .'"></a>
				</div>';
			}
		}
	}
?>
