<?php
	/* Template Name: Travel */
	get_header();

	$parent_slug = the_parent_slug();
	rnr3_get_secondary_nav( $parent_slug );

	$market = get_market2();

	if ( false === ( $event_info = get_transient( 'event_info_data_' . $market ) ) ) {
		$event_info = rnr3_get_event_info( $market );
	}

	$qt_lang = rnr3_get_language();
	include 'languages.php';

	$args = array(
		'post_type'			=> 'travel-packages',
		'posts_per_page'	=> -1
	);

	if ( false === ( $travel_query = get_transient( 'travel_query_results' ) ) ) {
		$travel_query = new WP_Query( $args );
		set_transient( 'travel_query_results', $travel_query, 5 * MINUTE_IN_SECONDS );
	}
?>

<!-- main content -->
<main role="main" id="main">
	<section class="wrapper">
		<?php
			// allowing custom text if wanted
			if (have_posts()) : while (have_posts()) : the_post();

				echo '<h2>'. get_the_title(). '</h2>';
				the_content();

			endwhile; endif;

			echo '<div class="grid_3_special">';

				if ($travel_query->have_posts()) : while ($travel_query->have_posts()) : $travel_query->the_post();
					$logo_id	= rnr3_get_image_id( get_post_meta( get_the_ID(), '_travel_image', true ) );
					$logo		= wp_get_attachment_image_src( $logo_id, 'travel-logo' );

					echo '<div class="column_special">
						<div class="travel_item">
							<h2><a href="'. get_post_meta( get_the_ID(), '_travel_link', true ) .'" target="_blank">'.get_the_title().'</a></h2>
							<ul>';

								if( $logo[0] != '' ) {
									echo '<li><img src="'. $logo[0] .'"></li>';
								}

								echo '<li>'. get_post_meta( get_the_ID(), '_travel_country', true ) .'</li>
								<li>'. get_post_meta( get_the_ID(), '_travel_contact', true ) .'</li>
								<li><a href="mailto:'. get_post_meta( get_the_ID(), '_travel_email', true ) .'">'. get_post_meta( get_the_ID(), '_travel_email', true ) .'</a></li>
								<li>'.$display_text['address'].': '. get_post_meta( get_the_ID(), '_travel_address', true ) .'</li>
								<li>'.$display_text['phone'].': '. get_post_meta( get_the_ID(), '_travel_phone', true ) .'</li>
							</ul>
						</div>
					</div>';

				endwhile; endif;

			echo '</div>';

		?>
	</section>

</main>

<?php get_footer(); ?>
