<?php
	/* Template Name: M's - Miles (Series) */
	get_header('special');
	$prefix		= '_rnr3_m_miles_';
	$qt_lang	= rnr3_get_language();

?>
<main role="main" id="main" class="m_miles">
	<section id="miles-panel1">
		<section class="grid_2 wrapper">

			<?php echo apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'cat_intro_txt', 1 ) ); ?>

			<div id="tabs" class="category_desktop">
				<div class="column">
					<ul>
						<?php
							for( $i = 1; $i < 9; $i++ ) {
								${'icon-' . $i}		= get_post_meta( get_the_ID(), $prefix . 'icon-' . $i, 1 );
								${'title-' . $i}	= get_post_meta( get_the_ID(), $prefix . 'title-' . $i, 1 );
								${'txt-' . $i}		= apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'txt-' . $i, 1 ) );
								${'filter-' . $i}	= get_post_meta( get_the_ID(), $prefix . 'filter-' . $i, 1 );

								echo '<li>
									<a href="#tab-'.$i.'" class="filter" data-filter=".miles-'. ${'filter-' . $i} .'"><img src="'. ${'icon-' . $i} .'"></a>
									<p>'.${'title-' . $i}.'</p>
								</li>';
							}
						?>

					</ul>
				</div>

				<div class="column">
					<?php for( $i = 1; $i < 9; $i++ ) {
						echo '<div id="tab-'.$i.'">
							<h3>'. ${'title-' . $i} .'</h3>
							'. ${'txt-' . $i} .'
						</div>';
					} ?>
				</div>
			</div>

			<div class="accordion category_mobile">
				<?php
					for( $i = 1; $i < 7; $i++ ) {
						echo '<div class="accordion-section" style="position: relative;">
							<div class="accordion-section-title" data-reveal="#cat'.$i.'">'. ${'title-' . $i} .'</div>
							<div class="accordion-section-content" id="cat'.$i.'">
								<figure><img src="'. get_post_meta( get_the_ID(), $prefix . 'icon-' . $i, 1 ) .'"></figure>

								'.apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'txt-' . $i, 1 ) ).'
							</div>
						</div>';

					}
				?>
			</div>

		</section>
	</section>


	<section id="miles-categories">
		<section class="wrapper">
			<?php
				$all_event_dates = rnr3_get_all_events_dates( $qt_lang['lang'] );
				echo apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'filter_intro_txt', 1 ) );
				echo '<div class="controls">
					<button class="filter" data-filter="all">Reset</button>
					<button class="filter" data-filter=".miles-pr">Set a PR</button>
					<button class="filter" data-filter=".miles-conquer">Conquer this Challenge</button>
					<button class="filter" data-filter=".miles-newbie">Fun for Newbies</button>
					<button class="filter" data-filter=".miles-urbanparadise">Urban Paradise</button>
					<button class="filter" data-filter=".miles-historic">Run into History</button>
					<button class="filter" data-filter=".miles-urbanexplorers">Urban Explorers</button>
					<button class="filter" data-filter=".miles-remix">Complete the Remix</button>
					<button class="filter" data-filter=".miles-adventurers">Adventure Seekers Wanted</button>
				</div>';

				if ( false === ( $miles_query = get_transient( 'miles_query_results' ) ) ) {
					$args = array(
						'post_type'			=> 'location',
						'post_status'		=> 'publish',
						'posts_per_page'	=> -1,
						'orderby'			=> 'title',
						'order'				=> 'ASC'
					);
					$miles_query = new WP_Query( $args );
					set_transient( 'miles_query_results', $miles_query, 5 * MINUTE_IN_SECONDS );
				}

				if( $miles_query->have_posts() ) {
					echo '<div class="mix_group">';
						while( $miles_query->have_posts() ) {
							$miles_query->the_post();

							$event_url		= get_post_meta( get_the_ID(), '_rnr3_location_url', 1 );
							$event_filters	= get_post_meta( get_the_ID(), '_rnr3_location_checks', 1 );
							$event_bg		= get_post_meta( get_the_ID(), '_rnr3_location_bg', 1 );
							$event_blog_id	= get_post_meta( get_the_ID(), '_rnr3_emi', 1 );

							if( isset( $event_filters ) ) {
								$type_array = $type_display_array = array();

								foreach( $event_filters as $filter ) {
									$filter_pieces			= explode( '::', $filter );
									$type_array[]			= 'miles-' . $filter_pieces[0];
									$type_display_array[]	= $filter_pieces[1];
								}
								$filter_string = implode( ' ', $type_array );
							}

							echo '<div class="mix column '.$filter_string.'">
								<div class="inner_location" style="background:url( '.$event_bg.' ) no-repeat top left #000;">
									<h3>'.get_the_title().'</h3>';

									//display date
									if( isset( $event_blog_id ) AND $event_blog_id > 0) {
										if( $qt_lang['lang'] == "en" ){
											echo '<p>'. $all_event_dates[$event_blog_id]["monthdate"];

											if( $all_event_dates[$event_blog_id]["year"] ) {
												echo ", ".$all_event_dates[$event_blog_id]["year"] . '</p>';
											}
										} else {
											echo '<p>' . $all_event_dates[$event_blog_id]["monthdateeuro"];

											if( $all_event_dates[$event_blog_id]["year"] ) {
												echo ", ".$all_event_dates[$event_blog_id]["year"] . '</p>';
											}
										}
									}
									//end display date

									if( isset( $type_display_array ) ) {

										echo '<ul class="event_filters_desktop">';
											foreach( $type_display_array as $type ) {
												echo '<li>
													'.$type.'
												</li>';
											}
										echo '</ul>';

									}
									echo apply_filters( 'the_content', get_the_content() );

									echo '<ul class="event_links">
										<li><a href="'.$event_url.'/register/">Register</a></li><li><a href="'.$event_url.'/">Learn More</a></li>
									</ul>
								</div>
							</div>';


						}
					echo '</section>';
				}

				wp_reset_postdata();

				echo '<div class="gap"></div>
				<div class="gap"></div>';
			?>

			<script src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/js/vendor/mixitup.min.js"></script>
			<script>
				var containerEl = document.querySelector('.m_miles');

				var mixer = mixitup(containerEl);

			</script>

		</section>
	</section>
</main>


<script>
	jQuery(function($) {
		$( "#tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
	});

	$(document).ready(function(){

		/**
		 * This part causes smooth scrolling using scrollto.js
		 * We target all a tags inside the nav, and apply the scrollto.js to it.
		 */
		$("#secondary a").click(function(evn){
			evn.preventDefault();
			$('html,body').scrollTo(this.hash, this.hash);
		});
	});

</script>
<?php get_footer('series'); ?>
