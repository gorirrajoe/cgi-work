<?php
	/**
	 * Template part for displaying posts with old video embeds on AMP
	 */

	// Array for post_class
	$post_class_array	= array(
		'col-xs-12',
		'col-sm-7',
		'col-md-8',
		'article',
		'article_type_single',
		'article_category_video'
	);

	$post_ID		= get_the_ID();

	$yt_vid			= get_post_meta( $post_ID, '_youtube_id', true );
	$brightcove_id	= get_post_meta( $post_ID, 'video_id', true );
?>
<article id="article" <?php post_class( $post_class_array ); ?>>

	<header class="article__header">

		<?php the_title( '<h1 class="article__title">', '</h1>' ); ?>
		<?php echo '<p class="post_author">By ' . get_guest_author() .', '. format_timestamp_2() .'</p>'; ?>

	</header>

	<?php if ( in_category( array( 'sponsored' ) ) ): ?>
		<h4 class="sponsored-label">Sponsored Content</h4>
	<?php endif; ?>

	<div class="row">

		<div class="col-xs-12"> <!-- #article-right on non-amp -->

			<section class="article__content">
				<?php
					echo AMP_Mods::get_social_sharing();

					/**
					 * Display Video (legacy, this option no longer exists on the 2016 redesign so that editors can place the video where every their heart desires)
					 * Adding full code so AMP shortcode registration is coded properly
					 */
					if ( $yt_vid != '' ) {

						printf(
							'<amp-youtube data-videoid="%s" height="270" width="480" layout="responsive"></amp-youtube>',
							$yt_vid
						);

					} elseif ( $brightcove_id != '' ) {

						printf(
							'<amp-brightcove
								data-account="3655502813001"
								data-player="r1oC9M1S"
								data-embed="default"
								data-video-id="%s"
								layout="responsive"
								width="480" height="270">
							</amp-brightcove>',
							$brightcove_id
						);

					}

					// the_content();
					echo $this->get( 'post_amp_content' );

					echo AMP_Mods::get_social_sharing();
				?>
			</section>

		</div><!-- END #ARTICLE-RIGHT -->

	</div><!-- END .row -->

</article>
