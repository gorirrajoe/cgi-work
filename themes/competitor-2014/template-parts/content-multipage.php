<?php
	/**
	 * Template part for displaying multipage single posts.
	 */
	global $page;
	global $post;

	// Check if we are trying to display all the pages in one
	$all	= isset( $_GET['all'] ) ? true : false;

	$is_mobile	= is_mobile();

	$post_ID	= $post->ID;

	$attachments	= get_children( array(
		'post_parent'		=> $post_ID,
		'post_status'		=> 'inherit',
		'post_type'			=> 'attachment',
		'post_mime_type'	=> 'image',
		'orderby'			=> 'menu_order',
		'order'				=> 'ASC'
	) );

	$attachments_count	= count( $attachments );

	/**
	 * create table of contents navigation
	 */
	preg_match_all( '/<!--pagetitle:(.*?)-->/', $post->post_content, $titlesarray, PREG_PATTERN_ORDER );
	$pagetitles					= $titlesarray[1];
	$pagetitles_count			= count( $pagetitles );
	$featured_thumb_only		= get_post_meta( $post_ID, '_featured_thumb', true ) ?: '';
	$the_permalink				= get_permalink();

	if ( $pagetitles_count > 0) {
		$mobile_nav		= '<script type="text/javascript">
				function goToPage(){
					var url = document.getElementById("selectbox").value;
					location.href = url;
				}
			</script>';
		$mobile_nav .= '<form class="table-of-contents" method="GET" action ="'. site_url() .'">
				<label>Page '. $page .' of '.$pagetitles_count.'</label>
				<select name="selectbox" id="selectbox" onchange="goToPage()">';

		$desktop_nav = '<div class="aside_recent_posts multipage_recent">
				<h3>Table of Contents</h3>
				<ul>';

		foreach ( $pagetitles as $i => $pagetitle ) {

			$i++;
			$page_number	= $i != 1 ? '/'. $i: '';
			$current_option	= $page == $i ? ' selected="selected" ' : '';
			$current_id		= $page == $i ? ' id="current-page"' : '';

			$desktop_nav .= '<li'. $current_id .'><a href="'. $the_permalink . $page_number . '">'. $pagetitle .'</a></li>';
			$mobile_nav .= '<option'. $current_option .' value="'. $the_permalink . $page_number .'">'. $pagetitle .'</option>';

		}
		$desktop_nav .= '</ul></div>';
		$mobile_nav .= '</select>
				<!--<button onclick="goToPage()" type="submit">Go</button>-->
			</form>';

		if ( !empty( $featured_thumb_only ) ) {
			// If they only want the featured image as a thumb in blog rolls, DO NOTHING
		} elseif ( $attachments_count > 0 ) {

			// We had to make sure there was any actual attachments if we are going to try the following

			// Pulling the attachment IDs to identify which one to use
			// With menu_order as key to find the right page
			// If there was no menu_order defined, it will use a natural order
			$attachment_IDs	= array();
			foreach ( $attachments as $key => $attachment ) {
				if ( $attachment->menu_order !== 0 ) {
					$attachment_IDs[$attachment->menu_order]	= $key;
				} else {
					$attachment_IDs[]	= $key;
				}
			}

			// Find out if we are using the code for order of custom featured images
			preg_match_all( '/<!--pageimage:(\s\d+)-->/', get_the_content(), $matches );

			// if we found a match for the page image then use that to display
			if ( !empty( $matches[0] ) ) {

				// make sure we trim the result
				$page_image	= trim( $matches[1][0] );

				// make absolutely sure the corresponding attachment ID number exists
				if ( array_key_exists( $page_image, $attachment_IDs ) ) {
					$attachment_ID	= $attachment_IDs[$page_image];
				}

			} elseif ( array_key_exists( $page, $attachment_IDs ) ) {

				// else if there was no match found, use the global $page to check for the corresponding attachment ID
				$attachment_ID	= $attachment_IDs[$page];

			}

		} elseif ( has_post_thumbnail() ) {

			// else if STILL there was no matches found, use the featured image of the post
			$attachment_ID = get_post_thumbnail_id();

		}

		// If the $attachment_ID was every defined, use that to display the image here
		if ( !empty( $attachment_ID ) ) {

			$image_size	= $is_mobile ? 'medium' : 'large';

			$image_array	= wp_get_attachment_image_src( $attachment_ID, $image_size );

			// Add vertical class if height is greater than the width
			$class	= $image_array[2] > $image_array[1] ? ' vertical-image' : '';

			// define the title and caption
			$thumbnail_title	= $attachments[$attachment_ID]->post_title != '' ? 'title="'. $attachments[$attachment_ID]->post_title .'"' : '';
			$thumbnail_caption	= $attachments[$attachment_ID]->post_excerpt != '' ? '<div class="figcaption">'. $attachments[$attachment_ID]->post_excerpt .'</div>' : '';

			// bring it all together and display it
			printf(
				'<div class="figure'. $class .'"><img %s src="%s"/>%s</div>',
				$thumbnail_title,
				$image_array[0],
				$thumbnail_caption
			);

		}

		// Display the navigations
		echo $desktop_nav;
		echo $mobile_nav;

	} else {
		// this is hiding all of the images that were in the story originally, we will place the images when the post is displayed
		// Check if we want the image inline or not
		if ( empty( $featured_thumb_only ) ) {

			if ( $page == 1 ) {

				if ( $attachments_count == 1 ) {

					if ( has_post_thumbnail() ){

						$attachment_ID = get_post_thumbnail_id();

					} else {

						// If there was not post thumbnail, get the first attachment and break out
						foreach ( $attachments as $attachment ) {
							$attachment_ID = $attachment->ID;
							break;
						}

					}

				} elseif ( $attachments_count > 1 ) {
					// else if there is more than one attachment display the gallery carousel of all the images that are attached
					echo my_gallery_2( '', array(), 'medium', 'carousel' );
				} elseif ( has_post_thumbnail() ) {

					$attachment_ID	= get_post_thumbnail_id();

				} else {
					// else if there are no images attached to the story show a list of the most recently posted stories
					// get 5 most recently posted articles and display the top four, and make sure that the one you're on is not in the list
					echo get_latest_stories_2( $post_ID );
				}
			} elseif ( $attachments_count > 0 ) {

				// We had to make sure there was any actual attachments if we are going to try the following

				// Pulling the attachment IDs to identify which one to use
				// With menu_order as key to find the right page
				// If there was no menu_order defined they all default to 0 so relax, this works
				$attachment_IDs	= wp_list_pluck( $attachments, 'ID', 'menu_order' );

				// Find out if we are using the code for order of custom featured images
				preg_match_all( '/<!--pageimage:(\s\d+)-->/', get_the_content(), $matches );

				if ( !empty( $matches[0] ) ) {

					// make sure we trim the result
					$page_image	= trim( $matches[1][0] );

					// make absolutely sure the corresponding attachment ID number exists
					if ( array_key_exists( $page_image, $attachment_IDs ) ) {
						$attachment_ID	= $attachment_IDs[$page_image];
					}

				}

			}

			// If the $attachment_ID was every defined, use that to display the image here
			// Yes, that mess above and even worse in PostType.php was just to determine the appropriate Attachment ID to be able to display it
			if ( !empty( $attachment_ID ) ) {

				$image_size		= $is_mobile ? 'medium' : 'large';

				$image_array	= wp_get_attachment_image_src( $attachment_ID, $image_size );

				// Add vertical class if height is greater than the width
				$class	= $image_array[2] > $image_array[1] ? ' vertical-image' : '';

				// define the title and caption
				$thumbnail_image	= get_post( $attachment_ID );
				$thumbnail_title	= $thumbnail_image->post_title != '' ? ' title="'. $thumbnail_image->post_title .'"' : '';
				$thumbnail_caption	= $thumbnail_image->post_excerpt != '' ? '<div class="figcaption">'. $thumbnail_image->post_excerpt .'</div>' : '';

				// bring it all together and display it
				printf(
					'<div class="figure'. $class .'"><img %s src="'. $image_array[0] .'"/>%s</div>',
					$thumbnail_title,
					$thumbnail_caption
				);

			}

		}

	}
	// if the URL has the parameter $all, then display all the post content, not just the single multipage content
	if ( $all ) {
		$content_string = apply_filters( 'the_content', $post->post_content );
	} else {
		$content_string = apply_filters( 'the_content', get_the_content() );
	}
	if ( !get_post_meta( $post->ID, '_display_images', true ) ) {
		echo '<style>.wp-caption{display: none;}</style>';
		$content_string = preg_replace( "/<img[^>]+\>/i", ' ', $content_string );
	}

	echo $content_string;

	$prev_next_links = wp_link_pages( array(
		'before'			=> '<div class="row multipage_row">',
		'after'				=> '</div>',
		'next_or_number'	=> 'next',
		'previouspagelink'	=> '<span class="icon-left-open"></span> Previous Page',
		'nextpagelink'		=> 'Next Page <span class="icon-right-open"></span>',
		'echo'				=> 0
	) );
	$prev_next_links	= str_replace( '<a href=', '<div class="btn_primary"><a class="next-post" href=', $prev_next_links );
	$prev_next_links	= str_replace( '</a>', '</a></div>', $prev_next_links );

	echo $prev_next_links;

	if ( $pagetitles_count == 0 ) {

		wp_link_pages( array(
			'before'			=> '<span id="page-index"><strong>Pages: </strong>',
			'after'				=> '</span>',
			'next_or_number'	=> 'number'
		) );

	}
