<?php
	/**
	 * Template part for displaying the gift-guide category.
	 */

	// category must be named "XXXX Gift Guide", where XXXX is the 4 digit year
	$categories		= get_category( get_query_var('cat') );

	/**
	 * check to see if this is the main category (e.g. 2017-gift-guide)
	 * or a subcategory under the main one
	 */
	if( $categories->parent == 0 ) {
		$slug_explode	= explode( '-', $categories->slug );
		$year			= $slug_explode[0];
		$gg_url			= site_url( '/category/' . $categories->slug );
		$subcategory	= false;
	} else {
		$parent_cat		= get_category( $categories->parent );
		$slug_explode	= explode( '-', $parent_cat->slug );
		$year			= $slug_explode[0];
		$gg_url			= site_url( '/category/' . $parent_cat->slug );
		$subcategory	= true;
	}


	// Grabbing all the  options so we can just use array keys from here on
	$gg_options	= GearGuide_Admin::get_guide_option( $year , 'all' );
?>

<div class="giftguide__final">
	<div class="gg_container">

		<?php if( array_key_exists( 'gift_guide_'. $year .'_banner_image', $gg_options ) ) { ?>
			<div class="gg_banner">
				<a href="<?php echo $gg_url; ?>"><img src="<?php echo $gg_options['gift_guide_'. $year .'_banner_image']; ?>" /></a>
			</div>
		<?php }


		/**
		 * page title
		 */
		echo '<h1 class="archive-labels">'. $categories->cat_name . '</h1>';


		/**
		 * nav
		 * only show nav on subcategories and single posts
		 */
		if( $subcategory != false ) {

			echo '<div class="gg-submenu__label">Categories <span class="icon-down-open"></span></div>';

			$args = array(
				'menu'				=> strtolower( $categories->cat_name ) . '-submenu',
				'menu_class'		=> 'gg-submenu__wrapper',
				'container_class'	=> 'gg-submenu',
				'fallback_cb'		=> false,
				'walker'			=> new GG_Walker_Nav_Menu()
			);
			wp_nav_menu( $args );

		} ?>


		<div id="article-left">

			<div id="share-links-left" >
				<?php print_social_media( 'left' ); ?>
			</div>

		</div>

		<div id="article-right">

			<div id="share-links">
				<div id="share-links-inner">

					<?php print_social_media( 'top' ); ?>

				</div>
			</div>

			<div class="cgg_2016_container">
				<div class="gg-row">

					<?php if( $subcategory == false ) { ?>

						<div class="gg-two-thirds">

							<?php
								/**
								 * intro
								 */
								$intro_txt = isset( $gg_options['gift_guide_'. $year .'_intro'] ) ? apply_filters( 'the_content', $gg_options['gift_guide_'. $year .'_intro'] ) : '';

								if( $intro_txt != '' ) {
									echo '<div class="gg-intro">'.
										$intro_txt .'
									</div>';
								}


								$group_count = count( $gg_options['gift_guide_'. $year .'_image_group'] );

								for( $i = 0; $i <= 3; $i++ ) {
									$parsed_url			= $gg_options['gift_guide_'. $year .'_image_group'][$i]['link'] != '' ? parse_url( $gg_options['gift_guide_'. $year .'_image_group'][$i]['link'] ) : '';
									$google_tracking	= '';

									if( !empty( $gg_options['gift_guide_'. $year .'_image_group'][$i]['link'] ) && strpos( home_url(), $parsed_url['host'] ) === FALSE ) {

										$google_tracking = 'onclick="_gaq.push([\'_trackEvent\', \'Competitor Running Gift Guide\', \'running\', \''. $gg_options['gift_guide_'. $year .'_image_group'][$i]['link'] .'\'])"';

									}

									$img	= isset( $gg_options['gift_guide_'. $year .'_image_group'][$i]['image'] ) ? $gg_options['gift_guide_'. $year .'_image_group'][$i]['image'] : '';
									$link	= isset( $gg_options['gift_guide_'. $year .'_image_group'][$i]['link'] ) ? $gg_options['gift_guide_'. $year .'_image_group'][$i]['link'] : '';
									$title	= isset( $gg_options['gift_guide_'. $year .'_image_group'][$i]['title'] ) ? ' alt="'. $gg_options['gift_guide_'. $year .'_image_group'][$i]['title'] .'"' : '';


									echo '<div class="gg-item">
										<a href="'. $link .'" '. $google_tracking .'>
											<img src="'. $img .'" id="img-'. $i .'"'. $title .'>
										</a>
									</div>';


								}
							?>
						</div>


						<div class="gg-one-third">
							<div class="cgg_2016_ad_col gg-ad">
								<h4>Advertisement</h4>
								<section class="advert advert_xs_300x250 advert_location_sidebar">
									<div class="advert__wrap"><?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('middle-gg') : ''; ?></div>
								</section>
							</div>
						</div>


						<div class="gg-row">
							<div class="gg-full">
								<?php
									for( $i = 4; $i < $group_count; $i++ ) {

										$parsed_url			= $gg_options['gift_guide_'. $year .'_image_group'][$i]['link'] != '' ? parse_url( $gg_options['gift_guide_'. $year .'_image_group'][$i]['link'] ) : '';
										$google_tracking	= '';

										if( !empty( $gg_options['gift_guide_'. $year .'_image_group'][$i]['link'] ) && strpos( home_url(), $parsed_url['host'] ) === FALSE ) {

											$google_tracking = 'onclick="_gaq.push([\'_trackEvent\', \'Competitor Running Gift Guide\', \'running\', \''. $gg_options['gift_guide_'. $year .'_image_group'][$i]['link'] .'\'])"';

										}

										$img	= isset( $gg_options['gift_guide_'. $year .'_image_group'][$i]['image'] ) ? $gg_options['gift_guide_'. $year .'_image_group'][$i]['image'] : '';
										$link	= isset( $gg_options['gift_guide_'. $year .'_image_group'][$i]['link'] ) ? $gg_options['gift_guide_'. $year .'_image_group'][$i]['link'] : '';
										$title	= isset( $gg_options['gift_guide_'. $year .'_image_group'][$i]['title'] ) ? ' alt="'. $gg_options['gift_guide_'. $year .'_image_group'][$i]['title'] .'"' : '';


										echo '<div class="gg-item">
											<a href="'. $link .'" '. $google_tracking .'>
												<img src="'. $img .'" id="img-' . $i .'"'. $title .'>
											</a>
										</div>';

									}
								?>
							</div>
						</div>

					<?php } else {

						if( have_posts() ) {

							$count = 1;

							echo '<div class="gg-two-thirds">';

								while( have_posts() ) {
									the_post();

									echo '<div class="gg-item">

										<a href="'. get_the_permalink() .'">'.
											get_the_post_thumbnail( get_the_ID(), 'square-gift-guide-product' ) .'
											<h4>'. get_the_title() .'</h4>
										</a>

									</div>';

									if( $count == 4 ) { ?>

										</div>

										<div class="gg-one-third">
											<div class="cgg_2016_ad_col gg-ad">
												<h4>Advertisement</h4>
												<section class="advert advert_xs_300x250 advert_location_sidebar">
													<div class="advert__wrap"><?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('middle-gg') : ''; ?></div>
												</section>
											</div>
										</div>

										<div class="gg-row">
											<div class="gg-full">

									<?php }

									$count++;
								}


								if( $count >= 4 ) {
										echo '</div>
									</div>';
								} else {
									echo '</div>';
								}
						}

					} ?>

				</div>
			</div>
		</div>

	</div>
</div>