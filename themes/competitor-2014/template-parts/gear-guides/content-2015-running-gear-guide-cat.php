<?php
	/* COMPETITOR GEAR GUIDE 2015 */

	$gg_options	= GearGuide_Admin::get_guide_option( 2015 , 'all' );
	$prefix		= 'gear_guide_2015_';
?>

<style>
	.rgg_banner {
		margin:2.375em 0 1.25em;
		text-align:center;
		background-color:#000;
	}
	.rgg_full {
		text-align:center;
		margin-bottom:1.25em;
	}
	.rgg_row {
		overflow:hidden;
		margin:auto;
	}
	.rgg_one_third.rgg_right_module {
		margin-left:0;
	}
	.rgg_ad_col {
		background-color:#eee;
		text-align:center;
		padding:2em 0;
		margin-bottom:1.25em;
	}
	.rgg_one_third,
	.rgg_module {
		overflow:hidden;
	}
	.rgg_ad_col h4 {
		padding:0 0 1.400em;
	}
	.rgg_one_third .rgg_one_sixth,
	.rgg_last_third .rgg_one_sixth {
		text-align:center;
		margin-bottom:1.25em;
	}
	#div-300_600_top_300_250 {
		display:table;
		margin:auto;
	}
	.buyers-guide-slider-container {
		clear:both;
	}
	.rgg_one_sixth.rgg_nomargin {
		margin-bottom:0;
	}
	@media only screen and ( min-width:440px ) {
		.rgg_one_third .rgg_one_sixth,
		.rgg_last_third .rgg_one_sixth {
			float:left;
			margin-bottom:1.875em;
		}
		.rgg_banner,
		.rgg_full,
		.rgg_ad_col {
			margin-bottom:1.875em;
		}
		.rgg_right_module {
			margin-left:1.875em;
		}
		.rgg_row {
			display:table;
		}
		.rgg_one_sixth.rgg_nomargin {
			margin-bottom:0;
		}
	}
	@media only screen and ( min-width:990px ) {
		.rgg_container {
			max-width:830px;
			margin:auto;
		}
		.rgg_banner {
			margin:2.375em 0 1.875em;
		}
		.rgg_one_third .rgg_one_sixth,
		.rgg_last_third .rgg_one_sixth {
			width:46.25%;
		}
		.rgg_one_third {
			float:left;
			width:48.19277108433735%;
		}
		.rgg_one_third.rgg_right_module {
			float:left;
			margin-left:1.875em;
		}
		.rgg_ad_col {
			margin-bottom:1.875em;
		}
		.rgg_right_module {
			margin-left:1.875em;
		}
		.rgg_last_third .rgg_row {
			float:left;
			display:block;
		}
		.rgg_right_module_2 {
			margin-left:1.875em;
			float:left;
		}
	}
	@media only screen and ( min-width:1300px ) {
		.rgg_container {
			max-width:1260px;
		}
		.rgg_banner {
			margin:2.375em 0 1.875em;
		}
		.rgg_two_thirds {
			float:left;
			width:65.87301587301587%;
		}
		.rgg_last_third {
			float: left;
			margin-left: 1.875em;
			width: 31.746%;
		}
		.rgg_last_third .rgg_row {
			width:auto;
			float:none;
			margin-left:0;
		}
		.rgg_ad_col {
			padding:2em 0 4.250em;
		}
		.rgg_right_module_2 {
			margin-left:0;
		}
	}
</style>


<div class="rgg_container">
	<div class="rgg_banner">
		<img src="<?php echo $gg_options[$prefix . 'banner_image']; ?>" />
	</div>

	<?php if( !( get_query_var( 'paged' ) > 1 ) ) { ?>
		<div class="rgg_two_thirds rgg_module">
			<div class="rgg_full">
				<?php /* 1 */
					$parsed_url_1 = parse_url( $gg_options[$prefix . 'link_1'] );
					$google_tracking_1 = '';
					if( $gg_options[$prefix . 'link_1'] != '' && strpos( home_url(), $parsed_url_1['host'] ) === FALSE ) {
						$google_tracking_1 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_1'].'\'] )"';
					}
				?>
				<a href="<?php echo $gg_options[$prefix . 'link_1']; ?>" <?php echo $google_tracking_1;?>>
					<img src="<?php echo $gg_options[$prefix . 'image_1']; ?>">
				</a>
			</div>
			<div class="rgg_one_third rgg_module">
				<div class="rgg_row">
					<div class="rgg_one_sixth rgg_module">
						<?php /* 2 */
							$parsed_url_2 = parse_url( $gg_options[$prefix . 'link_2'] );
							$google_tracking_2 = '';
							if( $gg_options[$prefix . 'link_2'] != '' && strpos( home_url(), $parsed_url_2['host'] ) === FALSE ) {
								$google_tracking_2 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_2'].'\'] )"';
							}
						?>
						<a href="<?php echo $gg_options[$prefix . 'link_2']; ?>" <?php echo $google_tracking_2;?>>
							<img src="<?php echo $gg_options[$prefix . 'image_2']; ?>">
						</a>
					</div>
					<div class="rgg_one_sixth rgg_right_module">
						<?php /* 3 */
							$parsed_url_3 = parse_url( $gg_options[$prefix . 'link_3'] );
							$google_tracking_3 = '';
							if( $gg_options[$prefix . 'link_3'] != '' && strpos( home_url(), $parsed_url_3['host'] ) === FALSE ) {
								$google_tracking_3 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_3'].'\'] )"';
							}
						?>
						<a href="<?php echo $gg_options[$prefix . 'link_3']; ?>" <?php echo $google_tracking_3;?>>
							<img src="<?php echo $gg_options[$prefix . 'image_3']; ?>">
						</a>
					</div>
				</div>
				<div class="rgg_full">
					<?php /* 4 */
						$parsed_url_4 = parse_url( $gg_options[$prefix . 'link_4'] );
						$google_tracking_4 = '';
						if( $gg_options[$prefix . 'link_4'] != '' && strpos( home_url(), $parsed_url_4['host'] ) === FALSE ) {
							$google_tracking_4 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_4'].'\'] )"';
						}
					?>
					<a href="<?php echo $gg_options[$prefix . 'link_4']; ?>" <?php echo $google_tracking_4;?>>
						<img src="<?php echo $gg_options[$prefix . 'image_4']; ?>">
					</a>
				</div>
				<div class="rgg_row">
					<div class="rgg_one_sixth rgg_module">
						<?php /* 5 */
							$parsed_url_5 = parse_url( $gg_options[$prefix . 'link_5'] );
							$google_tracking_5 = '';
							if( $gg_options[$prefix . 'link_5'] != '' && strpos( home_url(), $parsed_url_5['host'] ) === FALSE ) {
								$google_tracking_5 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_5'].'\'] )"';
							}
						?>
						<a href="<?php echo $gg_options[$prefix . 'link_5']; ?>" <?php echo $google_tracking_5;?>>
							<img src="<?php echo $gg_options[$prefix . 'image_5']; ?>">
						</a>
					</div>
					<div class="rgg_one_sixth rgg_right_module">
						<?php /* 6 */
							$parsed_url_6 = parse_url( $gg_options[$prefix . 'link_6'] );
							$google_tracking_6 = '';
							if( $gg_options[$prefix . 'link_6'] != '' && strpos( home_url(), $parsed_url_6['host'] ) === FALSE ) {
								$google_tracking_6 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_6'].'\'] )"';
							}
						?>
						<a href="<?php echo $gg_options[$prefix . 'link_6']; ?>" <?php echo $google_tracking_6;?>>
							<img src="<?php echo $gg_options[$prefix . 'image_6']; ?>">
						</a>
					</div>
				</div>
				<div class="rgg_full">
					<?php /* 7 */
						$parsed_url_7 = parse_url( $gg_options[$prefix . 'link_7'] );
						$google_tracking_7 = '';
						if( $gg_options[$prefix . 'link_7'] != '' && strpos( home_url(), $parsed_url_7['host'] ) === FALSE ) {
							$google_tracking_7 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_7'].'\'] )"';
						}
					?>
					<a href="<?php echo $gg_options[$prefix . 'link_7']; ?>" <?php echo $google_tracking_7;?>>
						<img src="<?php echo $gg_options[$prefix . 'image_7']; ?>">
					</a>
				</div>
				<div class="rgg_row">
					<div class="rgg_one_sixth rgg_module">
						<?php /* 8 */
							$parsed_url_8 = parse_url( $gg_options[$prefix . 'link_8'] );
							$google_tracking_8 = '';
							if( $gg_options[$prefix . 'link_8'] != '' && strpos( home_url(), $parsed_url_8['host'] ) === FALSE ) {
								$google_tracking_8 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_8'].'\'] )"';
							}
						?>
						<a href="<?php echo $gg_options[$prefix . 'link_8']; ?>" <?php echo $google_tracking_8;?>>
							<img src="<?php echo $gg_options[$prefix . 'image_8']; ?>">
						</a>
					</div>
					<div class="rgg_one_sixth rgg_right_module">
						<?php /* 9 */
							$parsed_url_9 = parse_url( $gg_options[$prefix . 'link_9'] );
							$google_tracking_9 = '';
							if( $gg_options[$prefix . 'link_9'] != '' && strpos( home_url(), $parsed_url_9['host'] ) === FALSE ) {
								$google_tracking_9 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_9'].'\'] )"';
							}
						?>
						<a href="<?php echo $gg_options[$prefix . 'link_9']; ?>" <?php echo $google_tracking_9;?>>
							<img src="<?php echo $gg_options[$prefix . 'image_9']; ?>">
						</a>
					</div>
				</div>

			</div>
			<div class="rgg_one_third rgg_right_module">
				<div class="rgg_full">
					<?php /* 10 */
						$parsed_url_10 = parse_url( $gg_options[$prefix . 'link_10'] );
						$google_tracking_10 = '';
						if( $gg_options[$prefix . 'link_10'] != '' && strpos( home_url(), $parsed_url_10['host'] ) === FALSE ) {
							$google_tracking_10 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_10'].'\'] )"';
						}
					?>
					<a href="<?php echo $gg_options[$prefix . 'link_10']; ?>" <?php echo $google_tracking_10;?>>
						<img src="<?php echo $gg_options[$prefix . 'image_10']; ?>">
					</a>
				</div>
				<div class="rgg_row">
					<div class="rgg_one_sixth rgg_module">
						<?php /* 11 */
							$parsed_url_11 = parse_url( $gg_options[$prefix . 'link_11'] );
							$google_tracking_11 = '';
							if( $gg_options[$prefix . 'link_11'] != '' && strpos( home_url(), $parsed_url_11['host'] ) === FALSE ) {
								$google_tracking_11 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_11'].'\'] )"';
							}
						?>
						<a href="<?php echo $gg_options[$prefix . 'link_11']; ?>" <?php echo $google_tracking_11;?>>
							<img src="<?php echo $gg_options[$prefix . 'image_11']; ?>">
						</a>
					</div>
					<div class="rgg_one_sixth rgg_right_module rgg_nomargin">
						<div class="rgg_full">
							<?php /* 12 */
								$parsed_url_12 = parse_url( $gg_options[$prefix . 'link_12'] );
								$google_tracking_12 = '';
								if( $gg_options[$prefix . 'link_12'] != '' && strpos( home_url(), $parsed_url_12['host'] ) === FALSE ) {
									$google_tracking_12 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_12'].'\'] )"';
								}
							?>
							<a href="<?php echo $gg_options[$prefix . 'link_12']; ?>" <?php echo $google_tracking_12;?>>
								<img src="<?php echo $gg_options[$prefix . 'image_12']; ?>">
							</a>
						</div>
						<div class="rgg_full">
							<?php /* 13 */
								$parsed_url_13 = parse_url( $gg_options[$prefix . 'link_13'] );
								$google_tracking_13 = '';
								if( $gg_options[$prefix . 'link_13'] != '' && strpos( home_url(), $parsed_url_13['host'] ) === FALSE ) {
									$google_tracking_13 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_13'].'\'] )"';
								}
							?>
							<a href="<?php echo $gg_options[$prefix . 'link_13']; ?>" <?php echo $google_tracking_13;?>>
								<img src="<?php echo $gg_options[$prefix . 'image_13']; ?>">
							</a>
						</div>
					</div>
					<div class="rgg_full">
						<?php /* 14 */
							$parsed_url_14 = parse_url( $gg_options[$prefix . 'link_14'] );
							$google_tracking_14 = '';
							if( $gg_options[$prefix . 'link_14'] != '' && strpos( home_url(), $parsed_url_14['host'] ) === FALSE ) {
								$google_tracking_14 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_14'].'\'] )"';
							}
						?>
						<a href="<?php echo $gg_options[$prefix . 'link_14']; ?>" <?php echo $google_tracking_14;?>>
							<img src="<?php echo $gg_options[$prefix . 'image_14']; ?>">
						</a>
					</div>
				</div>
			</div>

		</div>
		<div class="rgg_last_third">
		<?php
	 // if ( is_mobile() && in_category( array( '2015 Running Gear Guide', '2015-running-gear-guide' ) )  && is_category( '2015-running-gear-guide' ) )

		//{ ?>
			<div class="rgg_ad_col">
				<h4>Advertisement</h4>
				<?php
					// if ( function_exists( 'dynamic_sidebar' ) && dynamic_sidebar( 'atf-300x250' ) ) : else : endif;
					echo class_exists( 'Wp_Dfp_Ads' ) ? Wp_Dfp_Ads::display_ad( 'side-top' ) : '';
				?>
			</div>
			<?php //}  ?>
			<!-- ad unit -->
			<div class="rgg_row">
				<div class="rgg_full">
					<?php /* 15 */
						$parsed_url_15 = parse_url( $gg_options[$prefix . 'link_15'] );
						$google_tracking_15 = '';
						if( $gg_options[$prefix . 'link_15'] != '' && strpos( home_url(), $parsed_url_15['host'] ) === FALSE ) {
							$google_tracking_15 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_15'].'\'] )"';
						}
					?>
					<a href="<?php echo $gg_options[$prefix . 'link_15']; ?>" <?php echo $google_tracking_15;?>>
						<img src="<?php echo $gg_options[$prefix . 'image_15']; ?>">
					</a>
				</div>
				<div class="rgg_full">
					<div class="rgg_one_sixth rgg_module">
						<?php /* 16 */
							$parsed_url_16 = parse_url( $gg_options[$prefix . 'link_16'] );
							$google_tracking_16 = '';
							if( $gg_options[$prefix . 'link_16'] != '' && strpos( home_url(), $parsed_url_16['host'] ) === FALSE ) {
								$google_tracking_16 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_16'].'\'] )"';
							}
						?>
						<a href="<?php echo $gg_options[$prefix . 'link_16']; ?>" <?php echo $google_tracking_16;?>>
							<img src="<?php echo $gg_options[$prefix . 'image_16']; ?>">
						</a>
					</div>
					<div class="rgg_one_sixth rgg_module rgg_right_module">
						<?php /* 17 */
							$parsed_url_17 = parse_url( $gg_options[$prefix . 'link_17'] );
							$google_tracking_17 = '';
							if( $gg_options[$prefix . 'link_17'] != '' && strpos( home_url(), $parsed_url_17['host'] ) === FALSE ) {
								$google_tracking_17 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_17'].'\'] )"';
							}
						?>
						<a href="<?php echo $gg_options[$prefix . 'link_17']; ?>" <?php echo $google_tracking_17;?>>
							<img src="<?php echo $gg_options[$prefix . 'image_17']; ?>">
						</a>
					</div>
				</div>
			</div>
			<div class="rgg_right_module_2">
				<div class="rgg_full">
					<?php /* 18 */
						$parsed_url_18 = parse_url( $gg_options[$prefix . 'link_18'] );
						$google_tracking_18 = '';
						if( $gg_options[$prefix . 'link_18'] != '' && strpos( home_url(), $parsed_url_18['host'] ) === FALSE ) {
							$google_tracking_18 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_18'].'\'] )"';
						}
					?>
					<a href="<?php echo $gg_options[$prefix . 'link_18']; ?>" <?php echo $google_tracking_18;?>>
						<img src="<?php echo $gg_options[$prefix . 'image_18']; ?>">
					</a>
				</div>
				<div class="rgg_row">
					<div class="rgg_one_sixth rgg_module">
						<?php /* 19 */
							$parsed_url_19 = parse_url( $gg_options[$prefix . 'link_19'] );
							$google_tracking_19 = '';
							if( $gg_options[$prefix . 'link_19'] != '' && strpos( home_url(), $parsed_url_19['host'] ) === FALSE ) {
								$google_tracking_19 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_19'].'\'] )"';
							}
						?>
						<a href="<?php echo $gg_options[$prefix . 'link_19']; ?>" <?php echo $google_tracking_19;?>>
							<img src="<?php echo $gg_options[$prefix . 'image_19']; ?>">
						</a>
					</div>
					<div class="rgg_one_sixth rgg_right_module rgg_nomargin">
						<div class="rgg_full">
							<?php /* 20 */
								$parsed_url_20 = parse_url( $gg_options[$prefix . 'link_20'] );
								$google_tracking_20 = '';
								if( $gg_options[$prefix . 'link_20'] != '' && strpos( home_url(), $parsed_url_20['host'] ) === FALSE ) {
									$google_tracking_20 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_20'].'\'] )"';
								}
							?>
							<a href="<?php echo $gg_options[$prefix . 'link_20']; ?>" <?php echo $google_tracking_20;?>>
								<img src="<?php echo $gg_options[$prefix . 'image_20']; ?>">
							</a>
						</div>
						<div class="rgg_full">
							<?php /* 21 */
								$parsed_url_21 = parse_url( $gg_options[$prefix . 'link_21'] );
								$google_tracking_21 = '';
								if( $gg_options[$prefix . 'link_21'] != '' && strpos( home_url(), $parsed_url_21['host'] ) === FALSE ) {
									$google_tracking_21 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_21'].'\'] )"';
								}
							?>
							<a href="<?php echo $gg_options[$prefix . 'link_21']; ?>" <?php echo $google_tracking_21;?>>
								<img src="<?php echo $gg_options[$prefix . 'image_21']; ?>">
							</a>
						</div>
					</div>

				</div>
			</div>
		</div>
	<?php } else {
		$page = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; ?>
		<div class="left_column">
			<div>
				<?php
					$page = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
					echo '<h1><img class="slashes slash_blue" src="'.get_bloginfo( 'stylesheet_directory' ).'/images/running/running-slashes-blue.svg"> All Posts Categorized &#8216;' . single_cat_title( '', false ) . '&#8217;: Page ' . $page . '</h1>';
					$count = 1;
					if ( have_posts() ) : while ( have_posts() ) : the_post();
						$permalink = get_permalink();
						$title = get_the_title();
						$excerpt = get_the_excerpt();
						$post_classes = get_post_class();

						if ( has_post_thumbnail () ) {
							if( $is_mobile ){
								$image_array = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'marquee-carousel-mobile' );
							} else {
								if( $count == 1 ) {
									$image_array = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
								} else {
									$image_array = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'thumbnail' );
								}
							}
							$post_thumb = $image_array[0];
						} else {
							if( $count == 1 ) {
								$post_thumb = get_bloginfo( 'stylesheet_directory' ).'/images/running/default-post-thumbnail-lg.jpg';
							} else {
								$post_thumb = get_bloginfo( 'stylesheet_directory' ).'/images/running/default-post-thumbnail-sm.jpg';
							}
						}
						$post_classes_string = implode( " ", $post_classes );

						$month_published = get_the_time( 'M' );
						$day_published = get_the_time( 'd' );
						if ( $count == 1 && ( get_query_var( 'paged' ) < 3 ) ) {  // first post is a big one
							echo '
								<div class="featured_post '.$post_classes_string.'">
									<div class="featured_img">
										<a class="overlay" href="'.$permalink.'"></a>
										<img src="'.$post_thumb.'">
									</div>
									<div class="row featured_vitals">
										<div class="featured_post_date">
											<span class="featured_post_month">'.$month_published.'</span>
											<span class="featured_post_day">'.$day_published.'</span>
										</div>
										<div class="featured_title_author">
											<h3><a href="'.$permalink.'">'.$title.'</a></h3>
											<p class="post_author">by '.get_guest_author().'</p>
											<p class="excerpt">'.$excerpt.'</p>
										</div>
										<a class="arrow" href="'.$permalink.'"><span class="icon-right-open"></span></a>
									</div>
								</div>
							';
						} else {
							$thumb = '<div class="featured_img_archive"><a class="overlay" href="' . $permalink . '" title="' . $title . '"></a>' . '<img class="archive_thumb" src="'.$post_thumb.'"></div>';
							echo '
								<div class="row archive_row '.$post_classes_string.'">
									'.$thumb.'
									<h4><a href="'.$permalink.'">'.$title.'</a></h4>
									<p class="post_author">by '.get_guest_author().', '.format_timestamp_2().'</p>
									<p class="excerpt">'.$excerpt.'</p>
								</div>
							';
						}
						$count++;

					endwhile; else: ?>
						<div class="content">
							<p>Sorry, no posts matched your criteria.</p>
						</div>
					<?php endif; ?>
			</div>
			<?php
				echo '<div class="row archive_navi">
					<div class="btn_primary">
						'.get_previous_posts_link( '<span class="icon-left-open"></span> Previous Page' ).'
					</div>
					<div class="btn_primary">
						'.get_next_posts_link( 'Next Page <span class="icon-right-open"></span>' ).'
					</div>
				</div>';
			?>
		</div><!-- row 1/content -->
		<?php get_sidebar();
	} ?>
</div>

<?php /*
<div class="main clearfix">
	<?php $this_category = get_category( $cat );

	if( !( get_query_var( 'paged' ) > 1 ) ) {
		$parsed_url_2 = parse_url( $gg_options[$prefix . 'link_2'] );
		$google_tracking_2 = '';
		if( $gg_options[$prefix . 'link_2'] != '' && strpos( home_url(), $parsed_url_2['host'] ) === FALSE ) {
			$google_tracking_2 = 'onclick="_gaq.push( [\'_trackEvent\', \'2015 Running Gear Guide\', \'running\', \''. $gg_options[$prefix . 'link_2'].'\'] )"';
		} ?>
		<div class="float-left buyers-guide-image image-2">
			<a href="<?php echo $gg_options[$prefix . 'link_2']; ?>" <?php echo $google_tracking_2;?>>
				<img src="<?php echo $gg_options[$prefix . 'image_2']; ?>" />
			</a>
		</div>
	<?php } else {
		$page = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		echo '<h1>All ' . single_cat_title( '', false ) . ': Page ' . $page . '</h1>';
		echo '<ul>';

			if ( have_posts() ) : while ( have_posts() ) : the_post();
				if ( !in_array( get_the_ID(), $post_id_array ) ) {
					$thumb = '';
					$title = get_short_title();
					$url = get_permalink();
					$author = get_guest_author();
					if ( has_post_thumbnail () ) {
						$image_array = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'thumbnail' );
						$thumb = '<a class="overlay" href="' . $url . '" title="' . $title . '"></a>' . '<img src="'.$image_array[0].'" />';
					} else {
						$test = get_post_meta( $post->ID, "thumb", true );
						if( $test != "" ){
							$thumb = '<a class="overlay" href="' . $url . '" title="' . $title . '"></a>' . '<img src="' . get_post_meta( $post->ID, "thumb", true ) . '" alt="' . $title . '" />';
						}
					}
					if( $thumb == "" || $thumb == '<img src="">' ) {
						$thumb = "";
						$attachments = array();
						$attachment_id = '';
						$attachments = get_children( array(
							'post_parent' => $post->ID,
							'post_status' => 'inherit',
							'post_type' => 'attachment',
							'post_mime_type' => 'image',
							'orderby' => 'menu_order',
							'order'=> 'ASC'
						 ) );
						foreach( $attachments as $attachment ){
							$attachment_id = $attachment->ID;
							break;
						}
						$image_array = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
						if( count( $image_array ) != 0 ){
							if( $image_array[0] != "" ){
								$thumb = '<a class="overlay" href="' . $url . '" title="' . $title . '"></a>' . '<img src="' .$image_array[0]. '" />';
							}
						}
					} ?>
					<li <?php echo post_class(); ?>>
						<?php echo $thumb;
						echo '<h2><a title="' . $title . '" href="' . $url . '">' . $title . '</a></h2>';
						echo '<ul>';
							if( !is_author() ){
								echo '<li>By ' . $author . '</li>';
							}
						echo '</ul>';
						echo '<p>', get_the_excerpt(), '</p>';
					echo '</li>';
				}
			endwhile; else:
				echo '<li>Sorry, no posts match the criteria requested.</li>';
			endif;
		echo '</ul>';
		echo '<span id="page-nav">';
			previous_posts_link( '&laquo; Previous Page' );
			next_posts_link( 'Next Page &raquo;' );
		echo '</span>';
	} ?>
</div>
*/ ?>
<?php get_footer(); ?>
