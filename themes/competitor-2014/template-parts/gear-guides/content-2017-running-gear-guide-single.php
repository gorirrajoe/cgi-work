<?php
	// single.php will handle displaying posts, attachments and custom posttypes
	// global $posts;
	// global $page;
	// global $multipage;
	// global $Appid;

	$gg_options	= GearGuide_Admin::get_guide_option( 2017 , 'all' );
	$prefix		= 'gear_guide_2017_';

	// add specific classes to the article tag so they can be used for styling the page correctly
	$classes	= array();

	$content_string	=  apply_filters( 'the_content', get_the_content() );
	$content_string	= preg_replace( "/<img[^>]+\>/i", " ", $content_string );
	$content_string	= preg_replace( "/\[sig:[a-zA-Z]+\]/", " ", $content_string );

	$page			= ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;
?>

<style>
	.rgg_banner {
		margin:2.375em 0 1.25em;
		text-align:center;
		background-color:#000;
	}
	.rgg_full {
		text-align:center;
		margin-bottom:1.25em;
	}
	.rgg_row {
		overflow:hidden;
		margin:auto;
	}
	.rgg_one_third.rgg_right_module {
		margin-left:0;
	}
	.rgg_ad_col,
	.rgg_ad_col_2 {
		background-color:#eee;
		text-align:center;
		padding:2em 0;
		margin-bottom:1.25em;
	}
	.rgg_ad_col_2 .ad {
		display:inline-block;
	}
	.rgg_one_third,
	.rgg_module {
		overflow:hidden;
	}
	.rgg_ad_col h4 {
		padding:0 0 1.400em;
	}
	.rgg_one_third .rgg_one_sixth,
	.rgg_last_third .rgg_one_sixth {
		text-align:center;
		margin-bottom:1.25em;
	}
	#div-300_600_top_300_250 {
		display:table;
		margin:auto;
	}
	.gear-guide-slider-container {
		clear:both;
		margin-bottom:.938em;
		padding-bottom:.938em;
		border-bottom:1px solid #a7a9ac;
	}
	/* single */
	.rgg_one_half {
		text-align:center;
	}
	.rgg_last_half h2 {
		padding-top: 0.714em;
	}
	.slide h3 {
		padding:0.455em 0 0 0;
	}
	.owl-theme .owl-controls .owl-buttons div {
		position:relative;
	}
	.owl-theme .owl-controls {
		margin:0 0 .625em 0;
		overflow:hidden;
	}
	.rgg_leftnav,
	.owl-prev {
		float:left;
	}
	.rgg_rightnav,
	.owl-next {
		float:right;
	}
	.more-gear-slider-container h2 {
		padding-top:0;
	}
	.slide {
		text-align:center;
	}
	.owl-carousel .owl-item {
		padding:0 .625em;
	}
	.rgg_last_half h3 {
		padding-top:0;
	}
	.rgg_clear {
		clear:both;
	}
	@media only screen and ( min-width:440px ) {
		.rgg_one_third .rgg_one_sixth,
		.rgg_last_third .rgg_one_sixth {
			float:left;
			margin-bottom:1.25em;
		}
		.rgg_right_module {
			margin-left:1.25em;
		}
		.rgg_row {
			display:table;
		}
	}
	@media only screen and ( min-width:990px ) {
		.rgg_container {
			max-width:830px;
			margin:auto;
		}
		.rgg_banner {
			margin:2.375em 0 1.875em;
		}
		.rgg_full,
		.rgg_one_third .rgg_one_sixth,
		.rgg_last_third .rgg_one_sixth {
			margin-bottom:1.875em;
		}
		.rgg_one_third .rgg_one_sixth,
		.rgg_last_third .rgg_one_sixth {
			width:46.25%;
		}
		.rgg_one_third {
			float:left;
			width:48.19277108433735%;
		}
		.rgg_one_third.rgg_right_module {
			float:left;
			margin-left:1.875em;
		}
		.rgg_ad_col,
		.rgg_ad_col_2 {
			margin-bottom:1.875em;
		}
		.rgg_right_module {
			margin-left:1.875em;
		}
		.rgg_last_third .rgg_row {
			float:left;
			width:48.19277108433735%;
			display:block;
		}
		.owl-carousel .owl-item {
			padding:0 .938em;
		}
	}
	@media only screen and ( min-width:1300px ) {
		.rgg_container {
			max-width:1260px;
		}
		.rgg_banner {
			margin:2.375em 0 1.875em;
		}
		.rgg_two_thirds {
			float:left;
			width:65.87301587301587%;
		}
		.rgg_last_third {
			float: left;
			margin-left: 1.875em;
			width: 31.746%;
		}
		.rgg_last_third .rgg_row {
			width:auto;
			float:none;
			margin-left:0;
		}
		.rgg_one_half,
		.rgg_last_half {
			float:left;
			width:48.80952380952381%;
		}
		.rgg_last_half {
			margin-left:1.875em;
		}
		.rgg_ad_col {
			padding:2em 0 5.5em;
		}
		.rgg_last_half h2 {
			padding-top:0;
		}
	}
</style>

<div class="rgg_container">
	<?php if( !empty( $gg_options[$prefix .'banner_image'] ) ): ?>
		<div class="rgg_banner">
			<a href="<?php echo site_url( '/category/2017-running-gear-guide/' ); ?>"><img src="<?php echo $gg_options[$prefix .'banner_image']; ?>"></a>
		</div>
	<?php endif; ?>

	<h1>
		<img class="slashes slash_blue" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/images/running/running-slashes-blue.svg">
		<?php echo strip_tags( the_title( '','',false ) ); ?>
	</h1>

	<?php
		if( get_post_meta( get_the_ID(), 'guest_author', 1 ) != '' ) {
			echo '<p class="post_author">by '. get_guest_author() .', '.format_timestamp_2().'</p>';
		}


		$prev_next_links = wp_link_pages( array(
			'before'			=> '<div class="row rgg_navigation">',
			'after'				=> '</div>',
			'next_or_number'	=> 'next',
			'previouspagelink'	=> '<span class="icon-left-open"></span> Previous',
			'nextpagelink'		=> 'Next <span class="icon-right-open"></span>',
			'echo'				=> 0
		 ) );

		// count how many anchor tags there are, max will be 2 ( next, prev )
		$anchor_count = substr_count( $prev_next_links, '<a' );

		if( $anchor_count != 2 ) {  // if there are less than 2, give it a class for styling
			$is_left_link = strpos( $prev_next_links, 'Previous' );

			if( $is_left_link !== false ) {
				$prev_next_links = str_replace( '<a href=', '<div class="btn_primary"><a class="rgg_leftnav" href=', $prev_next_links );
				$prev_next_links = str_replace( '</a>', '</a></div>', $prev_next_links );
			}

			$is_right_link = strpos( $prev_next_links, 'Next' );

			if( $is_right_link !== false ) {
				$prev_next_links = str_replace( '<a href=', '<div class="btn_primary"><a class="rgg_rightnav" href=', $prev_next_links );
				$prev_next_links = str_replace( '</a>', '</a></div>', $prev_next_links );
			}
		} elseif( $anchor_count == 2 ) {  // if there are 2, style them like so
			echo '<style>
				.rgg_navigation div:first-child {
					float:left;
				}
				.rgg_navigation div:nth-child( 2 ) {
					float:right;
				}
			</style>';

			$prev_next_links	= str_replace( '<a href=', '<div class="btn_primary"><a href=', $prev_next_links );
			$prev_next_links	= str_replace( '</a>', '</a></div>', $prev_next_links );
		}

		echo $prev_next_links;


		$attachments = get_children( array(
			'post_parent'		=> get_the_ID(),
			'post_status'		=> 'inherit',
			'post_type'			=> 'attachment',
			'post_mime_type'	=> 'image',
			'orderby'			=> 'menu_order',
			'order'				=> 'ASC'
		 ) );

		$count = 1;
		$attachment_count = count( $attachments );

		foreach( $attachments as $attachment ) {

			$attachment_id = $attachment->ID;
			if( $page == $count ) {
				$attachment_id = $attachment->ID;
				break;
			}
			$count++;
		}

		if( is_mobile() ) {
			$image_array = wp_get_attachment_image_src( $attachment_id, 'medium' );
		} else{
			$image_array = wp_get_attachment_image_src( $attachment_id, 'full' );
		}
		$thumbnail_image	= get_post( $attachment_id );
		$thumbnail_caption	= "";
		$thumbnail_title	= "";

		if ( !empty( $thumbnail_image ) ) {
			$thumbnail_caption	= $thumbnail_image->post_excerpt;
			$thumbnail_title	= $thumbnail_image->post_title;
		}

		echo '<div class="rgg_one_half">
			<img title="'.$thumbnail_title.'" src="'.$image_array[0].'">
		</div>

		<div class="rgg_last_half">';

			$pagetitlestring = '/<!--pagetitle:(.*?)-->/';
			preg_match_all( $pagetitlestring, $posts[0]->post_content, $titlesarray, PREG_PATTERN_ORDER );
			$pagetitles = $titlesarray[1];

			echo '<h2>'.$pagetitles[$page - 1].'</h2>' .

			$content_string;


			if( is_mobile() && in_category( array( '2017 Running Gear Guide', '2017-running-gear-guide' ) ) ) {
				echo '<div class="rgg_ad_col_2">';
					echo class_exists( 'Wp_Dfp_Ads' ) ? Wp_Dfp_Ads::display_ad( 'side-top' ) : '';
				echo '</div>';
			}

		echo '</div>


		<div class="gear-guide-slider-container">
			<div class="owl-carousel" id="gear-guide-slider">';
				$page_count = 1;

				foreach( $attachments as $attachment ) {
					if( $page_count != $attachment_count ) {  // show all attachments but last one ( featured img )
						$attachment_id		= $attachment->ID;
						$image_array_thumb	= wp_get_attachment_image_src( $attachment_id, 'square-gift-guide-product' );

						echo '<div class="slide">
							<a href="'.get_permalink().'/'.$page_count.'"><img src="'.$image_array_thumb[0].'"/></a>
							<h3><a href="'.get_permalink().'/'.$page_count.'">'.$pagetitles[$page_count - 1].'</a></h3>
						</div>';
					}
					$page_count++;
				}
			echo '</div>
		</div>


		<div class="more-gear-slider-container">
			<h2>More</h2>
			<div class="owl-carousel" id="more-gear-slider">';
				$gear_count	= 1;
				$gear_array	= array();

				for( $i = 0; $i < 63; $i++ ) {
					if( $gear_count > 21 ) {
						break;
					} else {
						$random = rand( 1, 20 );

						if( !in_array( $random, $gear_array ) ) {
							$rgg_option_image	= $prefix . 'more_gear_image_'.$random;
							$rgg_option_link	= $prefix . 'link_'.$random;

							if( array_key_exists( $rgg_option_image, $gg_options ) && $gg_options[$rgg_option_image] != '' ) {

								// $parsed_url			= parse_url( $rgg_option_link );
								// $google_tracking	= '';

								// if( $rgg_option_link != '' && strpos( home_url(), $parsed_url['host'] ) === FALSE ) {
								// 	$google_tracking = 'onclick="_gaq.push( [\'_trackEvent\', \'2016 Running Gear Guide\', \'running\', \''. $gg_options[$rgg_option_link].'\'] )"';
								// }

								// echo '<div class="slide">
								// 	<a href="'.$gg_options[$rgg_option_link].'" '.$google_tracking.'><img src="'.$gg_options[$rgg_option_image].
								// 	'"/></a>
								// </div>';

								echo '<div class="slide">
									<a href="'.$gg_options[$rgg_option_link].'"><img src="'.$gg_options[$rgg_option_image].
									'"/></a>
								</div>';
							}

							$gear_array[] = $random;
							$gear_count++;
						}
					}
				}

			echo '</div>
		</div>';
	?>
</div>


<script>
	jQuery( document ).ready( function( $ ) {
		$( "#gear-guide-slider" ).owlCarousel( {
			navigation:true,
			navigationText:false,
			pagination:false,
			itemsCustom:[
				[0,1],
				[480,2],
				[990,3],
				[1300,5]
			]
		} );
		$( "#more-gear-slider" ).owlCarousel( {
			navigation:true,
			navigationText:false,
			pagination:false,
			itemsCustom:[
				[0,1],
				[480,2],
				[990,3]
			]
		} );
	} );
</script>
