<?php
	/**
	 * Template part for displaying posts in photos category (category defined in admin area)
	 */
?>
<article <?php post_class( array( 'col-xs-12', 'article', 'article_type_single' ) ); ?>>

	<header class="article__header">
		<?php the_title( '<h1 class="article__header__title">', '</h1>' ); ?>
		<?php echo '<p class="post_author">By ' . get_guest_author() .', '. format_timestamp_2() .'</p>'; ?>
	</header>

	<?php if ( in_category( array( 'sponsored' ) ) ): ?>
		<h4 class="sponsored-label">Sponsored Content</h4>
	<?php endif; ?>

	<section class="article__body">
		<?php
			echo AMP_Mods::get_social_sharing();

			echo $this->get( 'post_amp_content' );

			echo AMP_Mods::get_social_sharing();
		?>
	</section>

</article>
