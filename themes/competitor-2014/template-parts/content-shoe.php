<?php
	/**
	 * Template part for displaying Show CPT single posts.
	 */

	$post_ID	= get_the_ID();

	$attachments = get_children( array(
		'post_parent'		=> $post_ID,
		'post_status'		=> 'inherit',
		'post_type'			=> 'attachment',
		'post_mime_type'	=> 'image',
		'orderby'			=> 'menu_order',
		'order'				=> 'ASC'
	) );

	$is_mobile = is_mobile();

	if ( count( $attachments ) > 1 ) {
		echo my_gallery_2( '', array(), 'medium', 'carousel' );
	} else {

		$image_ID = get_post_thumbnail_id( $post_ID );

		$image_size	= $is_mobile ? 'medium' : 'large';

		$image_array	= wp_get_attachment_image_src( $image_ID, $image_size );

		// define the title and caption
		$thumbnail_image	= get_post( $image_ID );
		$thumbnail_title	= $thumbnail_image->post_title != '' ? ' title="'. $thumbnail_image->post_title .'"' : '';
		$thumbnail_caption	= $thumbnail_image->post_excerpt != '' ? '<div class="figcaption">'. $thumbnail_image->post_excerpt .'</div>' : '';

		// bring it all together and display it
		printf(
			'<div class="figure shoe_image"><img %s src="'. $image_array[0] .'"/>%s</div>',
			$thumbnail_title,
			$thumbnail_caption
		);

	}

	$shoe_type			= get_post_meta( $post_ID, '_type', true ) ?: '';
	$gender				= get_post_meta( $post_ID, '_gender', true ) ?: '';
	$shoe_weight		= get_post_meta( $post_ID, '_shoe_weight', true ) ?: '';
	$heel_toe_drop		= get_post_meta( $post_ID, '_heel_toe_drop', true ) ?: '';
	$msrp				= get_post_meta( $post_ID, '_msrp', true ) ?: '';
	$website			= get_post_meta( $post_ID, '_website', true ) ?: '';
	$description		= get_post_meta( $post_ID, '_description', true ) ?: '';
	$buy_now			= get_post_meta( $post_ID, '_buy_now', true ) ?: '';
	$buy_now_image		= get_post_meta( $post_ID, '_buy_now_button_image', true ) ?: '';
	$buy_now_2			= get_post_meta( $post_ID, '_buy_now_2', true ) ?: '';
	$buy_now_image_2	= get_post_meta( $post_ID, '_buy_now_button_image_2', true ) ?: '';
	$buy_now_3			= get_post_meta( $post_ID, '_buy_now_3', true ) ?: '';
	$buy_now_image_3	= get_post_meta( $post_ID, '_buy_now_button_image_3', true ) ?: '';

	echo '<ul class="single_shoe_deets">';
		if ( $shoe_type != '' ) {
			echo '<li><strong>Shoe Type:</strong> '. $shoe_type . '</li>';
		}
		if ( $gender != '' ) {
			echo '<li><strong>Gender:</strong> '. ucwords($gender) . '</li>';
		}
		if ( $shoe_weight != '' ) {
			echo '<li><strong>Shoe Weight:</strong> '. $shoe_weight . '</li>';
		}
		if ( $heel_toe_drop != '' ) {
			echo '<li><strong>Heel Toe Drop:</strong> '. $heel_toe_drop . '</li>';
		}
		if ( $msrp != '' ) {
			echo '<li><strong>MSRP:</strong> $'. $msrp . '</li>';
		}
		if ( $website != '' ) {
			echo '<li><strong>Website:</strong> <a href="'. $website .'">'. $website . '</a></li>';
		}
		if ( $description != '' ) {
			echo '<li><strong>Additional Information:</strong> '. $description . '</li>';
		}
	echo '</ul>';

	if ( $buy_now != '' && $buy_now_image != '' ) { ?>
		<img class="buy-now-image" src="<?php echo $buy_now_image;?>" onclick="window.open( '<?php echo $buy_now;?>', '_blank');" title="BUY NOW"/>
	<?php }
	if ( $buy_now_2 != '' && $buy_now_image_2 != '' ) { ?>
		<img class="buy-now-image" src="<?php echo $buy_now_image_2;?>" onclick="window.open( '<?php echo $buy_now_2;?>', '_blank');" title="BUY NOW"/>
	<?php }
	if ( $buy_now_3 != '' && $buy_now_image_3 != '' ) { ?>
		<img class="buy-now-image" src="<?php echo $buy_now_image_3;?>" onclick="window.open( '<?php echo $buy_now_3;?>', '_blank');" title="BUY NOW"/>
	<?php }

	// if form submitted
	$resultspage	= isset( $_REQUEST['resultspage'] ) ? $_REQUEST['resultspage'] : 1;

	// build the page query
	$page_query = '';

	$page_query	.= isset( $_REQUEST['shoe_type'] ) ? '&shoe_type='. $shoe_type : '';
	$page_query	.= isset( $_REQUEST['shoe_brand'] ) ? '&shoe_brand='. $shoe_brand : '';
	$page_query	.= isset( $_REQUEST['gender'] ) ? '&gender='. $gender : '';
	$page_query	.= isset( $_REQUEST['model_name'] ) ? '&model_name='. $model_name : '';
	$page_query	.= isset( $_REQUEST['price_range'] ) ? '&price_range='. $price_range : '';
	$page_query	.= isset( $_REQUEST['model_released'] ) ? '&model_released='. $model_released : '';

	$link = '?resultspage='. $resultspage . $page_query;

	echo '<div class="row shoes_navi_row">
		<div class="btn_primary"><a class="prev-post" href="'.site_url().'/shoe-directory'.$link.'">Back to Search Results</a></div>
		<div class="btn_primary"><a class="next-post" href="'.site_url().'/shoe-directory">Browse Shoe Directory</a></div>
	</div>';


	// perform a query based on the name that gets posts based on the tag Running Shoe Review
	$args	= array(
		's'		=> get_the_title( $post_ID ),
		'tag'	=> 'running-shoe-review',
	);
	$search_query = new WP_Query( $args );
	if ( $search_query->have_posts() ) {
		echo '<div class="single_recommended_feed">';
		echo '<h3><img class="slashes" src="'.get_bloginfo('stylesheet_directory').'/images/running/running-slashes-blue.png"> Related <span class="hidden">Articles</span></h3>';

		while ( $search_query->have_posts() ) : $search_query->the_post();

			$permalink		= get_permalink();
			$title			= get_the_title();
			$excerpt		= get_the_excerpt();
			$post_classes	= get_post_class();

			$post_classes_string = implode( ' ', $post_classes );

			if ( has_post_thumbnail () ) {
				$thumbnail_ID	= get_post_thumbnail_id( $post_ID );
				$image_array	= wp_get_attachment_image_src( $thumbnail_ID, 'thumbnail');
				$thumb			= '<div class="featured_img_archive"><a class="overlay" href="'. $permalink .'" title="'. $title .'"></a><img class="archive_thumb" src="' .$image_array[0] .'"></div>';
			} else {
				$thumb = '';
			}
			echo '
				<div class="row archive_row '. $post_classes_string .'">
					'. $thumb .'
					<h4><a href="'. $permalink .'">'. $title .'</a></h4>
					<p class="post_author">by '. get_guest_author() .', '. format_timestamp_2() .'</p>
					<p class="excerpt">'. $excerpt .'</p>
				</div>
			';

		endwhile;
		echo '</div>';
		wp_reset_postdata();
	}
