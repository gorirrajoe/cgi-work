<?php
	/**
	 * Template part for displaying attachment posts.
	 */

	$image		= get_queried_object();

	$post_ID	= $image->ID;

	$is_mobile	= is_mobile();

	$image_size	= $is_mobile ? 'medium' : 'large';

	$image_array		= wp_get_attachment_image_src( $post_ID, $image_size );
	$image_array_full	= wp_get_attachment_image_src( $post_ID, 'full');

	// define the title and caption
	$thumbnail_title	= $image->post_title != '' ? ' title="'. $image->post_title .'"' : '';
	$thumbnail_caption	= $image->post_excerpt != '' ? '<figcaption>'. $image->post_excerpt .'</figcaption>' : '';

	$content_string	= apply_filters( 'the_content', get_the_content() );

	if ( $image->post_parent != 0 ) {
		$permalink		= get_permalink( $image->post_parent );
		echo '<div><a class="link-button" href="'. $permalink .'" onClick="_gaq.push([\'_trackEvent\', \'Image Attachment\', \'Back\', \'Back to the Story\']);">
			&lt;&lt; Back to Story</a>';
	}

	// bring it all together and display it
	printf(
		'<figure class="multi-page-image"><img %s src="'. $image_array[0] .'"/>%s</figure>',
		$thumbnail_title,
		$thumbnail_caption
	);

	echo '<a href="' . $image_array_full[0] . '" target="_blank" title="Full Size Image"
		onClick="_gaq.push([\'_trackEvent\', \'Image Attachment\', \'Enlarge\', \'See Full Size Image\']);">Open full size in new window.</a></div>';

	// in case someone tried to put an image in the content description of this attachment
	$content_string = preg_replace( "/<img[^>]+\>/i", ' ', $content_string );

	echo $content_string;
