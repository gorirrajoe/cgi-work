<?php
	/**
	 * Template part for displaying standard single posts on AMP.
	 */
	$post_ID	= get_the_ID();

	$attachments	= get_children( array(
		'post_parent'		=> $post_ID,
		'post_status'		=> 'inherit',
		'post_type'			=> 'attachment',
		'post_mime_type'	=> 'image',
		'orderby'			=> 'menu_order',
		'order'				=> 'ASC'
	) );

	$attachments_count			= count( $attachments );

	$yt_vid					= get_post_meta( $post_ID, '_youtube_id', true );
	$brightcove_id			= get_post_meta( $post_ID, 'video_id', true );
	$show_all_images		= get_post_meta( $post_ID, '_display_images', true ) ?: '';
	$featured_thumb_only	= get_post_meta( $post_ID, '_featured_thumb', true ) ?: '';
?>
<article <?php post_class( array( 'col-xs-12', 'article', 'article_type_single' ) ); ?>>

	<header class="article__header">
		<?php the_title( '<h1 class="article__header__title">', '</h1>' ); ?>
		<?php echo '<p class="post_author">By ' . get_guest_author() .', '. format_timestamp_2() .'</p>'; ?>
	</header>

	<?php if ( in_category( array( 'sponsored' ) ) ): ?>
		<h4 class="sponsored-label">Sponsored Content</h4>
	<?php endif; ?>

	<section class="article__body">
		<?php
			echo AMP_Mods::get_social_sharing();

			if ( $yt_vid != '' ) {

				printf(
					'<amp-youtube data-videoid="%s" height="270" width="480" layout="responsive"></amp-youtube>',
					$yt_vid
				);

			} elseif ( $brightcove_id != '' ) {

				printf(
					'<amp-brightcove
						data-account="3655502813001"
						data-player="r1oC9M1S"
						data-embed="default"
						data-video-id="%s"
						layout="responsive"
						width="480" height="270">
					</amp-brightcove>',
					$brightcove_id
				);

			} elseif ( $featured_thumb_only ) {
				// if this was checked on, do not display the featured image
			} elseif ( $attachments_count == 1 ){
				// first check to see how many attachments there are, if less than two then find the attachment and display it at the top of the article
				if ( has_post_thumbnail( $post_ID ) ) {
					$image_ID = get_post_thumbnail_id( $post_ID );
				} else {
					foreach ( $attachments as $attachment ) {
						$image_ID = $attachment->ID;
						break;
					}
				}

				$image_array	= wp_get_attachment_image_src( $image_ID, 'medium' );

				// Add vertical class if height is greater than the width
				$class	= $image_array[2] > $image_array[1] ? ' vertical-image' : '';

				// define the title and caption
				$thumbnail_image	= get_post( $image_ID );
				$thumbnail_title	= $thumbnail_image->post_title != '' ? ' title="'. $thumbnail_image->post_title .'"' : '';
				$thumbnail_caption	= $thumbnail_image->post_excerpt != '' ? '<div class="figcaption">'. $thumbnail_image->post_excerpt .'</div>' : '';

				// bring it all together and display it
				printf(
					'<div class="figure'. $class .'"><amp-img %s src="'. $image_array[0] .'" width="'. $image_array[1] .'" height="'. $image_array[2] .'" />%s</div>',
					$thumbnail_title,
					$thumbnail_caption
				);

			} elseif ( $attachments_count > 1 ) {

				// else if there is more than one attachment display the gallery carousel of all the images that are attached
				echo my_gallery_2( '', array(), 'medium', 'carousel' );

			} elseif ( has_post_thumbnail( $post_ID ) ) {

				$image_ID		= get_post_thumbnail_id( $post_ID );

				$image_array	= wp_get_attachment_image_src( $image_ID, 'medium' );

				// Add vertical class if height is greater than the width
				$class	= $image_array[2] > $image_array[1] ? ' vertical-image' : '';

				// define the title and caption
				$thumbnail_image	= get_post( $image_ID );
				$thumbnail_title	= $thumbnail_image->post_title != '' ? ' title="'. $thumbnail_image->post_title .'"' : '';
				$thumbnail_caption	= $thumbnail_image->post_excerpt != '' ? '<div class="figcaption">'. $thumbnail_image->post_excerpt .'</div>' : '';

				// bring it all together and display it
				printf(
					'<div class="figure'. $class .'"><amp-img %s src="'. $image_array[0] .'" width="'. $image_array[1] .'" height="'. $image_array[2] .'" />%s</div>',
					$thumbnail_title,
					$thumbnail_caption
				);

			}

			$the_content	= $this->get( 'post_amp_content' );

			echo $the_content;

			echo AMP_Mods::get_social_sharing();
		?>
	</section>

</article>
