<?php
	/**
	 * Template part for displaying a single post from the 2016-gift-guide category.
	 */
	$category_name		= get_the_category();
	$category_explode	= explode( ' ', $category_name[0]->name );
	$year				= $category_explode[0];
	$gg_url				= site_url( '/category/' . $category_name[0]->slug );

	$gg_options	= GiftGuide_Admin::get_guide_option( $year, 'all' );
	$page		= ( $page == 0 ) ? 1 : $page;
?>

<div class="main clearfix category-gift-guide-single giftguide__final">
	<div class="wrbg_container">

		<?php if( array_key_exists( 'gift_guide_'. $year .'_banner_image', $gg_options ) ) { ?>
			<div class="gg_banner">
				<a href="<?php echo $gg_url; ?>"><img src="<?php echo $gg_options['gift_guide_'. $year .'_banner_image']; ?>" /></a>
			</div>
		<?php } ?>


		<article>
			<header>
				<h1 class="archive-labels">
					<?php echo $category_name[0]->name; ?>
				</h1>


				<?php
					/**
					 * nav
					 */
					/*$args = array(
						'menu'				=> strtolower( $category_name[0]->category_nicename ) .'-submenu',
						'menu_class'		=> 'gg-submenu__wrapper',
						'container_class'	=> 'gg-submenu',
						'fallback_cb'		=> false,
						'walker'			=> new GG_Walker_Nav_Menu(),
						'echo'				=> false,
						'theme_location'	=> $category_name[0]->category_nicename,
					);
					$navi = wp_nav_menu( $args );

					if( $navi != '' ) {

						echo '<div class="gg-submenu__label">Categories <span class="icon-down-open"></span></div>';

						echo $navi;

					}*/
				?>
			</header>

			<div id="article-left">

				<div id="share-links-left" >
					<?php print_social_media( 'left' ); ?>
				</div>

			</div>

			<div id="article-right">

				<div id="share-links">
					<div id="share-links-inner">

						<?php print_social_media( 'top' ); ?>

					</div>
				</div>

				<div class="gg-row">

					<div class="gg-two-thirds">

						<?php // page navigation
							$prev_next_links = wp_link_pages( array(
								'before'			=> '<div class="wrbg_navigation">',
								'after'				=> '</div>',
								'next_or_number'	=> 'next',
								'previouspagelink'	=> 'prev',
								'nextpagelink'		=> 'next',
								'echo'				=> 0,
								'theme_location'	=> '',
							 ) );

							$prev_next_links = str_replace( '>prev', ' class="wrbg_leftnav icon icon-angle-left">', $prev_next_links );
							$prev_next_links = str_replace( '>next', ' class="wrbg_rightnav icon icon-angle-right">', $prev_next_links );

							echo $prev_next_links;


							$attachments = get_children( array(
								'post_parent'		=> get_the_ID(),
								'post_status'		=> 'inherit',
								'post_type'			=> 'attachment',
								'post_mime_type'	=> 'image',
								'orderby'			=> 'menu_order',
								'order'				=> 'ASC'
							 ) );
							$count				= 1;
							$attachment_count	= count( $attachments );

							foreach( $attachments as $attachment ) {
								$attachment_id = $attachment->ID;

								if( $page == $count ) {
									$attachment_id = $attachment->ID;
									break;
								}

								$count++;
							}


							echo '<h2>'. strip_tags( the_title( '', '', false ) ) .'</h2>';

							$content_string	= apply_filters( 'the_content', get_the_content() );
							$content_string	= preg_replace( "/\[sig:[a-zA-Z]+\]/", " ", $content_string );
							$content_string	= preg_replace( "/\[imagebrowser id=[0-9]+\]/", " ", $content_string );

							echo $content_string;


							/**
							 * reviewer, rating, buy button row
							 */
							$reviewer_id	= get_post_meta( get_the_ID(), '_gg_author_id', 1 ) != '' ? get_post_meta( get_the_ID(), '_gg_author_id', 1 ) : '';
							$rating			= get_post_meta( get_the_ID(), '_gg_product_rating', 1 ) != 'none' ? get_post_meta( get_the_ID(), '_gg_product_rating', 1 ) : '';


							if( $reviewer_id != '' || $rating != '' ) {
								echo '<div class="gg-extra-row">';
							}


							if( $reviewer_id != '' ) {

								$author_meta	= get_user_meta( $reviewer_id );
								$author_name	= get_the_author_meta( 'display_name', $reviewer_id );

								$reviewer_output = '<div class="gg-column">
									<div class="reviewer_meta">';

										$template_directory		= get_template_directory_uri();
										$author_image			= get_template_directory() .'/images/authors/'. $reviewer_id .'.jpg';
										$author_image_display	= $template_directory .'/images/authors/'. $reviewer_id .'.jpg';

										if ( file_exists( $author_image ) ) {
											$reviewer_output .= '<img class="reviewer_meta__image" src="'. $author_image_display .'" alt="'. $author_name .'">';
										} else {
											$reviewer_output .= get_avatar( $reviewer_id, 120, '', $author_name, array( 'class' => 'reviewer_meta__image' ) );
										}

										$reviewer_output .= '<p class="reviewer_meta__name"><strong>Reviewed by:</strong><br>' .
										$author_name . '</p>
										<p class="reviewer_meta__bio">'. $author_meta['description'][0] .'</p>

									</div>
								</div>';

								echo $reviewer_output;

							}


							echo '<div class="gg-column">';

								if( $rating != '' ) {

									$stars = '<div class="stars_rating">';

										$total		= 5;
										$floor		= floor( $rating );
										$ceiling	= ceil( $rating );
										$remaining	= $total - $ceiling;

										if( strpos( $rating, '.' ) !== false ) {

											for( $i = 0; $i < $floor; $i++ ) {
												$stars .= '<span class="icon-star"></span>';
											}
											$stars .= '<span class="icon-star-half-alt"></span>';

										} else {

											for( $i = 0; $i < $rating; $i++ ) {
												$stars .= '<span class="icon-star"></span>';
											}

										}

										for( $x = 0; $x < $remaining; $x++ ) {
											$stars .= '<span class="icon-star-empty"></span>';
										}

									$stars .= '</div>';

									echo $stars;

								}


								$buy_url = get_post_meta( get_the_ID(), '_gg_buy_url', 1 ) != '' ? get_post_meta( get_the_ID(), '_gg_buy_url', 1 ) : '';

								if( $buy_url != '' ) {

									echo '<a class="buynow" href="'. $buy_url .'" onClick="_gaq.push( [\'_trackEvent\', \'Women\'s Running Buyer\'s Guide\', \'Buy Now Button\', \''. $buy_url .'\'] );" target="_blank">Buy Now</a>';

								}

							echo '</div>';


							if( $reviewer_id != '' || $rating != '' ) {
								echo '</div>';
							}

						?>
					</div>

					<div class="gg-one-third">
						<div class="cgg_2016_ad_col gg-ad">
							<h4>Advertisement</h4>
							<section class="advert advert_xs_300x250 advert_location_sidebar">
								<div class="advert__wrap"><?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('middle-gg') : ''; ?></div>
							</section>
						</div>
					</div>
				</div>


				<?php
					echo '<div class="gg-row__bottom">
						<div class="gg-related">
							<h2>Related Options</h2>';

							/**
							 * related options query
							 * grabs 4 items in the gift guide category, not including the current post
							 */
							$related_args = array(
								'posts_per_page'	=> 4,
								'category_name'		=> $category_name[0]->slug,
								'post__not_in'		=> array( get_the_ID() ),
								'orderby'			=> 'rand'
							);
							$related_query = new WP_Query( $related_args );


							if( $related_query->have_posts() ) {

								echo '<ul class="gg-related__list">';

									while( $related_query->have_posts() ) {
										$related_query->the_post();

										echo '<li>
											<a href="'. get_the_permalink() .'">'. get_the_post_thumbnail( get_the_ID(), 'newsletter-image' ) . '
											<div class="gg-related__title">'. get_the_title() .'</a>

										</li>';

									}

								echo '</ul>';

								wp_reset_postdata();

							}

						echo '</div>';


						/*$args = array(
							'menu'				=> strtolower( $category_name[0]->category_nicename ) .'-submenu',
							'menu_class'		=> 'gg-lowersubmenu__wrapper',
							'container_class'	=> 'gg-lowersubmenu',
							'fallback_cb'		=> false,
							'depth'				=> 1,
							'echo'				=> false,
							'theme_location'	=> '__none',
						);

						$bottom_navi = wp_nav_menu( $args );

						if( $bottom_navi != '' ) {
							echo '<div class="gg-morecats">
								<h2>More Categories</h2>'.
								$bottom_navi .'
							</div>';
						}*/

					echo '</div>';
				?>


			</div>

		</article>
	</div>
</div>
