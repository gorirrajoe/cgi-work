<?php
	// add specific classes to the article tag so they can be used for styling the page correctly
	$classes	= array();
	$is_mobile	= is_mobile();

	$content_string	= apply_filters( 'the_content', get_the_content() );
	$content_string	= preg_replace( "/<img[^>]+\>/i", " ", $content_string );
	$content_string	= preg_replace( "/\[sig:[a-zA-Z]+\]/", " ", $content_string );

	$buynowstart = strpos( $content_string, "buynowbutton" );

	$gg_options	= GiftGuide_Admin::get_guide_option( 2015 , 'all' );
	$page		= ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;
?>

<style>
	.cgg_banner {
		margin:2.375em 0 1.25em;
	}
	.cgg_full {
		text-align:center;
		margin-bottom:1.25em;
	}
	.cgg_row {
		overflow:hidden;
		margin:auto;
	}
	.cgg_one_third.cgg_right_module {
		margin-left:0;
	}
	.cgg_ad_col,
	.cgg_ad_col_2 {
		background-color:#eee;
		text-align:center;
		padding:2em 0;
		margin-bottom:1.25em;
	}
	.cgg_one_third,
	.cgg_module {
		overflow:hidden;
	}
	.cgg_ad_col h4 {
		padding:0 0 1.400em;
	}
	.cgg_one_third .cgg_one_sixth,
	.cgg_last_third .cgg_one_sixth {
		text-align:center;
		margin-bottom:1.25em;
	}
	#div-300_600_top_300_250 {
		display:table;
		margin:auto;
	}
	.buyers-guide-slider-container {
		clear:both;
		margin-bottom:.938em;
		padding-bottom:.938em;
		border-bottom:1px solid #a7a9ac;
	}
	/* single */
	.cgg_one_half {
		text-align:center;
	}
	.cgg_last_half p:nth-of-type( 1 ) {
		display: none;
	}
	.cgg_last_half h2 {
		padding-top: 0.714em;
	}
	.slide h3 {
		padding:0.455em 0 0 0;
	}
	.owl-theme .owl-controls .owl-buttons div {
		position:relative;
	}
	.owl-theme .owl-controls {
		margin:0 0 .625em 0;
		overflow:hidden;
	}
	.owl-prev {
		float:left;
	}
	.owl-next {
		float:right;
	}
	.more-gear-slider-container h2 {
		padding-top:0;
	}
	.slide {
		text-align:center;
	}
	.owl-carousel .owl-item {
		padding:0 .625em;
	}
	.cgg_last_half h3 {
		padding-top:0;
	}
	.buynowbutton a {
		background-image: url( <?php echo $gg_options['buynowimage']; ?> );
		height: 40px;
		display:block;
		margin-bottom: 1.25em;
		text-indent: -9999px;
		width: 200px;
	}
	.cgg_clear {
		clear:both;
	}
	@media only screen and ( min-width:440px ) {
		.cgg_one_third .cgg_one_sixth,
		.cgg_last_third .cgg_one_sixth {
			float:left;
			margin-bottom:1.25em;
		}
		.cgg_right_module {
			margin-left:1.25em;
		}
		.cgg_row {
			display:table;
		}
	}
	@media only screen and ( min-width:480px ) {
		.header_buynow {
			overflow:hidden;
		}
		<?php if( $buynowstart !== false ) { ?>
			.cgg_last_half h2 {
				float:left;
				margin-right:8.214em;
			}
		<?php } ?>
		.buynowbutton {
			float:right;
			margin:1.25em 0 0 -100%;
		}
	}
	@media only screen and ( min-width:990px ) {
		.cgg_container {
			max-width:830px;
			margin:auto;
		}
		.cgg_banner {
			margin:2.375em 0 1.875em;
		}
		.cgg_full,
		.cgg_one_third .cgg_one_sixth,
		.cgg_last_third .cgg_one_sixth {
			margin-bottom:1.875em;
		}
		.cgg_one_third .cgg_one_sixth,
		.cgg_last_third .cgg_one_sixth {
			width:46.25%;
		}
		.cgg_one_third {
			float:left;
			width:48.19277108433735%;
		}
		.cgg_one_third.cgg_right_module {
			float:left;
			margin-left:1.875em;
		}
		.cgg_ad_col,
		.cgg_ad_col_2 {
			margin-bottom:1.875em;
		}
		.cgg_right_module {
			margin-left:1.875em;
		}
		.cgg_last_third .cgg_row {
			float:left;
			width:48.19277108433735%;
			display:block;
		}
		.owl-carousel .owl-item {
			padding:0 .938em;
		}
	}
	@media only screen and ( min-width:1300px ) {
		.cgg_container {
			max-width:1260px;
		}
		.cgg_banner {
			margin:2.375em 0 1.875em;
		}
		.cgg_two_thirds {
			float:left;
			width:65.87301587301587%;
		}
		.cgg_last_third {
			float: left;
			margin-left: 1.875em;
			width: 31.746%;
		}
		.cgg_last_third .cgg_row {
			width:auto;
			float:none;
			margin-left:0;
		}
		.cgg_one_half,
		.cgg_last_half {
			float:left;
			width:48.80952380952381%;
		}
		.cgg_last_half {
			margin-left:1.875em;
		}
		.cgg_ad_col {
			padding:2em 0 5.5em;
		}
		.cgg_last_half h2 {
			padding-top:0;
		}
		.buynowbutton {
			margin-top:0;
		}
	}
</style>

<div class="cgg_container">
	<div class="cgg_banner">
		<a href="<?php echo site_url( '/category/competitor-gift-guide/' ); ?>"><img src="<?php echo $gg_options['gift_guide_banner_image']; ?>"></a>
	</div>

	<h1>
		<img class="slashes slash_blue" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/images/running/running-slashes-blue.svg">
		<?php echo strip_tags( the_title( '','',false ) ); ?>
	</h1>

	<?php
		$prev_next_links = wp_link_pages( array(
			'before'			=> '<div class="row multipage_row">',
			'after'				=> '</div>',
			'next_or_number'	=> 'next',
			'previouspagelink'	=> '<span class="icon-left-open"></span> Previous Gift',
			'nextpagelink'		=> 'Next Gift <span class="icon-right-open"></span>',
			'echo'				=> 0
		 ) );
		$prev_next_links	= str_replace( '<a href=', '<div class="btn_primary"><a class="next-post" href=', $prev_next_links );
		$prev_next_links	= str_replace( '</a>', '</a></div>', $prev_next_links );

		echo $prev_next_links;

		$attachments = get_children( array(
			'post_parent'		=> get_the_ID(),
			'post_status'		=> 'inherit',
			'post_type'			=> 'attachment',
			'post_mime_type'	=> 'image',
			'orderby'			=> 'menu_order',
			'order'				=> 'ASC'
		) );

		$count = 1;

		foreach( $attachments as $attachment ) {
			$attachment_id = $attachment->ID;

			if( $page == $count ) {
				$attachment_id = $attachment->ID;
				break;
			}

			$count++;
		}

		if( $is_mobile ) {
			$image_array = wp_get_attachment_image_src( $attachment_id, 'medium' );
		} else {
			$image_array = wp_get_attachment_image_src( $attachment_id, 'full' );
		}

		$thumbnail_image	= get_post( $attachment_id );
		$thumbnail_caption	= "";
		$thumbnail_title	= "";

		if ( !empty( $thumbnail_image ) ) {
			$thumbnail_caption	= $thumbnail_image->post_excerpt;
			$thumbnail_title	= $thumbnail_image->post_title;
		}

		echo '<div class="cgg_one_half">
			<img title="'.$thumbnail_title.'" src="'.$image_array[0].'">
		</div>

		<div class="cgg_last_half">';

			$pagetitlestring = '/<!--pagetitle:(.*?)-->/';
			preg_match_all( $pagetitlestring, $posts[0]->post_content, $titlesarray, PREG_PATTERN_ORDER );
			$pagetitles = $titlesarray[1];

			echo '<h2>'. $pagetitles[$page - 1] .'</h2>' .

			$content_string;

			echo '<div class="cgg_ad_col_2">';
				echo class_exists( 'Wp_Dfp_Ads' ) ? Wp_Dfp_Ads::display_ad( 'side-top' ) : '';
			echo '</div>

		</div>


		<div class="buyers-guide-slider-container">
			<div class="owl-carousel" id="buyers-guide-slider">';

				$page_count = 1;

				foreach( $attachments as $attachment ) {

					$attachment_id		= $attachment->ID;
					$image_array_thumb	= wp_get_attachment_image_src( $attachment_id, 'square-gift-guide-product' );

					echo '<div class="slide">
						<a href="'.get_permalink().'/'.$page_count.'"><img src="'.$image_array_thumb[0].'"/></a>
						<h3><a href="'.get_permalink().$page_count.'">'.$pagetitles[$page_count - 1].'</a></h3>
					</div>';

					$page_count++;

				}
			echo '</div>
		</div>


		<div class="more-gear-slider-container">
			<h2>More Gifts</h2>
			<div class="owl-carousel" id="more-gear-slider">';

				$gear_count	= 1;
				$gear_array	= array();

				for( $i = 0; $i < 52; $i++ ) {

					if( $gear_count > 13 ) {
						break;
					} else {
						$random = rand( 1, 13 );

						if( !in_array( $random, $gear_array ) ) {
							$theme_option_image = 'gift_guide_more_gear_image_'.$random;

							if( array_key_exists( $theme_option_image, $gg_options ) && $gg_options[$theme_option_image] != '' ) {

								$theme_option_link = 'gift_guide_link_'.$random;

								echo '<div class="slide">
									<a href="'.$gg_options[$theme_option_link].'"><img src="'.$gg_options[$theme_option_image].
									'"/></a>
								</div>';

							}

							$gear_array[] = $random;
							$gear_count++;
						}
					}
				}
			echo '</div>
		</div>';
	?>
</div>


<script>
	jQuery( document ).ready( function( $ ) {
		$( "#buyers-guide-slider" ).owlCarousel( {
			navigation:true,
			navigationText:false,
			pagination:false,
			itemsCustom:[
				[0,1],
				[480,2],
				[990,3],
				[1300,5]
			]
		} );
		$( "#more-gear-slider" ).owlCarousel( {
			navigation:true,
			navigationText:false,
			pagination:false,
			itemsCustom:[
				[0,1],
				[480,2],
				[990,3]
			]
		} );
	} );
</script>