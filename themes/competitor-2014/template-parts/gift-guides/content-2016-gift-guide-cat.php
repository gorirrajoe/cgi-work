<?php
	/* 2016 COMPETITOR GIFT GUIDE */

	$gg_options	= GiftGuide_Admin::get_guide_option( 2016 , 'all' );
	$prefix		= 'gift_guide_2016_';
?>

<div class="cgg">

	<div class="cgg_2016_container">

		<div class="cgg_2016_banner">
			<img src="<?php echo $gg_options[$prefix . 'banner_image']; ?>" />
		</div>

		<?php if( !( get_query_var( 'paged' ) > 1 ) ) { ?>

			<div class="cgg_2016_two_thirds cgg_2016_module">

				<?php /* 1 */ ?>

				<div class="cgg_2016_full">
					<?php
						$parsed_url_1		= parse_url( $gg_options[$prefix . 'link_1'] );
						$google_tracking_1	= '';

						if( $gg_options[$prefix . 'link_1'] != '' && strpos( home_url(), $parsed_url_1['host'] ) === FALSE ) {
							$google_tracking_1 = 'onclick="_gaq.push( [\'_trackEvent\', \'Competitor Gift Guide 2016\', \'running\', \''. $gg_options[$prefix . 'link_1'].'\'] )"';
						}
					?>
					<a href="<?php echo $gg_options[$prefix . 'link_1']; ?>" <?php echo $google_tracking_1;?>>
						<img src="<?php echo $gg_options[$prefix . 'image_1']; ?>">
					</a>
				</div>


				<div class="cgg_2016_one_third cgg_2016_module">
					<div class="cgg_2016_row">

						<?php /* 2 */ ?>

						<div class="cgg_2016_one_sixth cgg_2016_module">
							<?php
								$parsed_url_2		= parse_url( $gg_options[$prefix . 'link_2'] );
								$google_tracking_2	= '';

								if( $gg_options[$prefix . 'link_2'] != '' && strpos( home_url(), $parsed_url_2['host'] ) === FALSE ) {
									$google_tracking_2 = 'onclick="_gaq.push( [\'_trackEvent\', \'Competitor Gift Guide 2016\', \'running\', \''. $gg_options[$prefix . 'link_2'].'\'] )"';
								}
							?>
							<a href="<?php echo $gg_options[$prefix . 'link_2']; ?>" <?php echo $google_tracking_2;?>>
								<img src="<?php echo $gg_options[$prefix . 'image_2']; ?>">
							</a>
						</div>


						<?php /* 3 */ ?>

						<div class="cgg_2016_one_sixth cgg_2016_right_module">
							<?php
								$parsed_url_3		= parse_url( $gg_options[$prefix . 'link_3'] );
								$google_tracking_3	= '';

								if( $gg_options[$prefix . 'link_3'] != '' && strpos( home_url(), $parsed_url_3['host'] ) === FALSE ) {
									$google_tracking_3 = 'onclick="_gaq.push( [\'_trackEvent\', \'Competitor Gift Guide 2016\', \'running\', \''. $gg_options[$prefix . 'link_3'].'\'] )"';
								}
							?>
							<a href="<?php echo $gg_options[$prefix . 'link_3']; ?>" <?php echo $google_tracking_3;?>>
								<img src="<?php echo $gg_options[$prefix . 'image_3']; ?>">
							</a>
						</div>

					</div>


					<?php /* 4 */ ?>

					<div class="cgg_2016_full">
						<?php
							$parsed_url_4		= parse_url( $gg_options[$prefix . 'link_4'] );
							$google_tracking_4	= '';

							if( $gg_options[$prefix . 'link_4'] != '' && strpos( home_url(), $parsed_url_4['host'] ) === FALSE ) {
								$google_tracking_4 = 'onclick="_gaq.push( [\'_trackEvent\', \'Competitor Gift Guide 2016\', \'running\', \''. $gg_options[$prefix . 'link_4'].'\'] )"';
							}
						?>
						<a href="<?php echo $gg_options[$prefix . 'link_4']; ?>" <?php echo $google_tracking_4;?>>
							<img src="<?php echo $gg_options[$prefix . 'image_4']; ?>">
						</a>
					</div>


					<?php /* 5 */ ?>

					<div class="cgg_2016_full">
						<?php
							$parsed_url_5		= parse_url( $gg_options[$prefix . 'link_5'] );
							$google_tracking_5	= '';

							if( $gg_options[$prefix . 'link_5'] != '' && strpos( home_url(), $parsed_url_5['host'] ) === FALSE ) {
								$google_tracking_5 = 'onclick="_gaq.push( [\'_trackEvent\', \'Competitor Gift Guide 2016\', \'running\', \''. $gg_options[$prefix . 'link_5'].'\'] )"';
							}
						?>
						<a href="<?php echo $gg_options[$prefix . 'link_5']; ?>" <?php echo $google_tracking_5;?>>
							<img src="<?php echo $gg_options[$prefix . 'image_5']; ?>">
						</a>
					</div>

				</div>

				<div class="cgg_2016_one_third cgg_2016_right_module">

					<?php /* 6 */ ?>

					<div class="cgg_2016_full">
						<?php
							$parsed_url_6		= parse_url( $gg_options[$prefix . 'link_6'] );
							$google_tracking_6	= '';

							if( $gg_options[$prefix . 'link_6'] != '' && strpos( home_url(), $parsed_url_6['host'] ) === FALSE ) {
								$google_tracking_6 = 'onclick="_gaq.push( [\'_trackEvent\', \'Competitor Gift Guide 2016\', \'running\', \''. $gg_options[$prefix . 'link_6'].'\'] )"';
							}
						?>
						<a href="<?php echo $gg_options[$prefix . 'link_6']; ?>" <?php echo $google_tracking_6;?>>
							<img src="<?php echo $gg_options[$prefix . 'image_6']; ?>">
						</a>
					</div>


					<div class="cgg_2016_row">

						<?php /* 7 */ ?>

						<div class="cgg_2016_one_sixth cgg_2016_module">
							<?php
								$parsed_url_7		= parse_url( $gg_options[$prefix . 'link_7'] );
								$google_tracking_7	= '';

								if( $gg_options[$prefix . 'link_7'] != '' && strpos( home_url(), $parsed_url_7['host'] ) === FALSE ) {
									$google_tracking_7 = 'onclick="_gaq.push( [\'_trackEvent\', \'Competitor Gift Guide 2016\', \'running\', \''. $gg_options[$prefix . 'link_7'].'\'] )"';
								}
							?>
							<a href="<?php echo $gg_options[$prefix . 'link_7']; ?>" <?php echo $google_tracking_7;?>>
								<img src="<?php echo $gg_options[$prefix . 'image_7']; ?>">
							</a>
						</div>

						<div class="cgg_2016_one_sixth cgg_2016_right_module">

							<?php /* 8 & 9*/
							/* check for image 9, if no image use only 8 */

							if ( !empty( $gg_options[$prefix . 'image_9'] ) ): ?>

								<div class="cgg_2016_full">
									<?php
										$parsed_url_8		= parse_url( $gg_options[$prefix . 'link_8'] );
										$google_tracking_8	= '';

										if( $gg_options[$prefix . 'link_8'] != '' && strpos( home_url(), $parsed_url_8['host'] ) === FALSE ) {
											$google_tracking_8 = 'onclick="_gaq.push( [\'_trackEvent\', \'Competitor Gift Guide 2016\', \'running\', \''. $gg_options[$prefix . 'link_8'].'\'] )"';
										}
									?>
									<a href="<?php echo $gg_options[$prefix . 'link_8']; ?>" <?php echo $google_tracking_8;?>>
										<img src="<?php echo $gg_options[$prefix . 'image_8']; ?>">
									</a>
								</div>

								<?php /* 9 */ ?>

								<div class="cgg_2016_full">
									<?php
										$parsed_url_9		= parse_url( $gg_options[$prefix . 'link_9'] );
										$google_tracking_9	= '';

										if( $gg_options[$prefix . 'link_9'] != '' && strpos( home_url(), $parsed_url_9['host'] ) === FALSE ) {
											$google_tracking_9 = 'onclick="_gaq.push( [\'_trackEvent\', \'Competitor Gift Guide 2016\', \'running\', \''. $gg_options[$prefix . 'link_9'].'\'] )"';
										}
									?>
									<a href="<?php echo $gg_options[$prefix . 'link_9']; ?>" <?php echo $google_tracking_9;?>>
										<img src="<?php echo $gg_options[$prefix . 'image_9']; ?>">
									</a>
								</div>

							<?php else:

								$parsed_url_8 = parse_url( $gg_options[$prefix . 'link_8'] );
								$google_tracking_8 = '';

								if( $gg_options[$prefix . 'link_8'] != '' && strpos( home_url(), $parsed_url_8['host'] ) === FALSE ) {
									$google_tracking_8 = 'onclick="_gaq.push( [\'_trackEvent\', \'Competitor Gift Guide 2016\', \'running\', \''. $gg_options[$prefix . 'link_8'].'\'] )"';
								} ?>
								<a href="<?php echo $gg_options[$prefix . 'link_8']; ?>" <?php echo $google_tracking_8;?>>
									<img src="<?php echo $gg_options[$prefix . 'image_8']; ?>">
								</a>
							<?php endif; ?>

						</div>
					</div>
				</div>

			</div>

			<div class="cgg_2016_last_third">

				<div class="cgg_2016_ad_col">
					<h4>Advertisement</h4>
					<?php
						echo class_exists( 'Wp_Dfp_Ads' ) ? Wp_Dfp_Ads::display_ad( 'side-top' ) : '';
					?>
				</div>
				<!-- ad unit -->

				<div class="cgg_2016_row">

					<?php /* 10 */ ?>

					<div class="cgg_2016_full">
						<?php
							$parsed_url_10		= parse_url( $gg_options[$prefix . 'link_10'] );
							$google_tracking_10	= '';

							if( $gg_options[$prefix . 'link_10'] != '' && strpos( home_url(), $parsed_url_10['host'] ) === FALSE ) {
								$google_tracking_10 = 'onclick="_gaq.push( [\'_trackEvent\', \'Competitor Gift Guide 2016\', \'running\', \''. $gg_options[$prefix . 'link_10'].'\'] )"';
							}
						?>
						<a href="<?php echo $gg_options[$prefix . 'link_10']; ?>" <?php echo $google_tracking_10;?>>
							<img src="<?php echo $gg_options[$prefix . 'image_10']; ?>">
						</a>
					</div>

				</div>

				<div class="cgg_2016_row cgg_2016_right_module_2">

					<?php /* 11 */ ?>

					<div class="cgg_2016_full">
						<?php
							$parsed_url_11		= parse_url( $gg_options[$prefix . 'link_11'] );
							$google_tracking_11	= '';

							if( $gg_options[$prefix . 'link_11'] != '' && strpos( home_url(), $parsed_url_11['host'] ) === FALSE ) {
								$google_tracking_11 = 'onclick="_gaq.push( [\'_trackEvent\', \'Competitor Gift Guide 2016\', \'running\', \''. $gg_options[$prefix . 'link_11'].'\'] )"';
							}
						?>
						<a href="<?php echo $gg_options[$prefix . 'link_11']; ?>" <?php echo $google_tracking_11;?>>
							<img src="<?php echo $gg_options[$prefix . 'image_11']; ?>">
						</a>
					</div>
				</div>

			</div>

		<?php } else {

			$page = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; ?>

			<div class="left_column">
				<div>
					<?php
						echo '<h1><img class="slashes slash_blue" src="'.get_bloginfo( 'stylesheet_directory' ).'/images/running/running-slashes-blue.svg"> All Posts Categorized &#8216;' . single_cat_title( '', false ) . '&#8217;: Page ' . $page . '</h1>';

						$count = 1;

						if ( have_posts() ) : while ( have_posts() ) : the_post();

							$permalink		= get_permalink();
							$title			= get_the_title();
							$excerpt		= get_the_excerpt();
							$post_classes	= get_post_class();

							if ( has_post_thumbnail () ) {

								if( is_mobile() ) {
									$image_array = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'marquee-carousel-mobile' );
								} else {
									if( $count == 1 ) {
										$image_array = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
									} else {
										$image_array = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'thumbnail' );
									}
								}

								$post_thumb = $image_array[0];

							} else {
								if( $count == 1 ) {
									$post_thumb = get_bloginfo( 'stylesheet_directory' ).'/images/running/default-post-thumbnail-lg.jpg';
								} else {
									$post_thumb = get_bloginfo( 'stylesheet_directory' ).'/images/running/default-post-thumbnail-sm.jpg';
								}
							}

							$post_classes_string	= implode( " ", $post_classes );

							$month_published		= get_the_time( 'M' );
							$day_published			= get_the_time( 'd' );

							if ( $count == 1 && ( get_query_var( 'paged' ) < 3 ) ) {  // first post is a big one

								echo '<div class="featured_post '.$post_classes_string.'">
									<div class="featured_img">
										<a class="overlay" href="'.$permalink.'"></a>
										<img src="'.$post_thumb.'">
									</div>
									<div class="row featured_vitals">
										<div class="featured_post_date">
											<span class="featured_post_month">'.$month_published.'</span>
											<span class="featured_post_day">'.$day_published.'</span>
										</div>
										<div class="featured_title_author">
											<h3><a href="'.$permalink.'">'.$title.'</a></h3>
											<p class="post_author">by '.get_guest_author().'</p>
											<p class="excerpt">'.$excerpt.'</p>
										</div>
										<a class="arrow" href="'.$permalink.'"><span class="icon-right-open"></span></a>
									</div>
								</div>';

							} else {

								$thumb = '<div class="featured_img_archive"><a class="overlay" href="' . $permalink . '" title="' . $title . '"></a>' . '<img class="archive_thumb" src="'.$post_thumb.'"></div>';
								echo '<div class="row archive_row '.$post_classes_string.'">
									'.$thumb.'
									<h4><a href="'.$permalink.'">'.$title.'</a></h4>
									<p class="post_author">by '.get_guest_author().', '.format_timestamp_2().'</p>
									<p class="excerpt">'.$excerpt.'</p>
								</div>';
							}

							$count++;

						endwhile; else: ?>

							<div class="content">
								<p>Sorry, no posts matched your criteria.</p>
							</div>

						<?php endif;
					?>
				</div>

				<?php
					echo '<div class="row archive_navi">
						<div class="btn_primary">
							'.get_previous_posts_link( '<span class="icon-left-open"></span> Previous Page' ).'
						</div>
						<div class="btn_primary">
							'.get_next_posts_link( 'Next Page <span class="icon-right-open"></span>' ).'
						</div>
					</div>';
				?>

			</div><!-- row 1/content -->

			<?php get_sidebar();
		} ?>

	</div>
</div>
