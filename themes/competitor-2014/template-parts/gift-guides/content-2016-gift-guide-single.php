<?php
	$gg_options	= GiftGuide_Admin::get_guide_option( 2016, 'all' );

	$classes		= array();
	$is_mobile		= is_mobile();

	$content_string	=  apply_filters( 'the_content', get_the_content() );

	$content_string	= preg_replace( "/<img[^>]+\>/i", " ", $content_string );
	$content_string	= preg_replace( "/\[sig:[a-zA-Z]+\]/", " ", $content_string );
	$page			= ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;
?>

<div class="cgg">
	<div class="cgg_container">
		<div class="cgg_banner">
			<a href="<?php echo site_url( '/category/2016-gift-guide/' ); ?>"><img src="<?php echo $gg_options['gift_guide_2016_banner_image']; ?>"></a>
		</div>

		<h1>
			<img class="slashes slash_blue" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/images/running/running-slashes-blue.svg">
			<?php echo strip_tags( the_title( '','',false ) ); ?>
		</h1>


		<?php
			$prev_next_links = wp_link_pages( array(
				'before'			=> '<div class="row multipage_row">',
				'after'				=> '</div>',
				'next_or_number'	=> 'next',
				'previouspagelink'	=> '<span class="icon-left-open"></span> Previous Gift',
				'nextpagelink'		=> 'Next Gift <span class="icon-right-open"></span>',
				'echo'				=> 0
			 ) );
			$prev_next_links	= str_replace( '<a href=', '<div class="btn_primary"><a class="next-post" href=', $prev_next_links );
			$prev_next_links	= str_replace( '</a>', '</a></div>', $prev_next_links );

			echo $prev_next_links;

			$attachments = get_children( array(
				'post_parent'		=> get_the_ID(),
				'post_status'		=> 'inherit',
				'post_type'			=> 'attachment',
				'post_mime_type'	=> 'image',
				'orderby'			=> 'menu_order',
				'order'				=> 'ASC'
			 ) );

			$count = 1;

			foreach( $attachments as $attachment ) {
				$attachment_id = $attachment->ID;

				if( $page == $count ) {
					$attachment_id = $attachment->ID;
					break;
				}
				$count++;
			}
			$attachment_count = count( $attachments );

			if( $is_mobile ) {
				$image_array = wp_get_attachment_image_src( $attachment_id, 'medium' );
			} else{
				$image_array = wp_get_attachment_image_src( $attachment_id, 'single-post-big' );
			}

			$thumbnail_image	= get_post( $attachment_id );
			$thumbnail_caption	= "";
			$thumbnail_title	= "";

			if ( !empty( $thumbnail_image ) ) {
				$thumbnail_caption	= $thumbnail_image->post_excerpt;
				$thumbnail_title	= $thumbnail_image->post_title;
			}

			$pagetitlestring = '/<!--pagetitle:(.*?)-->/';
			preg_match_all( $pagetitlestring, $posts[0]->post_content, $titlesarray, PREG_PATTERN_ORDER );
			$pagetitles = $titlesarray[1];

			echo '<div class="cgg_one_half">
				<img title="'.$thumbnail_title.'" src="'.$image_array[0].'">
			</div>


			<div class="cgg_last_half">
				<h2>'.$pagetitles[$page - 1].'</h2>' .

				$content_string .'

				<div class="cgg_ad_col_2">';
					echo class_exists( 'Wp_Dfp_Ads' ) ? Wp_Dfp_Ads::display_ad( 'side-top' ) : '';
				echo '</div>

			</div>


			<div class="buyers-guide-slider-container">
				<div class="owl-carousel" id="buyers-guide-slider">';

					$page_count = 1;

					foreach( $attachments as $attachment ) {

						if( $page_count < ( $attachment_count ) ) {
							$attachment_id		= $attachment->ID;
							$image_array_thumb	= wp_get_attachment_image_src( $attachment_id, 'square-gift-guide-product' );

							echo '<div class="slide">
								<a href="'.get_permalink().'/'.$page_count.'"><img src="'.$image_array_thumb[0].'"/></a>
								<h3><a href="'.get_permalink().'/'.$page_count.'">'.$pagetitles[$page_count - 1].'</a></h3>
							</div>';

						}
						$page_count++;

					}

				echo '</div>
			</div>

			<div class="more-gear-slider-container">
				<h2>More Gifts</h2>
				<div class="owl-carousel" id="more-gear-slider">';

					$gear_count	= 1;
					$gear_array	= array();

					for( $i = 0; $i < 52; $i++ ) {

						if( $gear_count > 12 ) {
							break;
						} else {
							$random = rand( 1, 11 );

							if( !in_array( $random, $gear_array ) ) {
								$theme_option_image = 'more_gear_2016_image_'.$random;

								if( $gg_options[$theme_option_image] && $gg_options[$theme_option_image] != '' ) {

									$theme_option_link = 'gift_guide_2016_link_'.$random;

									echo '<div class="slide">
										<a href="'.$gg_options[$theme_option_link].'"><img src="'.$gg_options[$theme_option_image].
										'"/></a>
									</div>';
								}

								$gear_array[] = $random;
								$gear_count++;
							}
						}
					}
				echo '</div>
			</div>';
		?>
	</div>
</div>


<script>
	jQuery( document ).ready( function( $ ) {
		$( "#buyers-guide-slider" ).owlCarousel( {
			navigation:true,
			navigationText:false,
			pagination:false,
			itemsCustom:[
				[0,1],
				[480,2],
				[990,3],
				[1300,5]
			]
		} );
		$( "#more-gear-slider" ).owlCarousel( {
			navigation:true,
			navigationText:false,
			pagination:false,
			itemsCustom:[
				[0,1],
				[480,2],
				[990,3]
			]
		} );
	} );
</script>
