<?php
	/**
	 * Template part for displaying standard single posts.
	 */

	$post_ID	= get_the_ID();

	$attachments = get_children( array(
		'post_parent'		=> $post_ID,
		'post_status'		=> 'inherit',
		'post_type'			=> 'attachment',
		'post_mime_type'	=> 'image',
		'orderby'			=> 'menu_order',
		'order'				=> 'ASC'
	) );

	$attachments_count			= count( $attachments );

	// Get some of the meta data we need
	$brightcove_id				= get_post_meta( $post_ID, 'video_id', true ) ?: '';
	$youtube_id					= get_post_meta( $post_ID, '_youtube_id', true ) ?: '';
	$infographic_post			= get_post_meta( $post_ID, '_infographic_post', true ) ?: '';
	$show_all_images			= get_post_meta( $post_ID, '_display_images', true ) ?: '';
	$featured_thumb_only		= get_post_meta( $post_ID, '_featured_thumb', true ) ?: '';

	$videos						= false;
	$is_mobile					= is_mobile();
	$technical_theme_options	= get_option( 'cgi_media_technical_options' );

	// Determine if we are in the Video category
	if ( !empty( $technical_theme_options['video_category'] ) && in_category( $technical_theme_options['video_category'] ) ) {
		$videos	= true;
	}

	if ( $youtube_id != '' ) {
		echo do_shortcode( '[youtube id="'. $youtube_id .'"]' );
	} elseif ( $brightcove_id != '' ){
		echo do_shortcode( '[brightcove id="'. $brightcove_id .'"]' );
	} elseif ( $videos ) {
		// don't show any of the featured image section, just show the content
	} elseif ( $featured_thumb_only ) {
		/*if(in_category( 'sponsored' ) == false) {
			echo get_latest_stories_2(get_the_ID());
		}*/
	} elseif ( $attachments_count == 1 ){
		// first check to see how many attachments there are, if less than two then find the attachment and display it at the top of the article
		if ( has_post_thumbnail( $post_ID ) ) {
			$image_ID = get_post_thumbnail_id( $post_ID );
		} else {
			foreach ( $attachments as $attachment ) {
				$image_ID = $attachment->ID;
				break;
			}
		}

		$image_size	= $is_mobile ? 'medium' : 'single-post-big';

		$image_array	= wp_get_attachment_image_src( $image_ID, $image_size );

		// Add vertical class if height is greater than the width
		$class	= $image_array[2] > $image_array[1] ? ' vertical-image' : '';

		// define the title and caption
		$thumbnail_image	= get_post( $image_ID );
		$thumbnail_title	= $thumbnail_image->post_title != '' ? ' title="'. $thumbnail_image->post_title .'"' : '';
		$thumbnail_caption	= $thumbnail_image->post_excerpt != '' ? '<div class="figcaption">'. $thumbnail_image->post_excerpt .'</div>' : '';

		// bring it all together and display it
		printf(
			'<div class="figure'. $class .'"><img %s src="'. $image_array[0] .'"/>%s</div>',
			$thumbnail_title,
			$thumbnail_caption
		);

	} elseif ( $attachments_count > 1 && in_category( 'lists' ) ) {

		// else if there is more than one attachment and the category is "lists," display the special gallery carousel of all the images that are attached
		$content_string	=  apply_filters( 'the_content', get_the_content() );

		// hide all of the images that were in the story
		echo '<style>.wp-caption { display: none; }</style>';

		$content_string	= preg_replace( "/<img[^>]+\>/i", ' ', $content_string );
		$content_string	= preg_replace( "/\[sig:[a-zA-Z]+\]/", ' ', $content_string );

		echo my_gallery_2( '', array(), 'legacy-list', 'carousel' );
		echo $content_string;;

	} elseif ( $attachments_count > 1 ) {

		// else if there is more than one attachment display the gallery carousel of all the images that are attached
		echo my_gallery_2( '', array(), 'medium', 'carousel' );

	} elseif ( has_post_thumbnail( $post_ID ) ) {

		$image_ID = get_post_thumbnail_id( $post_ID );

		$image_size	= $is_mobile ? 'medium' : 'single-post-big';

		$image_array	= wp_get_attachment_image_src( $image_ID, $image_size );

		// Add vertical class if height is greater than the width
		$class	= $image_array[2] > $image_array[1] ? ' vertical-image' : '';

		// define the title and caption
		$thumbnail_image	= get_post( $image_ID );
		$thumbnail_title	= $thumbnail_image->post_title != '' ? ' title="'. $thumbnail_image->post_title .'"' : '';
		$thumbnail_caption	= $thumbnail_image->post_excerpt != '' ? '<div class="figcaption">'. $thumbnail_image->post_excerpt .'</div>' : '';

		// bring it all together and display it
		printf(
			'<div class="figure'. $class .'"><img %s src="'. $image_array[0] .'"/>%s</div>',
			$thumbnail_title,
			$thumbnail_caption
		);

	} else {
		/**
		 * Else if there are no images attached to the story,
		 * get 5 most recently posted articles and display the top four, and make sure that the one you're on is not in the list
		 */
		if ( !in_category( 'sponsored' ) ) {
			echo get_latest_stories_2( $post_ID );
		}
	}


	if ( !in_category( 'lists' ) ) {

		$the_content	= $post->post_content;
		$the_content	= preg_replace( "/\[sig:[a-zA-Z]+\]/", '', $the_content );

		if ( !$show_all_images ) {
			echo '<style>.wp-caption { display: none; }</style>';
			$the_content = preg_replace( "/<img[^>]+\>/i", '', $the_content );
		}

		$the_content				= apply_filters( 'the_content', $the_content );
		$content_parts				= get_extended( $the_content );
		$content_string_main		= $content_parts['main'];
		$content_string_extended	= $content_parts['extended'];

		echo '<div class="single_intro">';
			echo $content_string_main;
		echo '</div>';

		if ( $content_string_extended != '' ) { ?>

			<div class="single_rest">
				<h3>Continue Reading</h3>
				<div><?php echo $content_string_extended; ?></div>
			</div>

			<script>
				jQuery( function($) {
					$('.single_rest').accordion({
						active: false,
						collapsible: true
					});
				});
			</script>

			<?php
		}

	}
