<?php
	/**
	 * Template part for displaying standard single posts.
	 */

	global $post;
	global $page;
	$post_ID	= get_the_ID();

	$attachments	= get_children( array(
		'post_parent'		=> $post_ID,
		'post_status'		=> 'inherit',
		'post_type'			=> 'attachment',
		'post_mime_type'	=> 'image',
		'orderby'			=> 'menu_order',
		'order'				=> 'ASC'
	) );

	$attachments_count	= count( $attachments );
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'article' ); ?>>

	<header class="article__header">
		<?php the_title( '<h1 class="article__title">', '</h1>' ); ?>
		<?php echo '<p class="post_author">By ' . get_guest_author() .', '. format_timestamp_2() .'</p>'; ?>
	</header>

	<section class="article__body">
		<?php
			echo AMP_Mods::get_social_sharing();

			/**
			 * Handle the Table of Contents
			 */
			preg_match_all( '/<!--pagetitle:(.*?)-->/', $post->post_content, $titles, PREG_PATTERN_ORDER );

			$page_titles = ( isset( $titles[1] ) ? $titles[1] : array() );
			$featured_thumb_only		= get_post_meta( $post_ID, '_featured_thumb', true ) ?: '';

			if ( !empty( $page_titles ) ) {

				/**
				 * Show the Image
				 */
				if ( !empty( $featured_thumb_only ) ) {
					// If they only want the featured image as a thumb in blog rolls, DO NOTHING
				} elseif ( $attachments_count > 0 ) {

					// We had to make sure there was any actual attachments if we are going to try the following

					// Pulling the attachment IDs to identify which one to use
					// With menu_order as key to find the right page
					// If there was no menu_order defined they all default to 0 so relax, this works
					$attachment_IDs	= wp_list_pluck( $attachments, 'ID', 'menu_order' );

					// Find out if we are using the code for order of custom featured images
					preg_match_all( '/<!--pageimage:(\s\d+)-->/', get_the_content(), $matches );

					// if we found a match for the page image then use that to display
					if ( !empty( $matches[0] ) ) {

						// make sure we trim the result
						$page_image	= trim( $matches[1][0] );

						// make absolutely sure the corresponding attachment ID number exists
						if ( array_key_exists( $page_image, $attachment_IDs ) ) {
							$attachment_ID	= $attachment_IDs[$page_image];
						}

					} elseif ( array_key_exists( $page, $attachment_IDs ) ) {

						// else if there was no match found, use the global $page to check for the corresponding attachment ID
						$attachment_ID	= $attachment_IDs[$page];

					}

				} elseif ( has_post_thumbnail() ) {

					// else if STILL there was no matches found, use the featured image of the post
					$attachment_ID = get_post_thumbnail_id();

				}

				// If the $attachment_ID was every defined, use that to display the image here
				if ( !empty( $attachment_ID ) ) {

					$image_size	= 'medium';

					$image_array	= wp_get_attachment_image_src( $attachment_ID, $image_size );

					// Add vertical class if height is greater than the width
					$class	= $image_array[2] > $image_array[1] ? ' vertical-image' : '';

					// define the title and caption
					$thumbnail_title	= $attachments[$attachment_ID]->post_title != '' ? 'title="'. $attachments[$attachment_ID]->post_title .'"' : '';
					$thumbnail_caption	= $attachments[$attachment_ID]->post_excerpt != '' ? '<figcaption>'. $attachments[$attachment_ID]->post_excerpt .'</figcaption>' : '';

					// bring it all together and display it
					printf(

						'<figure class="figure'. $class .'"><amp-img %s layout="fixed" src="'. $image_array[0] .'" width="'. $image_array[1] .'" height="'. $image_array[2] .'" />%s</figure>',
						$thumbnail_title,
						$image_array[0],
						$thumbnail_caption
					);

				}


				?>

				<aside class="toc-content">

					<div class="toc-content__wrap">

						<h3 class="toc-content__title">Table of Contents</h3>

							<ul class="toc-content__menu">

							<?php foreach( $page_titles as $i => $page_title ) {

								$i++;

								echo '<li>';

									if ( $page == $i ) {
										echo $page_title;
									} elseif( 1 === $i ) {
										echo '<a href="'. get_permalink() .'/'. AMP_QUERY_VAR .'">'. $page_title .'</a>';
									} else {
										echo '<a href="'. get_permalink() .'/'. $i .'/'. AMP_QUERY_VAR .'">'. $page_title .'</a>';
									}

								echo '</li>';

							} ?>

						</ul>

					</div>

				</aside>

				<?php

			} else {

				// this is hiding all of the images that were in the story originally, we will place the images when the post is displayed
				// Check if we want the image inline or not
				if ( empty( $featured_thumb_only ) ) {

					if ( $page == 1 ) {

						if ( $attachments_count == 1 ) {

							if ( has_post_thumbnail() ){

								$attachment_ID = get_post_thumbnail_id();

							} else {

								// If there was not post thumbnail, get the first attachment and break out
								foreach ( $attachments as $attachment ) {
									$attachment_ID = $attachment->ID;
									break;
								}

							}

						} elseif ( $attachments_count > 1 ) {
							// else if there is more than one attachment display the gallery carousel of all the images that are attached
							echo my_gallery_2( '', array(), 'medium', 'carousel' );
						} elseif ( has_post_thumbnail() ) {

							$attachment_ID	= get_post_thumbnail_id();

						} else {
							// else if there are no images attached to the story show a list of the most recently posted stories
							// get 5 most recently posted articles and display the top four, and make sure that the one you're on is not in the list
							echo get_latest_stories_2( $post_ID );
						}
					} elseif ( $attachments_count > 0 ) {

						// We had to make sure there was any actual attachments if we are going to try the following

						// Pulling the attachment IDs to identify which one to use
						// With menu_order as key to find the right page
						// If there was no menu_order defined, it will use a natural order
						$attachment_IDs	= array();
						foreach ( $attachments as $key => $attachment ) {
							if ( $attachment->menu_order !== 0 ) {
								$attachment_IDs[$attachment->menu_order]	= $key;
							} else {
								$attachment_IDs[]	= $key;
							}
						}

						// Find out if we are using the code for order of custom featured images
						preg_match_all( '/<!--pageimage:(\s\d+)-->/', get_the_content(), $matches );

						if ( !empty( $matches[0] ) ) {

							// make sure we trim the result
							$page_image	= trim( $matches[1][0] );

							// make absolutely sure the corresponding attachment ID number exists
							if ( array_key_exists( $page_image, $attachment_IDs ) ) {
								$attachment_ID	= $attachment_IDs[$page_image];
							}

						}

					}

					// If the $attachment_ID was every defined, use that to display the image here
					// Yes, that mess above and even worse in PostType.php was just to determine the appropriate Attachment ID to be able to display it
					if ( !empty( $attachment_ID ) ) {

						$image_size		= 'medium';

						$image_array	= wp_get_attachment_image_src( $attachment_ID, $image_size );

						// Add vertical class if height is greater than the width
						$class	= $image_array[2] > $image_array[1] ? ' vertical-image' : '';

						// define the title and caption
						$thumbnail_image	= get_post( $attachment_ID );
						$thumbnail_title	= $thumbnail_image->post_title != '' ? 'title="'. $thumbnail_image->post_title .'"' : '';
						$thumbnail_caption	= $thumbnail_image->post_excerpt != '' ? '<div class="figcaption">'. $thumbnail_image->post_excerpt .'</div>' : '';

						// bring it all together and display it
						printf(
							'<div class="figure'. $class .'"><amp-img %s layout="fixed" src="'. $image_array[0] .'" width="'. $image_array[1] .'" height="'. $image_array[2] .'" />%s</div>',
							$thumbnail_title,
							$thumbnail_caption
						);

					}

				}

			} // end if ( !empty( $page_titles ) )

			echo $this->get( 'post_amp_content' );

			wp_link_pages(
				array(
					'before'      => '<div class="page-links">'. esc_html__( 'Pages:' ) .'<br>',
					'after'       => '</div>',
					'link_before' => '<span class="page-link-number" aria-label="Go To Page %">',
					'link_after'  => '</span>'
				)
			);

			echo AMP_Mods::get_social_sharing();
		?>
	</section>

</article><!-- #post-## -->
