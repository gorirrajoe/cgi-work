<?php
	/**
	 * Template Name: Interstitial Ad
	 */

	$is_mobile	= function_exists( 'is_mobile' ) ? is_mobile() : false;

	// Determine dimensions of ad based on GET parameters, but have fallback
	if ( !empty( $_GET['width'] ) && !empty( $_GET['height'] ) ) {
		$div_name	= 'div-'. $_GET['width'] .'_'. $_GET['height'];
		$dimensions	= $_GET['width'] .', '. $_GET['height'];
	} elseif ( $is_mobile ) {
		$div_name	= 'div-250_208';
		$dimensions	= '250, 208';
	} else {
		$div_name	= 'div-640_480';
		$dimensions	= '640, 480';
	}
?>
<!DOCTYPE html>
<html lang="en-US">
	<head>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="robots" content="noindex">
		<title>Advertisement</title>

		<style>
			.advert__wrap {
				margin-left: auto;
				margin-right: auto;
			}
			.advert_xs_250x208 .advert__wrap {
				width: 250px;
			}
			@media screen and ( min-width: 400px ){
				.advert_sm_640x480 .advert__wrap {
					width: 640px;
				}
			}
		</style>
		<script type='text/javascript'>
			(function() {
			var useSSL = 'https:' == document.location.protocol;
			var src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
			document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
			})();
		</script>
		<?php $adKw = class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::singleton()->generate_dfp_keywords() : ''; ?>
		<script type='text/javascript'>
			googletag.cmd.push( function() {
				googletag.defineSlot('<?php echo $adKw; ?>', [<?php echo $dimensions; ?>], '<?php echo $div_name; ?>' ).addService(googletag.pubads()).setTargeting('pos', 'interstitial');

				googletag.pubads().enableAsyncRendering();
				googletag.pubads().enableSingleRequest();
				googletag.enableServices();
			});
		</script>

	</head>
	<body style="margin:0 auto;">

		<section class="advert advert_xs_250x208 advert_sm_640x480 advert_location_interstitial">
			<div class="advert__wrap">
				<div id="<?php echo $div_name; ?>">
					<script type="text/javascript">googletag.cmd.push(function() { googletag.display("<?php echo $div_name; ?>"); });</script>
				</div>
			</div>
		</section>

	</body>
</html>
