<?php

//@header("HTTP/1.1 404 Not found", true, 404);
get_header();
$technical_theme_options	= get_option( 'cgi_media_technical_options' );
$accountKey					= $technical_theme_options['bing_search_appID'];
?>
	<div class="four_oh_four">
		<h1><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> Not Found</h1>

		<div class="content">
			<p>We are sorry but the requested content could not be found via the given link. Try doing a search or use the provided drop down menus or the menu above to find the content or send us an <a href="mailto:<?php echo get_bloginfo('admin_email') ?>">email</a>.</p>
			<p>You can also use this archive list, by month and year to help locate the content.</p>

			<form class="searchform" action="<?php bloginfo('url'); ?>/" method="GET">
				<select name="archive-dropdown" onChange="document.location.href=this.options[this.selectedIndex].value;">
					<option value=""><?php echo esc_attr(__('Select Month Year')); ?></option>
					<?php wp_get_archives('type=monthly&format=option'); ?>
				</select>
				<button type="submit">Go</button>
			</form>

			<p>Or you can browse the content by category.</p>
			<form class="searchform" action="<?php bloginfo('url'); ?>/" method="get">
				<?php
					$select = wp_dropdown_categories('show_option_none=Select Category&show_count=1&orderby=name&echo=0');
					$select = preg_replace("#<select([^>]*)>#", "<select$1 onchange='return this.form.submit()'>", $select);
					echo $select;
				?>
				<button type="submit">Go</button>
			</form>
			<?php
				$search_type	= "bing";
				$search_type	= $search_type . "_submit";
				$url			= "http://" . $_SERVER['HTTP_HOST']  . $_SERVER['REQUEST_URI'];
				$pieces			= explode("/", $url);
				$length			= count($pieces);
				$search_text	= $pieces[$length-1];
				$search_text	= str_replace("-", " ", $search_text);

				$page					= 1;
				$offset					= 0;
				$type					= 'web';
				$results_per_page_web	= 10;
			?>
			<form class="searchform" method="post" action="<?php echo site_url(); ?>/search?type=web&searchpage=1&q=<?php echo $search_text; ?>">
				<input type="text" id="searchText" name="searchText" value="<?php echo $search_text; ?>">
				<button type="submit" value="Search" name="<?php echo $search_type;?>" id="searchButton">Search</button>
			</form>
		</div>
	</div><!-- row 1/content -->
	<?php get_sidebar(); ?>
<?php get_footer(); ?>
