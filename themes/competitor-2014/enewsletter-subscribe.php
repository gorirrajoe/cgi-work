<?php
/* Template Name: Enewsletter Subscribe */
	$slug = basename(get_permalink());
	global $wpdb;
	get_header();

	// $technical_theme_options = get_option( 'cgi_media_technical_options' );
?>
	<div class="row">
		<div class="left_column">
			<div <?php post_class(); ?>>

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<!-- <h1><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> <?php the_title(); ?></h1> -->

					<div class="content">
						<?php the_content(); ?>
						<?php inline_iContact("inarticle"); ?>
					</div>

				<?php endwhile; else: ?>
					<div class="content">
						<p>Sorry, no posts matched your criteria.</p>
					</div>
				<?php endif; ?>

			</div>
		</div><!-- row 1/content -->

		<?php get_sidebar(); ?>

	</div>
<?php get_footer(); ?>
