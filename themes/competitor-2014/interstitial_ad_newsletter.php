<?php
	/**
	 * Template Name: Interstitial Ad Newsletter
	 */
?>
<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="robots" content="noindex">
		<title>Advertisement</title>
		<style>
			body {
				font-family: 'Open Sans', sans-serif;
				font-size: 16px;
				line-height: 23px;
				overflow: hidden;
				padding: 20px;
			}
			h3 {
				font-size: 1.75em;
				font-style: italic;
				font-weight: 300;
				margin-top: 0;
			}
			button {
				border: none;
				display: inline-block;
				font: italic 300 1em/1.42857143 'Open Sans', sans-serif;
				padding: 12px 10px;
				text-transform: uppercase;
				transition: all .25s ease;
			}
			#interstitial-ad {
				height: 340px;
			}
			.sub-leadin {
				font-style: italic;
				font-weight: bold;
			}
			.subscribe-form input.fields_email {
				background-color: #e6e7e8;
				border: 1px solid #e6e7e8;
				display: inline-block;
				font-family: 'Open Sans', sans-serif;
				font-size: 1.188em;
				outline: none;
				padding: 10px;
			}
			.subscribe-form .submit-button {
				color: #fff;
				background-color: #1c75bc;
			}
				.subscribe-form .submit-button:hover {
					background-color: #1e7dc9;
				}
			.subscribe-form .confirmMessage {
				background-color: #dfffdf;
				border: 1px solid darkgreen;
				color: #000;
				margin: 0 0 10px 0;
				padding: 8px;
			}
			.icontact_mini_links {
				font-size: 0.813em;
				font-style: italic;
				margin-bottom: 1em;
				margin-top: 1em;
			}
				.icontact_mini_links a {
					color: #141313;
					font-weight: bold;
					text-decoration: none;
				}
		</style>
	</head>
	<body>
		<div id="interstitial-ad">
			<?php inline_iContact( 'interstitial' ); ?>
		</div>

		<!-- need the JS code for the form -->
		<script src="//code.jquery.com/jquery-1.8.3.min.js"></script>
		<script src="<?php echo get_bloginfo( 'stylesheet_directory' ) .'/js/modules/subscribe-widget.js'; ?>"></script>
	</body>
</html>
