<?php
	/* Template Name: RnRMyFinisher Center*/
	get_header();
	$technical_theme_options = get_option( 'cgi_media_technical_options' );
	$is_mobile = is_mobile();
?>
	<!-- <div class="row">-->

		<!-- <div class="left_column"> -->
		<div class="content">
			<div <?php post_class( $classes ); ?>>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<div class="rnrr_banner">
						<h1><img class="rnrr_logo" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/images/rnr-logo-clean.svg" alt="Rock 'n' Roll Marathon Series"><?php the_title(); ?></h1>
						<?php if( ( $sponsor_logo = get_post_meta( get_the_ID(), '_sponsor_img', 1 ) ) != '' ) {
							if( ( $sponsor_url = get_post_meta( get_the_ID(), '_sponsor_url', 1 ) ) != '' ) {
								echo '<a href="'.$sponsor_url.'" target="_blank"><img class="sponsor_logo" src="'.$sponsor_logo.'"></a>';
							} else {
								echo '<img class="sponsor_logo" src="'.$sponsor_logo.'">';
							}
						} ?>
					</div>
					<?php if ( $is_mobile) { ?>
						<div id="mobile" class="cginetworkad">
							<script type="text/javascript">
								googletag.display("div-300_250_top");
							</script>
						</div>
					<?php }

					$rnr_twitter_handle = 'RunRockNRoll';
					//$social_media_key_publisher = "mtarf9ceu42js9q2ter9";
					if ( $social_media_key_publisher ) {
						echo '<div id="share-links">';
							$publisher_key = $social_media_key_publisher;
							$twitter_account = $rnr_twitter_handle; ?>

							<div id="share-links">
								<script>
									var pwidget_config = {
										shareQuote: false,
										onshare:bk_addSocialChannel,
										defaults: {
											gaTrackSocialInteractions: true,
											retina: true,
											mobileOverlay: true,
											afterShare: false,
											sharePopups: true
										}
									};

									function gaTrack(network, action) {
										_gaq.push(['_trackSocial', network, action]);
									}
									//Populate BlueKai social sharer
									function bk_addSocialChannel(channelName) {
										bk_addPageCtx('share', channelName);
										BKTAG.doTag(39226,1);
									}
								</script>

								<div id="sharingWidgetLarge">
									<div class="pw-widget pw-counter-horizontal" pw:twitter-via="<?php echo $twitter_account;?>">
										<a class="pw-button-facebook pw-look-native" onclick="gaTrack('Facebook','share')"></a>
										<a class="pw-button-twitter pw-look-native" onclick="gaTrack('Twitter','tweet')"></a>
										<a class="pw-button-googleplus pw-look-native" onclick="gaTrack('Google','+1')"></a>
										<a class="pw-button-pinterest pw-look-native" onclick="gaTrack('Pinterest','pin')"></a>
										<a class="pw-button-print pw-look-native pw-size-medium" onclick="gaTrack('Print','print')"></a>
									</div>
								</div>
								<div id="sharingWidgetSmall" style="display:none; " pw:twitter-via="<?php echo $twitter_account;?>">
									<div class="pw-widget pw-size-medium">
										<a class="pw-button-facebook pw-counter" onclick="gaTrack('Facebook','share')"></a>
										<a class="pw-button-twitter pw-counter" onclick="gaTrack('Twitter','tweet')"></a>
										<a class="pw-button-googleplus pw-counter" onclick="gaTrack('Google','+1')"></a>
										<a class="pw-button-pinterest pw-counter" onclick="gaTrack('Pinterest','pin')"></a>
										<a class="pw-button-print" onclick="gaTrack('Print','print')"></a>
									</div>
								</div>

								<script src="http://i.po.st/share/script/post-widget.js#init=immediate&amp;publisherKey=<?php echo $publisher_key;?>&amp;retina=true" type="text/javascript"></script>

								<script>
									jQuery(document).ready(function() {
										if(jQuery(window).width() < 482) {
											jQuery('#sharingWidgetLarge').hide();
											jQuery('#sharingWidgetSmall').show();
										}
										if(jQuery(window).width() > 482) {
											jQuery('#sharingWidgetLarge').show();
											jQuery('#sharingWidgetSmall').hide();
										}
									});

									jQuery(window).bind("resize", stickpw);
									function stickpw(e) {
										if(jQuery(window).width() < 482) {
											jQuery('#sharingWidgetLarge').hide();
											jQuery('#sharingWidgetSmall').show();
										}
										if(jQuery(window).width() > 482) {
											jQuery('#sharingWidgetLarge').show();
											jQuery('#sharingWidgetSmall').hide();
										}
									}
								</script>
							</div>
						</div>
					<?php } ?>

					<div class="content">
						<?php
							switch($_REQUEST['eId']) {
								case 83:
								case 88:
								case 80:
								case 85:
								case 86:
								case 89:
								case 82:
								case 84:
								case 81:
								case 79:
								case 87:
								case 91:
								case 59:
								case 36:
								case 58:
								case 35:
								case 77:
								case 37:
									//events not to show photos or finisher cert message
									$showPFMessage = 'N';
									break;
								default:
									$showPFMessage = 'Y';
									break;
							}
							if($showPFMessage == 'Y'): ?>
								<!-- <div style="padding: .625em; margin-bottom: 1.25em; background-color: #f3f8e4; border: 1px solid #6EB442; font-weight:600;">
									Click on your name in the search results to view your photos and finisher certificate download link. (2012 and newer events only)
								</div> -->
							<?php endif;

							include(ABSPATH. 'wp-content/plugins/results/frontend/rnrrace-results.php');
						?>
					</div>
				<?php endwhile; else: ?>
					<div class="content">
						<p>Sorry, no posts matched your criteria.</p>
					</div>
				<?php endif; ?>

			</div>
		</div><!-- row 1/content -->

	<!-- </div> -->
<?php get_footer(); ?>
