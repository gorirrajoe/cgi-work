<?php
	/**
	 * The template for displaying all Gift Guide category pages.
	 */
	get_header();


	if( is_category( 'competitor-gift-guide' ) ) {

		get_template_part( 'template-parts/gift-guides/content', 'competitor-gift-guide-cat' );

	} elseif( is_category( '2016-gift-guide' ) ) {

		get_template_part( 'template-parts/gift-guides/content', '2016-gift-guide-cat' );

	} else {

		// set up to automatically add future years
		$gift_guide_array	= array();
		$this_year			= date( 'Y' );


		for( $i = 2017; $i <= $this_year; $i++ ) {

			array_push( $gift_guide_array, $i );

		}

		$catid		= $wp_query->get_queried_object_id();
		$parents	= get_category_parents( $catid, false, '', true );

		foreach( $gift_guide_array as $key => $value ) {

			if( is_category( $value . '-gift-guide' ) || strpos( $parents, $value . '-gift-guide' ) !== false ) {
				get_template_part( 'template-parts/gift-guides/content', 'final-gift-guide-cat' );
			}

		}

	}
?>


<?php get_footer(); ?>

