<?php
/* Template Name: Running Calendar - v3 */
	$slug = basename(get_permalink());
	global $wpdb;

	get_header(); ?>

	<div class="special_category">
		<div class="row">
			<div class="category_left_column">
				<div class="blue_bg">
					<img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/category-race-calendar.png">
				</div>
				<div class="category_hdr">
					<h1 class="category_title">Race Calendar</h1>
					<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('special-category-sponsor') ) : else : endif; ?>
					<div class="btn_secondary">
						<a href="<?php echo site_url('/race-calendar/race-calendar-add-race/'); ?>">Add Race</a>
					</div>
				</div>
			</div>
			<div class="category_right_column">
				<div class="route_mid_column">
					<form class="race_search" action="<?php echo site_url(); ?>/<?php echo $slug; ?>" method="GET">
						<input type="hidden" value="1" name="result_page">
						<div class="row race_text_search">
							<ol>
								<li>
									<input id="race_search" class="cal-input" name="race_search" type="text" placeholder="Search by name or city" value="<?php echo stripslashes($_REQUEST['race_search']); ?>">
								</li>
								<li>
									<select name="select_state">
										<option value="all">State</option>
										<?php
											$myrows = $wpdb->get_results( "SELECT distinct event_state FROM wp_3_running_calendar where event_state is not null order by event_state" );
											foreach($myrows as $row){
												if($row->event_state != "") {
													$selected = '';
													if($row->event_state == $_REQUEST['select_state']) {
														$selected = 'selected = "selected"';
													}
													echo '<option '.$selected.' value="'.$row->event_state.'">'.$row->event_state.'</option>';
												}
											}
										?>
									</select>
								</li>
							</ol>
						</div>
						<div class="row race_date_range">
							<ol>
								<li>
									<input id="start_date" name="start_date" class="cal-input datepicker" type="text" placeholder="yyyy-mm-dd" value="<?php echo $_REQUEST['start_date']; ?>">
								</li>
								<li class="arrow_to"><span class="icon-right-open"></span><span class="icon-down-open"></span></li>
								<li>
									<input id="end_date" name="end_date" class="cal-input datepicker" type="text" placeholder="yyyy-mm-dd" value="<?php echo $_REQUEST['end_date']; ?>">
								</li>
							</ol>
						</div>
						<div class="row race_options_group">
							<?php
								if(isset($_REQUEST['race_type'])) {
									if(in_array('type_marathon', $_REQUEST['race_type'])) {
										$checked_marathon = 'checked="checked"';
									}
									if(in_array('type_5k', $_REQUEST['race_type'])) {
										$checked_5k = 'checked="checked"';
									}
									if(in_array('type_10k', $_REQUEST['race_type'])) {
										$checked_10k = 'checked="checked"';
									}
									if(in_array('type_half_marathon', $_REQUEST['race_type'])) {
										$checked_half_marathon = 'checked="checked"';
									}
									if(in_array('type_adventure_run', $_REQUEST['race_type'])) {
										$checked_adventure_run = 'checked="checked"';
									}
									if(in_array('type_multisport', $_REQUEST['race_type'])) {
										$checked_multisport = 'checked="checked"';
									}
								}
							?>
							<ol class="race_options">
								<li>
									<input type="checkbox" name="race_type[]" id="type_5k" value="type_5k" <?php echo $checked_5k; ?>><label for="type_5k"> 5k</label>
								</li>
								<li>
									<input type="checkbox" name="race_type[]" id="type_10k" value="type_10k" <?php echo $checked_10k; ?>><label for="type_10k"> 10k</label>
								</li>
								<li>
									<input type="checkbox" name="race_type[]" id="type_half_marathon" value="type_half_marathon" <?php echo $checked_half_marathon; ?>><label for="type_half_marathon"> half marathon</label>
								</li>
								<li>
									<input type="checkbox" name="race_type[]" id="type_marathon" value="type_marathon" <?php echo $checked_marathon; ?>><label for="type_marathon"> marathon</label>
								</li>
								<li>
									<input type="checkbox" name="race_type[]" id="type_multisport" value="type_multisport" <?php echo $checked_multisport; ?>><label for="type_multisport"> multisport</label>
								</li>
								<li class="last_option">
									<input type="checkbox" name="race_type[]" id="type_adventure_run" value="type_adventure_run" <?php echo $checked_adventure_run; ?>><label for="type_adventure_run"> adventure run</label>
								</li>
							</ol>
						</div>
						<button class="btn_race_search" type="submit" name="submit">Find Race</button>
						<span class="tab_indicator"></span>
					</form>
				</div>
				<div class="sidebar sidebar_sans_top_margin">
					<div class="hypnotoad_unit hypnotoad_300x250">
						<?php
							if(!$is_mobile){
								echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('side-top') : '';
							}
						?>
					</div><!-- ad unit -->
					<div class="hypnotoad_unit hypnotoad_300x600">
						<?php
							if(!$is_mobile){
								echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('side-middle') : '';
							}
						?>
					</div><!-- ad unit -->
				</div>
			</div>
		</div><!-- first group -->
		<div class="row">
			<?php
				if(isset($_REQUEST['submit']) || isset($_REQUEST['result_page'])) { // results based on user input
					if(!isset($_REQUEST['result_page'])) {
						$page_result = 1;
					} else {
						$page_result = $_REQUEST['result_page'];
					}
					if($_REQUEST['select_state']) {
						$state = $_REQUEST['select_state'];
					}
					$race_search = $_REQUEST['race_search'];
					$race_type = $_REQUEST['race_type'];
					$start_date = $_REQUEST['start_date'];
					$end_date = $_REQUEST['end_date'];
					if($start_date != '') {
						$start_date = date('Y-m-d', strtotime($_REQUEST['start_date']));
					}
					if($end_date != '') {
						$end_date = date('Y-m-d', strtotime($_REQUEST['end_date']));
					}

					$query = "SELECT * from wp_3_running_calendar WHERE";

					// set sql statements
					$race_search_sql = '';
					if($race_search) {
						$race_search_sql = ' (event_name LIKE \'%'. $race_search .'%\' OR event_city LIKE \'%'. $race_search .'%\')';
					}
					$state_sql = "";
					if($state != 'all'){
						$state_sql = ' event_state = "'.$state.'" ';
						$title .= "in ". $state ." ";
					}
					$race_type_sql = '';
					if( !empty( $race_type ) ) {
						$counter = 0;
						foreach( $race_type as $checked ) {
							if ( $counter == 0 ) {
								$query_and = '';
							} else {
								$query_and = ' OR ';
							}
							$race_type_sql .= $query_and .' '.$checked.'=1';
							$counter++;
						}
					}
					$start_date_sql = '';
					if($start_date) {
						$start_date_sql = ' event_date >= "'.$start_date.'" ';
					}
					$end_date_sql = '';
					if($end_date) {
						$end_date_sql = ' event_date <= "'.$end_date.'" ';
					}

					// enable sql statements based on user input
					if($race_search_sql) {
						$query .= $race_search_sql;
						if($state_sql) {
							$query .= ' AND '. $state_sql.'';
						}
						if($race_type_sql) {
							$query .= ' AND ('. $race_type_sql .')';
						}
						if($start_date_sql) {
							$query .= ' AND '. $start_date_sql;
						}
						if($end_date_sql) {
							$query .= ' AND '. $end_date_sql;
						}
					} elseif ($state_sql) {
						$query .= $state_sql;
						if($race_type_sql) {
							$query .= ' AND ('. $race_type_sql .')';
						}
						if($start_date_sql) {
							$query .= ' AND '. $start_date_sql;
						}
						if($end_date_sql) {
							$query .= ' AND '. $end_date_sql;
						}
					} elseif ($race_type_sql) {
						$query .= ' (' . $race_type_sql .')';
						if($start_date_sql) {
							$query .= ' AND '. $start_date_sql;
						}
						if($end_date_sql) {
							$query .= ' AND '. $end_date_sql;
						}
					} elseif ($start_date_sql) {
						$query .= $start_date_sql;
						if($end_date_sql) {
							$query .= ' AND '. $end_date_sql;
						}
					}
					if ( $start_date_sql == '' ) {
						$query .= ' AND event_date >= NOW()';
					}

					if($race_search == '' && $state_sql == '' && $race_type_sql == '' && $start_date_sql == '') {
						$query = 'SELECT * FROM wp_3_running_calendar WHERE event_date >= NOW()';
					}

					$query .= ' ORDER BY event_date';

					$myrows_count = $wpdb->get_results( $query );
					$query_count = count($myrows_count);

					$offset = 0;
					if(isset($_REQUEST['result_page']) && !empty($_REQUEST['result_page'])) {
						$offset =  ($page_result-1) * 20; // (page 2 - 1)*20 = offset of 20
					}

					$query .= ' LIMIT 20 OFFSET '. $offset;

					$page_query = "";
					if ($state) {
						$page_query .= "&select_state=".$state."";
					} else {
						$page_query .= "&select_state=all";
					}
					if($race_search) {
						$page_query .= '&race_search='.$race_search.'';
					} else {
						$page_query .= '&race_search=';
					}
					if(!empty($race_type)) {
						foreach( $race_type as $checked ) {
							$page_query .= '&race_type[]='.$checked;
						}
					}
					if($start_date) {
						$page_query .= '&start_date='.$start_date.'';
					} else {
						$page_query .= '&start_date=';
					}
					if($end_date) {
						$page_query .= '&end_date='.$end_date.'';
					} else {
						$page_query .= '&end_date=';
					}

					$event_search_details = $wpdb->get_results($query);
					$query_count_comma = number_format($query_count);
					echo '
						<h3 class="results_hdr"><img class="slashes slash_white_lg" src="'.get_bloginfo('stylesheet_directory').'/images/running/running-slashes-white-lg.svg"> Race Results - <strong>Found: '.$query_count_comma.'</strong></h3>
					';

					$num_per_page = 24;
					$total_pages = ceil($query_count/$num_per_page);
					$start_page = 1;
					$end_page = $total_pages + 1;
					if($total_pages > 5 ){
						if($page_result > 3){
							if($total_pages == $page_result){
								$start_page = $total_pages - 4;
							} else if($total_pages - $page_result < 2){
								$start_page = $total_pages - 5 + ($total_pages - $page_result);
							} else{
								$start_page = $page_result - 2;
							}
						}
					}
					if($total_pages > 5){
						if($start_page + 4 < $total_pages){
							$end_page = $start_page + 5;
						}
					}
					if($total_pages != 1) {
						echo '<div class="row">
							<ul class="cal_page_numbers">';
								for($i = $start_page; $i < $end_page; $i++){
									if($i == $page_result){
										echo '<li class="cal_current_page">Page '.$i.'</li>';
									} else {
										$link = '?result_page='. $i . $page_query;
										echo '<li><a href="'. $link .'">Page '.$i.'</a></li>';
									}
								}
							echo '</ul>
						</div>';
					}


					echo '<div class="archive">';
						$event_search_details = $wpdb->get_results($query);
						echo '<ul>';
							foreach($event_search_details as $event_search_detail) {
								echo '<li class="cal_post">';
									if($event_search_detail->event_date == '0000-00-00') {
										$event_date = 'TBD';
									} else {
										$event_month = date('M', strtotime($event_search_detail->event_date));
										$event_date = date('d', strtotime($event_search_detail->event_date));
										$event_year = date('Y', strtotime($event_search_detail->event_date));
										$event_date = '<span class="cal_event_day">' . $event_month . ' ' . $event_date . ' ' . $event_year . '</span>' ;
									}
									if ($event_search_detail->event_website) {
										$race_website_title = '<a href="http://'.$event_search_detail->event_website.'" target="_blank">'.$event_search_detail->event_name.'</a>';
									} else {
										$race_website_title = $event_search_detail->event_name;
									}
									if ($event_search_detail->featured_race == 1) {
										echo '<div class="featured_tag">Featured</div>';
									}
									echo '
										<h2 class="cal_post_name">'. $race_website_title .'</h2>
										<p class="cal_event_date">'. $event_date .'</p>
										<div class="cal_event_location">
											<p>
												<strong>'.$event_search_detail->event_city.', '.$event_search_detail->event_state.'</strong>';
												if ($event_search_detail->event_phone) {
													echo '<br>Phone: ' . $event_search_detail->event_phone.'<br>';
												}
												if ($event_search_detail->event_email) {
													echo '<br><a href="mailto:'.$event_search_detail->event_email.'">Email</a>';
												}
												if($event_search_detail->event_email && $event_search_detail->event_website) {
													echo ' | ';
												} elseif($result->event_email == '') {
													echo '<br>';
												}
												if ($event_search_detail->event_website) {
													if(strpos($event_search_detail->event_website, 'http') === FALSE) {
														$event_website = 'http://' . $event_search_detail->event_website;
													} else {
														$event_website = $event_search_detail->event_website;
													}
													echo '<a href="' . $event_website.'" target="_blank">Website</a>';
												}
											echo '</p>';
										echo '</div>';
										if ( $event_search_detail->type_marathon == 1
											|| $event_search_detail->type_5k == 1
											|| $event_search_detail->type_10k == 1
											|| $event_search_detail->type_half_marathon == 1
											|| $event_search_detail->type_multisport == 1
											|| $event_search_detail->type_adventure_run == 1) {
											echo '<ul class="race_tags">';
												if ($event_search_detail->type_adventure_run == 1) {
													echo '<li><a href="?result_page=1&race_search=&select_state=all&race_type[]=type_adventure_run&start_date=&end_date=">adventure run</a><?li>';
												}
												if ($event_search_detail->type_5k == 1) {
													echo '<li><a href="?result_page=1&race_search=&select_state=all&race_type[]=type_5k&start_date=&end_date=">5k</a></li>';
												}
												if ($event_search_detail->type_10k == 1) {
													echo '<li><a href="?result_page=1&race_search=&select_state=all&race_type[]=type_10k&start_date=&end_date=">10k</a></li>';
												}
												if ($event_search_detail->type_half_marathon == 1) {
													echo '<li><a href="?result_page=1&race_search=&select_state=all&race_type[]=type_half_marathon&start_date=&end_date=">half marathon</a></li>';
												}
												if ($event_search_detail->type_marathon == 1) {
													echo '<li><a href="?result_page=1&race_search=&select_state=all&race_type[]=type_marathon&start_date=&end_date=">marathon</a></li>';
												}
												if ($event_search_detail->type_multisport == 1) {
													echo '<li><a href="?result_page=1&race_search=&select_state=all&race_type[]=type_multisport&start_date=&end_date=">multisport</a></li>';
												}
											echo '</ul>';
										}
								echo '</li>';
							}
						echo '</ul>
					</div>';

					if($total_pages != 1) {
						echo '<div class="row">
							<ul class="cal_page_numbers">';
								for($i = $start_page; $i < $end_page; $i++){
									if($i == $page_result){
										echo '<li class="cal_current_page">Page '.$i.'</li>';
									} else {
										$link = '?result_page='. $i . $page_query;
										echo '<li><a href="'. $link .'">Page '.$i.'</a></li>';
									}
								}
							echo '</ul>
						</div>';
					}
				} else {  // when a user first lands on the page (maxmind stuffs!)

					$params = getopt('l:i:');

					if (!isset($params['l'])) $params['l'] = '2RZKLiR5msnj';
					$user_ip = get_user_ip();

					// for testing
					$url = site_url();
					$is_testing = strpos($url, 'cgitesting');
					if($is_testing !== false) {
						$user_ip = '70.183.95.62';
					}

					if (!isset($params['i'])) $params['i'] = $user_ip;

					$maxmind_query = 'https://geoip.maxmind.com/f?' . http_build_query($params);

					$insights_keys =
						array(
							'country_code',
							'region_code',
							'city_name',
							'postal_code',
							'latitude',
							'longitude',
							'metro_code',
							'area_code',
							'isp_name',
							'organization_name',
							'error',
							'time_zone',
							'continent_code',
							'domain',
							'as_number',
							'netspeed',
							'user_type',
							'accuracy_radius',
							'country_confidence',
							'city_confidence',
							'region_confidence',
							'postal_confidence',
							'region_name'
						);

					$curl = curl_init();
					curl_setopt_array(
						$curl,
						array(
							CURLOPT_URL => $maxmind_query,
							CURLOPT_USERAGENT => 'MaxMind PHP Sample',
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_SSL_VERIFYPEER => false
						)
					);

					$resp = curl_exec($curl);

					if (curl_errno($curl)) {
						throw new Exception(
							'GeoIP request failed with a curl_errno of '
							. curl_errno($curl)
						);
					}

					//See results
					$insights_values = str_getcsv($resp);
					$insights_values = array_pad($insights_values, sizeof($insights_keys), '');
					$insights = array_combine($insights_keys, $insights_values);

					$center_lat = $insights['latitude'];
					$center_lng = $insights['longitude'];
					$radius = 100;
					$limit = 20;
					$race_type = $_REQUEST['race_type'];

					if(!isset($_REQUEST['local_results'])) {
						$local_results = 1;
					} else {
						$local_results = $_REQUEST['local_results'];
					}
					$race_type_sql = '';
					if( !empty( $race_type ) ) {
						$race_type_sql .= $checked.'=1';
					}

					$default_query = "SELECT id, event_name, event_date, event_category, event_distance, event_location, event_city, event_state, event_email, event_phone, event_website, event_lat, event_lng, type_marathon, type_5k, type_10k, type_half_marathon, type_multisport, type_adventure_run, featured_race, ( 3959 * acos( cos( radians(".$center_lat.") ) * cos( radians( event_lat ) ) * cos( radians( event_lng ) - radians(".$center_lng.") ) + sin( radians(".$center_lat.") ) * sin( radians( event_lat ) ) ) ) AS distance FROM wp_3_running_calendar WHERE";

					/* testing
					echo '<pre>';
						print_r($insights);
					echo '</pre>';
					echo '<br><br><br>';
					*/
					if($race_type_sql) {
						$default_query .= ' (' . $race_type_sql .') AND';
					}

					$default_query .= " event_date >= NOW() HAVING distance < ".$radius." ORDER BY event_date";
					$results_sans_limit = $wpdb->get_results($default_query);

					// determine count before we limit it
					$default_query_count = count($results_sans_limit);
					// make the count look pretty (commas)
					$default_query_count_comma = number_format($default_query_count);

					$offset = 0;
					if(isset($_REQUEST['local_results']) && !empty($_REQUEST['local_results'])) {
						$offset =  ($local_results-1) * $limit; // (page 2 - 1)*20 = offset of 20
					}

					$default_query .= ' LIMIT '.$limit.' OFFSET '. $offset;
					$results = $wpdb->get_results($default_query);

					/* testing
					echo '<pre>';
					print_r($results);
					echo '</pre>';
					*/

					echo '
						<h3 class="results_hdr"><img class="slashes slash_white_lg" src="'.get_bloginfo('stylesheet_directory').'/images/running/running-slashes-white-lg.svg"> Race Events Near You - <strong>Found: '.$default_query_count_comma.'</strong></h3>
					';

					$num_per_page = $limit;
					$total_pages = ceil($default_query_count/$num_per_page);
					$start_page = 1;
					$end_page = $total_pages + 1;
					if($total_pages > 5 ){
						if($local_results > 3){
							if($total_pages == $local_results){
								$start_page = $total_pages - 4;
							} else if($total_pages - $local_results < 2){
								$start_page = $total_pages - 5 + ($total_pages - $local_results);
							} else{
								$start_page = $local_results - 2;
							}
						}
					}
					if($total_pages > 5){
						if($start_page + 4 < $total_pages){
							$end_page = $start_page + 5;
						}
					}
					if($total_pages != 1) {
						echo '<div class="row">
							<ul class="cal_page_numbers">';
								for($i = $start_page; $i < $end_page; $i++){
									if($i == $local_results){
										echo '<li class="cal_current_page">Page '.$i.'</li>';
									} else {
										$link = '?local_results='. $i . $page_query;
										echo '<li><a href="'. $link .'">Page '.$i.'</a></li>';
									}
								}
							echo '</ul>
						</div>';
					}


					echo '<div class="archive">';
						echo '<ul>';
							foreach($results as $result) {
								echo '<li class="cal_post">';
									if($result->event_date == '0000-00-00') {
										$event_date = 'TBD';
									} else {
										$event_month = date('M', strtotime($result->event_date));
										$event_date = date('d', strtotime($result->event_date));
										$event_year = date('Y', strtotime($result->event_date));
										$event_date = '<span class="cal_event_day">' . $event_month . ' ' . $event_date . ' ' . $event_year . '</span>' ;
									}
									if ($result->event_website) {
										$race_website_title = '<a href="http://'.$result->event_website.'" target="_blank">'.$result->event_name.'</a>';
									} else {
										$race_website_title = $result->event_name;
									}
									if ($result->featured_race == 1) {
										echo '<div class="featured_tag">Featured</div>';
									}
									echo '
										<h2 class="cal_post_name">'. $race_website_title .'</h2>
										<p class="cal_event_date">'. $event_date .'</p>
										<div class="cal_event_location">
											<p>
												<strong>'.$result->event_city.', '.$result->event_state.'</strong>';
												if ($result->event_phone) {
													echo '<br>Phone: ' . $result->event_phone;
												}
												if ($result->event_email) {
													echo '<br><a href="mailto:'.$result->event_email.'">Email</a>';
												}
												if($result->event_email && $result->event_website) {
													echo ' | ';
												} elseif($result->event_email == '') {
													echo '<br>';
												}
												if ($result->event_website) {
													echo '<a href="http://'.$result->event_website.'" target="_blank">Website</a>';
												}
											echo '</p>';
										echo '</div>';
										if ( $result->type_marathon == 1
											|| $result->type_5k == 1
											|| $result->type_10k == 1
											|| $result->type_half_marathon == 1
											|| $result->type_multisport == 1
											|| $result->type_adventure_run == 1) {
											echo '<ul class="race_tags">';
												if ($result->type_adventure_run == 1) {
													echo '<li><a href="?local_results=1&race_type[]=type_adventure_run">adventure run</a><?li>';
												}
												if ($result->type_5k == 1) {
													echo '<li><a href="?local_results=1&race_type[]=type_5k">5k</a></li>';
												}
												if ($result->type_10k == 1) {
													echo '<li><a href="?local_results=1&race_type[]=type_10k">10k</a></li>';
												}
												if ($result->type_half_marathon == 1) {
													echo '<li><a href="?local_results=1&race_type[]=type_half_marathon">half marathon</a></li>';
												}
												if ($result->type_marathon == 1) {
													echo '<li><a href="?local_results=1&race_type[]=type_marathon">marathon</a></li>';
												}
												if ($result->type_multisport == 1) {
													echo '<li><a href="?local_results=1&race_type[]=type_multisport">multisport</a></li>';
												}
											echo '</ul>';
										}
								echo '</li>';
							}
						echo '</ul>
					</div>';

					if($total_pages != 1) {
						echo '<div class="row">
							<ul class="cal_page_numbers">';
								for($i = $start_page; $i < $end_page; $i++){
									if($i == $local_results){
										echo '<li class="cal_current_page">Page '.$i.'</li>';
									} else {
										$link = '?local_results='. $i . $page_query;
										echo '<li><a href="'. $link .'">Page '.$i.'</a></li>';
									}
								}
							echo '</ul>
						</div>';
					}
				}


			?>

			<script>
				jQuery(function($) {
					$( "#start_date" ).datepicker({
						numberOfMonths: 1,
						dateFormat: 'yy-mm-dd',
						onClose: function( selectedDate ) {
							$( "#end_date" ).datepicker( "option", "minDate", selectedDate );
						}
					});
					$( "#end_date" ).datepicker({
						dateFormat: 'yy-mm-dd',
						numberOfMonths: 1
					});
				});
			</script>
		</div>
		<!-- end code -->

	</div>
<?php get_footer(); ?>
