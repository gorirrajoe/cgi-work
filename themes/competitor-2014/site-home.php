<?php
	/*
	 * Template Name: Site Homepage
	 */
	get_header();

	$technical_theme_options = get_option( 'cgi_media_technical_options' );
	$editorial_theme_options = get_option( 'cgi_media_editorial_options' );

	$is_mobile	= is_mobile();
	$is_tablet	= is_tablet2();

	// get all recent posts at once
	$newspostsArray = get_recent_home_posts();

	if ( !empty( $technical_theme_options['grd_section_pixlee_code'] ) ) : ?>

		<div class="row grd-section">

			<?php if ( !empty( $technical_theme_options['grd_section_heading'] ) ) { ?>
				<h3><img class="slashes slash_blue" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/images/running/running-slashes-blue.svg"> <a href="<?php echo $technical_theme_options['grd_section_url'] ?>"><?php echo $technical_theme_options['grd_section_heading'] ?> <span class="icon-right-open"></span></a></h3>
			<?php } ?>

			<?php if ( !empty( $technical_theme_options['grd_section_pixlee_code'] ) ) {
				echo $technical_theme_options['grd_section_pixlee_code'];
			} ?>
		</div>

	<?php endif;
?>


<div class="row">
	<h3><img class="slashes slash_blue" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/images/running/running-slashes-blue.svg"> The Latest <span class="hidden">in Training, News &amp; Nutrition</span></h3>

	<?php
		// show first 5 for carousel
		get_recent_posts_home( 1, $newspostsArray );
	?>

	<div class="home_news_feed module">
		<h3><img class="slashes slash_blue" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/images/running/running-slashes-blue.svg"> Top Stories</h3>
		<?php if ( function_exists( 'dynamic_sidebar' ) && dynamic_sidebar( 'hot-topics' ) ) : else : endif; ?>
	</div>

	<?php inline_iContact( 'home' ); ?>

</div><!-- row 1 -->


<div class="row">
	<?php
		// show the remaining 3 posts underneath carousel
		get_recent_posts_home( 2, $newspostsArray );
	?>
	<div class="hypnotoad_col">
		<?php echo class_exists( 'Wp_Dfp_Ads' ) ? Wp_Dfp_Ads::display_ad( 'side-top' ) : ''; ?>
	</div><!-- ad unit -->
</div><!-- row 1.5 -->


<?php if ( is_active_sidebar( 'marquee' ) ) { ?>
	<div class="row">

		<div class="module offers">

			<h3><img class="slashes slash_blue" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/images/running/running-slashes-blue.svg"> <span class="hidden">Get in on the latest Info, Races & Gear!</span><span class="visible">Running Resources</span></h3>
			<ul id="offers_carousel">
				<?php if ( function_exists( 'dynamic_sidebar' ) && dynamic_sidebar( 'marquee' ) ) : else : endif; ?>
			</ul>

		</div>

	</div><!-- row 2 -->
<?php }


if ( $is_mobile || $is_tablet ) { ?>

	<div class="row">
		<?php echo class_exists( 'Wp_Dfp_Ads' ) ? Wp_Dfp_Ads::display_ad( 'side-mobile' ) : ''; ?>
	</div>

<?php }


// START PARTNER CONNECT
if ( $technical_theme_options['partner_connect_ch'] ) {

	if ( get_category_by_slug( 'sponsored' ) !== false ) {
		$obtain_slug	= get_category_by_slug( 'sponsored' );
		$url_target		= 'sponsored';
	} else {
		$obtain_slug	= get_category_by_slug( 'partnerconnect' );
		$url_target		= 'partnerconnect';
	}

	$pc_catid = $obtain_slug->term_id; ?>

	<div class="row btmspace">
		<h3 class="sponsored-label"><img class="slashes slash_blue" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/images/running/running-slashes-blue.svg"> Sponsored Content</h3>
		<div class="partner_row">
			<?php echo print_pc_posts( $pc_catid,  true, true, true ); ?>
		</div>
	</div>


	<div class="row">
		<div class="more_link_arrow light_bg module">
			<a href="/category/<?php echo $url_target; ?>" title="Partner Connect" class="sponsored-label">More Sponsored</a>
			<a href="/category/<?php echo $url_target; ?>" title="Partner Connect"><span class="icon-right-open"></span></a>
		</div>
	</div>

<?php } // END PARTNER CONNECT ?>


<div class="row">
	<div class="module videos_module">
		<h3><img class="slashes slash_blue" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/images/running/running-slashes-blue.svg"> <span class="hidden">Watch the Latest</span> Videos</h3>
		<?php
			$video_cat_id = $technical_theme_options['video_category'] ? $technical_theme_options['video_category'] : 15;
			get_videos_home( $video_cat_id );
		?>
	</div>

	<div class="module photos_module">
		<h3><img class="slashes slash_blue" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/images/running/running-slashes-blue.svg"> Photo Galleries</h3>
		<?php
			if ( $technical_theme_options['gallery_category'] ) {
				$photo_cat_id = $technical_theme_options['gallery_category'];
			}
			get_photos_home( $photo_cat_id );
		?>
	</div>
</div><!-- row 3 -->


<?php get_footer(); ?>
