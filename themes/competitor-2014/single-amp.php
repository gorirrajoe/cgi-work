<?php
	global $multipage;

	$technical_theme_options = get_option( 'cgi_media_technical_options' );
	$editorial_theme_options = get_option( 'cgi_media_editorial_options' );

	$photos = false;

	if ( !empty( $technical_theme_options['gallery_category'] ) ) {
		$gallery_cat_array = explode( ',', $technical_theme_options['gallery_category'] );

		if ( in_category( $gallery_cat_array ) ) {
			$photos = true;
		}
	}
?>
<!doctype html>
<html amp <?php echo AMP_HTML_Utils::build_attributes_string( $this->get( 'html_tag_attributes' ) ); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php do_action( 'amp_post_template_head', $this ); ?>
	<style amp-custom><?php do_action( 'amp_post_template_css', $this ); ?></style>
</head>

<body <?php body_class('amp'); ?>>

	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'running' ); ?></a>

	<header class="header">
		<div class="container header__container">
			<div class="row">
				<div class="col-xs-6">
					<div class="header__logo">
						<a href="<?php echo site_url( '/' ); ?>">
							<?php global $post;

								$secondary_logo_categories_string	= $editorial_theme_options['secondary_logo_categories'];
								$secondary_logo_categories			= explode( ',', $secondary_logo_categories_string );
								if ( !empty( $secondary_logo_categories ) && in_category( $secondary_logo_categories ) && is_category( $secondary_logo_categories ) ) {
									$site_logo = $editorial_theme_options['secondary_logo'];
								} else {
									$site_logo = ($editorial_theme_options['logo']) ? $editorial_theme_options['logo'] : get_bloginfo('stylesheet_directory') . '/images/running/competitor-logo.svg';
								}
							 ?>
							<amp-img src="<?php echo $site_logo; ?>" alt="Competitor Logo" height="35" width="120"></amp-img>
						</a>
					</div>
				</div>
				<div class="col-xs-6">
					<nav id="social-nav" class="social-nav">
						<ul class="social_icons">
						<?php if ( !empty( $technical_theme_options['site_facebook_page'] ) ) { ?>
							<li>
								<a target="_blank" href="https://facebook.com/<?php echo $technical_theme_options['site_facebook_page']; ?>" title="Like us on Facebook">
									<span class="fa fa-facebook"></span>
								</a>
							</li>
						<?php }
						if ( !empty( $technical_theme_options['site_twitter_handle'] ) ) { ?>
							<li>
								<a target="_blank" href="https://twitter.com/<?php echo $technical_theme_options['site_twitter_handle']; ?>" title="Follow us on Twitter">
									<span class="fa fa-twitter"></span>
								</a>
							</li>
						<?php }
						if ( !empty( $technical_theme_options['site_instagram_handle'] ) ) { ?>
							<li>
								<a target="_blank" href="http://instagram.com/<?php echo $technical_theme_options['site_instagram_handle']; ?>" title="Follow us on Instagram">
									<span class="fa fa-instagram"></span>
								</a>
							</li>
						<?php }
						if ( !empty( $technical_theme_options['youtube_page'] ) ) { ?>
							<li>
								<a target="_blank" href="<?php echo $technical_theme_options['youtube_page']; ?>" title="Follow us on YouTube">
									<span class="fa fa-youtube"></span>
								</a>
							</li>
						<?php } ?>
					</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>

	<section id="content" class="content template--single">
		<div class="container content__container">
			<div class="row">

				<?php
				if ( have_posts() ) : while( have_posts() ) : the_post();

					if ( $photos ) {
						include( locate_template( 'template-parts/amp-photos.php' ) );
					} elseif ( in_category( 'video' )) {
						include( locate_template( 'template-parts/amp-old-video.php' ) );
						// get_template_part( 'template-parts/amp', 'old-video' );
					} elseif ( $multipage ) {
						include( locate_template( 'template-parts/amp-multipage.php' ) );
						// get_template_part( 'template-parts/amp', 'multipage' );
					} else {
						include( locate_template( 'template-parts/amp.php' ) );
						// get_template_part( 'template-parts/amp', get_post_format() );
					}

				endwhile; endif;

				echo do_shortcode( '[related location="article-footer"]' );
				?>

			</div>
		</div>
	</section>

	<footer class="footer">
		<div class="footer__copyright">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<p>&copy; <?php echo date( 'Y' );?> POCKET OUTDOOR MEDIA, LLC. ALL RIGHTS RESEVED.</p>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<?php do_action( 'amp_post_template_footer', $this ); ?>

</body>
</html>
