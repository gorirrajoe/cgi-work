<?php
	/**
	 * The template for displaying all Gift Guide category pages.
	 */
	get_header();


	if( is_category( '2015-running-gear-guide' ) ) {

		get_template_part( 'template-parts/gear-guides/content', '2015-running-gear-guide-cat' );

	} elseif( is_category( '2016-running-gear-guide' ) ) {

		get_template_part( 'template-parts/gear-guides/content', '2016-running-gear-guide-cat' );

	} elseif( is_category( '2017-running-gear-guide' ) ) {

		get_template_part( 'template-parts/gear-guides/content', '2017-running-gear-guide-cat' );

	} else {

		// set up to automatically add future years
		$gift_guide_array	= array();
		$this_year			= date( 'Y' );


		for( $i = 2018; $i <= $this_year; $i++ ) {

			array_push( $gift_guide_array, $i );

		}

		$catid		= $wp_query->get_queried_object_id();
		$parents	= get_category_parents( $catid, false, '', true );

		foreach( $gift_guide_array as $key => $value ) {

			if( is_category( $value . '-running-gear-guide' ) || strpos( $parents, 'guide' ) !== false ) {
				get_template_part( 'template-parts/gear-guides/content', 'final-running-gear-guide-cat' );
			}

		}

	}
?>


<?php get_footer(); ?>

