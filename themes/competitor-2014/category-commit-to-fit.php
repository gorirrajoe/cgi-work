<?php
/* OLYMPIC GAMES
custom-category.php will handle displaying sponsored category pages
use variables below to set custom options
copy and place file into child theme with corresponding cat id in name - category-###.php */

define ("category_template", "custom");
$youtube_playlist = 'PL7Bo6jJLBzKEe8exxdkQn4qvAD_Vkrd38';

if(!isset($technical_theme_options) OR empty($technical_theme_options)){
	$technical_theme_options = get_option( 'cgi_media_technical_options' );
}

get_header();
global $post_id_array;

$post_id_array = array();
$this_category = get_category($cat);
$category_name = $this_category->cat_name;
$is_mobile		= is_mobile();

if (!(get_query_var('paged') > 1)) { ?>
	<div class="special_category">
		<h1><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> All <?php echo $category_name; ?> Page: 1</h1>

		<div class="row">
			<div class="left_column">

				<?php get_recent_category_posts($this_category->term_id, 8, 1); ?>

				<div class="row">
					<h3><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> Competitor On Demand</h3>

					<?php get_youtube_playlist($youtube_playlist); ?>

				</div>

				<?php /*
				<div class="row">
					<h3><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> Photos</h3>
					<?php get_photos_category($this_category->term_id); ?>
				</div>
				*/ ?>
			</div>

			<div class="sidebar sidebar_sans_top_margin">

				<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('category-sponsor') ) : else : endif; ?>

				<div class="news_feed module">
					<h3><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> Top Stories</h3>

					<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('hot-topics')) : else : endif; ?>

				</div><!-- news feed -->

				<div class="hypnotoad_unit hypnotoad_300x250">
					<?php
						if(!$is_mobile){
							// if (function_exists('dynamic_sidebar') && dynamic_sidebar('atf-300x250') ) : else : endif;
							echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('side-top') : '';
						}
					?>
				</div><!-- ad unit -->

				<div class="module offers">
					<h3><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> Running Resources</h3>

					<ul id="sidebar_offers_carousel">
						<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('marquee') ) : else : endif; ?>
					</ul>

				</div><!-- offers carousel -->

				<div class="hypnotoad_unit hypnotoad_300x600">
					<?php
						if(!$is_mobile){
							// if (function_exists('dynamic_sidebar') && dynamic_sidebar('desktop-300x600') ) : else : endif;
							echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('side-middle') : '';
						}
					?>
				</div><!-- ad unit -->

			</div>
		</div>
	</div>
<?php } else {
	$page = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>

	<div class="left_column">
		<div>
			<?php
				$page = (get_query_var('paged')) ? get_query_var('paged') : 1;
				echo '<h1><img class="slashes slash_blue" src="'.get_bloginfo('stylesheet_directory').'/images/running/running-slashes-blue.svg"> All Posts Categorized &#8216;' . single_cat_title('', false) . '&#8217;: Page ' . $page . '</h1>';

				$count = 1;

				if ( have_posts() ) : while ( have_posts() ) : the_post();
					$permalink		= get_permalink();
					$title			= get_the_title();
					$excerpt		= get_the_excerpt();
					$post_classes	= get_post_class();

					if (has_post_thumbnail ()) {
						if($is_mobile){
							$image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'marquee-carousel-mobile');
						} else {
							if($count == 1) {
								$image_array = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'single-post-big');
							} else {
								$image_array = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail');
							}
						}
						$post_thumb = $image_array[0];
					} else {
						if($count == 1) {
							$post_thumb = get_bloginfo('stylesheet_directory').'/images/running/default-post-thumbnail-lg.jpg';
						} else {
							$post_thumb = get_bloginfo('stylesheet_directory').'/images/running/default-post-thumbnail-sm.jpg';
						}
					}
					$post_classes_string	= implode(" ", $post_classes);

					$month_published		= get_the_time('M');
					$day_published			= get_the_time('d');

					if ($count == 1 && (get_query_var('paged') < 3)) {  // first post is a big one
						echo '
							<div class="featured_post '.$post_classes_string.'">
								<div class="featured_img">
									<a class="overlay" href="'.$permalink.'"></a>
									<img src="'.$post_thumb.'">
								</div>
								<div class="row featured_vitals">
									<div class="featured_post_date">
										<span class="featured_post_month">'.$month_published.'</span>
										<span class="featured_post_day">'.$day_published.'</span>
									</div>
									<div class="featured_title_author">
										<h3><a href="'.$permalink.'">'.$title.'</a></h3>
										<p class="post_author">by '.get_guest_author().'</p>
										<p class="excerpt">'.$excerpt.'</p>
									</div>
									<a class="arrow" href="'.$permalink.'"><span class="icon-right-open"></span></a>
								</div>
							</div>
						';
					} else {
						$thumb = '<div class="featured_img_archive"><a class="overlay" href="' . $permalink . '" title="' . $title . '"></a>' . '<img class="archive_thumb" src="'.$post_thumb.'"></div>';
						echo '
							<div class="row archive_row '.$post_classes_string.'">
								'.$thumb.'
								<h4><a href="'.$permalink.'">'.$title.'</a></h4>
								<p class="post_author">by '.get_guest_author().', '.format_timestamp_2().'</p>
								<p class="excerpt">'.$excerpt.'</p>
							</div>
						';
					}
					$count++;

				endwhile; else: ?>
					<div class="content">
						<p>Sorry, no posts matched your criteria.</p>
					</div>
				<?php endif;
			?>

		</div>

		<?php
			echo '<div class="row archive_navi">
				<div class="btn_primary">
					'.get_previous_posts_link('<span class="icon-left-open"></span> Previous Page').'
				</div>
				<div class="btn_primary">
					'.get_next_posts_link('Next Page <span class="icon-right-open"></span>').'
				</div>
			</div>';
		?>

	</div><!-- row 1/content -->
	<?php get_sidebar();
}

get_footer(); ?>
