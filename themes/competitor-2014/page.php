<?php
	$technical_theme_options = get_option( 'cgi_media_technical_options' );
	get_header();
?>
	<div class="row">
		<div class="left_column">
			<div <?php post_class(); ?>>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<h1><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> <?php the_title(); ?></h1>
						<div id="share-links">
							<?php
								if ($technical_theme_options['social_media_publisher_key'] && $technical_theme_options['social_media_publisher_key'] != ""){
									$publisher_key = $technical_theme_options['social_media_publisher_key'];
									$twitter_account = $technical_theme_options['site_twitter_handle']; ?>

									<div id="share-links">
								 		<script>
										var pwidget_config = {
											shareQuote: false,
											onshare:bk_addSocialChannel,
											defaults: {
												gaTrackSocialInteractions: true,
												retina: true,
												mobileOverlay: true,
												afterShare: false,
												sharePopups: true
											}
										};

								 			function gaTrack(network, action) {
								 				_gaq.push(['_trackSocial', network, action]);
								 			}
								 			//Populate BlueKai social sharer
										  function bk_addSocialChannel(channelName) {
										   	bk_addPageCtx('share', channelName);
										    BKTAG.doTag(39226,1);
										  }


								 		</script>

										<div id="sharingWidgetLarge">
											<div class="pw-widget pw-counter-horizontal" pw:twitter-via="<?php echo $twitter_account;?>">
												<a class="pw-button-facebook pw-look-native" onclick="gaTrack('Facebook','share')"></a>
												<a class="pw-button-twitter pw-look-native" onclick="gaTrack('Twitter','tweet')"></a>
												<a class="pw-button-googleplus pw-look-native" onclick="gaTrack('Google','+1')"></a>
												<a class="pw-button-pinterest pw-look-native" onclick="gaTrack('Pinterest','pin')"></a>
												<a class="pw-button-print pw-look-native pw-size-medium" onclick="gaTrack('Print','print')"></a>
											</div>
										</div>
										<div id="sharingWidgetSmall" style="display:none; " pw:twitter-via="<?php echo $twitter_account;?>">
											<div class="pw-widget pw-size-medium">
												<a class="pw-button-facebook pw-counter" onclick="gaTrack('Facebook','share')"></a>
												<a class="pw-button-twitter pw-counter" onclick="gaTrack('Twitter','tweet')"></a>
												<a class="pw-button-googleplus pw-counter" onclick="gaTrack('Google','+1')"></a>
												<a class="pw-button-pinterest pw-counter" onclick="gaTrack('Pinterest','pin')"></a>
												<a class="pw-button-print" onclick="gaTrack('Print','print')"></a>
											</div>
										</div>

                  <script src="http://i.po.st/share/script/post-widget.js#init=immediate&amp;publisherKey=<?php echo $publisher_key;?>&amp;retina=true" type="text/javascript"></script>


										<script>
										jQuery(document).ready(function() {
											if(jQuery(window).width() < 482){
												jQuery('#sharingWidgetLarge').hide();
												jQuery('#sharingWidgetSmall').show();
											}
											if(jQuery(window).width() > 482){
												jQuery('#sharingWidgetLarge').show();
												jQuery('#sharingWidgetSmall').hide();
											}
										});

										jQuery(window).bind("resize", stickpw);
										function stickpw(e){
											if(jQuery(window).width() < 482){
												jQuery('#sharingWidgetLarge').hide();
												jQuery('#sharingWidgetSmall').show();
											}
											if(jQuery(window).width() > 482){
												jQuery('#sharingWidgetLarge').show();
												jQuery('#sharingWidgetSmall').hide();
											}
										}
										</script>
									</div>
								<?php }
							?>
						</div>

					<div class="content">
						<?php the_content(); ?>
					</div>
				<?php endwhile; else: ?>
					<div class="content">
						<p>Sorry, no posts matched your criteria.</p>
					</div>
				<?php endif; ?>

			</div>
		</div><!-- row 1/content -->
		<?php get_sidebar(); ?>
	</div>
<?php get_footer(); ?>
