<?php 
  /* Template Name: Modular */ 

  get_header();
?>
  <div <?php post_class($classes); ?>>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <div class="content">
        <?php the_content(); ?>
      </div>
    <?php endwhile; else: ?>
      <div class="content">
        <p>Sorry, no posts matched your criteria.</p>	
      </div>
    <?php endif; ?>

  </div><!-- row 1/content -->
<?php get_footer(); ?>