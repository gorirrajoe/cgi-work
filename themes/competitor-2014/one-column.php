<?php
	/* Template Name: One Column */

	get_header();
?>

<div <?php post_class(); ?>>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<h1><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> <?php the_title(); ?></h1>

		<div id="article-left">
			<div id="share-links-left">
				<?php print_social_media( 'left' ); ?>
			</div>
		</div>

		<div id="article-right" class="content">

			<div id="share-links">
				<?php print_social_media(); ?>
			</div>

			<div class="content">
				<?php the_content(); ?>
			</div>
		</div>

	<?php endwhile; else: ?>

		<div class="content">
			<p>Sorry, no posts matched your criteria.</p>
		</div>

	<?php endif; ?>

</div><!-- row 1/content -->

<?php get_footer(); ?>