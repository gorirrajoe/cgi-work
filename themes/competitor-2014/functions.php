<?php
// error_reporting( E_ALL );
// ini_set('display_errors', true);

/**
 * Require files
 */
// Theme
require_once 'inc/running-setup-theme.php';
require_once 'inc/running-setup-admin.php';
require_once 'inc/header-scripts.php';
require_once 'inc/footer-scripts.php';
require_once 'inc/running-shortcodes.php';
require_once 'inc/running-theme-settings.php';

// Menus/Navigation
require_once 'inc/running-menus.php';

// Mods & Enhancements
require_once 'inc/fbia-mods.php';
require_once 'inc/competitor-ads.php';
require_once 'inc/amp-mods.php';
require_once 'inc/interstitials.php';

// Guides
require_once 'inc/running-gift-guide.php';
require_once 'inc/running-gear-guide.php';
require_once 'inc/running-gg-walker.php';

/**
 * Initiate classes
 */
// Theme
Running_Setup_Theme::singleton();
Running_Setup_Admin::singleton();
Header_Scripts::singleton();
Footer_Scripts::singleton();
Running_Shortcodes::singleton();
Running_Theme_Settings::singleton();

// Header_Scripts::singleton();
// Footer_Scripts::singleton();
// Running_Shortcodes::singleton();
// Running_Theme_Settings::singleton();

// Menus/Navigation
Running_Menus::singleton();

// Mods & Enhancements
Competitor_Ads::singleton();
FBIA_Mods::singleton();
AMP_Mods::singleton();
Interstitials::singleton();

// Guides
GiftGuide_Admin::singleton();
GearGuide_Admin::singleton();
// add_action( 'init', array( 'GiftGuide_Admin', 'singleton' ) );


// MOBILE DETECT
function is_mobile(){
	require_once (ABSPATH . 'wp-content/includes/mobiledetect/mobile_detect.php');
	$detect = new Mobile_Detect;

	// Any mobile device (phones or tablets).
	if ( $detect->isMobile() && !$detect->isTablet()) {
		return true;
	}
	return false;
}

function is_tablet(){
	require_once (ABSPATH . 'wp-content/includes/mobiledetect/mobile_detect.php');
	$detect = new Mobile_Detect;

	// Any mobile device (phones or tablets).
	if ( $detect->isTablet() ) {
		return true;
	}
	return false;
}


function is_tablet2(){
	$useragent = $_SERVER['HTTP_USER_AGENT'];
	if(preg_match('/iPad|Android/',$useragent)) {
		return true;
	}
	return false;
}


function get_recent_home_posts(){
	$num = 8;

	if ( false === ( $featured_posts = get_transient( 'featured_posts_results' ) ) ) {
		echo '<!-- new query -->';
		$args = array(
			'meta_key'        => '_featured_post',
			'meta_value'      => 'featured',
			'posts_per_page'  => $num,
			'has_password'    => false,
			'ignore_sticky_posts' => true
		);
		$featured_posts = new WP_Query( $args );
		set_transient( 'featured_posts_results', $featured_posts, 1 * MINUTE_IN_SECONDS );
	} else {
		echo '<!-- from cache -->';
	}

	if ( $featured_posts->found_posts > 0 ) {
		$featured_count = $featured_posts->found_posts;
	} else {
		$featured_count = 0;
	}
	$most_recent = $num - $featured_posts->found_posts;
	wp_reset_postdata();

	if ( false === ( $most_recent_posts = get_transient( 'most_recent_posts_results' ) ) ) {
		echo '<!-- new query -->';
		$args = array(
			'meta_key'        => '_featured_post',
			'meta_value'      => 'not_featured',
			'posts_per_page'  => $most_recent,
			'has_password'    => false,
			'ignore_sticky_posts' => true
		);
		$most_recent_posts = new WP_Query( $args );
		set_transient( 'most_recent_posts_results', $most_recent_posts, 1 * MINUTE_IN_SECONDS );
	} else {
		echo '<!-- from cache -->';
	}

	$newsposts = new WP_Query();

	if($featured_count == 0) {
		$newsposts = $most_recent_posts;
	} elseif($featured_count < $num ) {
		$newsposts->posts = array_merge($featured_posts->posts, $most_recent_posts->posts);
		$newsposts->post_count = count($newsposts->posts);
	} else {
		$newsposts = $featured_posts;
	}
	wp_reset_postdata();

	return $newsposts;
}


function get_recent_posts_home( $position, $newsposts ) {
	global $post;
	$homenews_query_results = $newsposts;

	$count = 1;

	if ( $position == 1 ) {

		if ( $homenews_query_results->have_posts() ) : ?>

			<div class="latest_news module">
				<ul id="mostrecent_carousel">

					<?php while ( $homenews_query_results->have_posts() ) : $homenews_query_results->the_post();

						$post_ID			= get_the_ID();
						$the_permalink		= get_permalink();
						$month_published	= get_the_time('M');
						$day_published		= get_the_time('d');
						// ADDING Partner Connect for the Rotating Banner Title
						$partnerconnect		= get_partner_connect( $post_ID );

						$post_classes = array( 'featured_post' );

						if( has_post_thumbnail() ) {
							$post_thumb_url	= get_the_post_thumbnail_url( $post_ID, 'home-big-marquee' );
							$post_thumb		= '<img class="lazyOwl" data-src="'. $post_thumb_url .'" alt="'. get_the_title() .'">';
							$post_classes[]	= 'post-thumbnail';
						} else {
							$post_thumb		= '<img class="lazyOwl" data-src="'. get_bloginfo( 'stylesheet_directory' ) .'/images/running/default-post-thumbnail-marquee.jpg" alt="'. get_the_title() .'">';
						} ?>

						<li>
							<div <?php post_class( $post_classes ); ?>>
								<div class="featured_img">
									<a href="<?php echo $the_permalink; ?>" class="overlay"></a>
									<?php echo $post_thumb; ?>
								</div>
								<div class="row featured_vitals">
									<div class="featured_title_author">
										<h3><a href="<?php echo $the_permalink; ?>"><?php echo $partnerconnect . get_short_title(); ?></a></h3>
										<p class="post_author">by <?php echo get_guest_author(); ?></p>
									</div>
									<a class="arrow" href="<?php echo $the_permalink; ?>"><span class="icon-right-open"></span></a>
								</div>
							</div>
						</li>

						<?php
						// we only need the first 5 posts for this position so break out
						if ( $homenews_query_results->current_post == 4 ) {
							break;
						}

					endwhile; ?>

				</ul>
			</div>

			<?php

		endif;


	} else {

		if( $homenews_query_results->have_posts() ) :

			echo '<div class="category_row">';

				while( $homenews_query_results->have_posts() ) : $homenews_query_results->the_post();

					$post_ID		= get_the_ID();
					$the_permalink	= get_permalink();
					$post_classes	= array( 'col_'. $count, 'three_col' );

					if( has_post_thumbnail() ) {
						$post_thumb_url	= get_the_post_thumbnail_url( $post_ID, 'home-big-marquee' );
						$post_thumb		= '<img class="lazy" data-original="'. $post_thumb_url .'" alt="'. get_the_title() .'">';
						$post_classes[]	= 'post-thumbnail';
					} else {
						$post_thumb		= '<img class="lazy" data-original="'. get_bloginfo( 'stylesheet_directory' ) .'/images/running/default-post-thumbnail-marquee.jpg" alt="'. get_the_title() .'">';
					}

					if ( $count > 1  ) {
						$post_classes[]	= 'visible_lg';
					} ?>

					<div <?php post_class( $post_classes ); ?>>

						<?php if ( $count == 1 ) {

							// add the sponsored DFP ad here
							echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('content-sponsored') : ''; ?>

							<div class="ad_fallback">

								<div class="featured_img">
									<a href="<?php echo $the_permalink; ?>" class="overlay"></a>
									<?php echo $post_thumb; ?>
								</div>
								<div class="more_link white_bg module">
									<a href="<?php echo $the_permalink; ?>"><?php echo get_short_title(); ?></a><a href="<?php echo $the_permalink; ?>"><span class="icon-right-open"></span></a>
								</div>

							</div>

						<?php } else { ?>

							<div class="featured_img">
								<a href="<?php the_permalink(); ?>" class="overlay"></a>
								<?php echo $post_thumb; ?>
							</div>
							<div class="more_link white_bg module">
								<a href="<?php echo $the_permalink; ?>"><?php echo get_short_title(); ?></a><a href="<?php echo $the_permalink; ?>"><span class="icon-right-open"></span></a>
							</div>

						<?php } ?>
					</div>

					<?php $count++;

				endwhile;

				echo '<div class="more_link_arrow light_bg module"><a href="'. site_url() .'/all">Latest on Competitor.com </a><a href="'. site_url() .'/all"><span class="icon-right-open"></span></a></div>

			</div><!-- close off .category_row -->';

		endif;
	}

	wp_reset_postdata();
}

function get_videos_home( $category_id ){
	// $category_link = get_category_link( $category_id );
	$category_link = 'http://video.competitor.com/';

	if ( false === ( $video_query_results = get_transient( 'video_query_results' ) ) ) {
		echo '<!-- new query -->';

		$video_query_results = new WP_Query( array(
			'cat'				=> $category_id,
			'posts_per_page'	=> 3,
			'has_password'		=> false
		) );
		set_transient( 'video_query_results', $video_query_results, 15 * MINUTE_IN_SECONDS  );
	} else {
		echo '<!-- from cache -->';
	}

	if ( $video_query_results->have_posts() ) : while( $video_query_results->have_posts() ) : $video_query_results->the_post();

		$the_permalink	= get_permalink();
		$post_classes	= array();


		if( has_post_thumbnail() ) {
			$post_thumb_url	= get_the_post_thumbnail_url( get_the_ID(), 'home-big-marquee' );
			$post_thumb		= '<img class="lazy" data-original="'. $post_thumb_url .'" alt="'. get_the_title() .'">';
			$post_classes[]	= 'post-thumbnail';
		} else {
			$post_thumb		= '<img class="lazy" data-original="'. get_bloginfo( 'stylesheet_directory' ) .'/images/running/default-post-thumbnail-marquee.jpg" alt="'. get_the_title() .'">';
		}

		if ( $video_query_results->current_post == 0 ) {
			$post_classes[]	= 'big_vid';
		} elseif ( $video_query_results->current_post == 1 ) {
			$post_classes	= array_merge( $post_classes, array( 'video_col', 'visible_md', 'col_odd' ) );
		} elseif ( $video_query_results->current_post == 2 ) {
			$post_classes	= array_merge( $post_classes, array( 'video_col', 'visible_md', 'col_even' ) );
		} ?>

		<div <?php post_class( $post_classes ); ?>>
			<div class="featured_img">
				<a class="overlay" href="<?php echo $the_permalink; ?>"></a>
				<?php echo $post_thumb; ?>
			</div>
			<div class="more_link white_bg module">
				<a href="<?php echo $the_permalink; ?>"><?php echo get_short_title(); ?></a><a href="<?php echo $the_permalink; ?>"><span class="icon-right-open"></span></a>
			</div>
		</div>

		<?php

	endwhile; endif;

	echo '<div class="more_link_arrow light_bg module">
		<a href="'.$category_link.'" title="'.get_cat_name($category_id).'">More Videos </a><a href="'.$category_link.'" title="'.get_cat_name($category_id).'"><span class="icon-right-open"></span></a>
	</div>';

	wp_reset_postdata();
}

function get_videos_side( $category_id ) {
	// $category_link = get_category_link( $category_id );
	$category_link = 'http://video.competitor.com/';

	if ( false === ( $video_query_results = get_transient( 'video_query_results_side' ) ) ) {
		echo '<!-- new query -->';
		$video_query_results = new WP_Query( array(
			'cat'				=> $category_id,
			'posts_per_page'	=> 5
		) );
		set_transient( 'video_query_results_side', $video_query_results, 15 * MINUTE_IN_SECONDS  );
	} else {
		echo '<!-- from cache -->';
	}


	if ( $video_query_results->have_posts() ) {
		echo '<div id="video_carousel">';

			while($video_query_results->have_posts()) {
				$video_query_results->the_post();

				$post_thumb		= '';
				$post_classes	= get_post_class();

				if ( has_post_thumbnail() ) {
					$post_thumb_url	= get_the_post_thumbnail_url( get_the_ID(), 'medium' );
					$post_thumb		= '<img class="lazyOwl" data-src="'. $post_thumb_url .'" alt="'. get_the_title() .'">';
					$post_classes	= get_post_class( 'post-thumbnail' );
				} else {
					$post_thumb		= '<img class="lazyOwl" data-src="'. get_bloginfo( 'stylesheet_directory' ) .'/images/running/default-post-thumbnail-marquee.jpg" alt="'. get_the_title() .'">';
				}

				$post_classes_string = implode(" ", $post_classes);

				echo '<div>
					<figure class="featured_img '. $post_classes_string .'">
						<a class="overlay" href="'. get_permalink() .'"></a>
						'. $post_thumb .'
					</figure>
					<p class="more_link white_bg">
						<a href="'. get_permalink() .'">'. get_short_title() .' </a><a href="'. get_permalink() .'"><span class="icon-right-open"></span></a>
					</p>
				</div>';
			}
		echo '</div>';
	}
	wp_reset_postdata();
}


function get_photos_side( $photo_cat_id ) {

	if ( is_array( $photo_cat_id ) ) {
		$args = array(
			'category__and'		=> $photo_cat_id,
			'posts_per_page'	=> 4
		);
	} else {
		$args = array(
			'cat'				=> $photo_cat_id,
			'posts_per_page'	=> 4
		);
	}

	if ( false === ( $photo_query_results = get_transient( 'photo_query_results' ) ) ) {
		echo '<!-- new query -->';
		$photo_query_results = new WP_Query( $args );
		set_transient( 'photo_query_results', $photo_query_results, 15 * MINUTE_IN_SECONDS  );
	} else {
		echo '<!-- from cache -->';
	}

	if ( $photo_query_results->have_posts() ) {

		echo '<div id="photo_carousel">';
			while($photo_query_results->have_posts()) {
				$photo_query_results->the_post();

				if ( has_post_thumbnail() ) {
					$post_thumb_url	= get_the_post_thumbnail_url( get_the_ID(), 'medium' );
					$post_thumb		= '<img class="lazyOwl" data-src="'. $post_thumb_url .'" alt="'. get_the_title() .'">';
					$post_classes	= get_post_class( 'post-thumbnail' );
				} else {
					$post_thumb		= '<img class="lazyOwl" data-src="'. get_bloginfo( 'stylesheet_directory' ) .'/images/running/default-post-thumbnail-marquee.jpg" alt="'. get_the_title() .'">';
				}

				echo '<div>
					<a href="'. get_permalink() .'">'. $post_thumb .'</a>
					<p class="more_link white_bg">
						<a href="'. get_permalink() .'">'. get_short_title() .' </a><a href="'. get_permalink() .'"><span class="icon-right-open"></span></a>
					</p>
				</div>';
			}
		echo '</div>';

	}
	wp_reset_postdata();
}

function get_photos_home( $photo_cat_id ) {
	$photo_cat_obj	= get_category_by_slug('photos');
	$photo_cat_link	= get_category_link( $photo_cat_obj );

	if ( false === ( $photo_query_results = get_transient( 'photo_query_results' ) ) ) {
		$args = array(
			'cat'				=> explode( ',', $photo_cat_id ),
			'posts_per_page'	=> 4,
			'has_password'		=> false
		);
		$photo_query_results = new WP_Query( $args );

		set_transient( 'photo_query_results', $photo_query_results, 15 * MINUTE_IN_SECONDS  );
		echo '<!-- new query -->';
	} else {
		echo '<!-- from cache -->';
	}

	$count = 1;

	if ( $photo_query_results->have_posts() ) :
		while( $photo_query_results->have_posts() ) : $photo_query_results->the_post();

			$the_permalink		= get_permalink();
			$the_short_title	= get_short_title();

			if( has_post_thumbnail() ) {
				$post_thumb_url	= get_the_post_thumbnail_url( get_the_ID(), 'home-big-marquee' );
				$post_thumb		= '<img class="lazy" data-original="'. $post_thumb_url .'" alt="'. get_the_title() .'">';
			} else {
				$post_thumb		= '<img class="lazy" data-original="'. get_bloginfo( 'stylesheet_directory' ) .'/images/running/default-post-thumbnail-marquee.jpg" alt="'. get_the_title() .'">';
			}

			if ( ( $count % 2 ) == 1) {
				$even_odd = 'col_odd';
			} else {
				$even_odd = 'col_even';
			}

			if ( $count == 1 || $count == 3) {
				echo '<div class="row">
					<div class="photo_col '. $even_odd .' big_photo">
						<a href="'. $the_permalink .'">'. $post_thumb .'</a>
						<div class="more_link white_bg module">
							<a href="'. $the_permalink .'">'. $the_short_title .'</a><a href="'. $the_permalink .'"><span class="icon-right-open"></span></a>
						</div>
					</div>';
			} else {
				echo '<div class="photo_col '. $even_odd .'">
						<a href="'. $the_permalink .'">'. $post_thumb .'</a>
						<div class="more_link white_bg module">
							<a href="'. $the_permalink .'">'. $the_short_title .'</a><a href="'. $the_permalink .'"><span class="icon-right-open"></span></a>
						</div>
					</div>
				</div>';
			}
			$count++;

		endwhile;

		if ( $count == 1 || $count == 3 ) {
			echo '</div>';
		}

	endif;
	echo '<div class="more_link_arrow dark_bg module">
			<a href="'.$photo_cat_link.'" title="'.get_cat_name($photo_cat_obj->term_id).'">More Photos </a><a href="'.$photo_cat_link.'" title="'.get_cat_name($photo_cat_obj->term_id).'"><span class="icon-right-open"></span></a>
		</div>';

	wp_reset_postdata();
}

/* show posts categorized as a photo gallery AND in a particular category */
function get_photos_category( $category_id, $max_number = 6 ) {
	$photo_cat_obj	= get_category_by_slug('photos');
	$photo_cat_link	= get_category_link( $photo_cat_obj );

	if ( false === ( $photo_query_results = get_transient( 'photo_query_results_' . $category_id ) ) ) {

		$technical_theme_options	= get_option( 'cgi_media_technical_options' );

		$photo_cat_id = !empty( $technical_theme_options['gallery_category'] ) ? $technical_theme_options['gallery_category'] : '';

		$categories_ids	= explode( ',', $photo_cat_id );

		array_push( $categories_ids, $category_id );

		$args = array(
			'category__and'		=> array( $categories_ids ),
			'posts_per_page'	=> $max_number
		);

		echo '<!-- new query -->';
		$photo_query_results = new WP_Query( $args );
		set_transient( 'photo_query_results_' . $category_id, $photo_query_results, 15 * MINUTE_IN_SECONDS  );
	} else {
		echo '<!-- from cache -->';
	}

	$count = 1;

	if ( $photo_query_results->have_posts() ) {
		echo '<div class="row">';

			while($photo_query_results->have_posts()) {
				$photo_query_results->the_post();

				$post_classes = get_post_class();
				if ( has_post_thumbnail() ) {
					$post_thumb		= get_the_post_thumbnail(get_the_ID(), 'home-big-marquee');
					$post_classes	= get_post_class('post-thumbnail');
				} else {
					$post_thumb = '<img src="'.get_bloginfo('stylesheet_directory').'/images/running/default-post-thumbnail-marquee.jpg">';
				}

				$post_classes_string = implode(" ", $post_classes);

				if ( $count == 4 ) {
					$visible = ' visible_lg';
				} else {
					$visible = '';
				}

				if ( $count == 1 || $count == 4 ) {
					echo '<div class="row">
							<div class="col_1 three_col '.$post_classes_string.'">
								<div class="featured_img">
									<a href="'.get_permalink().'" class="overlay"></a>
									'.$post_thumb.'
								</div>
								<div class="more_link white_bg module">
									<a href="'.get_permalink().'">'.get_short_title() .' </a><a href="'.get_permalink().'"><span class="icon-right-open"></span></a>
								</div>
							</div>';
				} elseif ( $count == 2 || $count == 5 ) {
					echo '<div class="col_2 cat_odd three_col '.$post_classes_string.'">
							<div class="featured_img">
								<a href="'.get_permalink().'" class="overlay"></a>
								'.$post_thumb.'
							</div>
							<div class="more_link white_bg module">
								<a href="'.get_permalink().'">'.get_short_title() .' </a><a href="'.get_permalink().'"><span class="icon-right-open"></span></a>
							</div>
						</div>';
				} elseif ( $count == 3 || $count == 6 ) {
					echo '<div class="col_3 cat_even three_col '.$post_classes_string.'">
							<div class="featured_img">
								<a href="'.get_permalink().'" class="overlay"></a>
								'.$post_thumb.'
							</div>
							<div class="more_link white_bg module">
								<a href="'.get_permalink().'">'.get_short_title() .' </a><a href="'.get_permalink().'"><span class="icon-right-open"></span></a>
							</div>
						</div>
					</div>';
				}
				$count++;
			}
			if ( $count != 4 && $count != 7 ) {
				echo '</div>';
			}
		echo '</div>';
	}
	echo '<div class="more_link_arrow dark_bg module">
			<a href="'.$photo_cat_link.'" title="'.get_cat_name($photo_cat_obj->term_id).'">More Photos </a><a href="'.$photo_cat_link.'" title="'.get_cat_name($photo_cat_obj->term_id).'"><span class="icon-right-open"></span></a>
		</div>';

	wp_reset_postdata();
}

function get_newsfeed_side(){
	if ( false === ( $newsfeed_query_results = get_transient( 'newsfeed_query_results' ) ) ) {

		$args = array(
			'posts_per_page'		=> 15,
			'ignore_sticky_posts'	=> 1
		);
		$newsfeed_query_results = new WP_Query( $args );

		set_transient( 'newsfeed_query_results', $newsfeed_query_results, 15 * MINUTE_IN_SECONDS  );
		echo '<!-- new query -->';
	} else {
		echo '<!-- from cache -->';
	}
	$count = 1;

	if ( $newsfeed_query_results->have_posts() ) :
		echo '<ul class="list_carousel">';
			while ( $newsfeed_query_results->have_posts() ) : $newsfeed_query_results->the_post();

				if ( $count == 1 || $count == 6 || $count == 11 ) {
					echo '<li class="list_group"><ul>';
				}

				echo '<li><a href="'.get_permalink().'">'.get_short_title() .'</a></li>';

				if ( $count == 5 || $count == 10 || $count == 15 ) {
					echo '</ul></li>';
				}
				$count++;

			endwhile;
		echo '</ul>';
	endif;

	wp_reset_postdata();
}


function get_recommended_side(){
	$rec_stories = get_option( 'widget_hot-stories-2' );
	extract($rec_stories, EXTR_PREFIX_ALL, 'rec');

	if ( false === ( $recommended_query_results = get_transient( 'hot-stories-ga' ) ) ) {
		echo 'no data';
	} else {
		$count = 1;

		// $bloginfo = ' - '.get_bloginfo( 'name');
		$bloginfo2 = ' | '. get_bloginfo( 'name' );
		if ( count($recommended_query_results) > 1 ){

			echo '<ul class="list_carousel">';
				foreach ( $recommended_query_results as $recommended_query_result ) {

					if ( $count == 1 || $count == 6 || $count == 11 ) {
						echo '<li class="list_group"><ul>';
					}

					// $title = str_replace($bloginfo, '', $recommended_query_result[0]);
					$title = str_replace($bloginfo2, '', $recommended_query_result['title']);

					if ( strpos($title, " - Page") > 0 ) {
						$title = substr($title, 0, strpos($title, " - Page") );
					}

					echo '<li><a href="http://' . $recommended_query_result['url'] .'" title="' . $title . '">' . $title . '</a></li>' . "\n";

					if ( $count == 5 || $count == 10 || $count == 15 ) {
						echo '</ul></li>';
					}

					$count++;
				}

			echo '</ul>
			</li>
			</ul>';

		}
	}
}

function get_upcoming_vids( $postid, $offset = 0 ) {
	global $post;
	$count = 0;
	$thumb = '';

	$technical_theme_options	= get_option( 'cgi_media_technical_options' );
	$video_cat_id				= $technical_theme_options['video_category'];
	$categories					= get_the_category($postid);
	$category_ids				= array();
	$category_string			= '';

	foreach ( $categories as $category ) {
		$category_ids[] = $category->cat_ID;
		$category_string .= '_'.$category->cat_ID;
	}

	if ( false === ( $upcoming_vids_query = get_transient( 'upcoming_vids_query' ) ) ) {

		$args = array(
			'posts_per_page'	=> 8,
			'category__and'		=> $category_ids
		);
		$upcoming_vids_query = new WP_Query( $args );

		set_transient( 'upcoming_vids_query', $upcoming_vids_query, 15 * MINUTE_IN_SECONDS  );
		echo '<!-- new query -->';
	} else {
		echo '<!-- from cache -->';
	}

	if ( $offset == 0 ) {  // upcoming side
		echo '<h3><img class="slashes slash_blue" src="'.get_bloginfo('stylesheet_directory').'/images/running/running-slashes-blue.svg"> Up Next</h3>';
		if ( $upcoming_vids_query->have_posts() ) {
			while ( $upcoming_vids_query->have_posts() ) : $upcoming_vids_query->the_post();
				if ( $post->ID != $postid ) {
					$thumb	= '';
					$title	= get_short_title();
					$url	= get_permalink();

					if ( has_post_thumbnail () ) {
						$image_array = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');
						$thumb = '<a class="overlay" href="' . $url . '" title="' . $title . '"></a>' . '<img src="'.$image_array[0].'" />';
					}
					echo '<div class="video_col category-video">
							<div class="featured_img">
								'.$thumb.'
							</div>
							<div class="more_link white_bg module">
								<a href="'.$url.'">'.$title.' </a><a href="'.$url.'"><span class="icon-right-open"></span></a>
							</div>
						</div>';
					$count++;
					if ( $count > 1 ) {
						break;
					}
				}
			endwhile;
		}
	} else {  // upcoming beneath post
		echo '<h3><img class="slashes slash_blue" src="'.get_bloginfo('stylesheet_directory').'/images/running/running-slashes-blue.svg"> Related Videos</h3>';
		if ( $upcoming_vids_query->have_posts() ){
			while ( $upcoming_vids_query->have_posts() ) : $upcoming_vids_query->the_post();
				if ( $count > $offset ) {  // start count at 2
					if ( $post->ID != $postid ) {
						$thumb	= '';
						$title	= get_short_title();
						$url	= get_permalink();

						if ( has_post_thumbnail () ) {
							$image_array = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');
							$thumb = '<a class="overlay" href="' . $url . '" title="' . $title . '"></a>' . '<img src="'.$image_array[0].'" />';
						}

						// not really odd since count will start at 2
						$even_odd	= ($count % 2) == 0 ? 'col_odd' : 'col_even';

						if ( ($count % 2) == 0 ) {
							echo '<div class="row">
									<div class="video_col category-video '.$even_odd.'">
										<div class="featured_img">
											'.$thumb.'
										</div>
										<div class="more_link white_bg module">
											<a href="'.$url.'">'.$title.' </a><a href="'.$url.'"><span class="icon-right-open"></span></a>
										</div>
									</div>';
						} else {
							echo '<div class="video_col category-video '.$even_odd.'">
										<div class="featured_img">
											'.$thumb.'
										</div>
										<div class="more_link white_bg module">
											<a href="'.$url.'">'.$title.' </a><a href="'.$url.'"><span class="icon-right-open"></span></a>
										</div>
									</div>
								</div>';
						}
					} else {
						$count--;
					}
				}
				$count++;
			endwhile;
			if ( $count == 3 || $count == 5 || $count == 7 ) {
				echo '</div>';
			}
		}
	}

	wp_reset_postdata();
}

function get_recent_category_posts( $category_id, $max_number, $big_first_post = 0 ) {

	if ( false === ( $recent_category_posts = get_transient( 'recent_category_posts_'.$category_id) ) ) {
		// don't look at sticky posts -- just pure recent dates
		$args = array(
			'posts_per_page'			=> $max_number,
			'cat'						=> $category_id,
			'ignore_sticky_posts'		=> true,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false,
			'update_post_term_cache'	=> false
		);
		$recent_category_posts = new WP_Query($args);

		set_transient( 'recent_category_posts'.$category_id, $recent_category_posts, 15 * MINUTE_IN_SECONDS  );
		echo '<!-- new query -->';
	} else {
		echo '<!-- from cache -->';
	}

	if ( $recent_category_posts->have_posts() ) {

		$count = 1;

		while ( $recent_category_posts->have_posts() ) {
			$recent_category_posts->the_post();
			$post_classes = get_post_class();

			if ( $big_first_post == 1 ) {

				if ( $count < 5 ) {
					if ( has_post_thumbnail() ) {
						$post_thumb = get_the_post_thumbnail(get_the_ID(), 'home-big-marquee');
						$post_classes = get_post_class('post-thumbnail');
					} else {
						$post_thumb = '<img src="'.get_bloginfo('stylesheet_directory').'/images/running/default-post-thumbnail-marquee.jpg">';
					}
					$post_classes_string	= implode(" ", $post_classes);

					$month_published		= get_the_time('M');
					$day_published			= get_the_time('d');

					if($count == 1) {
						echo '
							<div class="featured_post category_big_post '.$post_classes_string.'">
								<div class="featured_img">
									<a class="overlay" href="'.get_permalink().'"></a>
									'.$post_thumb.'
								</div>
								<div class="row featured_vitals">
									<div class="featured_post_date">
										<span class="featured_post_month">'.$month_published.'</span>
										<span class="featured_post_day">'.$day_published.'</span>
									</div>
									<div class="featured_title_author">
										<h3><a href="'.get_permalink().'">'.get_short_title().'</a></h3>
										<p class="post_author">by '.get_guest_author().'</p>
										<p class="excerpt">'.get_the_excerpt().'</p>
									</div>
									<a class="arrow" href="'.get_permalink().'"><span class="icon-right-open"></span></a>
								</div>
							</div>
						';
					} elseif($count == 2) {
						echo '
							<div class="row">
								<div class="col_1 three_col '.$post_classes_string.'">
									<div class="featured_img">
										<a href="'.get_permalink().'" class="overlay"></a>
										'.$post_thumb.'
									</div>
									<div class="more_link white_bg module">
										<a href="'.get_permalink().'">'.get_short_title() .' </a><a href="'.get_permalink().'"><span class="icon-right-open"></span></a>
									</div>
								</div>
						';
					} elseif($count == 3) {
						echo '
							<div class="col_2 three_col '.$post_classes_string.'">
								<div class="featured_img">
									<a href="'.get_permalink().'" class="overlay"></a>
									'.$post_thumb.'
								</div>
								<div class="more_link white_bg module">
									<a href="'.get_permalink().'">'.get_short_title() .' </a><a href="'.get_permalink().'"><span class="icon-right-open"></span></a>
								</div>
							</div>
						';
					} elseif($count == 4) {
						echo '
								<div class="col_3 three_col '.$post_classes_string.'">
									<div class="featured_img">
										<a href="'.get_permalink().'" class="overlay"></a>
										'.$post_thumb.'
									</div>
									<div class="more_link white_bg module">
										<a href="'.get_permalink().'">'.get_short_title() .' </a><a href="'.get_permalink().'"><span class="icon-right-open"></span></a>
									</div>
								</div>
							</div>
						';
					}
				} else {
					if ( has_post_thumbnail() ) {
						$post_thumb		= get_the_post_thumbnail(get_the_ID(), 'thumbnail');
						$post_classes	= get_post_class('post-thumbnail');
					} else {
						$post_thumb = '<img src="'.get_bloginfo('stylesheet_directory').'/images/running/default-post-thumbnail-marquee.jpg">';
					}
					$post_classes_string = implode(" ", $post_classes);

					if($count == 5) {
						echo '<ul class="top_posts">';
					}
					echo '
						<li class="'.$post_classes_string.'">
							<div class="featured_img">
								<a href="'.get_permalink().'" class="overlay"></a>
								'.$post_thumb.'
							</div>
							<h4><a href="'.get_permalink().'">'.get_short_title().'</a></h4>
							<p class="post_author">by '.get_guest_author().'</p>
							<p class="excerpt">'.get_the_excerpt().'</p>
						</li>
					';
					if($count == $max_number) {
						echo '</ul>';
					}
				}
			} else {
				if($count < 4) {
					if ( has_post_thumbnail() ) {
						$post_thumb		= get_the_post_thumbnail(get_the_ID(), 'home-big-marquee');
						$post_classes	= get_post_class('post-thumbnail');
					} else {
						$post_thumb = '<img src="'.get_bloginfo('stylesheet_directory').'/images/running/default-post-thumbnail-marquee.jpg">';
					}
					$post_classes_string = implode(" ", $post_classes);

					if($count == 1) {
						echo '
							<div class="row">
								<div class="col_1 three_col '.$post_classes_string.'">
									<div class="featured_img">
										<a href="'.get_permalink().'" class="overlay"></a>
										'.$post_thumb.'
									</div>
									<div class="more_link white_bg module">
										<a href="'.get_permalink().'">'.get_short_title() .' </a><a href="'.get_permalink().'"><span class="icon-right-open"></span></a>
									</div>
								</div>
						';
					} elseif($count == 2) {
						echo '
							<div class="col_2 three_col '.$post_classes_string.'">
								<div class="featured_img">
									<a href="'.get_permalink().'" class="overlay"></a>
									'.$post_thumb.'
								</div>
								<div class="more_link white_bg module">
									<a href="'.get_permalink().'">'.get_short_title() .' </a><a href="'.get_permalink().'"><span class="icon-right-open"></span></a>
								</div>
							</div>
						';
					} elseif($count == 3) {
						echo '
								<div class="col_3 three_col '.$post_classes_string.'">
									<div class="featured_img">
										<a href="'.get_permalink().'" class="overlay"></a>
										'.$post_thumb.'
									</div>
									<div class="more_link white_bg module">
										<a href="'.get_permalink().'">'.get_short_title() .' </a><a href="'.get_permalink().'"><span class="icon-right-open"></span></a>
									</div>
								</div>
							</div>
						';
					}
				} else {
					if ( has_post_thumbnail() ) {
						$post_thumb		= get_the_post_thumbnail(get_the_ID(), 'thumbnail');
						$post_classes	= get_post_class('post-thumbnail');
					} else {
						$post_thumb = '<img src="'.get_bloginfo('stylesheet_directory').'/images/running/default-post-thumbnail-sm.jpg">';
					}
					$post_classes_string = implode(" ", $post_classes);

					if($count == 4) {
						echo '<ul class="top_posts">';
					}
					echo '
						<li class="'.$post_classes_string.'">
							<div class="featured_img">
								<a href="'.get_permalink().'" class="overlay"></a>
								'.$post_thumb.'
							</div>
							<h4><a href="'.get_permalink().'">'.get_short_title().'</a></h4>
							<p class="post_author">by '.get_guest_author().'</p>
							<p class="excerpt">'.get_the_excerpt().'</p>
						</li>
					';
					if($count == $max_number) {
						echo '</ul>';
					}
				}
			}
			$count++;
		}
	} else {
		// no featured posts found
	}
	$category_url = get_category_link($category_id);

	echo '
		<div class="more_link_arrow dark_bg module">
			<a href="'.$category_url.'/page/2" title="More '.get_cat_name($category_id).'">More '.get_cat_name($category_id).'</a><a href="'.$category_url.'/page/2" title="More '.get_cat_name($category_id).'"><span class="icon-right-open"></span></a>
		</div>
	';

	wp_reset_postdata();
}

function get_special_recent_category_posts( $category_id, $max_number ) {

	if ( false === $recent_category_posts = get_transient( 'recent_category_posts_'. $category_id ) ) {

		// don't look at sticky posts -- just pure recent dates
		$args = array(
			'posts_per_page'		=> $max_number,
			'cat'					=> $category_id,
			'ignore_sticky_posts'	=> 1
		);
		$recent_category_posts = new WP_Query($args);
		set_transient( 'recent_category_posts_'. $category_id, $recent_category_posts, 15 * MINUTE_IN_SECONDS  );

		echo '<!-- new query -->';
	} else {
		echo '<!-- from cache -->';
	}
	if ( $recent_category_posts->have_posts() ) :
		echo '<ul>';
		while ( $recent_category_posts->have_posts() ) : $recent_category_posts->the_post();
			if ( has_post_thumbnail() ) {
				$post_thumb = get_the_post_thumbnail( get_the_ID(), 'thumbnail' );
			}
			echo '<li>
					<a href="'. get_permalink() .'">'. $post_thumb .'</a>
					<span><a href="'. get_permalink(). '">'. get_short_title() .'</a></span>
				</li>';
		endwhile;
		echo '</ul>';
	endif;

	$category_url = get_category_link( $category_id );
	echo '<div class="more_link_arrow dark_bg module">
			<a href="'. $category_url .'/page/2" title="More '. get_cat_name( $category_id) .'">More '. get_cat_name( $category_id ) .'</a><a href="'. $category_url .'/page/2" title="More '. get_cat_name( $category_id ) .'"><span class="icon-right-open"></span></a>
		</div>';

	wp_reset_postdata();
}

function get_youtube_playlist( $playlist_id ) { ?>
	<script src="<?php echo get_bloginfo('stylesheet_directory'); ?>/js/ytembed.js"></script>
	<div class="video_playlist">
		<div id="ytThumbs"></div>
		<script type="text/javascript">
			ytEmbed.init({
				'block': 'ytThumbs',
				'q': '<?php echo $playlist_id; ?>',
				'type': 'playlist',
				'results': 20,
				'order': 'new_first',
				'player': 'embed',
				'display_first': true,
				'layout': 'thumbnails'
			});
		</script>
	</div>
	<?php
}

function get_brightcove_playlist( $player_id, $player_key, $passed_playlist_id, $video_player, $use_pre_roll ) { ?>

	<div class="video_playlist">

		<video id="myVideo"
			data-playlist-id="<?php echo $passed_playlist_id; ?>"
			data-account="3655502813001"
			data-player="HJmm2O0yW"
			data-embed="default"
			data-application-id
			class="video-js"
			controls>
		</video>
		<script src="//players.brightcove.net/3655502813001/HJmm2O0yW_default/index.min.js"></script>

		<ol class="vjs-playlist"></ol>

		<?php if ( $use_pre_roll ):
			$adKw		= class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::singleton()->generate_dfp_keywords() : ''; ?>
			<link href="//players.brightcove.net/videojs-ima3/2/videojs.ima3.min.css" rel="stylesheet">
			<script src="//players.brightcove.net/videojs-ima3/2/videojs.ima3.min.js"></script>
			<script type="text/javascript">
				var myPlayer = videojs('myVideo');
				myPlayer.ready(function(){
					myPlayer=this;

					myPlayer.ima3({
						serverUrl: "https://pubads.g.doubleclick.net/gampad/ads?sz=720x480&iu=<?php echo $adKw; ?>&ciu_szs&impl=s&gdfp_req=1&env=vp&output=xml_vast2&unviewed_position_start=1&url=[referrer_url]&correlator=[timestamp]",
						timeout: 5000,
						prerolltimeout: 1000,
						requestMode: 'onload',
						debug: true,
						adTechOrder: ['html5', 'flash'],
						vpaidMode: 'ENABLED'
					});
					myPlayer.one('loadedmetadata', function(){
						// get a reference to the player
						myPlayer = this;

						myPlayer.on("ima3-ad-error",function(event){
							console.log("ima3-ad-error: " + event);
						});

						myPlayer.on("adserror", function(event){
							console.log("adsError Fired: " + event);
						});
					});
				});
			</script>
		<?php endif; ?>

		</div>
	<?php

}

function get_latest_stories_2( $post_ID ) {

	$args = array(
		'posts_per_page'			=> 5,
		'ignore_sticky_posts'		=> 1,
		'no_found_rows'				=> true,
		'update_post_meta_cache'	=> false,
		'update_post_term_cache'	=> false,
	);

	if ( false === $latest_posts = get_transient( 'latest_posts' ) ) {

		$latest_posts	= new WP_Query( $args );
		set_transient( 'latest_posts', $latest_posts, 10 * MINUTE_IN_SECONDS );

	} elseif ( $latest_posts->have_posts() === false ) {

		$latest_posts	= new WP_Query( $args );
		set_transient( 'latest_posts', $latest_posts, 10 * MINUTE_IN_SECONDS );

	}

	ob_start();

	if ( $latest_posts->have_posts() ) : ?>
		<div class="aside_recent_posts">
			<h3>Latest Headlines</h3>
			<ul>
				<?php while ( $latest_posts->have_posts() ) : $latest_posts->the_post();
					if ( $latest_posts->current_post < 4 && get_the_ID() !== $post_ID ) { ?>
						<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
					<?php }
				endwhile; ?>
			</ul>
		</div>

	<?php endif;

	wp_reset_postdata();

	return ob_get_clean();

}


/* the next two walker_nav_menu classes deal with tabs */
class Sub_Category_Walker_2 extends Walker_Nav_Menu {
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$output .= '<li><a href="#tabs-'.$item->ID.'">'.$item->title;
	}
	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= '</a></li>';
	}
}


class Sub_Category_Walker_2a extends Walker_Nav_Menu {

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;

		$output .= '<div id="tabs-'.$item->ID.'">';

		if( strpos( $item->url, 'tag' ) !== false ) {
			$url_array	= explode( '/', $item->url );

			$args		= array(
				'posts_per_page'	=> 1,
				'tag'				=> $url_array[count( $url_array )-1]
			);

			$destination_url	= site_url() . '/tag/';
			$destination_obj	= get_term_by( 'slug', $url_array[count($url_array)-1], 'post_tag' );
			$destination_url	= $destination_url . $destination_obj->slug;

		} elseif( strpos( $item->url, 'category' ) !== false ) {

			$url_array	= explode( '/', $item->url );
			$thisCat	= get_category( get_query_var( 'cat' ),false );
			$idObj		= get_category_by_slug( $url_array[count( $url_array )-1] );
			$id			= $idObj->term_id;
			$args		= array(
				'posts_per_page'	=> 1,
				'category__and'		=>  array( $id, $thisCat->term_id )
			);
			$destination_url	= site_url() . '/category/';
			$destination_obj	= get_term_by( 'slug', $url_array[count( $url_array )-1], 'category' );
			$destination_url	= $destination_url . $destination_obj->slug;

		} else {

			$url_array	= explode( '/', $item->url );
			$page_obj	= get_page_by_path( $url_array[count( $url_array )-1], OBJECT );
			$args		= array(
				'page_id' => isset( $page_obj->ID ) ? $page_obj->ID : ''
			);

		}


		if( strpos( $item->url, 'tag' ) !== false || strpos( $item->url, 'category' ) !== false ) {
			$original_query	= $wp_query;
			$wp_query		= NULL;
			$wp_query		= new WP_Query( $args );
			$count			= 0;

			while( $wp_query->have_posts() ) : $wp_query->the_post();

				if( $count < 1 ) {
					$permalink			= get_permalink();
					$title				= get_short_title();
					$month_published	= get_the_time( 'M' );
					$day_published		= get_the_time( 'd' );

					$post_classes		= get_post_class();

					if( has_post_thumbnail() ) {
						$post_thumb		= get_the_post_thumbnail( get_the_ID(), 'single-post-big' );
						$post_classes	= get_post_class( 'post-thumbnail' );
					}

					$post_classes_string = implode( " ", $post_classes );

					$output .= '
						<div class="featured_post '. $post_classes_string .'">
							<div class="featured_img">
								<a href="'. $permalink .'" class="overlay"></a>'.
								$post_thumb .'
							</div>
							<div class="row featured_vitals">
								<div class="featured_post_date">
									<span class="featured_post_month">'. $month_published .'</span>
									<span class="featured_post_day">'. $day_published .'</span>
								</div>
								<div class="featured_title_author">
									<h3><a href="'. $permalink .'">'. $title .'</a></h3>
									<p class="post_author">by '. get_guest_author() .'</p>
								</div>
								<a class="arrow" href="'. $permalink .'"><span class="icon-right-open"></span></a>
							</div>
						</div>
						<div class="more_link_arrow dark_bg module"><a href="'. $destination_url .'">More '. ucwords( $destination_obj->name ) .'</a><a href="'. $destination_url .'"><span class="icon-right-open"></span></a></div>
					';
					$count++;
				}

			endwhile;

			$wp_query	= NULL;
			$wp_query	= $original_query;
			wp_reset_postdata();

		} else {

			$original_query	= $wp_query;
			$wp_query		= NULL;
			$wp_query		= new WP_Query($args);

			while( $wp_query->have_posts() ) : $wp_query->the_post();
				$permalink		= get_permalink();
				$title			= get_short_title();
				$post_classes	= get_post_class();

				if( has_post_thumbnail() ) {
					$post_thumb		= get_the_post_thumbnail( get_the_ID(), 'full' );
					$post_classes	= get_post_class( 'post-thumbnail' );
				} else {
					$post_thumb = '<img src="'. get_bloginfo( 'stylesheet_directory' ) .'/images/running/default-post-thumbnail-lg.jpg" alt="'. $title .'">';
				}

				$post_classes_string = implode( " ", $post_classes );

				$output .= '
					<div class="featured_post '. $post_classes_string .'">
						<div class="featured_img">
							<a href="'. $permalink .'" class="overlay"></a>'.
							$post_thumb .'
						</div>
						<div class="row featured_vitals">
							<div class="featured_post_date"></div>
							<div class="featured_title_author">
								<h3 style="padding:.625em 0;"><a href="'. $permalink .'">'. $title .'</a></h3>
								<p class="excerpt">'. get_the_excerpt() .'</p>
							</div>
							<a class="arrow" href="'. $permalink .'"><span class="icon-right-open"></span></a>
						</div>
					</div>
				';
			endwhile;

			$wp_query	= NULL;
			$wp_query	= $original_query;
			wp_reset_postdata();

		}
	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= '</div>';
	}

}


/* for vids */
class Sub_Category_Walker_2_vid extends Walker_Nav_Menu {
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$output .= '<li><a href="#tabs-'.$item->ID.'">'.$item->title;
	}
	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= '</a></li>';
	}
}
class Sub_Category_Walker_2a_vid extends Walker_Nav_Menu {

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;

		$output .= '<div id="tabs-'.$item->ID.'">';

			$url_array = explode('/', $item->url);
			$args = array(
				'posts_per_page' => 9,
				'tag' => $url_array[count($url_array)-1],
				'cat' => 15
			);

			if(strpos($item->url, 'tag') !== false) {
				$wp_query = new WP_Query($args);
				// $query_count = count($wp_query);
				$count = 1;
				while ($wp_query->have_posts()) : $wp_query->the_post();
					$permalink = get_permalink();
					$title = get_short_title();
					$month_published = get_the_time('M');
					$day_published = get_the_time('d');

					$post_classes = get_post_class();
					if ( has_post_thumbnail() ) {
						$post_thumb = get_the_post_thumbnail(get_the_ID(), 'square-big-thumbs');
						$post_classes = get_post_class('post-thumbnail');
					} else {
						$post_thumb = '<img src="'.get_bloginfo('stylesheet_directory').'/images/running/default-post-thumbnail-md.jpg">';
					}
					$post_classes_string = implode(" ", $post_classes);

					if($count == 1 || $count == 4 || $count == 7) {
						$output .= '
							<div class="row">
								<div class="col_1 three_col '.$post_classes_string.'">
									<div class="featured_img">
										<a href="'.get_permalink().'" class="overlay"></a>
										'.$post_thumb.'
									</div>
									<div class="more_link white_bg module">
										<a href="'.get_permalink().'">'.get_short_title() .' </a><a href="'.get_permalink().'"><span class="icon-right-open"></span></a>
									</div>
								</div>
						';
					} elseif($count == 2 || $count == 5 || $count == 8) {
						$output .= '
							<div class="col_2 three_col '.$post_classes_string.'">
								<div class="featured_img">
									<a href="'.get_permalink().'" class="overlay"></a>
									'.$post_thumb.'
								</div>
								<div class="more_link white_bg module">
									<a href="'.get_permalink().'">'.get_short_title() .' </a><a href="'.get_permalink().'"><span class="icon-right-open"></span></a>
								</div>
							</div>
						';
					} elseif($count == 3 || $count == 6 || $count == 9) {
						$output .= '
								<div class="col_3 three_col '.$post_classes_string.'">
									<div class="featured_img">
										<a href="'.get_permalink().'" class="overlay"></a>
										'.$post_thumb.'
									</div>
									<div class="more_link white_bg module">
										<a href="'.get_permalink().'">'.get_short_title() .' </a><a href="'.get_permalink().'"><span class="icon-right-open"></span></a>
									</div>
								</div>
							</div>
						';
					}
					$count++;
				endwhile;
				if($count != 4 && $count != 7 && $count != 10) {
					$output .= '</div>';
				}

			wp_reset_postdata();
		}
	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= '</div>';

	}
}

// accordion
class Sub_Category_Walker_2b extends Walker_Nav_Menu {

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;
		$output .= '<h5>'. $item->title.'</h5>';

		if ( strpos($item->url, 'tag') !== false ) {
			$url_array	= explode('/', $item->url);
			$args		= array(
				'posts_per_page'	=> 1,
				'tag'				=> $url_array[count($url_array)-1]
			);
			$destination_url	= site_url() . '/tag/';
			$destination_obj	= get_term_by('slug', $url_array[count($url_array)-1], 'post_tag');
			$destination_url	= $destination_url . $destination_obj->slug;
		} elseif ( strpos( $item->url, 'category' ) !== false ) {
			$url_array	= explode('/', $item->url);
			$thisCat	= get_category(get_query_var('cat'),false);
			$idObj		= get_category_by_slug($url_array[count($url_array)-1]);
			$id			= $idObj->term_id;
			$args		= array(
				'posts_per_page'	=> 1,
				'category__and'		=>  array($id, $thisCat->term_id)
			);
			$destination_url	= site_url() . '/category/';
			$destination_obj	= get_term_by('slug', $url_array[count($url_array)-1], 'category');
			$destination_url	= $destination_url . $destination_obj->slug;
		} else {
			$url_array	= explode('/', $item->url);
			$page_obj	= get_page_by_path($url_array[count($url_array)-1], OBJECT);
			$args		= array(
				'page_id' => isset( $page_obj->ID ) ? $page_obj->ID : ''
			);
		}
		if(strpos($item->url, 'tag') !== false || strpos($item->url, 'category') !== false){
			$original_query	= $wp_query;
			$wp_query		= NULL;
			$wp_query		= new WP_Query($args);
			$count			= 0;

			while ($wp_query->have_posts()) : $wp_query->the_post();
				if($count < 1){
					$permalink			= get_permalink();
					$title				= get_short_title();
					$month_published	= get_the_time('M');
					$day_published		= get_the_time('d');

					$post_classes		= get_post_class();
					if ( has_post_thumbnail() ) {
						$post_thumb		= get_the_post_thumbnail(get_the_ID(), 'home-big-marquee');
						$post_classes	= get_post_class('post-thumbnail');
					}
					$post_classes_string = implode(" ", $post_classes);

					$output .= '
						<div class="featured_post '.$post_classes_string.'">
							<div class="featured_img">
								<a href="'.$permalink.'" class="overlay"></a>
								'.$post_thumb.'
							</div>
							<div class="row featured_vitals">
								<div class="featured_post_date">
									<span class="featured_post_month">'.$month_published.'</span>
									<span class="featured_post_day">'.$day_published.'</span>
								</div>
								<div class="featured_title_author">
									<h3><a href="'.$permalink.'">'.$title.'</a></h3>
									<p class="post_author">by '.get_guest_author().'</p>
								</div>
								<a class="arrow" href="'.$permalink.'"><span class="icon-right-open"></span></a>
							</div>
							<div class="more_link_arrow dark_bg module"><a href="'.$destination_url.'">More '.ucwords($destination_obj->name).'</a><a href="'.$destination_url.'"><span class="icon-right-open"></span></a></div>
						</div>
					';
					$count++;
				}
			endwhile;

			$wp_query	= NULL;
			$wp_query	= $original_query;
			wp_reset_postdata();
		} else {
			$original_query	= $wp_query;
			$wp_query		= NULL;
			$wp_query		= new WP_Query($args);

			while ($wp_query->have_posts()) : $wp_query->the_post();
				$permalink		= get_permalink();
				$title			= get_short_title();

				$post_classes	= get_post_class();
				if ( has_post_thumbnail() ) {
					$post_thumb		= get_the_post_thumbnail(get_the_ID(), 'home-big-marquee');
					$post_classes	= get_post_class('post-thumbnail');
				}
				$post_classes_string = implode(" ", $post_classes);

				$output .= '
					<div class="featured_post '.$post_classes_string.'">
						<div class="featured_img">
							<a href="'.$permalink.'" class="overlay"></a>
							'.$post_thumb.'
						</div>
						<div class="row featured_vitals">
							<div class="featured_post_date"></div>
							<div class="featured_title_author">
								<h3 style="padding:.625em 0;"><a href="'.$permalink.'">'.$title.'</a></h3>
								<p class="excerpt">'.get_the_excerpt().'</p>
							</div>
							<a class="arrow" href="'.$permalink.'"><span class="icon-right-open"></span></a>
						</div>
					</div>
				';
			endwhile;

			$wp_query	= NULL;
			$wp_query	= $original_query;
			wp_reset_postdata();

		}
	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {

	}
}

function my_gallery_2( $gallery_html, $attr, $size, $gallery_id ) {
	global $post;

	 extract( shortcode_atts( array(
			'order'      => 'ASC',
			'orderby'    => 'menu_order ID',
			'id'         => $post->ID,
			'itemtag'    => 'dl',
			'icontag'    => 'dt',
			'captiontag' => 'dd',
			'columns'    => 3,
			'size'       => $size,
	 ), $attr) );

	$id = intval($id);

	if ( $size == 'legacy-list' ) {
		$thumb_ID		= get_post_thumbnail_id( $post->ID );
		$attachments	= get_children( array(
			'post_parent'		=> $id,
			'post_status'		=> 'inherit',
			'post_type'			=> 'attachment',
			'post_mime_type'	=> 'image',
			'order'				=> 'ASC',
			'exclude'			=> $thumb_ID,
			'orderby'			=> 'menu_order'
		) );
	} else {
		$attachments	= get_children( array(
			'post_parent'		=> $id,
			'post_status'		=> 'inherit',
			'post_type'			=> 'attachment',
			'post_mime_type'	=> 'image',
			'order'				=> 'ASC',
			'orderby'			=> 'menu_order'
		) );
	}

	// check for endpoint on others with custom code
	$endpoint		= AMP_QUERY_VAR;

	$url_parts		= explode( '?', $_SERVER["REQUEST_URI"] );
	$query_parts	= explode( '/', $url_parts[0] );

	if ( in_array( $endpoint, $query_parts ) ) {

		$output = '<amp-carousel width="400" height="300" layout="responsive" type="slides">';

			foreach ( $attachments as $id => $attachment ) {

				$image_meta	= wp_get_attachment_image_src( $id, 'medium' );
				$width		= $image_meta[1];
				$height		= $image_meta[2];

				$excerpt = trim( $attachment->post_excerpt );
				$excerpt = ( $excerpt ? wptexturize($excerpt) : '' );

				$output .= sprintf(
					'<amp-img src="%s" height="%s" width="%s">%s</amp-img>',
					wp_get_attachment_url( $attachment->ID ),
					$height,
					$width,
					$excerpt
				);

			}

		$output .= '</amp-carousel>';

		return $output;

	} else {

		$gallery_type = 'In Article';
		if ( $size == 'large' ) {
			$gallery_type	= 'Image Gallery';
		} elseif ( $size == 'legacy-list' ) {
			$gallery_type	= 'Legacy List';
		}
		$output = "
				<script type=\"text/javascript\">
					jQuery(document).ready(function($) {
						$('[data-fancybox]').fancybox({
							preload: true,
							css: {
								'max-height': '20px',
								'margin': '20, 60, 20, 60',
							},
							fullScreen : false,

						});
					});
				</script>
			";
		$refresh = '';
		if ( $size == 'large' ) {
			$refresh = 'googletag.pubads().refresh();';
		}
		if ( $size != 'legacy-list') {
			$output .= "<script src='".get_bloginfo('template_directory') . "/js/carousel-min.js?'></script>";
		}

		if ( $size != 'legacy-list' ) {
			$output .= "<script>jQuery(document).ready(function($) {
				$('.marquee-".$post->ID."').carousel({
					slider: '.slider',
					slide: '.slide',
					slideHed: '.slidehed',
					nextSlide : '.p-next',
					nextSlide2  : '.p-next-img',
					prevSlide : '.p-prev',
					addPagination: false,
					addNav : false,
					speed: 600
				});"."\n";
		}

		$output .= "$('.p-next').click(function() {
				$('#post-slider-secondary').trigger('nextprev', { dir: 'next' });"."\n".
				"_gaq.push(['_trackEvent', '".$gallery_type." Carousel', 'Next', '".$gallery_type." Carousel Next Button']);"."\n";
		if ( ($size == 'large' || $size == 'legacy-list' ) && !is_mobile() ) {
			$output .= "tracker_change(1, ad_slide_position); googletag.pubads().refresh();"."\n";
		}

		$output .= "});
			$('.p-next-img').click(function() {
				$('#post-slider-secondary').trigger('nextprev', { dir: 'next' });"."\n".
				"_gaq.push(['_trackEvent', '".$gallery_type." Carousel', 'Next', '".$gallery_type." Carousel Next Image']);"."\n";
		if ( ( $size == 'large' || $size == 'legacy-list' ) && !is_mobile() ) {
			$output .= "tracker_change(1, ad_slide_position); googletag.pubads().refresh();"."\n";
		}
		$output .= "});
			$('.p-prev').click(function() {
				$('#post-slider-secondary').trigger('nextprev', { dir: 'prev' });"."\n".
				"_gaq.push(['_trackEvent', '".$gallery_type." Carousel', 'Previous', '".$gallery_type." Carousel Previous Button']);"."\n";
		if ( ( $size == 'large' || $size == 'legacy-list' ) && !is_mobile() ) {
			$output .= "tracker_change(-1, ad_slide_position); googletag.pubads().refresh();"."\n";
		}

		$attachment_count = count($attachments);
		$output .= "});";

		if ( $size != 'legacy-list' ) {
			$output .= "$('.carousel-nav').css('display', 'block');";
		}
		$output .="
			$('.carousel-nav').css('display', 'block');
			$('.slide').css('margin-right', '0');
			$('.slideHed').css('display', 'block');
			$('.cover.secondary').css('max-height', '100%');
			$('#post-slider-secondary .slide').css('height', '100%');";

		if ( $size == 'large' ) {
			$output .= "$('.view-full-image').css('display', 'block');
				$('.slide p').css('display', 'block');
				$('#post-slider-secondary .slide').removeAttr('width');";
		}

		$output .= "});";

		$output .='</script><div class="'.$gallery_id.'">';

		$backToStartLink = "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
		if ( $size == 'large' ) {
			$output .= '
				<div class="gallery_hdr">
					<div class="carousel-nav">
						<div class="gallery_hdr_left"><span class="icon-picture"></span>Photo Gallery </div>
					</div>
					<div class="gallery_hdr_right">
						<a href="#" class="p-prev carousel-prev"></a>
						<label class="counter-text">1 of {count}</label><a href="#" class="p-next carousel-next"></a>
						<div class="backtostart"><a href="'.$backToStartLink.'" title="Back To Start" onClick="_gaq.push([\'_trackEvent\', \'Image Gallery Carousel\', \'BackToStart\', \'Back To Start\']);">Back to Start</a></div>
					</div>
				</div>
			';
		}

		$output.= '<div class="marquee marquee-'.$post->ID.'">';
		if ( $size == 'large' ) {
			$output .='<ul class="slider" id="post-slider-secondary">';
			$content_string = apply_filters('the_content', get_the_content());
			// hide all of the images that were in the story
			$content_string = preg_replace("/<img[^>]+\>/i", " ", $content_string);
		} else {
			$output .='<ul class="slider in-article" id="post-slider-secondary">';
		}
		$legacy_list_count		= count( $attachments );
		$ad_slide				= get_post_meta ( $post->ID, '_mid_slider_ad_number', true );
		$ad_slide_position		= $legacy_list_count - $ad_slide;
		$i						= 0;
		$ad_slide_link			= get_post_meta ( $post->ID, '_mid_slider_ad_link', true);
		$end_of_slider_ad_url	= get_post_meta ( $post->ID, '_end_of_slider_ad_url', true);
		$last_slide_is_ad		= get_post_meta ( $post->ID, '_last_slide_is_ad', true);
		foreach ( $attachments as $attachment ) {
			if ( $size == 'large' ) {
				$desc = $attachment->post_content;

				$image_array =  wp_get_attachment_image_src( $attachment->ID, 'gallery-carousel-2');
				//changeed from htmlentities to htmlspecialchars
				$link_text = '<div class="viewLargerImage"><a class="fancybox" data-fancybox="group" rel="gallery" href="' . $image_array[0] . '" title="'.htmlspecialchars($desc, ENT_QUOTES).'"
					onClick="_gaq.push([\'_trackEvent\', \'Image Gallery Carousel\', \'Enlarge\', \'View Larger Image\']);">View Larger Image</a></div>';
				$is_mobile = is_mobile();
				if ( $is_mobile ) {
					$image = wp_get_attachment_image_src( $attachment->ID, 'medium' );
				} else {
					$image = wp_get_attachment_image_src( $attachment->ID, 'large' );
				}
				$image = '<img src="'.$image[0].'" class="cover secondary"/>';
				$title = $attachment->post_title;

				$attachment_url = get_attachment_link($attachment->ID);
				$output .= '
					<li class="slide">
						'.$link_text.'<a href="#" class="p-next-img">'.$image.'</a>
						<h2 class="slideHed">'.$title.'</h2>
						<div class="slide_txt">
							<p>'.$desc.'</p>
						</div>
						'.$content_string.'
					</li>';
			} elseif ( $size == 'legacy-list' ) {
				$link = get_attachment_link($attachment->ID);
				$image = wp_get_attachment_image_src($attachment->ID, $size);
				$link = str_replace('<a ', '<a class="cover secondary" ', $link);
				$title = $attachment->post_title;
				$desc = $attachment->post_excerpt;

				if ( $i == $ad_slide_position ) {
					$output .= '<li class="slide">
					<h2 class="slideHed">'.$title.'</h2>
					<a onClick="_gaq.push([\'_trackEvent\', \'Legacy List Carousel\', \'Ad Click\', \'Legacy List Carousel Ad 1\']);" href="'.$ad_slide_link.'" target="_blank" class="cover" title="'.$title.'">
						<img src="'.$image[0].'"/></a>
						<p>'.$desc.'</p>
						</li>';
				} elseif ( $last_slide_is_ad && ( $i == ( $legacy_list_count - 1 ) ) ) {
					$output .= '<li class="slide">
					<h2 class="slideHed">'.$title.'</h2>
					<a onClick="_gaq.push([\'_trackEvent\', \'Legacy List Carousel\', \'Ad Click\', \'Legacy List Carousel Ad 2\']);" href="'.$end_of_slider_ad_url.'" target="_blank" class="cover" title="'.$title.'">
						<img src="'.$image[0].'"/></a>
						<p>'.$desc.'</p>
						</li>';
				} else {
					$output .= '<li class="slide">
					<h2 class="slideHed">'.$title.'</h2>
					<a href="#" class="p-next-img cover" title="'.$title.'">
						<img src="'.$image[0].'"/></a>
						<p>'.$desc.'</p>
						</li>';
				}
				$i++;
			} else {
				$link = get_attachment_link( $attachment->ID );
				global $blog_id;
				if ( $blog_id == 2 ) {
					$image = wp_get_attachment_image_src( $attachment->ID, 'large' );
				} else {
					$image = wp_get_attachment_image_src( $attachment->ID, $size );
				}
				$link	= str_replace( '<a ', '<a class="cover secondary" ', $link );
				$title	= $attachment->post_title;
				$desc	= strip_tags( $attachment->post_excerpt );
				$output .= '<li class="slide"><a href="#" class="cover secondary p-next-img" title="'.$title.'">
					<img src="'.$image[0].'"/></a>
					<p class="inline_description">'.$desc.'</p>
				</li>';
			}
		}
		// right here we could put a replay slide with any other additional options
		$editorial_theme_options = get_option( 'cgi_media_editorial_options' );
		$gallery_last_slide = $editorial_theme_options['gallery_last_slide'];
		if ( $gallery_last_slide && $size == 'large' ) {
			$gallery_last_slide_url = $editorial_theme_options['gallery_last_slide_url'];
			$output .= '<li class="slide"><a href="'.$gallery_last_slide_url.'"><img src="'.$gallery_last_slide.'" /></a></li>';
		}

		$output .= '</ul></div>';
		if ( $size == 'large' ) {
			$output .= '<div class="more_galleries"><a href="'.site_url().'/category/photos/">More Galleries</a></div>';
		}
		if ( ( $size != 'large') && ($size != 'legacy-list' ) ) {
			$output .= '<div class="carousel-nav">
			<a href="#" class="p-prev carousel-prev"></a><label class="counter-text">1 of {count} b</label><a href="#" class="p-next carousel-next"></a>
			</div>';
		}
		$output.= '</div>';

		return $output;

	}

}

function get_user_ip() {
	$url = site_url();
	$is_testing = strpos($url, 'cgitesting');
	if(!empty($_SERVER['HTTP_X_CLIENTSIDE'])){
		$client_side = $_SERVER['HTTP_X_CLIENTSIDE'];
		$client_side_array = explode('->', $client_side);
		$ip = explode(':', trim($client_side_array[0]));
		return $ip[0];
	} else if (!empty($_SERVER['HTTP_CLIENT_IP'])) {  //check ip from share internet
		return $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) { //to check ip is pass from proxy
		$ip = explode(',',$_SERVER['HTTP_X_FORWARDED_FOR']);
		return $ip[0];
	} else {
		return $_SERVER['REMOTE_ADDR'];
	}
}

function get_competitor_cover(){
	$output = "";
	$current_query = $wp_query;
	$wp_query = null;
	$wp_query = wp_cache_get( 'competitor_magazine' );
	$query = array(
		'category_name'=> "magazine",
		'posts_per_page'=>1,
	);
	if ($wp_query == TRUE) {
		if ($wp_query->have_posts() === false) {
			$wp_query = new WP_Query($query);
			wp_cache_replace( 'competitor_magazine', $wp_query, '', 1800 );
		}
	}
	else {
		$wp_query = new WP_Query($query);
		wp_cache_set( 'competitor_magazine', $wp_query, '', 1800 );
	}
	while($wp_query->have_posts()) : $wp_query->the_post();
		$id = get_post_thumbnail_id($wp_query->post->ID);
		$cover = wp_get_attachment_image_src($id, "medium");
		$title = get_the_title();
		$url = get_permalink();
		$output .= '<a title="Explore the ' . $title . ' Issue" href="' . $url . '"><img class="subscription_image" src="' . $cover[0] . '"/></a>';

	endwhile;
	$wp_query  = null;
	$wp_query = $current_query;
	wp_reset_postdata();
	return $output;
}

class training_paces_widget extends WP_Widget {

	function training_paces_widget(){
		$widget_ops = array('classname' => 'Training Paces Widget', 'description' => __( "Widget to display training paces based on previous race pace.") );
		parent::__construct('training_paces_widget', __('Training Paces Widget'), $widget_ops);
	}

	function widget( $args, $instance ) {
		extract($args);

		$title = $instance['title'];

		?>
		<div class="vdot-form-container">
			<script src="<?php echo get_bloginfo( 'stylesheet_directory' ) . '/js/vdot_calc.js';?>"></script>
			<script>
			function calculateVDot(){
				var value = document.getElementById("mile-value").value;
				var array = value.split(":");
				var vdot_value = document.getElementById("vdot-value");
				var training_paces_list = document.getElementById("training-paces-list");
				if(array.length != 3){
					alert("Time not formatted correctly.");
					training_paces_list.innerHTML = '';
					vdot_value.innerHTML = '';
					return;
				}
				var distance = document.getElementById("value-select").value;
				var vdot = 30;
				for(var i = 85; i > 29; i--){
					if(compare(vDot_obj[i][distance], value)){
						if(i != 85){
							vdot = i + 1;
						}
						else{
							vdot = i;
						}
						break;
					}
				}
				var training_paces_string = '<li>Easy training pace: '+ vDot_obj[vdot]['easy'] + '</li>' +
					'<li>Marathon pace: '+ vDot_obj[vdot]['marathon'] + '</li>' +
					'<li>Threshold pace: '+ vDot_obj[vdot]['threshold'] + '</li>' +
					'<li>Interval pace: '+ vDot_obj[vdot]['interval'] + '</li>' +
					'<li>Repetition pace: '+ vDot_obj[vdot]['repetition'] + '</li>';

				vdot_value.innerHTML = vdot;
				training_paces_list.innerHTML =training_paces_string;
				document.getElementById("results-div").style.display = "block";
			}
			function compare(value1, value2){
				var array1 = value1.split(":");
				var array2 = value2.split(":");
				var seconds = array1[array1.length - 1];
				var mins = array1[array1.length - 2];
				var hours = 0;
				if(array1.length == 3){
					hours = array1[0];
				}
				var value1Seconds = hours*3600 + mins*60 + seconds;
				var seconds = array2[array2.length - 1];
				var mins = array2[array2.length - 2];
				var hours = 0;
				if(array2.length == 3){
					hours = array2[0];
				}
				var value2Seconds = hours*3600 + mins*60 + seconds;

				if(parseInt(value1Seconds) > parseInt(value2Seconds)){
					return true;
				}
				return false;
			}
			</script>
			<h1><?php echo $title;?></h1>
			<div class="vdot-inner-container">
				<p class="description">VDOT is a way of determining one's training intensity
					based upon a recent race performance and refers to the rate at which oxygen is consumed - the volume of oxygen consumed per minute. </p>
				<p class="description"><a href="http://www.coacheseducation.com/endur/jack-daniels-nov-00.htm">Learn More</a></p>
				<p>Input time as hh:mm:ss</p>

				<input size="8" type="text" id="mile-value"/>
				<select id="value-select">
					<option value="Mile">Mile</option>
					<option value="5k">5k</option>
					<option value="10k">10k</option>
					<option value="13.1M">1/2 Marathon</option>
					<option value="Marathon">Marathon</option>
				</select>
				<button onclick="calculateVDot()">Calculate</button>
				<div id="results-div">
					<h2 class="vdot-header">VDOT Value: </h2>
					<div id="vdot-value"></div>

					<h2 class="training-paces-header">Training Paces: </h2>
					<ul id="training-paces-list"></ul>
				</div>
			</div>
		</div>
		<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = $new_instance['title'];

		return $instance;
	}

	function form( $instance ) {

		$title = isset( $instance['title'] ) ? $instance['title'] : '';

		?>
		<p style="text-align:left;">
			<label><b>Title:</b></label>
			<input type="text" value="<?php echo $title;?>" id="<?php echo $this->get_field_id("title");?>"
				name="<?php echo $this->get_field_name("title");?>" size="29"/><br />
		</p>
		<?php
	}
}

//Register the marquee, and add action for everything.
function training_paces_init() {
	register_widget( 'training_paces_widget' );
}

add_action( 'widgets_init', 'training_paces_init' );


function page_title() {

	// Build the title of the current page, default is site name and description
	$title = get_bloginfo('name') . ' - ' . stripslashes(get_bloginfo('description'));

	if (is_singular() && !is_home() && !is_front_page()) {
		// Set to post title if its a post, page, or attachment
		$title = single_post_title( '', false );
	} elseif (is_category()) {
		// Set to category name, description and site name if its a category
		$title = single_cat_title( '', false ) . ' - ' . stripslashes(strip_tags(category_description())) . ' - ' . get_bloginfo('name');
	} elseif (is_tag()) {
		// Set to tag name, description and site name if its a tag
		$title = single_cat_title( '', false ) . ' - ' . get_bloginfo('name');
	} elseif (is_date()) {
		// Set to Archive by Date and site name if its a date archive
		$title = 'Archive by Date - ' . get_bloginfo('name');
	} elseif (is_404()) {
		// Set to 404 Not Found and site name if its a missing page
		$title = '404 Not Found - ' . get_bloginfo('name');
	}

	return '<title>' . $title . '</title>';

}

function meta_keywords() {

	// Build the title of the current page, default is site name and description
	$keywords = stripslashes(get_bloginfo('description'));

	if (is_singular() && !is_home() && !is_front_page()) {
		global $post;
		$keywords = "";
		// see if there are keywords associated with the post or attachment and if not use the title
		$posttags = get_the_tags($post->ID);
		if(get_post_meta($post->ID, '_headspace_metakey', true) != ''){
			$keywords = get_post_meta($post->ID, '_headspace_metakey', true);
		}
		else if ($posttags) {
			foreach($posttags as $tag) {
					$keywords .= $tag->name . ', ';
				}
		}
			if($keywords != ""){
				$keywords = rtrim($keywords , ', ');
			}
			else{
		// Set to post title if its a post, page, or attachment and no tags
			$keywords = single_post_title( '', false );
			}
	} elseif (is_category()) {
		// Set to category name, description and site name if its a category
		$keywords = single_cat_title( '', false ) . ', ' . stripslashes(strip_tags(category_description()));
	} elseif (is_tag()) {
		// Set to tag name, description and site name if its a tag
		$keywords = single_cat_title( '', false );
	}

	return '<meta name="keywords" content="' . $keywords . '">' . "\n";

}

function meta_description() {

	// Build the title of the current page, default is site name and description
	$description = get_bloginfo('name') . ' - ' . stripslashes(get_bloginfo('description'));

	if (is_singular() && !is_home() && !is_front_page()) {
		global $post;
		$description = $post->post_excerpt;
		if(get_post_meta($post->ID, '_headspace_description', true) != ''){
			$description = get_post_meta($post->ID, '_headspace_description', true);
		}
		else if($description == ""){
			// Set to post title if its a post, page, or attachment and there is no excerpt
			$description = single_post_title( '', false );
		}
	} elseif (is_category()) {
		// Set to category name, description and site name if its a category
		$description = single_cat_title( '', false ) . ' - ' . stripslashes(strip_tags(category_description())) . ' - ' . get_bloginfo('name');
	} elseif (is_tag()) {
		// Set to tag name, description and site name if its a tag
		$description = single_cat_title( '', false ) . ' - ' . get_bloginfo('name');
	} elseif (is_date()) {
		// Set to Archive by Date and site name if its a date archive
		$description = 'Archive by Date - ' . get_bloginfo('name');
	} elseif (is_404()) {
		// Set to 404 Not Found and site name if its a missing page
		$description = '404 Not Found - ' . get_bloginfo('name');
	}
	return '<meta name="description" content="' . $description . '" >' . "\n";

}

function wp_open_graph_protocol() {

	// Open Graph Protocol - ogp.me

	// Common
	$og_site_name = get_bloginfo('name');
	$og_image = get_bloginfo('stylesheet_directory') . '/images/logo.jpg';

	if (is_front_page() || is_home()) {

		// Specific to a static or blog homepage
		$og_type = 'website';
		$og_title = get_bloginfo('name');
		$og_description = get_bloginfo('description');
		$og_url = get_bloginfo('url');
	} elseif (is_singular()) {

		// Specific to post, page or attachment
		global $post;

		$og_title = single_post_title('',false);
		$og_url = get_permalink();

		if (has_post_thumbnail( $post->ID )) {
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-big' );
			//$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'gallery-carousel' );
			$og_image = $image[0];
		}
		if (is_singular() && !is_attachment()) {
			$og_type = 'article';
		} elseif (is_page() && !is_attachment()) {
			$og_type = 'page';
		} else {
			if (wp_attachment_is_image()) {
				$og_type = 'image';
			} else {
				$og_type = 'media';
			}
			$og_url = get_attachment_link();
		}

		if(get_post_meta($post->ID, '_headspace_description', true) != ''){
			$excerpt = get_post_meta($post->ID, '_headspace_description', true);
		}
		else{
			$excerpt = get_the_excerpt();
		}
		if (strlen($excerpt) > 140) {
			$short_excerpt = substr($excerpt, 0, 140);
			$words = explode(" ", $short_excerpt);
			$trim_excerpt = -(strlen($words[count($words) - 1]));
			if ($trim_excerpt < 0) {
				$og_description = substr($short_excerpt, 0, $trim_excerpt);
			} else {
				$og_description = $short_excerpt;
			}
		} else {
			$og_description = $excerpt;
		}

	} elseif (is_archive()) {

		// Specific to author, category, tag or date archives
		$og_url = get_bloginfo('url') . $_SERVER['REQUEST_URI'];

		if (is_author()) {
			$og_type = 'author';
			if (get_query_var('author_name')) {
				$current_author = get_user_by('slug', get_query_var('author_name'));
			} else {
				$current_author = get_userdata(get_query_var('author'));
			}
			$og_title = $current_author->display_name;
			$og_description = strip_tags($current_author->description);
			$avatar = new DOMDocument();
			if($current_author->user_email != "" && get_avatar($current_author->user_email) != ""){
				$avatar->loadHTML(get_avatar($current_author->user_email));
					$og_image = $avatar->getElementsByTagName('img')->item(0)->getAttribute('src');
			}
		} else {
			$og_type = 'archive';
			if (!is_date()) {
				$og_title = single_cat_title('',false);
				$og_description = strip_tags(term_description());
			} else {
				$og_title = '';
				$og_description = '';
			}
		}

	}

	$ogp = '<meta property="og:type" content="'. $og_type .'" />' . "\n";
	$ogp .= '<meta property="og:title" content="'. $og_title .'" />' . "\n";
	$ogp .= '<meta property="og:description" content="'. $og_description .'" />' . "\n";
	$ogp .= '<meta property="og:site_name" content="'. $og_site_name .'" />' . "\n";
	$ogp .= '<meta property="og:url" content="'. $og_url .'" />' . "\n";
	$ogp .= '<meta property="og:image" content="'. $og_image .'" />' . "\n";


	$technical_theme_options = get_option( 'cgi_media_technical_options' );
	$twitter = $technical_theme_options['site_twitter_handle'];
	$ogp .= '<meta name="twitter:card" content="summary">' . "\n";
	$ogp .= '<meta name="twitter:site" content="@'.$twitter.'">' . "\n";
	$ogp .= '<meta name="twitter:url" content="'.$og_url.'">' . "\n";
	$ogp .= '<meta name="twitter:title" content="'.$og_title.'">' . "\n";
	$ogp .= '<meta name="twitter:description" content="'.$og_description.'">' . "\n";
	$ogp .= '<meta name="twitter:image" content="'.$og_image.'">' . "\n";

	return $ogp;
}

function get_short_title() {
	global $post;
	if (get_post_meta($post->ID, "short_title", true) != '') {
		$title = strip_tags(get_post_meta($post->ID, "short_title", true));
	} else {
		$title = the_title('', '', false);
	}

	if (strlen($title) > 60) {
		$short_title	= substr($title, 0, 60);
		$words			= explode(" ", $short_title);
		$trim_title		= -(strlen($words[count($words) - 1]));

		if ($trim_title < 0) {
			return substr($short_title, 0, $trim_title);
		} else {
			return $short_title;
		}
	} else {
		return $title;
	}
}


function get_guest_author() {
	global $post;

	$post_ID = $post->ID;
	$post_parent = get_post( $post_ID );

	if ( $post->post_type == 'attachment' && $post->post_parent != 0 ) {
		$post_ID = $post->post_parent;
	}

	if ( get_post_meta( $post_ID, 'guest_author', true ) && get_post_meta( $post_ID, 'guest_author' ) != '' ) {
		return get_post_meta( $post_ID, 'guest_author', true );
	}

	$post_author	= $post_parent->post_author;
	$author_name	= get_the_author_meta( 'display_name', $post_author );

	if ( $post->post_type == 'attachment' && $post->post_parent != 0 ) {
		$author_url		= get_the_author_meta( 'user_url', $post_author );

		return '<a href="'. $author_url .'">'. $author_name .'</a>';
	}

	$author_url		= get_author_posts_url( $post_author );

	return '<a href="'. $author_url .'">'. $author_name .'</a>';
}

$Appid = '844F0060BC425D0E56BFDB0DB647E6D1285C4FCD';

// -----------------------------------------------------------------------------function to get the most recent stories to display on posts with no image
function get_latest_stories( $post_ID ) {

	$args = array(
		'posts_per_page'			=> 5,
		'ignore_sticky_posts'		=> 1,
		'no_found_rows'				=> true,
		'update_post_meta_cache'	=> false,
		'update_post_term_cache'	=> false,
	);

	if ( false === $latest_posts = get_transient( 'latest_posts' ) ) {

		$latest_posts	= new WP_Query( $args );
		set_transient( 'latest_posts', $latest_posts, 10 * MINUTE_IN_SECONDS );

	} elseif ( $latest_posts->have_posts() === false ) {

		$latest_posts	= new WP_Query( $args );
		set_transient( 'latest_posts', $latest_posts, 10 * MINUTE_IN_SECONDS );

	}

	ob_start();

	if ( $latest_posts->have_posts() ) : ?>
		<aside id="recent-posts">
			<h3>Latest Headlines</h3>
			<ul>
				<?php while ( $latest_posts->have_posts() ) : $latest_posts->the_post();
					if ( $latest_post->current_post < 4 && get_the_ID() !== $post_ID ) { ?>
						<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
					<?php }
				endwhile; ?>
			</ul>
		</aside>

	<?php endif;

	wp_reset_postdata();

	return ob_get_clean();

}

function search_by_title(){
	global $post;
	$result_html = "";
	$technical_theme_options = get_option( 'cgi_media_technical_options' );
	$accountKey = $technical_theme_options['bing_search_appID'];
	$search_text = get_the_title();
	$page = 1;
	$offset = 0;
	$type = 'web';
	$results_per_page_web = 10;

	if ($search_text != '') {

				$ServiceRootURL =  'https://api.datamarket.azure.com/Bing/Search/';

				$WebSearchURL = $ServiceRootURL . 'Web?$format=json&$top='.$results_per_page_web.'&$skip='.$offset.'&Query=';

				$context = stream_context_create(array(
					'http' => array(
							'request_fulluri' => true,
								'header'  => "Authorization: Basic " . base64_encode($accountKey . ":" . $accountKey)
								)
		));
		$url = site_url();
		$url = str_replace("cgitesting.lan", "competitor.com", $url);
		$url = str_replace("competitor.lan", "competitor.com", $url);


		$result_html .= '<div class="posts"><h2>Articles:</h2>';

				$request = $WebSearchURL . urlencode( '\'' . $search_text . ' site:'.$url. '\'');

				$response = file_get_contents($request, 0, $context);

			$jsonobj = json_decode($response);

			$result_html .= '<ul ID="resultList">';

			foreach($jsonobj->d->results as $value){
				$url_array = explode("_", $value->Url);
				$array_count = count($url_array);
				$post_id = $url_array[$array_count - 1];
				$post_id_array = explode("/", $post_id);
				$post_id = $post_id_array[0];
				$thumb_url = '';
			if (is_numeric($post_id)) {
				$thumb_url = get_the_post_thumbnail($post_id, 'thumbnail');
			}
					$result_html .= '<li class="post"><a href="'.$value->Url.'">'.$thumb_url.'</a><a href="'.
						$value->Url.'"><h3>'.$value->Title.'</h3></a><span>'.stripslashes($value->Description).'</span></li>';

			}

			$result_html .= "</ul></div>";

			$result_html .= '<span id="page-nav">';
		if(count($jsonobj->d->results) == $results_per_page_web){
			$next_page = $page + 1;
			$result_html .= '<a href="/search?q=' . $search_text . '&page=' . $next_page . '&type=web">More Article Results</a>';
		}
		$result_html .= '</span>';
	}
	return $result_html;

}

// START Retrieve and display Partner Connect
function get_partner_connect( $post_ID ) {

	// check the checkbox for display
	if ( get_post_meta( $post_ID, 'meta-checkbox', true ) != '' ) {

		$sponsored_text_field	= get_post_meta( $post_ID, 'meta-text', true );

		// Checks if the field is not empty
		if ( !empty( $sponsored_text_field ) ) {
			return $sponsored_text_field .': ';
		}

	}

}
// END Partner Connect Display

// CHECK to see if parent page exists

if ( ! function_exists( 'post_is_in_descendant_category' ) ) {
	function post_is_in_descendant_category( $cats, $_post = null ) {
		foreach ( (array) $cats as $cat ) {
			// get_term_children() accepts integer ID only
			$descendants = get_term_children( (int) $cat, 'category' );
			if ( $descendants && in_category( $descendants, $_post ) )
				return true;
		}
		return false;
	}
}


// Training Plans Display

function get_training_plans( $post_ID = null ) {

	// make sure a post ID was passed to the function
	if ( $post_ID === null ) {
		return;
	}

	// Check to see if training plans checkbox on post has been checked, if so display training plans
	if ( get_post_meta( $post_ID,'_training_plans_inline', true ) !='' ) {

		$editorial_theme_options	= get_option( 'cgi_media_editorial_options' );
		$training_plans_txt			= $editorial_theme_options['training_plans_txt'];
		$training_plans_link		= $editorial_theme_options['training_plans_link'];
		$training_plans_title		= $editorial_theme_options['training_plans_title'];
		$imagelink					= get_bloginfo('stylesheet_directory') .'/images/running/running-slashes-blue.svg';

		ob_start(); ?>
			<br clear="all">
			<div class="training-title">
				<div style="padding-bottom: 7px;">
					<h3><img class="slashes slash_blue" src="<?php echo $imagelink; ?>"><?php echo $training_plans_title; ?></h3>
					<br>
					<?php echo $training_plans_txt; ?>
				</div>
				<br>
				<div class="digi_band training-button">
					<h5><a href="<?php echo $training_plans_link; ?>">View Training Plans</a></h5>
				</div>
			</div>
			<?php

		return ob_get_clean();
	}
}

function format_timestamp_2() {
	global $post;

	// We want to show the user relevant date info.
	// If the post has been modified more than one hour after the publish date we'll show
	// The date published and then the modified date (relative if not more than 7 days)
	// If the post hasn't been modified or was modified within an hour of publishing then
	// We'll show the publish date (relative if not more than 7 days)

	$today = time();
	$date_published = strtotime(get_the_date('M j, Y') . ' ' . get_the_time('g:i A'));
	$date_modified = strtotime(get_the_modified_date('M j, Y') . ' ' . get_the_modified_time('g:i A'));
	if ($date_modified - $date_published > 60 * 60) {
		if ($today - $date_modified > 60 * 60 * 24 * 7) {
			return 'Published ' . get_the_date() . ', Updated ' . get_the_modified_date() . ' at ' . get_the_modified_time() . '';
		}
		return 'Published ' . get_the_date() . ', Updated ' . human_time_diff( get_the_modified_time('U'), current_time('timestamp') ) . ' ago';
	} else {
		if ($today - $date_published > 60 * 60 * 24 * 7) {
			return 'Published ' . get_the_date();
		}
		return 'Published ' . human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago';
	}

}

// START Partner connect section on home page

function print_pc_posts( $category_id, $show_thumbnail, $show_excerpt, $is_two_column, $ignore_sticky_posts = false, $no_duplicates = false ) {
	if( is_array( $category_id ) ){
		$category_string = implode( '-', $category_id );
	} else {
		$category_string = $category_id;
	}

	// $post_thumb			= $image_array[0];
	$posts_output		= "";
	$num_in_query		= 4;
	$num_first_column	= 1;
	$num_second_column	= 0;

	if( $is_two_column || ( !$show_thumbnail && !$show_excerpt ) ) {
		$num_in_query = 4;
	}
	if( $is_two_column ) {
		$num_second_column = 4;
	}

	global $post_id_array;
	$original_query	= get_queried_object();
	$wp_query		= NULL;
	$wp_query		= wp_cache_get( $category_string .'_query' );

	if( is_array( $category_id ) ) {
		if( $ignore_sticky_posts ) {
			$args = array(
				'category__and'			=> $category_id,
				'posts_per_page'		=> $num_in_query,
				'ignore_sticky_posts'	=> 1,
				'has_password'			=> false
			);
		} else {
			$args = array(
				'category__and'		=> $category_id,
				'posts_per_page'	=> $num_in_query,
				'has_password'		=> false
			);
		}
	} else if( $category_id == -1 ) {
		$args = array(
			'posts_per_page'		=> $num_in_query,
			'ignore_sticky_posts'	=> 1,
			'has_password'			=> false
		);
	} else {
		if( $ignore_sticky_posts ) {
			$args = array(
				'category__and'			=> $category_id,
				'posts_per_page'		=> $num_in_query,
				'ignore_sticky_posts'	=> 1,
				'has_password'			=> false
			);
		} else {
			$args = array(
				'cat'				=> $category_id,
				'posts_per_page'	=> $num_in_query,
				'has_password'		=> false
			);
		}
	}
	if( $wp_query == TRUE ) {
		if( $wp_query->have_posts() === false ) {
			$wp_query = new WP_Query( $args );
			wp_cache_replace( $category_string .'_query', $wp_query, '', 600 );
		}
	} else {
		$wp_query = new WP_Query( $args );
		wp_cache_set( $category_string.'_query', $wp_query, '', 600 );
	}

	$count = 0;

	if( $num_second_column > 0 ) {
		//$posts_output .= '<ul class="top-posts">' . "\n";
		$posts_output .= '<div class="pc_col_odd big_photo">';
	} else if( !$show_thumbnail && !$show_excerpt ) {
		$num_first_column	= 4;
		$posts_output		.= '<ul class="titles-only">' . "\n";
	} else {
		$posts_output .= '<ul>' . "\n";
	}

	if( $wp_query->have_posts() ) : while( $wp_query->have_posts() ) : $wp_query->the_post();
		// check to see if we need to check for duplicates, but we only need to do that when the category is -1, meaning we are at the top
		// getting the latest news
		$get_post = true;

		if( ( !is_array( $category_id ) && $category_id == -1 ) || $ignore_sticky_posts || $no_duplicates ) {
			if( !in_array( get_the_ID(), $post_id_array ) ) {
				$get_post = true;
				$post_id_array[] = get_the_ID();
			} else {
				$get_post = false;
			}
		}

		if( $get_post ) {
			global $post;

			$pcthumb		= ( get_the_post_thumbnail_url( $post->ID, 'partner-connect-home-image' ) != '' ) ? '<img class="lazy" data-original="'. get_the_post_thumbnail_url( $post->ID, 'partner-connect-home-image') .'" alt="'. get_the_title( $post->ID ) .'">' : '<img class="lazy" data-original="'. get_bloginfo( 'stylesheet_directory' ) .'/images/running/default-post-thumbnail-marquee.jpg" alt="'. get_the_title( $post->ID ) .'">';
			$permalink		= get_permalink();
			$title			= get_short_title();
			$partnerconnect	= get_partner_connect( $post->ID ); // Display partner connect in latest section on home page, if value exists & checkbox is selected

			if( $count < $num_first_column ) {
				$thumb		= get_the_post_thumbnail( get_the_ID(), 'thumbnail' );
				$excerpt	= get_the_excerpt();

				if( $show_thumbnail ) {

					$post_classes = get_post_class();

					if( $thumb != "" ) {
						$post_classes = get_post_class( 'post-thumbnail' );
					}
					$post_classes_string = implode( " ", $post_classes );

					$posts_output	.= '<div class="pc_homeimg">
						<a href ="'. $permalink .'">'. $pcthumb .'</a>
					</div>'. "\n";

					$posts_output	.= '<div class="more_link white_bg module">
						<a href ="'. $permalink .'">' . $partnerconnect . '' . $title .'<br><span class="sponsored-label">Sponsored Content</span></a><a href="'. get_permalink() .'"><span class="icon-right-open"></span></a>
						</div>
					</div>';

				} else {

					$posts_output .= '<div class="more_link white_bg module">
						<a href ="'. $permalink .'">'. $partnerconnect .''. $title .'<br><span class="sponsored-label">Sponsored Content</span></a><a href="'.get_permalink() .'"><span class="icon-right-open"></span></a>
						</div>
					</div>';

				}
			} elseif( $count == ( $num_first_column ) && $num_second_column > 0 ) {

				$posts_output .= '<div class="pc_col_odd big_photo">
					<div class="pc_homeimg">
						<a href ="'. $permalink.'">'. $pcthumb .'</a></div><div class="more_link white_bg module"><a href="' . $permalink . '">' . $partnerconnect . '' . $title . '<br><span class="sponsored-label">Sponsored Content</span></a><span class="icon-right-open"></span></a>
					</div>
				</div>';

			} elseif( $count > ( $num_first_column ) && $count < ( $num_first_column + $num_second_column ) && $num_second_column > 0) {

				$post_classes = get_post_class();
					$post_classes_string	= implode(" ", $post_classes);
					$posts_output			.= '<div class="pc_col_odd big_photo">
						<div class="pc_homeimg">
							<a href ="'. $permalink.'">'. $pcthumb .'</a></div><div class="more_link white_bg module"><a href="' . $permalink . '">' . $partnerconnect . '' . $title . '<br><span class="sponsored-label">Sponsored Content</span></a><a href="'.get_permalink().'"><span class="icon-right-open"></span></a>
						</div>
					</div>';

			}

			$count++;
		}
	endwhile; else:

		$posts_output .= '<div>Sorry, no posts match that query.</div>' . "\n";

	endif;

	$wp_query	= NULL;
	$wp_query	= $original_query;
	wp_reset_postdata();

	return $posts_output;
}
// END Partner Connect section


function update_custom_post_meta( $post_meta_tag, $new_value, $post_id ) {

	$current_value = get_post_meta($post_id, $post_meta_tag, TRUE);
	if ($current_value) {
		if ($new_value == '') {
			delete_post_meta($post_id, $post_meta_tag);
		} else {
			update_post_meta($post_id, $post_meta_tag, $new_value);
		}
	} elseif ($new_value != '') {
		add_post_meta($post_id, $post_meta_tag, $new_value, TRUE);
	}

}



/* Catch submenus and setup output for large event menu dropdown */
class Sub_Category_Walker extends Walker_Nav_Menu {

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		 $output .= '<div id="menu-accordion">';
	}

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= "</div>\n";
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;

			$output .= '<h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. $item->title.'</h2>';

		if(strpos($item->url, 'tag') !== false){
			$url_array = explode('/', $item->url);
			$args = array(
				'posts_per_page' => 2,
				'tag' => $url_array[count($url_array)-1]
			);
		}
		else if(strpos($item->url, 'category') !== false){
			$url_array = explode('/', $item->url);
			$thisCat = get_category(get_query_var('cat'),false);
			$idObj = get_category_by_slug($url_array[count($url_array)-1]);
			$id = $idObj->term_id;
			$args = array(
				'posts_per_page' => 2,
				'category__and' =>  array($id, $thisCat->term_id)
			);
		}
		else{
			$output .= '<div><ul><li><a href="'.$item->url.'">'.$item->title.'</a></li></ul></div>';
		}
		if(strpos($item->url, 'tag') !== false || strpos($item->url, 'category') !== false){
			$original_query = $wp_query;
			$wp_query = NULL;
			$wp_query = new WP_Query($args);
			$output .= '<div><ul>';
			$count = 0;
			while ($wp_query->have_posts()) : $wp_query->the_post();
				if($count < 2){
					$permalink = get_permalink();
						$title = get_short_title();
							$output .= '<li>' . "\n";
							$output .= '<a href="'.$permalink.'">'.$title.'</a>' . "\n";
							$output .= '</li>' . "\n";
					$count++;
						}
			endwhile;
			$output .= '</ul>';
			$output .= '<a class="accordion-more" href="'.$item->url.'">More </a></div>';
			$wp_query  = NULL;
			$wp_query = $original_query;
			wp_reset_postdata();
			}
	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {

	}
}

class TopNav_Walker extends Walker_Nav_Menu {

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$output = substr($output, 0, -5);
		$output .= '<ul class="sub-menu">';
	}

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= '</ul></li>';
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$bloginfo = get_bloginfo( 'url' );
		$name_array_slashes = explode('/', $bloginfo);
		$name_dots = explode('.', $name_array_slashes[2]);
		$name_tracking = strtolower($name_dots[0]);

		$link_id = str_replace(' ', '-', $item->title);
		$link_id = str_replace('-&-', '-', $link_id);
		$link_id = strtolower($link_id);
		$class = implode(' ', $item->classes);
		$title  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$target = ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$output .= '<li class="'.trim($class).'"><a href="'.$item->url.'"
			onclick="_gaq.push([\'_trackEvent\', \'Top Nav Links\', \''.$name_tracking.'\', \''.$item->url.'\'])"
			'.$title.' '.$target.'>'.$item->title.'</a></li>';
	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {

	}
}
class TopNav_Walker_2 extends Walker_Nav_Menu {
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;
		$bloginfo = get_bloginfo( 'url' );
		$name_array_slashes = explode('/', $bloginfo);
		$name_dots = explode('.', $name_array_slashes[2]);
		$name_tracking = strtolower($name_dots[0]);

		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="'. esc_attr( $class_names ) . '"';

		$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

		if(strpos($item->url, 'tag') !== false){
			$url_array = explode('/', $item->url);
			$subitem_tag = $url_array[(count($url_array))-1];

			$args = array(
					'posts_per_page' => 5,
					'tag' => $subitem_tag
			);
			$destination_url = site_url() . '/tag/';
			$destination_obj = get_term_by('slug', $subitem_tag, 'post_tag');
			$destination_url = $destination_url . $destination_obj->slug;

			$wp_query = new WP_Query($args);
			$subitem_output = '<ul class="sub-menu2">';
			while ($wp_query->have_posts()) : $wp_query->the_post();
			$subitem_output .= '
					<li>
							<a href="'.get_permalink().'">'.get_the_post_thumbnail().
							get_the_title().'</a>
					</li>
			';
			endwhile;
			$subitem_output .= '</ul>';

		}

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .' onclick="_gaq.push([\'_trackEvent\', \'Top Nav Links\', \''.$name_tracking.'\', \''.$item->url.'\'])">';
		$item_output .= $args->link_before . $item->title;
		$item_output .= $args->link_after;
		$item_output .= '</a>';
		$item_output .= $subitem_output;
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}

class Slidebar_Walker extends Walker_Nav_Menu {

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$output = substr($output, 0, -5);
		$output .= '<ul class="sub-menu">';
	}

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= '</ul></li>';
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$bloginfo = get_bloginfo( 'url' );
				$name_array_slashes = explode('/', $bloginfo);
				$name_dots = explode('.', $name_array_slashes[2]);
				$name_tracking = strtolower($name_dots[0]);

		$link_id = str_replace(' ', '-', $item->title);
		$link_id = str_replace('-&-', '-', $link_id);
		$link_id = strtolower($link_id);
		$class = implode(' ', $item->classes);
		$title  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$target = ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$description = ! empty( $item->description )     ? '<img src="'.esc_attr( $item->description     ) .'" class="navicons sb_sprite">' : '';
		$output .= '<li class="'.trim($class).'"><a href="'.$item->url.'"
			onclick="_gaq.push([\'_trackEvent\', \'Slidebar Links\', \''.$name_tracking.'\', \''.$item->url.'\'])"
			'.$title.' '.$target.'>'.$description.$item->title.'</a></li>';
	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {

	}
}

class Subscription_Walker extends Walker_Nav_Menu {

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= '<li style="margin-top:5px;"><ul>';
	}

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= '</ul></li>';
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;
		$style = '';
		$bloginfo = get_bloginfo( 'url' );
				$name_array_slashes = explode('/', $bloginfo);
				$name_dots = explode('.', $name_array_slashes[2]);
				$name_tracking = strtolower($name_dots[0]);
		if($depth == 1){
			$style= 'style="float:none;font-size:1.25em;margin-top:0;"';
		}
		if($item->description != ''){
			if($item->description == 'magazine'){
				// get the current magazine cover
			}
			else if($item->description == 'newsletter'){
				// get the newsletter image
			}
			else{
			/* sferrino
			Added linking on primary image.

			TBD: is it possible to push all javascript into a variable to be echoed out in the footer?

			*/

				$link_id = str_replace(' ', '-', $item->title);
				$link_id = str_replace('-&-', '-', $link_id);
				$link_id = strtolower($link_id);
				$output .= '<li><a href="'.$item->url.'" onclick="_gaq.push([\'_trackEvent\', \'Top Nav Links\', \''.$name_tracking.'\', \''.$item->url.'\']);"
					><img src="'.$item->description.'"/></a><a href="'.$item->url.'"
					onclick="_gaq.push([\'_trackEvent\', \'Top Nav Links\', \''.$name_tracking.'\', \''.$item->url.'\']);"
					>'.$item->title.'</a></li>';
			}
		}
		else{
			if(trim($item->title) != 'subscribe-menu'){
				$link_id = str_replace(' ', '-', $item->title);
				$link_id = str_replace('-&-', '-', $link_id);
				$link_id = strtolower($link_id);
				$output .= '<li '.$style.'><a href="'.$item->url.'"
					onclick="_gaq.push([\'_trackEvent\', \'Top Nav Links\', \''.$name_tracking.'\', \''.$item->url.'\']);">'.$item->title.'</a></li>';
			}
		}

	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {

	}
}

class calories_burned_widget extends WP_Widget {

	function calories_burned_widget(){
		$widget_ops = array('classname' => 'Calories Burned Widget', 'description' => __( "Widget to display calories burned based on an activity.") );
		parent::__construct('calories_burned_widget', __('Calories Burned Widget'), $widget_ops);
	}

	function widget($args, $instance){
		extract($args);

		$title = $instance['title'];
	?>
		<div class="calories-burned-form-container">
		<script>
		function showOption(){
			var selected_activity = document.getElementById("activity_type").value;
			if(selected_activity == "bike"){
				var weight_inputs = document.getElementsByClassName("weight-div");
				for( var i = 0; i < weight_inputs.length; i++){
					weight_inputs[i].style.display = "block";
				}
				var time_inputs = document.getElementsByClassName("time-div");
				for( var i = 0; i < time_inputs.length; i++){
					time_inputs[i].style.display = "block";
				}
				var swim_inputs = document.getElementsByClassName("swim-select");
				for( var i = 0; i < swim_inputs.length; i++){
					swim_inputs[i].style.display = "none";
				}
				var run_inputs = document.getElementsByClassName("speed-select");
				for( var i = 0; i < run_inputs.length; i++){
					run_inputs[i].style.display = "none";
				}
				var bike_inputs = document.getElementsByClassName("bike-select");
				for( var i = 0; i < bike_inputs.length; i++){
					bike_inputs[i].style.display = "block";
				}
				var calories_inputs = document.getElementsByClassName("calculate_calories");
				for( var i = 0; i < calories_inputs.length; i++){
					calories_inputs[i].style.display = "block";
				}
			}
			else if(selected_activity == "run"){
				var weight_inputs = document.getElementsByClassName("weight-div");
				for( var i = 0; i < weight_inputs.length; i++){
					weight_inputs[i].style.display = "block";
				}
				var time_inputs = document.getElementsByClassName("time-div");
				for( var i = 0; i < time_inputs.length; i++){
					time_inputs[i].style.display = "block";
				}
				var swim_inputs = document.getElementsByClassName("swim-select");
				for( var i = 0; i < swim_inputs.length; i++){
					swim_inputs[i].style.display = "none";
				}
				var run_inputs = document.getElementsByClassName("speed-select");
				for( var i = 0; i < run_inputs.length; i++){
					run_inputs[i].style.display = "block";
				}
				var bike_inputs = document.getElementsByClassName("bike-select");
				for( var i = 0; i < bike_inputs.length; i++){
					bike_inputs[i].style.display = "none";
				}
				var calories_inputs = document.getElementsByClassName("calculate_calories");
				for( var i = 0; i < calories_inputs.length; i++){
					calories_inputs[i].style.display = "block";
				}
			}
			else if(selected_activity == "swim"){
				var weight_inputs = document.getElementsByClassName("weight-div");
				for( var i = 0; i < weight_inputs.length; i++){
					weight_inputs[i].style.display = "block";
				}
				var time_inputs = document.getElementsByClassName("time-div");
				for( var i = 0; i < time_inputs.length; i++){
					time_inputs[i].style.display = "block";
				}
				var swim_inputs = document.getElementsByClassName("swim-select");
				for( var i = 0; i < swim_inputs.length; i++){
					swim_inputs[i].style.display = "block";
				}
				var run_inputs = document.getElementsByClassName("speed-select");
				for( var i = 0; i < run_inputs.length; i++){
					run_inputs[i].style.display = "none";
				}
				var bike_inputs = document.getElementsByClassName("bike-select");
				for( var i = 0; i < bike_inputs.length; i++){
					bike_inputs[i].style.display = "none";
				}
				var calories_inputs = document.getElementsByClassName("calculate_calories");
				for( var i = 0; i < calories_inputs.length; i++){
					calories_inputs[i].style.display = "block";
				}
			}
			else{
				var weight_inputs = document.getElementsByClassName("weight-div");
				for( var i = 0; i < weight_inputs.length; i++){
					weight_inputs[i].style.display = "none";
				}
				var time_inputs = document.getElementsByClassName("time-div");
				for( var i = 0; i < time_inputs.length; i++){
					time_inputs[i].style.display = "none";
				}
				var swim_inputs = document.getElementsByClassName("swim-select");
				for( var i = 0; i < swim_inputs.length; i++){
					swim_inputs[i].style.display = "none";
				}
				var run_inputs = document.getElementsByClassName("speed-select");
				for( var i = 0; i < run_inputs.length; i++){
					run_inputs[i].style.display = "none";
				}
				var bike_inputs = document.getElementsByClassName("bike-select");
				for( var i = 0; i < bike_inputs.length; i++){
					bike_inputs[i].style.display = "none";
				}
				var calories_inputs = document.getElementsByClassName("calculate_calories");
				for( var i = 0; i < calories_inputs.length; i++){
					calories_inputs[i].style.display = "none";
				}
			}
			var calories_burned_inputs = document.getElementsByClassName("calories-burned-div");
			for( var i = 0; i < calories_burned_inputs.length; i++){
				calories_burned_inputs[i].style.display = "none";
			}
		}
		function calculateCaloriesBurned(){
			var calories_burned_div = document.getElementById("calories_burned");
			var selected_activity = document.getElementById("activity_type").value;
			var weight = document.getElementById("weight").value;
			var time = document.getElementById("time").value;
			if(selected_activity == "bike"){
				var met = document.getElementById("bike-met").value;
				var kg = parseInt(weight) * 0.453592;
				var calories = parseInt(time) * (met * 3.5 * kg) / 200;
			}
			else if(selected_activity == "run"){
				var met = document.getElementById("running-met").value;
				var kg = parseInt(weight) * 0.453592;
				var calories = parseInt(time) * (met * 3.5 * kg) / 200;
			}
			else if(selected_activity == "swim"){
				var met = document.getElementById("swim-met").value;
				var kg = parseInt(weight) * 0.453592;
				var calories = parseInt(time) * (met * 3.5 * kg) / 200;
			}
			var calories_burned_inputs = document.getElementsByClassName("calories-burned-div");
			for( var i = 0; i < calories_burned_inputs.length; i++){
				calories_burned_inputs[i].style.display = "block";
			}
			calories_burned_div.innerHTML = Math.round(calories);
		}
	</script>
	<h1><?php echo $title;?></h1>
	<div class="calories-burned-inner-container">
		<p class="description">Please note: results are an estimate, actual results will vary depending on age, gender and overall fitness level.</p>
		<table>
			<tr>
				<td><label>Activity Type: </label></td>
				<td>
					<select id="activity_type" onchange="showOption()">
						<option selected ="selected" value="-1">Select Activity</option>
						<option value="bike">Bike</option>
						<option value="run">Run</option>
						<option value="swim">Swim</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding-bottom:0;">
					<label class="bike-select">Speed</label>
					<br class="bike-select" />
					<select class="bike-select" id="bike-met">
						<option value="14.0">bicycling, mountain, uphill, vigorous</option>
						<option value="16.0">bicycling, mountain, competitive, racing</option>
						<option value="8.5">bicycling, BMX</option>
						<option value="8.5">bicycling, mountain, general</option>
						<option value="7.5">bicycling, general</option>
						<option value="3.5">bicycling, leisure, 5.5 mph</option>
						<option value="5.8">bicycling, leisure, 9.4 mph</option>
						<option value="6.8">bicycling, 10-11.9 mph, slow, light effort</option>
						<option value="8.0">bicycling, 12-13.9 mph, moderate effort</option>
						<option value="10.0">bicycling, 14-15.9 mph, vigorous effort</option>
						<option value="12.0">bicycling, 16-19 mph, racing</option>
						<option value="15.8">bicycling, &gt; 20 mph, racing</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding-bottom:0;">
					<label class="speed-select">Speed</label>
					<br class="speed-select" />
					<select class="speed-select" id="running-met">
						<option value="6.0">running, 4 mph (15 min/mile)</option>
						<option value="8.3">running, 5 mph (12 min/mile)</option>
						<option value="9.0">running, 5.2 mph (11.5 min/mile)</option>
						<option value="9.8">running, 6 mph (10 min/mile)</option>
						<option value="10.5">running, 6.7 mph (9 min/mile)</option>
						<option value="11.0">running, 7 mph (8.5 min/mile)</option>
						<option value="11.8">running, 7.5 mph (8 min/mile)</option>
						<option value="11.8">running, 8 mph (7.5 min/mile)</option>
						<option value="12.3">running, 8.6 mph (7 min/mile)</option>
						<option value="12.8">running, 9 mph (6.5 min/mile)</option>
						<option value="14.5">running, 10 mph (6 min/mile)</option>
						<option value="16.0">running, 11 mph (5.5 min/mile)</option>
						<option value="19.0">running, 12 mph (5 min/mile)</option>
						<option value="19.8">running, 13 mph (4.6 min/mile)</option>
						<option value="23.0">running, 14 mph (4.3 min/mile)</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label class="swim-select">Speed</label>
					<br class="swim-select" />
					<select class="swim-select" id="swim-met">
						<option value="9.8">freestyle, fast, vigorous effort</option>
						<option value="5.8">freestyle, light or moderate effort</option>
						<option value="9.5">backstroke, training or competition</option>
						<option value="4.8">backstroke, recreational</option>
						<option value="10.3">breaststroke, training or competition</option>
						<option value="5.3">breaststroke, recreational</option>
						<option value="13.8">butterfly, general</option>
						<option value="10.0">crawl, fast speed, ~75 yds/min</option>
						<option value="8.3">crawl, medium speed, ~50 yds/min</option>
						<option value="7.0">sidestroke, general</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<label class="weight-div">Weight in pounds:</label>
				</td>
				<td>
					<input size="5" class="weight-div" type="text" id="weight"/>
				</td>
			</tr>
			<tr>
				<td>
					<label class="time-div">Time in minutes:</label>
				</td>
				<td>
					<input size="5" class="time-div" type="text" id="time"/>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input class="calculate_calories" type="submit" value="Calculate" onclick="calculateCaloriesBurned()"/>
				</td>
			</tr>
			<tr>
				<td>
					<h2 class="calories-burned-div">Calories Burned: </h2>
				</td>
				<td>
					<div class="calories-burned-div" id="calories_burned"></div>
				</td>
			</tr>
			</table>
		</div>
		<script>
			showOption();
		</script>
		</div>
		<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = $new_instance['title'];

		return $instance;
	}

	function form( $instance ) {

		$title = isset( $instance['title']) ? $instance['title'] : '';
	?>
		<p style="text-align:left;">
			<label><b>Title:</b></label>
			<input type="text" value="<?php echo $title;?>" id="<?php echo $this->get_field_id("title");?>"
				name="<?php echo $this->get_field_name("title");?>" size="29"/><br />
		</p>
		<?php
	}
}

//Register the marquee, and add action for everything.
function calories_burned_init() {
	register_widget('calories_burned_widget');
}

add_action('widgets_init', 'calories_burned_init');

// BEGIN CUSTOM WALKER CLASS TO ADD ONCLICK EVENTS FOR FOOTER NAV
class footer_walker extends Walker_Nav_Menu{
	//add the description to the menu item output
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;
		$bloginfo = get_bloginfo( 'url' );
		$name_array_slashes = explode('/', $bloginfo);
		$name_dots = explode('.', $name_array_slashes[2]);
		$name = strtolower($name_dots[0]);
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		$class_names = $value = '';
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';
		$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )  ? ' target="' . esc_attr( $item->target   ) .'"' : '';
		$attributes .= ! empty( $item->xfn )    ? ' rel="'  . esc_attr( $item->xfn    ) .'"' : '';
		$attributes .= ! empty( $item->url )    ? ' href="'   . esc_attr( $item->url    ) .'"' : '';
		$attributes .= ! empty( $item->url ) ? ' onclick="_gaq.push([\'_trackEvent\', \'Footer Links\', \''.$name.'\', \''. esc_attr( $item->url) .'\'])"' : '';
		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		if(strlen($item->description)>2){ $item_output .= '<br /><span class="sub">' . $item->description . '</span>'; }
		$item_output .= '</a>';
		$item_output .= $args->after;
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}
// END CUSTOM WALKER CLASS TO ADD ONCLICK EVENTS FOR FOOTER NAV

// youtube embed widget
class YouTubeEmbed extends WP_Widget {
	function YouTubeEmbed() {
		$widget_ops = array('classname' => 'YouTubeEmbed', 'description' => 'Embeds a YouTube playlist' );
		parent::__construct('YouTubeEmbed', 'YouTube Embed', $widget_ops);
	}

	function form($instance) {
		$instance = wp_parse_args( (array) $instance, array( 'playlist_ID' => '', 'title' => '', 'user_ID' => '' ) );
		$playlist_ID = $instance['playlist_ID'];
		$title = $instance['title'];
		$user_ID = $instance['user_ID']; ?>

		<p class="description">Enter EITHER a YouTube Playlist OR User ID</p>
		<p><label for="<?php echo $this->get_field_id('title'); ?>">Title:</label> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>
		<p><label for="<?php echo $this->get_field_id('playlist_ID'); ?>">Playlist ID:</label> <input class="widefat" id="<?php echo $this->get_field_id('playlist_ID'); ?>" name="<?php echo $this->get_field_name('playlist_ID'); ?>" type="text" value="<?php echo esc_attr($playlist_ID); ?>" /></p>
		<p><label for="<?php echo $this->get_field_id('user_ID'); ?>">User ID:</label> <input class="widefat" id="<?php echo $this->get_field_id('user_ID'); ?>" name="<?php echo $this->get_field_name('user_ID'); ?>" type="text" value="<?php echo esc_attr($user_ID); ?>" /></p>
	<?php }

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['playlist_ID'] = $new_instance['playlist_ID'];
		$instance['title'] = $new_instance['title'];
		$instance['user_ID'] = $new_instance['user_ID'];
		return $instance;
	}

	function widget($args, $instance) {
		extract($args, EXTR_SKIP);

		echo $before_widget;
		$playlist_ID = empty($instance['playlist_ID']) ? ' ' : apply_filters('widget_playlist_ID', $instance['playlist_ID']);
		$title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
		$user_ID = empty($instance['user_ID']) ? ' ' : apply_filters('widget_user_ID', $instance['user_ID']);

		// WIDGET CODE GOES HERE
		echo '
			<h2 style="font-size:1.66667em;">'.$title.'</h2>
		';
		if ($playlist_ID != ' ') {
			$random_num = rand(1, 1000);
			echo '
				<div id="ytThumbs-'.$random_num.'"></div>
				<script type="text/javascript">
					ytEmbed.init({
						"block":"ytThumbs-'.$random_num.'",
						"q":"'.$playlist_ID.'",
						"type":"playlist",
						"results":10,
						"order":"new_first",
						"player":"embed",
						"display_first":true,
						"layout":"thumbnails"
					});
				</script>
			';
		} elseif ($user_ID != ' ') {
			$random_num = rand(1, 1000);
			echo '
				<div id="ytThumbs-'.$random_num.'"></div>
				<script type="text/javascript">
					ytEmbed.init({
						"block":"ytThumbs-'.$random_num.'",
						"q":"'.$user_ID.'",
						"type":"user",
						"results":10,
						"order":"new_first",
						"display_first":true,
						"player":"embed",
						"layout":"thumbnails"
					});
				</script>
			';
		}

		echo $after_widget;
	}

}
add_action( 'widgets_init', create_function('', 'return register_widget("YouTubeEmbed");') );


function inline_iContact( $location_var = "home" ) {

	switch ($location_var) {
		case "home":
			$home_var = 'home_';
			$wid_pos = 'home';
		break;

		default:
			$home_var = '';
			$wid_pos	= "$location_var";
	}

	$editorial_theme_options	= get_option( 'cgi_media_editorial_options' );
	$magazine_cover				= $editorial_theme_options['magazine_ipad'];
	$footer_icontact_txt		= $editorial_theme_options['footer_icontact_txt'];

	$form_url					= "/wp-content/includes/newsletter-subscribe/signup-form.php";

	$newsletter_url			= home_url('/') . 'the-rundown?list-name=competitors-best';
	$newsletter_cta			= 'Get our monthly digital magazine, weekly running content & exclusive offers delivered to your inbox.';
	$newsletter_name		= 'Competitor Digital';
	$subscription_lead_in	= 'Start your FREE subscription to '. $newsletter_name .' today!';
	$ga_name				= 'competitor running';
	$blue_slash				= '<img class="slashes slash_blue" src="'. get_bloginfo('stylesheet_directory') .'/images/running/running-slashes-blue.svg">';

	if($location_var == "home" || $location_var == "sidebar") { ?>

		<div class="<?php echo $home_var; ?>mag_nl_dsktp module">
			<h3><?php echo $blue_slash; ?>Subscribe</h3>
			<div id="subscribe-widget">
				<p class="sub-leadin"><?php echo $subscription_lead_in; ?></p>
				<div class="form_container">
					<p><?php echo $footer_icontact_txt; ?></p>
					<?php include( ABSPATH . $form_url ); ?>
				</div>
			</div>
		</div>

	<?php } elseif ($location_var == "inarticle")  { ?>

		<div id="icontact_wrapper_inarticle">
			<div class="icontact_header">
				<div class="icontact_intro_text">
					<h3><?php echo $blue_slash; ?>Subscribe</h3>
					<p class="sub-leadin"><?php echo $subscription_lead_in; ?></p>
					<p><?php echo $newsletter_cta; ?></p>
				</div>
				<img src="<?php echo get_bloginfo("stylesheet_directory"); ?>/images/newsletter/logo-inline-icontact.png" class="nl_logo">
			</div>

			<?php include( ABSPATH . $form_url ); ?>

		</div>

	<?php } else { ?>

		<div class="footer_form_container">
			<h3><?php echo $blue_slash; ?>Subscribe</h3>
			<p class="sub-leadin"><?php echo $subscription_lead_in; ?></p>
			<p><?php echo $footer_icontact_txt; ?></p>
			<?php include( ABSPATH . $form_url ); ?>
		</div> <!-- end .footer_form_container -->

	<?php }
} //end function inline_iContact


// iContact embed widget
class iContactEmbed extends WP_Widget {
	function iContactEmbed() {
		$widget_ops = array('classname' => 'iContactEmbed', 'description' => 'Embeds an iContact form' );
		parent::__construct('iContactEmbed', 'iContact Embed', $widget_ops);
	}

	function form($instance) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
		$title = $instance['title']; ?>

		<p><label for="<?php echo $this->get_field_id('title'); ?>">Title:</label> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>
	<?php }

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = $new_instance['title'];
		return $instance;
	}

	function widget($args, $instance) {
		extract($args, EXTR_SKIP);

		echo $before_widget;
		$title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);

		// WIDGET CODE GOES HERE ?>

		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery("form#icontact_side").prepend("<div id='please_wait_side' style='display: none;'><img src='<?php echo get_bloginfo('stylesheet_directory'); ?>/images/icontact-wait.gif'/></div>");
				jQuery("form#icontact_side").submit(function(){
					jQuery("input#fields_email_side").attr("readonly", true);
					jQuery("#icontact_side .submit-button").attr("disabled", true);
					/* Validation */
					var error = new Boolean(false);
					var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
					if(
						jQuery("input#fields_email_side").val() == '' ||
						jQuery("input#fields_email_side").val() == jQuery("input#fields_email_side")[0].defaultValue ||
						regex.test(jQuery("input#fields_email_side").val()) == false
					) {
						jQuery("input#fields_email_side").addClass("validation-error");
						error = true;
						jQuery("#errorMessage_side").html('There was an error with your email address.');
						jQuery("input#fields_email_side").removeAttr("readonly");
						jQuery("#icontact_side .submit-button").removeAttr("disabled");
					} else {
						jQuery("input#fields_email_side").removeClass("validation-error");
					}
					if(false != error){
						return false;
					};
					jQuery("input#fields_email_side").hide();
					jQuery("#icontact_side .submit-button").hide();
					jQuery("#please_wait_side").show();
					/* Ajax submission code here */
					//Submit form
					jQuery.post(
						"./",
						jQuery("#icontact_side").serialize() + "&ajax=true",
						 function(data){
							 jQuery("div#icontact_wrapper_side").html(data);
						 }
					);
					return false;

				});
				jQuery("#icontact_side input[type=text]").focus(function(){
					if (jQuery(this).val() == this.defaultValue) {
						jQuery(this).val("").removeClass("default");
					}
					jQuery(this).removeClass("validation-error");
				});
				jQuery("#icontact_side input[type=text]").blur(function(){
					if (jQuery(this).val() == "") {
						jQuery(this).val(this.defaultValue).addClass("default");
					}
				});
			});
		</script>
		<?php
			$blogurl = explode ('.', get_option( 'home' ));
			if ($blogurl[0] == 'http://running') {
				$newsletter_url = home_url('/') . 'the-rundown';
				$newsletter_name = 'Competitor Running';
				$ga_name = 'competitor running';
			} elseif ($blogurl[0] == 'http://triathlon') {
				$newsletter_url = home_url('/') . 'the-transition';
				$newsletter_name = 'Triathlete';
				$ga_name = 'triathlon';
			} elseif ($blogurl[0] == 'http://velonews') {
				$newsletter_url = home_url('/') . 'the-prologue';
				$newsletter_name = 'VeloNews';
				$ga_name = 'velo';
			}
		?>
		<div id="icontact_wrapper_side">
			<div class="icontact_header_side">
				<div class="icontact_intro_text_side">
					<h2><?php echo $title; ?></h2>
				</div>
			</div>
			<form name="icontact_side" id="icontact_side" method="POST" action="" onsubmit="_gaq.push(['_trackEvent', 'newsletter <?php echo $ga_name; ?>', 'subscribe', 'sidebar']);">
				<input type="text" name="fields_email_side" id="fields_email_side" class="default" value="Email" />
				<input type="hidden" name="icontact_side" value="true" />
				<input class="submit-button" type="submit" value="Subscribe" />
			</form>
			<div id="errorMessage_side"></div>
			<div class="icontact_mini_links">
				<div class="nl_privacy_link">
					<a href="http://pocketoutdoormedia.com/privacy-policy/" target="_blank">Privacy Policy</a>
				</div>
			</div>
		</div>

		<?php echo $after_widget;
	}

}
add_action( 'widgets_init', create_function('', 'return register_widget("iContactEmbed");') );

function related_stories() {
	$post_ID	= get_the_ID();
	$tag_IDs	= wp_get_post_tags( $post_ID, array( 'fields' => 'ids' ) );
	if ( !empty( $tag_IDs ) ) {

		$args = array(
			'post__not_in'				=> array( $post_ID ),
			'tag__in'					=> $tag_IDs,
			'posts_per_page'			=> 5,
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false,
			'ignore_sticky_posts'		=> true,
		);

		$related_posts	= new WP_Query( $args );

		if ( $related_posts->have_posts() ) : ?>
			<div class="related-posts-container">
				<h3><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> Related Articles</h3>
				<ul>

				<?php while ( $related_posts->have_posts() ) : $related_posts->the_post();
					$permalink	= get_permalink();
					$title		= get_the_title();
					?>
						<li><a href="<?php echo $permalink; ?>" onClick="_gaq.push(['_trackEvent', 'Related Stories', 'Click', '<?php echo $permalink; ?>']);"><?php echo $title; ?></a></li>
				<?php endwhile; ?>

				</ul>
			</div>
		<?php endif;

		wp_reset_postdata();
	}

}


/**
 * gift/gear guide redirects
 * gift == winter
 * gear == summer
 */
// require_once('gift-guide-theme-options.php');
// add_action('template_redirect','gift_guide_redirect');

// function gift_guide_redirect(){
// 	global $post;
// 	if(is_single()){
// 		$gift_guide_cat = 'competitor-gift-guide';
// 		if(in_category($gift_guide_cat)){
// 			include (STYLESHEETPATH . '/gift-guide-template.php');
// 			exit;
// 		}
// 	}
// }


// require_once('gift-guide-2016-theme-options.php');
// add_action( 'template_redirect', 'gift_guide_2016_redirect' );

// function gift_guide_2016_redirect() {
// 	global $post;

// 	if( is_single() ) {
// 		$gift_guide_cat = '2016-gift-guide';

// 		if( in_category($gift_guide_cat ) ){
// 			include( STYLESHEETPATH . '/gift-guide-2016-template.php' );
// 			exit;
// 		}

// 	}
// }


// require_once('running-gear-guide-theme-options.php');
// add_action('template_redirect','running_gear_guide_redirect');

// function running_gear_guide_redirect(){
// 	global $post;
// 	if(is_single()){
// 		$running_gear_guide_cat = '2015-running-gear-guide';
// 		if(in_category($running_gear_guide_cat)){
// 			include (STYLESHEETPATH . '/running-gear-guide-template.php');
// 			exit;
// 		}
// 	}
// }


// require_once('running-2016-gear-guide-theme-options.php');
// add_action('template_redirect','running_2016_gear_guide_redirect');

// function running_2016_gear_guide_redirect(){
// 	global $post;
// 	if(is_single()){
// 		$running_2016_gear_guide_cat = '2016-running-gear-guide';
// 		if(in_category($running_2016_gear_guide_cat)){
// 			include (STYLESHEETPATH . '/running-2016-gear-guide-template.php');
// 			exit;
// 		}
// 	}
// }


// require_once('running-2017-gear-guide-theme-options.php');
// add_action('template_redirect','running_2017_gear_guide_redirect');

// function running_2017_gear_guide_redirect(){
// 	global $post;
// 	if(is_single()){
// 		if(in_category( '2017-running-gear-guide' )){
// 			include (STYLESHEETPATH . '/running-2017-gear-guide-template.php');
// 			exit;
// 		}
// 	}
// }


/**
 * function: get meebo network
 */
function get_meebo_network( $meebonetwork ) {
	if ( $meebonetwork && $meebonetwork != '' ) {
		$GLOBALS['meebo'] = true; ?>
		<script type="text/javascript">
			window.Meebo||function(c){function p(){return["<",i,' onload="var d=',g,";d.getElementsByTagName('head')[0].",
			j,"(d.",h,"('script')).",k,"='//cim.meebo.com/cim?iv=",a.v,"&",q,"=",c[q],c[l]?
			"&"+l+"="+c[l]:"",c[e]?"&"+e+"="+c[e]:"","'\"></",i,">"].join("")}var f=window,
			a=f.Meebo=f.Meebo||function(){(a._=a._||[]).push(arguments)},d=document,i="body",
			m=d[i],r;if(!m){r=arguments.callee;return setTimeout(function(){r(c)},100)}a.$=
			{0:+new Date};a.T=function(u){a.$[u]=new Date-a.$[0]};a.v=5;var j="appendChild",
			h="createElement",k="src",l="lang",q="network",e="domain",n=d[h]("div"),v=n[j](d[h]("m")),
			b=d[h]("iframe"),g="document",o,s=function(){a.T("load");a("load")};f.addEventListener?
			f.addEventListener("load",s,false):f.attachEvent("onload",s);n.style.display="none";
			m.insertBefore(n,m.firstChild).id="meebo";b.frameBorder="0";b.name=b.id="meebo-iframe";
			b.allowTransparency="true";v[j](b);try{b.contentWindow[g].open()}catch(w){c[e]=
			d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{var t=
			b.contentWindow[g];t.write(p());t.close()}catch(x){b[k]=o+'d.write("'+p().replace(/"/g,
			'\\"')+'");d.close();'}a.T(1)}({network:"<?php echo $meebonetwork; ?>"});
		</script>
	<?php }
}

/**
 * Function to echo out the FB comment count
 * notes:
 * - not really in use since comments are not in use at the moment
 * - placed here when removing dependency of PostType.php file
 *
 */
function print_comments_number() {
	$write_comments = '<a href="#post-comments" id="comment-count"><fb:comments-count href="' . get_permalink() . '" /></fb:comments-count> comments</a>' . "\n";
	echo $write_comments;
}

/**
 * Check if we should echo the social sharing media buttons
 * with two positions available
 *
 * @param  string $position Determines markup for position of buttons
 */
function print_social_media( $position = 'top' ) {

	$technical_theme_options	= get_option( 'cgi_media_technical_options' );

	if ( !empty( $technical_theme_options['social_media_publisher_key'] ) ) {

		$twitter_account	= $technical_theme_options['site_twitter_handle']; ?>

		<?php if( $position == 'left' ) { ?>

			<div id="sharingWidgetSide" pw:twitter-via="<?php echo $twitter_account;?>">
				<div class="pw-widget" data-layout="vertical" data-size="48">
					<a class="pw-button-facebook" onclick="gaTrack('Facebook','share')"></a>
					<a class="pw-button-twitter" onclick="gaTrack('Twitter','tweet')"></a>
					<a class="pw-button-email" onclick="gaTrack('Email','email')"></a>
				</div>
			</div>

		<?php } else { ?>

			<div id="sharingWidgetTop" pw:twitter-via="<?php echo $twitter_account;?>">
				<div class="pw-widget">
					<a class="pw-button-facebook" data-label="Share" onclick="gaTrack('Facebook','share')"></a>
					<a class="pw-button-twitter" data-label="Tweet" onclick="gaTrack('Twitter','tweet')"></a>
					<a class="pw-button-email" data-label="Email" onclick="gaTrack('Email','email')"></a>
				</div>
			</div>

		<?php }

	}

}

/**
 * Print the taxonomies for a post
 */
function print_taxonomy() {

	$slash_image_source	= get_bloginfo('stylesheet_directory').'/images/running/running-slashes-blue.png';
	$category_list		= '';

	if ( in_category('sponsored') ){

		$categories   = get_the_category();

		foreach ( $categories as $key => $category ) {

			$category_name	= $category->name;
			$category_link	= get_category_link( $category->cat_ID );

			$category_list	.= '<a href="'. $category_link .'" rel="category_tag">'. $category_name .'</a>';

			if ( $key != ( count( $categories ) - 1 ) ) {
				$category_list	.= ' / ';
			};

		}

	} else {
		$category_list	= get_the_category_list(' / ');
	}

	echo '<h3><img class="slashes" src="'. $slash_image_source .'"> Filed Under: '. $category_list . '</h3>';

	echo '<div class="post_tags">'. get_the_tag_list('',' ','') .'</div>';

}
