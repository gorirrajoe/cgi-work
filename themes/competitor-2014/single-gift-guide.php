<?php
	// single.php will handle displaying posts, attachments and custom posttypes
	get_header();


	if ( have_posts() ) :  while ( have_posts() ) :  the_post();

		// set up to automatically add future years
		/*$all_cats			= get_categories( array( 'hide_empty' => false ) ); // get all the categories including empty
		$gift_guide_array	= [];

		foreach( $all_cats as $key => $value ) {

			if ( strpos( $value->slug, 'gift-guide' ) !== false ) { // check for gift guide cats

				array_push( $gift_guide_array, $value->slug ); // put them in an array

			}

		}*/

		if( in_category( 'competitor-gift-guide' ) ) {

			get_template_part( 'template-parts/gift-guides/content', 'competitor-gift-guide-single' );

		} elseif( in_category( '2016-gift-guide' ) ) {

			get_template_part( 'template-parts/gift-guides/content', '2016-gift-guide-single' );

		} else {

			get_template_part( 'template-parts/gift-guides/content', 'final-gift-guide-single' );

		}

	endwhile; else: ?>

		<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

	<?php endif;

		get_footer(); ?>
