<?php
/* Template Name: Automated Newsletter */

  // brand colors
  $blogurl = explode ('.', get_option( 'home' ));
  if ($blogurl[0] == 'http://running') {
    $brand_color = '#003e99';
    $campaign_var = 'newsletter_rundown';
    $newsletter_title = 'The Rundown';
  } elseif ($blogurl[0] == 'http://triathlon') {
    $brand_color = '#9c399c';
    $campaign_var = 'newsletter_transition';
  } elseif ($blogurl[0] == 'http://velonews') {
    $brand_color = '#c01100';
    $campaign_var = 'newsletter_prologue';
  }
  // Load options and display accordingly
  $technical_theme_options = get_option( 'cgi_media_technical_options' );
  $editorial_theme_options = get_option( 'cgi_media_editorial_options' );

  // Title and logo
  $newsletter_title = get_the_title();
  $logo = get_bloginfo('stylesheet_directory') . '/images/newsletter/logo.jpg';
  $facebook = $technical_theme_options['site_facebook_page'];
  if ( $facebook != '' ) {
    $facebook = '<a href="https://www.facebook.com/' . $technical_theme_options['site_facebook_page'] . '" style="text-decoration: none;color: '.$brand_color.';"><img src="' . get_bloginfo( 'template_url' ). '/images/newsletter/facebook.png"/></a>';
  }
  $twitter = $technical_theme_options['site_twitter_handle'];
  if ( $twitter != '' ) {
    $twitter = '<a href="https://twitter.com/' . $technical_theme_options['site_twitter_handle'] . '" style="text-decoration: none;color: '.$brand_color.';"><img src="' . get_bloginfo( 'template_url' ). '/images/newsletter/twitter.png"/></a>';
  }
  $twitter_share = get_bloginfo( 'template_url' ).'/images/newsletter/twitter-inverse.png';
  $facebook_share = get_bloginfo( 'template_url' ).'/images/newsletter/facebook-inverse.png';
  $newsletter_sponsor_image = $editorial_theme_options['newsletter_sponsor_image'];
  $newsletter_sponsor_link = $editorial_theme_options['newsletter_sponsor_link'];
  // Setup ad codes
  $ad_code_newsletter_name = str_replace(' ', '-', $newsletter_title) . '-Newsletter';

  //old calls
  //$leaderboard_ad = '<a href="http://ad.doubleclick.net/N8221/jump/'.$ad_code_newsletter_name.';dcov=r;sz=615x76;ord=12345?"><img src="http://ad.doubleclick.net/N8221/ad/'.$ad_code_newsletter_name.';dcov=r;sz=615x76;ord=12345?" width="615" height="76" /></a>';
  //$threeby250_ad = '<a href="http://ad.doubleclick.net/N8221/jump/'.$ad_code_newsletter_name.';pos=top;dcov=r;sz=300x250;ord=123456?"><img src="http://ad.doubleclick.net/N8221/ad/'.$ad_code_newsletter_name.';pos=top;dcov=r;sz=300x250;ord=123456?" width="300" height="250" /></a>';
  //$threeby250_ad2 = '<a href="http://ad.doubleclick.net/N8221/jump/'.$ad_code_newsletter_name.';pos=bottom;dcov=r;sz=300x250;ord=123457?"><img src="http://ad.doubleclick.net/N8221/ad/'.$ad_code_newsletter_name.';pos=bottom;dcov=r;sz=300x250;ord=123457?" width="300" height="250" /></a>';

  //new calls
  /*$leaderboard_ad_link = "%%=Concat('http://pubads.g.doubleclick.net/gampad/jump?co=1&iu=/8221/The-Rundown-Newsletter&sz=615x76&c=',@rand1)=%%";
  $threeby250_ad1_link = "%%=Concat('http://pubads.g.doubleclick.net/gampad/jump?co=1&iu=/8221/The-Rundown-Newsletter&sz=300x250&tile=1&c=',@rand2)=%%";
  $threeby250_ad2_link = "%%=Concat('http://pubads.g.doubleclick.net/gampad/jump?co=1&iu=/8221/The-Rundown-Newsletter&sz=300x250&tile=2&c=',@rand3)=%%";

  $leaderboard_ad = '<a href="' . $leaderboard_ad_link .'"><img src="http://pubads.g.doubleclick.net/gampad/ad?co=1&iu=/8221/The-Rundown-Newsletter&sz=615x76&c=%%=v(@rand1)=%%" width="615" height="76" /></a>';
  $threeby250_ad = '<a href="' . $threeby250_ad1_link .'"><img src="http://pubads.g.doubleclick.net/gampad/ad?co=1&iu=/8221/The-Rundown-Newsletter&sz=300x250&tile=1&c=%%=v(@rand2)=%%" width="300" height="250" /></a>';
  $threeby250_ad2 = '<a href="' . $threeby250_ad2_link .'"><img src="http://pubads.g.doubleclick.net/gampad/ad?co=1&iu=/8221/The-Rundown-Newsletter&sz=300x250&tile=2&c=%%=v(@rand3)=%%" width="300" height="250" /></a>';*/


  //new calls
  $leaderboard_ad = '<table width="100%" border="0" cellpadding="0" cellspacing="0"  align="center" style="margin-left:auto; margin-right:auto;max-width:620px;"><tr><td colspan="2"><a href="http://li.competitor.com/click?s=201357&layout=marquee&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.competitor.com/imp?s=201357&layout=marquee&e=%%emailaddr%%&p=%%jobid%%" border="0" style="display: block; width:100%; height:auto;" width="620" /></a></td></tr><tr style="display:block; height:1px; line-height:1px;"><td><img src="http://li.competitor.com/imp?s=201358&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /></td><td><img src="http://li.competitor.com/imp?s=201359&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /></td></tr><tr><td align="left"><a href="http://li.competitor.com/click?s=201360&sz=116x15&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.competitor.com/imp?s=201360&sz=116x15&e=%%emailaddr%%&p=%%jobid%%" border="0"/></a></td><td align="right"><a href="http://li.competitor.com/click?s=201361&sz=69x15&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.competitor.com/imp?s=201361&sz=69x15&e=%%emailaddr%%&p=%%jobid%%" border="0"/></a></td></tr></table>';
  $threeby250_ad = '<table border="0" cellpadding="0" cellspacing="0" align="center"><tr><td colspan="2"><a style="display: block; width: 300px; height: 250px;" href="http://li.competitor.com/click?s=201351&sz=300x250&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.competitor.com/imp?s=201351&sz=300x250&e=%%emailaddr%%&p=%%jobid%%" border="0" width="300" height="250"/></a></td></tr><tr style="display:block; height:1px; line-height:1px;"><td><img src="http://li.competitor.com/imp?s=201352&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /></td><td><img src="http://li.competitor.com/imp?s=201353&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /></td></tr><tr><td align="left"><a href="http://li.competitor.com/click?s=201120&sz=116x15&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.competitor.com/imp?s=201120&sz=116x15&e=%%emailaddr%%&p=%%jobid%%" border="0"/></a></td><td align="right"><a href="http://li.competitor.com/click?s=201121&sz=69x15&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.competitor.com/imp?s=201121&sz=69x15&e=%%emailaddr%%&p=%%jobid%%" border="0"/></a></td></tr></table>';
  $threeby250_ad2 = '<table border="0" cellpadding="0" cellspacing="0" align="center"><tr><td colspan="2"><a style="display: block; width: 300px; height: 250px;" href="http://li.competitor.com/click?s=201354&sz=300x250&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.competitor.com/imp?s=201354&sz=300x250&e=%%emailaddr%%&p=%%jobid%%" border="0" width="300" height="250"/></a></td></tr><tr style="display:block; height:1px; line-height:1px;"><td><img src="http://li.competitor.com/imp?s=201355&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /></td><td><img src="http://li.competitor.com/imp?s=201356&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /></td></tr><tr><td align="left"><a href="http://li.competitor.com/click?s=201120&sz=116x15&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.competitor.com/imp?s=201120&sz=116x15&e=%%emailaddr%%&p=%%jobid%%" border="0"/></a></td><td align="right"><a href="http://li.competitor.com/click?s=201121&sz=69x15&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.competitor.com/imp?s=201121&sz=69x15&e=%%emailaddr%%&p=%%jobid%%" border="0"/></a></td></tr></table>';
  $leaderboard_ad_bottom = '<table border="0" cellpadding="0" cellspacing="0"  align="center" style="margin-left:auto; margin-right:auto;max-width:620px;"><tr><td colspan="2"><a href="http://li.competitor.com/click?s=201362&layout=marquee&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.competitor.com/imp?s=201362&layout=marquee&e=%%emailaddr%%&p=%%jobid%%" border="0" style="display: block; width:100%; height:auto;" width="620" /></a></td></tr><tr style="display:block; height:1px; line-height:1px;"><td><img src="http://li.competitor.com/imp?s=201363&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /></td><td><img src="http://li.competitor.com/imp?s=201364&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /></td></tr><tr><td align="left"><a href="http://li.competitor.com/click?s=201365&sz=116x15&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.competitor.com/imp?s=201365&sz=116x15&e=%%emailaddr%%&p=%%jobid%%" border="0"/></a></td><td align="right"><a href="http://li.competitor.com/click?s=201366&sz=69x15&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.competitor.com/imp?s=201366&sz=69x15&e=%%emailaddr%%&p=%%jobid%%" border="0"/></a></td></tr></table>';
  $safe_rtb = '<table cellpadding="0" cellspacing="0" border="0" width="40" height="6"><tbody><tr><td><img src="http://li.competitor.com/imp?s=124521500&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.competitor.com/imp?s=124521501&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.competitor.com/imp?s=124521502&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.competitor.com/imp?s=124521503&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.competitor.com/imp?s=124521504&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.competitor.com/imp?s=124521505&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.competitor.com/imp?s=124521506&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.competitor.com/imp?s=124521507&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.competitor.com/imp?s=124521508&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.competitor.com/imp?s=124521509&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.competitor.com/imp?s=124521510&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.competitor.com/imp?s=124521511&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.competitor.com/imp?s=124521512&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.competitor.com/imp?s=124521513&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.competitor.com/imp?s=124521514&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.competitor.com/imp?s=124521515&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.competitor.com/imp?s=124521516&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.competitor.com/imp?s=124521517&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.competitor.com/imp?s=124521518&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td><td><img src="http://li.competitor.com/imp?s=124521519&sz=2x1&e=%%emailaddr%%&p=%%jobid%%" width="2" height="6" border="0" /></td></tr></tbody></table>';

  $native_ad  = '<table align="center" border="0" cellpadding="0" cellspacing="0" id="li-container" style="width:600px; margin:0 auto; font-size:0; line-height:0; border-collapse: collapse; background-color:#FFFFFF;"> <tr> <td class="adcontent" style="overflow:hidden; width:50%; padding:5px;"> <img src="https://s3-eu-west-1.amazonaws.com/avari-production-content-blocks-images/sponsored_content.png" width="217" height="22" border="0" style="display:inline-block;"> </td> <td class="adchoices" style="overflow:hidden; width:50%; vertical-align:bottom; text-align:right; padding-bottom:5px; padding-right:5px;"> <a href="http://li.competitor.com/click?s=201121&sz=69x15&e=%%emailaddr%%&p=%%jobid%%" rel="nofollow"><img src="http://li.competitor.com/imp?s=201121&sz=69x15&e=%%emailaddr%%&p=%%jobid%%" border="0"/></a> </td> </tr> <tr> <td colspan="2" align="center" width="100%" style="width:100%; text-align:center; font-size:0; line-height:0; vertical-align: top;"> <!--[if (gte mso 9)|(IE)]> <table width="600" align="center" cellpadding="0" cellspacing="0" border="0"> <tr> <td> <![endif]--> <!--[if (gte mso 9)|(IE)]> <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"> <tr><td width="150" valign="top"> <![endif]--> <div style="width:149px; display:inline-block; text-align:center; border:0; vertical-align:top; margin-bottom: 10px;" class="fullw"> <a href="http://li.competitor.com/click?s=246928&layout=recommendation_widget&e=%%emailaddr%%&p=%%jobid%%" target="_blank"><img src="http://li.competitor.com/imp?s=246928&layout=recommendation_widget&e=%%emailaddr%%&p=%%jobid%%" width="142" class="orgw"></a> <img src="http://li.competitor.com/imp?s=246929&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /><img src="http://li.competitor.com/imp?s=246930&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /> </div> <!--[if (gte mso 9)|(IE)]> </td><td width="150" align="center" valign="top"> <![endif]--> <div style="width:149px; display:inline-block; text-align:center; border:0; vertical-align:top; margin-bottom: 10px;" class="fullw"> <a href="http://li.competitor.com/click?s=246931&layout=recommendation_widget&e=%%emailaddr%%&p=%%jobid%%" target="_blank"><img src="http://li.competitor.com/imp?s=246931&layout=recommendation_widget&e=%%emailaddr%%&p=%%jobid%%" width="142" class="orgw"></a> <img src="http://li.competitor.com/imp?s=246932&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /><img src="http://li.competitor.com/imp?s=246933&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /> </div> <!--[if (gte mso 9)|(IE)]> </td><td width="150" align="center" valign="top"> <![endif]--> <div style="width:149px; display:inline-block; text-align:center; border:0; vertical-align:top; margin-bottom: 10px;" class="fullw"> <a href="http://li.competitor.com/click?s=246934&layout=recommendation_widget&e=%%emailaddr%%&p=%%jobid%%" target="_blank"><img src="http://li.competitor.com/imp?s=246934&layout=recommendation_widget&e=%%emailaddr%%&p=%%jobid%%" width="142" class="orgw"></a> <img src="http://li.competitor.com/imp?s=246935&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /><img src="http://li.competitor.com/imp?s=246936&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /> </div> <!--[if (gte mso 9)|(IE)]> </td><td width="150" valign="top"> <![endif]--> <div style="width:149px; display:inline-block; text-align:center; border:0; vertical-align:top; margin-bottom: 10px;" class="fullw"> <a href="http://li.competitor.com/click?s=246937&layout=recommendation_widget&e=%%emailaddr%%&p=%%jobid%%" target="_blank"><img src="http://li.competitor.com/imp?s=246937&layout=recommendation_widget&e=%%emailaddr%%&p=%%jobid%%" width="142" class="orgw"></a> <img src="http://li.competitor.com/imp?s=246938&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /><img src="http://li.competitor.com/imp?s=246939&sz=1x1&e=%%emailaddr%%&p=%%jobid%%" height="1" width="10" /> </div> <!--[if (gte mso 9)|(IE)]> </td></tr> </table> <![endif]--> <!--[if (gte mso 9)|(IE)]> </td> </tr> </table> <![endif]--> </td> </tr> </table>';

  $threeby100_ad1 = '';
  $threeby100_ad2 = '';
  // Post ID's to keep from duplicates showing up
  $post_id_array = array();

	/* this array requires 5 post ids, a query will run for
	each to populate the newsletter */
	//$newsletter_posts = '';
	$newsletter_posts = array();
	if($_GET['list-name'] == 'training'){
		if ($editorial_theme_options['newsletter_train_posts']) {
			$newsletter_posts = explode( ',' , $editorial_theme_options['newsletter_train_posts'] );
		}
		if(!empty($editorial_theme_options['newsletter_train_logo_image'])){
			$header_image = $editorial_theme_options['newsletter_train_logo_image'];
		}else{
			$header_image = $logo;
		}
		if(!empty($editorial_theme_options['newsletter_train_logo_link'])){
			$header_image_link = $editorial_theme_options['newsletter_train_logo_link'];
		}else{
			$header_image_link = get_option('home');
		}
	}elseif($_GET['list-name'] == 'shoes-and-gear'){
		if ($editorial_theme_options['newsletter_shoe_posts']) {
			$newsletter_posts = explode( ',' , $editorial_theme_options['newsletter_shoe_posts'] );
		}
		if(!empty($editorial_theme_options['newsletter_shoe_logo_image'])){
			$header_image = $editorial_theme_options['newsletter_shoe_logo_image'];
		}else{
			$header_image = $logo;
		}
		if(!empty($editorial_theme_options['newsletter_shoe_logo_link'])){
			$header_image_link = $editorial_theme_options['newsletter_shoe_logo_link'];
		}else{
			$header_image_link = get_option('home');
		}
	}elseif($_GET['list-name'] == 'nutrition'){
		if ($editorial_theme_options['newsletter_nut_posts']) {
			$newsletter_posts = explode( ',' , $editorial_theme_options['newsletter_nut_posts'] );
		}
		if(!empty($editorial_theme_options['newsletter_nut_logo_image'])){
			$header_image = $editorial_theme_options['newsletter_nut_logo_image'];
		}else{
			$header_image = $logo;
		}
		if(!empty($editorial_theme_options['newsletter_nut_logo_link'])){
			$header_image_link = $editorial_theme_options['newsletter_nut_logo_link'];
		}else{
			$header_image_link = get_option('home');
		}
	}else{
		if ($editorial_theme_options['newsletter_best_posts']) {
			$newsletter_posts = explode( ',' , $editorial_theme_options['newsletter_best_posts'] );
		}/*elseif ($editorial_theme_options['newsletter_posts']) {
			$newsletter_posts = explode( ',' , $editorial_theme_options['newsletter_posts'] );
	    	// print_r( $newsletter_posts);
		}*/
		if(!empty($editorial_theme_options['newsletter_best_logo_image'])){
			$header_image = $editorial_theme_options['newsletter_best_logo_image'];
		}else{
			$header_image = $logo;
		}
		if(!empty($editorial_theme_options['newsletter_best_logo_link'])){
			$header_image_link = $editorial_theme_options['newsletter_best_logo_link'];
		}else{
			$header_image_link = get_option('home');
		}
	}/*else{
		if ($editorial_theme_options['newsletter_posts']) {
			$newsletter_posts = explode( ',' , $editorial_theme_options['newsletter_posts'] );
	    // print_r( $newsletter_posts);
		}
		$header_image = $logo;
		$header_image_link = get_option('home');
	}*/

  $sticky = get_option( 'sticky_posts' );
  // Recent stories for sidebar
  $args = array(
    'post_type' => 'post',
    'post__not_in' => $sticky,
    'posts_per_page' => 12,
  );
  $original_query = $wp_query;
  $wp_query = null;
  $wp_query = wp_cache_get( 'newsletter_latest_posts' );
  if ($wp_query == TRUE) {
    if ($wp_query->have_posts() === false) {
      $wp_query = new WP_Query($args);
      wp_cache_replace( 'newsletter_latest_posts', $wp_query, '', 600 );
    }
  } else {
    $wp_query = new WP_Query($args);
    wp_cache_set( 'newsletter_latest_posts', $wp_query, '', 600 );
  }
  $count = 0;
	$latest_news = '<table cellspacing="0" border="0" cellpadding="0">';
  while ( $wp_query->have_posts() ) : $wp_query->the_post();
    if(!in_array(get_the_ID(), $newsletter_posts) && $count < 5 ){
      $post_id_array[] = get_the_ID();
      $title = get_the_title();
      //$title = (strlen(get_the_title()) > 34) ? substr(get_the_title(),0,31).'...' : get_the_title();

      $link = wp_get_shortlink();
			$latest_news .= '<tr><td height="20" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; padding-top: 5px;"><img src="' . get_bloginfo( 'template_url' ) . '/images/newsletter/bullet.gif" alt="" style="border: 0;" width="15" height="15" /></td><td class="list" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; padding-top: 5px;"><a href = "'. $link .'" style="color: '.$brand_color.'; font-family: Arial; font-size: 13px; text-decoration: none;"><span style="font-family:\'Open Sans\',Arial,Helvetica,sans-serif !important;">' . $title . '</span></a></td></tr>';
      $count++;
    }
  endwhile;
  $latest_news .= "</table>";
  $wp_query = null;
  $wp_query = $original_query;
  wp_reset_postdata();


  // Get 5 Stories in order which gives editors control over the main stories in Newsletter
  // $stories = '<table cellspacing="0" style="border-right: 1px solid #ccc;" cellpadding="0">';
  $stories = '';
  foreach ( $newsletter_posts as $count => $newsletter_post ) {
    $args = array(
      'p' => $newsletter_posts[$count]
    );
    // print_r($args);
    $original_query = $wp_query;
    $wp_query = NULL;

    $wp_query = new WP_Query($args);
    if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();
      if(!in_array(get_the_ID(), $post_id_array)){
        $post_id_array[] = get_the_ID();
        $title_link = '<a href="'. wp_get_shortlink() .'" title="Read full story" style="font-family: Arial, sans-serif; font-size:21px; line-height:24px; color:#333; margin:0; font-weight:bold; text-decoration:none;"><span style="font-family:\'Open Sans\',Arial,Helvetica,sans-serif !important; font-style:italic;">' .  get_the_title() . '</span></a>';
        $excerpt = get_the_excerpt();
        if ($count == 0) {
          $image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'newsletter-splash');
          $image_link = '<a href="'. wp_get_shortlink() .'" title="Read full story" style="text-decoration: none; color: '.$brand_color.';"><img src="' . $image_array[0] . '" class="max-size" width="620" height="400" /></a>';

          $stories .= '
					<tr>
                      <td width="100%" class="text-center">' . $image_link . '</td>
                    </tr>
                    <tr>
                      <td width="100%" bgcolor="#eaeaea">
                        <table width="100%" align="center" cellpadding="15" cellspacing="0" border="0">
                          <tr>
                            <td width="100%" style="font-family:Arial,Helvetica,sans-serif;font-size:15px;line-height:20px;color:#555; border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                              ' . $title_link . '
                              <br>
                              <span style="font-family:\'Open Sans\',Arial,Helvetica,sans-serif !important;">' . $excerpt . '</span>
                              <br /><br />
                              <a href="'.wp_get_shortlink().'" class="button" style="color:'.$brand_color.';font-family: Arial, sans-serif;font-size:13px;line-height:19px;"><span style="font-family:\'Open Sans\',Arial,Helvetica,sans-serif !important;">Find out more</span></a>
                            </td>
                          </tr>
                        </table>
                        <table width="180" align="right" cellpadding="15" cellspacing="0" border="0">
                          <tr>
                            <td bgcolor="#eaeaea"><span>Share:</span></td>
                            <td bgcolor="#eaeaea"><a  target="_blank" href="http://twitter.com/intent/tweet?original_referer=&text='.get_the_title().'&url='.wp_get_shortlink().'"><img src="'.$twitter_share.'" width="32" height="32"></a></td>
                            <td bgcolor="#eaeaea"><a href="https://www.facebook.com/sharer/sharer.php?u='.wp_get_shortlink().'" target="_blank"><img src="'.$facebook_share.'" width="32" height="32"></a></td>
                          </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                      <td width="100%" height="20" bgcolor="#ffffff"><br /></td>
                    </tr>
					<!-- end content wrap -->
                  </table>
                </td>
              </tr>
            </table>

          ';
        } elseif ($count < 5) {
          if ($count == 1) {
            $image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'newsletter-image');
            $image_link = '<a href="'. wp_get_shortlink() .'" title="Read full story" style="text-decoration: none; color: '.$brand_color.';"><img src="' . $image_array[0] . '" class="max-size" width="300" height="200" /></a>';

            $stories .= '
              <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="font-family:Arial,Helvetica,sans-serif;font-size:15px;line-height:20px;color:#555;">
                <tr>
                  <td width="100%">
                    <table width="620" cellpadding="0" cellspacing="0" border="0" align="center" class="content_wrap">
                      <tr>
                        <td width="100%">
                          <!-- content 2 -->
                          <table class="full_width" width="300" align="left" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                              <td class="text-center">
                                '.$image_link.'
                              </td>
                            </tr>
                            <tr>
                              <td bgcolor="#eaeaea" width="100%" valign="top">
                                <table width="300" cellpadding="15" cellspacing="0" border="0" class="full_width">
                                  <tr>
                                    <td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; font-family:Arial,Helvetica,sans-serif;font-size:15px;line-height:20px;color:#555;">
                                      '.$title_link.'
                                      <br />
                                      <span style="font-family:\'Open Sans\',Arial,Helvetica,sans-serif !important;">' . $excerpt . '</span>
                                      <br /><br />
                                      <a href="'.wp_get_shortlink().'" class="button" style="color:'.$brand_color.';font-family: Arial, sans-serif;font-size:13px;line-height:19px;"><span style="font-family:\'Open Sans\',Arial,Helvetica,sans-serif !important;">Find out more</span></a>
                                    </td>
                                  </tr>
                                </table>
                                <table width="200" align="right" cellpadding="15" cellspacing="0" border="0">
                                  <tr>
                                    <td bgcolor="#eaeaea"><span>Share:</span></td>
                                    <td bgcolor="#eaeaea"><a target="_blank" href="http://twitter.com/intent/tweet?original_referer=&text='.get_the_title().'&url='.wp_get_shortlink().'"><img src="'.$twitter_share.'" width="32" height="32"></a></td>

                                    <td bgcolor="#eaeaea"><a href="https://www.facebook.com/sharer/sharer.php?u='.wp_get_shortlink().'" target="_blank"><img src="'.$facebook_share.'" width="32" height="32"></a></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td width="100%" height="20" bgcolor="#ffffff"><br /></td>
                            </tr>
                          </table>
                          <!-- end content 2 -->

                          <!-- ad & bullets -->
                          <table class="full_width" width="300" align="right" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                              <td width="100%" class="text-center">
                                '.$threeby250_ad.'
                              </td>
                            </tr>
                            <tr>
                              <td bgcolor="#ffffff" width="100%" valign="top">
                                <table width="100%" cellpadding="15" cellspacing="0" border="0" class="full_width">
                                  <tr>
                                    <td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; font-family:Arial,Helvetica,sans-serif;font-size:15px;line-height:20px;color:#555;">
                                      <b>ALSO IN THIS ISSUE:</b><br />
                                      '.$latest_news.'
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                          <!-- end ad & bullets -->
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="hide">
                      <tr>
                        <td width="100%" height="10" bgcolor="#ffffff"><br /></td>
                    </tr>
                  </table>
                  </td>
                </tr>
              </table>
            ';
          } elseif ($count == 2) {
            $image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'newsletter-image');
            $image_link = '<a href="'. wp_get_shortlink() .'" title="Read full story" style="text-decoration: none; color: '.$brand_color.';"><img src="' . $image_array[0] . '" class="max-size" width="300" height="200" /></a>';

            $stories .= '
              <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="font-family:Arial,Helvetica,sans-serif;font-size:15px;line-height:20px;color:#555;">
                <tr>
                  <td width="100%">
                    <table width="620" cellpadding="0" cellspacing="0" border="0" align="center" class="content_wrap">
                      <tr>
                        <td width="100%">
                          <!--Content 3 & ads-->
                          <!-- content 3 -->
                          <table class="full_width" width="300" align="left" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                              <td class="text-center">
                                '.$image_link.'
                              </td>
                            </tr>
                            <tr>
                              <td bgcolor="#eaeaea" width="100%" valign="top">
                                <table width="300" cellpadding="15" cellspacing="0" border="0" class="full_width">
                                  <tr>
                                    <td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; font-family:Arial,Helvetica,sans-serif;font-size:15px;line-height:20px;color:#555;">
                                      '.$title_link.'
                                      <br />
                                      <span style="font-family:\'Open Sans\',Arial,Helvetica,sans-serif !important;">' . $excerpt . '</span>
                                      <br /><br />
                                      <a href="'.wp_get_shortlink().'" class="button" style="color:'.$brand_color.';font-family: Arial, sans-serif;font-size:13px;line-height:19px;"><span style="font-family:\'Open Sans\',Arial,Helvetica,sans-serif !important;">Find out more</span></a>
                                    </td>
                                  </tr>
                                </table>
                                <table width="200" align="right" cellpadding="15" cellspacing="0" border="0">
                                  <tr>
                                    <td bgcolor="#eaeaea"><span>Share:</span></td>
                                    <td bgcolor="#eaeaea"><a target="_blank" href="http://twitter.com/intent/tweet?original_referer=&text='.get_the_title().'&url='.wp_get_shortlink().'"><img src="'.$twitter_share.'" width="32" height="32"></a></td>

                                    <td bgcolor="#eaeaea"><a href="https://www.facebook.com/sharer/sharer.php?u='.wp_get_shortlink().'" target="_blank"><img src="'.$facebook_share.'" width="32" height="32"></a></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td width="100%" height="20" bgcolor="#ffffff"><br /></td>
                            </tr>
                          </table>
                          <!-- end content 3 -->

                          <!-- ads -->
                          <table class="full_width" width="300" align="right" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                              <td width="100%" class="text-center">
                                '.$threeby250_ad2.'
                              </td>
                            </tr>
                            <tr>
                              <td width="100%" height="20" bgcolor="#ffffff"><br /></td>
                            </tr>
                          </table>
                          <!-- end ads -->
                          <!--end Content 3 & ads -->
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            ';
          } elseif ($count == 3 || $count == 4) {
            $image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'newsletter-image');
            $image_link = '<a href="'. wp_get_shortlink() .'" title="Read full story" style="text-decoration: none; color: '.$brand_color.';"><img src="' . $image_array[0] . '" class="max-size" width="300" height="200" /></a>';
            if ($count == 3) {
              $stories .= '
                <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="font-family:Arial,Helvetica,sans-serif;font-size:15px;line-height:20px;color:#555;">
                  <tr>
                    <td width="100%">
                      <table width="620" cellpadding="0" cellspacing="0" border="0" align="center" class="content_wrap">
                        <tr>
                          <td width="100%">
                            <!--Content 4 & 5-->
                            <!-- content 4 -->
                            <table class="full_width" width="300" align="left" cellpadding="0" cellspacing="0" border="0">
                              <tr>
                                <td width="100%" class="text-center">
                                  '.$image_link.'
                                </td>
                              </tr>
                              <tr>
                                <td bgcolor="#eaeaea" width="100%" valign="top">
                                  <table width="300" cellpadding="15" cellspacing="0" border="0" class="full_width">
                                    <tr>
                                      <td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; font-family:Arial,Helvetica,sans-serif;font-size:15px;line-height:20px;color:#555;">
                                        '.$title_link.'
                                        <br />
                                        <span style="font-family:\'Open Sans\',Arial,Helvetica,sans-serif !important;">' . $excerpt . '</span>
                                        <br /><br />
                                        <a href="'.wp_get_shortlink().'" class="button" style="color:'.$brand_color.';font-family: Arial, sans-serif;font-size:13px;line-height:19px;"><span style="font-family:\'Open Sans\',Arial,Helvetica,sans-serif !important;">Find out more</span></a>
                                      </td>
                                    </tr>
                                  </table>
                                  <table width="200" align="right" cellpadding="15" cellspacing="0" border="0">
                                    <tr>
                                      <td bgcolor="#eaeaea"><span>Share:</span></td>
                                      <td bgcolor="#eaeaea"><a target="_blank" href="http://twitter.com/intent/tweet?original_referer=&text='.get_the_title().'&url='.wp_get_shortlink().'"><img src="'.$twitter_share.'" width="32" height="32"></a></td>

                                      <td bgcolor="#eaeaea"><a href="https://www.facebook.com/sharer/sharer.php?u='.wp_get_shortlink().'" target="_blank"><img src="'.$facebook_share.'" width="32" height="32"></a></td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td width="100%" height="20" bgcolor="#ffffff"><br /></td>
                              </tr>
                            </table>
                            <!-- end content 4 -->
              ';
            } elseif ($count == 4) {
              $stories .= '
                    <!-- content 5 -->
                    <table class="full_width" width="300" align="right" cellpadding="0" cellspacing="0" border="0">
                      <tr>
                        <td width="100%" class="text-center">
                          '.$image_link.'
                          </td>
                        </tr>
                        <tr>
                        <td bgcolor="#eaeaea" width="100%" valign="top">
                          <table width="300" cellpadding="15" cellspacing="0" border="0" class="full_width">
                            <tr>
                              <td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; font-family:Arial,Helvetica,sans-serif;font-size:15px;line-height:20px;color:#555;">
                                '.$title_link.'
                                <br />
                                <span style="font-family:\'Open Sans\',Arial,Helvetica,sans-serif !important;">' . $excerpt . '</span>
                                <br /><br />
                                <a href="'.wp_get_shortlink().'" class="button" style="color:'.$brand_color.';font-family: Arial, sans-serif;font-size:13px;line-height:19px;"><span style="font-family:\'Open Sans\',Arial,Helvetica,sans-serif !important;">Find out more</span></a>
                              </td>
                            </tr>
                          </table>
                          <table width="200" align="right" cellpadding="15" cellspacing="0" border="0">
                            <tr>
                              <td bgcolor="#eaeaea"><span>Share:</span></td>
                              <td bgcolor="#eaeaea"><a target="_blank" href="http://twitter.com/intent/tweet?original_referer=&text='.get_the_title().'&url='.wp_get_shortlink().'"><img src="'.$twitter_share.'" width="32" height="32"></a></td>

                              <td bgcolor="#eaeaea"><a href="https://www.facebook.com/sharer/sharer.php?u='.wp_get_shortlink().'" target="_blank"><img src="'.$facebook_share.'" width="32" height="32"></a></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                    <!-- end content 5 -->
                  <!--end Content 4 & 5-->
                </td>
              </tr>
              <tr>
                <td width="100%" height="20"><br /></td>
              </tr>
            </table>
          ';
          }
        }
      }
    }
    endwhile; else:
      $stories .= '<tr><td>Sorry, no posts match that category</td></tr>';
    endif;
  }
  $stories .= '</table>';
	$wp_query  = NULL;
	$wp_query = $original_query;
	wp_reset_postdata();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
  <title><?php echo $newsletter_title; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--[if !mso 9]><!-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!--<![endif]-->

  <style>

    body {
      width:100%!important;
      -webkit-text-size-adjust:100%;
      -ms-text-size-adjust:100%;
      margin:0;
      padding:0;
    }
    .ExternalClass {
      width: 100%;
    }
    span {
      font-family:'Open Sans', Arial, sans-serif;
      line-height:20px;
	  font-weight: 400;

    }
    table, td {
      border-collapse:collapse;
      mso-table-lspace:0pt;
      mso-table-rspace:0pt;
    }
    img{
        border:0;
        line-height:100%;
        outline:none;
        text-decoration:none;
    }
    img.max-size {
      height:auto!important;
    }
    img#header-logo {
    	margin-right: 0px;
    }
    img#sponsor-logo {
    	padding: 2px 0px;
    	margin: 2px 0px;
    }
    @media only screen and (max-width: 480px) {
      body,table,td,p,a,li,blockquote{
        -webkit-text-size-adjust:none !important;
      }
    }
    @media only screen and (max-width: 480px) {
      table[class=content_wrap] {
        width: 94%!important;
      }
    }
    @media only screen and (max-width: 480px) {
      table[class=full_width] {
        width: 100%!important;
      }
    }
    @media only screen and (max-width: 480px) {
      img[class=max-size] {
        width: 100%!important;
        height:auto;
      }
    }
    @media only screen and (max-width: 480px) {
      img[class=hide] {
        display: none!important;
      }
    }
    @media only screen and (max-width: 480px) {
      td[class=hide] {
        display: none!important;
      }
    }
    @media only screen and (max-width: 480px) {
      table[class=show] {
        display:inline!important;
      }
    }
    @media only screen and (max-width: 480px) {
      img[class=show] {
        display:inline!important;
      }
    }
    @media only screen and (max-width: 480px) {
      td[class=text-center] {
        text-align: center!important;
      }
    }
    @media only screen and (max-width: 480px) {
      td[class=text-left] {
        text-align: left!important;
      }
    }
    @media only screen and (max-width: 480px) {
      a[class=button] {
        border-radius:2px;
        background-color:<?php echo $brand_color; ?>;
        color:#fff!important;
        padding: 5px;
        text-align:center;
        display:block;
        text-decoration: none;
        text-transform: uppercase;
        margin: 0 0 10px 0;
      }
    }
    @media only screen and (max-width: 599px) {
      table[id=li-container]{ width: 100% !important; }
      div[class=fullw]{ width: 50% !important; }
      img[class=orgw]{ width: 95% !important; }
      td[class=adcontent]{ padding: 5px 0 5px 10px !important; }
      td[class=adchoices]{ padding: 0 10px 5px 0 !important; }
    }
  </style>
</head>
<body bgcolor="#ffffff" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="-webkit-font-smoothing:antialiased;width:100% !important;background-color:#ffffff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;-webkit-text-size-adjust:none;">
  <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">

    <tr>
      <td width="100%">

        <!--Head banner-->
        <!--Content wrapper-->
        <table width="620" cellpadding="0" cellspacing="0" border="0" align="center" class="content_wrap">
        <tr>
            <td width="100%" height="20" bgcolor="#ffffff"><br /></td>
          </tr>
              <tr>
            <td width="100%" bgcolor="#ffffff">
              <!-- header -->
              <table align="left" width="500" class="full_width" cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <!--Logo-->
                  <td bgcolor="#ffffff" width="453" class="text-center">
                    <a href="<?php echo site_url(); ?>"><img id="header-logo" src="<?php echo $header_image;?>" width="210" height="75" title="<?php echo get_bloginfo('name'); ?> Newsletter" alt="<?php echo get_bloginfo('name'); ?> Newsletter" border="0" /></a>
                    <?php
                  if(isset($newsletter_sponsor_image) AND $newsletter_sponsor_image != ''){
                  	if(isset($newsletter_sponsor_link) AND $newsletter_sponsor_link != ''){
                  		echo "<a href='".$newsletter_sponsor_link."' target='_blank'>";
                  	}
                  	echo "<img id='sponsor-logo' src='".$newsletter_sponsor_image."' width='175' height='40' border='0' />";
                  	if(isset($newsletter_sponsor_link) AND $newsletter_sponsor_link != ''){
                  		echo "</a>";
                  	}
                  }
                  ?>
                </td>
              </tr>
              </table>
              <table class="full_width" width="100" height="44" cellpadding="0" cellspacing="0" border="0" align="right">
                <tr>
                  <td align="right" width="20" class="text-left">
                  </td>
                  <td width="8"></td>
                  <td width="32">
                    <?php echo $twitter; ?>
                  </td>
                  <td width="8"></td>
                  <td width="32">
                    <?php echo $facebook; ?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td>
              <table width="100%" cellpadding="0" cellspacing="0" border="0" class="hide">
                <tr>
                  <td width="100%" height="10" bgcolor="#ffffff"><br /></td>
              </tr>
            </table>
            </td>
          </tr>
          <tr>
            <td bgcolor="#ffffff">
              <table width="100%" cellpadding="0" cellspacing="0" border="0" class="hide">
                <tr>
                  <!--ad-->
                  <td bgcolor="#ffffff" width="100%">
                    <?php echo $leaderboard_ad; ?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <!-- spacer -->
          <tr>
            <td width="100%" height="10" bgcolor="#ffffff"><br /></td>
          </tr>
          <!-- / header -->
          <?php echo $stories; ?>
          <tr>
            <td bgcolor="#ffffff">
              <table width="100%" cellpadding="0" cellspacing="0" border="0" class="hide">
                <tr>
                  <!--ad-->
                  <td bgcolor="#ffffff" width="100%">
                    <?php echo $leaderboard_ad_bottom; ?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td bgcolor="#ffffff">
              <table width="100%" cellpadding="0" cellspacing="0" border="0" class="hide">
                <tr>
                  <!--ad-->
                  <td bgcolor="#ffffff" width="100%">
                    <?php echo $native_ad; ?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
      </td>
    </tr>
  </table>
  <?php echo $safe_rtb; ?>
</body>
</html>
