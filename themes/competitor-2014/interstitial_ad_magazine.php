<?php
	/*
	 * Template Name: Interstitial Ad Magazine
	 */
	$technical_theme_options	= get_option( 'cgi_media_technical_options' );
?>
<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="robots" content="noindex">
		<title>Advertisement</title>
		<style>
			body {
				overflow: hidden;
			}
			#interstitial-ad {
				height: 215px;
			}
		</style>
	</head>
	<body>

		<div id="interstitial-ad">
			<a target="_blank" href="<?php echo $technical_theme_options['magazine_interstitial_link'];?>">
				<img src="<?php echo $technical_theme_options['magazine_interstitial_image'];?>" />
			</a>
		</div>

	</body>
</html>
