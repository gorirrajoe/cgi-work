<?php
	/**
	 * Template Name: Cache Flush
	 */

	if ( defined( 'ABSPATH' ) ) {
		require_once( ABSPATH .'/wp-load.php');
	} else {
		require_once( $_SERVER['DOCUMENT_ROOT'] .'/wp-load.php' );
	}

	$cache_delete	= wp_cache_delete( 'alloptions', 'options' );
	$cache_flush	= wp_cache_flush();
?>
<!doctype html>
<html lang="en">
	<head>
		<title>Cache Page</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="robots" content="noindex">
		<meta name="googlebot" content="noindex">
	</head>
	<body>

		<?php
			if ( isset( $_GET['velociraptor'] ) ) {

				if ( !$cache_delete ) {
					echo '<p>wp_cache_delete function returned false.</p>';
				} else {
					echo '<p>wp_cache_delete function returned true. Cache Deleted.</p>';
				}

				if ( !$cache_flush ) {
					echo '<p>wp_cache_flush function returned false.</p>';
				} else {
					echo '<p>wp_cache_flush function returned true. Cache Flushed.</p>';
				}

				if ( !empty( $_GET['transient'] ) ) {
					delete_transient( $_GET['transient'] );
				}

			} else {
				echo '<img src="https://media.giphy.com/media/uOAXDA7ZeJJzW/giphy.gif" alt="You didn\'t say the magic word">';
			}
		?>

	</body>
</html>
