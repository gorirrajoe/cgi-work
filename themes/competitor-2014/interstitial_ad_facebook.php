<?php
	/**
	 * Template Name: Interstitial Ad Facebook
	 */
	$technical_theme_options	= get_option( 'cgi_media_technical_options' );
	$facebook_appid				= !empty( $technical_theme_options['facebook_appid'] ) ? $technical_theme_options['facebook_appid'] : '';
	$facebook_page				= !empty( $technical_theme_options['site_facebook_page'] ) ? $technical_theme_options['site_facebook_page'] : '';
?>
<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="robots" content="noindex">
		<title>Advertisement</title>
		<style>
			body {
				overflow: hidden;
			}
			#interstitial-ad {
				height: 215px;
			}
			.fb-like.fb_iframe_widget {
				display: inline-block;
				left: 145px;
				position: absolute !important;
				top: 180px;
			}
		</style>
		<div id="fb-root"></div>
		<script>
			(function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo $facebook_appid; ?>";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>
	</head>
	<body>
		<div id="interstitial-ad">
		<img src="<?php echo get_bloginfo('stylesheet_directory') . '/images/facebook-interstitial.jpg'; ?>" />
		<div class="fb-like" data-href="https://www.facebook.com/<?php echo $facebook_page; ?>" data-width="292" data-layout="button_count" data-show-faces="true" data-send="false"></div>
	</body>
</html>
