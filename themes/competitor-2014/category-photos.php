<?php
	/* PHOTOS */
	$technical_theme_options = get_option( 'cgi_media_technical_options' );
	get_header();
	$page = (get_query_var('paged')) ? get_query_var('paged') : 1;

	$count = 1;
	$row_counter = 1;
?>
	<div class="special_category">
		<?php
			echo '<h1><img class="slashes slash_blue" src="'.get_bloginfo('stylesheet_directory').'/images/running/running-slashes-blue.svg"> All Photo Galleries: Page ' . $page . '</h1>';
			if ( have_posts() ) : while ( have_posts() ) : the_post();
				if ( has_post_thumbnail() ) {
					$post_thumb = get_the_post_thumbnail(get_the_ID(), 'square-big-thumbs');
					$post_classes = get_post_class('post-thumbnail');
				}
				$post_classes_string = implode(" ", $post_classes);

				if ( $count < 4 ) {  // first row will have 3 big images
					if($count == 1) {
						echo '<div class="row">';
					}
					echo '<div class="col_'.$count.' three_col_full '.$post_classes_string.'">
							<div class="featured_img">
								<a href="'.get_permalink().'" class="overlay"></a>
								'.$post_thumb.'
							</div>
							<div class="more_link white_bg module">
								<a href="'.get_permalink().'">'.get_short_title() .' </a><a href="'.get_permalink().'"><span class="icon-right-open"></span></a>
							</div>
						</div>';
					if($count == 3) {
						echo '</div>';
					}
				} else { // rest of rows
					if($count == 4 || $count == 8 || $count == 12 || $count == 16) {
						echo '<div class="row">';
						$row_counter = 1;
					}
					echo '<div class="col_'.$row_counter.' four_col '.$post_classes_string.'">
							<div class="featured_img">
								<a href="'.get_permalink().'" class="overlay"></a>
								'.$post_thumb.'
							</div>
							<div class="more_link white_bg module">
								<a href="'.get_permalink().'">'.get_short_title() .' </a><a href="'.get_permalink().'"><span class="icon-right-open"></span></a>
							</div>
						</div>';
					if($count == 7 || $count == 11 || $count == 15 || $count == 19) {
						echo '</div>';
					}
				}
				$count++;
				$row_counter++;
			endwhile; endif;
			echo '<div class="row archive_navi">
				<div class="btn_primary">
					'.get_previous_posts_link('<span class="icon-left-open"></span> Previous Page').'
				</div>
				<div class="btn_primary">
					'.get_next_posts_link('Next Page <span class="icon-right-open"></span>').'
				</div>
			</div>';

		?>
	</div>

<?php get_footer(); ?>
