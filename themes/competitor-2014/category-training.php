<?php
	/* TRAINING
	custom-category.php will handle displaying sponsored category pages
	use variables below to set custom options
	copy and place file into child theme with corresponding cat id in name - category-###.php */

	define ("category_template", "custom");

	// video player info from brightcove
	$playlist_id	= '3708066475001';
	$video_player	= '3708607968001';
	$player_id		= '3696482079001';
	$player_key		= 'AQ~~,AAADUxzyV0k~,28cyIIHd3ssqvhtcPwlbYiAYQUS1HpVW';

	if ( !isset( $technical_theme_options ) || empty( $technical_theme_options ) ){
		$technical_theme_options = get_option( 'cgi_media_technical_options' );
	}

	get_header();
	global $post_id_array;

	$post_id_array	= array();
	$this_category	= get_category($cat);
	$is_mobile		= is_mobile();

	if (!(get_query_var('paged') > 1)) { ?>
		<div class="special_category">
			<div class="row">
				<div class="category_left_column">

					<div class="blue_bg">
						<img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/training-category-logo.png">
					</div>

					<div class="category_hdr">
						<h1 class="category_title">Training Center</h1>

						<?php wp_nav_menu( array('menu' => 'training-nav', 'container' => false, 'fallback_cb' => 'false')); ?>

					</div>

					<?php if ( is_active_sidebar('special-category-sponsor') ) dynamic_sidebar('special-category-sponsor'); ?>

				</div>

				<div class="category_right_column">
					<div class="row category_first_row cat_row">
						<div class="cat_odd cat_boxes">

							<h3><img class="slashes slash_white_lg" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-white-lg.svg"> Training Plans</h3>
							<ul>
								<li><a href="<?php echo site_url(); ?>/trainingplanmarathon" target="_blank"><img class="navicons" src="<?php echo bloginfo('stylesheet_directory'); ?>/images/running/category-training-full-marathon.png">Marathon </a><a href="<?php echo site_url(); ?>/trainingplanmarathon" target="_blank"><span class="icon-right-open"></span></a></li>
								<li><a href="<?php echo site_url(); ?>/trainingplanhalfmarathon" target="_blank"><img class="navicons" src="<?php echo bloginfo('stylesheet_directory'); ?>/images/running/category-training-half-marathon.png">Half Marathon </a><a href="<?php echo site_url(); ?>/trainingplanhalfmarathon" target="_blank"><span class="icon-right-open"></span></a></li>
								<li><a href="<?php echo site_url(); ?>/trainingplan5k" target="_blank"><img class="navicons" src="<?php echo bloginfo('stylesheet_directory'); ?>/images/running/category-training-5k.png">5K </a><a href="<?php echo site_url(); ?>/trainingplan5k" target="_blank"><span class="icon-right-open"></span></a></li>
							</ul>

						</div>

						<div class="cat_even cat_boxes">
							<h3><img class="slashes slash_white_lg" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-white-lg.svg"> Toolbox</h3>
							<ul>
								<li><a href="<?php echo site_url(); ?>/pace-calculator"><img class="navicons" src="<?php echo bloginfo('stylesheet_directory'); ?>/images/running/category-training-pace.png">Pace Calculator </a><a href="<?php echo site_url(); ?>/pace-calculator"><span class="icon-right-open"></span></a></li>
								<li><a href="<?php echo site_url(); ?>/pace-calculator"><img class="navicons" src="<?php echo bloginfo('stylesheet_directory'); ?>/images/running/category-training-intensity.png">Intensity Calculator </a><a href="<?php echo site_url(); ?>/pace-calculator"><span class="icon-right-open"></span></a></li>
								<li><a href="<?php echo site_url(); ?>/runroutes"><img class="navicons" src="<?php echo bloginfo('stylesheet_directory'); ?>/images/running/category-training-route.png">Route Finder </a><a href="<?php echo site_url(); ?>/runroutes"><span class="icon-right-open"></span></a></li>
							</ul>
						</div>

					</div>
					<div class="row category_second_row cat_row">
						<div class="cat_odd cat_boxes">
							<h3><img class="slashes slash_white_lg" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-white-lg.svg"> Workouts</h3>
							<ul>
								<li><a href="<?php echo site_url(); ?>/category/workout-of-the-week"><img class="navicons" src="<?php echo bloginfo('stylesheet_directory'); ?>/images/running/category-training-workout.png">Workout of the Week </a><a href="<?php echo site_url(); ?>/category/workout-of-the-week"><span class="icon-right-open"></span></a></li>
								<li><a href="<?php echo site_url(); ?>/tag/cross-training-for-runners"><img class="navicons" src="<?php echo bloginfo('stylesheet_directory'); ?>/images/running/category-training-cross-training.png">Cross Training </a><a href="<?php echo site_url(); ?>/tag/cross-training-for-runners"><span class="icon-right-open"></span></a></li>
								<li><a href="<?php echo site_url(); ?>/tag/strength-training"><img class="navicons" src="<?php echo bloginfo('stylesheet_directory'); ?>/images/running/category-training-strength.png">Strength Training </a><a href="<?php echo site_url(); ?>/tag/strength-training"><span class="icon-right-open"></span></a></li>
							</ul>
						</div>
						<div class="cat_even cat_boxes cat_recent">
							<h3><img class="slashes slash_white_lg" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-white-lg.svg"> Recent</h3>

							<?php get_special_recent_category_posts($this_category->term_id, 2); ?>

						</div>
					</div>
				</div>
			</div><!-- first group -->

			<div class="row">
				<div class="left_column">
					<h3><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> Competitor On Demand</h3>

					<?php //get_youtube_playlist('PLEAB9DA2F6D9E4776'); ?>

					<?php get_brightcove_playlist($player_id, $player_key, $playlist_id, $video_player, $technical_theme_options['pre_roll']);
					?>

					<h3><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> Photos</h3>

					<?php get_photos_category($this_category->term_id); ?>

				</div>
				<div class="sidebar">
					<div class="hypnotoad_unit hypnotoad_300x250">
						<?php
							if(!$is_mobile){
								echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('side-top') : '';
							}
						?>
					</div><!-- ad unit -->

					<div class="module offers">
						<h3><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> Running Resources</h3>
						<ul id="sidebar_offers_carousel">
							<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('marquee') ) : else : endif; ?>
						</ul>
					</div><!-- offers carousel -->

					<div class="hypnotoad_unit hypnotoad_300x600">
						<?php
							if(!$is_mobile){
								echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('side-middle') : '';
							}
						?>
					</div><!-- ad unit -->

				</div>
			</div>
		</div>

	<?php } else {
		$page = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>

		<div class="left_column">
			<div>
				<?php
					$page = (get_query_var('paged')) ? get_query_var('paged') : 1;
					echo '<h1><img class="slashes slash_blue" src="'.get_bloginfo('stylesheet_directory').'/images/running/running-slashes-blue.svg"> All Posts Categorized &#8216;' . single_cat_title('', false) . '&#8217;: Page ' . $page . '</h1>';
					$count = 1;

					if ( have_posts() ) : while ( have_posts() ) : the_post();
						$permalink		= get_permalink();
						$title			= get_the_title();
						$excerpt		= get_the_excerpt();
						$post_classes	= get_post_class();

						if (has_post_thumbnail ()) {
							if($is_mobile){
								$image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'marquee-carousel-mobile');
							} else {
								if($count == 1) {
									$image_array = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'single-post-big');
								} else {
									$image_array = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail');
								}
							}
							$post_thumb = $image_array[0];
						} else {
							if($count == 1) {
								$post_thumb = get_bloginfo('stylesheet_directory').'/images/running/default-post-thumbnail-lg.jpg';
							} else {
								$post_thumb = get_bloginfo('stylesheet_directory').'/images/running/default-post-thumbnail-sm.jpg';
							}
						}
						$post_classes_string	= implode(" ", $post_classes);

						$month_published		= get_the_time('M');
						$day_published			= get_the_time('d');

						if ($count == 1 && (get_query_var('paged') < 3)) {  // first post is a big one
							echo '
								<div class="featured_post '.$post_classes_string.'">
									<div class="featured_img">
										<a class="overlay" href="'.$permalink.'"></a>
										<img src="'.$post_thumb.'">
									</div>
									<div class="row featured_vitals">
										<div class="featured_post_date">
											<span class="featured_post_month">'.$month_published.'</span>
											<span class="featured_post_day">'.$day_published.'</span>
										</div>
										<div class="featured_title_author">
											<h3><a href="'.$permalink.'">'.$title.'</a></h3>
											<p class="post_author">by '.get_guest_author().'</p>
											<p class="excerpt">'.$excerpt.'</p>
										</div>
										<a class="arrow" href="'.$permalink.'"><span class="icon-right-open"></span></a>
									</div>
								</div>
							';
						} else {
							$thumb = '<div class="featured_img_archive"><a class="overlay" href="' . $permalink . '" title="' . $title . '"></a>' . '<img class="archive_thumb" src="'.$post_thumb.'"></div>';
							echo '
								<div class="row archive_row '.$post_classes_string.'">
									'.$thumb.'
									<h4><a href="'.$permalink.'">'.$title.'</a></h4>
									<p class="post_author">by '.get_guest_author().', '.format_timestamp_2().'</p>
									<p class="excerpt">'.$excerpt.'</p>
								</div>
							';
						}

						$count++;

					endwhile; else: ?>
						<div class="content">
							<p>Sorry, no posts matched your criteria.</p>
						</div>
					<?php endif;
				?>

			</div>

			<?php
				echo '<div class="row archive_navi">
					<div class="btn_primary">
						'.get_previous_posts_link('<span class="icon-left-open"></span> Previous Page').'
					</div>
					<div class="btn_primary">
						'.get_next_posts_link('Next Page <span class="icon-right-open"></span>').'
					</div>
				</div>';
			?>
		</div><!-- row 1/content -->

		<?php get_sidebar();
	}
get_footer(); ?>
