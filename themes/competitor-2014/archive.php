<?php
	// archive.php will handle displaying category, tag, author, date and taxonomy pages
	$technical_theme_options	= get_option( 'cgi_media_technical_options' );
	$editorial_theme_options	= get_option( 'cgi_media_editorial_options' );

	get_header();

	$is_mobile				= is_mobile();
	$training_plans_txt		= $editorial_theme_options['training_plans_txt'];
	$training_plans_link	= $editorial_theme_options['training_plans_link'];

	$get_training_plans		= get_training_plans($post->ID); ?>

		<div class="left_column">
			<div>
				<?php
					$page			= (get_query_var('paged')) ? get_query_var('paged') : 1;
					//PARTNER CONNECT ARCHIVE TITLE
					$obtain_slug	= get_category_by_slug('sponsored') !== false ? get_category_by_slug('sponsored') : get_category_by_slug('partnerconnect'); // - Get Category ID from the slug
					$pc_catid		=  $obtain_slug->term_id;

					if (is_author()) {
						// Lets output an Author Header rather than Category Header
						$author = get_userdata(get_query_var('author'));

						echo '<h1><img class="slashes slash_blue" src="'.get_bloginfo('stylesheet_directory').'/images/running/running-slashes-blue.svg"> Articles by ' . $author->display_name . '</h1>

						<div class="postmeta author_meta">';
							$author_image			= get_template_directory() . '/images/author/'.get_the_author_meta('ID').'.jpg';
							$author_image_display	= get_template_directory_uri() . '/images/author/'.get_the_author_meta('ID').'.jpg';

							if(file_exists($author_image)){
								echo "<img class=\"avatar avatar-120 photo\" src=\"".$author_image_display."\">";
							} else {
								echo get_avatar($author->user_email, 120, '', $author->display_name);
							}

							//echo get_avatar(get_the_author_meta('ID'), 120, '', $author_name);

							echo '<p class="author_bio">' . get_the_author_meta('description') . '</p>';

							$twitter	= trim(get_the_author_meta('twitter'));
							$facebook	= trim(get_the_author_meta('facebook'));
							$googleplus	= trim(get_the_author_meta('googleplus'));

							echo '<ul class="author_social">';
								if ($twitter != '') {
									echo '<li><a class="custom-twitter-button" href="https://twitter.com/' . $twitter . '" title="Follow @' . $twitter . '" target="_blank"><span class="social_button"></span></a></li>';
								}
								if ($facebook != '') {
									echo '<li><a class="custom-facebook-button" href="http://facebook.com/' . $facebook . '" title="Subscribe on Facebook" target="_blank"><span class="social_button"></span></a></li>';
								}
								if ($googleplus != '') {
									echo '<li><a class="custom-gplus-button" href="'.$googleplus.'?rel=author" target="_blank" title="Follow on Google+"><span class="social_button"></span></a></li>';
								}
								echo '<li><a class="author-button" rel="author" href="' . get_author_posts_url(get_the_author_meta('ID')) . '" title="Read more by ' . $author_name . '">All articles by ' . get_the_author_meta('first_name') . '</a></li>
							</ul>
						</div>';
					} else if (is_category()) {
						$single_cat_title = is_category( $pc_catid ) ? '<span class="sponsored-label">Sponsored Content</span>' : single_cat_title('', false);
						echo '<h1><img class="slashes slash_blue" src="'.get_bloginfo('stylesheet_directory').'/images/running/running-slashes-blue.svg"> ' . $single_cat_title . ': Page ' . $page . '</h1>';
					} else if (is_tag()) {
						echo '<h1><img class="slashes slash_blue" src="'.get_bloginfo('stylesheet_directory').'/images/running/running-slashes-blue.svg"> ' . single_tag_title('', false) . ' Tag: Page ' . $page . '</h1>';
					} else if(is_date()) {
						echo '<h1><img class="slashes slash_blue" src="'.get_bloginfo('stylesheet_directory').'/images/running/running-slashes-blue.svg"> All Posts For &#8216;' . get_the_time('F Y') . '&#8217;: Page ' . $page . '</h1>';
					}

					$count = 1;

					if ( have_posts() ) : while ( have_posts() ) : the_post();
						$partnerconnect		= get_partner_connect($post->ID);
						$permalink			= get_permalink();
						$title				= get_the_title();
						$excerpt			= get_the_excerpt();
						$post_classes		= get_post_class();
						$sponsored_label	= in_category( $pc_catid ) ? '<p class="sponsored-label">Sponsored Content</p>' : '';

						if (has_post_thumbnail ()) {
							if($is_mobile){
								$image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'marquee-carousel-mobile');
							} else {
								if($count == 1) {
									$image_array = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'single-post-big');
								} else {
									$image_array = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail');
								}
							}
							$post_thumb = $image_array[0];
						} else {
							if($count == 1) {
								$post_thumb = get_bloginfo('stylesheet_directory').'/images/running/default-post-thumbnail-lg.jpg';
							} else {
								$post_thumb = get_bloginfo('stylesheet_directory').'/images/running/default-post-thumbnail-sm.jpg';
							}
						}
						$post_classes_string	= implode(" ", $post_classes);

						$month_published		= get_the_time('M');
						$day_published			= get_the_time('d');

						if ($count == 1 && (get_query_var('paged') < 2)) {  // first post is a big one
							echo '
								<div class="featured_post '.$post_classes_string.'">
									<div class="featured_img">
										<a class="overlay" href="'.$permalink.'"></a>
										<img src="'.$post_thumb.'">
									</div>
									<div class="row featured_vitals">
										<div class="featured_post_date">
											<span class="featured_post_month">'.$month_published.'</span>
											<span class="featured_post_day">'.$day_published.'</span>
										</div>
										<div class="featured_title_author">
											<h3><a href="'.$permalink.'">'.$title.'</a></h3>
											<p class="post_author">by '.get_guest_author().'</p>
											<p class="excerpt">'.$excerpt.'</p>'.
											$sponsored_label.
										'</div>
										<a class="arrow" href="'.$permalink.'"><span class="icon-right-open"></span></a>
									</div>
								</div>
							';
						} else {
							$thumb = '<div class="featured_img_archive"><a class="overlay" href="' . $permalink . '" title="' . $title . '"></a>' . '<img class="archive_thumb" src="'.$post_thumb.'"></div>';
							echo '
								<div class="row archive_row '.$post_classes_string.'">
									'.$thumb.'
									<h4><a href="'.$permalink.'">'.$title.'</a></h4>
									<p class="post_author">by '.get_guest_author().', '.format_timestamp_2().'</p>
									<p class="excerpt">'.$excerpt.'</p>'.
									$sponsored_label.
								'</div>
							';
						}

						$count++;

					endwhile; else: ?>
						<div class="content">
							<p>Sorry, no posts matched your criteria.</p>
						</div>
					<?php endif;
				?>

			</div>

			<?php
				echo '<div class="row archive_navi">
					<div class="btn_primary">
						'.get_previous_posts_link('<span class="icon-left-open"></span> Previous Page').'
					</div>
					<div class="btn_primary">
						'.get_next_posts_link('Next Page <span class="icon-right-open"></span>').'
					</div>
				</div>';
			?>

		</div><!-- row 1/content -->

		<?php // Start Training Plans

		echo $get_training_plans;

	get_sidebar();

get_footer(); ?>
