<?php
	/* Template Name: Search Page */
	get_header();
	$technical_theme_options	= get_option( 'cgi_media_technical_options' );
?>

<div class="left_column">
	<div <?php post_class(); ?>>

		<h1><img class="slashes slash_blue" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/images/running/running-slashes-blue.svg"> Search <?php echo bloginfo( 'name' ); ?></h1>

		<div class="content">

			<form method="get" action="<?php echo site_url(); ?>/search">

				<?php if ( isset( $_POST['searchText'] ) ) {
					$searchvalue = $_POST['searchText'];
				} else if( isset( $_REQUEST ['q'] ) ) {
					$searchvalue = $_REQUEST ['q'];
				} else {
					$searchvalue = '';
				} ?>

				<input type="text" id="searchText" name="q" value="<?php echo htmlentities( stripslashes( strip_tags( $searchvalue ) ), ENT_HTML5, 'UTF-8' ); ?>">
				<input type="hidden" name="searchpage" value="1">
				<input type="hidden" name="type" value="web">
				<button type="submit" value="Search" id="searchButton">Search</button>

				<?php // defaults
					$search_text			= '';
					$page_number			= 1;
					$offset					= 0;
					$type					= 'web';
					$results_per_page_web	= 10;
					$results_per_page_image	= 18;

					if ( isset( $_REQUEST['searchpage'] ) ) {
						$page_number	= htmlentities( $_REQUEST['searchpage'] );
						$type			= $_REQUEST['type'];

						if( $type == "web" ) {
							$offset = ( ( $page_number - 1 ) * $results_per_page_web );
						} elseif( $type == "image" ) {
							$offset = ( ( $page_number - 1 ) * $results_per_page_image );
						}
					}
					if ( isset( $_POST['searchText'] ) ) {
						$search_text = htmlentities( stripslashes( strip_tags( $_POST['searchText'] ) ), ENT_QUOTES | ENT_HTML5, 'UTF-8' );
					} elseif( isset( $_REQUEST ['q'] ) ) {
						$search_text = htmlentities( stripslashes( strip_tags( $_REQUEST ['q'] ) ), ENT_QUOTES | ENT_HTML5, 'UTF-8' );
					}

					if ( $search_text != '' ) {

						$accountKeyWeb	= $technical_theme_options['bing_web_search_id_v5'];
						$accountKeyImg	= $technical_theme_options['bing_img_search_id_v5'];

						$ServiceRootURL	= 'https://api.cognitive.microsoft.com/bing/v5.0/';
						$WebSearchURL	= $ServiceRootURL . 'search?count='. $results_per_page_web .'&offset='. $offset .'&q=';
						$ImageSearchURL	= $ServiceRootURL . 'images/search?count='. $results_per_page_image .'&offset='. $offset .'&q=';

						$url	= site_url();
						$url	= str_replace( "cgitesting.lan", "competitor.com", $url );
						$url	= str_replace( "competitor.lan", "competitor.com", $url );
						$url	= str_replace( "competitor.loc", "competitor.com", $url );
						$url	= str_replace( "running.loc", "competitor.com", $url );

						$search_query	= new WP_Query();
						$search_posts	= $search_query->query( 's='.$search_text .'&post_type=shoe' );


						if( $search_query->have_posts() ) {

							// show shoes first
							echo '<div class="search_shoe_directory">
								<h2>Shoe Directory</h2>';

								while( $search_query->have_posts() ) : $search_query->the_post();

									$title			= get_the_title();
									// $thumb		= get_the_post_thumbnail( get_the_ID(), 'thumbnail' );
									$msrp			= get_post_meta( $post->ID, '_msrp', true );
									$description	= get_post_meta( $post->ID, '_description', true );
									$permalink		= get_permalink();
									$post_classes	= get_post_class();

									if ( has_post_thumbnail () ) {

										if( $is_mobile ) {
											$image_array = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'marquee-carousel-mobile' );
										} else {
											$image_array = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'thumbnail' );
										}

										$thumb = '<div class="featured_img_archive"><a class="overlay" href="' . $permalink . '" title="' . $title . '"></a>' . '<img class="archive_thumb" src="'.$image_array[0].'"></div>';

									} else {
										$thumb = '';
									}

									$post_classes_string = implode( " ", $post_classes );

									echo '<div class="row archive_row '. $post_classes_string .'">'.
										$thumb .'
										<h4><a href="'. $permalink .'">'. $title .'</a></h4>
										<p>
											<strong>MSRP: </strong>' . $msrp . '<br>
											<strong>Description: </strong>' . $description . '<br>
										</p>
									</div>';

								endwhile;

							echo '</div>';

							$search_query			= null;
							$another_search_query	= null;
							wp_reset_postdata();

							echo '<div class="row archive_navi">
								<div class="btn_primary">
									<a href="'. site_url() .'/shoe/">Browse Shoe Directory</a>
								</div>
							</div>';
						}


						if( $type == "web" ) {  // search web content

							$context = stream_context_create( array(
								'http' => array(
									'method'	=> 'GET',
									'header'	=> 'Ocp-Apim-Subscription-Key: ' . $accountKeyWeb
								)
							) );

							echo '<ul class="search-tabs">
								<li class="ar-tab active">
									<a href="'. site_url() .'/search?type=web&searchpage=1&q='. $search_text .'">Article Results</a>
									<span class="tab_indicator"></span>
								</li>
								<li class="ir-tab">
									<a href="'. get_bloginfo( 'stylesheet_directory' ) .'/search?type=image&searchpage=1&q='. $search_text .'">Image Results</a>
									<span class="tab_indicator"></span>
								</li>
							</ul>

							<div class="web_results">';
								$request	= $WebSearchURL . urlencode( $search_text . ' site:'. $url );
								$request	= str_replace( '%26period%3B', '%2E', $request );
								$request	= str_replace( '%26apos%3B', '%27', $request );
								$response	= file_get_contents( $request, 0, $context );
								$jsonobj	= json_decode( $response );

								if( array_key_exists( 'webPages', $jsonobj ) && $jsonobj->webPages->totalEstimatedMatches > 0 ) {

									$valueObj = $jsonobj->webPages->value;

									foreach( $valueObj as $value ) {

										$bing_url		= explode( '&p=', $value->url );
										$url_array		= explode( "_", $bing_url[0] );
										$array_count	= count( $url_array );
										$post_id		= $url_array[$array_count - 1];
										$post_id_array	= explode( "/", $post_id );
										$post_id		= $post_id_array[0];
										$thumb			= '';

										if ( is_numeric( $post_id ) ) {
											$image_array	= wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'thumbnail' );
											$thumb			= '<div class="featured_img_archive"><a class="overlay" href="'. $value->url .'" title="'. $value->name .'"></a><img class="archive_thumb" src="'. $image_array[0] .'"></div>';
										}

										echo '<div class="row archive_row">'.
											$thumb .'
											<h4><a href="'. $value->url .'">'. $value->name .'</a></h4>
											<p class="excerpt">'. stripslashes( $value->snippet ) .'</p>
										</div>';
									}

								}  else {

									echo '<p>No web results found.</p>';

								}

							echo '</div>';

						} else {  // search image content

							$context = stream_context_create( array(
								'http' => array(
									'method'	=> 'GET',
									'header'	=> 'Ocp-Apim-Subscription-Key: ' . $accountKeyImg
								)
							) );

							echo '<ul class="search-tabs">
								<li class="ar-tab">
									<a href="'. site_url() .'/search?type=web&searchpage=1&q='. $search_text .'">Article Results</a>
									<span class="tab_indicator"></span>
								</li>
								<li class="ir-tab active">
									<a href="'. site_url() .'/search?type=image&searchpage=1&q='. $search_text .'">Image Results</a>
									<span class="tab_indicator"></span>
								</li>
							</ul>';


							$request	= $ImageSearchURL . urlencode( $search_text . ' site:'. $url );
							$request	= str_replace( '%26period%3B', '%2E', $request );
							$request	= str_replace( '%26apos%3B', '%27', $request );
							$response	= file_get_contents( $request, 0, $context );
							$jsonobj	= json_decode( $response );

							$count3 = 1;

							if( array_key_exists( 'totalEstimatedMatches', $jsonobj ) && $jsonobj->totalEstimatedMatches > 0 ) {

								$valueObj = $jsonobj->value;

								echo '<div class="grid_view">
									<ul class="images_search_result">';

										foreach( $valueObj as $value ) {

											if( ( $count3 % 3 ) == 1 ) {
												echo '<div class="row images_search_result_row">';
											}

											echo '<li>
												<div class="featured_img">
													<a href="'. $value->hostPageUrl .'"><img src="'. $value->thumbnailUrl .'"/></a>
												</div>
												<p class="image_search_title"><a href="'. $value->hostPageUrl .'">'. $value->name .'</a></p>
											</li>';

											if( ( $count3 % 3 ) == 0 ) {
												echo '</div>';
											}

											$count3++;

										}

									echo '</ul>
								</div>';

							}  else {

								echo '<p>No image results found.</p>';

							}

						}

						echo '<div class="row archive_navi">';

							if( $page_number > 1 ) {

								$previous_page = $page_number - 1;
								echo '<div class="btn_primary">
									<a class="prev-post wide_btn" href="'. site_url() .'/search?q='. $search_text .'&searchpage='. $previous_page .'&type='. $type .'"><span class="icon-left-open"></span> Previous <span class="hidden">Results</span></a>
								</div>';

							}

							if( $type == 'web' ) {

								$page_results_count = ( array_key_exists( 'webPages', $jsonobj ) ) ? count( $jsonobj->webPages->value ) : 0;
								$more_text_type = 'Article';

							} else {

								$page_results_count = count( $jsonobj->value );
								$more_text_type = 'Image';

							}

							if( $page_results_count == ${'results_per_page_' . $type} ) {

								$next_page = $page_number + 1;
								echo '<div class="btn_primary">
									<a class="next-post wide_btn" href="'. site_url() .'/search?q='. $search_text .'&searchpage='. $next_page .'&type='. $type .'">More <span class="hidden">'. $more_text_type .' Results</span> <span class="icon-right-open"></span></a>
								</div>';

							}

						echo '</div>';

					}
				?>
			</form>
		</div>
	</div>
</div><!-- row 1/content -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>
