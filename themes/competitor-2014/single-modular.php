<?php
	// single.php will handle displaying posts, attachments and custom posttypes
	get_header();

	$technical_theme_options	= get_option( 'cgi_media_technical_options' );
	$editorial_theme_options	= get_option( 'cgi_media_editorial_options' );
	global $Appid;

	if( have_posts() ) :  while ( have_posts() ) :  the_post();
		$post_type = get_post_type();

		// add specific classes to the article tag so they can be used for styling the page correctly
		$classes = array();

		if( has_post_thumbnail( get_the_ID() ) ) {
			$classes[] = "attachment";
		} else {
			$classes[] = "no-attachment";
		}

		$attachments = get_children(
			array(
				'post_parent'		=> get_the_ID(),
				'post_status'		=> 'inherit',
				'post_type'			=> 'attachment',
				'post_mime_type'	=> 'image',
				'orderby'			=> 'menu_order',
				'order'				=> 'ASC'
			 )
		 );

		if( count( $attachments ) > 1 ) {
			$classes[] = "gallery";
		}

		// LEGACY LISTS
		if( in_category( 'lists' ) ) {

			// legacy lists don't happen for modular posts

		} else { // NORMAL POSTS ?>
			<div class="left_column">
				<div <?php post_class( $classes ); ?>>
					<h1><img class="slashes slash_blue" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/images/running/running-slashes-blue.svg">
						<?php
							$partnerconnect = get_partner_connect( $post->ID );
							if( !empty( $partnerconnect ) ) {
								echo $partnerconnect;
								echo strip_tags( the_title( '','',false ) );
							} else {
								echo strip_tags( the_title( '','',false ) );
							}
						?>
					</h1>
					<p class="post_author">By <?php echo get_guest_author(); ?>, <?php echo format_timestamp_2(); ?></p>
					<div id="share-links">
						<?php
							if ( $technical_theme_options['facebook_comments'] ) {
								print_comments_number();
							}

							print_social_media();
						?>
					</div>

					<div class="content">
						<?php
							$photos = false;
							foreach( ( get_the_category() ) as $category ) {
								if( $technical_theme_options['gallery_category'] ) {
									if( $category->cat_ID == $technical_theme_options['gallery_category'] ) {
										$photos = true;
									}
								}
							}
							if( $photos ) {
								//$output = my_gallery( '', array(), "large", 'gallery-slider' );
								$output =  apply_filters( 'the_content', get_the_content() );
								echo $output;
							} else {
								get_template_part( 'template-parts/content', get_post_format() );
							}
						?>
					</div>

					<?php if( !is_singular( 'shoe' ) ) {
						if( in_category( array( 'partnerconnect', 'sponsored' ) ) ) { ?>

							<div class="postmeta">
								<?php
									print_taxonomy();
									$author_id = -1;

									if( $post->post_type == 'attachment' && $post->post_parent != 0 ) {
										$post_parent = get_post( $post->post_parent );
										if( !get_post_meta( $post_parent->ID, 'guest_author', true ) ) {
											$author_id = $post_parent->post_author;
										}
									} else if( !get_post_meta( $post->ID, 'guest_author', true ) && is_singular( array( 'post', 'page', 'attachment' ) ) ) {
										$author_name = get_the_author_meta( 'display_name' );
										if( !( strpos( $author_name, ".com" ) > -1 ) ) {
											$author_id = $post->post_author;
										}
									}

									if( $author_id != -1 ) {
										echo '<div class="author_meta">';

											$author_image			= get_template_directory() . '/images/author/'.get_the_author_meta( 'ID' ).'.jpg';
											$author_image_display	= get_template_directory_uri() . '/images/author/'.get_the_author_meta( 'ID' ).'.jpg';

											if( file_exists( $author_image ) ) {
												echo "<img src=\"".$author_image_display."\">";
											} else {
												echo get_avatar( get_the_author_meta( 'ID' ), 120, '', $author_name );
											}

											//echo get_avatar( get_the_author_meta( 'ID' ), 120, '', $author_name );
											echo '<h4>' . $author_name . '</h4>
											<p class="author_bio">' . get_the_author_meta( 'description' ) . '</p>';

											$twitter	= trim( get_the_author_meta( 'twitter' ) );
											$facebook	= trim( get_the_author_meta( 'facebook' ) );
											$googleplus	= trim( get_the_author_meta( 'googleplus' ) );

											echo '<ul class="author_social">';
												if( $twitter != '' ) {
													echo '<li><a class="custom-twitter-button" href="https://twitter.com/' . $twitter . '" title="Follow @' . $twitter . '" target="_blank"><span class="social_button"></span></a></li>';
												}
												if( $facebook != '' ) {
													echo '<li><a class="custom-facebook-button" href="http://facebook.com/' . $facebook . '" title="Subscribe on Facebook" target="_blank"><span class="social_button"></span></a></li>';
												}
												if( $googleplus != '' ) {
													echo '<li><a class="custom-gplus-button" href="' . $googleplus . '?rel=author" target="_blank" title="Follow on Google+"><span class="social_button"></span></a></li>';
												}
												echo '<li><a class="author-button" rel="author" href="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '" title="Read more by ' . $author_name . '">All articles by ' . get_the_author_meta( 'first_name' ) . '</a></li>
											</ul>

										</div>';
									}
								?>
							</div><!-- postmeta -->
						<?php } ?>

						<div class="single_recommended_feed">
							<?php
								if( !is_mobile() ) {
									if( $technical_theme_options['outbrain'] ) { ?>
										<div class="OUTBRAIN" data-src="<?php  echo get_permalink(); ?>" data-widget-id="AR_1" data-ob-template="cgi" ></div>
										<script type="text/javascript" async src="http://widgets.outbrain.com/outbrain.js"></script>
									<?php }
								} else {
									if( $technical_theme_options['outbrain'] ) { ?>
										<div class="OUTBRAIN" data-src="<?php  echo get_permalink(); ?>" data-widget-id="MB_1" data-ob-template="cgi" ></div>
										<script type="text/javascript" async src="http://widgets.outbrain.com/outbrain.js"></script>
									<?php }
								}
							?>
						</div><!-- recommended -->

						<?php
							/* begin inline iContact newsletter */
							$icontact_enable	= $editorial_theme_options['icontact_enable'];
							$icontact_inline	= get_post_meta( $post->ID,'_icontact_inline',true );

							//training plans
							$training_plans_txt		= $editorial_theme_options['training_plans_txt'];
							$training_plans_link	= $editorial_theme_options['training_plans_link'];
							$get_training_plans		= get_training_plans( $post->ID );

							if( $icontact_enable && ( $icontact_inline != 'no' ) ) {
								inline_iContact( 'inarticle' );
							}
							/* end inline iContact newsletter */

							echo $get_training_plans;

							if( !in_category( array( 'partnerconnect', 'sponsored' ) ) ) { ?>
								<div class="postmeta">
									<?php
										print_taxonomy();

										$author_id = -1;
										if( $post->post_type == 'attachment' && $post->post_parent != 0 ) {
											$post_parent = get_post( $post->post_parent );
											if( !get_post_meta( $post_parent->ID, 'guest_author', true ) ) {
												$author_id = $post_parent->post_author;
											}
										} else if( !get_post_meta( $post->ID, 'guest_author', true ) && is_singular( array( 'post', 'page', 'attachment' ) ) ) {
											$author_name = get_the_author_meta( 'display_name' );
											if( !( strpos( $author_name, ".com" ) > -1 ) ) {
												$author_id = $post->post_author;
											}
										}
										if( $author_id != -1 ) {

											echo '<div class="author_meta">';
												$author_image			= get_template_directory() . '/images/author/'.get_the_author_meta( 'ID' ).'.jpg';
												$author_image_display	= get_template_directory_uri() . '/images/author/'.get_the_author_meta( 'ID' ).'.jpg';

												if( file_exists( $author_image ) ) {
													echo "<img src=\"".$author_image_display."\">";
												} else {
														echo get_avatar( get_the_author_meta( 'ID' ), 120, '', $author_name );
												}

												//echo get_avatar( get_the_author_meta( 'ID' ), 120, '', $author_name );
												echo '<h4>' . $author_name . '</h4>
												<p class="author_bio">' . get_the_author_meta( 'description' ) . '</p>';

												$twitter	= trim( get_the_author_meta( 'twitter' ) );
												$facebook	= trim( get_the_author_meta( 'facebook' ) );
												$googleplus	= trim( get_the_author_meta( 'googleplus' ) );

												echo '<ul class="author_social">';
													if( $twitter != '' ) {
														echo '<li><a class="custom-twitter-button" href="https://twitter.com/' . $twitter . '" title="Follow @' . $twitter . '" target="_blank"><span class="social_button"></span></a></li>';
													}
													if( $facebook != '' ) {
														echo '<li><a class="custom-facebook-button" href="http://facebook.com/' . $facebook . '" title="Subscribe on Facebook" target="_blank"><span class="social_button"></span></a></li>';
													}
													if( $googleplus != '' ) {
														echo '<li><a class="custom-gplus-button" href="' . $googleplus . '?rel=author" target="_blank" title="Follow on Google+"><span class="social_button"></span></a></li>';
													}
													echo '<li><a class="author-button" rel="author" href="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '" title="Read more by ' . $author_name . '">All articles by ' . get_the_author_meta( 'first_name' ) . '</a></li>
												</ul>
											</div>';

										}
									?>
								</div><!-- postmeta -->
							<?php }

							// related_stories();
							if( !is_mobile() && !is_tablet2() ) { ?>
								<div id="post-comments">
									<?php  if( is_singular( array( 'post', 'attachment' ) ) && $technical_theme_options['facebook_comments'] ) { ?>
										<div class="fb-comments" data-href="<?php  echo get_permalink(); ?>" data-num-posts="10" data-width="660"></div>
									<?php  } ?>
								</div>

								<?php if( $technical_theme_options['ad_com'] && $technical_theme_options['ad_com_pid'] && $technical_theme_options['ad_com_pid'] != "" && $technical_theme_options['ad_com_650_300_pid'] && $technical_theme_options['ad_com_650_300_pid'] != "" ) { ?>
									<div class="advertising-com">
										<script type="text/javascript">
											adsonar_placementId=<?php  echo $technical_theme_options['ad_com_650_300_pid']; ?>;
											adsonar_pid=<?php  echo $technical_theme_options['ad_com_pid']; ?>;
											adsonar_ps=0;
											adsonar_zw=650;
											adsonar_zh=300;
											adsonar_jv='ads.adsonar.com';
										</script>
										<script language="JavaScript" src="http://js.adsonar.com/js/adsonar.js"></script>
									</div>
								<?php }
							} else { ?>
								<div id="post-comments">
									<?php if( is_singular( array( 'post', 'attachment' ) ) && $technical_theme_options['facebook_comments'] ) { ?>
										<div class="fb-comments" data-href="<?php  echo get_permalink(); ?>" data-num-posts="10" data-width="660" data-mobile="false"></div>
									<?php } ?>
								</div>
							<?php }
						?>
					<?php } ?>
				</div>
			</div><!-- row 1/content -->


			<!-- END -->
			<?php
				get_sidebar();

				get_footer();
		}
	endwhile; else : ?>

		<p><?php  _e( 'Sorry, no posts matched your criteria.' ); ?></p>

	<?php endif; ?>
