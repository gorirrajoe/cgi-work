<?php
/**
 * File to hold all scripts in the wp_footer and custom hook for before the close body tag
 */

class Footer_Scripts {

	public static $instance = false;

	public static $technical_theme_options;

	public static $editorial_theme_options;

	public function __construct() {

		// Get technical options and store for use
		self::$technical_theme_options	= get_option( 'cgi_media_technical_options' );
		self::$editorial_theme_options	= get_option( 'cgi_media_editorial_options' );

		add_action( 'wp_footer', array( $this, 'add_site_search_js' ) );
		add_action( 'wp_footer', array( $this, 'add_mapmyrun_js' ) );
		add_action( 'wp_footer', array( $this, 'add_svgeezy_js' ) );
		add_action( 'wp_footer', array( $this, 'add_comScore_tag' ) );
		add_action( 'wp_footer', array( $this, 'add_reinvigorate' ) );
		add_action( 'wp_footer', array( $this, 'add_mandelbrot' ) );
		add_action( 'wp_footer', array( $this, 'add_brightcove_fluid_vid_js' ) );
		add_action( 'wp_footer', array( $this, 'add_social_slider') );
		add_action( 'wp_footer', array( $this, 'add_fb_js') );
		add_action( 'wp_footer', array( $this, 'add_meebo') );
		add_action( 'wp_footer', array( $this, 'add_eXelate') );
		add_action( 'wp_footer', array( $this, 'add_pinterest_script') );

	}

	function add_site_search_js() {

		if ( is_page_template( 'site-search.php' ) ) { ?>

			<script>
				jQuery(function($) { $('#search-tabs').tabs(); });
			</script>

			<?php
		}

	}

	function add_mapmyrun_js() {

		if ( is_page_template( 'mapmyrun-page.php' ) ) { ?>

			<script>
				jQuery(document).ready(function($) {
					$('.fancyroute').fancybox({margin : [20, 60, 20, 60]});
				});
			</script>

			<?php
		}

	}

	function add_svgeezy_js() { ?>

		<script>
			if ( typeof(svgeezy) !== 'undefined' ) {
				svgeezy.init('noreplace', 'png'); // add 'noreplace' class to images you don't want to check (false for all images)
			}
		</script>
		<?php

	}

	function add_comScore_tag() { ?>

		<!-- Begin comScore Tag -->
		<script>
			var _comscore = _comscore || [];
			_comscore.push({ c1: "2", c2: "9728917" });
			(function() {
				var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
				s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
				el.parentNode.insertBefore(s, el);
			})();
		</script>
		<noscript>
			<img src="http://b.scorecardresearch.com/p?c1=2&c2=9728917&cv=2.0&cj=1" />
		</noscript>
		<!-- End comScore Tag -->
		<?php

	}

	function add_reinvigorate() {

		if ( !empty( self::$technical_theme_options['reinvigorate'] ) ) { ?>

			<script type="text/javascript" src="http://include.reinvigorate.net/re_.js"></script>
			<script type="text/javascript">
			try {
				reinvigorate.track("<?php echo self::$technical_theme_options['reinvigorate']; ?>");
			} catch(err) {}
			</script>

		<?php }

	}

	function add_mandelbrot() {

		if ( !empty( self::$technical_theme_options['mandelbrot'] ) ) { ?>
			<script type="text/javascript">
			// <![CDATA[
				var _mb_site_guid = "<?php echo self::$technical_theme_options['mandelbrot']; ?>";
				(function(d, t){
						var mb = d.createElement(t), s = d.getElementsByTagName(t)[0];
						mb.async = mb.src = '//cdn.linksmart.com/linksmart_2.0.0.min.js';
						s.parentNode.insertBefore(mb, s);
				}(document, 'script'));
			// ]]>
			</script>
		<?php }

	}

	function add_brightcove_fluid_vid_js() {

		if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') ) {
			echo '<script type="text/javascript">brightcove.createExperiences();</script>' . "\n";
			echo '<script type="text/javascript">
				jQuery(document).ready(function() {jQuery(".fluid-width-video-wrapper").css("padding-top", "56.333333333333336%");});
				</script>';
		}

	}

	function add_social_slider() {

		$show_social_slider	= isset( self::$technical_theme_options['show_social_slider'] ) ? self::$technical_theme_options['show_social_slider'] : false;
		$twitter			= isset( self::$technical_theme_options['site_twitter_handle'] ) ? self::$technical_theme_options['site_twitter_handle'] : '';

		if ( $show_social_slider ) { ?>

			<script type="text/javascript">
				var $container	= $('#container');

				if ( $container.length > 0 ) {
					$container.append('<div id="UIShareBarContainer"></div>');
				}

				var unEncodedCanonical	= "<?php echo get_bloginfo('url');?><?php $_SERVER['REQUEST_URI']; ?>";
				var encodedCanonical	= encodeURIComponent(unEncodedCanonical);
				var siteTwitterHandle	= "<?php echo $twitter; ?>";
			</script>
			<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/share-footer.js" ></script>

		<?php }

	}

	function add_fb_js() {

		$facebook_appid	= !empty( self::$technical_theme_options['facebook_appid'] ) ? self::$technical_theme_options['facebook_appid'] : '';

		if ( $facebook_appid != '' ) { ?>

			<div id="fb-root"></div>
			<script type="text/javascript">
				// load the facebook javascript on all pages if there's an app id
				window.fbAsyncInit = function() {
					FB.init({
						appId: '<?php echo $facebook_appid;?>', // App ID
						status: true, // check login status
						cookie: true, // enable cookies to allow the server to access the session
						xfbml: true  // parse XFBML
					});

					// Additional initialization code here
				};
				// Load the SDK Asynchronously
				(function(d){
					var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
					if (d.getElementById(id)) {return;}
					js = d.createElement('script'); js.id = id; js.async = true;
					js.src = "//connect.facebook.net/en_US/all.js";
					ref.parentNode.insertBefore(js, ref);
				 }(document));

			</script>

			<?php
		}

	}

	function add_meebo() {

		if ( isset( $GLOBALS['meebo'] ) && $GLOBALS['meebo'] == true ) { ?>
			<style type="text/css">
				#meebo img {
					max-width:inherit;
				}
			</style>
			<script type="text/javascript">
				Meebo('domReady');
			</script>
		<?php }

	}

	function add_eXelate() {

		if ( !empty( self::$technical_theme_options['eXelate'] ) ) {
			// get all of the information to put in eXelate code:
			$category_string	= '';
			$keyword_string		= '';

			global $post;

			if ( is_category() ) {

				$queried_category = get_term( get_query_var('cat'), 'category' );
				$category_string = $queried_category->name;

			} elseif ( is_archive() ) {

				$category_string = '';

			} else {

				foreach ( ( get_the_category( $post->ID ) ) as $category ) {
					$category_string .= $category->cat_name . ',';
				}
				if ( $category_string != '' ) {
					$category_string = substr($category_string, 0, -1);
				}

			}
			if ( $_REQUEST['q'] ) {

				$keyword_string		= $_REQUEST['q'];
				$category_string	= '';

			} elseif ( $_POST['searchText'] ) {

				$keyword_string		= $_POST['searchText'];
				$category_string	= '';

			}

			$url	= site_url();
			$g		= '';

			if ( $url == 'http://running.competitor.com' ) {
				$g	= '001';
			}

			echo '<script type="text/javascript" src="http://loadus.exelator.com/load/?p=349&g='. $g .'&c=118748&ctg='. $category_string .'&kw='. $keyword_string .'"></SCRIPT>';
		}

	}

	function add_pinterest_script() { ?>

		<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
		<?php

	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( !self::$instance )
			self::$instance = new self;

		return self::$instance;
	}

}
