<?php

	class Running_AMP_Gifs extends AMP_Base_Embed_Handler {
		public function register_embed() {
			add_shortcode( 'gifs', array( $this, 'gifs_embed' ) );
		}

		public function unregister_embed() {
			remove_shortcode( 'gifs' );
		}

		public function get_scripts() {
			if ( ! $this->did_convert_elements ) {
				return array();
			}

			return array( 'amp-iframe' => 'https://cdn.ampproject.org/v0/amp-iframe-0.1.js' );
		}

		public function gifs_embed( $atts = array() ) {
			$a = shortcode_atts(
				array(
					'url'	=> '',
					'file'	=> ''
				),
				$atts
			);

			if ( empty( $a['url'] ) ) {
				return;
			}

			// If we've made it this far then we will be displaying the shortcode
			// Record this
			$this->did_convert_elements = true;

			return sprintf(
				'<amp-iframe width=480 height=270
					sandbox="allow-scripts allow-same-origin allow-popups allow-popups-to-escape-sandbox"
					layout="responsive"
					frameborder="0"
					src="%s">
					<amp-img layout="fill" src="%s" placeholder></amp-img>
				</amp-iframe>',
				$a['url'],
				$a['file']
			);

		}
	}

?>
