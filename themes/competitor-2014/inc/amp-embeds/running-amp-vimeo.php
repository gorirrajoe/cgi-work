<?php

	class Running_AMP_Vimeo extends AMP_Base_Embed_Handler {
		public function register_embed() {
			add_shortcode( 'vimeo', array( $this, 'vimeo_embed' ) );
		}

		public function unregister_embed() {
			remove_shortcode( 'vimeo' );
		}

		public function get_scripts() {
			if ( ! $this->did_convert_elements ) {
				return array();
			}

			return array( 'amp-vimeo' => 'https://cdn.ampproject.org/v0/amp-vimeo-0.1.js' );
		}

		public function vimeo_embed( $atts = array() ) {
			$a = shortcode_atts(
				array(
					'id' => ''
				),
				$atts
			);

			if ( empty( $a['id'] ) ) {
				return;
			}

			// If we've made it this far then we will be displaying the shortcode
			// Record this
			$this->did_convert_elements = true;

			return sprintf(
				'<amp-vimeo data-videoid="%s" layout="responsive" width="500" height="281"></amp-vimeo>',
				$a['id']
			);
		}
	}

?>
