<?php

	class Running_AMP_Gallery extends AMP_Base_Embed_Handler {
		public function register_embed() {
			add_shortcode( 'gallery', array( $this, 'gallery_embed' ) );
		}

		public function unregister_embed() {
			remove_shortcode( 'gallery' );
		}

		public function get_scripts() {
			if ( !$this->did_convert_elements ) {
				return array();
			}

			return array( 'amp-carousel' => 'https://cdn.ampproject.org/v0/amp-carousel-0.1.js' );
		}

		public function gallery_embed( $attr = array() ) {

			global $post;

			// check $post exists
			if ( empty( $post ) ) {
				return;
			}

			static $index = 0;
			$index++;

			if ( !empty( $attr['ids'] ) ) {

				// 'ids' is explicitly ordered, unless you specify otherwise.
				if ( empty( $attr['orderby'] ) ) {
					$attr['orderby'] = 'post__in';
				}

				$attr['include'] = $attr['ids'];

			}

			$content	= get_the_content();

			$output		= apply_filters( 'post_gallery', '', $attr, $content );

			if ( ! empty( $output ) ) {
				return $output;
			}

			$size = 'medium';

			$html5 = current_theme_supports( 'html5', 'gallery' );

			$atts = shortcode_atts(
				array(
					'order'			=> 'ASC',
					'orderby'		=> 'menu_order ID',
					'id'			=> ( $post ? $post->ID : 0 ),
					'itemtag'		=> ( $html5 ? 'figure'     : 'dl' ),
					'icontag'		=> ( $html5 ? 'div'        : 'dt' ),
					'captiontag'	=> ( $html5 ? 'figcaption' : 'dd' ),
					'columns'		=> 1,
					'include'		=> '',
					'exclude'		=> '',
					'link'			=> '',
					'autoplay'		=> ''
				),
				$attr,
				'gallery'
			);

			$id = intval( $atts['id'] );

			if ( ! empty( $atts['include'] ) ) {

				$attachments = array();

				$params = array(
					'post__in'			=> explode( ',', $atts['include'] ),
					'post_status'		=> 'inherit',
					'post_type'			=> 'attachment',
					'post_mime_type'	=> 'image',
					'order'				=> $atts['order'],
					'orderby'			=> $atts['orderby'],
					'posts_per_page'	=> -1
				);

				$_attachments = new WP_Query( $params );

				if ( $_attachments->have_posts() ) : while ( $_attachments->have_posts() ) : $_attachments->the_post();

					$attachments[$post->ID] = $post;

				endwhile; endif;

				wp_reset_postdata();

			} elseif ( ! empty( $atts['exclude'] ) ) {

				$params =
					array(
						'post_parent'		=> $id,
						'exclude'			=> $atts['exclude'],
						'post_status'		=> 'inherit',
						'post_type'			=> 'attachment',
						'post_mime_type'	=> 'image',
						'order'				=> $atts['order'],
						'orderby'			=> $atts['orderby']
					);

				$attachments = get_children( $params );

			} else {

				$params =
					array(
						'post_parent'		=> $id,
						'post_status'		=> 'inherit',
						'post_type'			=> 'attachment',
						'post_mime_type'	=> 'image',
						'order'				=> $atts['order'],
						'orderby'			=> $atts['orderby']
					);

				$attachments = get_children( $params );

			}

			if ( empty( $attachments ) ) {
				return '';
			}

			// If we've made it this far then we will be displaying the shortcode
			// Record this
			$this->did_convert_elements = true;

			$output = '<amp-carousel width="400" height="300" layout="responsive" type="slides" class="amp-carousel">';

				foreach ( $attachments as $id => $attachment ) {

					$image_meta  = wp_get_attachment_metadata( $id );
					if ( !empty( $image_meta['sizes'][$size]['width'] ) ) {
						$width	= $image_meta['sizes'][$size]['width'];
					} elseif ( !empty( $image_meta['width'] ) ) {
						$width	= $image_meta['width'];
					}
					if ( !empty( $image_meta['sizes'][$size]['height'] ) ) {
						$height	= $image_meta['sizes'][$size]['height'];
					} elseif ( !empty( $image_meta['height'] ) ) {
						$height	= $image_meta['height'];
					}

					$image_tag = sprintf(
						'<amp-img src="%s" layout="fill"></amp-img>',
						wp_get_attachment_image_src( $attachment->ID, $size )[0]
					);

					$caption	= '';

					if ( !empty( $attachment->post_title ) || !empty( $attachment->post_content ) ) {

						$caption_title	= ( $attachment->post_title != '' ) ? '<h4>'. $attachment->post_title .'</h4>' : '';

						$excerpt = trim( $attachment->post_content );
						$excerpt = wptexturize($excerpt);

						$caption	= $caption_title . $excerpt;

						$caption	= '<div class="caption">'. $caption .'</div>';
					}

					// Tie it all up with the carousel
					$output	.= '<div class="slide">'. $image_tag . $caption .'</div>';

				}

			$output .= '</amp-carousel>';

			return $output;

		}
	}

?>
