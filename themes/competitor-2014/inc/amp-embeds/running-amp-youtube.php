<?php

	class Running_AMP_Youtube extends AMP_Base_Embed_Handler {
		public function register_embed() {
			remove_action('the_content', 'vid_before'); // that pesky addition of the video through a filter. might need to clean that up
			add_shortcode( 'youtube', array( $this, 'youtube_embed' ) );
		}

		public function unregister_embed() {
			remove_shortcode( 'youtube' );
		}

		public function get_scripts() {
			if ( ! $this->did_convert_elements ) {
				return array();
			}

			return array( 'amp-youtube' => 'https://cdn.ampproject.org/v0/amp-youtube-0.1.js' );
		}

		public function youtube_embed( $atts = array() ) {
			$a = shortcode_atts(
				array(
					'id' => ''
				),
				$atts
			);

			if ( empty( $a['id'] ) ) {
				return;
			}

			// If we've made it this far then we will be displaying the shortcode
			// Record this
			$this->did_convert_elements = true;

			return sprintf(
				'<amp-youtube data-videoid="%s" height="270" width="480" layout="responsive"></amp-youtube>',
				$a['id']
			);
		}
	}

?>
