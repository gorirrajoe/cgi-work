<?php
/**
 * Class for Gear Guide
 * Adding a new Gear Guide:
 *		1) Add new year to generate_options_array
 *		2) Define a new function at bottom of class with fields
 *		3) add function call to register_cmb2_options
 */
class GearGuide_Admin {

	/**
	 * Option key, and option page slug
	 * @var string
	 */
	private $key = 'gearguide_options';

	/**
	 * Options page metabox id
	 * @var string
	 */
	private $metabox_id = 'gearguide_option_metabox';

	/**
	 * Options Page title
	 * @var string
	 */
	protected $title = '';

	/**
	 * Options Page hook
	 * @var string
	 */
	protected $options_page = '';

	protected $suboptions_pages = array();

	/**
	 * Holds an instance of the object
	 *
	 * @var GearGuide_Admin
	 **/
	private static $instance = null;

	/**
	 * Holds array with the options sub-pages and slugs
	 *
	 * @var Array
	 **/
	protected $categorty_groups = '';

	/**
	 * Holds array with the slugs
	 *
	 * @var Array
	 **/
	protected $category_slugs = '';

	/**
	 * Holds the page/subpage currently on
	 *
	 * @var string
	 **/
	protected $page = '';

	/**
	 * Constructor
	 * @since 0.1.0
	 */
	private function __construct() {

		$this->title = __( 'Gear Guide Options', 'gearguide' );

		$this->generate_options_array();
		$this->_add_actions();

	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if(! self::$instance)
			self::$instance = new self;

		return self::$instance;
	}

	/**
	 * Options Array Generator
	 *
	 * Adds years to Gear Guide.
	 */
	public function generate_options_array() {
		$year_beginning	= 2015;
		$year_current	= date( 'Y' );

		$options_array	= array();
		$the_slugs		= array();
		$key			= $this->key;

		for( $i = $year_current; $i >= $year_beginning; $i-- ) {
			$options_array[$key .'-'. $i]	= array(
				'title'	=> $i .' Gear Guide',
				'slug'	=> $key .'-'. $i,
				'year'	=> "$i"
			);
			$the_slugs[]	= $i . '-running-gear-guide';
		}

		$options_array[$key .'-help']	= array(
			'title'	=> 'Gear Guide Help',
			'slug'	=> $key .'-help',
			'year'	=> "help"
		);

		$this->option_groups	= $options_array;
		$this->category_slugs	= $the_slugs;

	}

	/**
	 * Enqueue our scripts necessary for these pages
	 */
	public function gear_guide_scripts() {

		/*if ( is_category( $this->category_slugs ) || in_category( $this->category_slugs ) ) {
			wp_enqueue_style( 'gear-guide-style', get_template_directory_uri() . '/css/gear-guide.min.css', array(), filemtime( get_template_directory() . '/css/gear-guide.min.css' ) );
			// wp_enqueue_style( 'owl-carousel-style', get_template_directory_uri() . '/css/vendor/owl.min.css', array(), filemtime( get_template_directory() . '/css/vendor/owl.min.css' ) );

		}*/
	}

	/**
	 * Register our setting to WP
	 */
	public function init() {
		register_setting( $this->key, $this->key );
	}


	/**
	 * Add menu options page
	 */
	public function add_options_page() {

		$this->options_page	= add_menu_page( $this->title, $this->title, 'manage_options', $this->key, array( $this, 'admin_page_display' ) );

		foreach( $this->option_groups as $option_group ) {
			$this->suboptions_pages[]	= add_submenu_page( $this->key, $option_group['title'], $option_group['year'] ,'edit_posts', $option_group['slug'], array( $this, 'admin_page_display' ) );
		}

		// remove top level submenu which holds no data (no year)
		// TODO: possibly make the top level menu into a form adding new years?
		remove_submenu_page( $this->key, $this->key );

		// Include CMB CSS in the head to avoid FOUC
		add_action( "admin_print_styles-{$this->options_page}", array( 'CMB2_hookup', 'enqueue_cmb_css' ) );

		foreach( $this->suboptions_pages as $suboptions_page){
			add_action( "admin_print_styles-{$suboptions_page}", array( 'CMB2_hookup', 'enqueue_cmb_css' ) );
		}

	}

	/**
	 * Register CMB2 Options
	 *
	 * Controller method for registering all CMB2 options.
	 */
	public function register_cmb2_options() {

		$this->_register_2015_options();
		$this->_register_2016_options();
		$this->_register_2017_options();
		$this->_register_final_options();
		$this->_register_help_options();


	}

	/**
	 * Admin page markup. Mostly handled by CMB2
	 * @since  0.1.0
	 */
	public function admin_page_display() {

		$this->page		= isset( $_GET['page'] ) ? $_GET['page']: '';
		$current_option	= array();

		// check which page we are on
		foreach( $this->option_groups as $option_group ){

			if( in_array( $this->page, $option_group ) ){
				$current_option	= $option_group;
				break;
			}

		} ?>

		<div class="wrap cmb2-options-page <?php echo $current_option['slug']; ?>">
			<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>

			<?php if( $current_option['slug'] == 'gearguide_options-help' ) {

				echo $this->_register_help_options();

			} else {

				cmb2_metabox_form( $this->metabox_id . '-' . $current_option['year'], $current_option['slug'] );

			} ?>

		</div>

	<?php }


	/**
	 * Register settings notices for display
	 *
	 * @param  int   $object_id Option key
	 * @param  array $updated   Array of updated fields
	 * @return void
	 */
	public function settings_notices( $object_id, $updated = array() ) {

		$this->page		= isset( $_GET['page'] ) ? $_GET['page']: '';

		if ( $object_id !== $this->page || empty( $updated ) ) {
			return;
		}

		add_settings_error( $this->page . '-notices', '', __( 'Settings updated.', 'running' ), 'updated' );
		settings_errors( $this->page . '-notices' );
	}

	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {

		add_action( 'admin_init', array( $this, 'init' ) );
		add_action( 'admin_menu', array( $this, 'add_options_page' ) );
		add_action( 'cmb2_admin_init', array( $this, 'register_cmb2_options' ) );
		add_action( 'template_include', array( $this, 'gear_guide_template' ) );
		// add_action( 'wp_enqueue_scripts', array( $this, 'gear_guide_scripts' ) );


		foreach( $this->option_groups as $option_group ) {
			add_action( "cmb2_save_options-page_fields_{$this->metabox_id}-{$option_group['year']}", array( $this, 'settings_notices' ), 10, 2 );
		}

	}


	protected function _register_2015_options() {
		$prefix	= 'gear_guide_2015_';

		$rgg_2015 = new_cmb2_box( array(
			'id'		=> $this->metabox_id . '-2015',
			'hookup'	=> false,
			'show_on'	=> array(
				'key'	=> 'options-page',
				'value'	=> array( $this->key . '-2015', )
			),
		) );

		// banner fields
		$rgg_2015->add_field( array(
			'id'		=> $prefix . 'banner_image',
			'name'		=> __( 'Gear Guide Banner Image', 'running' ),
			'desc'		=> 'Max image size is 1260x200. Either enter a full URL to an image or upload/choose an image to/from the media library.',
			'type'		=> 'file',
			'options'	=> array(
				'add_upload_file_text' => 'Choose Image'
			)
		) );

		/* grid images */
		for( $i = 1; $i < 22; $i++) {

			$rgg_2015->add_field( array(
				'id'		=> $prefix . 'image_' . $i,
				'name'		=> __( 'Image '. $i, 'running'),
				'type'		=> 'file',
				'options'	=> array(
					'add_upload_file_text' => 'Choose Image'
				)
			) );
			$rgg_2015->add_field( array(
				'id'		=> $prefix . 'link_' . $i,
				'name'		=> __( 'Link '. $i, 'running'),
				'desc'		=> 'Enter a link for image #'. $i,
				'type'		=> 'text_url',
			) );

			$rgg_2015->add_field( array(
				'id'		=> $prefix . 'more_gear_image_' . $i,
				'name'		=> __( 'Gear Image '. $i, 'running'),
				'desc'		=> 'Image size is 390 x 200. Either enter a full URL to an image or upload/choose an image to/from the media library. ',
				'type'		=> 'file',
				'options'	=> array(
					'add_upload_file_text' => 'Choose Image'
				)
			) );
		} //end grid images

	}


	protected function _register_2016_options() {
		$prefix	= 'gear_guide_2016_';

		$rgg_2016 = new_cmb2_box( array(
			'id'		=> $this->metabox_id . '-2016',
			'hookup'	=> false,
			'show_on'	=> array(
				'key'	=> 'options-page',
				'value'	=> array( $this->key . '-2016', )
			),
		) );

		// banner fields
		$rgg_2016->add_field( array(
			'id'		=> $prefix . 'banner_image',
			'name'		=> __( 'Gear Guide Banner Image', 'running' ),
			'desc'		=> 'Max image size is 1260x200. Either enter a full URL to an image or upload/choose an image to/from the media library.',
			'type'		=> 'file',
			'options'	=> array(
				'add_upload_file_text' => 'Choose Image'
			)
		) );

		/* grid images */
		for( $i = 1; $i < 22; $i++ ) {

			$rgg_2016->add_field( array(
				'id'		=> $prefix . 'image_' . $i,
				'name'		=> __( 'Image '. $i, 'running'),
				'type'		=> 'file',
				'options'	=> array(
					'add_upload_file_text' => 'Choose Image'
				)
			) );
			$rgg_2016->add_field( array(
				'id'		=> $prefix . 'link_' . $i,
				'name'		=> __( 'Link '. $i, 'running'),
				'desc'		=> 'Enter a link for image #'. $i,
				'type'		=> 'text_url',
			) );
			$rgg_2016->add_field( array(
				'id'		=> 'more_gear_2016_image_' . $i,
				'name'		=> __( 'Gear Image '. $i, 'running'),
				'desc'		=> 'Image size is 390x200. Either enter a full URL to an image or upload/choose an image to/from the media library.',
				'type'		=> 'file',
				'options'	=> array(
					'add_upload_file_text' => 'Choose Image'
				)
			) );

		} //end grid images

	}


	protected function _register_2017_options() {
		$prefix	= 'gear_guide_2017_';

		$rgg_2017 = new_cmb2_box( array(
			'id'		=> $this->metabox_id . '-2017',
			'hookup'	=> false,
			'show_on'	=> array(
				'key'	=> 'options-page',
				'value'	=> array( $this->key . '-2017', )
			),
		) );

		// banner fields
		$rgg_2017->add_field( array(
			'id'		=> $prefix . 'banner_image',
			'name'		=> __( 'Gear Guide Banner Image', 'running' ),
			'desc'		=> 'Max image size is 1260x200. Either enter a full URL to an image or upload/choose an image to/from the media library.',
			'type'		=> 'file',
			'options'	=> array(
				'add_upload_file_text' => 'Choose Image'
			)
		) );

		/* grid images */
		for( $i = 1; $i < 21; $i++ ) {

			$rgg_2017->add_field( array(
				'id'		=> $prefix . 'image_' . $i,
				'name'		=> __( 'Image '. $i, 'running'),
				'type'		=> 'file',
				'options'	=> array(
					'add_upload_file_text' => 'Choose Image'
				)
			) );
			$rgg_2017->add_field( array(
				'id'		=> $prefix . 'link_' . $i,
				'name'		=> __( 'Link '. $i, 'running'),
				'desc'		=> 'Enter a link for image #'. $i,
				'type'		=> 'text_url',
			) );
			$rgg_2017->add_field( array(
				'id'		=> $prefix . 'more_gear_image_' . $i,
				'name'		=> __( 'Gear Image '. $i, 'running'),
				'desc'		=> 'Image size is 390x200. Either enter a full URL to an image or upload/choose an image to/from the media library.',
				'type'		=> 'file',
				'options'	=> array(
					'add_upload_file_text' => 'Choose Image'
				)
			) );

		} //end grid images

	}


	protected function _register_final_options() {

		for( $x = 2018; $x <= date( 'Y' ); $x++ ) {

			$prefix	= 'gear_guide_'. $x .'_';

			$rgg_cmb2_var = 'rgg_'. $x;

			$rgg_cmb2_var = new_cmb2_box( array(
				'id'		=> $this->metabox_id . '-' . $x,
				'hookup'	=> false,
				'show_on'	=> array(
					'key'	=> 'options-page',
					'value'	=> array( $this->key . '-' . $x, )
				),
			) );

			$rgg_cmb2_var->add_field( array(
				'name'    => 'Intro Text',
				'id'      => $prefix . 'intro',
				'type'    => 'wysiwyg',
				'options' => array(),
			) );

			// banner fields
			$rgg_cmb2_var->add_field( array(
				'id'		=> $prefix . 'banner_image',
				'name'		=> __( 'Gear Guide Banner Image', 'wr' ),
				'desc'		=> 'Max image size is 1260 x 200. Either enter a full URL to an image or upload/choose an image to/from the media library.',
				'type'		=> 'file',
				'options'	=> array(
					'add_upload_file_text' => 'Choose Image'
				)
			) );

			$group_field_id = $rgg_cmb2_var->add_field( array(
				'id'          => $prefix . 'image_group',
				'type'        => 'group',
				'description' => __( 'Images for the Gear Guide', 'cmb2' ),
				'options'     => array(
					'group_title'   => __( 'Image {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
					'add_button'    => __( 'Add Another Image', 'cmb2' ),
					'remove_button' => __( 'Remove Image', 'cmb2' ),
					'sortable'      => true, // beta
				),
			) );

			$rgg_cmb2_var->add_group_field( $group_field_id, array(
				'name' => 'Title',
				'id'   => 'title',
				'type' => 'text',
			) );

			$rgg_cmb2_var->add_group_field( $group_field_id, array(
				'name' => 'Image',
				'id'   => 'image',
				'desc' => 'Max image size is <strong>360x360</strong>. Either enter a full URL to an image or upload/choose an image to/from the media library.',
				'type' => 'file',
			) );

			$rgg_cmb2_var->add_group_field( $group_field_id, array(
				'name' => 'Link',
				'id'   => 'link',
				'type' => 'text_url',
			) );

			// $rgg_cmb2_var->add_group_field( $group_field_id, array(
			// 	'name' => 'Gear Image',
			// 	'id'   => 'gear_image',
			// 	'type' => 'file',
			// 	'desc' => 'Image size is <strong>360 x 165</strong>. Either enter a full URL to an image or upload/choose an image to/from the media library.'
			// ) );

			/*$rgg_cmb2_var->add_field( array(
				'id'		=> $prefix . 'subscribe_image',
				'name'		=> __( 'Subscription Image', 'running'),
				'desc'		=> 'Select Image for subscription link',
				'type'		=> 'file',
				'options'	=> array(
					'add_upload_file_text' => 'Choose Image'
				)
			) );
			$rgg_cmb2_var->add_field( array(
				'id'		=> $prefix . 'subscribe_link',
				'name'		=> __( 'Subscribe Link ', 'running'),
				'desc'		=> 'Enter a link for subscription link',
				'type'		=> 'text_url',
			) );*/
		}

	}


	protected function _register_help_options() {

		$help = '
			<h3>Gift Guide Options</h3>
			<p>Every year, a new gift guide options page will automatically be created.</p>
			<ul style="list-style: disc; margin-left: 35px;">
				<li><strong>Title</strong>: Enter the title for the image. It will most likely be whatever text is in the image.</li>
				<li><strong>Gift Guide Banner Image</strong>: If an image is uploaded here, it will appear at the top of all associated gift guide pages and posts. This is optional, size requirements are mentioned below the field.</li>
				<li><strong>Image #</strong>:
					<ul style="list-style: disc; margin-left: 35px;">
						<li><strong>Title</strong>: The URL in this field will link to either the subcategory page (e.g. /category/2017-gift-guide/accessories) or the post itself.</li>
						<li><strong>Image</strong>: You can add as many images as you need, size requirements are mentioned below the upload field. No need to add a border within the image itself -- the site will give it a border.</li>
						<li><strong>Link</strong>: The URL in this field will link to either the subcategory page (e.g. /category/2017-gift-guide/accessories) or the post itself.</li>
					</ul>
				</li>
			</ul>

			<h3>Category</h3>
			<p>Under <strong>Posts > Categories</strong>, type in the current year, then "Gift Guide" -- ex: "2017 Gift Guide" (without quotes). Then click "Add New Category"</p>
			<p>You can then add as many subcategories under that parent category.</p>

			<h3>Menu</h3>
			<p>Under <strong>Appearance > Menus</strong>, create a new menu using the name you just used for the above category with "Submenu" added to it -- ex: "2017 Gift Guide Submenu" (without quotes).</p>
			<p>If set up properly, each subcategory will be added here. Top level menu items are used as dropdowns and will not be clickable to a category page.</p>

			<h3>Single Post</h3>
			<p>Write the review as you normally would. <strong>Categorize</strong> the post as the current year\'s Gift Guide (e.g. "2017 Gift Guide") and then subcategorize it if needed.</p>
			<p>In addition, scroll down under the "Running Custom Fields" area to see "Gift Guide - Reviewer ID", "Gift Guide - Product Rating", and "Gift Guide - Buy URL". All fields are optional.</p>
			<ul style="list-style: disc; margin-left: 35px;">
				<li><strong>Gift Guide - Reviewer ID</strong>: Enter the ID for the particular reviewer. This will pull both the reviewer\'s avatar and bio.</li>
				<li><strong>Gift Guide - Product Rating</strong>: Each product can display a 5-star max rating, with half star increments.</li>
				<li><strong>Gift Guide - Buy URL</strong>: Enter the URL where the reader can purchase the product.</li>
			</ul>
			<p>The safest bet is to make all product images square since this is what will appear on the landing page grid. This would include the image within the editor of the post and the Featured Image.</p>
		';

		return $help;

	}

	public function gear_guide_template( $original_template ) {

		if ( is_category( $this->category_slugs ) || ( !is_single() && in_category( $this->category_slugs ) ) ) {
			return STYLESHEETPATH . '/category-gear-guide.php';
		} elseif( is_single() && in_category( $this->category_slugs ) ) {
			return STYLESHEETPATH . '/single-gear-guide.php';
		} else {
			return $original_template;
		}

	}

	public function gear_guide_redirect() {
		global $post;

		if ( is_single() ) {
			$gear_guide_cat = 'gear-guide';

			if ( in_category( $gear_guide_cat ) ) {
				include ( STYLESHEETPATH . '/gear-guide-template.php' );
				exit;
			}
		}
	}

	/**
	 * Get Theme Option
	 *
	 * Utility method that returns the given theme option (if exists).
	 *
	 * @param string $option Options array key
	 *
	 * @return mixed Options array or specific field
	 */
	public static function get_guide_option( $year = null,  $option = null ) {
		$self = self::singleton();
		$value = null;

		if( function_exists( 'cmb2_get_option' ) ) {

			$value = cmb2_get_option( $self->key . '-' . $year, $option );

		} else {

			$options = get_option( $self->key . '-' . $year );

			if( !empty( $options ) && isset( $options[$option] ) ) {
				$value = $options[$option];
			}

		}

		return $value;
	}
}
