<?php

class Running_Setup_Theme {

	public static $instance = false;

	public static $technical_theme_options;

	public static $editorial_theme_options;

	public function __construct() {

		// Get options and store for use in functions
		self::$technical_theme_options	= get_option( 'cgi_media_technical_options' );
		self::$editorial_theme_options	= get_option( 'cgi_media_editorial_options' );

		$this->_setup_hooks();

	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( !self::$instance )
			self::$instance = new self;

		return self::$instance;

	}

	/**
	 * Setup WP Hooks
	 *
	 * Defines all the WordPress actions and filters used by this theme.
	 */
	protected function _setup_hooks() {

		add_action( 'after_setup_theme', array( $this, 'competitor2k14_setup' ) );
		add_action( 'widgets_init', array( $this, 'competitor2k14_widgets_init' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'competitor2k14_enqueue_scripts' ) );

		add_filter( 'previous_posts_link_attributes', array( $this, 'posts_link_attributes_prev' ) );
		add_filter( 'next_posts_link_attributes', array( $this, 'posts_link_attributes_next' ) );
		add_filter( 'pre_get_posts', array( $this, 'per_category_basis' ), 1 );
		//add_filter( 'the_excerpt_rss', array( $this, 'insertThumbnailRSS' ) );
		add_filter( 'the_content_feed', array( $this, 'insertThumbnailRSS' ) );

		add_filter( 'get_the_title', array( $this, 'strip_title_tags' ), 10, 1 );
		add_filter( 'the_title', array( $this, 'strip_title_tags' ), 10, 1 );
		add_filter( 'single_post_title', array( $this, 'strip_title_tags' ), 10, 1 );
		add_filter( 'get_the_excerpt', array( $this, 'trim_excerpt' ) );
		add_filter( 'gallery_style', array( $this, 'cgi_media_gallery' ), 99 );
		add_filter( 'comment_cookie_lifetime', array( $this, 'set_lower_comment_cookie_lifetime' ) );
		// add_filter( 'post_gallery', array( $this, 'skip_gallery' ), 10, 2 );
		add_filter( 'query_vars', array( $this, 'parameter_queryvars' ) );
		// add_filter( 'post_thumbnail_html', array( $this, 'my_post_image_html' ), 10, 3 );
		add_filter( 'the_content', array( $this, 'shortcode_empty_paragraph_fix' ) );

		// Redirects
		add_action( 'category_template', array( $this, 'child_force_category_template' ) );
		add_action( 'template_redirect', array( $this, 'video_redirect') , 9999 );
		add_filter( 'single_template', array( $this, 'redirect_single_lists_posts' )  );

		// RSS related
		add_action( 'rss2_head', array( $this, 'no_responsive_image_feeds' ) );
		add_action( 'atom_head', array( $this, 'no_responsive_image_feeds' ) );
		add_action( 'rss_head', array( $this, 'no_responsive_image_feeds' ) );

		// remove nasty features of WP
		remove_action( 'wp_head', array( $this, 'print_emoji_detection_script' ), 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'admin_print_styles', 'print_emoji_styles' );

		// remove WP version number
		remove_action( 'wp_head', 'wp_generator' );
		add_filter( 'style_loader_src', array( $this, 'remove_wp_ver_css_js') , 9999 );
		add_filter( 'script_loader_src', array( $this, 'remove_wp_ver_css_js') , 9999 );

		// custom hooks
		add_action( 'body_end_hook', array( $this, 'body_end_code' ) );
		add_action( 'body_end_hook', array( $this, 'eu_regulation' ) );

	}

	public function competitor2k14_setup() {

		register_nav_menus( array(
			'primary-nav'			=> 'Primary Navigation',
			'digital_network'		=> 'Digital Network',
			'events_network'		=> 'Events Network',
			'slidebar_left_nav'		=> 'Slidebar Left Nav',
			'slidebar_right_nav'	=> 'Slidebar Right Nav'
		) );

		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );

		set_post_thumbnail_size( 120, 120, true );

		add_image_size( 'marquee-carousel', 728, 410, true );
		add_image_size( 'marquee-carousel-mobile', 480, 270, true );
		add_image_size( 'small-marquee', 580, 435, true );
		add_image_size( 'gallery-carousel', 200, 200, true );
		add_image_size( 'gallery-carousel-2', 930, 0, false );
		add_image_size( 'featured-thumb', 120, 68, true );
		add_image_size( 'featured-gallery', 590, 360, true );
		add_image_size( 'newsletter-top', 400, 225, true );
		add_image_size( 'newsletter-splash', 620, 400, true );
		add_image_size( 'newsletter-image', 300, 200, true );
		add_image_size( 'legacy-list', 660, 495, false );
		add_image_size( 'single-post-big', 800, 600, false );
		add_image_size( 'home-photo-gallery', 640, 480, true );
		add_image_size( 'home-big-marquee', 640, 360, true );
		add_image_size( 'square-big-thumbs', 640, 640, true );
		add_image_size( 'square-gift-guide-product', 420, 420, true );
		add_image_size( 'partner-connect-home-image', 640, 360, true);

		$editor = get_role('editor');
		// add $cap capability to this role object
		$editor->add_cap('edit_theme_options');
		$editor->add_cap('manage_options');

		add_post_type_support( 'page', 'excerpt' );
	}

	public function competitor2k14_widgets_init() {

		register_sidebar( array(
			'name'			=> 'Marquee',
			'id'			=> 'marquee',
			'description'	=> '',
			'before_widget'	=> '',
			'after_widget'	=> '',
			'before_title'	=> '',
			'after_title'	=> ''
		) );

		register_sidebar( array(
			'name'			=> 'Hot Topics',
			'id'			=> 'hot-topics',
			'description'	=> __('Place the hot topics widget in this spot.'),
			'before_widget'	=> '<div class="widget">',
			'after_widget'	=> '</div>',
			'before_title'	=> '',
			'after_title'	=> ''
		) );

		register_sidebar( array(
			'name'			=> 'Regular Category Sponsor',
			'id'			=> 'category-sponsor',
			'description'	=> __('Upload image through Media page, then enter HTML IMG tag here. Max width: 300px'),
			'before_widget'	=> '<div class="category_sponsor">',
			'after_widget'	=> '</div>',
			'before_title'	=> '',
			'after_title'	=> ''
		) );

		register_sidebar( array(
			'name'			=> 'Special Category Sponsor',
			'id'			=> 'special-category-sponsor',
			'description'	=> __('Upload image through Media page, then enter HTML IMG tag here. Max width: 200px'),
			'before_widget'	=> '<div class="special_category_sponsor">',
			'after_widget'	=> '</div>',
			'before_title'	=> '',
			'after_title'	=> ''
		) );

		register_sidebar( array(
			'name'			=> 'PubExchange',
			'id'			=> 'pubexchange',
			'description'	=> __( 'Place the PubExchange text widget in this spot.' ),
			'before_widget'	=> '<div class="widget pubexchange">',
			'after_widget'	=> '</div>',
			'before_title'	=> '',
			'after_title'	=> ''
		) );

	}

	public function competitor2k14_enqueue_scripts() {
		global $version;
		$version = '20.5';
		// $version = '?v=20.5' keep this for global search/update

		wp_deregister_script( 'jquery' );
		wp_enqueue_script( 'jquery', '//code.jquery.com/jquery-1.8.3.min.js', array(), false, false );
		wp_deregister_script( 'jquery-ui-core' );
		wp_enqueue_script( 'jquery-ui-core', '//code.jquery.com/ui/1.8.24/jquery-ui.min.js', array( 'jquery' ), false, true );

		if ( is_page( 'race-calendar' ) ) {
			wp_deregister_script('jquery-ui-datepicker');
			wp_register_script('jquery-ui-datepicker', '/wp-includes/js/jquery/ui/jquery.ui.datepicker.min.js', array( 'jquery' ), false, false);
			wp_enqueue_script('jquery-ui-datepicker');
			wp_enqueue_style( 'jquery-ui-datepicker-css', '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css');
		}

		if ( is_page_template( 'pace-calculator.php' ) ) {
			wp_register_script( 'pace-calculator', plugins_url() . '/pace-calculator/js/pace-calculator-2.js', array(), false, false );
			wp_enqueue_script( 'pace-calculator' );
			wp_register_script( 'vdot-calculator', get_stylesheet_directory_uri() . '/js/vdot_calc.js', array(), false, false );
			wp_enqueue_script( 'vdot-calculator' );
		}

		wp_register_script( 'competitor-site-scripts', get_bloginfo( 'stylesheet_directory' ) . '/js/competitor.min.js', array( 'jquery' ), $version, true );
		wp_enqueue_script( 'competitor-site-scripts' );

		wp_enqueue_style( 'main-style', get_bloginfo( 'stylesheet_directory' ) . '/style.min.css', false, $version );

		if ( is_page( 'saucony-26-strong' ) ) {
			wp_register_script( 'jquery-ui-accordion', '/wp-includes/js/jquery/ui/jquery.ui.accordion.min.js', array( 'jquery' ), false, true );
			wp_enqueue_script( 'jquery-ui-accordion' );
			wp_register_script( 'responsive-nav', get_stylesheet_directory_uri(). '/js/responsive-nav.min.js', array(), false, false );
			wp_enqueue_script( 'responsive-nav' );
			wp_register_script( 'tss-carousel', get_stylesheet_directory_uri() . '/js/twentysixstrong-carousel.js', array(), false, true );
			wp_enqueue_script( 'tss-carousel' );
			wp_register_style( 'googleFonts', 'http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic' );
			wp_enqueue_style( 'googleFonts' );
			wp_register_style( 'twenty-six-strong', get_bloginfo( 'stylesheet_directory' ) . '/css/style-26-strong.min.css' );
			wp_enqueue_style( 'twenty-six-strong' );
		}

		// if ( is_category( '2016-gift-guide' ) || in_category( '2016-gift-guide' ) || is_page_template( 'gift-guide.php' ) ) {
		// 	wp_enqueue_style( 'gift-guide-style', get_template_directory_uri() . '/css/gift-guide.min.css', array(), filemtime( get_template_directory() . '/css/gift-guide.min.css' ) );
		// }

		if ( is_page_template( 'one-column.php' ) ) {
			wp_enqueue_style( 'one-column-template', get_template_directory_uri() . '/css/one-column-template.min.css', array(), $version );
		}

	}

	public function posts_link_attributes_prev() {
		return 'class="prev-post"';
	}

	public function posts_link_attributes_next() {
		return 'class="next-post"';
	}

	public function per_category_basis( $query ) {

		if ( $query->is_category ) {

			// photo category show 19 posts
			if ( isset( $query->query['category_name'] ) && $query->query['category_name'] == 'photos' ) {
				$query->set( 'posts_per_page', 19 );
				return;
			}

		}

	}

	/**
	 * This includes the thumbnail in our RSS feed
	 */
	public function insertThumbnailRSS( $content ) {
		global $post;

		if ( has_post_thumbnail( $post->ID ) ) {
			$id					= get_post_thumbnail_id( $post->ID );
			$thumbnail_image	= get_post( $id );

			$thumbnail_caption	= !empty( $thumbnail_image ) ? '<p>'. $thumbnail_image->post_excerpt .'</p>' : '';
			$thumbnail_title	= !empty( $thumbnail_image ) ? ' title="'. $thumbnail_image->post_title .'"' : '';

			$image_array	= wp_get_attachment_image_src($id, 'medium');
			$content		= '<figure><img'. $thumbnail_title .' src="'. $image_array[0] .'" />'. $thumbnail_caption .'</figure>'. $content;
		}

		return $content;
	}

	public function strip_title_tags( $title ) {
		return strip_tags( $title );
	}

	/**
	 * Excerpt adjustments
	 */
	public function trim_excerpt( $full_excerpt ) {
		$full_excerpt = strip_tags( $full_excerpt );

		if ( strlen($full_excerpt) > 140 ) {
			$short_excerpt	= substr( $full_excerpt, 0, 140 );
			$words			= explode( ' ', $short_excerpt );
			$trim_excerpt	= -( strlen($words[count( $words ) - 1]) );
			if ( $trim_excerpt < 0 ) {
				return substr($short_excerpt, 0, $trim_excerpt);
			} else {
				return $short_excerpt;
			}
		} else {
			return $full_excerpt;
		}
	}

	/**
	 * Filter gallery styles out and replace with our own
	 */
	public function cgi_media_gallery () {
		return '<div class="gallery">';
	}

	/**
	 * Sets comment cookie lifetime to 3 minutes to improves cache hit rate
	 */
	public function set_lower_comment_cookie_lifetime( $content ) {
		return 180;
	}

	public function skip_gallery( $gallery_html, $attr ) {
		return "<div></div>";
	}

	public function parameter_queryvars( $qvars ) {
		$qvars[] = 'all';

		return $qvars;
	}

	public function my_post_image_html( $html, $post_ID, $post_image_id ) {
		$attachments = get_children( array(
			'post_parent'		=> $post_ID,
			'post_status'		=> 'inherit',
			'post_type'			=> 'attachment',
			'post_mime_type'	=> 'image',
			'orderby'			=> 'menu_order',
			'order'				=> 'ASC'
		) );

		if ( has_post_thumbnail( $post_ID ) ) {
			$html	= get_the_post_thumbnail( $post_ID, 'thumbnail' );
		} elseif ( get_post_meta( $post_ID, 'thumb', true ) ) {
			$title	= get_the_title( $post_ID );
			$html	= '<img src="' . get_post_meta( $post_ID, 'thumb', true ) . '" alt="' . $title . '" />';
		} elseif ( count($attachments) > 0 ) {
			foreach( $attachments as $attachment ) {
				$attachment_id = $attachment->ID;
				break;
			}

			$image_array	= wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
			$html			= "<img src='".$image_array[0]."'/>";
		} else {
			// p75GetThumbnail is from Simple Post Thumbnails plugin. but this whole parent function isn't even in use
			$html	= '<img src="'. p75GetThumbnail( $post_ID, '150', '150', '' ).'" />';
		}

		return $html;
	}

	/**
	 * Filters the content to remove any extra paragraph or break tags
	 * caused by shortcodes.
	 *
	 * @since 1.0.0
	 *
	 * @param string $content  String of HTML content.
	 * @return string $content Amended string of HTML content.
	 */
	public function shortcode_empty_paragraph_fix( $content ) {
		$array = array(
			'<p>['		=> '[',
			']</p>'		=> ']',
			']<br />'	=> ']'
		);
		return strtr( $content, $array );

	}

	public function child_force_category_template( $template ) {

		$video_cat_id	= !empty( self::$technical_theme_options['video_category'] ) ? self::$technical_theme_options['video_category'] : '';
		$cat			= get_query_var('cat');
		$category		= get_category( $cat );

		if ( $category->cat_ID == $video_cat_id || $category->category_parent == $video_cat_id ) {
			$cat_template	= TEMPLATEPATH . '/category-videos.php';
		} else {
			$cat_template	= $template;
		}

		return $cat_template;
	}


	public function video_redirect() {

		if ( is_single() ) {
			$video_cat_id	= !empty( self::$technical_theme_options['video_category'] ) ? self::$technical_theme_options['video_category'] : '';

			if ( in_category( $video_cat_id ) ) {
				include TEMPLATEPATH . '/video-template.php';
				exit;
			}
		}

	}

	/**
	 * Redirect single posts in category "Lists" to a specific template
	 * - For file template file organization and code clean up
	 */
	public function redirect_single_lists_posts( $single_template ) {

		if ( in_category( 'lists' ) ) {
			$single_template = get_template_directory() .'/single-cat-lists.php';
		}

		return $single_template;
	}

	/**
	 * Removes responsive images from Feeds
	 * srcset was causing issues with validation
	 */
	public function no_responsive_image_feeds() {
		add_filter( 'max_srcset_image_width', function() {
			return 1;
		} );
	}

	/**
	 * Remove the WP version param from any scripts
	 * @param  string $src full source path
	 * @return string      (un)altered source path
	 */
	public function remove_wp_ver_css_js( $src ) {
		if ( !is_user_logged_in() && strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) ) {
			global $version;
			// Little easter egg for our ex-sysadmin
			$src	= remove_query_arg( 'ver', $src );
			$src	= add_query_arg( 'bbreve', $version, $src );
		}
		return $src;
	}

	/**
	 * Code that should be printed into the before body hook
	 */
	public function body_end_code() {
		$before_closing_body	= isset( self::$technical_theme_options['before_closing_body'] ) ? self::$technical_theme_options['before_closing_body'] : '';

		if ( !empty( $before_closing_body ) ) {
			echo $before_closing_body;
		}
	}

	/**
	 * EU Regulation cookie set up
	 */
	public function eu_regulation(){
		$cookie_name		= 'running_eu_regulation';
		$eu_regulation_txt	= self::$editorial_theme_options['eu_regulations_text'];

		if ( !isset($_COOKIE[$cookie_name]) && !empty($eu_regulation_txt) ) { ?>

			<div id="cookie-popup-bar" class="container">
				<a href="#" class="close-cookie-bar"></a>
				<?php echo stripslashes( $eu_regulation_txt ); ?>
			</div>

			<script>
				// Make absolutely sure to display or not display the cookie bar (there might be no recognition due to cache)
				(function() {

					// Check for both localStorage data and cookie
					var cookieStatus	= document.cookie.indexOf( '<?php echo $cookie_name; ?>' ) !== -1 ? true : false;
					var storageStatus	= localStorage.getItem( '<?php echo $cookie_name; ?>' ) ? true : false;

					if ( cookieStatus || storageStatus ) {
						var regulations_bar	= document.getElementById('cookie-popup-bar');

						regulations_bar.style.display	= 'none';
					}
				} )();

			</script>
		<?php }
	}



}
