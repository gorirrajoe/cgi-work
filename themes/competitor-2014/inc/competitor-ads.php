<?php

/**
 * Class holding anything we need to mod for the WP DFP Ads plugin
 */

class Competitor_Ads {

	static $instance = false;

	public function __construct() {

		// front-end hooks
		add_filter( 'wp_dfp_ads_filter', array( $this, '_filter_inarticle' ) );
		add_filter( 'wp_dfp_ads_filter', array( $this, '_filter_inarticle_alt' ) );
		add_filter( 'wp_dfp_ads_keywords', array( $this, '_filter_keywords' ) );

	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self;

		return self::$instance;
	}

	/**
	 * Unset any inarticle ad if there was not enough content for the ad to display
	 *
	 * @param  array $ads Contains all the ads that we are preparing to display
	 * @return array      $ads excluding/inlcuding inarticle ads, depending on the logic happening
	 */
	public function _filter_inarticle( $ads ) {

		if ( is_single() ) {

			global $post;

			$content		= $post->post_content;
			$post_content	= apply_filters( 'the_content', $content );

			// if the ad call is not found inside the content, then pop the inarticle ad out of the $ads
			if ( !strpos( $post_content, 'advert_location_inline' ) ) {

				foreach ( $ads as $key => $ad ) {

					foreach ( $ad['advert-slots'] as $i => $slot ) {

						if ( strpos( $slot, 'inarticle' ) )  { unset($ads[$key]); }

					} // end foreach $ad['slots']

				} // end foreach $ads

			} // end strpos

		} // end is_single()

		return $ads;

	}

	public function _filter_inarticle_alt( $ads ) {

		if ( is_single() ) {

			global $post;

			$content		= $post->post_content;
			$post_content	= apply_filters( 'the_content', $content );

			// Get all inarticle ads (by location)
			/**
			 * Get all inarticle ads (by location)
			 * note: this is different from the rest of our media sites
			 * we get the advert-location taxonomies and then pluck the slug
			 * not 100% sure why this isn't working for Competitor.
			 * Could be the WP version or that it's multisite.
			 * Future Dev, look at Triathlete ads file for reference.
			 */
			$advert_locations	= get_terms( array(
				'taxonomy'	=> 'advert-location',
				// 'fields'	=> 'slugs',
			) );

			$advert_locations	= wp_list_pluck( $advert_locations, 'slug' );

			$inarticle_locations	= array_filter( $advert_locations, function( $value ) {
				if ( substr( $value, 0, strlen('inarticle') ) == 'inarticle' ) {
					return $value;
				}
			} );

			// Reverse the array so we look for the numbered inarticle ads first
			$inarticle_locations	= array_reverse( $inarticle_locations );

			foreach ( $inarticle_locations as $location ) {

				// if the ad call is not found inside the content, then pop the inarticle ad out of the $ads
				if ( !strpos( $post_content, $location )  ) {

					foreach ( $ads as $key => $ad ) {

						foreach ( $ad['advert-slots'] as $i => $slot ) {

							if ( strpos( $slot, $location ) !== false ) { unset($ads[$key]); }

						} // end foreach $ad['slots']

					} // end foreach $ads

				} // end strpos

			}

		} // end is_single()

		return $ads;

	}

	public function _filter_keywords( $suffix ) {

		if ( is_category() ) {

			$category = get_queried_object();

			/**
			 * Every category page should return it's parents ( if exist ), then
			 * itself.
			 */
			if ( !empty( $category->category_parent ) ) {

				// this will already have the suffix with the proper set up,
				// but we need it to have underscored instead of dashes
				// so just do that
				$suffix	= str_replace( '-', '_', $suffix );

			} else {

				// Make ad calls as 'partner_connect' instead of 'partnerconnect'
				// Because AdOps team can't be bothered to target sites individually instead we have to hard code this in...grrrr
				// Guess what? apparently we've been targeting everything with underscores in place of dashes and no body told us. HOORRRAAAY!!!
				// but also, guess what again? there's a few rogue categories they DID use dashes in...so make an exception for those
				// and these have to be specific to our URLs...
				if ( $category->slug == 'partnerconnect' ) {

					$suffix = '/partner_connect';

				} elseif ( in_array( $category->slug, array( '2016-gift-guide' ) ) ) {

					$suffix = '/'. Wp_Dfp_Ads::_sanitize_term( $category->slug );

				} else {

					$suffix = '/'. str_replace( '-', '_', Wp_Dfp_Ads::_sanitize_term( $category->slug ) );

				}

			}

		} elseif ( is_single() ) {

			global $post;

			$categories	= ( $post->post_parent == '0' ? get_the_category( $post->ID ) : get_the_category( $post->post_parent ) );

			$keywords			= array();
			$author				= false;
			$sponsored_content	= false;

			// clear out the $suffix
			$suffix	= '';

			// sort the categories based on our AdOps provided list IF the posts belongs to one of them
			$prioritized_categories	= array(
				'photos',
				'inside-the-magazine',
				'news',
				'injury-prevention',
				'partner-connect',
				'boston-marathon',
				'shoes-and-gear',
				'recovery',
				'new-york-city-marathon',
				'ask-the-experts',
				'olympic-trials',
				'training',
				'cross-training-101',
				'barefoot-running',
				'trail-running',
				'nutrition',
				'beer-and-running',
				'50-best-running-stores',
				'shoe-review',
				'running-injuries',
				'rock-n-roll-marathon-series',
				'olympic-games',
				'features',
				'crossfit',
				'trailrunningshoes',
				'strengthtraining',
				'runroutes',
				'marathontraining',
				'home',
				'halfmarathontraining',
				'gift-guide',
				'crosstrainingforrunners',
				'beginnertraining',
				'5ktraining',
				'10ktraining',
			);

			// If primary category exists, make it a part of the prioritized categories
			if ( function_exists('yoast_get_primary_term_id') ) {
				$primary_cat_ID	= yoast_get_primary_term_id();

				// only do this if there was a primary category
				if ( false !== $primary_cat_ID ) {

					$primary_cat_data	= get_term( $primary_cat_ID );
					$primary_cat_slug	= $primary_cat_data->slug;

					// add if it's not already a part of the prioritized categories
					// gets lowest priority on the list
					if ( !in_array( $primary_cat_slug, $prioritized_categories ) ) {
						$primary_cat_slug_array	= array( $primary_cat_slug );
						$prioritized_categories	= $primary_cat_slug_array + $prioritized_categories;
					}

				}

			}

			if ( in_category( $prioritized_categories ) ) {

				usort( $categories, function( $a, $b ) use( $prioritized_categories ) {
					// If $a category is in the prioritized categories but $b is not, $a should be bumped up
					if ( in_array( $a->slug, $prioritized_categories ) && !in_array( $b->slug, $prioritized_categories ) ) {
						return -1;
					} elseif ( !in_array( $a->slug, $prioritized_categories ) && in_array( $b->slug, $prioritized_categories ) ) {
						// else if vice versa of previous
						return 1;
					} elseif ( in_array( $a->slug, $prioritized_categories ) && in_array( $b->slug, $prioritized_categories ) ) {
						// else if both are in the prioritized categories, figure out which is higher up the list
						$a_key	= array_search( $a->slug, $prioritized_categories );
						$b_key	= array_search( $b->slug, $prioritized_categories );

						 return ( $a_key < $b_key ) ? -1 : 1;
					} else {
						return 0;
					}

				} );

			}

			foreach ( $categories as $category ) {

				$parents = get_category_parents( $category->cat_ID );

				if ( !is_wp_error( $parents ) && !empty( $parents ) ) {

					foreach ( explode( '/', $parents ) as $cat ) {

						$cat = Wp_Dfp_Ads::_sanitize_term( $cat );
						$cat = str_replace( '-', '_', $cat );

						if ( !empty( $cat ) && !in_array( $category->slug, $keywords ) ) {

							$keywords[] = $cat;

							// sponsored needs the author in the ad call
							// and MUST be '/sponsored' even if there are other categories
							if ( 'sponsored' === $category->slug || $sponsored_content == true ) {
								$suffix 			= '/'. $category->slug;
								$author				= true;
								$sponsored_content	= true;
							} elseif ( in_array( $category->slug, array( '2016-gift-guide' ) ) ) {

								$suffix .= '/'. $category->slug;

							} else {
								$cat	= $category->slug;
								$cat	= str_replace( '-', '_', $cat );
								$suffix	.= "/$cat";
							}

						}

					}

					// adding author to keywords for partner connect
					if ( true === $author ) {

						$author_name = get_the_author_meta( 'login', $post->post_author );

						if ( $author_name != ',' && $author_name != '' ) {

							$author_name = Wp_Dfp_Ads::_sanitize_term( $author_name );

							if ( !empty( $author_name ) && !in_array( $author_name, $keywords ) ) {

								$keywords[] = $author_name;
								$suffix .= "/{$author_name}";

							}

						}

						// if we added the author, it means we were in a sponsored post
						// so breakout of the categories, no need to loop through the rest of the categories
						break;

					}

				}

			}

		}

		return $suffix;

	}

}
