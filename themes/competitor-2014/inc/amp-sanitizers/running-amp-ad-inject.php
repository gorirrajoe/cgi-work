<?php

	class Running_AMP_Ad_Injection_Sanitizer extends AMP_Base_Sanitizer {

		public function get_scripts() {
			if ( !$this->did_convert_elements ) {
				return array();
			}

			return array( 'amp-ad' => 'https://cdn.ampproject.org/v0/amp-ad-0.1.js' );
		}

		public function sanitize() {

			if ( class_exists('Wp_Dfp_Ads') ) {

				/**
				 * Since our special ad class exists,
				 * we'll be adding two ads
				 * one at the bottom of the article body
				 * and one inarticle IF the article is longer than 8 paragraphs
				 */

				$adKw =  Wp_Dfp_Ads::singleton()->generate_dfp_keywords();

				$body = $this->get_body_node();


				/**
				 * BOTTOM AD
				 */
				// Build our amp-ad tag
				$ad_node_bottom = AMP_DOM_Utils::create_node( $this->dom, 'amp-ad', array(
					'width'		=> 300,
					'height'	=> 250,
					'type'		=> 'doubleclick',
					'data-slot'	=> "$adKw",
				) );

				// Add a placeholder to show while loading
				$fallback_node = AMP_DOM_Utils::create_node( $this->dom, 'amp-img', array(
					'placeholder'	=> 'Loading ad...',
					'layout'		=> 'fill',
					'src'			=> 'https://placehold.it/300x250?text=Loading',
				) );
				$ad_node_bottom->appendChild( $fallback_node );

				// Create our wrapper divs
				$advert_div_bottom	= $this->dom->createElement( 'div', '' );
				$advert_wrap_bottom	= $this->dom->createElement( 'div', '' );

				$advert_wrap_bottom->appendChild( $ad_node_bottom );
				$advert_div_bottom->appendChild( $advert_wrap_bottom );
				$advert_wrap_bottom->setAttribute( 'class', 'advert__wrap' );
				$advert_div_bottom->setAttribute( 'class', 'advert advert_xs_300x250 advert_location_bottom' );

				// Append the ad to the bottom of the article body
				$body->appendChild( $advert_div_bottom );

				// Make sure our ad script is added to the page
				$this->did_convert_elements = true;

				/**
				 * INARTICLE AD
				 */

				// Build our amp-ad tag
				$ad_node_inarticle = AMP_DOM_Utils::create_node( $this->dom, 'amp-ad', array(
					'width'		=> 300,
					'height'	=> 250,
					'type'		=> 'doubleclick',
					'data-slot'	=> "$adKw",
				) );

				// Add a placeholder to show while loading
				$fallback_node = AMP_DOM_Utils::create_node( $this->dom, 'amp-img', array(
					'placeholder'	=> 'Loading ad...',
					'layout'		=> 'fill',
					'src'			=> 'https://placehold.it/300x250?text=Loading',
				) );
				$ad_node_inarticle->appendChild( $fallback_node );

				// Create our wrapper divs
				$advert_div_inarticle	= $this->dom->createElement( 'div', '' );
				$advert_wrap_inarticle	= $this->dom->createElement( 'div', '' );

				$advert_wrap_inarticle->appendChild( $ad_node_inarticle );
				$advert_div_inarticle->appendChild( $advert_wrap_inarticle );
				$advert_wrap_inarticle->setAttribute( 'class', 'advert__wrap' );
				$advert_div_inarticle->setAttribute( 'class', 'advert advert_xs_300x250 advert_location_inline' );

				// Only add the ad if there is more than 8 paragraphs
				// Add it at the middle of the article
				$p_nodes = $body->getElementsByTagName( 'p' );

				if ( $p_nodes->length > 8 ) {

					$middle_p	= floor( $p_nodes->length / 2 );

					$p_nodes->item( $middle_p )->parentNode->insertBefore( $advert_div_inarticle, $p_nodes->item( $middle_p ) );
				}

			}

		}

	}
