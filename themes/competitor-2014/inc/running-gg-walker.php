<?php
/**
 * Gift Guide Bottom Nav Walker
 */
class GG_Walker_Nav_Menu extends Walker_Nav_Menu {
	private $count = 1;

	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$classes = array( 'sub-menu' );

		$class_names = join( ' ', apply_filters( 'nav_menu_submenu_css_class', $classes, $args, $depth ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

		$output .= "<ul $class_names>";
	}

	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= '</ul>';
	}

	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

		$classes	= empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[]	= 'menu-item-' . $item->ID;

		$args			= apply_filters( 'nav_menu_item_args', $args, $item, $depth );

		$class_names	= join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
		$class_names	= $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

		$atts = array();
		$atts['title']	= ! empty( $item->attr_title ) ? $item->attr_title : '';
		$atts['target']	= ! empty( $item->target )     ? $item->target     : '';
		$atts['rel']	= ! empty( $item->xfn )        ? $item->xfn        : '';
		$atts['href']	= ! empty( $item->url )        ? $item->url        : '';

		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

		$attributes = '';
		foreach ( $atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}

		$title = apply_filters( 'the_title', $item->title, $item->ID );

		$title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before . $title . $args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;


        $data = ( $depth == 0 && $args->walker->has_children == 1 ) ? ' data-dropdown="closed" ' : '';

		if( $this->count < 6 ) {

			$output .= '<li' . $data . $id . $class_names .'>';
				$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );


		} elseif( $this->count == 6 ) {

			$menu_name_exploded	= explode( '-submenu', $args->menu );
			$menu_name			= str_replace( ' ', '-', $menu_name_exploded[0] );

			$output .= '<li><a href="' . site_url( '/category/' . $menu_name ) . '">More</a></li>';

		} else {

			$output .= '';

		}

		if( $depth == 0 ) {

			$this->count++;

		}
	}

	public function end_el( &$output, $item, $depth = 0, $args = array() ) {

	}
}
