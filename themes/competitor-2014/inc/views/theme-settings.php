<div class="wrap cmb2-options-page <?php echo $this->options_page; ?>">

	<h1>Running's <?php echo get_admin_page_title(); ?></h1>

	<div id="poststuff">

		<div id="cmb2-nav">

			<div class="nav-tab-wrapper">
				<?php
				foreach ( $tabs as $option_group ) {
					printf( '<a class="nav-tab" href="#%s">%s</a>', $option_group['slug'], $option_group['title'] );
				}
				?>
			</div>

		</div>

		<div class="meta-box-sortables ui-sortable">

			<?php if ( function_exists( 'cmb2_metabox_form' ) ): ?>

				<?php foreach ( $tabs as $option_group ): ?>

					<div class="postbox" id="<?php echo $option_group['slug']; ?>" style="display: block;">

						<button type="button" class="handlediv button-link" aria-expanded="true">
							<span class="toggle-indicator" aria-hidden="true"></span>
						</button>

						<h3 class="hndle"><?php echo $option_group['title']; ?></h3>

						<div class="inside">
							<?php cmb2_metabox_form( $option_group['slug'], $active_page ); ?>
						</div>

					</div>

				<?php endforeach; ?>

			<?php endif; ?>

		</div> <!-- END .meta-box-sortables.ui-sortable -->

	</div>

</div>
