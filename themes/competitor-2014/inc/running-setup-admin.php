<?php

class Running_Setup_Admin {

	public static $instance = false;

	public function __construct() {

		$this->_setup_hooks();

	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( !self::$instance )
			self::$instance = new self;

		return self::$instance;

	}

	/**
	 * Setup WP Hooks
	 *
	 * Defines all the WordPress actions and filters used by this theme.
	 */
	protected function _setup_hooks() {

		add_action( 'admin_menu', array( $this, 'my_remove_menu_pages' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'add_admin_scripts' ), 10, 1 );
		add_action( 'admin_footer-post.php', array( $this, 'excerpt_length_counter' ) );
		add_action( 'admin_footer-post-new.php', array( $this, 'excerpt_length_counter' ) );

		// user capabilities
		add_filter( 'map_meta_cap', array( $this, 'mc_admin_users_caps' ), 1, 4 );
		remove_all_filters( 'enable_edit_any_user_configuration' );
		add_filter( 'enable_edit_any_user_configuration', '__return_true');
		add_filter( 'admin_head', array( $this, 'mc_edit_permission_check' ), 1, 4 );

		add_filter( 'user_contactmethods', array( $this, 'update_contactmethods' ), 10, 1 );

		// admin columns
		add_filter( 'manage_posts_columns' , array( $this, 'add_sticky_column' ) );
		add_action( 'manage_posts_custom_column' , array( $this, 'custom_featured_column' ), 10, 2 );
		add_filter( 'manage_media_columns', array( $this, 'cgi_add_filesize_column' ) );
		add_action( 'manage_media_custom_column', array( $this, 'cgi_manage_filesize_column' ), 10, 2 );


	}

	public function my_remove_menu_pages() {
		remove_submenu_page('tools.php', 'ms-delete-site.php');
	}

	public function add_admin_scripts( $hook ) {
		global $post;

		if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
			if ( 'shoe' === $post->post_type ) {
				wp_register_script( 'my-uploader', get_bloginfo('template_directory') .'/js/uploader.js', array( 'jquery', 'media-upload', 'thickbox' ) );
				wp_enqueue_script( 'my-uploader' );
			}
		}

	}

	/**
	 * Excerpt Length Counter
	 * Add a counter to the excerpt field
	 * updated during 2017 SPRING CLEANING
	 */
	public function excerpt_length_counter() { ?>
		<script>
			jQuery( document ).ready( function($) {
				$('#postexcerpt .handlediv').after( '<div style="position:absolute;top:12px;right:34px;color:#666;"><small>Note: Excerpts are cut at 140 chars.  Current excerpt length: </small><span id="excerpt_counter"></span><span style="font-weight:bold; padding-left:7px;">/ 140</span><small><span style="font-weight:bold; padding-left:7px;">character(s).</span></small></div>' );
				$('span#excerpt_counter').text( jQuery( "#excerpt" ).val().length );
				$('#excerpt').keyup( function() {
					if ( $( this ).val().length > 140 ) {
						$( this ).val( $( this ).val().substr( 0, 140 ) );
					}
					$('span#excerpt_counter').text( $('#excerpt').val().length );
				} );
			} );
		</script>
		<?php
	}

	public function mc_admin_users_caps( $caps, $cap, $user_id, $args ) {

		foreach ( $caps as $key => $capability ) {

			if ( $capability != 'do_not_allow' )
				continue;

			switch ( $cap ) {
				case 'edit_user':
				case 'edit_users':
					$caps[$key] = 'edit_users';
					break;
				case 'delete_user':
				case 'delete_users':
					$caps[$key] = 'delete_users';
					break;
				case 'create_users':
					$caps[$key] = $cap;
					break;
			}
		}

		return $caps;
	}

	/**
	 * Checks that both the editing user and the user being edited are
	 * members of the blog and prevents the super admin being edited.
	 */
	public function mc_edit_permission_check() {
		global $current_user, $profileuser;

		$screen = get_current_screen();

		$current_user = wp_get_current_user();

		if ( ! is_super_admin( $current_user->ID ) && in_array( $screen->base, array( 'user-edit', 'user-edit-network' ) ) ) {
			// editing a user profile
			if ( is_super_admin( $profileuser->ID ) ) { // trying to edit a superadmin while less than a superadmin
				wp_die( __( 'You do not have permission to edit this user.' ) );
			} elseif ( ! ( is_user_member_of_blog( $profileuser->ID, get_current_blog_id() ) && is_user_member_of_blog( $current_user->ID, get_current_blog_id() ) )) { // editing user and edited user aren't members of the same blog
				wp_die( __( 'You do not have permission to edit this user.' ) );
			}
		}

	}

	/**
	 * Update User Contact Methods
	 */
	public function update_contactmethods( $contactmethods ) {

		// Add fields
		$contactmethods['twitter'] = 'Twitter handle (no @)';
		$contactmethods['facebook'] = 'Facebook username (no url)';
		$contactmethods['googleplus'] = 'Google+ id (full url)';

		// Remove fields
		unset( $contactmethods['aim'] );
		unset( $contactmethods['jabber'] );
		unset( $contactmethods['yim'] );

		return $contactmethods;

	}

	/**
	 * function: add featured column to 'all posts' admin page
	 */
	public function add_sticky_column( $columns ) {
		return array_merge( $columns, array( 'featured' => __( 'Featured?' ) ) );
	}

	/**
	 * function: add content to column to 'all posts' admin page to see whether featured or not
	 */
	public function custom_featured_column( $column, $post_id ) {
		switch ( $column ) {
			case 'featured':
				$is_featured = get_post_meta( $post_id, '_featured_post', true );
				if ( $is_featured == 'featured' ) {
					echo 'featured';
				} elseif ( $is_featured == 'not_featured' ) {
					echo 'not featured';
				} else {
					echo '---';
				}
			break;
		}
	}

	/**
	 * Filter the Media list table columns to add a File Size column.
	 *
	 * @param	array	$columns	Existing array of columns.
	 *
	 * @return	array				Array with columns including filesize.
	 */
	public function cgi_add_filesize_column( $columns ) {
		$columns['filesize'] = 'File Size';

		return $columns;
	}

	/**
	 * Handle File Size column display
	 *
	 * @param	string	$column_name	Name of the custom column.
	 * @param	int		$post_ID		Current Attachment ID.
	 */
	public function cgi_manage_filesize_column( $column_name, $post_ID ) {
		// if this is not our column, bail. we don't want it interfering other places
		if ( 'filesize' !== $column_name ) {
			return;
		}

		$bytes = filesize( get_attached_file( $post_ID ) );

		echo size_format( $bytes, 2 );
	}





}
