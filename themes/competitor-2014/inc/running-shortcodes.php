<?php
/**
 * Running Shortcodes
 *
 * Adds custom shortcodes to the WordPress API.
 *
 * NOTICE
 *
 * The Facebook Instant Articles plugin requires the following rules be added to
 * the custom ruleset. Instant Articles > Publishing Settings >
 * Custom transformer rules.
 *
 * <code>
 * {
 *   "rules": [
 *     {
 *       "class": "PassThroughRule",
 *       "selector": "figure.op-interactive"
 *     },
 *     {
 *       "class": "PassThroughRule",
 *       "selector": "figure.op-slideshow"
 *     },
 *     {
 *       "class": "PassThroughRule",
 *       "selector": "figure.op-ad"
 *     },
 *     {
 *       "class": "SocialEmbedRule",
 *       "selector": "iframe",
 *       "properties": {
 *         "socialembed.url": {
 *           "type": "string",
 *           "selector": "iframe",
 *           "attribute": "src"
 *         },
 *         "socialembed.height": {
 *           "type": "int",
 *           "selector": "iframe",
 *           "attribute": "height"
 *         },
 *         "socialembed.width": {
 *           "type": "int",
 *           "selector": "iframe",
 *           "attribute": "width"
 *         },
 *         "socialembed.iframe": {
 *           "type": "children",
 *           "selector": "iframe"
 *         },
 *         "socialembed.caption": {
 *           "type": "element",
 *           "selector": "figcaption"
 *         }
 *       }
 *     }
 *   ]
 * }
 * </code>
 */
class Running_Shortcodes {

	public static $instance = false;

	public function __construct() {
		$this->_add_actions();
	}

	/**
	 * Facebook Embed
	 *
	 * Embeds a the given facebook video in the current post.
	 *
	 * Example:
	 * [facebook url="https://www.facebook.com/runningMagazine/videos/10154150108373656/" /]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function facebook_embed( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'url'	=> '',
				'type'	=> 'video'
			),
			$atts
		);

		if ( empty( $a['url'] ) ) {
			return;
		}

		if ( defined( 'INSTANT_ARTICLES_SLUG' ) && is_feed( INSTANT_ARTICLES_SLUG ) ) {
			if ( $a['type'] == 'video' ) {
				$output = sprintf(
					'<figure class="op-interactive">
						<iframe>
							<script>(function(d, s, id) {
								var js, fjs = d.getElementsByTagName(s)[0];
								if (d.getElementById(id)) return;
								js = d.createElement(s); js.id = id;
								js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&amp;version=v2.5";
								fjs.parentNode.insertBefore(js, fjs);
							}(document, "script", "facebook-jssdk"));</script>
							<div id="fb-root"></div>
							<div class="fb-video" data-href="%s" data-allowfullscreen="true"></div>
						</iframe>
					</figure>',
					$a['url']
				);
			} elseif ( $a['type'] == 'post' ) {
				$output = sprintf(
					'<figure class="op-interactive">
						<iframe>
							<script>(function(d, s, id) {
								var js, fjs = d.getElementsByTagName(s)[0];
								if (d.getElementById(id)) return;
								js = d.createElement(s); js.id = id;
								js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&amp;version=v2.5";
								fjs.parentNode.insertBefore(js, fjs);
							}(document, "script", "facebook-jssdk"));</script>
							<div id="fb-root"></div>
							<div class="fb-post" data-href="%s"></div>
						</iframe>
					</figure>',
					$a['url']
				);
			}

		} else {
			if ( $a['type'] == 'video' ) {
				$output = sprintf(
					'<div id="fb-root"></div>
					<script>(function(d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) return;
						js = d.createElement(s); js.id = id;
						js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&amp;version=v2.5";
						fjs.parentNode.insertBefore(js, fjs);
					}(document, "script", "facebook-jssdk"));</script>
					<div class="fb-video" data-href="%s" data-allowfullscreen="true"></div>',
					$a['url']
				);
			} elseif ( $a['type'] == 'post' ) {
				$encoded_url = urlencode( $a['url'] );
				$output = sprintf(
					'<iframe src="https://www.facebook.com/plugins/post.php?href=%s&width=500" width="500" height="754" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>',
					$encoded_url
				);
			}

		}

		return $output;
	}



	/**
	 * WordPress Gallery Embed
	 *
	 * Overwrites the default WordPress gallery shortcode.
	 *
	 * Example:
	 * [gallery ids="" include="" exclude="" order="" orderby="" /]
	 *
	 * @link https://codex.wordpress.org/Gallery_Shortcode
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function gallery_embed( $attr = array(), $content = null ) {

		global $post;

		// check $post exists
		if ( empty( $post ) ) {
			return;
		}

		if ( ! empty( $attr['ids'] ) ) {

			// 'ids' is explicitly ordered, unless you specify otherwise.
			if ( empty( $attr['orderby'] ) ) {
				$attr['orderby'] = 'post__in';
			}

			$attr['include'] = $attr['ids'];

		} elseif( !empty( $attr['images'] ) ) {
			if ( empty( $attr['orderby'] ) ) {
				$attr['orderby'] = 'post__in';
			}
			$attr['include'] = $attr['images'];
		}

		static $instance = 0;
		$instance++;

		// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
		if ( isset( $attr['orderby'] ) ) {
			$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
			if ( !$attr['orderby'] )
				unset( $attr['orderby'] );
		}

		// make sure the ID is set up
		$post_ID	= !empty( $post->ID ) ? $post->ID : '';

		extract( shortcode_atts( array(
			'order'      => 'ASC',
			'orderby'    => 'menu_order ID',
			'id'         => $post_ID,
			'itemtag'    => 'dl',
			'icontag'    => 'dt',
			'captiontag' => 'dd',
			'columns'    => 3,
			'size'       => 'large',
			'include'    => '',
			'exclude'    => ''
		), $attr ) );

		$id = intval( $id );

		if ( 'RAND' == $order ) {
			$orderby = 'none';
		}

		if ( !empty($attr['include']) ) {

			$attachments = array();

			$params = array(
				'post__in'			=> explode( ',', $attr['include'] ),
				'post_status'		=> 'inherit',
				'post_type'			=> 'attachment',
				'post_mime_type'	=> 'image',
				// 'order'			=> $attr['order'],
				'orderby'			=> $attr['orderby'],
				'posts_per_page'	=> -1
			);

			$_attachments = new WP_Query( $params );

			if ( $_attachments->have_posts() ) : while ( $_attachments->have_posts() ) : $_attachments->the_post();

				$attachments[$post->ID] = $post;

			endwhile; endif;

			wp_reset_postdata();

		} elseif ( !empty($attr['exclude']) ) {

			$attachments	= get_children( array(
				'post_parent'		=> $id,
				'exclude'			=> $attr['exclude'],
				'post_status'		=> 'inherit',
				'post_type'			=> 'attachment',
				'post_mime_type'	=> 'image',
				// 'order'			=> $attr['order'],
				'orderby'			=> $attr['orderby']
			) );

		} else {
			$attachments	= get_children( array(
				'post_parent'		=> $id,
				'post_status'		=> 'inherit',
				'post_type'			=> 'attachment',
				'post_mime_type'	=> 'image',
				// 'order'			=> $attr['order'],
				'orderby'			=> $attr['orderby']
			) );
		}


		//Non FBIA feed
		if ( is_feed() && ( !defined( 'INSTANT_ARTICLES_SLUG' ) || !is_feed( INSTANT_ARTICLES_SLUG ) ) ) {

			$output = "\n";

			foreach ( $attachments as $att_id => $attachment ) {
				$output .= wp_get_attachment_link( $att_id, $size, true ) . "\n";
			}

			return $output;

		}

		$output				= '';
		$gallery_type		= "Image Gallery";
		$backToStartLink	= "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];

		ob_start(); ?>

			<script src="<?php echo get_bloginfo('template_directory') .'/js/carousel-min.js?'; ?>"></script>
			<script type="text/javascript">
				jQuery(document).ready(function($) {

					$( '.fancybox' ).fancybox({
						preload: true,
						fullScreen: false,
						css: {
							margin : [20, 60, 20, 60],
						},

					});

					var $main_carousel = $('.marquee-<?php echo $post_ID; ?> .slider')
					.owlCarousel({
						items: 1,
						itemsCustom: [[0,1]],
						autoHeight: true,
						pagination: false,
						afterAction: function(el){
							$counter		= el.parents('.gallery-slider').find('.counter-text');
							current_slide	= this.currentItem + 1;
							$counter.html( current_slide + ' of ' + this.itemsAmount );
						}
					})
					.data('owlCarousel');

					$('.p-next').bind('touchstart click', function(e) {
						e.preventDefault();
						$main_carousel.next();

						if ( typeof( _gaq ) !== 'undefined' ) {
							_gaq.push(['_trackEvent', '"<?php echo $gallery_type; ?>" Carousel', 'Next', '<?php echo $gallery_type; ?> Carousel Next Button']);
						}

						<?php echo !is_mobile() ? "googletag.pubads().refresh();\n" : ''; ?>
					});
					$('.p-next-img').on('click', function(e) {
						e.preventDefault();
						$main_carousel.next();

						if ( typeof( _gaq ) !== 'undefined' ) {
							_gaq.push(['_trackEvent', '<?php echo $gallery_type; ?> Carousel', 'Next', '<?php echo $gallery_type; ?> Carousel Next Image']);
						}

						<?php echo !is_mobile() ? "googletag.pubads().refresh();\n" : ''; ?>
					});
					$('.p-prev').bind('touchstart click', function(e) {
						e.preventDefault();
						$main_carousel.prev();

						if ( typeof( _gaq ) !== 'undefined' ) {
							_gaq.push(['_trackEvent', '<?php echo $gallery_type; ?> Carousel', 'Previous', '<?php echo $gallery_type; ?> Carousel Previous Button']);
						}

						<?php echo !is_mobile() ? "googletag.pubads().refresh();\n" : ''; ?>
					});

				});
			</script>

			<div class="gallery-slider">

				<div class="gallery_hdr">
					<div class="gallery_hdr_left"><span class="icon-picture"></span>Photo Gallery</div>
					<div class="gallery_hdr_right">
						<div class="carousel-nav">
							<a href="#" class="p-prev carousel-prev"></a>
							<label class="counter-text">1 of {count}</label>
							<a href="#" class="p-next carousel-next"></a>
							<div class="backtostart">
								<a href="<?php echo $backToStartLink; ?>" title="Back To Start" onClick="_gaq.push(['_trackEvent', 'Image Gallery Carousel', 'BackToStart', 'Back To Start']);">Back to Start</a>
							</div>
						</div>
					</div>
				</div>

				<div class="marquee marquee-<?php echo $post_ID; ?>">
					<ul class="slider" id="post-slider-secondary">
						<?php
							foreach ( $attachments as $attachment ) {

								$desc = $attachment->post_content;

								$image_array =  wp_get_attachment_image_src( $attachment->ID, 'gallery-carousel-2');
								//changed from htmlentities to htmlspecialchars
								$link_text = '
									<div class="viewLargerImage">
										<a class="fancybox" data-fancybox="group" rel="gallery" href="' . $image_array[0] . '" title="'.htmlspecialchars($desc, ENT_QUOTES).'"';
											$link_text .= ' onClick="_gaq.push([\'_trackEvent\', \'Image Gallery Carousel\', \'Enlarge\', \'View Larger Image\']);"';
										$link_text .= '>View Larger Image</a>
									</div>
								';

								$is_mobile = is_mobile();
								if ( $is_mobile ) {
									$image = wp_get_attachment_image_src($attachment->ID, 'medium');
								} else {
									$image = $image_array;
								}

								$image			= '<img src="'.$image[0].'" class="cover secondary">';
								$title			= $attachment->post_title;
								$attachment_url	= get_attachment_link($attachment->ID);

								echo '
									<li class="slide">
										'.$link_text.'
										<a href="#" class="p-next-img">'.$image.'</a>
										<div class="slide_txt">
											<p class="slide_hdr">'.$title.'</p>
											<p class="slide_desc">'.$desc.'</p>
										</div>
									</li>
								';
							}

							// most recent 4 posts categorized as 'photos' and tagged as whatever the post is tagged as
							$post_tags					= get_the_tags();
							$technical_theme_options	= get_option( 'cgi_media_technical_options' );

							if ( $technical_theme_options['gallery_category'] ) {
								$photo_cat_id	= explode( ',', $technical_theme_options['gallery_category'] );
							}

							if ( $post_tags ) {


								$tag_ids	= wp_list_pluck( $post_tags, 'term_id' );

								$args = array(
									'post__not_in'				=> array( $post_ID ),
									'posts_per_page'			=> 4,
									'category__and'				=> $photo_cat_id,
									'tag__in'					=> $tag_ids,
									'no_found_rows'				=> true,
									'update_post_meta_cache'	=> false,
									'ignore_sticky_posts'		=> true,
								);

								$related_galleries  = new WP_Query( $args );

								$count = 1;
								if ( $related_galleries->have_posts() ) :
									echo '<li class="slide related_galleries">
										<h3>Related Galleries</h3>';

									while ( $related_galleries->have_posts() ) : $related_galleries->the_post();

										$permalink = get_permalink();

										if ( has_post_thumbnail() ) {
											$post_thumb	= get_the_post_thumbnail( get_the_ID(), 'home-big-marquee' );
										}
										if ( $related_galleries->current_post == 0 || $related_galleries->current_post == 2 ) {
											echo '<div class="row">
												<div class="photo_col col_odd">
													<a href="'.$permalink.'" onClick="_gaq.push([\'_trackEvent\', \'Related Galleries\', \'Click\', \''.$permalink.'\']);">'.$post_thumb.'</a>
													<div class="more_link white_bg module">
														<a href="'.$permalink.'" onClick="_gaq.push([\'_trackEvent\', \'Related Galleries\', \'Click\', \''.$permalink.'\']);">'.get_the_title().'</a>
														<a href="'.$permalink.'" onClick="_gaq.push([\'_trackEvent\', \'Related Galleries\', \'Click\', \''.$permalink.'\']);"><span class="icon-right-open"></span></a>
													</div>
												</div>';
										} else {
											echo '
												<div class="photo_col col_even">
													<a href="'.$permalink.'" onClick="_gaq.push([\'_trackEvent\', \'Related Galleries\', \'Click\', \''.$permalink.'\']);">'.$post_thumb.'</a>
													<div class="more_link white_bg module">
														<a href="'.$permalink.'" onClick="_gaq.push([\'_trackEvent\', \'Related Galleries\', \'Click\', \''.$permalink.'\']);">'.get_the_title().'</a>
														<a href="'.$permalink.'" onClick="_gaq.push([\'_trackEvent\', \'Related Galleries\', \'Click\', \''.$permalink.'\']);"><span class="icon-right-open"></span></a>
													</div>
												</div>
											</div>';
										}
										$count	= $related_galleries->current_post;

									endwhile;

									if ( $count == 2 || $count == 4 ) {
										echo '</div>';
									}

									echo '</li>';

								endif;

								wp_reset_postdata();
							}

							// right here we could put a replay slide with any other additional options
							$editorial_theme_options	= get_option( 'cgi_media_editorial_options' );
							$gallery_last_slide			= $editorial_theme_options['gallery_last_slide'];

							if ( $gallery_last_slide ) {
								$gallery_last_slide_url = $editorial_theme_options['gallery_last_slide_url'];
								echo '<li class="slide"><a href="'.$gallery_last_slide_url.'"><img src="'.$gallery_last_slide.'" /></a></li>';
							}
						?>
					</ul>
				</div>

				<div class="more_galleries"><a href="<?php echo site_url(); ?>/category/photos/">More Galleries</a></div>
			</div>

			<?php
		$output	.= ob_get_clean();

		// add ad right after gallery if gallery displayed has text after it
		if ( is_single() && in_category('photos') ) {
			$test_content	= wpautop( get_the_content() );
			$test_content	= preg_replace( '/(<p>)\s*\[gallery/', '[gallery', $test_content );
			$test_content	= preg_replace( '/(]<\/p>)/', ']', $test_content );

			if ( strpos( $test_content, '<p>' ) ) {

				$gallery_ad	= class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('bottom-inarticle') : '';
				$gallery_ad	= $gallery_ad != '' ? sprintf( '<div class="hypnotoad_unit_container"><div class="hypnotoad_unit">%s</div></div>', $gallery_ad ) : '';

				$output	.= $gallery_ad;
			}

		}


		return $output;

	}

	/**
	 * Instagram Embed
	 *
	 * Adds the given instagram photo to the curent post.
	 *
	 * Example:
	 * [instagram url="https://www.instagram.com/p/BEW239AmoHh/" hidecaption="{true|false}" align="{left|center|right}"]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function instagram_embed( $atts = array(), $content = null ) {

		$a = shortcode_atts(
			array(
				'url' => '',
				'hidecaption' => '',
				'align' => ''
			),
			$atts
		);

		if ( empty( $a['url'] ) ) {
			return;
		}

		$align = ( $a['align'] == '' ? '' : 'align'. $a['align'] );

		$hidecaption = ( $a['hidecaption'] == 'true' ? 'true' : '' );

		$json		= file_get_contents( 'https://api.instagram.com/oembed?url='. $a['url'] .'&hidecaption='. $hidecaption  );
		$response	= ( ! empty($json) ? json_decode( $json ) : '' );

		if ( defined( 'INSTANT_ARTICLES_SLUG' ) && is_feed( INSTANT_ARTICLES_SLUG ) ) {

			return sprintf(
				'<figure class="op-interactive">
					<iframe>%s</iframe>
				</figure>',
				$response->html
			);

		} else {

			return sprintf(
				'<div class="instagram_embed %s">%s</div>',
				$align,
				$response->html
			);

		}

	}


	/**
	 * Embed Twitter
	 *
	 * Adds the given tweet to the current post.
	 *
	 * Example:
	 * [twitter url="https://twitter.com/running/status/724731422385164288" align="{left|center|right}"]
	 */
	public function twitter_embed( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'url' => '',
				'align' => 'center'
			),
			$atts
		);

		if ( empty( $a['url'] ) ) {
			return;
		}

		$align = ( ! empty( $a['align'] ) ? $a['align'] : '' );
		$divalign = ( ! empty( $align ) ? "twitter_{$align}" : '' );

		$json		= file_get_contents( 'https://api.twitter.com/1/statuses/oembed.json?url=' . $a['url'] .'&align='. $align );
		$response	= json_decode( $json );

		if ( defined( 'INSTANT_ARTICLES_SLUG' ) && is_feed( INSTANT_ARTICLES_SLUG ) ) {

			return sprintf(
				'<figure class="op-interactive"><iframe>%s</iframe></figure>',
				$response->html
			);

		} else {

			return sprintf(
				'<div class="twitter_embed %s">
					%s
				</div>',
				$divalign,
				$response->html
			);

		}

	}

	/**
	 * YouTube Embed
	 *
	 * Embeds the given YouTube video in the current post.
	 *
	 * Example:
	 * [youtube id="GjUXUN-F9Pg" /]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function youtube_embed( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'id' => ''
			),
			$atts
		);

		if ( empty( $a['id'] ) ) {
			return;
		}

		return sprintf(
			'<div class="video-container shortcode">
				<iframe width="1280" height="720" src="https://www.youtube.com/embed/%s?rel=0" frameborder="0" allowfullscreen></iframe>
			</div>',
			$a['id']
		);

	}

	/**
	 * Visual Composer Image
	 *
	 * Embeds the given image ID into the current post.
	 *
	 * Example:
	 * [vc_single_image border_color="grey" img_link_target="_self" img_size="full" image="389121"]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function vc_image( $atts = array() ) {
		$a = shortcode_atts(
			array(
				'image'				=> '',
				'img_link_target'	=> '_self',
				'img_size'			=> 'full',
				'alignment'			=> ''
			),
			$atts
		);

		if( empty( $a['alignment'] ) ) {
			$alignclass = '';
		} else {
			$alignclass = 'align'. $a['alignment'];
		}
		if ( empty( $a['image'] ) ) {
			return;
		}

		$content = apply_filters( 'the_content', wp_get_attachment_image( $a['image'], $a['img_size'], '', array( 'class' => $alignclass ) ) );

		return $content;
	}

	/**
	 * Visual Composer Grid Gallery for Longform posts
	 *
	 * Embeds a Bootstrap column in the current post.
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function vc_images_grid( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'ids' 				=> '',
				'slides_per_view'	=> ''
			),
			$atts
		);

		$columns	= intval( $a['slides_per_view'] );
		$image_ids	= explode( ',', $a['ids'] );
		$images		= '';
		$content	= '';

		// if count value isn't set or there are no image ids, ignore shortcode. do not pass GO, do not collect $200
		if ( $columns < 1 || empty($image_ids) ) {
			return;
		}


		switch ( $columns ) {
			case 2:
				$class = 'col-xs-12 col-md-6 grid-exempt';
				break;
			case 3:
				$class = 'col-xs-12 col-md-4 grid-exempt';
				break;
			case 4:
				$class = 'col-xs-6 col-md-3 grid-exempt';
				break;
			default:
				$class = 'col-xs-12 col-md-6 grid-exempt';
				break;
		}

		foreach ( $image_ids as $image_id ){
			$images	.= '<div class="'. $class .'">'. wp_get_attachment_image( $image_id, 'full' ) .'</div>';
		}

		return sprintf('<div class="row">%s</div>', $images );
	}

	/**
	 * Visual Composer Dummy Shortcode
	 *
	 * Dummy shortcode to replace the VC shortcodes with their contents.
	 *
	 * Examples:
	 * [vc_column_text]Wrapped Text[/vc_column_text]
	 * [vc_column]Wrapped Text[/vc_column]
	 * [vc_row]Wrapped Text[/vc_row]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function vc_plain_return( $atts = array(), $content = null ) {

		if ( empty( $content ) ) {
			return;
		}

		$content = do_shortcode($content);

		// $content = apply_filters( 'the_content', $content );

		return $content;
	}

	/**
	 * Visual Composer - No Return
	 *
	 * ignores the particular vc* shortcode
	 */
	public function vc_no_return( $atts = array() ) {
		return;
	}


	/**
	 * Visual Composer Google Font Usage
	 *
	 * parses through the vc code to style the heading properly
	 * editor will need to embed the google font script code into the post
	 */
	public function vc_heading( $atts = array() ) {
		$a = shortcode_atts(
			array(
				'text'				=> '',
				'font_container'	=> '',
				'google_fonts'		=> '',
			),
			$atts
		);

		$googlefont		= explode( '|', $a['google_fonts'] );
		$font_container	= explode( '|', $a['font_container'] );

		foreach( $googlefont as $font ) {
			$attrib = explode( ':', $font );

			if( $attrib[0] == 'font_family' ) {
				if( strpos( $attrib[1], '%3A' ) !== false ) {
					$exploded_font	= explode( '%3A', $attrib[1] );
					$family			= str_replace( '%20', ' ', $exploded_font[0] );
				}
			}
		}

		// set defaults
		$tag		= 'h3';
		$font_size	= '1.75em;';
		$text_align	= 'center';

		foreach( $font_container as $fontstyle ) {

			$attrib = explode( ':', $fontstyle );

			if( $attrib[0] == 'tag' ) {
				$tag = $attrib[1];
			} elseif( $attrib[0] == 'font_size' ) {
				$font_size = $attrib[1] . 'px';
			} elseif( $attrib[0] == 'text_align' ) {
				$text_align = $attrib[1];
			}

		}

		return sprintf(
			'<%s style="font-size: %s; text-align: %s; font-family: %s; font-style: normal;">
				%s
			</%s>',
			$tag,
			$font_size,
			$text_align,
			$family,
			$a['text'],
			$tag
		);

	}

	/**
	 * Brightcove Embed
	 *
	 * Embeds the given Brightcove video in the current post.
	 *
	 * Example:
	 * [brightcove id="4933566086001" /]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function brightcove_embed( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'id' => ''
			),
			$atts
		);

		if ( empty( $a['id'] ) ) {
			return;
		}

		$technical_theme_options	= get_option( 'cgi_media_technical_options' );

		$adKw		=  class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::singleton()->generate_dfp_keywords() : '';
		ob_start(); ?>

		<div class="video-container">
			<div style="display: block; position: relative; max-width: 100%;">
				<div style="padding-top: 56.25%;">
					<video id="myVideo"
						data-video-id="<?php echo trim( $a['id'] ); ?>"
						data-account="3655502813001"
						data-player="r1oC9M1S"
						data-embed="default"
						class="video-js"
						controls style="width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;">
					</video>
				</div>
			</div>

			<script src="//players.brightcove.net/3655502813001/r1oC9M1S_default/index.min.js"></script>
			<?php if ( !empty( $technical_theme_options['pre_roll'] ) ): ?>
				<link href="//players.brightcove.net/videojs-ima3/2/videojs.ima3.min.css" rel="stylesheet">
				<script src="//players.brightcove.net/videojs-ima3/2/videojs.ima3.min.js"></script>
				<script type="text/javascript">
					var myPlayer = videojs('myVideo');
					myPlayer.ready(function(){
						myPlayer=this;

						myPlayer.ima3({
							serverUrl:"https://pubads.g.doubleclick.net/gampad/ads?sz=720x480&iu=<?php echo $adKw; ?>&ciu_szs&impl=s&gdfp_req=1&env=vp&output=xml_vast2&unviewed_position_start=1&url=[referrer_url]&correlator=[timestamp]",
							timeout:5000,
							prerolltimeout:1000,
							requestMode: 'onload',
							debug:true,
							adTechOrder:["html5", "flash"],
							vpaidMode:"ENABLED"
						});
						myPlayer.one('loadedmetadata', function(){
							// get a reference to the player
							myPlayer = this;

							myPlayer.on("ima3-ad-error",function(event){
								console.log("ima3-ad-error: " + event);
							});

							myPlayer.on("adserror", function(event){
								console.log("adsError Fired: " + event);
							});
						});
					});
				</script>
			<?php endif;?>
		</div>

		<?php

		return ob_get_clean();
	}


	/**
	 * Vimeo Embed
	 *
	 * Embeds the given Vimeo video in the current post.
	 *
	 * Example:
	 * [vimeo id="110962890" /]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function vimeo_embed( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'id' => ''
			),
			$atts
		);

		if ( empty( $a['id'] ) ) {
			return;
		}

		return sprintf(
			'<div class="video-container">
				<iframe src="https://player.vimeo.com/video/%s" width="1280" height="720" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>',
			$a['id']
		);

	}


	/**
	 * GIFS.com Embed
	 *
	 * Embeds the given gif image in the current post.
	 *
	 * Example:
	 * [gifs id="v2645r" /]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function gifs_embed( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'url'	=> '',
				'file'	=> ''
			),
			$atts
		);

		if ( empty( $a['url'] ) ) {
			return;
		}

		if ( defined( 'INSTANT_ARTICLES_SLUG' ) && is_feed( INSTANT_ARTICLES_SLUG ) ) {

			return sprintf(
				'<figure class="op-interactive">
					<iframe src="%s" frameborder="0" scrolling="no" width="480" height="270" style="-webkit-backface-visibility: hidden;-webkit-transform: scale(1);" ></iframe>
				</figure>',
				$a['url']
			);

		} else {

			return sprintf(
				'<div class="video-container">
					<iframe src="%s" frameborder="0" scrolling="no" width="480" height="270" style="-webkit-backface-visibility: hidden;-webkit-transform: scale(1);" ></iframe>
				</div>',
				$a['url']
			);

		}
	}


	/**
	 * Buy Now shortcode
	 *
	 * Embeds the Buy Now button (link) in the current post.
	 *
	 * Example:
	 * [buy-now text="Buy This!" url="http://..." /]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function buynow_func( $atts ) {
		$a = shortcode_atts(array(
			'text'	=> '',
			'url'	=> ''
		), $atts);

		if ( empty( $a['url'] ) || empty( $a['text'] ) ) {
			return;
		}

		return sprintf(
			'<p class="btn_secondary"><a class="btn--buynow" href="%s" target="_blank">%s <span class="icon-right-open"></span></a></p>',
			$a['url'],
			$a['text']
		);

	}


	/**
	 * Related Articles Embed
	 *
	 * Adds a set of related articles to the current post.
	 *
	 * Example:
	 * [related title="Sample Title" tag="Sample Tag" align="{left|center|right}"]
	 *
	 * @todo  convert title of tags in parameter into slugs
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function related_articles_embed( $atts = array(), $content = null ) {

		$post_ID	= get_the_ID();
		$post_tags	= get_the_tags( $post_ID );
		$tags_array	= array();
		$tag_string	= '';

		if ( !empty( $post_tags ) ) {

			foreach ( $post_tags as $tag ) {
				$tags_array[]	.= $tag->slug;
			}

			$tag_string = implode( ',', $tags_array );
		}


		$a = shortcode_atts( array(
			'title'		=> 'Read More',
			'tag'		=> $tag_string,
			'align'		=> 'none',
			'post-ids'	=> '',
			'style'		=> 0,
			'location'	=> ''
		), $atts );

		$align = ( !empty( $a['align'] ) ? 'align'. $a['align'] : '' );

		// define arguments that queries have in common
		$args = array(
			'posts_per_page'			=> 4,
			'post__not_in'				=> array( $post_ID ),
			'no_found_rows'				=> true,
			'update_post_meta_cache'	=> false,
			'ignore_sticky_posts'		=> true,
			'has_password'				=> false
		);

		// Save our data into an array we can loop over several times instead of making all the calls repeatedly
		// A/B testing purposes
		$posts_array = array();

		if ( $a['location'] == 'article-footer' ) {

			// When the widget is placed in the article-footer, it displays articles based on the post's categories
			$cat_IDs	=  wp_get_post_categories( $post_ID, array( 'fields' => 'ids' ) );

			// check if AMP article with the AMP plugin's method
			$is_amp_endpoint = function_exists( 'is_amp_endpoint' ) ? is_amp_endpoint() : false;

			if ( !empty( $cat_IDs ) ) {

				$args['category__in']	= $cat_IDs;
				$args['posts_per_page']	= 3;

				$related_query	= new WP_Query( $args );

				if ( $related_query->have_posts() ):

					// build heading image tag
					$slashes_src	= get_bloginfo( 'stylesheet_directory' ) .'/images/running/running-slashes-blue.svg';

					if ( $is_amp_endpoint ) {
						$slaches_img	= '<amp-img class="slashes slash_blue" src="'. $slashes_src .'" width="38" height="22" layout="fixed"></amp-img>';
					} else {
						$slaches_img	= '<img class="slashes slash_blue" src="'. $slashes_src .'" />';
					}

					ob_start(); ?>

					<section>

						<h3><?php echo $slaches_img; ?>Related Articles</h3>

						<div class="related-articles-category">

							<?php
								while ( $related_query->have_posts() ): $related_query->the_post();
									// Prep thumbnail markup
									if ( !get_the_post_thumbnail_url() ) {
										$thumb_src	= get_bloginfo( 'stylesheet_directory' ) .'/images/running/default-post-thumbnail-marquee.jpg';
										$post_thumb	= $is_amp_endpoint ? '<amp-img src="'. $thumb_src .'" height="340" width="640" layout="responsive"></amp-img>' : '<img src="'. $thumb_src . '" />';
										$arrow	= '<span class="icon-right-open"></span>';
									} elseif ( $is_amp_endpoint ) {
										$post_thumb	= wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );
										$post_thumb	= '<amp-img layout="responsive" src="'. $post_thumb[0] .'" width="'. $post_thumb[1] .'" height="'. $post_thumb[2] .'" alt="'. get_the_title() .'"></amp-img> ';

										// set up the arrow
										$arrow	= '<span class="fa fa-angle-right"></span>';
									} else {
										$post_thumb	= get_the_post_thumbnail_url( null, 'home-big-marquee' );
										$post_thumb	= '<img src="' .$post_thumb .'" alt="'. get_the_title() .'" class="article__thumbnail lazyload">';

										// set up the arrow
										$arrow	= '<span class="icon-right-open"></span>';
									}

									$post_class	= $related_query->current_post == 0 ? ' class="ad_fallback"' : ''; ?>

									<article>
										<?php if ( $related_query->current_post == 0 && !$is_amp_endpoint ) {
											echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('recommended-roll') : '';
										} ?>

										<a href="<?php the_permalink(); ?>"<?php echo $post_class; ?>>
											<div class="more_link white_bg module">
												<?php echo $post_thumb; ?>
												<div class="title"><?php echo $arrow . get_short_title(); ?></div>
											</div>
										</a>

									</article>

								<?php endwhile;
							?>

						</div>

					</section>

					<?php $widget = ob_get_clean();

				endif;

				wp_reset_postdata();

				return $widget;

			}

		} elseif ( !empty( $a['tag'] ) ) {

			$widget = '';

			// add the tag(s) to the arguments
			$args['tag']	= $a['tag'];

			// use IDs instead of by tag if defined
			if ( !empty( $a['post-ids'] ) ) {

				unset( $args['tag'] );
				unset( $args['post__not_in'] );

				$args['post__in'] = explode( ',', $a['post-ids'] );
			}

			$related_query = new WP_Query( $args );

			// Save our data into an array we can loop over several times instead of making all the calls repeatedly
			$posts_array = array();

			if ( $related_query->have_posts() ): while ( $related_query->have_posts() ): $related_query->the_post();

				// so we can keep adding to array
				$loop_count	= $related_query->current_post;

				$posts_array[$loop_count]['title']		= get_the_title();
				$posts_array[$loop_count]['permalink']	= get_permalink();
				$posts_array[$loop_count]['excerpt']	= get_the_excerpt();

				// replace the title with the short one if it exists
				if ( '' != $short_title = get_post_meta( $related_query->post->ID, 'short_title', true ) ) {
					$posts_array[$loop_count]['title']	= strip_tags( $short_title );
				}

				if ( has_post_thumbnail() ) {

					$image = '<figure class="article__thumbnail">';
						$image .= get_the_post_thumbnail( $related_query->post->ID, 'post-thumbnail' );
					$image .= '</figure>';

				} else {

					// if there is no featured image, use default in theme settings
					$default_image	= get_stylesheet_directory_uri() . 'images/running/default-post-thumbnail-sm.jpg';

					$image = '<figure class="article__thumbnail">';
						$image .= '<img src="'. $default_image .'">';
					$image .= '</figure>';

				}

				$posts_array[$loop_count]['image'] = $image;

			endwhile; endif;

			wp_reset_postdata();

			// Make sure the array of posts is not empty
			if ( !empty( $posts_array ) ) {

				// Original widget (which I still prefer, styling wise)
				$style	= $a['style'] == 0 ? ' style="display: block;"' : ' style="display: none;"';
				$widget = sprintf( '<aside class="recommended-widget alt-0 %s"%s>', $align, $style );

					$widget .= sprintf( '<h3 class="recommended-widget__title"><span>%s</span></h3>', $a['title'] );

					$widget .= '<ul class="recommended-widget__menu">';

						foreach ( $posts_array as $post ) {

							$widget .= sprintf( '<li class="recommended-widget__menu-item"><a href="%s">%s</a></li>', $post['permalink'], $post['title'] );

						}

					$widget .= '</ul>';

				$widget .= '</aside>';

				// Widget alt version 1 for A/B (AND EVEN C whoaaaaa) Testing
				$style	= $a['style'] == 1 ? ' style="display: block;"' : ' style="display: none;"';
				$widget .= sprintf( '<aside class="recommended-widget alt-1 %s"%s>', $align, $style );

					$widget .= sprintf( '<h3 class="recommended-widget__title"><span>%s</span></h3>', $a['title'] );

					$widget .= '<ul class="recommended-widget__menu">';

						foreach ( $posts_array as $post ) {

							$widget .= sprintf( '<li class="recommended-widget__menu-item"><a href="%s">%s</a></li>', $post['permalink'], $post['title'] );

						}

					$widget .= '</ul>';

				$widget .= '</aside>';

				// Widget alt version 2 OR B? C?
				$style	= $a['style'] == 2 ? ' style="display: block;"' : ' style="display: none;"';
				$widget .= sprintf( '<aside class="recommended-widget alt-2 %s"%s>', $align, $style );

				$widget .= sprintf( '<h3 class="recommended-widget__title"><span>%s</span></h3>', $a['title'] );

					foreach ( $posts_array as $post ) {

						$widget .='<article class="article article_type_latest">';

							$widget .= sprintf( '<a href="%s" class="article__permalink">', $post['permalink'] );

								$widget .= $post['image'];

								$widget .= '<div class="article__details">';
									$widget .= sprintf( '<h3 class="article__title">%s</h3>', $post['title'] );
									$widget .= sprintf( '<p class="article__excerpt">%s</p>', $post['excerpt'] );
								$widget .= '</div>';

							$widget .= '</a>';

						$widget .= '</article>';

						break; // we only need the first post so we break out ASAP

					}

				$widget .= '</aside>';

			}

			return $widget;
		}

		return $content;

	}

	/**
	 * VeloPress Book Embed
	 *
	 * Adds VeloPress book embed to content.
	 *
	 * Example:
	 * [velopress align="{left|right}" {cta="Buy Now!"}]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function velopress_embed( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'align'	=> '',
				'cta'	=> '',
				'title'	=> ''
			),
			$atts
		);

		$book_id = ( get_post_meta( get_the_ID(), '_run_velopress_book', 1 ) != '' ) ? get_post_meta( get_the_ID(), '_run_velopress_book', 1 ) : '';

		/**
		 * if no book meta option was chosen, don't return anything
		 */
		if ( $book_id != '' ) {

			$align	= ( !empty( $a['align'] ) ? 'align' . $a['align'] : 'aligncenter' );
			$cta	= ( !empty( $a['cta'] ) ? $a['cta'] : 'Buy Now' );
			$title	= ( !empty( $a['title'] ) ? $a['title'] : 'Read the whole book');

			$book_json	= file_get_contents( 'http://www.velopress.com/wp-json/velopress/book/?id=' . $book_id );

			$book_obj	= json_decode( $book_json );

			if ( $book_obj->status == 'success' ) {

				return sprintf(
					'<aside class="velopress-book %s">

						<div class="velopress-book__header">
							<h4 class="velopress-book__header__module_title">%s</h4>
						</div>

						<div class="velopress-book__content">
							<figure class="velopress-book__content__cover">
								<a href="%s" target="_blank"><img src="%s"></a>
							</figure>
							<p class="velopress-book__content__cta"><a href="%s" target="_blank">%s</a></p>
							<h3 class="velopress-book__content__title"><a href="%s" target="_blank">%s</a></h3>
							<p class="velopress-book__content__subtitle">%s</p>
							<p class="velopress-book__content__author">%s</p>
							<p class="velopress-book__content__desc">%s</p>
						</div>

					</aside>',
					$align,
					$title,
					$book_obj->book_url,
					$book_obj->cover_url,
					$book_obj->book_url,
					$cta,
					$book_obj->book_url,
					$book_obj->title,
					$book_obj->sub_title,
					$book_obj->author,
					$book_obj->description

				);

			} else {
				// failure
				return sprintf(
					'<aside class="velopress-book %s">

						<div class="velopress-book__header">
							<h4 class="velopress-book__header__module_title">%s</h4>
						</div>

						<div class="velopress-book__content">
							<figure class="velopress-book__content__cover">
								<a href="%s" target="_blank"><img src="%s"></a>
							</figure>
							<p class="velopress-book__content__cta"><a href="%s" target="_blank">%s</a></p>
							<h3 class="velopress-book__content__title"><a href="%s" target="_blank">%s</a></h3>
							<div class="velopress-book__content__desc">%s</div>
						</div>

					</aside>',
					$align,
					$title,
					$book_obj->book_url,
					$book_obj->cover_url,
					$book_obj->book_url,
					$cta,
					$book_obj->book_url,
					$book_obj->title,
					$book_obj->description
				);
			}

		} else {

			return false;

		}

	}

	// START Partner Connect Short code for text and link under featured image on single post
	// To implement: 1. Create custom field, 2. name can be anything, 3. Value is the link, 4. enter shortcode [pctext=partnerconnect] in content section of post
	public function partnerconnect_sc(){
		global $post;

		if ( !isset( $post ) ) {
			return;
		}

		$pc_link = get_post_meta( $post->ID, 'partnerconnect' , true );

		ob_start();

		if ( is_single() ) {
			if ( !empty( $pc_link ) ) {?>
				<div style="padding-bottom: 25px;"><span class="sponsored-label" style="font-weight: bold;">Sponsored Content</span>: Content straight from brands. Here are the<a href="<?php echo $pc_link; ?>"> details</a>.</div>
			<?php }
		}

		return ob_get_clean();

	}


	// [bartag foo="foo-value"]
	public function buynowbutton_func( $atts ) {
		$a = shortcode_atts( array(
			'url' => ''
		), $atts );
		return '
			<div class="buynowbutton">
				<a class="buynowimage" href="'.$a['url'].'" onClick="_gaq.push([\'_trackEvent\', \'Competitor Gift Guide\', \'Buy Now Button\', \''.$a['url'].'\']);" target="_blank">Buy</a>
			</div>
			<div class="cgg_clear"></div>';
	}


	// iContact shortcode
	public function inline_iContact_shortcode( $atts, $content = null ) {
		$a = shortcode_atts( array(
			'heading' => 'Get our monthly digital magazine, weekly running content & exclusive offers delivered to your inbox.',
			'subheading' => 'Subscribe to the FREE Competitor Running newsletter',
			'position' => 'inarticle'
		), $atts );

		$newsletter_url = home_url('/') . 'the-rundown?list-name=competitors-best';

		ob_start(); ?>
			<div id="icontact_wrapper_<?php echo $a['position']; ?>">
				<div class="icontact_header">
					<div class="icontact_intro_text">
						<h3><?php echo $a['heading']; ?></h3>
						<p><?php echo $a['subheading']; ?></p>
					</div>
				</div>
				<form class="subscribe-form icontact_<?php echo $a['position']; ?>" name="icontact_<?php echo $a['position']; ?>" id="icontact_<?php echo $a['position']; ?>" onsubmit="return validateform(this.id);_gaq.push(['_trackEvent', 'newsletter competitor running', 'subscribe', '<?php echo $a['position'] ?>']);">
					<?php wp_nonce_field( 'nl_subscribe_'. $a['position'], 'nl_subscribe_nonce_'. $a['position'], true, true ); ?>

					<input type="hidden" name="mbid" value="competitor" />
					<input type="hidden" name="pos" value="<?php echo $a['position']; ?>">
					<input type="hidden" name="sub[]" value="media-all" />
					<div class="please_wait" style="display:none;"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icontact-wait.gif"></div>
					<div class="errorMessage" style="display:none;"></div>
					<div class="confirmMessage" style="display:none;"></div>
					<div class="nl-options"></div>
					<input type="email" name="fields_email" id="fields_email_<?php echo $a['position']; ?>" class="fields_email default" placeholder="Enter Your Email Address">
					<button type="submit" class="submit-button">Sign Up <span class="icon-right-open"></span></button>
					<div class="icontact_mini_links">
						<div class="nl_privacy_link"> You can unsubscribe at any time. <a href="http://pocketoutdoormedia.com/privacy-policy/" target="_blank">View Privacy Policy</a> </div>
					</div>
				</form>
			</div>
			<?php
		return ob_get_clean();
	}

	/**
	 * survey gizmo shortcode
	 */
	public function surveygizmo_embed( $atts ) {
		$a = shortcode_atts( array(
			'code' => ''
		 ), $atts );

		return '
			<script type="text/javascript">// <![CDATA[
			document.write( \'<script src="http\' + ( ( "https:" == document.location.protocol ) ? "s" : "" ) + \'://'.$a['code'].'?__ref=\' + escape( document.location ) + \'" type="text/javascript" ></scr\'  + \'ipt>\' );
			// ]]></script>
		';
	}

	/**
	 * survey gizmo shortcode - new
	 */
	public function surveygizmo_embed_new( $atts ) {
		$a = shortcode_atts( array(
			'code' => ''
		 ), $atts );

		return '
			<script type="text/javascript" >document.write( \'<script src="http\' + ( ( "https:" == document.location.protocol ) ? "s" : "" ) + \'://'.$a['code'].'?__output=embedjs&__ref=\' + escape( document.location.origin + document.location.pathname ) + \'" type="text/javascript" ></scr\'  + \'ipt>\' );</script>
		';
	}


	/**
	 * accordion shortcode
	 */
	public function accordion_wrapper( $atts, $content = null ) {
		return '<div class="accordion">
			'. do_shortcode( $content ) .'
		</div>';
	}


	public function accordion_single( $atts, $content = null ) {
		STATIC $i = 0;
		$i++;
		$a = shortcode_atts( array(
			'title' => '',
		), $atts );
		return '<div class="accordion-section custom" style="position: relative;">
			<div class="accordion-section-title" data-reveal="#panel'.$i.'">'.$a['title'].'</div>
			<div class="accordion-section-content" id="panel'.$i.'">
				'. do_shortcode( $content ) .'
			</div>
		</div>';
	}


	// grid/columns
	public function grid_container( $atts, $content = null ) {
		$a = shortcode_atts( array(
			'count' => ''
		), $atts );
		return '<div class="grid_'. $a['count'] .'_special">
			'. do_shortcode( $content ) .'
		</div>';
	}


	public function grid_column( $atts, $content = null ) {
		return '<div class="column_special">'. do_shortcode( $content ) .'</div>';
	}

	/**
	 * Dummy code to return a blank shortcode
	 * @param  array  $atts User defined attributes in shortcode tag.
	 * @return string		Blank/nothing
	 */
	public function blank_return( $atts = array() ) {
		return;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {

		// override default gallery shortcode
		remove_shortcode( 'gallery' );
		add_shortcode( 'gallery',  array( $this, 'gallery_embed' ) );

		// register "instagram" shortcode
		add_shortcode( 'instagram', array( $this, 'instagram_embed' ) );

		// register "related" shortcode
		add_shortcode( 'twitter', array( $this, 'twitter_embed' ) );

		// register "youtube" shortcode
		add_shortcode( 'youtube', array( $this, 'youtube_embed' ) );

		// register "facebook" shortcode
		add_shortcode( 'facebook', array( $this, 'facebook_embed') );

		// register "vimeo" shortcode
		add_shortcode( 'vimeo', array( $this, 'vimeo_embed' ) );

		// register "brightcove" shortcode
		add_shortcode( 'brightcove', array( $this, 'brightcove_embed' ) );

		// register "gifs" shortcode
		add_shortcode( 'gifs', array( $this, 'gifs_embed' ) );

		add_shortcode( 'icontact-inline', array( $this, 'inline_iContact_shortcode' ) );

		// register Visual Composer replacement shortcodes
		add_shortcode( 'vc_images_carousel', array( $this, 'vc_images_grid' ) );
		add_shortcode( 'vc_single_image', array( $this, 'vc_image' ) );
		add_shortcode( 'vc_column_text', array( $this, 'vc_plain_return' ) );
		add_shortcode( 'vc_column', array( $this, 'vc_plain_return' ) );
		add_shortcode( 'vc_row', array( $this, 'vc_plain_return' ) );
		add_shortcode( 'vc_separator', array( $this, 'vc_no_return' ) );
		add_shortcode( 'vc_facebook', array( $this, 'vc_no_return' ) );
		add_shortcode( 'vc_tweetmeme', array( $this, 'vc_no_return' ) );
		add_shortcode( 'vc_accordion', array( $this, 'accordion_wrapper' ) );
		add_shortcode( 'vc_accordion_tab', array( $this, 'accordion_single' ) );
		add_shortcode( 'vc_gallery', array( $this, 'gallery_embed' ) );
		add_shortcode( 'vc_custom_heading', array( $this, 'vc_heading' ) );

		// miscellaneous shortcodes
		add_shortcode( 'buy-now', array( $this, 'buynow_func' ) );
		add_shortcode( 'related', array( $this, 'related_articles_embed' ) );
		add_shortcode( 'velopress', array( $this, 'velopress_embed' ) );
		add_shortcode( 'pctext', array( $this, 'partnerconnect_sc' ) );
		add_shortcode( 'buynowbutton', array( $this, 'buynowbutton_func' ) );
		add_shortcode( 'surveygizmo', array( $this, 'surveygizmo_embed' ) );
		add_shortcode( 'sg', array( $this, 'surveygizmo_embed_new' ) );

		// remove the attempt to use imagebrowser shortcode
		add_shortcode( 'imagebrowser', array( $this, 'blank_return' ) );

		add_shortcode( 'grid', array( $this, 'grid_container' ) );
		add_shortcode( 'column', array( $this, 'grid_column' ) );

	}
		/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( !self::$instance )
			self::$instance = new self();

		return self::$instance;
	}
}

