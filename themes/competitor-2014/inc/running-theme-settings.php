<?php

class Running_Theme_Settings {

	public $options_page = 'cgi_media_technical_options';

	public $editorial_options_page	= 'cgi_media_editorial_options';

	public $plugin_pages = array();

	public static $instance = false;

	protected $technical_options_groups =
		array(
			array(
				'title'	=> 'Analytics',
				'slug'	=> 'analytics-options'
			),
			array(
				'title'	=> 'Category',
				'slug'	=> 'Category-options'
			),
			array(
				'title'	=> 'Social',
				'slug'	=> 'social-options'
			),
			array(
				'title'	=> 'Advertising',
				'slug'	=> 'advertising-options'
			),
			array(
				'title'	=> 'Sponsored Content',
				'slug'	=> 'sponsored-content-options'
			),
			array(
				'title'	=> 'Misc.',
				'slug'	=> 'misc-options'
			),
		);

	protected $editorial_options_groups =
		array(
			array(
				'title'	=> 'General',
				'slug'	=> 'general-options'
			),
			array(
				'title'	=> 'Content',
				'slug'	=> 'content-options'
			),
			array(
				'title'	=> 'iContact',
				'slug'	=> 'icontact-options'
			),
			array(
				'title'	=> 'Newsletters Options',
				'slug'	=> 'newsletters-options'
			),
			array(
				'title'	=> 'Footer',
				'slug'	=> 'footer-options'
			),
			array(
				'title'	=> 'Training Plans',
				'slug'	=> 'training-plans-options'
			),
		);

	public function __construct() {
		$this->_add_actions();
	}

	/**
	 * Public getter method for retrieving protected/protected variables
	 *
	 * @param string $field Name of field to retrieve
	 *
	 * @return mixed Field value if successful; Exception if request fails.
	 */
	public function __get( $field = null ) {

		if ( property_exists( $this, $field ) ) {
			return $this->{$field};
		}

		throw new Exception( "Invalid property: {$field}" );
	}

	/**
	 * Enqueue Admin Assets
	 *
	 * Enqueues the necessary css and js files when the WordPress admin is loaded.
	 */
	public function enqueue_admin_assets( $hook ) {

		if ( !in_array( $hook, $this->plugin_pages ) )
			return;

		add_action( 'admin_print_styles', array( 'CMB2_hookup', 'enqueue_cmb_css' ) );
		add_action( 'admin_footer', array( $this, 'enqueue_inline_scripts' ) );

		wp_enqueue_script( 'postbox' );

	}

	/**
	 * Enqueue Inline Scripts
	 *
	 * Enqueues the necessary js files/functions inside the page footer.
	 */
	public function enqueue_inline_scripts() {
		echo '<script>jQuery(document).ready(function() { postboxes.add_postbox_toggles(pagenow); });</script>';
	}

	/**
	 * Add Admin Menu
	 *
	 * Add menu pages to admin navigation.
	 */
	public function add_admin_menu() {

		add_menu_page(
			'Running Theme Options',
			'Running Theme Options',
			'manage_options',
			$this->options_page,
			array( $this, 'generate_options_page' ),
			'',
			80
		);

		$technical_options_page	= add_submenu_page(
			$this->options_page,
			'Technical Options',
			'Technical Options',
			'manage_options',
			$this->options_page,
			array( $this, 'generate_options_page' )
		);

		$editorial_options_page	= add_submenu_page(
			$this->options_page,
			'Editorial Options',
			'Editorial Options',
			'manage_options',
			$this->editorial_options_page,
			array( $this, 'generate_options_page' )
		);

		// variable with all our plugin pages
		$this->plugin_pages	= array(
			$technical_options_page,
			$editorial_options_page,
		);

	}

	/**
	 * Generate Options Page
	 *
	 * Dynamically generates the theme options page.
	 */
	public function generate_options_page() {

		$active_page = ( isset( $_GET['page'] )  ? $_GET['page'] : $this->options_page );

		if ( $active_page === 'cgi_media_editorial_options' ) {
			$tabs	= $this->editorial_options_groups;
		} else {
			$tabs	= $this->technical_options_groups;
		}

		require get_stylesheet_directory() .'/inc/views/theme-settings.php';
	}

	/**
	 * Register Technical Options
	 *
	 * Controller method for registering Technical Options with CMB2.
	 */
	public function register_techinical_options() {

		$this->_register_analytics_options();
		$this->_register_category_options();
		$this->_register_social_options();
		$this->_register_advertising_options();
		$this->_register_sponsored_content_options();
		$this->_register_misc_options();

	}

	/**
	 * Register Editorial Options
	 *
	 * Controller method for registering Editorial Options with CMB2.
	 */
	public function register_editorial_options() {

		$this->_register_general_options();
		$this->_register_content_options();
		$this->_register_icontact_options();
		$this->_register_newsletter_options();
		$this->_register_footer_options();
		$this->_register_training_plans_options();

	}

	/**
	 * Register settings notices for display
	 *
	 * @param int $object_id Option key
	 * @param array $updated Array of updated fields
	 */
	public function settings_notices( $object_id = null, $updated = array() ) {

		if ( $object_id !== $this->options_page || empty( $updated ) ) {
			return;
		}

		add_settings_error( "{$this->options_page}-notices", '', __( 'Settings updated.', 'cgi' ), 'updated' );
		settings_errors( "{$this->options_page}-notices" );
	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( !self::$instance )
			self::$instance = new self;

		return self::$instance;
	}

	/**
	 * Get Theme Option
	 *
	 * Utility function for getting theme settings from the database
	 *
	 * @param  mixed $fields Array or string containing the field name(s) to retrieve.
	 * @param  mixed $default The default value to return if no value is returned (optional)
	 *
	 * @return mixed Current value for the specified option. If the option does not exist, returns parameter $default if specified or boolean FALSE by default.
	 */
	public static function get_technical_option( $fields = null, $default = null ) {

		$settings = get_option( self::singleton()->options_page, $default );

		// return a specific field, or set of fields
		if ( !empty( $settings ) && !empty( $fields ) ) {

			if ( !is_array( $fields ) ) {

				if ( isset( $settings[$fields] ) ) {
					return $settings[$fields];
				}

			} else {

				$values = array();

				foreach ( $fields as $key ) {
					$values[$key] = ( isset( $settings[$key] ) ? $settings[$key] : '' );
				}

				return $values;
			}

		}

		return false;
	}

	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {

		// Create menu pages
		add_action( 'admin_menu', array( $this, 'add_admin_menu' ) );

		// CMB LOGIC
		add_action( 'cmb2_admin_init', array( $this, 'register_techinical_options' ) );
		add_action( 'cmb2_admin_init', array( $this, 'register_editorial_options' ) );

		// Register scripts/styles
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_assets' ) );

		foreach ( $this->technical_options_groups as $option_group ) {
			add_action( "cmb2_save_options-page_fields_{$option_group['slug']}", array( $this, 'settings_notices' ), 10, 2 );
		}
	}

	/**
	 * REGISTER TECHNICAL OPTIONS
	 */

	/**
	 * Register Header Options
	 *
	 * Adds the necessary theme options for the given section.
	 */
	protected function _register_analytics_options() {

		$cmb = new_cmb2_box(
			array(
				'id'         => $this->technical_options_groups[0]['slug'],
				'hookup'     => false,
				'cmb_styles' => false,
				'title'		 => $this->technical_options_groups[0]['title'],
				'show_on'    => array(
					// These are important, don't remove
					'key'   => 'options-page',
					'value' => array( $this->options_page )
				)
			)
		);

		$cmb->add_field( array(
			'id'      => 'google_analytics',
			'name'    => __( 'Google Analytics', 'cgi' ),
			'desc'    => __( 'Google Analytics ID', 'cgi' ),
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'id'      => 'reinvigorate',
			'name'    => __( 'Reinvigorate', 'cgi' ),
			'desc'    => __( 'Reinvigorate ID', 'cgi' ),
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'id'      => 'bing_search_appID',
			'name'    => __( 'Bing Search App ID', 'cgi' ),
			'desc'    => __( 'Bing Search App ID', 'cgi' ),
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'id'      => 'bing_web_search_id_v5',
			'name'    => __( 'Bing Web Search Subscription Key - API v5.0', 'cgi' ),
			'desc'    => __( 'bing api v5', 'cgi' ),
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'id'      => 'bing_img_search_id_v5',
			'name'    => __( 'Bing Image Search Subscription Key - API v5.0', 'cgi' ),
			'desc'    => __( 'bing api v5', 'cgi' ),
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'google_tag',
			'name'	=> __( 'Google Tag Manager Code', 'cgi' ),
			'desc'	=> __( 'Google Tag Manager Code.', 'cgi' ),
			'type'	=> 'textarea_code',
		) );

		$cmb->add_field( array(
			'id'	=> 'google_tag_head',
			'name'	=> __( 'Google Tag Manager Code (in head)', 'cgi' ),
			'desc'	=> __( 'Google Tag Manager Code (in head).', 'cgi' ),
			'type'	=> 'textarea_code',
		) );

		$cmb->add_field( array(
			'id'	=> 'menu_tracking_pixel',
			'name'	=> __( 'Menu Tracking Pixel Code', 'cgi' ),
			'desc'	=> __( 'Menu Tracking Pixel Code.', 'cgi' ),
			'type'	=> 'textarea_code',
		) );

		$cmb->add_field( array(
			'id'	=> 'before_closing_body',
			'name'	=> __( 'Before &lt;/body&gt; Tag', 'cgi' ),
			'desc'	=> __( 'Code to be placed directly before closing tag.', 'cgi' ),
			'type'	=> 'textarea_code',
		) );

	}

	/**
	 * Register Category Options
	 *
	 * Adds the necessary theme options for the given section.
	 */
	protected function _register_category_options() {

		$cmb = new_cmb2_box( array(
			'id'         => $this->technical_options_groups[1]['slug'],
			'hookup'     => false,
			'cmb_styles' => false,
			'title'      => $this->technical_options_groups[1]['title'],
			'show_on'    => array(
				// These are important, don't remove
				'key'       => 'options-page',
				'value'     => array( $this->options_page )
			)
		) );

		$cmb->add_field( array(
			'id'				=> 'gallery_category',
			'name'				=> __( 'Galery Category', 'cgi' ),
			'desc'				=> __( 'Enter the ID of the gallery category to use for queries and widgets related to gallery posts', 'cgi' ),
			'type'				=> 'text_small',
			'sanitization_cb'	=> 'absint'
		) );

		$cmb->add_field( array(
			'id'				=> 'video_category',
			'name'				=> __( 'Video Category', 'cgi' ),
			'desc'				=> __( 'Enter the ID of the video category to use for queries and widgets related to video posts', 'cgi' ),
			'type'				=> 'text_small',
			'sanitization_cb'	=> 'absint'
		) );

	}

	/**
	 * Register Social Options
	 *
	 * Adds the necessary theme options for the given section.
	 */
	protected function _register_social_options() {

		$cmb = new_cmb2_box( array(
			'id'			=> $this->technical_options_groups[2]['slug'],
			'hookup'		=> false,
			'cmb_styles'	=> false,
			'title'			=> $this->technical_options_groups[2]['title'],
			'show_on'		=> array(
				// These are important, don't remove
				'key'	=> 'options-page',
				'value'	=> array( $this->options_page )
			),
		) );

		$cmb->add_field( array(
			'id'	=> 'site_twitter_handle',
			'name'	=> __( 'Site Twitter Handle', 'cgi' ),
			'desc'	=> __( 'Enter the Twitter handle for this site', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'site_instagram_handle',
			'name'	=> __( 'Site Instagram Handle', 'cgi' ),
			'desc'	=> __( 'Enter the Instagram handle for this site', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'youtube_page',
			'name'	=> __( 'YouTube Page', 'cgi' ),
			'desc'	=> __( 'Enter the YouTube page URL for this site', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'secondary_twitter_handle',
			'name'	=> __( 'Secondary Twitter Handle', 'cgi' ),
			'desc'	=> __( 'Enter the Secondary Twitter handle for this site', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'site_facebook_page',
			'name'	=> __( 'Site Facebook Page', 'cgi' ),
			'desc'	=> __( 'Enter the Facebook page for this site', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'site_facebook_page_id',
			'name'	=> __( 'Site Facebook Page ID', 'cgi' ),
			'desc'	=> __( 'Enter the Facebook page id for this site', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'facebook_comments',
			'name'	=> __( 'Facebook Comments', 'cgi' ),
			'desc'	=> __( 'If checked, Facebook comments will load below posts. Uncheck to disable.', 'cgi' ),
			'type'	=> 'checkbox',
		) );

		$cmb->add_field( array(
			'id'	=> 'facebook_appid',
			'name'	=> __( 'Facebook App ID', 'cgi' ),
			'desc'	=> __( 'Enter Facebook App ID to provide commenting functionality and moderator access.', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'itunes_appid',
			'name'	=> __( 'iTunes App ID', 'cgi' ),
			'desc'	=> __( 'Enter iTunes App ID to display smart app banner.', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'bing_webmaster_vid',
			'name'	=> __( 'Bing Webmaster Verify ID', 'cgi' ),
			'desc'	=> __( 'Enter Bing Webmaster Verify Id for Site Owner Verification.', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'show_social_slider',
			'name'	=> __( 'Show Social Slider', 'cgi' ),
			'desc'	=> __( 'If checked, the left hand social share slider will be visible. Uncheck to disable.', 'cgi' ),
			'type'	=> 'checkbox',
		) );

		$cmb->add_field( array(
			'id'	=> 'social_media_publisher_key',
			'name'	=> __( 'Social Media Publisher Key', 'cgi' ),
			'desc'	=> __( 'Enter publisher key for social media sharing icons.', 'cgi' ),
			'type'	=> 'text',
		) );

	}

	protected function _register_advertising_options() {

		$cmb = new_cmb2_box( array(
			'id'			=> $this->technical_options_groups[3]['slug'],
			'hookup'		=> false,
			'cmb_styles'	=> false,
			'title'			=> $this->technical_options_groups[3]['title'],
			'show_on'		=> array(
				// These are important, don't remove
				'key'	=> 'options-page',
				'value'	=> array( $this->options_page )
			),
		) );

		$cmb->add_field( array(
			'id'	=> 'background_skin',
			'name'	=> __( 'Background Skin', 'cgi' ),
			'desc'	=> __( 'If checked, header will load the ad code for the background skin. Uncheck to disable.', 'cgi' ),
			'type'	=> 'checkbox',
		) );

		$cmb->add_field( array(
			'id'	=> 'interstitial_ad',
			'name'	=> __( 'Interstitial Ad', 'cgi' ),
			'desc'	=> __( 'If checked, an interstitial ad will be displayed when the first link is clicked.  Uncheck to disable.', 'cgi' ),
			'type'	=> 'checkbox',
		) );

		$cmb->add_field( array(
			'id'	=> 'interstitial_ad_cat',
			'name'	=> __( 'Interstitial Ad Category', 'cgi' ),
			'desc'	=> __( 'Enter a category for the interstitial ad.', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'interstitial_ad_excl_cat',
			'name'	=> __( 'Interstitial Ad Exclude Category', 'cgi' ),
			'desc'	=> __( 'Enter a category for the interstitial ad to be excluded, will only work if Interstitial Ad field is empty.', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'			=> 'interstitial_exp_time',
			'name'			=> __( 'Interstitial Ad Cookie Expiration time', 'cgi' ),
			'desc'			=> __( 'Enter an amount in MINUTES that this cookie should expire in.', 'cgi' ),
			'type'			=> 'text_small',
			'default'		=> '7.5', // 7.5 minutes
			'attributes'	=> array(
				'type'	=> 'number',
				'min'	=> '0',
				'step'	=> '.1'
			),
		) );

		$cmb->add_field( array(
			'id'	=> 'interstitial_mobile',
			'name'	=> __( 'Interstitial Mobile Ad', 'cgi' ),
			'desc'	=> __( 'If checked, an interstitial ad will be displayed when the first link is clicked. Uncheck to disable.', 'cgi' ),
			'type'	=> 'checkbox',
		) );

		$cmb->add_field( array(
			'id'	=> 'interstitial_mobile_cat',
			'name'	=> __( 'Interstitial Mobile Ad Category', 'cgi' ),
			'desc'	=> __( 'Enter a category for the interstitial ad.', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'interstitial_mobile_excl_cat',
			'name'	=> __( 'Interstitial Mobile Exclude Category', 'cgi' ),
			'desc'	=> __( 'Enter a category for the interstitial mobile ad to be excluded, will only work if Interstitial Mobile Ad field is empty.', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'			=> 'interstitial_mobile_exp_time',
			'name'			=> __( 'Interstitial Mobile Cookie Expiration time', 'cgi' ),
			'desc'			=> __( 'Enter an amount in <strong>MINUTES</strong> that this cookie should expire in.', 'cgi' ),
			'type'			=> 'text_small',
			'default'		=> '7.5', // 7.5 minutes
			'attributes'	=> array(
				'type'	=> 'number',
				'min'	=> '0',
				'step'	=> '.1'
			),
		) );

		$cmb->add_field( array(
			'id'	=> 'facebook_interstitial_ad',
			'name'	=> __( 'Facebook Interstitial Ad', 'cgi' ),
			'desc'	=> __( 'If checked, a Facebook Like Box interstitial ad will be displayed when the first link is clicked. Uncheck to disable.', 'cgi' ),
			'type'	=> 'checkbox',
		) );

		$cmb->add_field( array(
			'id'	=> 'fb_interstitial_excl_cat',
			'name'	=> __( 'Facebook/Magazine Interstitial Ad Exclude Category', 'cgi' ),
			'desc'	=> __( 'Enter categories where the Facebook Like Box interstitial ad and/or Magazine subscription interstitial will not display.', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'magazine_interstitial_ad',
			'name'	=> __( 'Magazine Interstitial Ad', 'cgi' ),
			'desc'	=> __( 'If checked, a Magazine subscription interstitial ad will be displayed after a minute on the site, rotating with the Facebook Interstitial if checked. Uncheck to disable.', 'cgi' ),
			'type'	=> 'checkbox',
		) );

		$cmb->add_field( array(
			'id'	=> 'magazine_interstitial_image',
			'name'	=> __( 'Magazine Interstitial Image', 'cgi' ),
			'desc'	=> __( 'Max image size is 700 x 200. Either enter a full URL to an image or upload/choose an image to/from the media library. Use the insert into post button in the Media Library to place the image link here.', 'cgi' ),
			'type'	=> 'file',
		) );

		$cmb->add_field( array(
			'id'	=> 'magazine_interstitial_link',
			'name'	=> __( 'Magazine Interstitial Link', 'cgi' ),
			'desc'	=> __( 'Enter a link for the magazine interstitial.', 'cgi' ),
			'type'	=> 'text_url',
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_interstitial_ad',
			'name'	=> __( 'Newsletter Interstitial Ad', 'cgi' ),
			'desc'	=> __( 'If checked, a Newsletter interstitial ad will be displayed when the first link is clicked. Uncheck to disable.', 'cgi' ),
			'type'	=> 'checkbox',
		) );

		$cmb->add_field( array(
			'id'	=> 'pre_roll',
			'name'	=> __( 'Pre-Roll', 'cgi' ),
			'desc'	=> __( 'If checked, pre-roll ads will be displayed on videos. Uncheck to disable.', 'cgi' ),
			'type'	=> 'checkbox',
		) );

		$cmb->add_field( array(
			'id'	=> 'outbrain',
			'name'	=> __( 'Outbrain', 'cgi' ),
			'desc'	=> __( 'If checked, the Outbrain js will load under articles on posts. Uncheck to disable.', 'cgi' ),
			'type'	=> 'checkbox',
		) );

		$cmb->add_field( array(
			'id'	=> 'pubexchange',
			'name'	=> __( 'PubExchange', 'cgi' ),
			'desc'	=> __( 'If checked, PubExchange will load under articles on posts. Uncheck to disable.', 'cgi' ),
			'type'	=> 'checkbox',
		) );

		$cmb->add_field( array(
			'id'	=> 'mandelbrot',
			'name'	=> __( 'Mandelbrot', 'cgi' ),
			'desc'	=> __( 'Mandelbrot ID.', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'meebonetwork',
			'name'	=> __( 'Meebo Network', 'cgi' ),
			'desc'	=> __( 'Meebo Network Value.', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'eXelate',
			'name'	=> __( 'eXelate', 'cgi' ),
			'desc'	=> __( 'If checked, eXelate script will be added to the footer on all pages. Uncheck to disable.', 'cgi' ),
			'type'	=> 'checkbox',
		) );

		$cmb->add_field( array(
			'id'	=> 'ad_com',
			'name'	=> __( 'Advertising.com', 'cgi' ),
			'desc'	=> __( 'If checked, Advertising.com ads will be displayed on post pages. Uncheck to disable.', 'cgi' ),
			'type'	=> 'checkbox',
		) );

		$cmb->add_field( array(
			'id'	=> 'ad_com_pid',
			'name'	=> __( 'Advertising.com Placement Id', 'cgi' ),
			'desc'	=> __( 'Enter the Advertising.com PID for this site.', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'ad_com_300_250_pid',
			'name'	=> __( 'Advertising.com 300x250 Placement Id', 'cgi' ),
			'desc'	=> __( 'Enter the Advertising.com placement id for the in article ad.', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'ad_com_650_300_pid',
			'name'	=> __( 'Advertising.com 650x300 Placement Id', 'cgi' ),
			'desc'	=> __( 'Enter the Advertising.com placement id for the in article ad.', 'cgi' ),
			'type'	=> 'text',
		) );

	}

	protected function _register_sponsored_content_options() {

		$cmb = new_cmb2_box( array(
			'id'			=> $this->technical_options_groups[4]['slug'],
			'hookup'		=> false,
			'cmb_styles'	=> false,
			'title'			=> $this->technical_options_groups[4]['title'],
			'show_on'		=> array(
				// These are important, don't remove
				'key'	=> 'options-page',
				'value'	=> array( $this->options_page )
			),
		) );

		$cmb->add_field( array(
			'id'	=> 'partner_connect_ch',
			'name'	=> __( 'Sponsored Content Home Page', 'cgi' ),
			'desc'	=> __( 'If checked, Sponsored Content home page section will be visible. Uncheck to disable.', 'cgi' ),
			'type'	=> 'checkbox',
		) );

		/*$cmb->add_field( array(
			'id'	=> 'partner_connect_link',
			'name'	=> __( 'From Our Partners ', 'cgi' ),
			'desc'	=> __( 'Will change the from our partners link from the default.', 'cgi' ),
			'type'	=> 'text',
		) );*/

	}

	protected function _register_misc_options() {

		$cmb = new_cmb2_box( array(
			'id'			=> $this->technical_options_groups[5]['slug'],
			'hookup'		=> false,
			'cmb_styles'	=> false,
			'title'			=> $this->technical_options_groups[5]['title'],
			'show_on'		=> array(
				// These are important, don't remove
				'key'	=> 'options-page',
				'value'	=> array( $this->options_page )
			),
		) );

		/*$cmb->add_field( array(
			'id'	=> 'optimizely_code',
			'name'	=> __( 'Optimizely ', 'cgi' ),
			'desc'	=> __( 'Enter full script tag.', 'cgi' ),
			'type'	=> 'textarea_code',
		) );*/

		$cmb->add_field( array(
			'id'	=> 'amp_exclude_multipage_posts',
			'name'	=> __( 'AMP - Exclude MultiPage Posts ', 'cgi' ),
			'desc'	=> __( 'Exclude multipage posts from AMP.', 'cgi' ),
			'type'	=> 'checkbox',
		) );

		$cmb->add_field( array(
			'id'	=> 'grd_section_heading',
			'name'	=> __( 'GRD Section Heading ', 'cgi' ),
			'desc'	=> __( 'Heading for the top section that will appear in the home page.', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'grd_section_url',
			'name'	=> __( 'GRD Section URL ', 'cgi' ),
			'desc'	=> __( 'URL will be used for anchor links in GRD section.', 'cgi' ),
			'type'	=> 'text_url',
		) );

		$cmb->add_field( array(
			'id'	=> 'grd_section_pixlee_code',
			'name'	=> __( 'GRD Section Pixlee Code ', 'cgi' ),
			'desc'	=> __( 'JS Pixlee code for the GRD section on the home page. If this is left blank, the section will not show at all.', 'cgi' ),
			'type'	=> 'textarea_code',
		) );

	}



	/**
	 * REGISTER EDITORIAL OPTIONS
	 */

	protected function _register_general_options() {

		$cmb = new_cmb2_box( array(
			'id'			=> $this->editorial_options_groups[0]['slug'],
			'hookup'		=> false,
			'cmb_styles'	=> false,
			'title'			=> $this->editorial_options_groups[0]['title'],
			'show_on'		=> array(
				// These are important, don't remove
				'key'	=> 'options-page',
				'value'	=> array( $this->editorial_options_page )
			),
		) );

		$cmb->add_field( array(
			'id'	=> 'logo',
			'name'	=> __( 'Site Logo', 'cgi' ),
			'desc'	=> __( 'Max image size is 300 x 60. Either enter a full URL to an image or upload/choose an image to/from the media library. Use the insert into post button in the Media Library to place the image link here.', 'cgi' ),
			'type'	=> 'file',
		) );

		$cmb->add_field( array(
			'id'	=> 'secondary_logo',
			'name'	=> __( 'Site Logo', 'cgi' ),
			'desc'	=> __( 'Max image size is 300 x 60. Either enter a full URL to an image or upload/choose an image to/from the media library. Use the insert into post button in the Media Library to place the image link here.', 'cgi' ),
			'type'	=> 'file',
		) );

		$cmb->add_field( array(
			'id'	=> 'secondary_logo_categories',
			'name'	=> __( 'Secondary Logo ', 'cgi' ),
			'desc'	=> __( 'Enter the ID( s ) ( comma delimited ) of the categories you wish to use the secondary logo', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'apple_touch_icon',
			'name'	=> __( 'Apple Touch Icon', 'cgi' ),
			'desc'	=> __( 'Image size is 114 x 114. Either enter a full URL to an image or upload/choose an image to/from the media library. Use the insert into post button in the Media Library to place the image link here.', 'cgi' ),
			'type'	=> 'file',
		) );

		$cmb->add_field( array(
			'id'	=> 'shortcut_icon',
			'name'	=> __( 'Shortcut Icon', 'cgi' ),
			'desc'	=> __( 'Image size is 48 x 48. Either enter a full URL to an image or upload/choose an image to/from the media library. Use the insert into post button in the Media Library to place the image link here.', 'cgi' ),
			'type'	=> 'file',
		) );

		$cmb->add_field( array(
			'id'	=> 'eu_regulations_text',
			'name'	=> __( 'EU Regulations Text', 'cgi' ),
			'desc'	=> __( 'Text to display about privacy policy.', 'cgi' ),
			'type'	=> 'textarea_code',
		) );

		$cmb->add_field( array(
			'id'	=> 'breaking_news_headline',
			'name'	=> __( 'Breaking News Headline', 'cgi' ),
			'desc'	=> __( 'Only plain text allowed. Keep it short, will show up on mobile as well.', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'breaking_news_link',
			'name'	=> __( 'Breaking News Link', 'cgi' ),
			'desc'	=> __( 'Enter full URL to use for the breaking news headline.', 'cgi' ),
			'type'	=> 'text_url',
		) );

		$cmb->add_field( array(
			'id'	=> 'sub_leaderboard_alert_banner_text',
			'name'	=> __( 'Event Coverage Headline', 'cgi' ),
			'desc'	=> __( 'Only plain text allowed. Keep it short, will show up on mobile as well.', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'sub_leaderboard_alert_banner_link',
			'name'	=> __( 'Event Coverage Link', 'cgi' ),
			'desc'	=> __( 'Enter full URL to use for the banner alert.', 'cgi' ),
			'type'	=> 'text_url',
		) );

	}

	protected function _register_content_options() {

		$cmb = new_cmb2_box( array(
			'id'			=> $this->editorial_options_groups[1]['slug'],
			'hookup'		=> false,
			'cmb_styles'	=> false,
			'title'			=> $this->editorial_options_groups[1]['title'],
			'show_on'		=> array(
				// These are important, don't remove
				'key'	=> 'options-page',
				'value'	=> array( $this->editorial_options_page )
			),
		) );

		$cmb->add_field( array(
			'id'	=> 'featured_cats',
			'name'	=> __( 'Homepage Featured Categories', 'cgi' ),
			'desc'	=> __( 'Enter the 3 IDs ( comma delimited ) of the categories you wish to highlight on the homepage in the row near the bottom', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'sub_featured_articles_banner_image',
			'name'	=> __( 'Event Coverage Banner Image', 'cgi' ),
			'desc'	=> __( 'Image must be 660x50 exactly. Either enter a full URL to an image or upload/choose an image to/from the media library. This will appear underneath the set of five featured articles.', 'cgi' ),
			'type'	=> 'file',
		) );

		$cmb->add_field( array(
			'id'	=> 'sub_featured_articles_banner_link',
			'name'	=> __( 'Event Coverage Banner Link', 'cgi' ),
			'desc'	=> __( 'Enter full URL to use for the banner image.', 'cgi' ),
			'type'	=> 'text_url',
		) );

		$cmb->add_field( array(
			'id'	=> 'featured_gallery',
			'name'	=> __( 'Editors Pix', 'cgi' ),
			'desc'	=> __( 'Enter the ID of the gallery post you wish to highlight as the Editors Pix Gallery on the homepage.', 'cgi' ),
			'type'	=> 'text_small',
		) );

		$cmb->add_field( array(
			'id'	=> 'home_marquee_image',
			'name'	=> __( 'Custom Home Marquee Image', 'cgi' ),
			'desc'	=> __( 'Image must be 580 x 435 exactly. Either enter a full URL to an image or upload/choose an image to/from the media library. Use the insert into post button in the Media Library to place the image link here.', 'cgi' ),
			'type'	=> 'file',
		) );

		$cmb->add_field( array(
			'id'	=> 'home_marquee_title',
			'name'	=> __( 'Custom Home Marquee Title', 'cgi' ),
			'desc'	=> __( 'Only plain text allowed. Keep it short, similar to a short title in a post.', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'home_marquee_description',
			'name'	=> __( 'Custom Home Marquee Description', 'cgi' ),
			'desc'	=> __( 'Description is limited to 140 characters and will be trimmed at the closest word to that length. Plain text only.', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'home_marquee_link',
			'name'	=> __( 'Custom Home Marquee Link', 'cgi' ),
			'desc'	=> __( 'Enter full URL to use for the custom marquee.', 'cgi' ),
			'type'	=> 'text_url',
		) );

		$cmb->add_field( array(
			'id'	=> 'secondary_marquee_id',
			'name'	=> __( 'Secondary Marquee Post ID', 'cgi' ),
			'desc'	=> __( 'Enter post ID to use for secondary marquee.', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'gallery_last_slide',
			'name'	=> __( 'Gallery Last Slide', 'cgi' ),
			'type'	=> 'file',
		) );

		$cmb->add_field( array(
			'id'	=> 'gallery_last_slide_url',
			'name'	=> __( 'Gallery Last Slide URL', 'cgi' ),
			'desc'	=> __( 'Enter full URL to use for the last slide image of the gallery.', 'cgi' ),
			'type'	=> 'text_url',
		) );

		$cmb->add_field( array(
			'id'	=> 'calendar_page',
			'name'	=> __( 'Race Calendar URL', 'cgi' ),
			'desc'	=> __( 'Enter full URL to use for the Race Calendar link in the "Subscribe" navigation. Defaults to calendar.[domain].com.', 'cgi' ),
			'type'	=> 'text_url',
		) );

		$cmb->add_field( array(
			'id'	=> 'article_slider_category',
			'name'	=> __( 'Category for Article Slider', 'cgi' ),
			'desc'	=> __( 'Enter name of posts category to display at bottom of article pages. Will not display slider if left blank.', 'cgi' ),
			'type'	=> 'text',
		) );

		/*$cmb->add_field( array(
			'id'	=> 'liveblog_tumblr_url',
			'name'	=> __( 'LiveBlog - Tumblr URL', 'cgi' ),
			'desc'	=> __( 'Enter Tumblr SUBDOMAIN URL of site for liveblog. Ex: womensrunning-cgi', 'cgi' ),
			'type'	=> 'text_url',
		) );

		$cmb->add_field( array(
			'id'	=> 'liveblog_tumblr_consumer_key',
			'name'	=> __( 'LiveBlog - Tumblr Consumer Key', 'cgi' ),
			'desc'	=> __( 'Enter Tumblr Consumer API key for liveblog.', 'cgi' ),
			'type'	=> 'text',
		) );*/

	}

	/**
	 * Register iContact Options
	 *
	 * Adds the necessary theme options for the given section.
	 */
	protected function _register_icontact_options() {

		$cmb = new_cmb2_box( array(
			'id'			=> $this->editorial_options_groups[2]['slug'],
			'hookup'		=> false,
			'cmb_styles'	=> false,
			'title'			=> $this->editorial_options_groups[2]['title'],
			'show_on'		=> array(
				// These are important, don't remove
				'key'	=> 'options-page',
				'value'	=> array( $this->editorial_options_page )
			),
		) );

		$cmb->add_field( array(
			'id'	=> 'icontact_enable',
			'name'	=> __( 'Enable?', 'cgi' ),
			'desc'	=> __( 'If checked, the option to enable the inline iContact form on posts will be available.', 'cgi' ),
			'type'	=> 'checkbox',
		) );

		$cmb->add_field( array(
			'id'	=> 'icontact_listid',
			'name'	=> __( 'List ID', 'cgi' ),
			'type'	=> 'text_small',
		) );

		$cmb->add_field( array(
			'id'	=> 'icontact_specialid',
			'name'	=> __( 'Special ID', 'cgi' ),
			'desc'	=> __( 'Should be the same as ListID', 'cgi' ),
			'type'	=> 'text_small',
		) );

		$cmb->add_field( array(
			'id'	=> 'icontact_specialidvalue',
			'name'	=> 'Special ID Value',
			'type'	=> 'text_small',
		) );

		$cmb->add_field( array(
			'id'	=> 'icontact_clientid',
			'name'	=> 'Client ID',
			'type'	=> 'text_small',
		) );

		$cmb->add_field( array(
			'id'	=> 'icontact_formid',
			'name'	=> __( 'Form ID', 'cgi' ),
			'type'	=> 'text_small',
		) );

	}

	protected function _register_newsletter_options() {

		$cmb = new_cmb2_box( array(
			'id'			=> $this->editorial_options_groups[3]['slug'],
			'hookup'		=> false,
			'cmb_styles'	=> false,
			'title'			=> $this->editorial_options_groups[3]['title'],
			'show_on'		=> array(
				// These are important, don't remove
				'key'	=> 'options-page',
				'value'	=> array( $this->editorial_options_page )
			),
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_sponsor_title',
			'name'	=> 'Newsletter Sponsor Options',
			'desc'	=> 'Manage newsletter sponsors options',
			'type'	=> 'title',
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_sponsor_image',
			'name'	=> __( 'Newsletter Sponsor Image', 'cgi' ),
			'desc'	=> __( 'Must be 175x40 in size', 'cgi' ),
			'type'	=> 'file',
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_sponsor_link',
			'name'	=> __( 'Newsletter Sponsor Link', 'cgi' ),
			'desc'	=> __( 'Enter full URL to use for the Newsletter Sponsor Link.', 'cgi' ),
			'type'	=> 'text_url',
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_train_title',
			'name'	=> 'Training Newsletter Options',
			'desc'	=> 'Manage Training newsletter options',
			'type'	=> 'title',
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_train_posts',
			'name'	=> __( 'Training Newsletter Posts', 'cgi' ),
			'desc'	=> __( 'Enter the 6 IDs ( comma delimited ) of the posts you wish to be shown on the Training newsletter', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_train_logo_image',
			'name'	=> __( 'Training Newsletter Logo Image', 'cgi' ),
			'desc'	=> __( 'Must be 210x75 in size', 'cgi' ),
			'type'	=> 'file',
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_train_logo_link',
			'name'	=> __( 'Training Newsletter Logo Link', 'cgi' ),
			'desc'	=> __( 'Enter full URL to use for the Training Newsletter Logo Link.', 'cgi' ),
			'type'	=> 'text_url',
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_shoe_title',
			'name'	=> 'Shoes &amp; Gear Newsletter Options',
			'desc'	=> 'Manage Shoes &amp; Gear newsletter options',
			'type'	=> 'title',
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_shoe_posts',
			'name'	=> __( 'Shoes &amp; Gear Newsletter Posts', 'cgi' ),
			'desc'	=> __( 'Enter the 6 IDs ( comma delimited ) of the posts you wish to be shown on the Shoes and Gear newsletter', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_shoe_logo_image',
			'name'	=> __( 'Shoes &amp; Gear Newsletter Logo Image', 'cgi' ),
			'desc'	=> __( 'Must be 210x75 in size', 'cgi' ),
			'type'	=> 'file',
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_shoe_logo_link',
			'name'	=> __( 'Shoes &amp; Gear Newsletter Logo Link', 'cgi' ),
			'desc'	=> __( 'Enter full URL to use for the Shoes and Gear Newsletter Logo Link.', 'cgi' ),
			'type'	=> 'text_url',
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_nut_title',
			'name'	=> 'Nutrition Newsletter Options',
			'desc'	=> 'Manage Shoes &amp; Gear newsletter options',
			'type'	=> 'title',
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_nut_posts',
			'name'	=> __( 'Nutrition Posts', 'cgi' ),
			'desc'	=> __( 'Enter the 6 IDs ( comma delimited ) of the posts you wish to be shown on the Nutrition newsletter', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_nut_logo_image',
			'name'	=> __( 'Nutrition Newsletter Logo Image', 'cgi' ),
			'desc'	=> __( 'Must be 210x75 in size', 'cgi' ),
			'type'	=> 'file',
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_nut_logo_link',
			'name'	=> __( 'Nutrition Newsletter Logo Link', 'cgi' ),
			'desc'	=> __( 'Enter full URL to use for the Nutrition Newsletter Logo Link.', 'cgi' ),
			'type'	=> 'text_url',
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_best_title',
			'name'	=> 'Competitors Best Newsletter Options',
			'desc'	=> 'Manage Shoes &amp; Gear newsletter options',
			'type'	=> 'title',
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_best_posts',
			'name'	=> __( 'Competitors Best Posts', 'cgi' ),
			'desc'	=> __( 'Enter the 6 IDs ( comma delimited ) of the posts you wish to be shown on the Competitor\'s Best newsletter', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_best_logo_image',
			'name'	=> __( 'Competitors Best Newsletter Logo Image', 'cgi' ),
			'desc'	=> __( 'Must be 210x75 in size', 'cgi' ),
			'type'	=> 'file',
		) );

		$cmb->add_field( array(
			'id'	=> 'newsletter_best_logo_link',
			'name'	=> __( 'Competitors Best Newsletter Logo Link', 'cgi' ),
			'desc'	=> __( 'Enter full URL to use for the Competitor\'s Best Newsletter Logo Link.', 'cgi' ),
			'type'	=> 'text_url',
		) );

		$cmb->add_field( array(
			'id'	=> 'artner_showcase_newsletter_title',
			'name'	=> 'Partner Showcase Newsletter Options',
			'desc'	=> 'Manage Parter Showcase newsletter options',
			'type'	=> 'title',
		) );

		$cmb->add_field( array(
			'id'	=> 'partner_showcase_newsletter_posts',
			'name'	=> __( 'Partner Showcase Newsletter Posts', 'cgi' ),
			'desc'	=> __( 'Enter the 6 IDs ( comma delimited ) of the posts you wish to be shown on the Partner Showcase newsletter', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'partner_showcase_newsletter_logo_image',
			'name'	=> __( 'Partner Showcase Newsletter Logo Image', 'cgi' ),
			'desc'	=> __( 'Must be 210x75 in size', 'cgi' ),
			'type'	=> 'file',
		) );

		$cmb->add_field( array(
			'id'	=> 'partner_showcase_newsletter_logo_link',
			'name'	=> __( 'Partner Showcase Newsletter Logo Link', 'cgi' ),
			'desc'	=> __( 'Enter full URL to use for the Partner Select Newsletter Logo Link.', 'cgi' ),
			'type'	=> 'text_url',
		) );

	}

	protected function _register_footer_options() {
		$cmb = new_cmb2_box( array(
			'id'			=> $this->editorial_options_groups[4]['slug'],
			'hookup'		=> false,
			'cmb_styles'	=> false,
			'title'			=> $this->editorial_options_groups[4]['title'],
			'show_on'		=> array(
				// These are important, don't remove
				'key'	=> 'options-page',
				'value'	=> array( $this->editorial_options_page )
			),
		) );

		$cmb->add_field( array(
			'id'	=> 'magazine_ipad',
			'name'	=> __( 'Digital Magazine Cover', 'cgi' ),
			'desc'	=> __( 'Image size has a max width of 165px. Height can vary but please keep it to under 235px. Image should be magazine cover within iPad.', 'cgi' ),
			'type'	=> 'file',
		) );

		$cmb->add_field( array(
			'id'	=> 'footer_icontact_txt',
			'name'	=> __( 'Footer iContact Text', 'cgi' ),
			'desc'	=> __( 'Type the text you want to appear before the iContact form that displays in the footer.', 'cgi' ),
			'type'	=> 'text',
		) );


	}

	protected function _register_training_plans_options() {

		$cmb = new_cmb2_box( array(
			'id'			=> $this->editorial_options_groups[5]['slug'],
			'hookup'		=> false,
			'cmb_styles'	=> false,
			'title'			=> $this->editorial_options_groups[5]['title'],
			'show_on'		=> array(
				// These are important, don't remove
				'key'	=> 'options-page',
				'value'	=> array( $this->editorial_options_page )
			),
		) );

		$cmb->add_field( array(
			'id'	=> 'training_plans_enable',
			'name'	=> __( 'Enable?', 'cgi' ),
			'desc'	=> __( 'If checked, Training Plans will be visible on posts. Uncheck to disable.', 'cgi' ),
			'type'	=> 'checkbox',
			'default'	=> 1
		) );

		$cmb->add_field( array(
			'id'	=> 'training_plans_title',
			'name'	=> __( 'Training Plans Title', 'cgi' ),
			'desc'	=> __( 'Enter the title of the Training Plans Section', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'training_plans_txt',
			'name'	=> __( 'Training Plans Call to Action Text', 'cgi' ),
			'desc'	=> __( 'Type the text you want to introduce the training plans.', 'cgi' ),
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'id'	=> 'training_plans_link',
			'name'	=> __( 'Training Plans Link', 'cgi' ),
			'desc'	=> __( 'Enter full URL link for the Training Plans Landing page', 'cgi' ),
			'type'	=> 'text_url',
		) );

	}

}
