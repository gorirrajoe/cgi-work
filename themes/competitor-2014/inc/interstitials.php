<?php
/**
 * Interstitials Class
 * For all related to the Interstitial Ads
 */
class Interstitials {

	public static $instance	= false;

	public static $show_interstitial		= false;
	public static $show_interstitial_mobile	= false;
	public static $show_interstitial_nl		= false;
	public static $show_interstitial_fb		= false;
	public static $show_tracking_pixel		= false;
	public static $cookie_name				= '';
	public static $cookie_time				= '';
	public static $target					= null;
	public static $type						= null;
	public static $is_mobile				= false;

	public function __construct() {

		self::$is_mobile	= function_exists('is_mobile') ? is_mobile() : false;

		$this->_add_actions();

	}

	/**
	 * Save the interstitial cookie
	 *
	 * @param string	$prefix		Decide how to name the interstitial (facebook/mobile).
	 * @param integer	$exp_quant	The quantity for the time duration of cookie.
	 * @param string	$exp_unit	The unit of measurement for the cookie expiration.
	 *
	 * @return Boolean 				Whether or not the cookie was set (to determine whether or not to display interstitial).
	 */
	public static function set_cookie( $prefix = '', $exp_quant = 30, $exp_unit = 'minute' ) {

		/*$cookie_name = 'interstitial';
		if ( !isset( $_COOKIE[$cookie_name] ) ) {
			// interstitial will show up every 30 minutes on new page load
			setcookie( $cookie_name, 1, time()+60*30, $cookie_path, $cookie_domain, false );
		}*/

		// let's check if the cookie already exists, because if it does...then why run any more code?!
		$cookie_name		= ( $prefix != '' ) ? $prefix . '-interstitial' : 'interstitial';
		self::$cookie_name	= $cookie_name;

		if ( !isset( $_COOKIE[$cookie_name] ) ) {

			switch ( $exp_unit ) {

				case 'seconds':
					$time_constant = 1;
					break;
				case 'minute':
					$time_constant = MINUTE_IN_SECONDS;
					break;
				case 'hour':
					$time_constant = HOUR_IN_SECONDS;
					break;
				case 'day':
					$time_constant = DAY_IN_SECONDS;
					break;
				case 'week':
					$time_constant = WEEK_IN_SECONDS;
					break;
				case 'year':
					$time_constant = YEAR_IN_SECONDS;
					break;
				default:
					$time_constant = 1;

			}

			// check for .com, if not found then save the cookie (I want to see the cookie every time in local at least)
			// $cookie_domain	= ( strpos( $_SERVER['HTTP_HOST'], '.com' ) !== false ) ? str_replace( 'http://', '', home_url() ) : false;
			$cookie_path	= '/';
			$cookie_exp		= $exp_quant * $time_constant;
			$cookie_domain	= preg_replace( '#^http(s)?://#', '', home_url() );


			// setcookie( $cookie_name, 1, time() + $cookie_exp, $cookie_path, $cookie_domain, false );

			// Declare function which will set the cookie
			/*echo '<script>
				function setInterstitialCookie() {
					var d = new Date();
					d.setTime( d.getTime() + ( 1000*' . $cookie_exp . ' ) );
					document.cookie = "' . $cookie_name . '=1; path=' . $cookie_path . ';expires=" + d.toUTCString();
				}
				</script>';*/

			return true;

		} else {

			return false;

		}

	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if( !self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Logic and calls to generate the proper interstitial
	 */
	public static function setup_interstitial() {

		// Get and set up all our options
		$technical_theme_options		= get_option( 'cgi_media_technical_options' );

		$interstitial_option			= !empty( $technical_theme_options['interstitial_ad'] ) ? true : false;
		$interstitial_mobile_option		= !empty( $technical_theme_options['interstitial_mobile'] ) ? true : false;
		$interstitial_nl_option			= !empty( $technical_theme_options['newsletter_interstitial_ad'] ) ? true : false;
		$interstitial_fb_option			= !empty( $technical_theme_options['facebook_interstitial_ad'] ) ? true : false;

		// Set up included/excluded categories
		$cat_array					= !empty( $technical_theme_options['interstitial_ad_cat'] ) ? explode( ',', $technical_theme_options['interstitial_ad_cat'] ) : array();
		$exclude_cat_array			= !empty( $technical_theme_options['interstitial_ad_excl_cat'] ) ? explode( ',', $technical_theme_options['interstitial_ad_excl_cat'] ) : array();
		$exclude_page_array			= !empty( $technical_theme_options['interstitial_ad_excl_page'] ) ? explode( ',', $technical_theme_options['interstitial_ad_excl_page'] ) : array();
		$cat_array_mobile			= !empty( $technical_theme_options['interstitial_mobile_cat'] ) ? explode( ',', $technical_theme_options['interstitial_mobile_cat'] ) : array();
		$exclude_cat_array_mobile	= !empty( $technical_theme_options['interstitial_mobile_excl_cat'] ) ? explode( ',', $technical_theme_options['interstitial_mobile_excl_cat'] ) : array();

		// Set up variables for the cookie times
		$interstitial_cookie_time			= !empty( $technical_theme_options['interstitial_exp_time'] ) ? $technical_theme_options['interstitial_exp_time'] : 0;
		$interstitial_mobile_cookie_time	= !empty( $technical_theme_options['interstitial_mobile_exp_time'] ) ? $technical_theme_options['interstitial_mobile_exp_time'] : 0;

		// Determine whether or not we should attempt to display the desktop size interstitial
		// Bear with me, the logic is weird, so I threw in all the comments to explain as much as possible
		do {

			// If the interstitial option is not on or the cookie was already set
			// break and do not set the variables
			if ( !$interstitial_option || isset( $_COOKIE['interstitial'] ) )
				break;

			// If we're here...
			// If the Cat array is not empty, it means we ONLY want to show the interstitial in the comma delimited categories
			// If the field is not empty, but we are NOT in one of the categories or the single post is NOT in one of those categories
			// break and do not set the variables
			if ( !empty( $cat_array ) && ( !is_category( $cat_array ) || !in_category( $cat_array ) ) )
				break;

			// If we're here...
			// If the Exclude Cat array is not empty, it means we want to show the interstitial everywhere EXCEPT these categories
			// If the field is not empty, but we are in one of the categories or the single post is in one of those categories
			// break and do not set the variables
			if ( !empty( $exclude_cat_array ) && ( is_category( $exclude_cat_array ) || in_category( $exclude_cat_array ) ) )
				break;

			// If we're here...
			// Now we check if we are trying to exclude the the interstitial on specific pages
			// If the field is not empty, and we are on one of those pages, BAIL!
			// break and do not set the variables
			if ( !empty( $exclude_page_array ) && is_page( $exclude_page_array ) )
				break;

			// If we've made it this far...WE'RE POSSIBLY GOING TO SHOW THE DESKTOP INTERSTITIAL
			// (POSSIBLY because we'll be checking if we show it through JS due to screen size)
			self::$show_interstitial	= true;
			self::$target				= '/interstitial-ad';
			self::$type					= 'iframe';
			self::$cookie_name			= 'interstitial';
			self::$cookie_time			= '1000 * 60 * ' . $interstitial_cookie_time; // In string format for JS to set

		} while (0);

		// Determine whether or not we should attempt to display the mobile size interstitial
		// Again, bear with me, ...sorry, forgive me! I didn't come up with these scenarios
		do {

			// If the interstitial option is not on or the cookie was already set
			// break and do not set the variables
			if ( !$interstitial_mobile_option || isset( $_COOKIE['interstitial'] ) )
				break;

			// If we're here...
			// If the Cat array is not empty, it means we ONLY want to show the interstitial in the comma delimited categories
			// If the field is not empty, but we are NOT in one of the categories or the single post is NOT in one of those categories
			// break and do not set the variables
			if ( !empty( $cat_array_mobile ) && ( !is_category( $cat_array_mobile ) || !in_category( $cat_array_mobile ) ) )
				break;

			// If we're here...
			// If the Exclude Cat array is not empty, it means we want to show the interstitial everywhere EXCEPT these categories
			// If the field is not empty, but we are in one of the categories or the single post is in one of those categories
			// return and do not set the variables
			if ( !empty( $exclude_cat_array_mobile ) && ( is_category( $exclude_cat_array_mobile ) || in_category( $exclude_cat_array_mobile ) ) )
				break;

			// If we've made it this far...WE'RE POSSIBLY GOING TO SHOW THE MOBILE INTERSTITIAL
			// (POSSIBLY because we'll be checking if we show it through JS due to screen size)
			self::$show_interstitial_mobile	= true;
			self::$target					= '/interstitial-ad';
			self::$type						= 'iframe';
			self::$cookie_name				= 'interstitial';
			self::$cookie_time				= '1000 * 60 * ' . $interstitial_mobile_cookie_time; // In string format for JS

		} while (0);

		// Determine whether or not we should attempt to display the email interstitial
		do {

			// If the Newsletter interstitial option isn't even on, don't bother
			if ( !$interstitial_nl_option )
				break;

			// Let's make sure our page even exists
			if ( !get_page_by_path( 'interstitial-ad-newsletter' ) )
				break;

			// If the cookie is already there, stop trying to make it happen, it's not going to happen
			if ( isset( $_COOKIE['newsletter-interstitial'] ) )
				break;

			// If we are trying to show the regular interstitial, don't set anything either
			if ( self::$show_interstitial )
				break;

			// WE'RE POSSIBLY GOING TO SHOW THE FACEBOOK INTERSTITIAL
			// (POSSIBLY because we'll be checking if we show it through JS due to screen size)
			self::$show_interstitial_nl		= true;
			self::$target					= '/interstitial-ad-newsletter';
			self::$type						= 'iframe';
			self::$cookie_name				= 'newsletter-interstitial';
			self::$cookie_time				= '0'; // In string format for JS to set (session)

		} while (0);

		// Determine whether or not we should attempt to display the Facebook interstitial
		// This one's not as complicated... :D
		do {

			// If the FB interstitial option isn't even on, don't bother
			if ( !$interstitial_fb_option )
				break;

			// If it's the front/home page, nope, shaniqua don't live here no more
			if ( is_home() || is_front_page() )
				break;

			// Let's make sure our page even exists
			if ( !get_page_by_path( 'interstitial-ad-facebook' ) )
				break;

			// If the cookie is already there, do not pass go, do not collect $200
			if ( isset( $_COOKIE['facebook-interstitial'] ) )
				break;

			// If we are trying to show the regular interstitial, don't set anything either
			if ( self::$show_interstitial )
				break;

			// If we are trying to show the newsletter interstitial, don't set it either, newsletter gets priority
			if ( self::$show_interstitial_nl )
				break;

			// WE'RE POSSIBLY GOING TO SHOW THE FACEBOOK INTERSTITIAL
			// (POSSIBLY because we'll be checking if we show it through JS due to screen size)
			self::$show_interstitial_fb		= true;
			self::$target					= '/interstitial-ad-facebook';
			self::$type						= 'iframe';
			self::$cookie_name				= 'facebook-interstitial';
			self::$cookie_time				= '1000 * 60 * 60 * 24 * 7'; // In string format for JS to set (one week)

		} while (0);

		/**
		 * Set interstitial cookies when ads are off, but tracking impressions for inventory purposes capitalization
		 */
		$_ENV['sictp'] = 858;
		if ( ( !$interstitial_option || !$interstitial_mobile_option ) && !isset( $_COOKIE['interstitial'] ) ) {
			self::set_cookie();
			self::$show_tracking_pixel	= true;
			$_ENV['sictp'] = 1;

		} else {
			$_ENV['sictp'] = 23;
		}

	}

	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this theme.
	 */
	protected function _add_actions() {

		// front-end hooks
		add_action( 'wp_head', array( $this, 'setup_interstitial' ), 2 );
		add_action( 'wp_footer', array( $this, 'generate_functionality_markup' ), 20 );

	}

	/**
	 * Generate the markup needed to launch interstitial
	 */
	public static function generate_functionality_markup() {

		if ( !isset( $_COOKIE[self::$cookie_name] ) && ( self::$show_interstitial || self::$show_interstitial_mobile || self::$show_interstitial_fb || self::$show_interstitial_nl ) ) {
			// Determine href target and attach to anchor link
			$target_string	= self::$target == null ? '#interstitial-ad' : self::$target; ?>

			<script type="text/javascript">

				$( function () {

					function interstitialEnable() {

						if ( document.cookie.indexOf('<?php echo self::$cookie_name; ?>') == -1 ) {

							// Determine size of ad to display
							var $window_width	= $(window).width(),
								ad_width		= '250',
								ad_height		= '208',
								ad_target		= '<?php echo self::$target; ?>',
								show_small		= <?php echo self::$show_interstitial_mobile ? 'true' : 'false'; ?>,
								show_large		= <?php echo self::$show_interstitial ? 'true' : 'false'; ?>,
								show_nl			= <?php echo self::$show_interstitial_nl ? 'true': 'false'; ?>,
								show_fb			= <?php echo self::$show_interstitial_fb ? 'true': 'false'; ?>;

							// decide our window width depending on browser width
							if ( $window_width > 650 ) {

								if ( show_large) {
									ad_width	= '640';
									ad_height	= '480';
								} else if ( show_fb ) {
									ad_width	= '720';
									ad_height	= '231';
								} else if ( show_nl ) {
									ad_width	= '640';
									ad_height	= '300';
								}

							}

							// Depending on our logic AND browser width, determine whether or not to show one of the ads
							if ( ( $window_width > 650 && ( show_large || show_fb || show_nl ) ) || ( $window_width <= 650 && show_small ) ) {

								$('#interstitial').attr( 'data-src', ad_target +'?width='+ ad_width +'&height='+ ad_height );

								$( '#interstitial' ).fancybox( {
									buttons: false,
									iframe: {
										preload: true,
										css: {
											width: ad_width+'px',
											height: ad_height+'px'
										}

									},
									afterLoad: function( instance, slide ) {
										// Add the circle closing button to interstitial
										slide.$content
											.append('<a data-fancybox-close class="button-close--interstitial" href="javascript:;">&times;</a>')
											.addClass('fancybox-content--interstitial');

										// Set the cookie
										var d = new Date();
										var cookie_time	= <?php echo self::$cookie_time; ?>;

										d.setTime( d.getTime() + cookie_time );

										var expiration_string	= ( cookie_time != 0 ) ? 'expires=' + d.toUTCString() : '';

										document.cookie			= "<?php echo self::$cookie_name; ?>=1; path=/;" + expiration_string;

										// Set timeout to close interstitial for standard and FB interstitials
										if ( !show_nl ) {

											var t = setTimeout(
												function() { $.fancybox.close(); },
												10000
											);

										}
									},
									<?php if ( self::$is_mobile ) : ?>
									beforeLoad: function () {
										if ( !$('body').hasClass( 'fancybox-enabled' ) ) {
											$('body').addClass( 'fancybox-enabled' );
										}
									},
									beforeClose: function() {
										$('body').removeClass( 'fancybox-enabled' );
									}
									<?php endif; ?>

								} ).trigger( 'click' );

							}

						}

					}

					// only launch if adblockers aren't present, ad blocks cause FOC and temp inaccessibility to the site
					if ( typeof blockAdBlock !== 'undefined' ) {
						blockAdBlock.on(false, interstitialEnable);
					}

				} );

			</script>
			<a id="interstitial" data-fancybox data-src="<?php echo self::$target; ?>" href="javascript:;" style="display: none;">Interstitial</a>

		<?php } elseif ( self::$show_tracking_pixel ) {
			$adKw = class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::singleton()->generate_dfp_keywords() : '';
			?>

			<script>

				$( function () {
					var $img	= $('<img />');

					// Build the image src
					var $window_width	= $(window).width(),
						ad_width		= '250',
						ad_height		= '208',
						d				= new Date(),
						time			= d.getHours() + d.getMinutes() + d.getSeconds() + d.getMilliseconds();

					if ( $window_width > 650 ) {
						ad_width	= '640';
						ad_height	= '480';
					}

					d.strftime('%H%M%S')

					var img_src	= 'http://pubads.g.doubleclick.net/gampad/ad?iu=';

					img_src	+= '<?php echo $adKw; ?>';
					img_src	+= '&sz'+ ad_width +'x'+ ad_height;
					img_src	+= '&c='+ time;

					$img
						.attr( 'src', img_src )
						.css( 'display', 'none' );

					$('body').append( $img );
				} );

			</script>

			<?php
		}

	}

}
