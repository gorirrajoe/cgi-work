<?php
/**
 * File to hold all scripts in the wp_head
 */

class Header_Scripts {

	public static $instance = false;

	public static $technical_theme_options;

	public static $editorial_theme_options;

	public function __construct() {

		// Get options and store for use in functions
		self::$technical_theme_options	= get_option( 'cgi_media_technical_options' );
		self::$editorial_theme_options	= get_option( 'cgi_media_editorial_options' );

		add_action( 'wp_head', array( $this, 'add_fonts_asynchronously' ), 0 );
		add_action( 'wp_head', array( $this, 'add_social_slider_style' ) );
		add_action( 'wp_head', array( $this, 'add_google_analytics' ) );
		add_action( 'wp_head', array( $this, 'add_google_tag_head' ) );
		add_action( 'wp_head', array( $this, 'add_datalayer' ) );
		add_action( 'wp_head', array( $this, 'add_social_media_script' ) );

		/**
		 * Handle favicon
		 * 		prevent wp_site_icon() from adding favicons to the wp_head
		 * 		we already have a function that handles that
		 * 		must add an image to the customizer so that AMP can work properly
		 */
		remove_action( 'wp_head', 'wp_site_icon', 99 );
		add_action( 'wp_head', array( $this, 'generate_favicon' ) );

	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( !self::$instance )
			self::$instance = new self;

		return self::$instance;

	}

	/**
	 * Adding Google fonts asynchronously because it blocks page loading if loaded old fashion way
	 * @import url(http://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300,300italic,600italic,600);
	 */
	public function add_fonts_asynchronously() { ?>

		<script>
			WebFontConfig = {
				classes: false,
				events: false,
				google: {
					families: ['Open+Sans:400,400italic,300,300italic,600italic,600']
				},

			};

			(function(d) {
				var wf = document.createElement('script');
				wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
					'://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js';
				wf.type = 'text/javascript';
				wf.async = 'true';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(wf, s);
			})(document);
		</script>

		<?php
	}

	function add_social_slider_style() {

		if ( !empty( self::$technical_theme_options['show_social_slider'] ) ) { ?>
			<style>
				#UISideShareBar {
					display: none;
				}
				.UISide-sharebar {
					background: -webkit-gradient(linear, center top, center bottom, from(#ebebeb), to(#e9e9e9) ) repeat scroll 0 0 transparent;
					background: -moz-linear-gradient(center top , #EbEbEb 0pt, #E9E9E9 100%) repeat scroll 0 0 transparent;
					border: 1px solid #ccc;
					border-radius: 5px;
					-moz-border-radius: 5px;
					height: 82px;
					margin: 0 0 0 -85px;
					top: 270px;
					width: 89px;
					position: absolute;
					z-index:9999;
				}
				.UISide-sharebar .UIShareBar-twitter {
					position: absolute;
					left: 5px;
					top: 30px;
				}
				.UISide-sharebar .UIShareBar-fb {
					position: absolute;
					left: 5px;
					top: 6px;
				}
				.UISide-sharebar .UIShareBar-gplus {
					position: absolute;
					display: block;
					left: 5px;
					top: 55px;
				}
				.UISide-sharebar .UIShareBar-follow-fb {
					position: absolute;
					left: 8px;
					top: 200px;
				}
				@media only screen and (min-width: 1176px) {
					#UISideShareBar {
						display: block;
					}
				}
			</style>

		<?php }

	}

	function add_google_analytics() {

		// NEW GA coding to include Author
		if ( !empty( self::$technical_theme_options['google_analytics'] ) ) {

			$getid		= get_post_field( 'post_author', get_the_ID() );
			$user_info	= get_userdata( $getid );
			$wp_author	= ( isset( $user_info->user_login ) ? $user_info->user_login : '' );
			?>

			<script type="text/javascript">
				var _gaq = _gaq || [];
				var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
				_gaq.push(
					['_require', 'inpage_linkid', pluginUrl],
					['_setAccount', '<?php echo self::$technical_theme_options['google_analytics'];?>'],
					['_setDomainName', '.competitor.com'],
					['_setAllowLinker', true],
					['_setCustomVar',1,'wp_author','<?php echo $wp_author ?>',3],
					['_trackPageview']
				 );
				(function() {
					var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
				})();
			</script>

		<?php }

	}

	function add_google_tag_head() {

		if ( !empty( self::$technical_theme_options['google_tag_head'] ) ) {
			echo self::$technical_theme_options['google_tag_head'];
		}

	}

	function add_datalayer() {

		global $post;

		// Define dataLayer for site analytics
		if ( is_page() ) {
			global $page;
			$the_id	= get_queried_object_id();
		} elseif ( is_singular( 'post' ) ) {
			$multipage = ( strpos($post->post_content,'<!--nextpage-->') ) ? true : false;
			$the_id	= $post->ID;
		} else {
			$the_id	= '';
		}
		$category     = 'category';
		$wpAuthor     = isset($post) ? get_the_author_meta('user_nicename', $post->post_author) : '';
		$guestAuthor  = 'none';
		$hasVideo     = ( get_post_meta( $the_id, '_youtube_id', true ) == '' && get_post_meta($the_id, 'video_id', true) == '') ? 'no' : 'yes';
		$partnerName  = get_post_meta( $the_id, 'meta-text', true ) ? get_post_meta( $the_id, 'meta-text', true ) : '';

		$categories   = wp_get_post_categories($the_id, array('fields' => 'names')) ? wp_get_post_categories($the_id, array('fields' => 'names')) : array();

		if ( is_page() || is_404() || is_search() ) $category = 'page';
		if ( is_singular( 'post' ) ) {
			$category     = 'post';
			if($multipage){ $category .= ' multipage'; }
			$guestAuthor  = get_post_meta( $the_id, 'guest_author', true ) ? get_post_meta( $the_id, 'guest_author', true ) : 'none';
		}
		if ( in_array('Photos', $categories ) || ( isset($post) && has_shortcode( $post->post_content , 'gallery' ) ) ) $category = 'gallery';
		if ( in_array('Partner Connect', $categories ) || get_post_meta( $the_id, 'meta-text', true ) != '') $category = 'partner connect';

		//blue kai data layer info
		if ( $category != 'category' ) {
			$cat_dl			= wp_get_post_categories($the_id);
			$dl_sections	= '';

			if ( count( $cat_dl ) ){
				foreach ( $cat_dl as $c ) {
					$cat = get_category( $c );

					if ( strlen($dl_sections) > 1 ) {
						$dl_sections .= ',';
					}

					$dl_sections .= $cat->slug;
				}
			}

			$posttags	= get_the_tags();
			$dl_tags	= '';

			if ( $posttags ) {
				foreach ( $posttags as $tag ) {
					if ( strlen( $dl_tags ) > 1) {
						$dl_tags .= ',';
					}
					$dl_tags .= $tag->slug;
				}
			}

		} else {
			$dl_sections = get_queried_object()->slug;
			$dl_tags = '';
		}
		//End blue kai data layer
		?>
		<script type="text/javascript">
			window.dataLayer = window.dataLayer || [];
			window.dataLayer.push({
				'pageType': '<?php echo strtolower( $category ); ?>',
				'wpAuthor': '<?php echo $wpAuthor; ?>',
				'guestAuthor': '<?php echo strtolower( $guestAuthor ); ?>',
				'hasVideo': '<?php echo strtolower( $hasVideo ); ?>',
				'partnerConnectName': '<?php echo strtolower( $partnerName ); ?>',
				'category': '<?php echo strtolower($dl_sections); ?>',
				'tags': '<?php echo strtolower($dl_tags); ?>'
			});
		</script>
		<?php

	}

	function add_social_media_script() {

		if ( !empty( self::$technical_theme_options['social_media_publisher_key'] ) ) { ?>

		<script type="text/javascript">
			(function () {
				var s = document.createElement('script'),
					x = document.getElementsByTagName('script')[0];
				s.type = 'text/javascript';
				s.async = true;
				s.src = ('https:' == document.location.protocol ? 'https://s' : 'http://i')
					+ '.po.st/static/v4/post-widget.js#publisherKey=<?php echo self::$technical_theme_options['social_media_publisher_key']; ?>&retina=true';
				x.parentNode.insertBefore(s, x);

				window.pwidget_config = {
					shareQuote: false,
					onshare:bk_addSocialChannel,
					defaults: {
						gaTrackSocialInteractions: true,
						retina: true,
						mobileOverlay: true,
						afterShare: false,
						sharePopups: true,
						copypaste: false
					}
				};

				function gaTrack(network, action) {
					_gaq.push(['_trackSocial', network, action]);
				}
				//Populate BlueKai social sharer
				function bk_addSocialChannel(channelName) {
					bk_addPageCtx('share', channelName);
					BKTAG.doTag(39226,1);
				}

			})();
		</script>

		<?php }

	}

	/**
	 * Generate Favicon
	 *
	 * Generates the markup for the favicon.
	 * Passed into wp_head.
	 */
	public function generate_favicon() {
		$pathtofavicon = get_template_directory_uri() .'/images/favicon'; ?>

		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $pathtofavicon; ?>/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $pathtofavicon; ?>/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $pathtofavicon; ?>/favicon-16x16.png">
		<link rel="manifest" href="<?php echo $pathtofavicon; ?>/manifest.json">
		<link rel="mask-icon" href="<?php echo $pathtofavicon; ?>/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="theme-color" content="#ffffff">

		<?php
	}

}
