<?php
/* Template Name: Partner Showcase Automated Newsletter */

// Load options and display accordingly
$technical_theme_options = get_option( 'cgi_media_technical_options' );
$editorial_theme_options = get_option( 'cgi_media_editorial_options' );

// Title and logo
$newsletter_title = get_the_title();
$logo = get_bloginfo('stylesheet_directory') . '/images/newsletter/logo.jpg';
$facebook = $technical_theme_options['site_facebook_page'];
if ( $facebook != '' ) {
	$facebook = '<a href="https://www.facebook.com/' . $technical_theme_options['site_facebook_page'] . '" style="text-decoration: none;color: '.$brand_color.';"><img src="' . get_bloginfo( 'template_url' ). '/images/newsletter/facebook.png"/></a>';
}
$twitter = $technical_theme_options['site_twitter_handle'];
if ( $twitter != '' ) {
	$twitter = '<a href="https://twitter.com/' . $technical_theme_options['site_twitter_handle'] . '" style="text-decoration: none;color: '.$brand_color.';"><img src="' . get_bloginfo( 'template_url' ). '/images/newsletter/twitter.png"/></a>';
}
$twitter_share = get_bloginfo( 'template_url' ).'/images/newsletter/twitter-inverse.png';
$facebook_share = get_bloginfo( 'template_url' ).'/images/newsletter/facebook-inverse.png';
$newsletter_sponsor_image = $editorial_theme_options['newsletter_sponsor_image'];
$newsletter_sponsor_link = $editorial_theme_options['newsletter_sponsor_link'];
//$newsletter_title = 'Partner Showcase';
//$brand_color = '#003e99';
// brand colors

$blogurl = explode ('.', get_option( 'home' ));
if ($blogurl[0] == 'http://running') {
	$brand_color = '#003e99';
	$campaign_var = 'partner_showcase_competitor';
	$newsletter_title_pc = 'Partner Showcase Competitor';
} elseif ($blogurl[0] == 'http://triathlon') {
	$brand_color = '#9c399c';
	$campaign_var = 'partner_showcase_triathlete';
} elseif ($blogurl[0] == 'http://velonews') {
	$brand_color = '#c01100';
	$campaign_var = 'partner_showcase_velo';
}

// Setup ad codes
$ad_code_newsletter_name = strtolower(str_replace(' ', '_', $newsletter_title_pc));

//old call
//$parnter_solution = '<a href="http://ad.doubleclick.net/N8221/jump/'.$campaign_var.';dcov=r;sz=615x76;ord=12345?"><img src="http://ad.doubleclick.net/N8221/ad/'.$campaign_var.';dcov=r;sz=615x76;ord=12345?" width="615" height="76" /></a>';

//new call
$parnter_solution = '<a href="http://pubads.g.doubleclick.net/gampad/jump?iu=/8221/partner_showcase_competitor&sz=615x76&c=123454321"><img src="http://pubads.g.doubleclick.net/gampad/ad?iu=/8221/partner_showcase_competitor&sz=615x76&c=123454321" width="615" height="76" /></a>';

// Post ID's to keep from duplicates showing up
$post_id_array = array();

/* this array requires 5 post ids, a query will run for
 each to populate the newsletter */
//$newsletter_posts = '';
$newsletter_posts = array();

if ($editorial_theme_options['partner_showcase_newsletter_posts']) {
	$newsletter_posts = explode( ',' , $editorial_theme_options['partner_showcase_newsletter_posts'] );
}
if(!empty($editorial_theme_options['partner_showcase_newsletter_logo_image'])){
	$header_image = $editorial_theme_options['partner_showcase_newsletter_logo_image'];
}else{
	$header_image = $logo;
}
if(!empty($editorial_theme_options['partner_showcase_newsletter_logo_link'])){
	$header_image_link = $editorial_theme_options['partner_showcase_newsletter_logo_link'];
}else{
	$header_image_link = get_option('home');
}
	/*
if($_GET['list-name'] == 'training'){
	if ($editorial_theme_options['newsletter_train_posts']) {
		$newsletter_posts = explode( ',' , $editorial_theme_options['newsletter_train_posts'] );
	}
	if(!empty($editorial_theme_options['newsletter_train_logo_image'])){
		$header_image = $editorial_theme_options['newsletter_train_logo_image'];
	}else{
		$header_image = $logo;
	}
	if(!empty($editorial_theme_options['newsletter_train_logo_link'])){
		$header_image_link = $editorial_theme_options['newsletter_train_logo_link'];
	}else{
		$header_image_link = get_option('home');
	}
}elseif($_GET['list-name'] == 'shoes-and-gear'){
	if ($editorial_theme_options['newsletter_shoe_posts']) {
		$newsletter_posts = explode( ',' , $editorial_theme_options['newsletter_shoe_posts'] );
	}
	if(!empty($editorial_theme_options['newsletter_shoe_logo_image'])){
		$header_image = $editorial_theme_options['newsletter_shoe_logo_image'];
	}else{
		$header_image = $logo;
	}
	if(!empty($editorial_theme_options['newsletter_shoe_logo_link'])){
		$header_image_link = $editorial_theme_options['newsletter_shoe_logo_link'];
	}else{
		$header_image_link = get_option('home');
	}
}elseif($_GET['list-name'] == 'nutrition'){
	if ($editorial_theme_options['newsletter_nut_posts']) {
		$newsletter_posts = explode( ',' , $editorial_theme_options['newsletter_nut_posts'] );
	}
	if(!empty($editorial_theme_options['newsletter_nut_logo_image'])){
		$header_image = $editorial_theme_options['newsletter_nut_logo_image'];
	}else{
		$header_image = $logo;
	}
	if(!empty($editorial_theme_options['newsletter_nut_logo_link'])){
		$header_image_link = $editorial_theme_options['newsletter_nut_logo_link'];
	}else{
		$header_image_link = get_option('home');
	}
}else{
	if ($editorial_theme_options['newsletter_best_posts']) {
		$newsletter_posts = explode( ',' , $editorial_theme_options['newsletter_best_posts'] );
	}/*elseif ($editorial_theme_options['newsletter_posts']) {
	$newsletter_posts = explode( ',' , $editorial_theme_options['newsletter_posts'] );
	// print_r( $newsletter_posts);

	if(!empty($editorial_theme_options['newsletter_best_logo_image'])){
		$header_image = $editorial_theme_options['newsletter_best_logo_image'];
	}else{
		$header_image = $logo;
	}
	if(!empty($editorial_theme_options['newsletter_best_logo_link'])){
		$header_image_link = $editorial_theme_options['newsletter_best_logo_link'];
	}else{
		$header_image_link = get_option('home');
	}
}/*else{
if ($editorial_theme_options['newsletter_posts']) {
$newsletter_posts = explode( ',' , $editorial_theme_options['newsletter_posts'] );
// print_r( $newsletter_posts);
}
$header_image = $logo;
$header_image_link = get_option('home');
}*/

$sticky = get_option( 'sticky_posts' );
// Recent stories for sidebar
$args = array(
		'post_type' => 'post',
		'post__not_in' => $sticky,
		'posts_per_page' => 12,
);
$original_query = $wp_query;
$wp_query = null;
$wp_query = wp_cache_get( 'newsletter_latest_posts' );
if ($wp_query == TRUE) {
	if ($wp_query->have_posts() === false) {
		$wp_query = new WP_Query($args);
		wp_cache_replace( 'newsletter_latest_posts', $wp_query, '', 600 );
	}
} else {
	$wp_query = new WP_Query($args);
	wp_cache_set( 'newsletter_latest_posts', $wp_query, '', 600 );
}
$count = 0;
$latest_news = '<table cellspacing="0" border="0" cellpadding="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">';
while ( $wp_query->have_posts() ) : $wp_query->the_post();
if(!in_array(get_the_ID(), $newsletter_posts) && $count < 5 ){
	$post_id_array[] = get_the_ID();
	$title = get_the_title();
	//$title = (strlen(get_the_title()) > 34) ? substr(get_the_title(),0,31).'...' : get_the_title();

	$link = wp_get_shortlink();
	$latest_news .= '<tr><td height="20" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; padding-top: 5px;"><img src="' . get_bloginfo( 'template_url' ) . '/images/newsletter/bullet.gif" alt="" style="border: 0;" width="15" height="15" /></td><td class="list" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; padding-top: 5px;"><a href = "'. $link .'" style="color: '.$brand_color.'; font-family: Arial; font-size: 13px; text-decoration: none;">' . $title . '</a></td></tr>';
	$count++;
}
endwhile;
$latest_news .= "</table>";
$wp_query = null;
$wp_query = $original_query;
wp_reset_postdata();



// Get 5 Stories in order which gives editors control over the main stories in Newsletter
// $stories = '<table cellspacing="0" style="border-right: 1px solid #ccc;" cellpadding="0">';
for($count = 0; $count < 6; $count++){
	$args = array(
			'p' => $newsletter_posts[$count]
	);
	// print_r($args);
	$original_query = $wp_query;
	$wp_query = NULL;

	$wp_query = new WP_Query($args);
	if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();
	if(!in_array(get_the_ID(), $post_id_array)){
		$post_id_array[] = get_the_ID();
		$title_link = '<a href="'. wp_get_shortlink() .'" title="Read full story" style="font-family: Arial, sans-serif; font-size:21px; line-height:24px; color:#333; margin:0; font-weight:bold; text-decoration:none;">' .  get_the_title() . '</a>';
		$excerpt = get_the_excerpt();
		if ($count == 0) {
			$image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'newsletter-splash');
			$image_link = '<a href="'. wp_get_shortlink() .'" title="Read full story" style="text-decoration: none; color: '.$brand_color.';"><img src="' . $image_array[0] . '" class="max-size" width="620" height="400" /></a>';

			$stories .= '
			<tr>
			<td width="100%" class="text-center" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">' . $image_link . '</td>
			</tr>
			<tr>
			<td width="100%" bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
			';
				if ( $post->post_excerpt) {
					$stories .= '
						<table width="100%" align="center" cellpadding="15" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
						<tr>
						<td width="100%" style="font-family:Arial, sans-serif;font-size:15px;line-height:20px;color:#555; border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
						' . $title_link . '
						<br>
						<span>' . $excerpt . '</span>
						<br /><br />
						<a href="'.wp_get_shortlink().'" class="button" style="color:'.$brand_color.';font-family: Arial, sans-serif;font-size:13px;line-height:19px;">Find out more</a>
						</td>
						</tr>
						</table>';
				}
			$stories .= '
			<table width="180" align="right" cellpadding="15" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
			<tr>
			<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><span>Share:</span></td>
			<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><a  target="_blank" href="http://twitter.com/intent/tweet?original_referer=&text='.get_the_title().'&url='.wp_get_shortlink().'"><img src="'.$twitter_share.'" width="32" height="32"></a></td>

			<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><a href="https://www.facebook.com/sharer/sharer.php?u='.wp_get_shortlink().'" target="_blank"><img src="'.$facebook_share.'" width="32" height="32"></a></td>
			</tr>
			</table>
			</td>
			</tr>
			<tr>
			<td width="100%" height="20" bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br /></td>
			</tr>
			</table>
			</td>
			</tr>
			</table>
			';
		} elseif ($count < 5) {
			if ($count == 1) {
				$image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'newsletter-image');
				$image_link = '<a href="'. wp_get_shortlink() .'" title="Read full story" style="text-decoration: none; color: '.$brand_color.';"><img src="' . $image_array[0] . '" class="max-size" width="300" height="200" /></a>';

				$stories .= '
				<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="font-family:Arial, sans-serif;font-size:15px;line-height:20px;color:#555;">
				<tr>
				<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
				<table width="620" cellpadding="0" cellspacing="0" border="0" align="center" class="content_wrap" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
				<tr>
				<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
				<!-- content 2 -->
				<table class="full_width" width="300" align="left" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
				<tr>
				<td class="text-center" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
				'.$image_link.'
				</td>
				</tr>
				<tr>
				<td bgcolor="#eaeaea" width="100%" valign="top" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
				<table width="300" cellpadding="15" cellspacing="0" border="0" class="full_width" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
				<tr>
				<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; font-family:Arial, sans-serif;font-size:15px;line-height:20px;color:#555;">
				'.$title_link.'
				<br />
				<span>'.$excerpt.'</span>
				<br /><br />
				<a href="'.wp_get_shortlink().'" class="button" style="color:'.$brand_color.';font-family: Arial, sans-serif;font-size:13px;line-height:19px;">Find out more</a>
				</td>
				</tr>
				</table>
				<table width="200" align="right" cellpadding="15" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
				<tr>
				<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><span>Share:</span></td>
				<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><a target="_blank" href="http://twitter.com/intent/tweet?original_referer=&text='.get_the_title().'&url='.wp_get_shortlink().'"><img src="'.$twitter_share.'" width="32" height="32"></a></td>

				<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><a href="https://www.facebook.com/sharer/sharer.php?u='.wp_get_shortlink().'" target="_blank"><img src="'.$facebook_share.'" width="32" height="32"></a></td>
				</tr>
				</table>
				</td>
				</tr>
				<tr>
				<td width="100%" height="20" bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br /></td>
				</tr>
				</table>
				<!-- end content 2 -->

				';
			} elseif ($count == 2) {
				$image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'newsletter-image');
				$image_link = '<a href="'. wp_get_shortlink() .'" title="Read full story" style="text-decoration: none; color: '.$brand_color.';"><img src="' . $image_array[0] . '" class="max-size" width="300" height="200" /></a>';

				$stories .= '
				<!-- content 3 -->
				<table class="full_width" width="300" align="right" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
				<tr>
				<td class="text-center" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
				'.$image_link.'
				</td>
				</tr>
				<tr>
				<td bgcolor="#eaeaea" width="100%" valign="top" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
				<table width="300" cellpadding="15" cellspacing="0" border="0" class="full_width" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
				<tr>
				<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; font-family:Arial, sans-serif;font-size:15px;line-height:20px;color:#555;">
				'.$title_link.'
				<br />
				<span>'.$excerpt.'</span>
				<br /><br />
				<a href="'.wp_get_shortlink().'" class="button" style="color:'.$brand_color.';font-family: Arial, sans-serif;font-size:13px;line-height:19px;">Find out more</a>
				</td>
				</tr>
				</table>
				<table width="200" align="right" cellpadding="15" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
				<tr>
				<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><span>Share:</span></td>
				<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><a target="_blank" href="http://twitter.com/intent/tweet?original_referer=&text='.get_the_title().'&url='.wp_get_shortlink().'"><img src="'.$twitter_share.'" width="32" height="32"></a></td>

				<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><a href="https://www.facebook.com/sharer/sharer.php?u='.wp_get_shortlink().'" target="_blank"><img src="'.$facebook_share.'" width="32" height="32"></a></td>
				</tr>
				</table>
				</td>
				</tr>
				<tr>
				<td width="100%" height="20" bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br /></td>
				</tr>
				</table>
				<!-- end content 3 -->
				</td>
				</tr>
				</table>
				</td>
				</tr>
				<tr>
				<td style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
				<table width="100%" cellpadding="0" cellspacing="0" border="0" class="hide" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
				<tr>
				<td width="100%" height="10" bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br /></td>
				</tr>
				</table>
				</td>
				</tr>
				</table>
				';
			} elseif ($count == 3 || $count == 4) {
				$image_array = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'newsletter-image');
				$image_link = '<a href="'. wp_get_shortlink() .'" title="Read full story" style="text-decoration: none; color: '.$brand_color.';"><img src="' . $image_array[0] . '" class="max-size" width="300" height="200" /></a>';
				if ($count == 3) {
					$stories .= '
					<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="font-family:Arial, sans-serif;font-size:15px;line-height:20px;color:#555;">
					<tr>
					<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
					<table width="620" cellpadding="0" cellspacing="0" border="0" align="center" class="content_wrap" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
					<tr>
					<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
					<!--Content 4 & 5-->
					<!-- content 4 -->
					<table class="full_width" width="300" align="left" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
					<tr>
					<td width="100%" class="text-center" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
					'.$image_link.'
					</td>
					</tr>
					<tr>
					<td bgcolor="#eaeaea" width="100%" valign="top" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
					<table width="300" cellpadding="15" cellspacing="0" border="0" class="full_width" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
					<tr>
					<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; font-family:Arial, sans-serif;font-size:15px;line-height:20px;color:#555;">
					'.$title_link.'
					<br />
					<span>'.$excerpt.'</span>
					<br /><br />
					<a href="'.wp_get_shortlink().'" class="button" style="color:'.$brand_color.';font-family: Arial, sans-serif;font-size:13px;line-height:19px;">Find out more</a>
					</td>
					</tr>
					</table>
					<table width="200" align="right" cellpadding="15" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
					<tr>
					<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><span>Share:</span></td>
					<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><a target="_blank" href="http://twitter.com/intent/tweet?original_referer=&text='.get_the_title().'&url='.wp_get_shortlink().'"><img src="'.$twitter_share.'" width="32" height="32"></a></td>

					<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><a href="https://www.facebook.com/sharer/sharer.php?u='.wp_get_shortlink().'" target="_blank"><img src="'.$facebook_share.'" width="32" height="32"></a></td>
					</tr>
					</table>
					</td>
					</tr>
					<tr>
					<td width="100%" height="20" bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br /></td>
					</tr>
					</table>
					<!-- end content 4 -->
					';
				} elseif ($count == 4) {
					$stories .= '
					<!-- content 5 -->
					<table class="full_width" width="300" align="right" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
					<tr>
					<td width="100%" class="text-center" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
					'.$image_link.'
					</td>
					</tr>
					<tr>
					<td bgcolor="#eaeaea" width="100%" valign="top" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
					<table width="300" cellpadding="15" cellspacing="0" border="0" class="full_width" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
					<tr>
					<td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; font-family:Arial, sans-serif;font-size:15px;line-height:20px;color:#555;">
					'.$title_link.'
					<br />
					<span>'.$excerpt.'</span>
					<br /><br />
					<a href="'.wp_get_shortlink().'" class="button" style="color:'.$brand_color.';font-family: Arial, sans-serif;font-size:13px;line-height:19px;">Find out more</a>
					</td>
					</tr>
					</table>
					<table width="200" align="right" cellpadding="15" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
					<tr>
					<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><span>Share:</span></td>
					<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><a target="_blank" href="http://twitter.com/intent/tweet?original_referer=&text='.get_the_title().'&url='.wp_get_shortlink().'"><img src="'.$twitter_share.'" width="32" height="32"></a></td>

					<td bgcolor="#eaeaea" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><a href="https://www.facebook.com/sharer/sharer.php?u='.wp_get_shortlink().'" target="_blank"><img src="'.$facebook_share.'" width="32" height="32"></a></td>
					</tr>
					</table>
					</td>
					</tr>
					</table>
					<!-- end content 5 -->
					<!--end Content 4 & 5-->
					</td>
					</tr>
					<tr>
					<td width="100%" height="20" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br /></td>
					</tr>
					</table>
					';
				}
			}
		}
	}
	endwhile; else:
	$stories .= '<tr><td>Sorry, no posts match that category</td></tr>';
	endif;
}
$stories .= '</table>';
$wp_query  = NULL;
$wp_query = $original_query;
wp_reset_postdata();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
  <title><?php echo $newsletter_title; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <style>

    body {
      width:100%!important;
      -webkit-text-size-adjust:100%;
      -ms-text-size-adjust:100%;
      margin:0;
      padding:0;
    }
    .ExternalClass {
      width: 100%;
    }
    span {
      font-family:Arial, sans-serif;
      font-size:16px;
      line-height:20px;
      color:#555;
    }
    table, td {
      border-collapse:collapse;
      mso-table-lspace:0pt;
      mso-table-rspace:0pt;
    }
    img{
        border:0;
        line-height:100%;
        outline:none;
        text-decoration:none;
    }
    img.max-size {
      height:auto!important;
    }
    img#header-logo {
    	margin-right: 0px;
    }
    img#sponsor-logo {
    	padding: 2px 0px;
    	margin: 2px 0px;
    }
    @media only screen and (max-width: 480px) {
      body,table,td,p,a,li,blockquote{
        -webkit-text-size-adjust:none !important;
      }
    }
    @media only screen and (max-width: 480px) {
      table[class=content_wrap] {
        width: 94%!important;
      }
    }
    @media only screen and (max-width: 480px) {
      table[class=full_width] {
        width: 100%!important;
      }
    }
    @media only screen and (max-width: 480px) {
      img[class=max-size] {
        width: 100%!important;
        height:auto;
      }
    }
    @media only screen and (max-width: 480px) {
      img[class=hide] {
        display: none!important;
      }
    }
    @media only screen and (max-width: 480px) {
      td[class=hide] {
        display: none!important;
      }
    }
    @media only screen and (max-width: 480px) {
      table[class=show] {
        display:inline!important;
      }
    }
    @media only screen and (max-width: 480px) {
      img[class=show] {
        display:inline!important;
      }
    }
    @media only screen and (max-width: 480px) {
      td[class=text-center] {
        text-align: center!important;
      }
    }
    @media only screen and (max-width: 480px) {
      td[class=text-left] {
        text-align: left!important;
      }
    }
    @media only screen and (max-width: 480px) {
      a[class=button] {
        border-radius:2px;
        background-color:<?php echo $brand_color; ?>;
        color:#fff!important;
        padding: 5px;
        text-align:center;
        display:block;
        text-decoration: none;
        text-transform: uppercase;
        margin: 0 0 10px 0;
      }
    }
  </style>
</head>
<body bgcolor="#ffffff" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="-webkit-font-smoothing:antialiased;width:100% !important;background-color:#ffffff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;-webkit-text-size-adjust:none;">
  <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="font-family:Arial, sans-serif;font-size:15px;line-height:20px;color:#555;">

    <tr>
      <td width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">

        <!--Head banner-->
        <!--Content wrapper-->
        <table width="620" cellpadding="0" cellspacing="0" border="0" align="center" class="content_wrap" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
          <tr>
            <td width="100%" bgcolor="#2F353E" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
              <table align="center" cellpadding="5" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                <tr>
                  <td class="text-center" bgcolor="#2F353E" width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt; font-size:14px;font-family:Arial,sans-serif;">
                    <a href="[webversionurl]" style="color:#f2f2f2;text-decoration:none;font-weight:bold;">View in browser</a>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
            <td width="100%" height="20" bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br /></td>
          </tr>
              <tr>
            <td width="100%" bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
              <!-- header -->
              <table align="left" width="500" class="full_width" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                <tr>
                  <!--Logo-->
                  <td bgcolor="#ffffff" width="453" class="text-center" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                    <a href="<?php echo $header_logo_link; ?>"><img id="header-logo" src="<?php echo $header_image;?>" width="210" height="75" title="<?php echo get_bloginfo('name'); ?> Newsletter" alt="<?php echo get_bloginfo('name'); ?> Newsletter" border="0" /></a>
                    <?php /* removing - not needed for now
                  if(isset($newsletter_sponsor_image) AND $newsletter_sponsor_image != ''){
                  	if(isset($newsletter_sponsor_link) AND $newsletter_sponsor_link != ''){
                  		echo "<a href='".$newsletter_sponsor_link."' target='_blank'>";
                  	}
                  	echo "<img id='sponsor-logo' src='".$newsletter_sponsor_image."' width='175' height='40' border='0' />";
                  	if(isset($newsletter_sponsor_link) AND $newsletter_sponsor_link != ''){
                  		echo "</a>";
                  	}
                  } */
                  ?>
                </td>
              </tr>
              </table>
              <table class="full_width" width="100" height="44" cellpadding="0" cellspacing="0" border="0" align="right" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                <tr>
                  <td align="right" width="20" class="text-left" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                  </td>
                  <td width="8" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"></td>
                  <td width="32" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                    <?php echo $twitter; ?>
                  </td>
                  <td width="8" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"></td>
                  <td width="32" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                    <?php echo $facebook; ?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
              <table width="100%" cellpadding="0" cellspacing="0" border="0" class="hide" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                <tr>
                  <td width="100%" height="10" bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br /></td>
              </tr>
            </table>
            </td>
          </tr>
          <tr>
            <td bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
              <table width="100%" cellpadding="0" cellspacing="0" border="0" class="hide" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                <tr>
                  <!--ad-->
                  <td bgcolor="#ffffff" width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                    <?php echo $parnter_solution; ?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <!-- spacer -->
          <tr>
            <td width="100%" height="10" bgcolor="#ffffff" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br /></td>
          </tr>
          <!-- / header -->
          <?php echo $stories; ?>
      </td>
    </tr>
  </table>
</body>
</html>
