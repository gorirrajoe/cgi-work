<?php
	// single.php will handle displaying posts, attachments and custom post types

	get_header();

	global $Appid;
	global $multipage;

	$is_mobile	= is_mobile();
	$is_tablet	= is_tablet();

	$post_ID	= get_the_ID();

	$technical_theme_options	= get_option( 'cgi_media_technical_options' );
	$editorial_theme_options	= get_option( 'cgi_media_editorial_options' );

	$photos = false;

	if ( !empty( $technical_theme_options['gallery_category'] ) ) {
		$gallery_cat_array = explode( ',', $technical_theme_options['gallery_category'] );

		if ( in_category( $gallery_cat_array ) ) {
			$photos = true;
		}
	}

	// add specific classes to the article tag so they can be used for styling the page correctly
	$classes = array();

	$classes[]	= has_post_thumbnail( $post_ID ) ? 'attachment' : 'no-attachment';

	$attachments = get_children( array(
		'post_parent'		=> $post_ID,
		'post_status'		=> 'inherit',
		'post_type'			=> 'attachment',
		'post_mime_type'	=> 'image',
		'orderby'			=> 'menu_order',
		'order'				=> 'ASC'
	) );

	if ( count( $attachments ) > 1 ) {
		$classes[] = 'gallery';
	}

	if ( have_posts() ) :  while ( have_posts() ) :  the_post(); ?>

		<div class="left_column">
			<div <?php post_class( $classes ); ?>>

				<?php if ( in_category( array( 'partnerconnect', 'sponsored' ) ) ): ?>
					<h4 class="sponsored-label">Sponsored Content</h4>
				<?php endif; ?>

				<h1>
					<img class="slashes slash_blue" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/images/running/running-slashes-blue.svg">

					<?php
						// Sponsored text returns blank if nothing is there
						// and returns with space after it so it's safe to echo these together
						echo get_partner_connect( $post->ID ) . get_the_title();
					?>
				</h1>

				<?php if ( !is_singular( 'shoe' ) ) {
					echo '<p class="post_author">By ' . get_guest_author() .', '. format_timestamp_2() .'</p>';
				} ?>

				<div id="article-left">
					<div id="share-links-left">
						<?php print_social_media( 'left' ); ?>
					</div>
				</div>

				<div id="article-right" class="content">

					<div id="share-links">
						<?php print_social_media(); ?>
					</div>

					<?php
						if ( $photos ) {
							the_content();
						} elseif ( $multipage ) {
							get_template_part( 'template-parts/content', 'multipage' );
						} elseif ( is_attachment() ) {
							get_template_part( 'template-parts/content', 'attachment' );
						} elseif ( is_singular( 'shoe' ) ) {
							get_template_part( 'template-parts/content', 'shoe' );
						} else {
							get_template_part( 'template-parts/content', get_post_format() );
						}

					?>
				</div>


				<?php if ( !is_singular( 'shoe' ) ) {

					if ( in_category( array( 'partnerconnect', 'sponsored' ) ) ) { ?>

						<div class="postmeta">
							<?php
								print_taxonomy();

								$author_id = -1;
								if ( $post->post_type == 'attachment' && $post->post_parent != 0 ) {

									$post_parent = get_post( $post->post_parent );

									if ( !get_post_meta( $post_parent->ID, 'guest_author', true ) ) {
										$author_id = $post_parent->post_author;
									}

								} else if ( !get_post_meta( $post->ID, 'guest_author', true ) && is_singular( array( 'post', 'page', 'attachment' ) ) ) {

									$author_name = get_the_author_meta( 'display_name' );

									if ( !( strpos( $author_name, ".com" ) > -1 ) ) {
										$author_id = $post->post_author;
									}

								}

								if ( $author_id != -1 ) {
									echo '<div class="author_meta">';
										$author_image			= get_template_directory() . '/images/author/'.get_the_author_meta( 'ID' ).'.jpg';
										$author_image_display	= get_template_directory_uri() . '/images/author/'.get_the_author_meta( 'ID' ).'.jpg';

										if ( file_exists( $author_image ) ) {
											echo '<img class="avatar" src="'.$author_image_display.'">';
										} else {
											echo get_avatar( get_the_author_meta( 'ID' ), 120, '', $author_name );
										}

										echo '<h4>' . $author_name . '</h4>
										<p class="author_bio">' . get_the_author_meta( 'description' ) . '</p>';

										$twitter	= trim( get_the_author_meta( 'twitter' ) );
										$facebook	= trim( get_the_author_meta( 'facebook' ) );
										$googleplus	= trim( get_the_author_meta( 'googleplus' ) );

										echo '<ul class="author_social">';
											if ( $twitter != '' ) {
												echo '<li><a class="custom-twitter-button" href="https://twitter.com/' . $twitter . '" title="Follow @' . $twitter . '" target="_blank"><span class="social_button"></span></a></li>';
											}
											if ( $facebook != '' ) {
												echo '<li><a class="custom-facebook-button" href="http://facebook.com/' . $facebook . '" title="Subscribe on Facebook" target="_blank"><span class="social_button"></span></a></li>';
											}
											if ( $googleplus != '' ) {
												echo '<li><a class="custom-gplus-button" href="' . $googleplus . '?rel=author" target="_blank" title="Follow on Google+"><span class="social_button"></span></a></li>';
											}
											echo '<li><a class="author-button" rel="author" href="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '" title="Read more by ' . $author_name . '">All articles by ' . get_the_author_meta( 'first_name' ) . '</a></li>
										</ul>
									</div>';
								}
							?>
						</div><!-- postmeta -->

					<?php }


					if ( !in_category( array( 'partnerconnect', 'sponsored' ) ) ) {

						if ( is_single() && $photos ) {

							$test_content	= wpautop( get_the_content() );
							$test_content	= preg_replace( '/( <p> )\s*\[gallery/', '[gallery', $test_content );
							$test_content	= preg_replace( '/( ]<\/p> )/', ']', $test_content );

							if ( !strpos( $test_content, '<p>' ) ) { ?>
								<div class="hypnotoad_unit_container">
									<div class="hypnotoad_unit">
										<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('middle') : ''; ?>
									</div><!-- ad unit -->
								</div>
							<?php }

						}

						if ( $is_mobile ) : ?>
							<div class="hypnotoad_unit hypnotoad_300x250 hypnotoad_300x250_4">
								<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('side-mobile') : ''; ?>
							</div><!-- ad unit -->
						<?php endif; ?>




						<div class="single_recommended_feed">

							<?php echo do_shortcode( '[related location="article-footer"]' ); ?>

							<?php if ( !empty( $technical_theme_options['outbrain'] ) ) { ?>
								<?php if ( !$is_mobile ) { ?>
									<div class="OUTBRAIN" data-src="<?php echo get_permalink(); ?>" data-widget-id="AR_1" data-ob-template="cgi" ></div>
								<?php } else { ?>
									<div class="OUTBRAIN" data-src="<?php echo get_permalink(); ?>" data-widget-id="MB_1" data-ob-template="cgi" ></div>
								<?php } ?>

								<script type="text/javascript" async src="http://widgets.outbrain.com/outbrain.js"></script>
							<?php }

							if ( !$is_mobile ) {

								if ( is_single() && !$photos ) { ?>
									<div class="hypnotoad_unit_container">
										<div class="hypnotoad_unit hypnotoad_728x90">
											<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('middle') : ''; ?>
										</div><!-- ad unit -->
									</div>
								<?php }


								/**
								 * pubexchange after article
								 */
								if ( !empty($technical_theme_options['pubexchange']) ) {
									if ( is_active_sidebar('pubexchange') ) : dynamic_sidebar('pubexchange'); endif;
								}

							} ?>
						</div><!-- recommended -->

					<?php }

					// training plans
					echo get_training_plans( $post->ID );

					if ( !$is_mobile && !is_tablet2() ) {

						if ( is_singular( array( 'post', 'attachment' ) ) && !empty( $technical_theme_options['facebook_comments'] ) ) { ?>
							<div id="post-comments">
								<div class="fb-comments" data-href="<?php  echo get_permalink(); ?>" data-num-posts="10" data-width="660"></div>
							</div>
						<?php  }

						if ( !empty( $technical_theme_options['ad_com'] ) && !empty( $technical_theme_options['ad_com_pid'] ) && !empty( $technical_theme_options['ad_com_650_300_pid'] ) ) { ?>
							<div class="advertising-com">
								<script type="text/javascript">
									adsonar_placementId=<?php echo $technical_theme_options['ad_com_650_300_pid']; ?>;
									adsonar_pid=<?php echo $technical_theme_options['ad_com_pid']; ?>;
									adsonar_ps=0;
									adsonar_zw=650;
									adsonar_zh=300;
									adsonar_jv='ads.adsonar.com';
								</script>
								<script language="JavaScript" src="http://js.adsonar.com/js/adsonar.js"></script>
							</div>
						<?php }

					} elseif ( is_singular( array( 'post', 'attachment' ) ) && $technical_theme_options['facebook_comments'] ) {
						if ( is_singular( array( 'post', 'attachment' ) ) && $technical_theme_options['facebook_comments'] ) { ?>
							<div id="post-comments">
							<div class="fb-comments" data-href="<?php  echo get_permalink(); ?>" data-num-posts="10" data-width="660" data-mobile="false"></div>
							</div>
						<?php }
					}
				} ?>
			</div>
		</div><!-- row 1/content -->

		<?php

	endwhile; else : ?>

		<p><?php  _e( 'Sorry, no posts matched your criteria.' ); ?></p>

	<?php endif; ?>

<?php

	get_sidebar();

	get_footer();

?>
