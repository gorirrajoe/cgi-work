<?php
	$technical_theme_options	= get_option( 'cgi_media_technical_options' );
	$name_tracking				= 'running';
?>
<div class="sb-slidebar sb-left sb-width-wide sb-static">
	<div class="social_row">
		<span class="icon-cancel sb-close"></span>
		<ul class="side_social_icons">

			<?php if ( !empty( $technical_theme_options['site_facebook_page'] ) ) { ?>
				<li>
					<a target="_blank" href="https://facebook.com/<?php echo $technical_theme_options['site_facebook_page']; ?>" title="Like us on Facebook" onclick="_gaq.push(['_trackEvent', 'Slidebar Left Links', '<?php echo $name_tracking;?>', 'https://facebook.com/<?php echo $technical_theme_options['site_facebook_page']; ?>']);">
						<span class="icon-facebook"></span>
					</a>
				</li>
			<?php }

			if ( !empty( $technical_theme_options['site_twitter_handle'] ) ) { ?>
				<li>
					<a target="_blank" href="https://twitter.com/<?php echo $technical_theme_options['site_twitter_handle']; ?>" title="Follow us on Twitter" onclick="_gaq.push(['_trackEvent', 'Slidebar Left Links', '<?php echo $name_tracking;?>', 'https://twitter.com/<?php echo $technical_theme_options['site_twitter_handle']; ?>']);">
						<span class="icon-twitter"></span>
					</a>
				</li>
			<?php }

			if ( !empty( $technical_theme_options['site_instagram_handle'] ) ) { ?>
				<li>
					<a target="_blank" href="http://instagram.com/<?php echo $technical_theme_options['site_instagram_handle']; ?>" title="Follow us on Instagram" onclick="_gaq.push(['_trackEvent', 'Slidebar Left Links', '<?php echo $name_tracking;?>', 'http://instagram.com/<?php echo $technical_theme_options['site_instagram_handle']; ?>']);">
						<span class="icon-instagramm"></span>
					</a>
				</li>
			<?php }

			if ( !empty( $technical_theme_options['youtube_page'] ) ) { ?>
				<li>
					<a target="_blank" href="<?php echo $technical_theme_options['youtube_page']; ?>" title="Follow us on YouTube" onclick="_gaq.push(['_trackEvent', 'Slidebar Left Links', '<?php echo $name_tracking;?>', '<?php echo $technical_theme_options['youtube_page']; ?>']);">
						<span class="icon-youtube"></span>
					</a>
				</li>
			<?php } ?>

		</ul>
	</div><!-- social -->

	<div class="side_row nav_group">
		<?php
			wp_nav_menu( array(
				'menu'				=> 'slidebar-left-nav',
				'menu_class'		=> 'slidebar_left_nav',
				'container'			=> false,
				'fallback_cb'		=> 'false',
				'walker'			=> new Slidebar_Walker(),
				'theme_location'	=> 'slidebar_left_nav'
			) ); ?>

	</div><!-- side nav group -->

	<div class="side_row side_news_feed">
		<h3><img class="slashes slash_white" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-white.svg"> Recent Stories</h3>
		<?php get_newsfeed_side(); ?>
	</div><!-- news feed -->

	<div class="side_row recommended_feed">
		<h3><img class="slashes slash_white" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-white.svg"> Recommended</h3>
		<?php get_recommended_side(); ?>
	</div><!-- recommended -->

</div><!-- left slidebar -->
