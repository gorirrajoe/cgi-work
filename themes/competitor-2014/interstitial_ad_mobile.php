<?php
	/*
	 * Template Name: Mobile Interstitial Ad
	 */
	set_mobile_interstitial_cookie();
?>
<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="robots" content="noindex">
		<title>Advertisement</title>
		<script type='text/javascript'>
			(function() {
			var useSSL = 'https:' == document.location.protocol;
			var src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
			document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
			})();
		</script>
		<?php
			$adKw = class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::singleton()->generate_dfp_keywords() : '';

			echo "<script type='text/javascript'>\n\t\t\t";

			echo "googletag.defineSlot('".$adKw."', [250, 208], 'div-250_208_interstitial').addService(googletag.pubads()).setTargeting( 'pos', 'interstitial' );\n\t\t\t";

			echo "googletag.pubads().enableSyncRendering();\n\t\t\t";
			echo "googletag.pubads().enableSingleRequest();\n\t\t\t";
			echo "googletag.enableServices();\n\t\t";
			echo "</script>\n";
		?>
	</head>
	<body style="margin:0 auto;">

		<div id="interstitial-ad" style="">
			<div id="div-250_208_interstitial" style="width: 250px; height: 208px;">
				<script type="text/javascript">googletag.display("div-250_208_interstitial");</script>
			</div>
		</div>

	</body>
</html>
