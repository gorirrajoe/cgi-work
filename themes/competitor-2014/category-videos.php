<?php
	get_header( 'video' );
	$count = 1;
	global $post;
	$post_id	= $post->ID;
	$is_mobile	= is_mobile();

	$this_category	= get_category( $cat );
	$category_name	= $this_category->cat_name;
	$page			= get_query_var( 'paged', 1 );
?>
			<div class="row video_row">
				<div class="left_column">
					<div <?php post_class(); ?>>
						<h1><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> Competitor On Demand Page: <?php echo $page; ?></h1>
						<div class="content">
							<style>
								.content .more_link {
									display:none;
								}
							</style>
							<?php get_recent_category_posts($this_category->term_id, 1, 1); ?>
						</div>
					</div>
				</div><!-- row 1/content -->
				<div class="sidebar">
					<?php
						get_upcoming_vids(get_the_ID(), 0);

						if ( is_active_widget('category-sponsor') ) { ?>
							<div class="category_sponsor">
								<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('category-sponsor') ) : else : endif; ?>
							</div>
						<?php }
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="container video_bottom_container">
		<div class="row">
			<script>
				jQuery(function($) {
					$( "#tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
					$( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );

					$( "#menu-video-nav-2" ).accordion({
						header:'h5',
						heightStyle:'content'
					});
				});
			</script>

			<div id="tabs">
				<div class="tabs_left_col">
					<?php
						$slug		= $this_category->slug;
						$menu_name	= $slug. '-nav';
						wp_nav_menu( array(
							'menu'		=> $menu_name,
							'container'	=> false,
							'walker'	=> new Sub_Category_Walker_2_vid() )
						);
					?>
				</div>
				<?php wp_nav_menu( array(
					'menu'			=> $menu_name,
					'container'		=> false,
					'walker'		=> new Sub_Category_Walker_2a_vid(),
					'items_wrap'	=> '%3$s' )
				); ?>
			</div>
			<div id="accordion">
				<?php
					$slug		= $this_category->slug;
					$menu_name	= $slug. '-nav';
					wp_nav_menu( array(
						'menu'		=> $menu_name,
						'container'	=> false,
						'walker'	=> new Sub_Category_Walker_2b() )
					);
				?>
			</div>
		</div>
<?php get_footer(); ?>
