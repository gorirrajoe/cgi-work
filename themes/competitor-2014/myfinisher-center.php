<?php
	/* Template Name: MyFinisher Center*/
	get_header();
?>
	<div class="row">
		<div class="left_column">
			<div <?php post_class($classes); ?>>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<h1><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> <?php the_title(); ?></h1>

					<div id="share-links">
						<?php
							if ($technical_theme_options['social_media_publisher_key'] && $technical_theme_options['social_media_publisher_key'] != ""){
								$publisher_key = $technical_theme_options['social_media_publisher_key'];
								$twitter_account = $technical_theme_options['site_twitter_handle']; ?>

								<div id="share-links">
							 		<script>
									var pwidget_config = {
										shareQuote: false,
										onshare:bk_addSocialChannel,
										defaults: {
											gaTrackSocialInteractions: true,
											retina: true,
											mobileOverlay: true,
											afterShare: false,
											sharePopups: true
										}
									};

							 			function gaTrack(network, action) {
							 				_gaq.push(['_trackSocial', network, action]);
							 			}
							 			//Populate BlueKai social sharer
									  function bk_addSocialChannel(channelName) {
									   	bk_addPageCtx('share', channelName);
									    BKTAG.doTag(39226,1);
									  }


							 		</script>

									<div id="sharingWidgetLarge">
										<div class="pw-widget pw-counter-horizontal" pw:twitter-via="<?php echo $twitter_account;?>">
											<a class="pw-button-facebook pw-look-native" onclick="gaTrack('Facebook','share')"></a>
											<a class="pw-button-twitter pw-look-native" onclick="gaTrack('Twitter','tweet')"></a>
											<a class="pw-button-googleplus pw-look-native" onclick="gaTrack('Google','+1')"></a>
											<a class="pw-button-pinterest pw-look-native" onclick="gaTrack('Pinterest','pin')"></a>
											<a class="pw-button-print pw-look-native pw-size-medium" onclick="gaTrack('Print','print')"></a>
										</div>
									</div>
									<div id="sharingWidgetSmall" style="display:none; " pw:twitter-via="<?php echo $twitter_account;?>">
										<div class="pw-widget pw-size-medium">
											<a class="pw-button-facebook pw-counter" onclick="gaTrack('Facebook','share')"></a>
											<a class="pw-button-twitter pw-counter" onclick="gaTrack('Twitter','tweet')"></a>
											<a class="pw-button-googleplus pw-counter" onclick="gaTrack('Google','+1')"></a>
											<a class="pw-button-pinterest pw-counter" onclick="gaTrack('Pinterest','pin')"></a>
											<a class="pw-button-print" onclick="gaTrack('Print','print')"></a>
										</div>
									</div>

									<script src="http://i.po.st/share/script/post-widget.js#init=immediate&amp;publisherKey=<?php echo $publisher_key;?>&amp;retina=true" type="text/javascript"></script>

									<script>
									jQuery(document).ready(function() {
										if(jQuery(window).width() < 482){
											jQuery('#sharingWidgetLarge').hide();
											jQuery('#sharingWidgetSmall').show();
										}
										if(jQuery(window).width() > 482){
											jQuery('#sharingWidgetLarge').show();
											jQuery('#sharingWidgetSmall').hide();
										}
									});

									jQuery(window).bind("resize", stickpw);
									function stickpw(e){
										if(jQuery(window).width() < 482){
											jQuery('#sharingWidgetLarge').hide();
											jQuery('#sharingWidgetSmall').show();
										}
										if(jQuery(window).width() > 482){
											jQuery('#sharingWidgetLarge').show();
											jQuery('#sharingWidgetSmall').hide();
										}
									}
									</script>
								</div>
							<?php }
						?>
					</div>

					<div class="content">
						<?php
							if(isset($_REQUEST['eId']) AND ($_REQUEST['eId'] == 51 OR $_REQUEST['eId'] == 50)){
								echo '<h1 style="color: red;">Results & Finisher Certificate Presented by <span style="color:black;border-bottom: 3px solid #FFFF00;">yp</span></h1>';
							}
							switch($_REQUEST['eId']){
								case 83:
								case 88:
								case 80:
								case 85:
								case 86:
								case 89:
								case 82:
								case 84:
								case 81:
								case 79:
								case 87:
								case 91:
								case 59:
								case 36:
								case 58:
								case 35:
								case 77:
								case 37:
									//events not to show photos or finisher cert message
									$showPFMessage = 'N';
									break;
								default:
									$showPFMessage = 'Y';
									break;
							}
							if($showPFMessage == 'Y'):
						?>
						<div style="padding: .625em; margin-bottom: 1.25em; background-color: #f3f8e4; border: 1px solid #6EB442; font-weight:600;">
							Click on your name in the search results to view your photos and finisher certificate download link. (2012 and newer events only)
						</div>
						<?php
						endif;
						include(ABSPATH. 'wp-content/plugins/results/frontend/race-results.php'); ?>

						<h3><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> How to Recover<span class="visible_md"> From Your Race</span></h3>
						<?php
							// don't look at sticky posts -- just pure recent dates
							$args = array(
								'posts_per_page' => 10,
								'tag' => 'recovery',
								'ignore_sticky_posts' => 1
							);
							if ( false === ( $recent_tag_posts = get_transient( 'recent_tag_posts') ) ) {
								$recent_tag_posts = new WP_Query($args);
								set_transient( 'recent_tag_posts', $recent_tag_posts, 12 * HOUR_IN_SECONDS );
							}
							if ( $recent_tag_posts->have_posts() ) {
								$count = 1;
								echo '<ul class="top_posts">';
									while ( $recent_tag_posts->have_posts() ) {
										$recent_tag_posts->the_post();
										$post_classes = get_post_class();

										if ( has_post_thumbnail() ) {
											$post_thumb = get_the_post_thumbnail(get_the_ID(), 'thumbnail');
											$post_classes = get_post_class('post-thumbnail');
										} else {
											$post_thumb = get_bloginfo('stylesheet_directory').'/images/running/default-post-thumbnail-sm.jpg';
										}
										$post_classes_string = implode(" ", $post_classes);

										echo '
											<li class="'.$post_classes_string.'">
												<div class="featured_img">
													<a href="'.get_permalink().'" class="overlay"></a>
													'.$post_thumb.'
												</div>
												<h4><a href="'.get_permalink().'">'.get_short_title().'</a></h4>
												<p class="post_author">by '.get_guest_author().'</p>
												<p class="excerpt">'.get_the_excerpt().'</p>
											</li>
										';
									}
								echo '</ul>';
							} else {
							// no featured posts found
							}
							echo '
								<div class="more_link_arrow dark_bg module">
									<a href="'.site_url().'/tag/recovery/page/2" title="More Recovery">More Recovery </a><a href="'.site_url().'/tag/recovery/page/2" title="More Recovery"><span class="icon-right-open"></span></a>
								</div>
							';

							wp_reset_postdata();
						?>
					</div>
				<?php endwhile; else: ?>
					<div class="content">
						<p>Sorry, no posts matched your criteria.</p>
					</div>
				<?php endif; ?>

			</div>
		</div><!-- row 1/content -->
		<?php get_sidebar(); ?>
	</div>
<?php get_footer(); ?>
