<?php
	/**
	 * Template file for single posts in the category "Lists"
	 * File load handled by single template redirect
	 */

	get_header();

	global $Appid;
	global $multipage;

	$is_mobile	= is_mobile();
	$is_tablet	= is_tablet();

	$post_ID	= get_the_ID();

	$technical_theme_options	= get_option( 'cgi_media_technical_options' );
	$editorial_theme_options	= get_option( 'cgi_media_editorial_options' );

	$photos = false;

	if ( !empty( $technical_theme_options['gallery_category'] ) ) {
		$gallery_cat_array = explode( ',', $technical_theme_options['gallery_category'] );

		if ( in_category( $gallery_cat_array ) ) {
			$photos = true;
		}
	}

	// add specific classes to the article tag so they can be used for styling the page correctly
	$classes = array();

	$classes[]	= has_post_thumbnail( $post_ID ) ? 'attachment' : 'no-attachment';

	$attachments = get_children( array(
		'post_parent'		=> $post_ID,
		'post_status'		=> 'inherit',
		'post_type'			=> 'attachment',
		'post_mime_type'	=> 'image',
		'orderby'			=> 'menu_order',
		'order'				=> 'ASC'
	) );

	if ( count( $attachments ) > 1 ) {
		$classes[] = 'gallery';
	}

	if ( have_posts() ) :  while ( have_posts() ) :  the_post();

		$franchise_image = get_post_meta ( $post_ID, '_franchise_image', true ); ?>

		<div class="lists_top_container">
			<?php if ( !empty( $franchise_image ) ) {

				echo '<div class="franchise_image">
					<h1><img src="' . $franchise_image . '" alt="' . strip_tags( the_title( '','',false ) ) . '"></h1>
				</div>';

			} ?>

			<div class="list_thumbs_container">
				<?php
					$images_count			= count( $attachments );
					$first_slide_is_title	= get_post_meta( $post_ID,'_first_slide_is_title',true );
					$ad_slide				= get_post_meta ( $post_ID, '_mid_slider_ad_number', true );
					$last_slide_is_ad		= get_post_meta ( $post_ID, '_last_slide_is_ad', true );
					$ad_slide_position		= $images_count - $ad_slide;
					$thumbs_array			= array();

					foreach ( $attachments as $attachment ) {
						array_push( $thumbs_array, wp_get_attachment_thumb_url( $attachment->ID ) );
					}

					$count = count( $thumbs_array );

					if ( $first_slide_is_title ) {
						$count--;
					}
					if ( $last_slide_is_ad ) {
						$count--;
					}
					if ( $ad_slide ) {
						$count--;
					}
				?>

				<script type="text/javascript">
					var tracker = 0;
					var ad_slide_position = <?php echo $ad_slide; ?>;
					var thumb_images_length = <?php echo $images_count; ?>;
				</script>

				<div id="sync2" class="owl-carousel">

					<?php
						$i = $images_count;

						foreach ( $thumbs_array as $thumb ) {

							if ( $first_slide_is_title && ( $i == $images_count ) ) {
								echo '<div class="item item-'.$i.'">
									<img src="'.$thumb.'">
									<div class="slide_number">&nbsp;</div>
								</div>';
							} elseif ( $i == $ad_slide ) {
								echo '<div class="item item-'.$i.'">
									<img src="'.$thumb.'">
									<div class="slide_number">&nbsp;</div>
								</div>';
							} elseif ( $last_slide_is_ad && ( $i == 1 ) ) {
								echo '<div class="item item-'.$i.'">
									<img src="'.$thumb.'">
									<div class="slide_number">&nbsp;</div>
								</div>';
							} else {
								echo '<div class="item item-'.$i.'" onclick="tracker_update( '.$i.' ); _gaq.push( [\'_trackEvent\', \'Legacy List Slider\', \'Image Select\', \'Legacy List Slider Image Select\'] ); googletag.pubads().refresh();">
									<img src="'.$thumb.'">
									<div class="slide_number">'.$count.'</div>
								</div>';
								$count--;
							}
							$i--;

						}

					?>
				</div>

			</div>
		</div>

		<?php
			$styling = '';
			if ( $ad_slide ) {
				$styling .= '
					#sync2 .item-'.( $ad_slide ).' {
						display:none;
					}
				';
			}
			if ( $last_slide_is_ad ) {
				$styling .= '
					#sync2 .item-1 {
						display:none;
					}
				';
			}
		?>
		<style>
			<?php echo $styling; ?>
			#sync1 {
				border-bottom:1px solid #a7a9ac;
				margin-bottom:1.875em;
			}
			#sync2 .item{
				text-align: center;
				cursor: pointer;
				padding:1.25em 0;
				border:1px solid #a7a9ac;
			}
			#sync2 .owl-item img {
				border:1px solid #a7a9ac
			}
			#sync2 .synced {
				background-color: #27aae1;
			}
			.slide_number {
				font-size: 1.5em;
				margin-top: 0.417em;
			}
			.legacy_list_navi {
				background-color:#fff;
				position: absolute;
				right: 0;
				z-index: 10;
			}
			.legacy_list_navi .ll-owl-prev,
			.legacy_list_navi .ll-owl-next {
				cursor: pointer;
				display: inline-block;
				font-family: "fontello";
				font-size: 3em;
				font-style: normal;
				font-variant: normal;
				font-weight: normal;
				line-height: 1em;
				text-align: center;
				text-decoration: inherit;
				text-transform: none;
			}
			.legacy_list_navi .ll-owl-prev:before {
				content: '\e80a';
			}
			.legacy_list_navi .ll-owl-next:before {
				content: '\e80b';
			}

			.legacy_list_navi .btn {
				color: #939393;
				font-size: 3em;
				padding: 0.208em;
				text-decoration: none;
			}
			.ll-big-img {
				display: table;
				margin: 0 auto 1.875em;
			}
			.ll-title {
				padding-top: 0.333em;
				margin-right:5em;
			}
		</style>

		<div class="left_column">
			<div <?php post_class( $classes ); ?>>
				<div class="content">

					<?php
						$thumb_ID		= get_post_thumbnail_id( $post->ID );
						$attachments	= get_children( array(
							'post_parent'		=> $post_ID,
							'post_status'		=> 'inherit',
							'post_type'			=> 'attachment',
							'post_mime_type'	=> 'image',
							'order'				=> 'ASC',
							'exclude'			=> $thumb_ID,
							'orderby'			=> 'menu_order'
						) );

						$gallery_type = "Legacy List";
					?>

					<div class="legacy_list_navi">
						<a class="btn ll-owl-prev"></a>
						<a class="btn ll-owl-next"></a>
					</div>


					<div id="sync1" class="owl-carousel">

						<?php
							$i						= 0;
							$ad_slide_link			= get_post_meta ( $post_ID, '_mid_slider_ad_link', true );
							$end_of_slider_ad_url	= get_post_meta ( $post_ID, '_end_of_slider_ad_url', true );

							foreach ( $attachments as $attachment ) {

								$link	= get_attachment_link( $attachment->ID );
								$image	= wp_get_attachment_image_src( $attachment->ID );
								$title	= $attachment->post_title;
								$desc	= $attachment->post_excerpt;

								if ( $i == $ad_slide_position ) {
									echo '
										<div class="item">
											<h2 class="ll-title">'.$title.'</h2>
											<a onClick="_gaq.push( [\'_trackEvent\', \'Legacy List Carousel\', \'Ad Click\', \'Legacy List Carousel Ad 1\'] );" href="'.$ad_slide_link.'" target="_blank" class="cover" title="'.$title.'"><img class="ll-big-img" src="'.$image[0].'"/></a>
											<p>'.$desc.'</p>
										</div>
									';
								} elseif ( $last_slide_is_ad && ( $i == ( $images_count-1 ) ) ) {
									echo '
										<div class="item">
											<h2 class="ll-title">'.$title.'</h2>
											<a onClick="_gaq.push( [\'_trackEvent\', \'Legacy List Carousel\', \'Ad Click\', \'Legacy List Carousel Ad 2\'] );" href="'.$end_of_slider_ad_url.'" target="_blank" class="cover" title="'.$title.'"><img class="ll-big-img" src="'.$image[0].'"/></a>
											<p>'.$desc.'</p>
										</div>
									';
								} else {
									echo '
										<div class="item">
											<h2 class="ll-title">'.$title.'</h2>
											<img src="'.$image[0].'" class="ll-big-img">
											<p>'.$desc.'</p>
										</div>
									';
								}

								$i++;

							}
						?>
					</div>

					<?php
						$content_string = apply_filters( 'the_content', get_the_content() );

						// hide all of the images that were in the story
						echo '<style>.wp-caption{display: none;}</style>';
						$content_string	= preg_replace( "/<img[^>]+\>/i", ' ', $content_string );
						$content_string	= preg_replace( "/\[sig:[a-zA-Z]+\]/", ' ', $content_string );

						echo $content_string;
					?>

					<script>
						$( document ).ready( function() {
							var sync1	= $('#sync1');
							var sync2	= $('#sync2');

							sync1.owlCarousel( {
								singleItem: true,
								slideSpeed: 1000,
								pagination: false,
								autoHeight: true,
								afterAction: syncPosition,
								responsiveRefreshRate: 200,
								mouseDrag: false,
								touchDrag: false
							} );

							$('.ll-owl-next').click( function() {
								sync1.trigger( 'owl.next' );
								_gaq.push( ['_trackEvent', 'Legacy List Carousel', 'Next', 'Legacy List Carousel Next Button'] );
								tracker_change( 1, ad_slide_position );
								googletag.pubads().refresh();
							} );

							$('.ll-owl-prev').click( function() {
								sync1.trigger( 'owl.prev' );
								_gaq.push( ['_trackEvent', 'Legacy List Carousel', 'Previous', 'Legacy List Carousel Previous Button'] );
								tracker_change( -1, ad_slide_position );
								googletag.pubads().refresh();
							} );

							sync2.owlCarousel( {
								items: 7,
								pagination: false,
								responsiveRefreshRate: 100,
								afterInit: function( el ) {
									el.find('.owl-item').eq( 0 ).addClass('synced');
								}
							} );

							function syncPosition( el ) {
								var current = this.currentItem;
								$('#sync2')
									.find('.owl-item')
									.removeClass('synced')
									.eq( current )
									.addClass('synced');

								if ( $('#sync2').data('owlCarousel') !== undefined ) {
									center( current )
								}
							}

							$('#sync2').on( 'click', '.owl-item', function( e ) {
								e.preventDefault();
								var number = $( this ).data('owlItem');
								sync1.trigger( 'owl.goTo', number );
							} );

							function center( number ) {
								var sync2visible	= sync2.data( 'owlCarousel' ).owl.visibleItems;
								var num				= number;
								var found			= false;

								for( var i in sync2visible ) {
									if ( num === sync2visible[i] ) {
										var found = true;
									}
								}

								if ( found === false ) {

									if ( num>sync2visible[sync2visible.length-1] ) {
										sync2.trigger( 'owl.goTo', num - sync2visible.length+2 )
									} else {
										if ( num - 1 === -1 ) {
											num = 0;
										}
										sync2.trigger( 'owl.goTo', num );
									}

								} else if ( num === sync2visible[sync2visible.length-1] ) {
									sync2.trigger( 'owl.goTo', sync2visible[1] )
								} else if ( num === sync2visible[0] ) {
									sync2.trigger( "owl.goTo", num-1 )
								}
							}
						} );

						function tracker_change( x, ad_slide_position ) {
							if ( x == 1 ) {

								if ( window.tracker == ( thumb_images_length -1 ) ) {
									return;
								} else {
									window.tracker++;
								}

							} else {

								if ( window.tracker == 0 ) {
									return;
								} else {
									window.tracker--;
								}

							}

							if ( window.tracker == ad_slide_position ) {
								_gaq.push( ['_trackEvent', 'Legacy List Carousel', 'Ad Impression', 'Legacy List Carousel Ad 1','',true] );
							}

							if ( window.tracker == ( thumb_images_length -1 ) && last_slide_is_ad == 1 ) {
								_gaq.push( ['_trackEvent', 'Legacy List Carousel', 'Ad Impression', 'Legacy List Carousel Ad 2','',true] );
							}
						}
						function tracker_update( x ) {
							window.tracker = x;
							//console.log( "tracker=", tracker );
						}
					</script>

				</div>

				<?php if ( !empty( $technical_theme_options['outbrain'] ) ) { ?>
					<div class="single_recommended_feed">

						<?php if ( !$is_mobile ) { ?>
							<div class="OUTBRAIN" data-src="<?php echo get_permalink(); ?>" data-widget-id="AR_1" data-ob-template="cgi" ></div>
						<?php } else { ?>
							<div class="OUTBRAIN" data-src="<?php echo get_permalink(); ?>" data-widget-id="MB_1" data-ob-template="cgi" ></div>
						<?php } ?>

						<script type="text/javascript" async src="http://widgets.outbrain.com/outbrain.js"></script>
					</div><!-- outbrain recommended -->
				<?php } ?>

				<?php
					/**
					 * pubexchange after article
					 */
					if ( !empty( $technical_theme_options['pubexchange'] ) ) {
						if ( is_active_sidebar('pubexchange') ) : dynamic_sidebar('pubexchange'); endif;
					}

					if ( !$is_mobile && !is_tablet2() ) { ?>

						<?php  if ( is_singular( array( 'post', 'attachment' ) ) && $technical_theme_options['facebook_comments'] ) { ?>
							<div id="post-comments">
								<div class="fb-comments" data-href="<?php echo get_permalink(); ?>" data-num-posts="10" data-width="660"></div>
							</div>
						<?php } ?>

						<?php if ( $technical_theme_options['ad_com'] && $technical_theme_options['ad_com_pid'] && $technical_theme_options['ad_com_pid'] != "" && $technical_theme_options['ad_com_650_300_pid'] && $technical_theme_options['ad_com_650_300_pid'] != "" ) { ?>

							<div class="advertising-com">
								<script type="text/javascript">
									adsonar_placementId=<?php  echo $technical_theme_options['ad_com_650_300_pid']; ?>;
									adsonar_pid=<?php  echo $technical_theme_options['ad_com_pid']; ?>;
									adsonar_ps=0;
									adsonar_zw=650;
									adsonar_zh=300;
									adsonar_jv='ads.adsonar.com';
								</script>
								<script language="JavaScript" src="http://js.adsonar.com/js/adsonar.js"></script>
							</div>

						<?php }

					} else { ?>

						<?php if ( is_singular( array( 'post', 'attachment' ) ) && $technical_theme_options['facebook_comments'] ) { ?>
							<div id="post-comments">
								<div class="fb-comments" data-href="<?php echo get_permalink(); ?>" data-num-posts="10" data-width="660" data-mobile="false"></div>
							</div>
						<?php } ?>

					<?php }

				?>

			</div>
		</div>


		<?php

	endwhile; else : ?>

		<p><?php  _e( 'Sorry, no posts matched your criteria.' ); ?></p>

	<?php endif; ?>

<?php

	get_sidebar();

	get_footer();

?>
