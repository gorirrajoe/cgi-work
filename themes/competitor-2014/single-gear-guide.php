<?php
	// single.php will handle displaying posts, attachments and custom posttypes
	get_header();


	if ( have_posts() ) :  while ( have_posts() ) :  the_post();

		if( in_category( '2015-running-gear-guide' ) ) {

			get_template_part( 'template-parts/gear-guides/content', '2015-running-gear-guide-single' );

		} elseif( in_category( '2016-running-gear-guide' ) ) {

			get_template_part( 'template-parts/gear-guides/content', '2016-running-gear-guide-single' );

		} elseif( in_category( '2017-running-gear-guide' ) ) {

			get_template_part( 'template-parts/gear-guides/content', '2017-running-gear-guide-single' );

		} else {

			get_template_part( 'template-parts/gear-guides/content', 'final-running-gear-guide-single' );

		}

	endwhile; else: ?>

		<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

	<?php endif;

		get_footer(); ?>
