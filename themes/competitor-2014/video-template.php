<?php
	get_header();

	/* just by setting post's category to "video" will call this template to action */

	$count = 1;
	global $post;
	$post_id	= $post->ID;
	$is_mobile	= is_mobile();
	$technical_theme_options	= get_option( 'cgi_media_technical_options' );
	$editorial_theme_options	= get_option( 'cgi_media_editorial_options' );

	$brightcove_id	= get_post_meta( $post_id, 'video_id', true ) ? get_post_meta( $post_id, 'video_id', true ) : '';
	$youtube_id		= get_post_meta( $post_id, '_youtube_id', true ) ? get_post_meta( $post_id, '_youtube_id', true ) : '';
?>

	<div class="left_column">

		<div <?php post_class(); ?>>

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<h1><img class="slashes slash_blue" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/images/running/running-slashes-blue.svg"> <?php the_title(); ?></h1>

				<p class="post_author">By <?php echo get_guest_author(); ?>, <?php echo format_timestamp_2(); ?></p>


				<div id="article-left">
					<div id="share-links-left">
						<?php
							// if ( $technical_theme_options['facebook_comments'] ) {
							// 	print_comments_number();
							// }
							print_social_media( 'left' );
						?>
					</div>
				</div>


				<div id="article-right" class="content">

					<div id="share-links">
						<?php print_social_media( 'top' ); ?>
					</div>

					<?php
						$url = site_url();
						$adKw = class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::singleton()->generate_dfp_keywords() : '';

						if ( $youtube_id != '' ) {
							echo do_shortcode( '[youtube id="'. $youtube_id .'"]' );
						} elseif ( $brightcove_id != '' ) {
							echo do_shortcode('[brightcove id="'. $brightcove_id .'"]');
						}

						echo apply_filters( 'the_content', get_the_content() );
					?>
				</div>

				<div class="postmeta">

					<?php
						print_taxonomy();

						$author_id = -1;

						if( $post->post_type == 'attachment' && $post->post_parent != 0 ){
							$post_parent = get_post( $post->post_parent );

							if( !get_post_meta( $post_parent->ID, 'guest_author', true ) ){
								$author_id = $post_parent->post_author;
							}

						} else if ( !get_post_meta( $post->ID, 'guest_author', true ) && is_singular( array( 'post', 'page', 'attachment' ) ) ) {
							$author_name = get_the_author_meta( 'display_name' );

							if( !( strpos( $author_name, ".com" ) > -1 ) ){
								$author_id = $post->post_author;
							}
						}

						if( $author_id != -1 ) {
							echo '<div class="author_meta">';
								$author_image			= get_template_directory() . '/images/author/'.get_the_author_meta( 'ID' ).'.jpg';
								$author_image_display	= get_template_directory_uri() . '/images/author/'.get_the_author_meta( 'ID' ).'.jpg';

								if( file_exists( $author_image ) ){
									echo "<img src=\"".$author_image_display."\">";
								} else {
									echo get_avatar( get_the_author_meta( 'ID' ), 120, '', $author_name );
								}

								//echo get_avatar( get_the_author_meta( 'ID' ), 120, '', $author_name );
								echo '<h4>' . $author_name . '</h4>
								<p class="author_bio">' . get_the_author_meta( 'description' ) . '</p>';

								$twitter	= trim( get_the_author_meta( 'twitter' ) );
								$facebook	= trim( get_the_author_meta( 'facebook' ) );
								$googleplus	= trim( get_the_author_meta( 'googleplus' ) );

								echo '<ul class="author_social">';
									if ( $twitter != '' ) {
										echo '<li><a class="custom-twitter-button" href="https://twitter.com/' . $twitter . '" title="Follow @' . $twitter . '" target="_blank"><span class="social_button"></span></a></li>';
									}
									if ( $facebook != '' ) {
										echo '<li><a class="custom-facebook-button" href="http://facebook.com/' . $facebook . '" title="Subscribe on Facebook" target="_blank"><span class="social_button"></span></a></li>';
									}
									if ( $googleplus != '' ) {
										echo '<li><a class="custom-gplus-button" href="' . $googleplus . '?rel=author" target="_blank" title="Follow on Google+"><span class="social_button"></span></a></li>';
									}
									echo '<li><a class="author-button" rel="author" href="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '" title="Read more by ' . $author_name . '">All articles by ' . get_the_author_meta( 'first_name' ) . '</a></li>
								</ul>
							</div>';
						}
					?>
				</div><!-- postmeta -->


				<div class="related_videos">
					<?php get_upcoming_vids( $post_id, 1 ); ?>
				</div>


			<?php endwhile; else: ?>

				<div class="content">
					<p>Sorry, no posts matched your criteria.</p>
				</div>

			<?php endif; ?>

		</div>
	</div><!-- row 1/content -->

	<?php get_sidebar( 'video' );

get_footer(); ?>
