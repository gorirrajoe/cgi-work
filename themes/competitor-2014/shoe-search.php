<?php
/* Template Name: Shoe Search Page */
get_header();
global $wpdb;

// query setup
$results_count = 0;
$shoe_type = -1;
$shoe_brand = -1;
$model_released = -1;
$gender = -1;
$model_name = '';
$price_range = -1;
$page = 0;
$resultspage = 1;
$query = '';

if(isset($_REQUEST['resultspage'])) { // if form submitted
	if(!isset($_REQUEST['resultspage'])) {
		$resultspage = 1;
	} else {
		$resultspage = $_REQUEST['resultspage'];
	}
	$shoe_type = $_REQUEST['shoe_type'];
	$model_released = $_REQUEST['model_released'];
	$price_range = $_REQUEST['price_range'];
	$shoe_brand = $_REQUEST['shoe_brand'];
	$gender = $_REQUEST['gender'];
	$model_name = $_REQUEST['model_name'];
}
// for navi and selects
// shoe type
$query = 'SELECT DISTINCT meta_value FROM wp_3_postmeta WHERE meta_key = "_type" order by meta_value';
$entries_type = $wpdb->get_results($query);

// release date
$query = 'SELECT DISTINCT meta_value FROM wp_3_postmeta WHERE meta_key = "_season_released" AND meta_value != "-1" order by meta_value';
$entries_season = $wpdb->get_results($query);
$seasons_array = array();
foreach($entries_season as $entry_season){
	$seasons_array[] = $entry_season->meta_value;
}
$query = 'SELECT DISTINCT meta_value FROM wp_3_postmeta WHERE meta_key = "_year_released" AND meta_value != "-1" order by meta_value';
$entries_year = $wpdb->get_results($query);
$year_array = array();
foreach($entries_year as $entry_year){
	$year_array[] = $entry_year->meta_value;
}
arsort($year_array);
asort($seasons_array);

// shoe brand
$query = 'SELECT DISTINCT meta_value FROM wp_3_postmeta WHERE meta_key = "_brand" order by meta_value';
$entries_brand = $wpdb->get_results($query);



?>
	<div class="special_category">
		<div class="category_left_column">
			<div class="blue_bg">
				<img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/category-shoe-directory.png">
			</div>
			<div class="category_hdr">
				<h1 class="category_title">Shoe Directory</h1>
				<?php if(is_active_widget('special-category-sponsor')) { ?>
					<div class="special_category_sponsor">
						<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('special-category-sponsor') ) : else : endif; ?>
					</div>
				<?php } ?>
			</div>
			<div class="filter_links">
				<h4>Shoe Type</h4>
				<ul>
					<?php
						foreach($entries_type as $entry_type){
							echo '<li><a href="?resultspage=1&shoe_type='.$entry_type->meta_value.'&model_released=-1&price_range=-1&shoe_brand=-1&gender=-1&model_name=">'.$entry_type->meta_value.'</a></li>';
						}
					?>
				</ul>
				<h4>Price Range</h4>
				<ul>
					<li><a href="?resultspage=1&shoe_type=-1&model_released=-1&price_range=0-100&shoe_brand=-1&gender=-1&model_name=">$0 - $100</a></li>
					<li><a href="?resultspage=1&shoe_type=-1&model_released=-1&price_range=101-150&shoe_brand=-1&gender=-1&model_name=">$101 - $150</a></li>
					<li><a href="?resultspage=1&shoe_type=-1&model_released=-1&price_range=150+&shoe_brand=-1&gender=-1&model_name=">$150+</a></li>
				</ul>
				<h4>Gender</h4>
				<ul>
					<li><a href="?resultspage=1&shoe_type=-1&model_released=-1&price_range=-1&shoe_brand=-1&gender=male&model_name=">Male</a></li>
					<li><a href="?resultspage=1&shoe_type=-1&model_released=-1&price_range=-1&shoe_brand=-1&gender=female&model_name=">Female</a></li>
				</ul>
				<h4>Release Date</h4>
				<ul>
					<?php
						foreach ($year_array as $year){
							foreach ($seasons_array as $season){
								$stringValue = $season.' ';
								$stringValue .= $year;
								/* removing code to allow fall of current year to display
								if($season != 'fall'){
									echo '<li><a href="?resultspage=1&shoe_type=-1&model_released='.$season.' '.$year.'&price_range=-1&shoe_brand=-1&gender=-1&model_name=">'.ucwords($season).' '.$year.'</a></li>';
								}elseif($year != date('Y')){
									echo '<li><a href="?resultspage=1&shoe_type=-1&model_released='.$season.' '.$year.'&price_range=-1&shoe_brand=-1&gender=-1&model_name=">'.ucwords($season).' '.$year.'</a></li>';
								}
								*/
								// show spring AND fall for current year
								echo '<li><a href="?resultspage=1&shoe_type=-1&model_released='.$season.' '.$year.'&price_range=-1&shoe_brand=-1&gender=-1&model_name=">'.ucwords($season).' '.$year.'</a></li>';
							}
						}
					?>
				</ul>
				<h4>Brand</h4>
				<ul>
					<?php
						foreach($entries_brand as $entry_brand){
							echo '<li><a href="?resultspage=1&shoe_type=-1&model_released=-1&price_range=-1&shoe_brand='.$entry_brand->meta_value.'&gender=-1&model_name=">'.$entry_brand->meta_value.'</a></li>';
						}
					?>
				</ul>
			</div>
		</div>
		<div class="category_right_column">
			<div class="row">
				<div class="shoe_mid_column">
					<div class="content">
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
							the_content();
							endwhile; endif;
						?>
					</div>
					<form class="row shoe_search" action="<?php echo site_url(); ?>/shoe-directory" method="GET">
						<input type="hidden" value="1" name="resultspage">
						<div class="col_odd shoe_col shoe_col_odd">
							<ol>
								<li>
									<select name="shoe_type">
										<option value='-1'>Select Shoe Type</option>
										<?php
											foreach($entries_type as $entry_type){
												$selected = "";
												if($shoe_type == $entry_type->meta_value){
													$selected = 'selected = "selected"';
												}
												echo '<option '.$selected.' value="'.$entry_type->meta_value.'">'.$entry_type->meta_value.'</option>';
											}
										?>
									</select>
								</li>
								<li>
									<select name="model_released">
										<option value='-1'>Release Date</option>
										<?php
											foreach ($year_array as $year){
												foreach ($seasons_array as $season){
													$selected = "";
													$stringValue = $season.' ';
													$stringValue .= $year;
													if($model_released == $stringValue){
														$selected = 'selected = "selected"';
													}
													/* removing code to allow fall of current year to display
													if($season != 'fall'){
														echo '<option '.$selected.' value="'.$season.' '.$year.'">'.ucwords($season).' '.$year.'</option>';
													}elseif($year != date('Y')){
														echo '<option '.$selected.' value="'.$season.' '.$year.'">'.ucwords($season).' '.$year.'</option>';
													}
													*/
													// show spring AND fall for current year
													echo '<option '.$selected.' value="'.$season.' '.$year.'">'.ucwords($season).' '.$year.'</option>';
												}
											}
										?>
									</select>
								</li>
								<li>
									<select name="price_range">
										<?php
											$shoe_price_0_100 = '';
											$shoe_price_101_150 = '';
											$shoe_price_151 = '';
											if($price_range == "0-100"){
												$shoe_price_0_100 = 'selected = "selected"';
											} else if($price_range == "101-150") {
												$shoe_price_101_150 = 'selected = "selected"';
											} else if($price_range == "150+") {
												$shoe_price_151 = 'selected = "selected"';
											}
										?>
										<option value='-1'>Select Price Range</option>
										<option <?php echo $shoe_price_0_100; ?> value="0-100">$0 - $100</option>
										<option <?php echo $shoe_price_101_150; ?> value="101-150">$101 - $150</option>
										<option <?php echo $shoe_price_151; ?> value="150+">$150+</option>
									</select>
								</li>
							</ol>
						</div>
						<div class="col_even shoe_col shoe_col_even">
							<ol>
								<li>
									<select name="shoe_brand">
										<option value='-1'>Select Brand</option>
										<?php
											foreach($entries_brand as $entry_brand){
												$selected = "";
												if($shoe_brand == $entry_brand->meta_value){
													$selected = 'selected = "selected"';
												}
												echo '<option '.$selected.' value="'.$entry_brand->meta_value.'">'.$entry_brand->meta_value.'</option>';
											}
										?>
									</select>
								</li>
								<li>
									<select name="gender">
										<?php
											$select_male = '';
											$select_female = '';
											if($gender == "male"){
												$select_male = 'selected = "selected"';
											} else if($gender == "female"){
												$select_female = 'selected = "selected"';
											}
										?>
										<option value="-1">Select Gender</option>
										<option <?php echo $select_male; ?> value="male">Male</option>
										<option <?php echo $select_female; ?> value="female">Female</option>
									</select>
								</li>
								<li>
									<input class="route-input" value="<?php echo $model_name; ?>" type="text" name="model_name" placeholder="Model Name">
								</li>
							</ol>
						</div>
						<button class="btn_find_shoes" type="submit">Search Shoes</button>
						<span class="tab_indicator"></span>
					</form>
				</div>
				<div class="sidebar sidebar_sans_top_margin">
					<div class="hypnotoad_unit hypnotoad_300x250">
						<?php
							if(!$is_mobile){
								echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('side-top') : '';
							}
						?>
					</div><!-- ad unit -->
					<div class="hypnotoad_unit hypnotoad_300x600">
						<?php
							if(!$is_mobile){
								echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('side-middle') : '';
							}
						?>
					</div><!-- ad unit -->
				</div>
			</div>
			<div class="row">
				<?php
					if($shoe_type != -1 || $shoe_brand != -1 || $gender != -1 || $model_name != '' || $price_range != -1 || $model_released != -1){ // if any item is chosen
						$query = 'SELECT * from wp_3_posts where post_type = "shoe" AND post_status = "publish"';
						if($shoe_type != -1){
							$shoe_type_sql = 'SELECT post_id FROM wp_3_postmeta where meta_key = "_type" and meta_value="'.$shoe_type.'"';
						}
						if($shoe_brand != -1){
							$brand_sql = 'SELECT post_id FROM wp_3_postmeta where meta_key = "_brand" and meta_value="'.$shoe_brand.'"';
						}
						if($model_released != -1){
							//Parse data
							$released_models = explode(" ", $model_released);
							$season_released_sql = 'SELECT post_id FROM wp_3_postmeta where meta_key = "_season_released" and meta_value="'.$released_models[0].'"';
							$year_released_sql = 'SELECT post_id FROM wp_3_postmeta where meta_key = "_year_released" and meta_value="'.$released_models[1].'"';
						}
						if($gender != -1){
							$gender_sql = 'SELECT post_id FROM wp_3_postmeta where meta_key = "_gender" and meta_value="'.$gender.'"';
						}
						if($model_name != ""){
							$model_sql = 'SELECT post_id FROM wp_3_postmeta where meta_key = "_model_name" and meta_value like "%'.$model_name.'%"';
						}
						if($price_range != -1){
							if($price_range == "0-100"){
								$price_sql = 'Select post_id from wp_3_postmeta where meta_key ="_msrp" and meta_value > 0 and meta_value < 101';
							} else if($price_range == "101-150"){
								$price_sql = 'Select post_id from wp_3_postmeta where meta_key ="_msrp" and meta_value < 151 and meta_value > 100';
							} else if($price_range == "150+"){
								$price_sql = 'Select post_id from wp_3_postmeta where meta_key ="_msrp" and meta_value > 150';
							}
						}
						$count = 1;
						if($shoe_type_sql != ""){
							$query .= ' and ID in ('.$shoe_type_sql;

							if($brand_sql != ""){
								$query .= ' and post_id in ('.$brand_sql;
								$count++;
							}
							if($season_released_sql != ""){
								$query .= ' and post_id in ('.$season_released_sql;
								$count++;
							}
							if($year_released_sql != ""){
								$query .= ' and post_id in ('.$year_released_sql;
								$count++;
							}
							if($gender_sql != ""){
								$query .= ' and post_id in ('.$gender_sql;
								$count++;
							}
							if($model_sql != ""){
								$query .= ' and post_id in ('.$model_sql;
								$count++;
							}
							if($price_sql != ""){
								$query .= ' and post_id in ('.$price_sql;
								$count++;
							}
						} else if($brand_sql != ""){
							$query .= ' and ID in ('.$brand_sql;
							if($season_released_sql != ""){
								$query .= ' and post_id in ('.$season_released_sql;
								$count++;
							}
							if($year_released_sql != ""){
								$query .= ' and post_id in ('.$year_released_sql;
								$count++;
							}
							if($gender_sql != ""){
								$query .= ' and post_id in ('.$gender_sql;
								$count++;
							}
							if($model_sql != ""){
								$query .= ' and post_id in ('.$model_sql;
								$count++;
							}
							if($price_sql != ""){
								$query .= ' and post_id in ('.$price_sql;
								$count++;
							}
						} else if($season_released_sql != ""){
							$query .= ' and ID in ('.$season_released_sql;
							if($year_released_sql != ""){
								$query .= ' and post_id in ('.$year_released_sql;
								$count++;
							}
							if($gender_sql != ""){
								$query .= ' and post_id in ('.$gender_sql;
								$count++;
							}
							if($model_sql != ""){
								$query .= ' and post_id in ('.$model_sql;
								$count++;
							}
							if($price_sql != ""){
								$query .= ' and post_id in ('.$price_sql;
								$count++;
							}
						} else if($gender_sql != ""){
							$query .= ' and ID in ('.$gender_sql;
							if($model_sql != ""){
								$query .= ' and post_id in ('.$model_sql;
								$count++;
							}
							if($price_sql != ""){
								$query .= ' and post_id in ('.$price_sql;
								$count++;
							}
						} else if($model_sql != ""){
							$query .= ' and ID in ('.$model_sql;
							if($price_sql != ""){
								$query .= ' and post_id in ('.$price_sql;
								$count++;
							}
						} else if($price_sql != ""){
							$query .= ' and ID in ('.$price_sql;
						}
						for($i = 0; $i < $count; $i++){
							$query .= ")";
						}
						$offset = 0;
						if($resultspage > 1) {
							$offset =  ($resultspage-1) * 20; // (page 2 - 1)*20 = offset of 20
						}
						$query_sans_offset = $query;
						$query .= ' ORDER BY post_date DESC LIMIT 20 OFFSET '. $offset;
					} else {  // if no items are chosen (default)
						$query = 'SELECT * from wp_3_posts where post_type = "shoe" AND post_status = "publish"';
						$offset = 0;
						if($resultspage > 1) {
							$offset =  ($resultspage-1) * 20; // (page 2 - 1)*20 = offset of 20
						}
						$query_sans_offset = $query;
						$query .= ' ORDER BY post_date DESC LIMIT 20 OFFSET '. $offset;
					}
					// for url
					$page_query = "";
					if ($shoe_type) {
						$page_query .= "&shoe_type=".$shoe_type."";
					} else {
						$page_query .= "&shoe_type=-1";
					}
					if($shoe_brand) {
						$page_query .= '&shoe_brand='.$shoe_brand.'';
					} else {
						$page_query .= '&shoe_brand=-1';
					}
					if($gender) {
						$page_query .= '&gender='.$gender.'';
					} else {
						$page_query .= '&gender=-1';
					}
					if($model_name) {
						$page_query .= '&model_name='.$model_name.'';
					} else {
						$page_query .= '&model_name=';
					}
					if($price_range) {
						$page_query .= '&price_range='.$price_range.'';
					} else {
						$page_query .= '&price_range=-1';
					}
					if($model_released) {
						$page_query .= '&model_released='.$model_released.'';
					} else {
						$page_query .= '&model_released=-1';
					}
					if($query != ''){
						$count = 0;
						$entries = $wpdb->get_results($query);
						$entries_sans_offset = $wpdb->get_results($query_sans_offset);
						$results = '<div class="row"><ul>';
						foreach($entries as $entry){
							//$image = wp_get_attachment_image_src($entry->ID, 'large');
							$image = get_the_post_thumbnail($entry->ID, 'large');
							$shoe_weight = get_post_meta( $entry->ID, '_shoe_weight', true );
							$msrp = get_post_meta( $entry->ID, '_msrp', true );
							$heel_toe_drop = get_post_meta($entry->ID, '_heel_toe_drop', true);

							$buy_now_link_1 = get_post_meta( $entry->ID, '_buy_now', true );
							$buy_now_image_1 = get_post_meta( $entry->ID, '_buy_now_button_image', true );

							$buy_now_link_2 = get_post_meta( $entry->ID, '_buy_now_2', true );
							$buy_now_image_2 = get_post_meta( $entry->ID, '_buy_now_button_image_2', true );

							$buy_now_link_3 = get_post_meta( $entry->ID, '_buy_now_3', true );
							$buy_now_image_3 = get_post_meta( $entry->ID, '_buy_now_button_image_3', true );

							$results .= '<li class="shoe_post">
								<a href="/?p='.$entry->ID.'&shoe_type='.$shoe_type.'&brand='.$shoe_brand.'&model_released='.$model_released.'&gender='.$gender.'&model='.$model_name.'&price_range='.$price_range.'">'.$image.'</a>
								<h3><a href="/?p='.$entry->ID.'&shoe_type='.$shoe_type.'&brand='.$shoe_brand.'&model_released='.$model_released.'&gender='.$gender.'&model='.$model_name.'&price_range='.$price_range.'&resultspage='.$resultspage.'">'.$entry->post_title.'</a></h3>';
								$results .= '<div class="shoe_deets">';
									if($msrp != ''){
										$results .= '
											<div class="shoe_deets_left">MSRP:</div>
											<div class="shoe_deets_right">$'.$msrp.'</div>
										';
									}
									if($shoe_weight != ''){
										$results .= '
											<div class="shoe_deets_left">Shoe Weight:</div>
											<div class="shoe_deets_right">'.$shoe_weight.'</div>
										';
									}
									if($heel_toe_drop != ''){
										$results .= '
											<div class="shoe_deets_left">Heel Toe Drop:</div>
											<div class="shoe_deets_right">'.$heel_toe_drop.'</div>
										';
									}
									if($buy_now_link_1 != '' && $buy_now_image_1 != ''){
										$results .= '<img class="buy-now-image" src="'.$buy_now_image_1.'" onclick="window.open( \''.$buy_now_link_1.'\', \'_blank\');" title="BUY NOW">';
									}
									if($buy_now_link_2 != '' && $buy_now_image_2 != ''){
										$results .= '<img class="buy-now-image" src="'.$buy_now_image_2.'" onclick="window.open( \''. $buy_now_link_2.'\', \'_blank\');" title="BUY NOW">';
									}
									if($buy_now_link_3 != '' && $buy_now_image_3 != ''){
										$results .= '<img class="buy-now-image" src="'.$buy_now_image_3.'" onclick="window.open( \''.$buy_now_link_3.'\', \'_blank\');" title="BUY NOW">';
									}
								$results .= '</div>';

							$results .='</li>';
							$results_count++;
						}
						$results .= '</ul></div>';
					}
					$entries_sans_offset_count = count($entries_sans_offset);
					$query_count_comma = number_format($entries_sans_offset_count);
					echo '
						<h3 class="results_hdr"><img class="slashes slash_white_lg" src="'.get_bloginfo('stylesheet_directory').'/images/running/running-slashes-white-lg.svg"> Results - <strong>Shoes Found: '.$query_count_comma.'</strong></h3>
					';

					$num_per_page = 20;
					$total_pages = ceil($entries_sans_offset_count/$num_per_page);
					$start_page = 1;
					$end_page = $total_pages + 1;
					if($total_pages > 5 ){
						if($resultspage > 3){
							if($total_pages == $resultspage){
								$start_page = $total_pages - 4;
							} else if($total_pages - $resultspage < 2){
								$start_page = $total_pages - 5 + ($total_pages - $resultspage);
							} else{
								$start_page = $resultspage - 2;
							}
						}
						if($start_page + 4 < $total_pages){
							$end_page = $start_page + 5;
						}
					}

					if($shoe_type == -1 && $shoe_brand == -1 && $gender == -1 && $model_name == '' && $price_range == -1 && $model_released == -1 ){
						if($total_pages != 1) {
							echo '<div class="row">
								<ul class="cal_page_numbers">';
									for($i = $start_page; $i < $end_page; $i++){
										if($i != $resultspage){
											$link = '?resultspage='. $i . $page_query;
											echo '<li><a href="'. $link .'">Page '.$i.'</a></li>';
										} else{
											echo '<li class="cal_current_page">Page '.$i.'</li>';
										}
									}
								echo '</ul>
							</div>';
						}
					} else{
						echo '<div class="row shoes_navi_row"><div class="btn_primary"><a class="prev-post" href="'.site_url().'/shoe-directory">Back to All Shoes</a></div></div>';
					}

					echo $results;

					if($shoe_type == -1 && $shoe_brand == -1 && $gender == -1 && $model_name == '' && $price_range == -1 && $model_released == -1 ){
						if($total_pages != 1) {
							echo '<div class="row">
								<ul class="cal_page_numbers">';
									for($i = $start_page; $i < $end_page; $i++){
										if($i != $page){
											$link = '?resultspage='. $i . $page_query;
											echo '<li><a href="'. $link .'">Page '.$i.'</a></li>';
										} else{
											echo '<li class="cal_current_page">Page '.$i.'</li>';
										}
									}
								echo '</ul>
							</div>';
						}
					}

				?>
			</div>
		</div>
		<!-- end code -->

	</div>
<?php get_footer(); ?>
