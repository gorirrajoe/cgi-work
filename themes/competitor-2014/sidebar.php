<?php
	$is_mobile	= is_mobile();
	$is_tablet	= is_tablet2();
?>
<div class="sidebar">

	<div class="hypnotoad_unit hypnotoad_300x600">
		<?php
			if ( !$is_mobile ) {
				echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('side-top') : '';
			}
		?>
	</div><!-- ad unit -->


	<?php if ( is_active_sidebar('category-sponsor') && dynamic_sidebar('category-sponsor') ) : else : endif; ?>


	<div class="news_feed module">
		<h3><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> Top Stories</h3>
		<?php if ( is_active_sidebar('hot-topics') && dynamic_sidebar('hot-topics') ) : else : endif; ?>
	</div><!-- news feed -->


	<div class="hypnotoad_unit hypnotoad_300x250 hypnotoad_300x250_1">
		<?php
			echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('side-middle') : '';
		?>
	</div><!-- ad unit -->


	<?php inline_iContact("sidebar"); ?>


	<div class="hypnotoad_unit hypnotoad_300x250 hypnotoad_300x250_4">
		<?php
			echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('side-bottom') : '';
		?>
	</div><!-- ad unit -->


	<?php if ( is_active_sidebar('marquee') ) : ?>

		<div class="module offers">
			<h3><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> Running Resources</h3>
			<ul id="sidebar_offers_carousel">
				<?php dynamic_sidebar('marquee'); ?>
			</ul>
		</div><!-- offers carousel -->

	<?php endif; ?>


	<div class="row">

		<?php
			if ( ( $is_mobile || $is_tablet ) && !is_single() ) {
				echo '<div class="hypnotoad_unit hypnotoad_300x250 hypnotoad_300x250_4">';
					echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('side-mobile') : '';
				echo '</div><!-- ad unit -->';
			}
		?>

	</div>


	<div class="related-articles">
		<?php related_stories(); ?>
	</div>

	<div class="hypnotoad_unit hypnotoad_300x600">
		<?php
			if ( !$is_mobile ) {
				echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('side-bottom-2') : '';
			}
		?>
	</div><!-- ad unit -->


</div>
