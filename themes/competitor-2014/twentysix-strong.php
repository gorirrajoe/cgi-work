<?php
	 /**
	  * Template Name: Twenty-Six Strong
	  */
?>
<!DOCTYPE html>
<?php
	$post_ID	= get_the_ID();
	$bloginfo	= get_bloginfo( 'name' );
	$name		= str_replace( '.com', '', $bloginfo );
	$name		= strtolower( $name );
	session_start();
	$_SESSION['user_start'] = time();
?>
<html lang="en" xmlns:<?php  echo $name; ?>="<?php  echo site_url(); ?>/ns#">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php
	//echo "\n<!-- $name -->\n";
	// modified 6.20.13 - sferrino

	if ( $name != "competitor" && $name != "competitor running" && $name != "velonews" && $name != "triathlon") {
		echo meta_keywords();
		echo meta_description();
	}

	// Load options and display accordingly
	$technical_theme_options	= get_option( 'cgi_media_technical_options' );
	$editorial_theme_options	= get_option( 'cgi_media_editorial_options' );
	$facebook_appid				= $technical_theme_options['facebook_appid'];
	$itunes_appid				= $technical_theme_options['itunes_appid'];


	/*add 2013-12-13 SK*/
	if ($itunes_appid != '' && (is_home() OR is_front_page())) {
		echo '<meta name="apple-itunes-app" content="app-id=' . $itunes_appid . '"/>';
	}
	/* modified 6.20.13 - sferrino */
	if ( $name != "competitor" && $name != "competitor running" && $name != "velonews" && $name != "triathlon") {
		echo wp_open_graph_protocol();
	}
	$apple_touch_icon = ($editorial_theme_options['apple_touch_icon']);
	if ($apple_touch_icon != '') {
		echo '<link rel="apple-touch-icon" type="image/png" href="' . $apple_touch_icon . '" />';
	}
	$shortcut_icon = ($editorial_theme_options['shortcut_icon']);
	if ($shortcut_icon != '') {
		echo '<link rel="shortcut icon" href="' . $shortcut_icon . '" />';
	}

	if(isset($_GET['all'])){
		$all = 1;
	} else {
		$all = 0;
	}


	/* sferrino */
	if ( $name != "competitor" && $name != "competitor running" && $name != "velonews" && $name != "triathlon") {
		echo page_title();
	} else {
		echo "<title>";
		wp_title( '' );
		echo "</title>";
	}

	// Mobile Test
	$is_mobile	= is_mobile();
	$is_tablet	= is_tablet2();

	if($technical_theme_options['google_analytics'] && $technical_theme_options['google_analytics'] != "") { ?>
		<script type="text/javascript">
			var _gaq = _gaq || [];
			var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
			_gaq.push(
				['_require', 'inpage_linkid', pluginUrl],
				['_setAccount', '<?php  echo $technical_theme_options['google_analytics']; ?>'],
				['_setDomainName', '.competitor.com'],
				['_setAllowLinker', true],
				['_trackPageview']
			);
			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
	<?php }

	if($technical_theme_options['google_tag_head'] AND !(empty($technical_theme_options['google_tag_head']))){
		echo $technical_theme_options['google_tag_head'];
	}

	wp_head();

	$photos				= false;
	$gallery_cat_array	= array();
?>
</head>
<body <?php body_class(); ?>>
	<?php
		if($technical_theme_options['google_tag'] AND !(empty($technical_theme_options['google_tag']))){
			echo $technical_theme_options['google_tag'];
		}

		if($technical_theme_options['meebonetwork'] && $technical_theme_options['meebonetwork'] != "") {
			$GLOBALS['meebo'] = true; ?>
			<script type="text/javascript">
				window.Meebo||function(c){function p(){return["<",i,' onload="var d=',g,";d.getElementsByTagName('head')[0].",
				j,"(d.",h,"('script')).",k,"='//cim.meebo.com/cim?iv=",a.v,"&",q,"=",c[q],c[l]?
				"&"+l+"="+c[l]:"",c[e]?"&"+e+"="+c[e]:"","'\"></",i,">"].join("")}var f=window,
				a=f.Meebo=f.Meebo||function(){(a._=a._||[]).push(arguments)},d=document,i="body",
				m=d[i],r;if(!m){r=arguments.callee;return setTimeout(function(){r(c)},100)}a.$=
				{0:+new Date};a.T=function(u){a.$[u]=new Date-a.$[0]};a.v=5;var j="appendChild",
				h="createElement",k="src",l="lang",q="network",e="domain",n=d[h]("div"),v=n[j](d[h]("m")),
				b=d[h]("iframe"),g="document",o,s=function(){a.T("load");a("load")};f.addEventListener?
				f.addEventListener("load",s,false):f.attachEvent("onload",s);n.style.display="none";
				m.insertBefore(n,m.firstChild).id="meebo";b.frameBorder="0";b.name=b.id="meebo-iframe";
				b.allowTransparency="true";v[j](b);try{b.contentWindow[g].open()}catch(w){c[e]=
				d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{var t=
				b.contentWindow[g];t.write(p());t.close()}catch(x){b[k]=o+'d.write("'+p().replace(/"/g,
				'\\"')+'");d.close();'}a.T(1)}({network:"<?php  echo $technical_theme_options['meebonetwork']; ?>"});
			</script>
		<?php }


		// 26 strong post meta variables
		$intro_text			= apply_filters('the_content', get_post_meta($post_ID, '_intro_text', TRUE));
		$leaderboard_ad		= get_post_meta($post_ID, '_leaderboard_ad', TRUE);
		$leaderboard_ad_url	= get_post_meta($post_ID, '_leaderboard_ad_url', TRUE);
		$top_ad_300x250		= get_post_meta($post_ID, '_top_ad_300x250', TRUE);
		$top_ad_300x250_url	= get_post_meta($post_ID, '_top_ad_300x250_url', TRUE);
		$mid_ad_960x400		= get_post_meta($post_ID, '_mid_ad_960x400', TRUE);
		$mid_ad_960x400_url	= get_post_meta($post_ID, '_mid_ad_960x400_url', TRUE);
		$bot_ad_300x250		= get_post_meta($post_ID, '_bot_ad_300x250', TRUE);
		$bot_ad_300x250_url	= get_post_meta($post_ID, '_bot_ad_300x250_url', TRUE);

		$instagram_script = get_post_meta($post_ID, '_instagram_script', TRUE);
	?>

	<div class="nav-collapse">
		<ul>
			<li><a href="#home">Home</a></li>
			<li><a href="#teams">Teams</a></li>
			<li><a href="#blog">The Blog</a></li>
			<li><a href="#twentysixstrong">#26Strong</a></li>
			<?php /*<li><a href="#results">Results</a></li>*/ ?>
		</ul>
	</div>

	<div class="tss_container">
		<div class="tss">
			<div id="toggle" class="closed"><span class="genericon genericon-menu"></span></div>
			<div class="leaderboard_ad">
				<?php
					if ( $leaderboard_ad != '' ) {
						echo '<a href="'. $leaderboard_ad_url .'" onClick="_gaq.push([\'_trackEvent\', \'26 Strong Ad Banner\', \'Click\', \'728x90_1\',,true]);">
							<img style="border: 0px" src="'. $leaderboard_ad .'" alt="Saucony | Explore The Ride 7" onload="_gaq.push([\'_trackEvent\', \'26 Strong Ad Banner\', \'Impression\', \'728x90_1\',,true]);" />
						</a>';
					}
				?>
			</div>

			<div class="navi">
				<div class="tss_logo">
					<a href="<?php echo get_permalink($post_ID); ?>"><img class="tss_logo_svg" src="<?php echo get_bloginfo('stylesheet_directory');?>/images/twosix-strong/26strong-logo.svg" alt="26 Strong Presented By Saucony&reg;" title="26 Strong Presented By Saucony&reg;"></a>
				</div>

				<div class="main_navi">
					<ul>
						<li><a href="#home">Home</a></li>
						<li><a href="#blog">The Blog</a></li>
						<li><a href="#teams">Teams</a></li>
						<li><a href="#twentysixstrong">#26Strong</a></li>
					</ul>
				</div>

				<div class="tss_sponsor">
					<img src="<?php echo get_bloginfo('stylesheet_directory');?>/images/twosix-strong/sponsor.png" alt="26 Strong Presented By Saucony&reg;" title="26 Strong Presented By Saucony&reg;">
				</div>
			</div>

			<div class="tss_module1_container">
				<div class="tss_module1" id="home">
					<div class="tss_module1_txt">
						<div class="tss_title">
							<h1>13 Coaches<br />13 Cadets<br /><span class="tss_red">26 Strong</span></h1>
						</div>

						<?php echo $intro_text; ?>

						<div class="tss_link_right">
							<a href="#teams">Meet the Teams</a> <span class="genericon genericon-rightarrow"></span></span>
						</div>
					</div>

					<div class="hypnotoad_unit hypnotoad_300x250">
						<?php
							if ( $top_ad_300x250 != '' ) {
								echo '
									<a href="'. $top_ad_300x250_url .'" onClick="_gaq.push([\'_trackEvent\', \'26 Strong Ad Banner\', \'Click\', \'300x250_1\',,true]);" >
										<img style="border: 0px" src="'. $top_ad_300x250 .'" alt="Saucony | Explore The Ride 7" onload="_gaq.push([\'_trackEvent\', \'26 Strong Ad Banner\', \'Impression\', \'300x250_1\',,true]);" />
									</a>
								';
							}
						?>
					</div>
				</div>
			</div>

			<div class="tss_module3_container">
				<div class="tss_module3" id="blog">
					<div class="title_and_links">
						<h2 class="module_title">The Blog</h2>
						<div class="tss_link_right tss_bloglinks">
							<ul class="tss_module3_nav">

								<?php
									$s26s_object	= get_category_by_slug('saucony-26-strong');
									$s26s_id		= $s26s_object->term_id;
									$mtips_object	= get_term_by('slug', 'marathon-tips', 'post_tag');
									$mtips_id		= $mtips_object->term_id;
								?>

								<li><a href="<?php echo esc_url(get_category_link($s26s_id)); ?>">Team Blogs</a> <span class="genericon genericon-rightarrow"></span></li>
								<li><a href="<?php echo esc_url(get_tag_link($mtips_id)); ?>">Marathon Tips</a> <span class="genericon genericon-rightarrow"></span></li>
								<?php /* <li><a href="#">See All</a> <span class="genericon genericon-rightarrow"></span></li> */ ?>
							</ul>
						</div>
					</div>
					<div class="tss_blogroll">
						<ul>
							<?php
								$args = array(
									'posts_per_page'	=> 3,
									'category_name'		=> 'saucony-26-strong'
								);
								$query = new WP_Query($args);

								if($query->have_posts()):
									while($query->have_posts()) : $query->the_post();
										$posttag		= '';
										$posttaglink	= '';
										$posttags		= get_the_tags($post_ID);

										if ($posttags) {
											foreach($posttags as $tag) {
												if (strpos(strtolower($tag->name), 'team') !== false || strpos(strtolower($tag->name), 'tips') !== false) {
													$posttag		= $tag->name;
													$posttaglink	= get_tag_link($tag->term_id);
												}
											}
										}

										$postcontent	= get_the_content();
										$trimmed		= wp_trim_words($postcontent, 100, '... <a href="'.get_permalink().'">Read More</a>');

										echo '<li>
											<div class="tss_tagname">'.$posttag.'</div>
											<h2><a href="'.get_permalink().'">'.get_the_title().'</a></h2>
											<p class="tss_postdate">'.get_the_date('F j, Y - g:i A').'</p>
											<div class="tss_excerpt">
												'.$trimmed.'
											</div>
											<div class="tss_link_right">
												<a href="'.$posttaglink.'">Read other posts by this team</a> <span class="genericon genericon-rightarrow"></span>
											</div>
										</li>';
									endwhile;
									wp_reset_postdata();
								endif;
							?>
						</ul>
						<div class="tss_link_right">
							<a href="<?php echo esc_url(get_category_link($s26s_id)); ?>">Read previous posts</a> <span class="genericon genericon-rightarrow"></span>
						</div>

					</div>
				</div>
			</div>
			<div class="hypnotoad_unit hypnotoad_960x400">
				<?php
					if ( $mid_ad_960x400 != '' ) {
						echo '
							<a href="'. $mid_ad_960x400_url .'" target="_blank" onClick="_gaq.push([\'_trackEvent\', \'26 Strong Ad Banner\', \'Click\', \'960x400_1\',,true]);" >
								<img style="border: 0px" src="'. $mid_ad_960x400 .'" alt="Saucony | Explore The Ride 7" onload="_gaq.push([\'_trackEvent\', \'26 Strong Ad Banner\', \'Impression\', \'960x400_1\',,true]);" />
							</a>
						';
					}
				?>
			</div>
			<div class="tss_module2_container">
				<div class="tss_module2" id="teams">
					<h2>2015 Teams</h2>
					<div id="paginationHere"></div>
					<div class="slidewrap">
						<ul class="tss_slider" id="sliderName">
							<?php
								for($i = 1; $i < 14; $i++) {
									$coach_name		= 'team_' . $i . '_coach_name';
									$$coach_name	= get_post_meta($post_ID, '_team_' . $i . '_coach_name', true);
									$coach_ig		= 'team_' . $i . '_coach_ig';
									$$coach_ig		= get_post_meta($post_ID, '_team_' . $i . '_coach_ig', true);
									$coach_tw		= 'team_' . $i . '_coach_tw';
									$$coach_tw		= get_post_meta($post_ID, '_team_' . $i . '_coach_tw', true);
									$coach_blog		= 'team_' . $i . '_coach_blog';
									$$coach_blog	= get_post_meta($post_ID, '_team_' . $i . '_coach_blog', true);
									$coach_bio		= 'team_' . $i . '_coach_bio';
									$$coach_bio		= apply_filters( 'the_content', get_post_meta($post_ID, '_team_' . $i . '_coach_bio', true) );
									// $coach_more	= 'team_' . $i . '_coach_more';
									// $$coach_more	= apply_filters( 'the_content', get_post_meta($post_ID, '_team_' . $i . '_coach_more', true) );
									$coach_pic		= 'team_' . $i . '_coach_pic';
									$$coach_pic		= get_post_meta($post_ID, '_team_' . $i . '_coach_pic', true);

									$cadet_name		= 'team_' . $i . '_cadet_name';
									$$cadet_name	= get_post_meta($post_ID, '_team_' . $i . '_cadet_name', true);
									$cadet_ig		= 'team_' . $i . '_cadet_ig';
									$$cadet_ig		= get_post_meta($post_ID, '_team_' . $i . '_cadet_ig', true);
									$cadet_tw		= 'team_' . $i . '_cadet_tw';
									$$cadet_tw		= get_post_meta($post_ID, '_team_' . $i . '_cadet_tw', true);
									$cadet_blog		= 'team_' . $i . '_cadet_blog';
									$$cadet_blog	= get_post_meta($post_ID, '_team_' . $i . '_cadet_blog', true);
									$cadet_bio		= 'team_' . $i . '_cadet_bio';
									$$cadet_bio		= apply_filters( 'the_content', get_post_meta($post_ID, '_team_' . $i . '_cadet_bio', true) );
									// $cadet_more	= 'team_' . $i . '_cadet_more';
									// $$cadet_more	= apply_filters( 'the_content', get_post_meta($post_ID, '_team_' . $i . '_cadet_more', true) );
									$cadet_pic		= 'team_' . $i . '_cadet_pic';
									$$cadet_pic		= get_post_meta($post_ID, '_team_' . $i . '_cadet_pic', true); ?>

									<li class="tss_slide">
										<h3 class="slidehed">Team <?php echo $i; ?></h3>
										<div class="tss_link_right">

											<?php
												$siteurl		= get_site_url(get_current_blog_id(), '/tag/');
												$teambloglink	= $siteurl . 'team-' . $i;
											?>

											<a href="<?php echo $teambloglink; ?>">Read posts from this team</a> <span class="genericon genericon-rightarrow"></span>
										</div>
										<div class="tss_coach_cadet">
											<div class="shape">
												<?php
													if( $$coach_pic != '' ) {
														$coach_avatar = $$coach_pic;
													} else {
														$coach_avatar = get_bloginfo('stylesheet_directory') . '/images/twosix-strong/' . 'no_photo.jpg';
													}
												?>
												<img class="avatar" src="<?php echo $coach_avatar; ?>" alt="<?php echo $$coach_name; ?>" title="<?php echo $$coach_name; ?>">
												<div class="socnet">
													<ul>
														<?php if( $$coach_tw ) {
															echo '<li><a href="'. $$coach_tw .'" target="_blank"><span class="genericon genericon-twitter"></span></a></li>';
														}
														if ( $$coach_ig != '' ) {
															echo '<li><a href="'. $$coach_ig .'" target="_blank"><span class="genericon genericon-instagram"></span></a></li>';
														}
														if ( $$coach_blog != '' ) {
															echo '<li><a href="'. $$coach_blog .'" target="_blank"><span class="genericon genericon-link"></span></a></li>';
														}
														?>
													</ul>
												</div>
												<div class="top"></div>
											</div>
											<div class="coach_cadet_txt">
												<h3 class="tss_name"><?php echo $$coach_name; ?></h3>
												<h4 class="tss_role">Coach</h4>
												<div class="tss_dossier">
													<?php echo $$coach_bio; ?>
													<?php /*
													<p class="tss_read_more">Read more</p>
													<div>
														<?php echo $$coach_more; ?>
													</div>
													*/ ?>
												</div>
											</div>
										</div>
										<div class="tss_coach_cadet">
											<div class="shape">
												<?php
													if( $$cadet_pic != '' ) {
														$cadet_avatar = $$cadet_pic;
													} else {
														$cadet_avatar = get_bloginfo('stylesheet_directory') . '/images/twosix-strong/' . 'no_photo.jpg';
													}
												?>
												<img class="avatar" src="<?php echo $cadet_avatar; ?>" alt="<?php echo $$cadet_name; ?>" title="<?php echo $$cadet_name; ?>">
												<div class="socnet">
													<ul>
														<?php if( $$cadet_tw != '' ) {
															echo '<li><a href="'.$$cadet_tw.'" target="_blank"><span class="genericon genericon-twitter"></span></a></li>';
														}
														if ( $$cadet_ig != '' ) {
															echo '<li><a href="'.$$cadet_ig.'" target="_blank"><span class="genericon genericon-instagram"></span></a></li>';
														}
														if ( $$cadet_blog ) {
															echo '<li><a href="'. $$cadet_blog .'" target="_blank"><span class="genericon genericon-link"></span></a></li>';
														} ?>
													</ul>
												</div>
												<div class="top"></div>
											</div>
											<div class="coach_cadet_txt">
												<h3 class="tss_name"><?php echo $$cadet_name; ?></h3>
												<h4 class="tss_role">Cadet</h4>
												<div class="tss_dossier">
													<?php echo $$cadet_bio; ?>
													<?php /*
													<p class="tss_read_more">Read more</p>
													<div>
														<?php echo $$cadet_more; ?>
													</div>
													*/ ?>
												</div>
											</div>
										</div>
									</li>
								<?php }
							?>
						</ul>
					</div>
				</div>
			</div>
			<div class="tss_module4_container">
				<div class="tss_module4" id="twentysixstrong">
					<div class="title_and_links">
						<h2 class="module_title">Instagram</h2>
						<div class="tss_link_right tss_instagram">
							#26Strong</a>
						</div>
					</div>
					<div>
						<?php echo $instagram_script; ?>
					</div>
				</div>
			</div>

			<div class="tss_modulefoot_container">
				<div class="tss_modulefoot">
					<div class="hypnotoad_unit footer_ad_300x250">
						<?php
							if ( $bot_ad_300x250 != '' ) {
								echo '
									<a href="'. $bot_ad_300x250_url .'" onClick="_gaq.push([\'_trackEvent\', \'26 Strong Ad Banner\', \'Click\', \'300x250_2\',,true]);" >
										<img style="border: 0px" src="'. $bot_ad_300x250 .'" alt="Saucony | Explore The Ride 7" onload="_gaq.push([\'_trackEvent\', \'26 Strong Ad Banner\', \'Impression\', \'300x250_2\',,true]);" />
									</a>
								';
							}
						?>
					</div>
					<div class="tss_modulefoot_txt">
						<div class="footer_logo">
							<img class="tss_logo_svg" src="<?php echo get_bloginfo('stylesheet_directory');?>/images/twosix-strong/26strong-logo.svg" alt="26 Strong Presented By Saucony&reg;" title="26 Strong Presented By Saucony&reg;">
						</div>
						<div class="roll_call">
							<div class="roll_call_left">
								<h4>CGI Network</h4>
								<ul>
									<li><a onclick="_gaq.push(['_trackEvent', 'Footer Links', 'running', 'http://running.competitor.com'])" href="http://running.competitor.com">Competitor Running</a></li>
									<li><a onclick="_gaq.push(['_trackEvent', 'Footer Links', 'running', 'http://triathlon.competitor.com'])" href="http://triathlon.competitor.com">Triathlete</a></li>
									<li><a onclick="_gaq.push(['_trackEvent', 'Footer Links', 'running', 'http://velonews.competitor.com'])" href="http://velonews.competitor.com">VeloNews</a></li>
									<li><a onclick="_gaq.push(['_trackEvent', 'Footer Links', 'running', 'http://womensrunning.competitor.com/'])" href="http://womensrunning.competitor.com/">Women's Running</a></li>
									<li><a onclick="_gaq.push(['_trackEvent', 'Footer Links', 'running', 'http://www.gearbuzz.com/national'])" href="http://www.gearbuzz.com/national">GearBuzz</a></li>
									<li><a onclick="_gaq.push(['_trackEvent', 'Footer Links', 'running', 'http://triathlete-europe.competitor.com'])" href="http://triathlete-europe.competitor.com">Triathlete Europe</a></li>
									<li><a onclick="_gaq.push(['_trackEvent', 'Footer Links', 'running', 'http://runnow.eu/'])" href="http://runnow.eu/">RunNow.eu</a></li>
								</ul>
							</div>
							<div class="roll_call_right">
								<h4>CGI Events</h4>
								<ul>
									<li><a onclick="_gaq.push(['_trackEvent', 'Footer Links', 'running', 'http://runrocknroll.competitor.com/'])" href="http://runrocknroll.competitor.com/">Rock 'n' Roll Marathon Series</a></li>
									<li><a onclick="_gaq.push(['_trackEvent', 'Footer Links', 'running', 'http://carlsbad.competitor.com/'])" href="http://carlsbad.competitor.com/">Carlsbad 5000</a></li>
									<li><a onclick="_gaq.push(['_trackEvent', 'Footer Links', 'running', 'http://running.competitor.com/endurance-live'])" href="http://running.competitor.com/endurance-live">Endurance Live</a></li>
									<li><a onclick="_gaq.push(['_trackEvent', 'Footer Links', 'running', 'http://kidsrock.competitor.com/'])" href="http://kidsrock.competitor.com/">Kids Rock</a></li>
									<li><a onclick="_gaq.push(['_trackEvent', 'Footer Links', 'running', 'http://nationstri.com/'])" href="http://nationstri.com/">NationsTri</a></li>
									<li><a onclick="_gaq.push(['_trackEvent', 'Footer Links', 'running', 'http://runmudrun.competitor.com/'])" href="http://runmudrun.competitor.com/">Run Mud Run</a></li>
									<li><a onclick="_gaq.push(['_trackEvent', 'Footer Links', 'running', 'http://trirock.competitor.com'])" href="http://trirock.competitor.com">TriRock Triathlon Series</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		// smooth scrolling
		jQuery(function($) {
			$('a[href*=#]:not([href=#])').click(function() {
				if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
					var target = $(this.hash);
					target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
					if (target.length) {
						$('html,body').animate({
							scrollTop: target.offset().top
						}, 1000);
						return false;
					}
				}
			});
		});
		jQuery(function($) {
			jQuery( ".tss_dossier" ).accordion({
				collapsible: true,
				header: '.tss_read_more',
				active: true,
				heightStyle: 'content'
			});
		});
		jQuery(document).ready(function() {
			jQuery('.slidewrap').carousel({
				slider: '.tss_slider',
				slide: '.tss_slide',
				slideHed: '.slidehed',
				nextSlide : '.next',
				prevSlide : '.prev',
				addPagination: '#paginationHere',
				addNav : true
			});
		});
		var navigation = responsiveNav(".nav-collapse", {
			customToggle: "#toggle",
			open: function () {
				var toggle = document.getElementById("toggle");
				toggle.className = toggle.className.replace(/(^|\s)closed(\s|$)/, "opened");
			},
			close: function () {
				var toggle = document.getElementById("toggle");
				toggle.className = toggle.className.replace(/(^|\s)opened(\s|$)/, "closed");
			}
		});
	</script>

<?php
	$is_mobile		= is_mobile();
	$is_tablet		= is_tablet2();
	$the_sidebars	= wp_get_sidebars_widgets();
	// $count_footer_ad = count( $the_sidebars['desktop-anchor'] );

	$bloginfo	= get_bloginfo( 'name' );
	$name		= str_replace('.com', '', $bloginfo);
	$name		= strtolower($name);

	$siteurl			= get_bloginfo( 'url' );
	$name_array_slashes	= explode('/', $siteurl);
	$name_dots			= explode('.', $name_array_slashes[2]);
	$name_tracking		= strtolower($name_dots[0]);

	$technical_theme_options	= get_option( 'cgi_media_technical_options' );
	$twitter					= $technical_theme_options['site_twitter_handle'];
	$facebook_appid				= $technical_theme_options['facebook_appid'];
?>
<!-- Begin comScore Tag -->
<script>
	var _comscore = _comscore || [];
	_comscore.push({ c1: "2", c2: "9728917" });
	(function() {
		var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
		s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
		el.parentNode.insertBefore(s, el);
	})();
</script>
<noscript>
	<img src="http://b.scorecardresearch.com/p?c1=2&c2=9728917&cv=2.0&cj=1" />
</noscript>
<!-- End comScore Tag -->

<?php
	if($technical_theme_options['reinvigorate'] && $technical_theme_options['reinvigorate'] != "") { ?>
		<script type="text/javascript" src="http://include.reinvigorate.net/re_.js"></script>
		<script type="text/javascript">
		try {
			reinvigorate.track("<?php echo $technical_theme_options['reinvigorate'];?>");
		} catch(err) {}
		</script>
	<?php }

	if($technical_theme_options['mandelbrot'] && $technical_theme_options['mandelbrot'] != ""){ ?>
		<script type="text/javascript">
		// <![CDATA[
				var _mb_site_guid = "<?php echo $technical_theme_options['mandelbrot'];?>";
				(function(d, t){
					var mb = d.createElement(t), s = d.getElementsByTagName(t)[0];
					mb.async = mb.src = '//cdn.linksmart.com/linksmart_2.0.0.min.js';
					s.parentNode.insertBefore(mb, s);
				}(document, 'script'));
		// ]]>
		</script>
		<?php

	}
?>
<div id="fb-root"></div>
<script type="text/javascript">
// load the facebook javascript on all pages if there's an app id
<?php
/*  sferrino - 6.20.13
Relocated the facebook javascript down here to handle additional facebook
widgets without clashing
*/

if( $facebook_appid && $facebook_appid != "") { ?>

	window.fbAsyncInit = function() {
		FB.init({
			appId      : '<?php echo $facebook_appid;?>', // App ID
			status     : true, // check login status
			cookie     : true, // enable cookies to allow the server to access the session
			xfbml      : true  // parse XFBML
		});

		// Additional initialization code here
	};
	// Load the SDK Asynchronously
	(function(d){
		 var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
		 if (d.getElementById(id)) {return;}
		 js = d.createElement('script'); js.id = id; js.async = true;
		 js.src = "//connect.facebook.net/en_US/all.js";
		 ref.parentNode.insertBefore(js, ref);
	}(document));

<?php } ?>
	jQuery(document).ready(function() {
		jQuery('[placeholder]').focus(function() {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
					input.val('');
					input.removeClass('placeholder');
				}
			}).blur(function() {
				var input = $(this);
				if (input.val() == '' || input.val() == input.attr('placeholder')) {
					input.addClass('placeholder');
					input.val(input.attr('placeholder'));
				}
			}).blur();
		jQuery('[placeholder]').parents('form').submit(function() {
				$(this).find('[placeholder]').each(function() {
					var input = $(this);
					if (input.val() == input.attr('placeholder')) {
						input.val('');
					}
				})
			});
		jQuery('.popular-now').show();
		jQuery('.ga-tab').css("width", "33.33333333333333%");
		jQuery('.cb-tab').show();
	});
</script>

<?php
	wp_footer();

	if (isset($GLOBALS['meebo']) && ($GLOBALS['meebo'] == true)) { ?>
		<style type="text/css">
			#meebo img {
			max-width:inherit;
			}
		</style>
		<script type="text/javascript">
				Meebo('domReady');
		</script>
	<?php }


	if($technical_theme_options['eXelate']){
		// get all of the information to put in eXelate code:
		$category_string = '';
		$keyword_string = '';
		global $post;
		if(is_category()){
			$queried_category = get_term( get_query_var('cat'), 'category' );
			$category_string = $queried_category->name;
		}
		else if(is_archive()){
			$category_string = '';
		}
		else{
			foreach((get_the_category($post->ID)) as $category) {
					$category_string .= $category->cat_name . ',';
			}
			if($category_string != ""){
				$category_string = substr($category_string, 0, -1);
			}
		}
		if($_REQUEST['q']){
			$keyword_string = $_REQUEST['q'];
			$category_string = '';
		}
		else if($_POST['searchText']){
			$keyword_string = $_POST['searchText'];
			$category_string = '';
		}
		$url = site_url();
		$g = '';
		if($url == 'http://running.competitor.com'){
			$g = '001';
		}
		else if($url == 'http://velonews.competitor.com'){
			$g = '002';
		}
		else if($url == 'http://triathlon.competitor.com'){
			$g = '003';
		}
		else if($url == 'http://singletrack.competitor.com'){
			$g = '004';
		}

		echo '<SCRIPT TYPE="text/javascript" SRC="http://loadus.exelator.com/load/?p=349&g='.$g.'&c=118748&ctg='.$category_string.'
			&kw='.$keyword_string.'"></SCRIPT>';
	}
?>
</body>
</html>
