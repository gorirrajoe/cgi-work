<?php
	get_header();
?>

<div class="left_column">
	<div <?php post_class(); ?>>

		<?php
			$page = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			echo '<h1><img class="slashes slash_blue" src="' . get_bloginfo( 'stylesheet_directory') . '/images/running/running-slashes-blue.svg"> All Recent Articles: Page ' . $page . '</h1>';

			$args = array (
				'paged'					=> $page,
				'ignore_sticky_posts'	=> true,
				'posts_per_page'		=> 10
			);

			$the_query = new WP_Query( $args );

			if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();

				$permalink		= get_permalink();
				$title			= get_the_title();
				$excerpt		= get_the_excerpt();
				$post_classes	= get_post_class();

				$post_classes_string	= implode( ' ', $post_classes );
				$month_published		= get_the_time( 'M' );
				$day_published			= get_the_time( 'd' );

				if ( has_post_thumbnail() ) {
					$image_array	= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'thumbnail' );
					$thumb			= '<div class="featured_img_archive"><a class="overlay" href="' . $permalink . '" title="' . $title . '"></a>' . '<img class="archive_thumb" src="' . $image_array[0] . '"></div>';
				} else {
					$thumb = '<div class="featured_img_archive"><a class="overlay" href="' . $permalink . '" title="' . $title . '"></a>' . '<img class="archive_thumb" src="' . get_bloginfo('stylesheet_directory') . '/images/running/default-post-thumbnail-sm.jpg"></div>';
				}


				echo '
					<div class="row archive_row ' . $post_classes_string . '">' .
						$thumb . '
						<h4><a href="' . $permalink . '">' . $title . '</a></h4>
						<p class="post_author">by ' . get_guest_author() . ', ' . format_timestamp_2() . '</p>
						<p class="excerpt">' . $excerpt . '</p>
					</div>
				';

			endwhile;
				wp_reset_postdata();
			else: ?>

				<div class="content">
					<p>Sorry, no posts matched your criteria.</p>
				</div>

			<?php endif;
		?>
	</div>


	<?php
		echo '<div class="row archive_navi">

			<div class="btn_primary">'.
				get_previous_posts_link('<span class="icon-left-open"></span> Previous Page') .'
			</div>

			<div class="btn_primary">'.
				get_next_posts_link('Next Page <span class="icon-right-open"></span>') .'
			</div>

		</div>';
	?>
</div><!-- row 1/content -->

<?php
	get_sidebar();
	get_footer();
?>
