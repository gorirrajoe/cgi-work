<?php
/* Template Name: Pace Calculator */
	$technical_theme_options = get_option( 'cgi_media_technical_options' );
	get_header();
?>
<style>
	/* @@ pace calc */
	.two_col {
		overflow:hidden;
	}
	.calc_div {
		border:1px solid #a7a9ac;
		padding:1.25em;
		margin:1.875em 0;
	}
	.calc_div h3 {
		padding-top:0;
	}
	.calc_div label {
		display:block;
		margin-bottom:.313em;
		cursor:pointer;
		font-style:italic;
		font-size:.875em;
	}
	.calc_div button {
		font-size:1em;
	}
	.vdot-form-container #results-div {
		display:none;
	}
	.calc-input {
		margin-right:.313em;
		font:italic 300 1em/1.42857 'Open Sans',sans-serif;
		padding:.625em;
	}
	.calc_div select {
		margin-bottom:.625em;
	}
	.calc_div table {
		margin-bottom:1.25em;
	}
	.pace-table-div .pace-table .heading {
		border-top:1px solid #a7a9ac;
		padding:.625em 0;
	}
	.pace-table-div .pace-table .calc_instructions {
		vertical-align:top;
		margin-bottom:1.875em;
	}
	.calc_instructions p {
		font-size:.875em;
	}
	@media screen and (min-width: 480px) {
		.left_stuff {
			float:left;
			width:50%;
		}
		.right_stuff {
			float:right;
			width:50%;
			padding-left:1.25em;
		}
	}
	@media screen and (min-width: 768px) {
		.left_stuff {
			width:40%;
		}
		.right_stuff {
			width:60%;
		}
	}
	@media screen and (min-width: 1300px) {
		.two_col_left {
			width:48.38709677419355%;
			float:left;
		}
		.two_col_right {
			width:48.38709677419355%;
			float:right;
		}
		.left_stuff {
			width:52%;
		}
		.right_stuff {
			width:48%;
		}
	}
</style>

<script>
	function showOption(){
		var selected_activity = document.getElementById("activity_type").value;
		if(selected_activity == "bike"){
			var weight_inputs = document.getElementsByClassName("weight-div");
			for( var i = 0; i < weight_inputs.length; i++){
				weight_inputs[i].style.display = "block";
			}
			var time_inputs = document.getElementsByClassName("time-div");
			for( var i = 0; i < time_inputs.length; i++){
				time_inputs[i].style.display = "block";
			}
			var swim_inputs = document.getElementsByClassName("swim-select");
			for( var i = 0; i < swim_inputs.length; i++){
				swim_inputs[i].style.display = "none";
			}
			var run_inputs = document.getElementsByClassName("speed-select");
			for( var i = 0; i < run_inputs.length; i++){
				run_inputs[i].style.display = "none";
			}
			var bike_inputs = document.getElementsByClassName("bike-select");
			for( var i = 0; i < bike_inputs.length; i++){
				bike_inputs[i].style.display = "block";
			}
			var calories_inputs = document.getElementsByClassName("calculate_calories");
			for( var i = 0; i < calories_inputs.length; i++){
				calories_inputs[i].style.display = "block";
			}
		}
		else if(selected_activity == "run"){
			var weight_inputs = document.getElementsByClassName("weight-div");
			for( var i = 0; i < weight_inputs.length; i++){
				weight_inputs[i].style.display = "block";
			}
			var time_inputs = document.getElementsByClassName("time-div");
			for( var i = 0; i < time_inputs.length; i++){
				time_inputs[i].style.display = "block";
			}
			var swim_inputs = document.getElementsByClassName("swim-select");
			for( var i = 0; i < swim_inputs.length; i++){
				swim_inputs[i].style.display = "none";
			}
			var run_inputs = document.getElementsByClassName("speed-select");
			for( var i = 0; i < run_inputs.length; i++){
				run_inputs[i].style.display = "block";
			}
			var bike_inputs = document.getElementsByClassName("bike-select");
			for( var i = 0; i < bike_inputs.length; i++){
				bike_inputs[i].style.display = "none";
			}
			var calories_inputs = document.getElementsByClassName("calculate_calories");
			for( var i = 0; i < calories_inputs.length; i++){
				calories_inputs[i].style.display = "block";
			}
		}
		else if(selected_activity == "swim"){
			var weight_inputs = document.getElementsByClassName("weight-div");
			for( var i = 0; i < weight_inputs.length; i++){
				weight_inputs[i].style.display = "block";
			}
			var time_inputs = document.getElementsByClassName("time-div");
			for( var i = 0; i < time_inputs.length; i++){
				time_inputs[i].style.display = "block";
			}
			var swim_inputs = document.getElementsByClassName("swim-select");
			for( var i = 0; i < swim_inputs.length; i++){
				swim_inputs[i].style.display = "block";
			}
			var run_inputs = document.getElementsByClassName("speed-select");
			for( var i = 0; i < run_inputs.length; i++){
				run_inputs[i].style.display = "none";
			}
			var bike_inputs = document.getElementsByClassName("bike-select");
			for( var i = 0; i < bike_inputs.length; i++){
				bike_inputs[i].style.display = "none";
			}
			var calories_inputs = document.getElementsByClassName("calculate_calories");
			for( var i = 0; i < calories_inputs.length; i++){
				calories_inputs[i].style.display = "block";
			}
		}
		else{
			var weight_inputs = document.getElementsByClassName("weight-div");
			for( var i = 0; i < weight_inputs.length; i++){
				weight_inputs[i].style.display = "none";
			}
			var time_inputs = document.getElementsByClassName("time-div");
			for( var i = 0; i < time_inputs.length; i++){
				time_inputs[i].style.display = "none";
			}
			var swim_inputs = document.getElementsByClassName("swim-select");
			for( var i = 0; i < swim_inputs.length; i++){
				swim_inputs[i].style.display = "none";
			}
			var run_inputs = document.getElementsByClassName("speed-select");
			for( var i = 0; i < run_inputs.length; i++){
				run_inputs[i].style.display = "none";
			}
			var bike_inputs = document.getElementsByClassName("bike-select");
			for( var i = 0; i < bike_inputs.length; i++){
				bike_inputs[i].style.display = "none";
			}
			var calories_inputs = document.getElementsByClassName("calculate_calories");
			for( var i = 0; i < calories_inputs.length; i++){
				calories_inputs[i].style.display = "none";
			}
		}
		var calories_burned_inputs = document.getElementsByClassName("calories-burned-div");
		for( var i = 0; i < calories_burned_inputs.length; i++){
			calories_burned_inputs[i].style.display = "none";
		}
	}
	function calculateCaloriesBurned(){
		var calories_burned_div = document.getElementById("calories_burned");
		var selected_activity = document.getElementById("activity_type").value;
		var weight = document.getElementById("weight").value;
		var time = document.getElementById("time").value;
		if(selected_activity == "bike"){
			var met = document.getElementById("bike-met").value;
			var kg = parseInt(weight) * 0.453592;
			var calories = parseInt(time) * (met * 3.5 * kg) / 200;
		}
		else if(selected_activity == "run"){
			var met = document.getElementById("running-met").value;
			var kg = parseInt(weight) * 0.453592;
			var calories = parseInt(time) * (met * 3.5 * kg) / 200;
		}
		else if(selected_activity == "swim"){
			var met = document.getElementById("swim-met").value;
			var kg = parseInt(weight) * 0.453592;
			var calories = parseInt(time) * (met * 3.5 * kg) / 200;
		}
		var calories_burned_inputs = document.getElementsByClassName("calories-burned-div");
		for( var i = 0; i < calories_burned_inputs.length; i++){
			calories_burned_inputs[i].style.display = "block";
		}
		calories_burned_div.innerHTML = Math.round(calories);
	}
</script>
<script>
	function calculateVDot(){
		var value = document.getElementById("mile-value").value;
		var array = value.split(":");
		var training_paces_list = document.getElementById("training-paces-list");
		if(array.length != 3){
			alert("Time not formatted correctly.");
			training_paces_list.innerHTML = '';
			return;
		}
		var distance = document.getElementById("value-select").value;
		var vdot = 30;
		for(var i = 85; i > 29; i--){
			if(compare(vDot_obj[i][distance], value)){
				if(i != 85){
					vdot = i + 1;
				}
				else{
					vdot = i;
				}
				break;
			}
		}
		var training_paces_string = '<li>Easy training pace: '+ vDot_obj[vdot]['easy'] + '</li>' +
			'<li>Marathon pace: '+ vDot_obj[vdot]['marathon'] + '</li>' +
			'<li>Threshold pace: '+ vDot_obj[vdot]['threshold'] + '</li>' +
			'<li>Interval pace: '+ vDot_obj[vdot]['interval'] + '</li>' +
			'<li>Repetition pace: '+ vDot_obj[vdot]['repetition'] + '</li>';
		training_paces_list.innerHTML =training_paces_string;
		document.getElementById("results-div").style.display = "block";
	}
	function compare(value1, value2){
		var array1 = value1.split(":");
		var array2 = value2.split(":");
		var seconds = array1[array1.length - 1];
		var mins = array1[array1.length - 2];
		var hours = 0;
		if(array1.length == 3){
			hours = array1[0];
		}
		var value1Seconds = hours*3600 + mins*60 + seconds;

		var seconds = array2[array2.length - 1];
		var mins = array2[array2.length - 2];
		var hours = 0;
		if(array2.length == 3){
			hours = array2[0];
		}
		var value2Seconds = hours*3600 + mins*60 + seconds;

		if(parseInt(value1Seconds) > parseInt(value2Seconds)){
			return true;
		}
		return false;
	}
</script>
	<div class="row">
		<div class="left_column">
			<h1><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> <?php the_title(); ?></h1>
			<div id="share-links">
				<?php
					if ($technical_theme_options['social_media_publisher_key'] && $technical_theme_options['social_media_publisher_key'] != ""){
						$publisher_key = $technical_theme_options['social_media_publisher_key'];
						$twitter_account = $technical_theme_options['site_twitter_handle']; ?>

								<div id="share-links">
							 		<script>
									var pwidget_config = {
										shareQuote: false,
										onshare:bk_addSocialChannel,
										defaults: {
											gaTrackSocialInteractions: true,
											retina: true,
											mobileOverlay: true,
											afterShare: false,
											sharePopups: true
										}
									};

							 			function gaTrack(network, action) {
							 				_gaq.push(['_trackSocial', network, action]);
							 			}
							 			//Populate BlueKai social sharer
									  function bk_addSocialChannel(channelName) {
									   	bk_addPageCtx('share', channelName);
									    BKTAG.doTag(39226,1);
									  }


							 		</script>

									<div id="sharingWidgetLarge">
										<div class="pw-widget pw-counter-horizontal" pw:twitter-via="<?php echo $twitter_account;?>">
											<a class="pw-button-facebook pw-look-native" onclick="gaTrack('Facebook','share')"></a>
											<a class="pw-button-twitter pw-look-native" onclick="gaTrack('Twitter','tweet')"></a>
											<a class="pw-button-googleplus pw-look-native" onclick="gaTrack('Google','+1')"></a>
											<a class="pw-button-pinterest pw-look-native" onclick="gaTrack('Pinterest','pin')"></a>
											<a class="pw-button-print pw-look-native pw-size-medium" onclick="gaTrack('Print','print')"></a>
										</div>
									</div>
									<div id="sharingWidgetSmall" style="display:none; " pw:twitter-via="<?php echo $twitter_account;?>">
										<div class="pw-widget pw-size-medium">
											<a class="pw-button-facebook pw-counter" onclick="gaTrack('Facebook','share')"></a>
											<a class="pw-button-twitter pw-counter" onclick="gaTrack('Twitter','tweet')"></a>
											<a class="pw-button-googleplus pw-counter" onclick="gaTrack('Google','+1')"></a>
											<a class="pw-button-pinterest pw-counter" onclick="gaTrack('Pinterest','pin')"></a>
											<a class="pw-button-print" onclick="gaTrack('Print','print')"></a>
										</div>
									</div>

              <script src="http://i.po.st/share/script/post-widget.js#init=immediate&amp;publisherKey=<?php echo $publisher_key;?>&amp;retina=true" type="text/javascript"></script>

							<script>
							jQuery(document).ready(function() {
								if(jQuery(window).width() < 482){
									jQuery('#sharingWidgetLarge').hide();
									jQuery('#sharingWidgetSmall').show();
								}
								if(jQuery(window).width() > 482){
									jQuery('#sharingWidgetLarge').show();
									jQuery('#sharingWidgetSmall').hide();
								}
							});

							jQuery(window).bind("resize", stickpw);
							function stickpw(e){
								if(jQuery(window).width() < 482){
									jQuery('#sharingWidgetLarge').hide();
									jQuery('#sharingWidgetSmall').show();
								}
								if(jQuery(window).width() > 482){
									jQuery('#sharingWidgetLarge').show();
									jQuery('#sharingWidgetSmall').hide();
								}
							}
							</script>
						</div>
					<?php }
				?>
			</div>
			<div class="content">
				<div class="two_col">
					<div class="two_col_left">
						<div class="calc_div pace-table-div">
							<h3>Pace Calculator</h3>
							<p>Our <strong>pace calculator</strong> can help you figure out the average pace for your most recent race or workout by simply inputting the length of time you ran and the distance you covered. It can also use pace and distance to help you calculate time and your overall time and pace to help solve for distance.</p>
							<div class="pace-table" id="pace-table">
								<div class="row">
									<div class="heading" >TIME</div>
									<div class="left_stuff">
										<table>
											<tr>
												<td><label for="time_hours_input">Hours</label></td>
												<td><label for="time_minutes_input">Mins</label></td>
												<td><label for="time_seconds_input">Secs</label></td>
											</tr>
											<tr>
												<td><input class="calc-input" id="time_hours_input" maxlength="2" size="2" type="text" value="0">
												</td>
												<td><input class="calc-input" id="time_minutes_input" maxlength="2" size="2" type="text" value="0">
												</td>
												<td><input class="calc-input" id="time_seconds_input" maxlength="2" size="2" type="text" value="0">
												</td>
											</tr>
										</table>
									</div>
									<div class="right_stuff calc_instructions">
										<p>To calculate time input distance and pace and click calculate time</p>
										<button id="calc_time" onclick="calculateTime()">Calculate Time</button>
									</div>
								</div>
								<div class="row">
									<div class="heading" >DISTANCE</div>
									<div class="left_stuff">
										<p><input id="distance_input" class="calc-input" type="text" value="0"></p>
										<select id="select_units">
											<option value="mile">
												Miles
											</option>

											<option value="km">
												Kilometers
											</option>

											<option value="m">
												Meters
											</option>

											<option value="yd">
												Yards
											</option>
										</select>
										<p>Or</p>
										<select id="select_event" onchange="populateDistance()">
											<option value="-1">
												Pick Event
											</option>

											<option value="26.219m">
												Marathon
											</option>

											<option value="13.1094m">
												Half Marathon
											</option>

											<option value="5k">
												5K
											</option>

											<option value="5m">
												5 Mile
											</option>

											<option value="8k">
												8K
											</option>

											<option value="10k">
												10K
											</option>

											<option value="15k">
												15K
											</option>

											<option value="10m">
												10 mile
											</option>

											<option value="10k">
												20K
											</option>

											<option value="15m">
												15 mile
											</option>

											<option value="25k">
												25K
											</option>

											<option value="30k">
												30K
											</option>

											<option value="20m">
												20 mile
											</option>
										</select>
									</div>
									<div class="right_stuff calc_instructions">
										<p>To calculate your distance input time and pace and click calculate distance</p>
										<button id="calc_distance" onclick="calculateDistance()">Calculate Distance</button>
									</div>
								</div>
								<div class="row">
									<div class="heading" >PACE</div>
									<div class="left_stuff">
										<table style="margin-bottom:.625em;">
											<tbody>
												<tr>
													<td><label for="pace_hours_input">Hours</label></td>

													<td><label for="pace_minutes_input">Mins</label></td>

													<td><label for="pace_seconds_input">Secs</label></td>
												</tr>


												<tr>
													<td><input id="pace_hours_input" maxlength="2" size="2" class="calc-input" type="text" value="0">
													</td>

													<td><input id="pace_minutes_input" maxlength="2" size="2" class="calc-input" type="text" value="0">
													</td>

													<td><input id="pace_seconds_input" maxlength="2" size="2" class="calc-input" type="text" value="0">
													</td>
												</tr>
											</tbody>
										</table>
										<label for="select_pace">Per</label>
										<select id="select_pace">
											<option value="mile">
												Mile
											</option>

											<option value="km">
												Kilometer
											</option>
										</select>
									</div>
									<div class="right_stuff calc_instructions">
										<p>To calculate pace input time and distance and click calculate pace</p>
										<button id="calc_pace" onclick="calculatePace()">Calculate Pace</button>
									</div>
								</div>

								<script type="text/javascript">
									// <![CDATA[
										showRunCalc();
									// ]]>
								</script>
							</div>
						</div>
					</div>
					<div class="two_col_right">
						<div class="calc_div vdot-form-container">
							<h3>Training Intensity Calculator</h3>
							<div class="vdot-inner-container">
								<p class="description">This tool will help you determine your training intensity based upon a recent race performance by measuring the rate and volume at which oxygen is consumed per minute.</p>
								<?php /*
								<p>The training intensity calculator takes your most recent race results and calculates equivalent performances at other distances while also providing you with pacing guidelines for easy runs, speed workouts, tempo runs and long runs.</p>
								*/ ?>


								<p class="description"><a href="http://www.coacheseducation.com/endur/jack-daniels-nov-00.htm" target="_blank">Learn More</a>
								</p>
								<p>
									<label for="mile-value">Input time as hh:mm:ss</label>
									<input id="mile-value" size="8" class="calc-input" type="text">
								</p>
								<select id="value-select">
									<option value="Mile">
										Mile
									</option>

									<option value="5k">
										5k
									</option>

									<option value="10k">
										10k
									</option>

									<option value="13.1M">
										1/2 Marathon
									</option>

									<option value="Marathon">
										Marathon
									</option>
								</select>
								<button onclick="calculateVDot()">Calculate</button>

								<div id="results-div">
									<p><strong>Training Paces:</strong></p>
									<ul id="training-paces-list">
									</ul>
								</div>
							</div>
						</div>
						<div class="calc_div calories-burned-form-container">
							<h3>Calorie Burn Calculator</h3>
							<div class="calories-burned-inner-container">
								<p>The <strong>calorie burn calculator</strong> uses your weight, speed and the duration of your workouts to calculate how many calories you burned while running, swimming or biking.</p>
								<p class="description">Please note: results are an estimate, actual results will vary depending on age, gender and overall fitness level.</p>
								<label for="activity_type">Activity Type:</label>
								<select id="activity_type" onchange="showOption()">
									<option selected="selected" value="-1">
										Select Activity
									</option>

									<option value="bike">
										Bike
									</option>

									<option value="run">
										Run
									</option>

									<option value="swim">
										Swim
									</option>
								</select>
								<table>
									<tbody>
										<tr>
											<td  style="padding-bottom:0;">
												<label class="bike-select" style="display: none;">Speed</label>
												<select class="bike-select" id="bike-met" style="display: none;">
													<option value="14.0">
														bicycling, mountain, uphill, vigorous
													</option>

													<option value="16.0">
														bicycling, mountain, competitive, racing
													</option>

													<option value="8.5">
														bicycling, BMX
													</option>

													<option value="8.5">
														bicycling, mountain, general
													</option>

													<option value="7.5">
														bicycling, general
													</option>

													<option value="3.5">
														bicycling, leisure, 5.5 mph
													</option>

													<option value="5.8">
														bicycling, leisure, 9.4 mph
													</option>

													<option value="6.8">
														bicycling, 10-11.9 mph, slow, light effort
													</option>

													<option value="8.0">
														bicycling, 12-13.9 mph, moderate effort
													</option>

													<option value="10.0">
														bicycling, 14-15.9 mph, vigorous effort
													</option>

													<option value="12.0">
														bicycling, 16-19 mph, racing
													</option>

													<option value="15.8">
														bicycling, &gt; 20 mph, racing
													</option>
												</select>
											</td>
										</tr>
										<tr>
											<td  style="padding-bottom:0;">
												<label class="speed-select" style="display: block;">Speed</label>
												<select class="speed-select" id="running-met" style="display: block;">
													<option value="6.0">
														running, 4 mph (15 min/mile)
													</option>

													<option value="8.3">
														running, 5 mph (12 min/mile)
													</option>

													<option value="9.0">
														running, 5.2 mph (11.5 min/mile)
													</option>

													<option value="9.8">
														running, 6 mph (10 min/mile)
													</option>

													<option value="10.5">
														running, 6.7 mph (9 min/mile)
													</option>

													<option value="11.0">
														running, 7 mph (8.5 min/mile)
													</option>

													<option value="11.8">
														running, 7.5 mph (8 min/mile)
													</option>

													<option value="11.8">
														running, 8 mph (7.5 min/mile)
													</option>

													<option value="12.3">
														running, 8.6 mph (7 min/mile)
													</option>

													<option value="12.8">
														running, 9 mph (6.5 min/mile)
													</option>

													<option value="14.5">
														running, 10 mph (6 min/mile)
													</option>

													<option value="16.0">
														running, 11 mph (5.5 min/mile)
													</option>

													<option value="19.0">
														running, 12 mph (5 min/mile)
													</option>

													<option value="19.8">
														running, 13 mph (4.6 min/mile)
													</option>

													<option value="23.0">
														running, 14 mph (4.3 min/mile)
													</option>
												</select>
											</td>
										</tr>
										<tr>
											<td >
												<label class="swim-select" style="display: none;">Speed</label>
												<select class="swim-select" id="swim-met" style="display: none;">
													<option value="9.8">
														freestyle, fast, vigorous effort
													</option>

													<option value="5.8">
														freestyle, light or moderate effort
													</option>

													<option value="9.5">
														backstroke, training or competition
													</option>

													<option value="4.8">
														backstroke, recreational
													</option>

													<option value="10.3">
														breaststroke, training or competition
													</option>

													<option value="5.3">
														breaststroke, recreational
													</option>

													<option value="13.8">
														butterfly, general
													</option>

													<option value="10.0">
														crawl, fast speed, ~75 yds/min
													</option>

													<option value="8.3">
														crawl, medium speed, ~50 yds/min
													</option>

													<option value="7.0">
														sidestroke, general
													</option>
												</select>
											</td>
										</tr>
										<tr>
											<td  style="padding:.625em 0;">
												<label class="weight-div" style="display: block;" for="weight">Weight in pounds:</label>
												<input class="calc-input weight-div" id="weight" size="5" style="display: block;" type="text">
											<td>
										</tr>
										<tr>
											<td  style="padding:.625em 0;">
												<label class="time-div" for="time" style="display: block;">Time in minutes:</label>
												<input class="calc-input time-div" id="time" size="5" style="display: block;" type="text">
											</td>
										</tr>
										<tr>
											<td  style="padding:.625em 0;">
												<button class="calculate_calories" onclick="calculateCaloriesBurned()" style="display: block;" type="submit">Calculate</button>
											</td>
										</tr>
										<tr>
											<td>
												<p class="calories-burned-div" style="font-weight:bold; display: none;">
													Calories Burned: <span class="calories-burned-div" id="calories_burned"></span>
												</p>
											</td>
										</tr>
									</tbody>
								</table>
							</div>


							<script>
								showOption();
							</script>
						</div>

					</div>
				</div>
			</div>
		</div>
		<?php get_sidebar(); ?>
	</div><!-- row 1/content -->
<?php get_footer(); ?>
