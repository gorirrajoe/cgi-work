<?php
	$is_mobile	= is_mobile();
	$is_tablet	= is_tablet2();

	$the_sidebars	= wp_get_sidebars_widgets();

	$bloginfo		= get_bloginfo( 'name' );
	$name_tracking	= 'running';
	$name			= str_replace( '.com', '', $bloginfo );
	$name			= strtolower( $name );

	$technical_theme_options	= get_option( 'cgi_media_technical_options' );
	$editorial_theme_options	= get_option( 'cgi_media_editorial_options' );

	$magazine_cover			= isset( $editorial_theme_options['magazine_ipad'] ) ? $editorial_theme_options['magazine_ipad'] : '';
	$footer_icontact_txt	= isset( $editorial_theme_options['footer_icontact_txt'] ) ? $editorial_theme_options['footer_icontact_txt'] : '';
	$twitter				= isset( $technical_theme_options['site_twitter_handle'] ) ? $technical_theme_options['site_twitter_handle'] : '';
	$facebook_page			= isset( $technical_theme_options['site_facebook_page'] ) ? $technical_theme_options['site_facebook_page'] : '';
	$youtube				= isset( $technical_theme_options['youtube_page'] ) ? $technical_theme_options['youtube_page'] : '';
	$instagram				= isset( $technical_theme_options['site_instagram_handle'] ) ? $technical_theme_options['site_instagram_handle'] : '';

	$bloginfo			= get_bloginfo( 'url' );
	$name_array_slashes	= explode('/', $bloginfo);
	$name_dots			= explode('.', $name_array_slashes[2]);
	$name_tracking		= strtolower($name_dots[0]);
?>

		<div class="row">

			<div class="hypnotoad_unit_container">
				<div class="hypnotoad_unit">
					<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('bottom') : ''; ?>
				</div><!-- ad unit -->
			</div>

			<div class="footer">
				<div class="footer_col_1 footer_col">
					<h4>Subscribe</h4>
					<div class="subscribe_group">
						<div class="digi_band">
							<h5><a href="<?php echo site_url(); ?>/digital">Digital Edition</a></h5>
						</div><div class="mag_image">
							<img src="<?php echo $magazine_cover; ?>">
						</div>
					</div>
				</div>

				<div class="footer_col_2 footer_col">
					<h4>Follow Us</h4>
					<ul>
						<?php if($facebook_page != '') {
							echo '<li><a href="https://www.facebook.com/'.$facebook_page.'" target="_blank" onclick="_gaq.push([\'_trackEvent\', \'Footer Social Links\', '.$name_tracking.', '.$technical_theme_options['youtube_page'].']);"><span class="footer_icons icon-facebook"></span>/competitor.running</a></li>';
						}

						if($youtube != '') {
							echo '<li><a href="'.$youtube.'" target="_blank"><span class="footer_icons icon-youtube"></span>/competitor</a></li>';
						}

						if($twitter != '') {
							echo '<li><a href="https://twitter.com/'.$twitter.'" target="_blank"><span class="footer_icons icon-twitter"></span>/runcompetitor</a></li>';
						}

						if($instagram != '') {
							echo '<li><a href="http://instagram.com/'.$instagram.'" target="_blank"><span class="footer_icons icon-instagramm"></span>/runcompetitor</a></li>';
						} ?>
					</ul>
				</div>

				<div class="footer_col_3 footer_col">

					<?php inline_iContact("footer"); ?>

				</div> <!-- end .footer_col_3 -->

			</div> <!-- .footer -->


			<div class="sub_footer">
				<div class="sub_footer_col_1">
					<h5>Digital Network</h5>
					<?php
						wp_nav_menu( array(
							'menu'				=> 'footer-col-2-a',
							'container'			=> false,
							'fallback_cb'		=> false,
							'menu_class'		=> 'sublist',
							'walker'			=> new footer_walker(),
							'theme_location'	=> 'digital_network'
						) );
					?>
				</div>

				<!-- <div class="sub_footer_col_2">
					<h5>Events Network</h5>
					<?php
						wp_nav_menu( array(
							'menu'				=> 'footer-col-1-a',
							'container'			=> false,
							'fallback_cb'		=> false,
							'menu_class'		=> 'sublist',
							'walker'			=> new footer_walker(),
							'theme_location'	=> 'events_network'
						) );
					?>
				</div> -->

				<div class="sub_footer_col_3">
					<p class="copyright">&copy; <?php echo date( 'Y' ); ?> Pocket Outdoor Media, LLC. All Rights Reserved</p>
					<ul>
						<li><a href="http://pocketoutdoormedia.com/privacy-policy/" target="_blank" onClick="_gaq.push(['_trackEvent', 'Footer Links', '<?php echo $name_tracking; ?>', 'http://pocketoutdoormedia.com/privacy-policy/']);">Privacy Policy</a></li>
						<li><a href="<?php echo get_option('home'); ?>/contact" onClick="_gaq.push(['_trackEvent', 'Footer Links', '<?php echo $name_tracking; ?>', '<?php echo get_option("home"); ?>/contact']);">Contact</a></li>
					</ul>
				</div>
			</div>
		</div> <!-- end .row -->
	</div>
</div>
<!-- slidebars -->
<?php
	get_sidebar( 'slidebar-left' );
	get_sidebar( 'slidebar-right' );
	wp_footer();

	do_action( 'body_end_hook' );
?>
</body>
</html>
