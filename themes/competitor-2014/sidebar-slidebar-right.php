<?php
	$technical_theme_options	= get_option( 'cgi_media_technical_options' );
	$name_tracking				= 'running';
?>

<div class="sb-slidebar sb-right sb-width-wide sb-static">
	<div class="social_row">
		<ul class="side_social_icons">

			<?php if ( $technical_theme_options['site_facebook_page'] != '' ) { ?>

				<li>
					<a target="_blank" href="https://facebook.com/<?php echo $technical_theme_options['site_facebook_page']; ?>" title="Like us on Facebook" onclick="_gaq.push(['_trackEvent', 'Slidebar Right Links', '<?php echo $name_tracking;?>', 'https://facebook.com/<?php echo $technical_theme_options['site_facebook_page']; ?>']);">
						<span class="icon-facebook"></span>
					</a>
				</li>

			<?php }


			if ( $technical_theme_options['site_twitter_handle'] != '' ) { ?>

				<li>
					<a target="_blank" href="https://twitter.com/<?php echo $technical_theme_options['site_twitter_handle']; ?>" title="Follow us on Twitter" onclick="_gaq.push(['_trackEvent', 'Slidebar Right Links', '<?php echo $name_tracking;?>', 'https://twitter.com/<?php echo $technical_theme_options['site_twitter_handle']; ?>']);">
						<span class="icon-twitter"></span>
					</a>
				</li>

			<?php }


			if ( $technical_theme_options['site_instagram_handle'] != '' ) { ?>

				<li>
					<a target="_blank" href="http://instagram.com/<?php echo $technical_theme_options['site_instagram_handle']; ?>" title="Follow us on Instagram" onclick="_gaq.push(['_trackEvent', 'Slidebar Right Links', '<?php echo $name_tracking;?>', 'http://instagram.com/<?php echo $technical_theme_options['site_instagram_handle']; ?>']);">
						<span class="icon-instagramm"></span>
					</a>
				</li>

			<?php }


			if ( $technical_theme_options['youtube_page'] != '' ) { ?>

				<li>
					<a target="_blank" href="<?php echo $technical_theme_options['youtube_page']; ?>" title="Follow us on YouTube" onclick="_gaq.push(['_trackEvent', 'Slidebar Right Links', '<?php echo $name_tracking;?>', '<?php echo $technical_theme_options['youtube_page']; ?>']);">
						<span class="icon-youtube"></span>
					</a>
				</li>

			<?php } ?>

		</ul>

		<span class="icon-cancel sb-close"></span>
	</div><!-- social -->


	<div class="side_row nav_group">
		<?php
			wp_nav_menu( array(
				'menu'				=> 'slidebar-right-nav',
				'menu_class'		=> 'slidebar_right_nav',
				'container'			=> false,
				'fallback_cb'		=> 'false',
				'walker'			=> new Slidebar_Walker(),
				'theme_location'	=> 'slidebar_right_nav'
			) );
		?>

	</div><!-- side nav group -->


	<div class="side_row video_group">
		<h3><img class="slashes slash_white" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-white.svg"> Videos</h3>
		<?php
			$video_cat_id = $technical_theme_options['video_category'] ? $technical_theme_options['video_category'] : 15;
			get_videos_side( $video_cat_id );
		?>
	</div>


	<div class="side_row photos_group">
		<h3><img class="slashes slash_white" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-white.svg"> Photos</h3>
		<?php
			$photo_cat_id = $technical_theme_options['gallery_category'] ? $technical_theme_options['gallery_category'] : 14;
			get_photos_side( $photo_cat_id );
		?>
	</div>

</div><!-- right sidebar -->
