<?php
if ( defined('ABSPATH') ) {
	require_once(ABSPATH . 'wp-load.php');
} else {
	require_once("../../../wp-load.php");
}
/*
 * 
CREATE TABLE `wp_newsletters` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `email` varchar(128) NOT NULL,
 `brand` varchar(48) NOT NULL,
 `lists` varchar(128) NOT NULL,
 `nameFirst` varchar(64) NOT NULL,
 `nameLast` varchar(64) NOT NULL,
 `state` varchar(50) NOT NULL,
 `country` varchar(100) NOT NULL,
 `gender` varchar(20) NOT NULL,
 `other` varchar(128) NOT NULL,
 `ip` varchar(20) NOT NULL,
 `submit_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
 */
//test
/*$xml=simplexml_load_string("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><RaceItRequest user=\"dweed@competitorgroup.com\" password=\"cgiR0ck$\"><getSignups><brand>velo</brand><idsearch><after>20</after></idsearch></getSignups></RaceItRequest>") or die("Error: Cannot create object");*/
/*$xml=simplexml_load_string("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><RaceItRequest user=\"dweed@competitorgroup.com\" password=\"cgiR0ck$\"><getSignups><brand>velo</brand></getSignups></RaceItRequest>") or die("Error: Cannot create object");*/

$dataPOST = trim(file_get_contents('php://input'));
$xml=simplexml_load_string($dataPOST) or die("Error: Cannot create object");
//print_r($xml);

//get username and password
foreach($xml->attributes() as $a => $b) {
    if($a == 'user'){
    	$username = $b;
    }
    if($a == 'password'){
    	$password_s = $b;
    }
}

$xml_output = new SimpleXMLElement('<xml/>');

//check username and password
if($username != 'dweed@competitorgroup.com' and $password_s != 'cgiR0ck$'){
	$track = $xml_output->addChild('CGIRequestFail');
	$track->addAttribute('error', "loginfail");
	Header('Content-type: text/xml');
	print($xml_output->asXML());
	exit();
}else{
	//get requested info
	//[getSignups] => SimpleXMLElement Object ( [brand] => velo [regID] => SimpleXMLElement Object ( [from] => 20 ) ) )
	//print_r($xml);
	foreach ($xml->{"getSignups"}->children() as $criteria => $values) {
		switch($criteria){
			case 'brand':
				$brand_search = $values;
				break;
			case 'idsearch':
				foreach ($values->children() as $period => $iId) {
					$nIntVal = intval($iId);
					if($nIntVal <= 0){
						$track->addAttribute('error', "invalidSearchId");
						break;
					}
					if($period == "after"){
						$after = $iId;
					}
					if($period == "before"){
						$before = $iId;
					}
				}
				break;
			case 'timestampsearch':
				foreach ($values->children() as $iDate => $tStamp) {
					if(intval($tStamp) <= 0){
						$track->addAttribute('error', "invalidTimeStamp");
						break;
					}
					if($iDate == "start"){
						$start = $tStamp;
					}
					if($iDate == "end"){
						$end = $tStamp;
					}
				}
				break;
			case 'limit':
				$limit_search = $values;
				break;
			default:
				break;
		}
	}
	//make query
	$sql = "SELECT * FROM wp_newsletters WHERE 1";
	if(isset($brand_search) AND $brand_search != ""){
		$sql .= " AND brand = '".$brand_search."'";
	}else{
		$sql .= ' AND brand != ""';
	}
	if(isset($after) AND !empty($after)){
		$sql .= " AND id >= ".$after;
	}
	if(isset($before) AND !empty($before)){
		$sql .= " AND id <= ".$before;
	}
	if(isset($start) AND !empty($start)){
		$sql .= ' AND submit_date >= "'.$start .' 00:00:00"';
	}
	if(isset($end) AND !empty($end)){
		$sql .= ' AND submit_date < "'.$end .' 00:00:00"';
	}
	//output results
	//echo $sql;
	global $wpdb;
	$wpdb->flush(); 
	$info = $wpdb->get_results($sql, ARRAY_A);
	if ( count($info) == 0 ) {
		echo "empty";
		exit();
	} else {
		//print_r($info);
		foreach( $info as $indiv ) {
			$indiv['date'];
			$track = $xml_output->addChild('CGIRequest');
			$track->addAttribute('id', $indiv['id']);
			$track->addChild('brand', $indiv['brand']);
			$track->addChild('email', $indiv['email']);
			$track->addChild('lists', $indiv['lists']);
			if($indiv['nameFirst']){
				$track->addChild('nameFirst', $indiv['nameFirst']);
			}
			if($indiv['nameLast']){
				$track->addChild('nameLast', $indiv['nameLast']);
			}
			if($indiv['state']){
				$track->addChild('state', $indiv['state']);
			}
			if($indiv['country']){
				$track->addChild('country', $indiv['country']);
			}
			if($indiv['gender']){
				$track->addChild('gender', $indiv['gender']);
			}
			if($indiv['other']){
				$track->addChild('other', $indiv['other']);
			}
			if($indiv['ip']){
				$track->addChild('ip', $indiv['ip']);
			}
			$track->addChild('submit_date', $indiv['submit_date']);
		}				
	}
	Header('Content-type: text/xml');
	print($xml_output->asXML());
	exit();
}
?>