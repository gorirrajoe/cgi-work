<?php
	/* Template Name: Map My Fitness */
	get_header();
	$editorial_theme_options = get_option( 'cgi_media_editorial_options' );

	require_once(ABSPATH . 'wp-content/includes/mmf/mmf.php');
	$states = array('AK','AL','AR','AZ','CA','CO','CT','DE','FL','GA','HI','IA','ID','IL','IN','KS','KY','LA','MA','MD','ME','MI','MN','MO','MS','MT','NC','ND','NE','NH','NJ','NM','NV','NY','OH','OK','OR','PA','PR','RI','SC','SD','TN','TX','UT','VA','VT','WA','WI','WV','WY');
	$mapmyfitness = new MapMyFitness();

	$route_type = -1;
	$city = '';
	$state = -1;
	$zip = '';
	$num_results = '';
	$page = 1;
	$route = '';
	$route_key = -1;
	$min_distance = '';
	$max_distance = '';
	$min_distance_km = 0;
	$max_distance_km = 0;
	$keywords = '';
	$compare_routes = array();
	if(isset($_GET) && isset($_GET['routePage'])){
		$page = $_GET['routePage'];
		$route_type = $_GET['route_type'];
		$city = $_GET['city'];
		$state = $_GET['state'];
		$zip = $_GET['zip'];
		$num_results = $_GET['num_per_page'];
		$min_distance = $_GET['min_distance'];
		if($_GET['min_distance'] != ''){
			$min_distance_km = floatval ($_GET['min_distance']) * 1.609344;
		}
		$max_distance = $_GET['max_distance'];
		if($_GET['max_distance'] != ''){
			$max_distance_km = floatval ($_GET['max_distance']) * 1.609344;
		}
		$keywords = $_GET['keywords'];
		if($_GET['keywords'] != ""){
			$keywords_string = str_replace(' ', '+', $_GET['keywords']);
		}
		if(isset($_GET) && isset($_GET['route_key'])){
			$route = $_GET['route_key'];
		}
	} else if(isset($_POST)) {
		foreach($_POST as $name => $value){
			if ($route = strstr($name, "map_")) {
				$array = explode("_", $route);
				$route_id = $array[1];
				$compare_routes[] = $route_id;
			}
		}
		if(isset($_POST['routePage'])){
			$page = $_POST['routePage'];
		}
		$route_type = $_POST['route_type'];
		$city = $_POST['city'];
		$state = $_POST['state'];
		$zip = $_POST['zip'];
		$num_results = $_POST['num_results'];
		$min_distance = $_POST['min_distance'];
		if($_POST['min_distance'] != ''){
			$min_distance_km = floatval ($_POST['min_distance']) * 1.609344;
		}
		$max_distance = $_POST['max_distance'];
		if($_POST['max_distance']){
			$max_distance_km = floatval ($_POST['max_distance']) * 1.609344;
		}
		$keywords = $_POST['keywords'];
		if($keywords != ""){
			$keywords_string = str_replace(' ', '+', $_POST['keywords']);
		}
	} else {

	}

	$output = $mapmyfitness->get_route_types();
	echo '<div class="special_category">';
		if($route == '' && count($compare_routes) == 0){  // default screen/form ?>
			<script type="text/javascript">
				function validateForm(){
					var city = document.getElementById("city").value;
					var state = document.getElementById("state").value;
					var zip = document.getElementById("zip").value;
					if((city == "" && state == -1 && zip == "") || (zip == "" && city == "") || (state == -1 && zip == "")){
						alert("You must enter either a city, state combination or a valid zip code.");
						return false;
					}
					return true;
				}
				function checkCompareBoxes(){
					var checkboxes = document.getElementsByTagName("input");
					var count = 0;
					for(var checkbox in checkboxes){
						if(checkbox.indexOf("map") == -1){
							if(checkboxes[checkbox].type == "checkbox" && checkboxes[checkbox].checked == true){
								count++;
							}
						}
					}
					if(count > 3){
						alert("You can only compare up to three routes at one time.");
						return false;
					}
					return true;
				}
			</script>

			<div class="row">
				<div class="category_left_column">
					<div class="blue_bg">
						<img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-runroutes-logo.png">
					</div>
					<div class="category_hdr">
						<h1 class="category_title">Route Finder</h1>
						<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('special-category-sponsor') ) : else : endif; ?>
					</div>
				</div>
				<div class="category_right_column">
					<div class="route_mid_column">
						<form class="find_route_form" action="<?php echo site_url(); ?>/runroutes" method="post" onsubmit="return validateForm()">
							<div class="col_odd route_col_odd">
								<ol>
									<li>
										<select name="route_type">
											<option value='-1'>Route Type</option>
											<?php
												$selected = $editorial_theme_options['mmf_route_types'];
												foreach($output as $result){
													if(count($selected) > 0){
														if(in_array($result->route_type_id, $selected)){
															if($result->route_type_id == $route_type){
																echo '<option selected="selected" value="'.$result->route_type_id .'">'.$result->route_type_name .'</option>';
															} else {
																echo '<option value="'.$result->route_type_id .'">'.$result->route_type_name .'</option>';
															}
														}
													} else {
														if($result->route_type_id == $route_type){
															echo '<option selected="selected" value="'.$result->route_type_id .'">'.$result->route_type_name .'</option>';
														} else {
															echo '<option value="'.$result->route_type_id .'">'.$result->route_type_name .'</option>';
														}
													}
												}
											?>
										</select>
									</li>
									<li>
										<input id="city" placeholder="City" class="route-input" value="<?php echo $city;?>" type="text" name="city">
									</li>
									<li>
										<select name="state" id="state">
											<option value="-1">State</option>
											<?php foreach($states as $state_select){
												if($state == $state_select){
													echo '<option selected="selected" value="'.$state_select.'">'.$state_select.'</option>';
												} else {
													echo '<option value="'.$state_select.'">'.$state_select.'</option>';
												}
											} ?>
										</select>
									</li>
									<li>
										<input id="zip" placeholder="Zip" class="route-input" value="<?php echo $zip;?>" type="text" name="zip">
									</li>
								</ol>
							</div>
							<div class="col_even route_col_even">
								<ol>
									<li>
										<input class="route-input" placeholder="Results Per Page" value="<?php echo $num_results;?>" type="text" name="num_results">
									</li>
									<li>
										<input class="route-input" value="<?php echo $min_distance;?>" type="text" name="min_distance" placeholder="Minimum Distance">
									</li>
									<li>
										<input class="route-input" placeholder="Maximum Distance" value="<?php echo $max_distance;?>" type="text" name="max_distance">
									</li>
									<li>
										<input class="route-input" value="<?php echo $keywords;?>" type="text" name="keywords" placeholder="Keywords">
									</li>
								</ol>
							</div>
							<button class="btn_find_routes" type="submit" name="submit" onclick="return validateForm()">Find Routes</button>
							<span class="tab_indicator"></span>
						</form>
					</div>
					<div class="sidebar sidebar_sans_top_margin">
						<div class="hypnotoad_unit hypnotoad_300x250">
							<?php
								if(!$is_mobile){
									// if (function_exists('dynamic_sidebar') && dynamic_sidebar('atf-300x250') ) : else : endif;
									echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('side-top') : '';
								}
							?>
						</div><!-- ad unit -->
						<div class="hypnotoad_unit hypnotoad_300x600">
							<?php
								if(!$is_mobile){
									// if (function_exists('dynamic_sidebar') && dynamic_sidebar('desktop-300x600') ) : else : endif;
									echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('side-middle') : '';
								}
							?>
						</div><!-- ad unit -->
					</div>
				</div>
			</div><!-- first group -->

		<?php }
		if($route != '' || count($compare_routes) > 0) {  // individual route
			echo '<div class="left_column">';
				$link = '/runroutes?routePage='.$page.'&num_per_page='.$num_results.'&route_type='.$route_type.'&city='.$city.'&state='.$state.'&zip='.$zip.'&min_distance='.$min_distance.'&max_distance='.$max_distance.'&keywords='.$keywords;
				echo '
					<div class="row archive_navi">
						<div class="btn_primary">
							<a class="prev-post" href="'. $link .'"><span class="icon-left-open"></span> Back to Results</a>
						</div>
					</div>
				';
				echo '<h1><img class="slashes slash_blue" src="'.get_bloginfo('stylesheet_directory').'/images/running/running-slashes-blue.svg"> Route Finder</h1>';
				if($route != ''){
					$compare_routes[] = $route;
					$route_id = $_GET['route_id'];
				} else {
					$route_id = '';
				}
				$count = 0;

				echo '<div class="row route_row">';
				foreach($compare_routes as $name => $route_key_value){
					$mapmyfitness->get_route_information($route_key_value, $route_id);
					$route_info = $mapmyfitness->getRouteInfo();
					$color = 'red';
					$col_count = ' col_1';
					if($count == 1){
						$color = 'blue';
						$col_count = ' col_2';
					} else if($count == 2) {
						$color = 'green';
						$col_count = ' col_3';
					}
					echo '
						<div class="three_col'.$col_count.'">
							<h2 style="color:'.$color.'">'.$route_info->route_name .'</h2>
					';
					if(count($compare_routes) > 1){
						$link = '/runroutes?routePage='.$page.'&num_per_page='.$num_results.'&route_type='.$route_type.'&city='.$city.'&state='.$state.'&zip='.$zip.'&min_distance='.$min_distance.'&max_distance='.$max_distance.'&keywords='.$keywords.'&route_key='.$route_key_value;
						echo '<a href="'.$link.'">Go to Individual Result Page</a><br />';
					}
					// get the location from the first point on the route instead of the user's information
					$mapmyfitness->get_route_start($route_info->points[0]->lat, $route_info->points[0]->lng);
					$location_info = $mapmyfitness->getLocationInfo();
					echo '
						<p><strong>Route Location: </strong>' . $location_info->geocoded_address->LocalityName .', ' .$location_info->geocoded_address->AdministrativeAreaCode .' '.$location_info->geocoded_address->CountryName.'<br />
					';

					echo '<strong>Route Distance: </strong>';
						// distance is in meters, should convert to miles
						$distance = 0.000621371192 * $route_info->distance;
						echo '<span>'.round ($distance, 2) .' miles</span><br />';

					if($route_info->route_description != ""){
						echo '<strong>Route Description: </strong>';
						echo $route_info->route_description .'<br />';
					}
					if($route_info->tags != ""){
						echo '<strong>Route Tags: </strong>';
						echo $route_info->tags;
					}
					echo '</p>';
					echo '<p><strong>Route Elevation</strong></p>';
					$image_src = 'http://api.mapmyfitness.com/3.1/routes/view_route_elevation?&r=&route_key='.$route_key_value .'&route_id=&pt=&width=700&height=300&graph_color=003798&graph_shadow_color=003333&graph_border_color=FFFFFF&graph_background_color=FFFFCC&'.'background_color=FFFFFF&schema=mapmyrun.com&units=imperial&scale_y_start=&scale_y_end=&grade=none&foreground_levels=10&shadow_levels=10&print_override=none&'.'oversize_override=none&csv_flag=&json_flag=&json=&rotate=&get_profile=&compression_factor=1';
					echo '<a href="'.$image_src.'" class="fancyroute"><img src="'.$image_src.'"></a>';

					echo '</div><!-- route col-->';
					$count++;
				}
				echo '</div><!-- row -->';
				$secret = CUSTOMER_SECRET;
				$timestamp = time();
				$signed_token = $timestamp . "*" . sha1($timestamp . "|" . $secret);
				if(count($compare_routes) == 2){
					echo '<span style="color:red">The first route is the red line</span> and <span style="color:blue">the second route is the blue line</span>.';
				} else if(count($compare_routes) == 3) {
					echo '<span style="color:red">The first route is the red line</span>, <span style="color:blue">the second route is the blue line</span>, and <span style="color:green">the third route is the green line</span>.';
				}
				echo '<script language="javascript" type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>';
				echo '<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>';
				echo'
					<script type="text/javascript">
						jQuery(document).ready(function($){
							$(".b_opacity").click(function(){
								//this will find all the images in the map canvas container. Normally you would want to target specific images by the value of src
								$("#map_canvas").find("img").css("opacity","0.4")
						})
						var myOptions = {
							zoom: 11,
							zoomControl: true,
							zoomControlOptions: {
								style: google.maps.ZoomControlStyle.LARGE
							},
							mapTypeId: google.maps.MapTypeId.ROADMAP
						}
						var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
				';
				$count = 0;
				foreach($compare_routes as $name=>$route_key_value){
					if($count == 0){
						$line_color = '7f0000ff';
					} else if($count == 1) {
						$line_color = '7fff0000';
					} else if($count == 2) {
						$line_color = '7f00ff00';
					}
					// if more than one route then don't show the mile markers, only show them when one route
					if(count($compare_routes) > 1){
						$param_string = 'display_distance_marker_flag=0&distance_marker_interval=1609.344&display_custom_marker_flag=1&line_color='.$line_color.'&site=consumer_key=' . CUSTOMER_KEY .'&signed_token='.$signed_token .'&route_key='.$route_key_value.'&signed_token_limit=1';
					} else {
						$param_string = 'display_distance_marker_flag=1&distance_marker_interval=1609.344&display_custom_marker_flag=1&line_color='.$line_color.'&site=consumer_key=' . CUSTOMER_KEY .'&signed_token='.$signed_token .'&route_key='.$route_key_value.'&signed_token_limit=1';
					}
					$google_maps_url = 'http://api.mapmyfitness.com/3.1/routes/get_route_kml?'.$param_string;
					echo 'var ctaLayer_'.$route_key_value.' = new google.maps.KmlLayer("'.$google_maps_url .'");
						ctaLayer_'.$route_key_value.'.setMap(map);';
					$count++;
				}
				echo '
					});
				</script>
				<style>
					#map_canvas {
						height:600px;
						margin-bottom:1.875em;
						width:100%;
					}
					#map_canvas img {
						max-width:none;
					}
				</style>
				<div id="map_canvas" style="position:relative; overflow:hidden;"></div>';
				/*
				$count = 0;
				foreach($compare_routes as $name=>$route_key_value){
					$color = 'red';
					if($count == 1){
						$color = 'blue';
					} else if($count == 2) {
						$color = 'green';
					}
					echo '<h2 style="color:'.$color.'">Route Elevation</h2>';
					$image_src = 'http://api.mapmyfitness.com/3.1/routes/view_route_elevation?&r=&route_key='.$route_key_value .'&route_id=&pt=&width=700&height=300&graph_color=003798&graph_shadow_color=003333&graph_border_color=FFFFFF&graph_background_color=FFFFCC&'.'background_color=FFFFFF&schema=mapmyrun.com&units=imperial&scale_y_start=&scale_y_end=&grade=none&foreground_levels=10&shadow_levels=10&print_override=none&'.'oversize_override=none&csv_flag=&json_flag=&json=&rotate=&get_profile=&compression_factor=1';
					echo '<img src="'.$image_src.'">';
					$count++;
				}
				*/
			echo '</div>';
			get_sidebar();
		} else {  // results
			echo '
				<div class="row">
					<h3 class="results_hdr"><img class="slashes slash_white_lg" src="'.get_bloginfo('stylesheet_directory').'/images/running/running-slashes-white-lg.svg"> Route Results - ';
					// figure out what the new search criteria are
					$param_string = 'o=json&consumer_key='.CUSTOMER_KEY;
					if($route_type != -1 && $route_type != ''){
						$param_string .= '&route_type='.$route_type;
					}
					if(trim($city) != ""){
						$city_spaces = str_replace(' ', '+', $city);
						$param_string .='&city='.$city_spaces;
					}
					if($state != -1 && $state != ''){
						$param_string .='&state='.$state;
					}
					if($zip != ""){
						$param_string .='&zip='.$zip;
					}
					if($num_results != ""){
						$param_string .='&result_limit='.$num_results;
					}
					if($page > 1){
						$start_record = (intval($page) - 1) * intval($num_results);
						$param_string .= '&result_offset='.$start_record;
					}
					if($min_distance_km != 0){
						$param_string .= '&min_distance='.$min_distance_km;
					}
					if($max_distance_km != 0){
						$param_string .= '&max_distance='.$max_distance_km;
					}
					if($keywords_string != ''){
						$param_string .= '&keywords='.$keywords_string;
					}
					if($city == '' && $state == '' && $zip == ''){
						$mapmyfitness->searchRoutes($param_string.'&route_type=1', true);
					}
					else{
						$mapmyfitness->searchRoutes($param_string, false);
					}
					$search_routes = $mapmyfitness->getSearchRoutesInfo();
					//decide size count returned might be higher then actual array count returned
					$numRoutesReturned = count($search_routes->routes);
					if($search_routes->count > $numRoutesReturned){
						$useSize = $numRoutesReturned;
					} elseif($search_routes->count <= $numRoutesReturned){
						$useSize = $search_routes->count;
					}

					$total_results = $search_routes->count;
					if($total_results > $numRoutesReturned){
						$num_per_page = 24;
						if($num_results != ""){
							$num_per_page = $num_results;
						}
						$total_results_commas = number_format($total_results);
						echo '<strong>Found: '. $total_results_commas .'</strong></h3>';
						$total_pages = ceil($total_results/$num_per_page);
						$start_page = 1;
						$end_page = $total_pages + 1;
						if($total_pages > 5 ){
							if($page > 3){
								if($total_pages == $page){
									$start_page = $total_pages - 4;
								} else if($total_pages - $page < 2){
									$start_page = $total_pages - 5 + ($total_pages - $page);
								} else{
									$start_page = $page - 2;
								}
							}
						}
						if($total_pages > 5){
							if($start_page + 4 < $total_pages){
								$end_page = $start_page + 5;
							}
						}
						echo '<div class="row"><ul class="route_page_numbers">';
							for($i = $start_page; $i < $end_page; $i++){
								if($i == $page){
									echo '<li class="navi_current_page">Page '.$i.'</li>';
								} else {
									$link = '/runroutes?routePage='.$i.'&num_per_page='.$num_per_page.'&route_type='.$route_type.'&city='.$city.'&state='.$state.'&zip='.$zip.'&min_distance='.$min_distance.'&max_distance='.$max_distance.'&keywords='.$keywords;
									echo '<li><a href="'. $link .'">Page '.$i.'</a></li>';
								}
							}
						echo '</ul></div>';
					}

					echo '
						<div class="archive">
							<form onsubmit="return checkCompareBoxes()" action="'.site_url().'/runroutes" method="post">
								<div class="compare_btn_div">
									<button type="submit" class="compare-routes-button">Compare Up To 3 Routes</button>
								</div>
									<ul>
					';
					if(count($search_routes->routes) > 0){
						foreach($search_routes->routes as $search_route){
							$mapmyfitness->setRouteImageShowElevationSpark('0');
							$link = '/runroutes?route='. $search_route->route_id .'&route_key='.$search_route->route_key.'&routePage='.$page.'&num_per_page='.$num_per_page.'&route_type='.$route_type.'&city='.$city.'&state='.$state.'&zip='.$zip.'&min_distance='.$min_distance.'&max_distance='.$max_distance.'&keywords='.$keywords;

							echo '<li class="route_post">
								<div class="route_compare"><label for="map_'.$search_route->route_key.'">Compare Route</label> <input id="map_'.$search_route->route_key.'" type="checkbox" name="map_'.$search_route->route_key.'"></div>
								<a href="'.$link.'"><img src="'.$mapmyfitness->getRouteImage($search_route->route_id,$search_route->route_key,'480','200').'"></a>
								<div class="route_info">
									<h2 class="route_name"><a href="'.$link.'">'.number_format($search_route->total_distance, 2, '.', ',').' mi</a></h2>
									<ul>
										<li>'.ucwords($search_route->city).'<br>
										<strong>'.ucwords($search_route->state).' '.strtoupper($search_route->country).'</strong></li>
									</ul>
								</div>
							</li>';
						}
					} else {
						echo '<li>No results found.</li>';
					}
					// also have hidden inputs for the rest of the route search so we can have a back button to the search results
					echo '</ul>
						<input type="hidden" value="'.$page.'" name="routePage"/>
						<input type="hidden" value="'.$num_per_page.'" name="num_per_page"/>
						<input type="hidden" value="'.$route_type.'" name="route_type"/>
						<input type="hidden" value="'.$city.'" name="city"/>
						<input type="hidden" value="'.$zip.'" name="zip"/>
						<input type="hidden" value="'.$state.'" name="state"/>
						<input type="hidden" value="'.$min_distance.'" name="min_distance"/>
						<input type="hidden" value="'.$max_distance.'" name="max_distance"/>
						<input type="hidden" value="'.$keywords.'" name="keywords"/>
						<div class="compare_btn_div">
							<button type="submit" class="compare-routes-button">Compare Up To 3 Routes</button>
						</div>
						</form></div>';
					echo '</div>';
		} ?>
	</div>
<?php get_footer(); ?>
