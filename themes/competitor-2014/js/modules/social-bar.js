/**
 * Logic for displaying the "sticky" social sharing icons on the site.
 */

jQuery(document).ready(function($) {
	var $header				= $('.navbar'),
		$adminBar			= $('#wpadminbar'),
		$articleLeft		= $('#article-left'),
		$articleRight		= $('#article-right'),
		$topBar				= $('#share-links'),
		$leftBar			= $('#share-links-left'),
		$onePageTemplate	= $('.page-template-one-column-php'),
		guide				= false,
		topBoundry			= 0,
		bottomBoundry		= 0,
		positionTop			= 0,
		positionBottom		= 0,
		windowWidth			= 0,
		adminBarSticky		= false,
		activeWidget		= '',
		fixed				= false,
		fixedBottom			= false,
		lastHeight			= 0,
		firstCount			= 0;

	/**
	 * Init
	 *
	 * Constructor method which calls all the utility methods used to
	 * calculate the relevant element dimensions.
	 */
	function init() {

		windowWidth = ( $(window).width() + calcScrollbarWidth() );

		guide = $('#rgg').hasClass('rgg_container');

		if( guide ) {

			activeWidget = 'sm-desktop';

			generateWidgetStyles();

		} else {

			if( windowWidth <= 989 ) {

				/**
				 * size: 320px - 989px
				 * up until header doesn't become sticky
				 */
				activeWidget = 'mobile';

				generateWidgetStyles();

			} else if( windowWidth <= 1299) {

				/**
				 * size: 990px - 1299px
				 * this is the point where content area expands
				 */

				activeWidget = 'sm-desktop';

				generateWidgetStyles();

			} else {

				/**
				 * size: 1300px - up
				 * switch to side sticky share
				 */

				activeWidget = 'desktop';

				$articleLeft.css( 'height', $articleRight.height() );

			}

			if( windowWidth >= 601 && $adminBar.length ) {

				adminBarSticky = true;

			}

		}

		topBoundry      = calcTopBoundry();
		bottomBoundry   = calcBottomBoundry();
		positionTop     = calcPositionTop();
		// positionBottom = calcPositionBottom();

		firstCount = $('.navbar div').length;
	}

	/**
	 * Calculate Scrollbar Width
	 *
	 * Calculate the scrollbar width (if exists).
	 *
	 * @return int Width of scrollbars
	 */
	function calcScrollbarWidth() {
		var inner, outer, w1, w2;

		inner = document.createElement('p');
			inner.style.width   = "100%";
			inner.style.height  = "200px";

		outer = document.createElement('div');
			outer.style.position    = "absolute";
			outer.style.top         = "0px";
			outer.style.left        = "0px";
			outer.style.visibility  = "hidden";
			outer.style.width       = "200px";
			outer.style.height      = "150px";
			outer.style.overflow    = "hidden";
			outer.appendChild(inner);

		document.body.appendChild(outer);

		w1 = inner.offsetWidth;
		outer.style.overflow = 'scroll';

		w2 = inner.offsetWidth;

		if(w1 == w2) {
			w2 = outer.clientWidth;
		}

		document.body.removeChild(outer);

		return (w1 - w2);
	}

	/**
	 * Calculate Top Boundry
	 *
	 * "topBoundry" defines the point at which the social widget should
	 * become "fixed" when scrolling down, or "relative" when scrolling up.
	 *
	 * Note: This is the highest we want the widget to go on the page.
	 *
	 * @return int Position of the "top" scroll boundry.
	 */
	function calcTopBoundry() {

		if( 'mobile' === activeWidget ) {

			if( windowWidth < 601 && $adminBar.length ) {

				return $articleRight.offset().top - 10;

			} else if( adminBarSticky == true ) {

				return $articleRight.offset().top - $adminBar.height() - 10;

			} else {

				return $articleRight.offset().top - $header.height() - 10;

			}


		} else if( 'sm-desktop' === activeWidget ) {

			if( adminBarSticky == true ) {

				return $articleRight.offset().top - $adminBar.height() - 15;

			} else {

				return $articleRight.offset().top - 15;

			}

		} else if( 'desktop' === activeWidget ) {

			if( adminBarSticky == true ) {

				return $articleRight.offset().top - $adminBar.height() - 15;

			} else {

				return $articleRight.offset().top - 15;

			}

		}

	}

	/**
	 * Calculate Bottom Boundry
	 *
	 * "bottomBoundry" defines the point at which the social widget
	 * should become "absolute" when scrolling down, or "fixed" when
	 * scrolling up.
	 *
	 * Note: This is the lowest we want the widget to go on the page.
	 *
	 * @return int Position of the "bottom" scroll boundry.
	 */
	function calcBottomBoundry() {

		if('desktop' === activeWidget ) {

			if( adminBarSticky == true ) {

				if( $onePageTemplate.length ) {

					return $articleRight.height() + $articleRight.offset().top - ( $adminBar.height() + 165 );

				} else {

					return $articleRight.height() + $articleRight.offset().top - ( $adminBar.height() + $leftBar.height() + 15 );

				}


			} else {

				return $articleRight.height() + $articleRight.offset().top - ( $leftBar.height() + 15 );

			}

		}

	}

	/**
	 * Calculate Position Top
	 *
	 * "positionTop" defines the y-coordinate at which the social widget
	 * should remained fixed at while the widget is "fixed".
	 *
	 * @return int Position of the widget while "fixed".
	 */
	function calcPositionTop() {
		if( activeWidget === 'desktop' ) {
			if( $adminBar.length ) {
				return $adminBar.height() + 15;
			} else {
				return 15;
			}
		} else if( activeWidget === 'sm-desktop' ) {
			if( $adminBar.length ) {
				return $adminBar.height() + 15;
			} else {
				return 0;
			}
		} else {
			if( $adminBar.length ) {
				return $header.outerHeight() + $adminBar.height() + 15;
			} else {
				return $header.outerHeight() + 15;
			}

		}
	}

	/**
	 * Calculate Position Bottom
	 *
	 * "positionBottom" defines the lowest y-coordinate at which the
	 * social widget should remained fixed so that it doesn't escape
	 * it's container.
	 *
	 * @return int Position of the widget while "absolute".
	 */
	// function calcPositionBottom() {
	//  // return ($articleRight.height() - $leftBar.height());
	//  console.log( $articleRight.outerHeight(),  $articleRight.offset().top - $(document).scrollTop());

	//  return $articleRight.height() + ( $articleRight.offset().top - $(document).scrollTop() ) - 15;

	// }

	/**
	 * Generate Mobile Widget Styles
	 *
	 * Adds dynamically generated styles to the HEAD.
	 */
	function generateWidgetStyles() {
		var style = '';

		// style += '@media screen and (min-width:768px) {';
			style += '.sub_footer {';
				style += 'padding-bottom: '+ ($topBar.outerHeight() + 20) + 'px;';
			style += '}';
		// style += '}';

		if($('#competitor-sb-css').length < 1) {

			$('<style id="competitor-sb-css">'+ style +'</style>').appendTo('head');

		} else {

			$('#competitor-sb-css').html(style);

		}

	}

	/**
	 * Position Mobile Widget
	 *
	 * Controls the positioning of the social widget for screens that
	 * are <= 991px wide.
	 *
	 * @param int position Current position of the scrollbar
	 */
	function positionMobileWidget(position) {

		if(true === fixed) {

			// top-boundry logic
			if( position <= topBoundry) {

				$topBar.removeClass('social-bar--fixed');
				$articleRight.removeClass('content--fixed');

				$topBar.css({
					'position': '',
					'left': '',
					// 'top': '',
					'bottom': '',
					'width': ''
				});
				fixed = false;

			}

		} else {

			// top-boundry logic
			if(position >= topBoundry ) {

				$topBar.addClass('social-bar--fixed');
				$articleRight.addClass('content--fixed');

				$topBar.css({
					'position': 'fixed',
					'left': 0,
					// 'top': $header.height(),
					'bottom': 0,
					'width': '100%'
				});
				fixed = true;

			}
		}

	}


	function positionSmDesktopWidget(position) {

		if(true === fixed) {

			// top-boundry logic
			if( position <= topBoundry) {

				$topBar.removeClass('social-bar--fixed');
				$articleRight.removeClass('content--fixed');

				$topBar.css({
					'position': '',
					'left': '',
					// 'top': '',
					'bottom': '',
					'width': ''
				});
				fixed = false;

			}

		} else {

			// top-boundry logic
			if(position >= topBoundry ) {

				$topBar.addClass('social-bar--fixed');
				$articleRight.addClass('content--fixed');

				$topBar.css({
					'position': 'fixed',
					'left': 0,
					// 'top': positionTop,
					'bottom': 0,
					'width': '100%'
				});
				fixed = true;

			}
		}

	}

	/**
	 * Position Desktop Widget
	 *
	 * Controls the positioning of the social widget for screens that
	 * are >= 992px wide.
	 *
	 * @param int position Current position of the scrollbar
	 */
	function positionDesktopWidget(position) {

		if(true === fixed) {

			// bottom boundry logic
			if(false === fixedBottom && position >= bottomBoundry) {

				$leftBar.css({
					'bottom': 0,
					'position': 'absolute',
					'top': ''
				});

				fixedBottom = true;

			} else if(true === fixedBottom && position <= bottomBoundry) {

				$leftBar.css({
					'position': 'fixed',
					'top': positionTop,
					'bottom': ''
				});

				fixedBottom = false;

			}

			// top-boundry logic
			if( position <= topBoundry ) {

				$leftBar.removeClass('social-bar-left--fixed');
				$articleRight.removeClass('content-left--fixed');
				$topBar.removeClass('social-bar--fixed');
				$articleRight.removeClass('content--fixed');

				$leftBar.css({
					'position': 'relative',
					'top': '0'
				});

				fixed = false;

			}

		} else {

			// top-boundry logic
			if( position >= topBoundry ) {

				$leftBar.addClass('social-bar-left--fixed');
				$articleRight.addClass('content-left--fixed');
				// $topBar.addClass('social-bar--fixed');
				// $articleRight.addClass('content--fixed');

				$leftBar.css({
					'position': 'fixed',
					'top': positionTop
				});

				fixed = true;

			}

		}

	}


	function checkForChanges( lastCount ) {

		if( firstCount != lastCount ) {

			firstCount = lastCount;
			init();

		}

	}


	/**
	 * Note: wait until "load" event so element heights are set.
	 */
	$(window).on('load', function() {

		if ( $topBar.length > 0 && $leftBar.length > 0 ) {

			init();

			// bind window events logic
			$(window).scroll(function() {

				if( 'desktop' === activeWidget) {

					positionDesktopWidget( $(this).scrollTop() );

				} else if( 'sm-desktop' === activeWidget ) {

					positionSmDesktopWidget( $(this).scrollTop() );

				} else {

					positionMobileWidget( $(this).scrollTop() );

				}

				lastCount = $('.navbar div').length;
				checkForChanges( lastCount );

			}).resize(function() {

				var newWidth = ( $( window ).width() + calcScrollbarWidth() );

				// don't call init unless screen width has changed
				if( 0 === windowWidth || newWidth !== windowWidth ) {

					init();

				}

			});

		}

	});

});
