var validateform = function( formid ) {
    console.log('hello world');
    var formid;
    var formid = 'form#' + formid;
    var error = new Boolean(false);
    var errmsg = '';
    var emailField = jQuery(formid + ' .fields_email');
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    /* Validation */

    //Test email textbox
    if ( !emailField.val() || regex.test( emailField.val() ) == false ) {
        error = true;
        errmsg = 'Please enter a valid email address.';
    }
    if ( error == false ) {
        jQuery(formid + ' .errorMessage').hide();
        jQuery(formid + ' .fields_email').hide();
        jQuery(formid + ' .submit-button').hide();
        jQuery(formid + ' .nl-options').hide();
        jQuery(formid + ' .please_wait').show();

        /* Ajax submission code here */
        //Submit form
        jQuery.post(
            window.location.origin + '/wp-content/includes/newsletter-subscribe/nlsubscribe.php',
            jQuery(formid).serialize() + '&ajax=true',
            function(data) {
                jQuery(formid + ' .please_wait').hide();
                jQuery(formid + ' .confirmMessage').show().html(data);
            }
        );
        // If we are in an iframe, set a timer to close that damn thing
        if ( window.self !== window.top ) {
            setTimeout(
                function() { parent.$.fancybox.close(); },
                5000
            );
        };
    } else {
        jQuery(formid + ' .errorMessage').show();
        jQuery(formid + ' .errorMessage').html(errmsg);
    }

    error = false;
    return false;

}; /* end validate() function */
