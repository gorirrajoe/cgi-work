jQuery(document).ready(function($) {

	var $submenuMobileLabel = $('.gg-submenu__label'),
		$submenu = $('.gg-submenu');

	if( $submenuMobileLabel.length ) {

		$submenuMobileLabel.on( 'click', function() {
			$submenu.slideToggle();
		});

	}

	$('.gg-submenu .menu-item-has-children > a').click( function( e ) {

		e.preventDefault();

		var $link = $( this ).attr( 'href' ),
			$text = $( this ).text();
			// $dupe = $( this ).next().find( '.menu-item-dupe' );

		// if( $dupe.length == 0 ) {
		// 	$( this ).next().prepend( '<li class="menu-item-dupe"><a href="'+ $link +'">'+ $text +'</a></li>' );
		// }

		$( this ).parentsUntil( 'ul' ).siblings().children().next( '.sub-menu' ).slideUp();
		$( this ).next().slideToggle();

	});


});


$( window ).click( function( e ) {

	var $container = $( '.gg-submenu' ),
		$toggle = $( '.gg-submenu .menu-item-has-children a' );

	if( !$container.is( e.target ) && $container.has( e.target ).length === 0 ) {
		$toggle.siblings( '.sub-menu' ).slideUp();
	}

});

