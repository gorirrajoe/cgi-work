function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=600,width=800,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes')
}


(function($) {
	$(document).ready(function() {
		$.slidebars();
		$.fancybox.defaults.fullScreen = false;
	});
})(jQuery);


jQuery(document).ready(function() {
	jQuery('.tab_content').hide();
	jQuery('ul.tabs li:first').addClass('active').show();
	jQuery('.tab_content:first').show();

	jQuery('ul.tabs li').click(function() {

		jQuery('ul.tabs li').removeClass('active');
		jQuery(this).addClass('active');
		jQuery('.tab_content').hide();

		var activeTab = jQuery(this).find('a').attr('href');
		jQuery(activeTab).show();

		return false;
	});

});


jQuery(document).ready(function() {
	jQuery('.tab_content_2').hide();
	jQuery('ul.tabs_2 li:first').addClass('active').show();
	jQuery('.tab_content_2:first').show();

	jQuery('ul.tabs_2 li').click(function() {

		jQuery('ul.tabs_2 li').removeClass('active');
		jQuery(this).addClass('active');
		jQuery('.tab_content_2').hide();

		var activeTab = jQuery(this).find('a').attr('href');
		jQuery(activeTab).fadeIn();

		return false;
	});

});


jQuery(function($) {
	$( '.mag_nl_rwd' ).accordion({
		collapsible: true,
		active: false,
		heightStyle: 'content'
	});
});


jQuery(document).ready(function($) {
	$('#mostrecent_carousel').owlCarousel({
		items:1,
		singleItem:true,
		autoPlay:false,
		navigation:true,
		autoHeight:true,
		navigationText: false,
		lazyLoad:true
	});
	$('#offers_carousel').owlCarousel({
		items:4,
		itemsDesktopSmall:[989,3],
		autoPlay:true,
		stopOnHover:true,
		navigation:true,
		navigationText: false
	});
	$('#sidebar_offers_carousel').owlCarousel({
		items:4,
		itemsCustom:[[0,1],[480,2],[640,3],[960,1]],
		autoPlay:true,
		stopOnHover:true,
		navigation:true,
		navigationText: false,
		lazyLoad:true
	});
	$('.recommended_carousel').owlCarousel({
		items:1,
		singleItem:true,
		autoPlay:false,
		navigation:true,
		navigationText: false
	});
	$('#video_carousel').owlCarousel({
		items:1,
		singleItem:true,
		autoPlay:false,
		navigation:true,
		navigationText: false,
		lazyLoad:true
	});
	$('#photo_carousel').owlCarousel({
		items:1,
		singleItem:true,
		autoPlay:false,
		navigation:true,
		navigationText: false,
		lazyLoad:true
	});
	$('.list_carousel').owlCarousel({
		items:1,
		singleItem:true,
		autoPlay:false,
		navigation:true,
		navigationText: false
	});
});


jQuery(function($){
	var $searchlink = $('#searchtoggle');
	var $searchbar  = $('#searchbar');

	$('.right_nav_btns div').on('click', function(e){
		// e.preventDefault();

		if ( $(this).attr('id') == 'searchtoggle' ) {
			if ( !$searchbar.is(':visible') ) {
				// if invisible we switch the icon to appear collapsable
				$searchlink.removeClass('fa-search').addClass('fa-search-minus');
			} else {
				// if visible we switch the icon to appear as a toggle
				$searchlink.removeClass('fa-search-minus').addClass('fa-search');
			}
			$searchbar.slideToggle( 300, function() {
				// callback after search bar animation
			});
		}
	});
});


function toggle_visibility( id ) {
	var e = document.getElementById(id);
	if(e.style.display == 'block')
		e.style.display = 'none';
	else
		e.style.display = 'block';
}


var toggleselect = function( location ) {
	var location;
	var checkBoxes = jQuery("#icontact_" + location + " input:not(#comp_best_" + location +")");
	checkBoxes.prop("checked", !checkBoxes.prop("checked"));
};


// for tooltip
var $tooltips = $( '.tooltip' );
$tooltips.on( 'click', function( e ) {
	$( '.tooltip_txt' ).hide();
	var $id = this.id + '_tooltip';
	$( '#' + $id ).show();

	return false;
	// console.log($id);
} );


$( document ).click( function() {
	$( '.tooltip_txt' ).hide();
	// console.log( 'hide all tooltips' );
});


$(function() {
	$( "img.lazy" ).lazyload({
		effect : "fadeIn"
	});
});


// Accordion functionality ( used on reg page, etc. )
function close_accordion_section() {
	$( '.accordion .accordion-section-title' ).removeClass( 'active' );
	$( '.accordion .accordion-section-content' ).slideUp( 300 ).removeClass( 'open' );
}

$( '.accordion-section-title' ).click( function( e ) {
	// Grab current anchor value
	var currentAttrValue = $( this ).data( 'reveal' );

	if( $( e.target ).is( '.active' ) ) {
		close_accordion_section();
	} else {
		close_accordion_section();

		// Add active class to section title
		$( this ).addClass( 'active' );
		// Open up the hidden content panel
		$( '.accordion ' + currentAttrValue ).slideDown( 300 ).addClass( 'open' );
	}

	e.preventDefault();
} );