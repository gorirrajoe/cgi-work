// by Steven Ferrino
// steven@327media.com

var FBObject = null;

if (!encodedCanonical) {
    var encodedCanonical = encodeURIComponent(document.location.href);
}

var nonEncoded = decodeURIComponent(encodedCanonical);


var shareBarContent ="<div id='UISideShareBar' class='UISide-sharebar'>" +
    "<div class='UIShareBar-container'>" +
    "<div class='UIShareBar-twitter'><a href='http:\/\/twitter.com\/share' class='twitter-share-button' data-count='horizontal' data-via='" + siteTwitterHandle + "' data-url='" + nonEncoded + "' data-size='small'><\/a><\/div>" +
    "<div class='UIShareBar-fb'><div class='fb-like' data-send='false' data-layout='button_count' data-width='42' data-show-faces='false' data-href='" + unEncodedCanonical + "'><\/div><\/div>" +
    "<div class='UIShareBar-gplus'><g:plusone class='g-plusone' callback='plusoneCallback' size='medium' href='" + unEncodedCanonical + "'><\/g:plusone><\/div>" +
    "<\/div>" +
    "<\/div>";

jQuery("#UIShareBarContainer").html( shareBarContent );

function plusoneCallback(plusoneObj)
{
   if (plusoneObj && plusoneObj.state && plusoneObj.state == "off") {
      _gaq.push(['_trackEvent', 'Share', 'Plus1-off', canonicalPage ]);
   }
   else {
      _gaq.push(['_trackEvent', 'Share', 'Plus1', canonicalPage ]);
   }
}

var inScrollMode = false;
jQuery(window).scroll(function() {

    if ( jQuery(window).scrollTop() > 305)  {
        jQuery("#UISideShareBar").css("position", "fixed");
        jQuery("#UISideShareBar").css("top", "20px");
   } else {
        jQuery("#UISideShareBar").css("position", "absolute");
        jQuery("#UISideShareBar").css("top", "270px");
   }

});


// Share Items
// Plus 1
var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
po.src = 'https://apis.google.com/js/plusone.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);

// facebook
if ( typeof FB == "undefined" ) { //} && typeof FB.init == "undefined" ) {
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id; js.async = true;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
}


// twitter
(function(d,s,id){
    var js,fjs=d.getElementsByTagName(s)[0];
    if(!d.getElementById(id)){
        js=d.createElement(s);
        js.id=id;
        js.src="//platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js,fjs);
    }
}(document,"script","twitter-wjs"));
