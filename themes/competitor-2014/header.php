<!DOCTYPE html>
<?php
	$bloginfo	= get_bloginfo( 'name' );
	$name		= str_replace('.com', '', $bloginfo);
	$name		= strtolower($name);

	session_start();
	$_SESSION['user_start']	= time();
	$name_tracking			= 'running';
	$fbshare				= ( isset($post) && get_post_meta( $post->ID, '_fb_share_title', true ) ) ? get_post_meta( $post->ID, '_fb_share_title', true) : get_the_title();
	$time_sensitive			= ( isset($post) && get_post_meta( $post->ID, '_fb_share_time_sensitive', true ) =='on' ) ? true : false;
?><html lang="en" xmlns:<?php echo $name; ?>="<?php echo site_url();?>/ns#">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="FacebookShareMessage" content="<?php echo $fbshare; ?>">
	<?php
		if ( $time_sensitive ) {
			echo '<meta name="TimeSensitive" content="1">' ."\n";
		}

		// Load options and display accordingly
		$technical_theme_options	= get_option( 'cgi_media_technical_options' );
		$editorial_theme_options	= get_option( 'cgi_media_editorial_options' );

		$facebook_appid		= isset( $technical_theme_options['facebook_appid']) ? $technical_theme_options['facebook_appid'] : '';
		$itunes_appid		= isset( $technical_theme_options['itunes_appid']) ? $technical_theme_options['itunes_appid'] : '';
		$bing_webmaster_vid	= isset( $technical_theme_options['bing_webmaster_vid']) ? $technical_theme_options['bing_webmaster_vid'] : '';

		if ( !defined('WPSEO_VERSION') ) { // Disable hard-coded SEO items if Yoast is Active

			echo meta_keywords();
			echo meta_description();

			if ( $facebook_appid != '' && isset( $technical_theme_options['facebook_comments'] ) ) {
				echo '<meta property="fb:app_id" content="' . $facebook_appid . '"/>';
			}
			/* add 2015-03-19 SK */
			if ( $bing_webmaster_vid != '' && ( is_home() || is_front_page() ) ) {
				echo '<meta name="msvalidate.01" content="' . $bing_webmaster_vid . '"/>';
			}

			/* modified 6.20.13 - sferrino */
			echo wp_open_graph_protocol();

			if ( in_category('video') ) {
				echo wp_open_graph_protocol();
			}

		} // End if !defined(WPSEO_VERSION)

		/* add 2013-12-13 SK */
		if ( $itunes_appid != '' && ( is_home() || is_front_page() ) ) {
			echo '<meta name="apple-itunes-app" content="app-id=' . $itunes_appid . '"/>';
		}

		$apple_touch_icon = ($editorial_theme_options['apple_touch_icon']);
		if ( $apple_touch_icon != '' ) {
			echo '<link rel="apple-touch-icon" type="image/png" href="' . $apple_touch_icon . '" />';
		}

		$shortcut_icon = ($editorial_theme_options['shortcut_icon']);
		if ( $shortcut_icon != '' ) {
			echo '<link rel="shortcut icon" href="' . $shortcut_icon . '" />';
		}

		// we're not sure what this global is for
		global $all;
		if(isset($_GET['all'])) {
			$all = 1;
		} else {
			$all = 0;
		}

		/* sferrino */
		if ( $name != 'competitor' && $name != 'competitor running' && $name != 'velonews' && $name != 'triathlon' ) {
			echo page_title();
		} else {
			echo "<title>";
			wp_title( '' );
			echo "</title>";
		}

		// Mobile Test
		$is_mobile	= is_mobile();
		$is_tablet	= is_tablet2();

		wp_head();

		// Determine if we are in photos/gallery category
		$photos	= false;
		if ( $technical_theme_options['gallery_category'] ){
			if ( strpos ($technical_theme_options['gallery_category'], ',' ) !== false ){
				$gallery_cat_array = explode( ',', $technical_theme_options['gallery_category'] );
			} else {
				$gallery_cat_array[] = $technical_theme_options['gallery_category'];
			}
		}

		if ( !is_category() ){
			foreach ( get_the_category() as $category ) {
				if ( in_array( $category->cat_ID, $gallery_cat_array ) ){
					$photos = true;
				}
			}
		}

		if ( in_category('lists') ) {
			$photos = true;
		}

		// Determine if there's a special logo we should display instead of the image saved in the theme folder
		$secondary_logo_categories	= explode(',', $editorial_theme_options['secondary_logo_categories'] );

		if ( !empty( $secondary_logo_categories ) && !empty( $editorial_theme_options['secondary_logo'] ) && ( in_category( $secondary_logo_categories ) || is_category( $secondary_logo_categories ) ) ) {
			$site_logo = $editorial_theme_options['secondary_logo'];
		} else {
			$site_logo = !empty( $editorial_theme_options['logo'] ) ? $editorial_theme_options['logo'] : get_bloginfo('stylesheet_directory') . '/images/running/competitor-running-logo.svg';
		}
	?>
</head>
<body <?php body_class(); ?>>
	<?php
		if ( !empty( $technical_theme_options['google_tag'] ) ) {
			echo $technical_theme_options['google_tag'];
		}

		if ( !empty( $technical_theme_options['meebonetwork'] ) ) {
			get_meebo_network( $technical_theme_options['meebonetwork'] );
		}
	?>

	<div id="sb-site">

		<?php
			$background_skin = class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('background-skin') : '';
			if ( $background_skin != '' && !$is_mobile && !$is_tablet && $technical_theme_options['background_skin'] && !$photos ) {
				echo '<div id="background-skin">' .
					$background_skin . '
				</div>';
			}
		?>

		<div class="navbar" role="navigation">
			<div class="navbar-header">

				<div class="sb-toggle-left navbar-toggle">
					<span class="icon-menu"></span>
				</div>

				<a class="navbar-brand" href="<?php echo home_url(); ?>">
					<img alt="<?php echo get_bloginfo( 'name' ); ?> | <?php echo get_bloginfo( 'description' ); ?>" class="brand" src="<?php echo $site_logo ?>">
				</a>

				<?php
					wp_nav_menu( array(
						'menu'				=> 'primary',
						'menu_class'		=> 'main_nav',
						'container'			=> false,
						'fallback_cb'		=> 'false',
						'walker'			=> new TopNav_Walker(),
						'theme_location'	=> 'primary-nav'
					) );
				?>

				<div class="right_nav_btns">
					<ul class="social_icons">

						<?php if ( !empty( $technical_theme_options['site_facebook_page'] ) ) { ?>
							<li>
								<a target="_blank" href="https://facebook.com/<?php echo $technical_theme_options['site_facebook_page']; ?>" title="Like us on Facebook" onclick="_gaq.push(['_trackEvent', 'Top Nav Links', '<?php echo $name_tracking;?>', 'https://facebook.com/<?php echo $technical_theme_options['site_facebook_page']; ?>']);">
									<span class="icon-facebook"></span>
								</a>
							</li>
						<?php }

						if ( !empty( $technical_theme_options['site_twitter_handle'] ) ) { ?>
							<li>
								<a target="_blank" href="https://twitter.com/<?php echo $technical_theme_options['site_twitter_handle']; ?>" title="Follow us on Twitter" onclick="_gaq.push(['_trackEvent', 'Top Nav Links', '<?php echo $name_tracking;?>', 'https://twitter.com/<?php echo $technical_theme_options['site_twitter_handle']; ?>']);">
									<span class="icon-twitter"></span>
								</a>
							</li>
						<?php }

						if ( !empty( $technical_theme_options['site_instagram_handle'] ) ) { ?>
							<li>
								<a target="_blank" href="http://instagram.com/<?php echo $technical_theme_options['site_instagram_handle']; ?>" title="Follow us on Instagram" onclick="_gaq.push(['_trackEvent', 'Top Nav Links', '<?php echo $name_tracking;?>', 'http://instagram.com/<?php echo $technical_theme_options['site_instagram_handle']; ?>']);">
									<span class="icon-instagramm"></span>
								</a>
							</li>
						<?php }

						if ( !empty( $technical_theme_options['youtube_page'] ) ) { ?>
							<li>
								<a target="_blank" href="<?php echo $technical_theme_options['youtube_page']; ?>" title="Follow us on YouTube" onclick="_gaq.push(['_trackEvent', 'Top Nav Links', '<?php echo $name_tracking;?>', '<?php echo $technical_theme_options['youtube_page']; ?>']);">
									<span class="icon-youtube"></span>
								</a>
							</li>
						<?php } ?>

					</ul>

					<div class="sb-toggle-right navbar-right navbar-toggle">
						<span class="icon-dot-3"></span>
					</div>

					<div id="searchtoggle" class="sb-toggle-search navbar-search navbar-toggle">
						<span class="icon-search"></span>
					</div>

				</div>
				<div id="searchbar">
					<form class="navbar-form search" action="<?php echo home_url(); ?>/search" role="form">
						<input type="text" name="q" class="form-control" placeholder="Search <?php echo get_bloginfo('name'); ?>" value="">
						<button class="btn" type="submit">Search</button>
					</form>
				</div>
			</div>
		</div>

		<div class="container" id="container">

			<div id="externalBanner"></div>

			<?php
				if ( !is_page_template('one-column-modular.php') || ( is_page_template('one-column-modular.php') && get_post_meta($post->ID, 'leaderboard-checkbox', true) !='' ) ) { ?>

						<div class="hypnotoad_unit hypnotoad_970x90">
							<?php echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('top') : ''; ?>
						</div><!-- ad unit -->

					<?php if ( $editorial_theme_options['breaking_news_headline'] && $editorial_theme_options['breaking_news_link'] ) {
						echo '<div class="breaking_news_alert"><a href="' . $editorial_theme_options['breaking_news_link'] . '" title="Breaking News Alert">' . htmlspecialchars_decode($editorial_theme_options['breaking_news_headline']) . '</a></div>';
					}

					if ( $editorial_theme_options['sub_leaderboard_alert_banner_text'] && $editorial_theme_options['sub_leaderboard_alert_banner_link'] ) {
						echo '<div class="leaderboard_news_alert"><a href="' . $editorial_theme_options['sub_leaderboard_alert_banner_link'] . '">' . $editorial_theme_options['sub_leaderboard_alert_banner_text'] . '</a></div>';
					}
				}
			?>
