<?php
	$is_mobile = is_mobile();
?>

<div class="sidebar">
	<div class="video_side">
		<?php
			get_upcoming_vids( get_the_ID(), 0 );
		?>
	</div>
	<div class="hypnotoad_unit hypnotoad_300x250 hypnotoad_300x250_1">
		<?php
			if ( !$is_mobile ) {
				echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('side-top') : '';
			}
		?>
	</div><!-- ad unit -->

	<?php inline_iContact('sidebar'); ?>

	<?php if ( is_active_sidebar('marquee') ) : ?>
	<div class="module offers">
		<h3><img class="slashes slash_blue" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/running/running-slashes-blue.svg"> Running Resources</h3>
		<ul id="sidebar_offers_carousel">
			<?php dynamic_sidebar('marquee'); ?>
		</ul>
	</div><!-- offers carousel -->
	<?php endif; ?>

	<div class="row">
		<div class="hypnotoad_unit hypnotoad_300x250 hypnotoad_300x250_2">
			<?php
				echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('side-middle') : '';
			?>
		</div><!-- ad unit -->

		<div class="hypnotoad_unit hypnotoad_300x250 hypnotoad_300x250_3">
			<?php
				if ( !is_mobile() ) {
					echo class_exists('Wp_Dfp_Ads') ? Wp_Dfp_Ads::display_ad('side-video') : '';
				}
			?>
		</div><!-- ad unit -->
	</div>

</div>
