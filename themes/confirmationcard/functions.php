<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'before_widget'	=> '<li id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</li>',
		'before_title'	=> '<span class="widgettitle">',
		'after_title'	=> '</span>',
	));
}


function guid(){
	mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
	$charid	= strtoupper(md5(uniqid(rand(), true)));
	$hyphen	= chr(45);// "-"
	$uuid	= substr($charid, 0, 8).$hyphen
					.substr($charid, 8, 4).$hyphen
					.substr($charid,12, 4).$hyphen
					.substr($charid,16, 4).$hyphen
					.substr($charid,20,12);
	return $uuid;
}

/**
 * function: cmb2 filter to exclude a metabox from a template
 */
function be_metabox_exclude_on_template( $display, $meta_box ) {

	if( isset( $meta_box['show_on']['key'] ) && 'exclude_template' !== $meta_box['show_on']['key'] )
		return $display;

	// Get the current ID
	if( isset( $_GET['post'] ) ) $post_id = $_GET['post'];
	elseif( isset( $_POST['post_ID'] ) ) $post_id = $_POST['post_ID'];
	if( !isset( $post_id ) ) return false;

	$template_name = get_page_template_slug( $post_id );
	$template_name = substr($template_name, 0, -4);

	// If value isn't an array, turn it into one
	if( isset( $meta_box['show_on']['value'] ) ) {
		if( !is_array( $meta_box['show_on']['value'] ) ) {
			$showonvalue = array( $meta_box['show_on']['value'] );
		} else {
			$showonvalue = $meta_box['show_on']['value'];
		}
	} else {
		$showonvalue = array();
	}

	// See if there's a match
	if( in_array( $template_name, $showonvalue ) ) {
		return false;
	} else {
		return true;
	}
}
add_filter( 'cmb2_show_on', 'be_metabox_exclude_on_template', 10, 2 );


/**
 * get race display name
 * @param	string $division	specific distance
 * @param	string $lang		language
 * @return	string				distance, translated
 */
function get_race_display_name( $division, $lang ) {

	switch ( $lang ) {
		case "es":
			$div_1k		= '1 kil&oacute;metro';
			$div_1m		= '1 Mile';
			$div_4k		= '4 kil&oacute;metros';
			$div_5k		= '5 kil&oacute;metros';
			$div_7k		= '7 kil&oacute;metros';
			$div_8k		= '8 kil&oacute;metros';
			$div_10k	= '10 kil&oacute;metros';
			$div_11k	= '11 kil&oacute;metros';
			$div_mini	= 'Mini Marath&oacute;n';
			$div_relay	= 'Relevo medio marat&oacute;n para 2 personas';
			$div_half	= 'Medio Marat&oacute;n';
			$div_full	= 'Marat&oacute;n';

			break;
		case "pt":
			$div_1k		= '1 kil&oacute;metro';
			$div_1m		= '1 Mile';
			$div_4k		= '4 kil&oacute;metros';
			$div_5k		= '5 kil&oacute;metros';
			$div_7k		= '7 kil&oacute;metros';
			$div_8k		= '8 kil&oacute;metros';
			$div_10k	= '10 kil&oacute;metros';
			$div_11k	= '11 kil&oacute;metros';
			$div_mini	= 'Mini Marath&oacute;n';
			$div_relay	= 'Relevo medio marat&oacute;n para 2 personas';
			$div_half	= 'Medio Marat&oacute;n';
			$div_full	= 'Marat&oacute;n';

			break;
		case "en":
			$div_1k		= '1K';
			$div_1m		= '1 Mile';
			$div_4k		= '4K';
			$div_5k		= '5K';
			$div_7k		= '7K';
			$div_8k		= '8K';
			$div_10k	= '10K';
			$div_11k	= '11K';
			$div_mini	= 'Mini Marathon';
			$div_relay	= '2-Person Half Marathon Relay';
			$div_half	= 'Half Marathon';
			$div_full	= 'Marathon';

			break;
		case "fr":
			$div_1k		= 'P&apos;tit Marathon - 1 km';
			$div_1m		= '1 Mile';
			$div_4k		= '4 km';
			$div_5k		= '5 km';
			$div_7k		= '7 km';
			$div_8k		= '8 km';
			$div_10k	= '10 km';
			$div_11k	= '11 km';
			$div_mini	= 'Mini marathon';
			$div_relay	= 'Demi-marathon &agrave; relais- 2 personnes';
			$div_half	= 'Demi-marathon';
			$div_full	= 'Marathon';

			break;
		case 'mx';
			$div_1k		= '1 kil&oacute;metro';
			$div_1m		= '1 Mile';
			$div_4k		= '4 kil&oacute;metros';
			$div_5k		= '5 kil&oacute;metros';
			$div_7k		= '7 kil&oacute;metros';
			$div_8k		= '8 kil&oacute;metros';
			$div_10k	= '10 kil&oacute;metros';
			$div_11k	= '11 kil&oacute;metros';
			$div_mini	= 'Mini Marath&oacute;n';
			$div_relay	= 'Relevo medio marat&oacute;n para 2 personas';
			$div_half	= 'Medio Marat&oacute;n';
			$div_full	= 'Marat&oacute;n';

			break;
		default:
			$div_1k		= '1K';
			$div_1m		= '1 Mile';
			$div_4k		= '4K';
			$div_5k		= '5K';
			$div_7k		= '7K';
			$div_8k		= '8K';
			$div_10k	= '10K';
			$div_11k	= '11K';
			$div_mini	= 'Mini Marathon';
			$div_relay	= '2-Person Half Marathon Relay';
			$div_half	= 'Half Marathon';
			$div_full	= 'Marathon';

			break;
	}


	if( ( strpos( $division, '11k' ) ) !== false ) {

		$division_parsed = $div_11k;

	} elseif( ( strpos( $division, '1m' ) ) !== false ) {

		$division_parsed = $div_1m;

	} elseif( ( strpos( $division, '4k' ) ) !== false ) {

		$division_parsed = $div_4k;

	} elseif( ( strpos( $division, '5k' ) ) !== false ) {

		$division_parsed = $div_5k;

	} elseif( ( strpos( $division, '7k' ) ) !== false ) {

		$division_parsed = $div_7k;

	} elseif( ( strpos( $division, '8k' ) ) !== false ) {

		$division_parsed = $div_8k;

	} elseif( ( strpos( $division, '10k' ) ) !== false ) {

		$division_parsed = $div_10k;

	} elseif( ( strpos( $division, '1k' ) ) !== false ) {

		$division_parsed = $div_1k;

	} elseif( ( strpos( $division, 'mini' ) ) !== false ) {

		$division_parsed = $div_mini;

	} elseif( ( strpos( $division, 'half' ) ) !== false ) {

		$division_parsed = $div_half;

	} elseif( ( strpos( $division, 'full' ) ) !== false ) {

		$division_parsed = $div_full;

	} elseif( ( strpos( $division, 'relay' ) ) !== false ) {

		$division_parsed = $div_relay;

	} elseif( ( strpos( $division, 'mini' ) ) !== false ) {

		$division_parsed = $div_mini;

	}

	return $division_parsed;

}


/**
 * get month name
 * @param	string	$lang		language
 * @param	int		$position	month number (1-12)
 * @return	string				specific month in language specified
 */
function get_month_name( $lang, $position ) {

	switch( $lang ) {
		case 'pt':
			$month = array( '', "Janeiro", "Fevereiro", "Mar&ccedil;o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" );
			break;
		case 'es':
		case 'mx':
			$month = array( '', "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" );
			break;
		case 'fr':
			$month = array( '', "Janvier", "F&#233;vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Ao&#251;t", "Septembre", "Octobre", "Novembre", "D&#233;cembre" );
			break;
		default:
			$month = array( '', "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" );
			break;
	}
	return $month[$position];

}

?>