<?php
  header('P3P: CP="CAO PSA OUR"');
	session_start(); // start up your PHP session!
  require_once("../../../wp-load.php");
  global $wpdb;

  $first_name = $_SESSION['first_name'];
  $last_name = $_SESSION['last_name'];
  $birthday = $_SESSION['birthdate'];
  $event = $_SESSION['event'];
  $lang = $_REQUEST['lang'];
  $post_id = $_SESSION['post_id'];

  //echo $first_name . $last_name . $birthday . $event;

  $query = "SELECT last_name, first_name, address_1, address_2, city, state, zip, birthdate, wave_number, bib_number, team_name, start_time, division, event, race FROM wp_mb_confirmation_card_data WHERE birthdate = '$birthday' AND first_name = '$first_name' AND last_name = '$last_name' AND event LIKE '%$event%'";
  // echo $query;
  $racer = $wpdb->get_row($query);

  $lang2 = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 5);

?>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"> 
 <html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $lang2; ?>">
  <head profile="http://gmpg.org/xfn/11">
  <title><?php echo $raceinfo->full_event_name . " Confirmation Sheet"; ?></title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/confcard.css" />
  <script type="text/javascript">
    function printPage() {
      if (window.print)
        window.print()
      else
        alert("Sorry, your browser doesn't support this feature.\n\nPlease use the print button on your browser instead.");
    }
  </script>
 </head>

 <body>
  <div id="print_this">
    <a href="#" onclick="javascript:printPage()">
      <?php echo 'Click Here To Print This Page'; ?>
    </a>
  </div>
  <div id="wrapper">
    <div id="header_text">
      <h1>
        <?php echo get_post_meta ($post_id, '_mb_full_event_name', true); ?><br />Confirmation Sheet
      </h1>
      <p><strong><?php echo get_post_meta ($post_id, '_mb_confirmation_message', true); ?></strong></p>
    </div>
    <div class="group">
      <h2>Participant Name: <?php echo $racer->first_name . " " . $racer->last_name ?></h2>
      <ol id="mb_raceday_info">
        <li class="first_row team_name">Team Name:
          <span class="result"><?php echo $racer->team_name; ?></span> 
        </li>
        <li class="first_row start_time">Start Time: 
          <span class="result"><?php echo $racer->start_time; ?></span>
        </li>
        <li class="first_row bib_number">Bib Number: 
          <span class="result"><?php echo $racer->bib_number; ?></span>
        </li>
        <li class="first_row wave_number">Wave Number: 
          <span class="result"><?php echo $racer->wave_number; ?></span>
        </li>
        <li class="first_row event">Event: 
          <span class="result"><?php echo $racer->race; ?></span>
        </li>
        <li class="first_row division">Division: 
          <span class="result"><?php echo $racer->division; ?></span>
        </li>
      </ol>
    </div>
    <div class="group">
      <div id="waiver_text">
        <h2>
          <?php echo get_post_meta ($post_id, '_mb_pre_waiver_heading', true); ?>
        </h2>
        <p><?php echo get_post_meta ($post_id, '_mb_waiver_text', true); ?></p>

      </div>
    </div>

    <ol id="signature">
      <li>Print Name</li>
      <li>Signature of Athlete</li>
      <li class="signature_date">Date</li>
    </ol>

    <h2>Parent/Guardian signature for minors (UNDER 18 YEARS OLD) and participants in Mini Muddy Buddy (ages 4-11)</h2>
    <p><?php echo get_post_meta ($post_id, '_mb_minor_waiver_text', true); ?></p>
    <p class="minor_disclaimer"><?php echo $minor_disclaimer; ?></p>
    <ol id="signature_minor1">
      <li>Child's Name</li>
      <li>Age</li>
    </ol>
    <ol id="signature_minor2">
      <li>Parent/Guardian Name</li>
      <li>Parent/Guardian Signature</li>
      <li>Date</li>
    </ol>
  </form>
 </body>
</html>
<?php session_destroy(); ?>


