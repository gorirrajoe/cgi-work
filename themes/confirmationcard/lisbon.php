<?php
/* Template Name: Confirmation Card Lisbon */

  header( 'P3P: CP="CAO PSA OUR"' );
  session_start(); // start up your PHP session!

  global $wpdb;
  
  $pageurl = get_permalink();
  $pageurl_pieces = explode( "/", $pageurl );

  $slug = basename( get_permalink() );

  $event = get_post_meta ( get_the_ID(), '_confcard_lisbon_event_code', true );

  //print_r( $pageurl_pieces );
  if ( $pageurl_pieces[4] == "es" ) {
    $lang2 = "es";
  } elseif ( $pageurl_pieces[4] == "pt" ) {
    $lang2 = "pt";
  } else {
    $lang2 = "en";
  }
  $lang2 = $pageurl_pieces[4];

  // default language = english
  if ( strlen( $lang2 ) > 2 ) {
    $lang2 = "en";
  }
  //echo $lang2;

  $url_vars = parse_url( $pageurl );
  if ( $url_vars[host] == 'confcard.lan' ) {  // dev environment
    $parent_url = 'http://runrocknroll.cgitesting.lan/';
  } elseif ( $url_vars[host] == 'confirmation.competitor.com' ) { // production environment
    $parent_url = 'http://runrocknroll.competitor.com/';
  }

  get_header(); ?>

  <div class="wrapper">

    <?php
      if( isset( $_POST['submit'] ) ) {
        // Define form variables, if isset, if not default
        if( isset( $_POST['first_name'] ) ) {
          $first_name = htmlentities( $_POST['first_name'], ENT_COMPAT, 'UTF-8' );
        } else {
          $first_name = "";
        }
        if( isset( $_POST['email'] ) ){
          $email = $_POST['email'];
        } else {
          $email = "";
        }
        if( isset( $_POST['birthyear'] ) ){
          $birthyear = $_POST['birthyear'];
        } else {
          $birthyear = "";
        }
        /* testing
        echo $first_name . "<br />";
        echo $email . "<br />";
        echo $birthyear ."<br />";
        echo $event;
        */

        $event_var = '%' . $event . '%';
        $racer = $wpdb->get_row( 
          $wpdb->prepare( "SELECT first_name, email, birthdate, event_code FROM wp_lis_confirmation_card_data WHERE birthdate = %s AND first_name LIKE %s AND email LIKE %s AND event_code LIKE %s", $birthyear, $first_name, $email, $event_var )
        );

        // print_r( $racer );
        $_SESSION['email'] = $racer->email;
        $_SESSION['first_name'] = $racer->first_name;
        $_SESSION['birthyear'] = $racer->birthdate;
        $_SESSION['event'] = $event;
        $_SESSION['post_id'] = get_the_ID();

        /*
        echo $_SESSION['email'] . "<br />";
        echo $_SESSION['first_name'] . "<br />";
        echo $_SESSION['birthyear'] ."<br />";
        */

        $page_url = site_url() . "/" . $permalink_slug . "/" . $slug;

        if ( $racer != NULL  ) {
          $event_slug = $permalink_slug;
          $stylesheet_directory = get_bloginfo( "stylesheet_directory" );
          $conf_card_printout = $stylesheet_directory . "/lisbon-confirmation-card-printout.php";

          echo '<h2>';
            _e( "[:en]Confirmation Sheet Found!
            [:es]&#161;Hoja de confirmaci&#243;n encontrada!
            [:pt]O comprovativo de inscri&ccedil;&atilde;o foi encontrado" );
          echo '</h2>
          <p>';
            _e( "[:en]Your participant information was located in our system. Please press the button below to retrieve a printable version of your Confirmation Sheet
            [:es]Hemos encontrado tu informaci&#243;n de participaci&#243;n en nuestro sistema. Pulsa en el bot&#243;n siguiente para recuperar una versi&#243;n imprimible de tu hoja de confirmaci&#243;n.
            [:pt]A sua informa&#231;&#227;o de participa&#231;&#227;o foi localizada no nosso sistema. Por favor pressione o bot&#227;o abaixo para recuperar uma vers&#227;o para impress&#227;o do seu comprovativo de inscri&#231;&#227;o." );

          echo '</p>';
          $guid_value = guid();
          if( get_post_meta ( get_the_ID(), '_confcard_lisbon_redirect_url', true ) ) {
            $redirect_url = get_post_meta ( get_the_ID(), '_confcard_lisbon_redirect_url', true );
          } else {
            $redirect_url = $parent_url.$slug;
          }
          echo '<div class="print_confirm_btn">
            <a class="print_confirmation_sheet" href="'.$redirect_url.'" target="_parent" onclick="window.open( \''.$conf_card_printout.'?lang='.$lang2.'&result='.$guid_value.'\', \'confirmation\', \'toolbar=yes, location=yes, directories=yes, status=yes, menubar=yes, scrollbars=yes, copyhistory=yes, resizable=yes\' )">';
              _e( "[:en]Print Confirmation Sheet
              [:es]Imprimir hoja de confirmaci&#243;n
              [:pt]Imprimir o documento comprovativo de inscri&#231;&#227;o" );
            echo '</a>
          </div>';
        } else {
          echo '<h2>';
            _e( "[:en]No Matching Participant Found
            [:es]Participante no encontrado
            [:pt]N&#227;o encontr&#225;mos nenhum registo do participante" );
          echo '</h2>
          <p>';
            _e( "[:en]Please double check your information and try submitting again. In case you have difficulties downloading or printing this mandatory document, blank confirmation cards will be available in the lobby of the Health &amp; Fitness Expo for you to complete and turn in. Please bring photo ID
            [:es]Comprueba la informaci&#243;n y vuelve a intentarlo. Si tienes alg&#250;n problema para descargar o imprimir este documento obligatorio, habr&#225; tarjetas de confirmaci&#243;n en blanco disponibles en el vest&#237;bulo de la Health & Fitness Expo para que las rellenes y entregues. Trae un documento de identidad con foto.
            [:pt]Por favor confirme as informa&#231;&#245;es e tente submeter novamente.  Caso tenha dificuldade em descarregar ou imprimir este documento, vamos disponibilizar impressos em branco &#224; entrada da  Feira do desporto e sa&#250;de , para preencher e nos entregar. Por favor traga o seu documento de identifica&#231;&#227;o com fotografia." );
          echo '</p>
          <div class="print_confirm_btn">
            <a class="try_again print_confirmation_sheet" href="'.$page_url.'">';
              _e( "[:en]Try Again
              [:es]Int&#233;ntalo de nuevo
              [:pt]Tente outra vez" );
            echo '</a>
          </div>';
        }
      } else { // main page ?>
        <h1>
          <?php _e( get_post_meta ( get_the_ID(), '_confcard_lisbon_full_event_name', true ) ); ?>
        </h1>
        <h2>
          <?php
            _e( "[:en]Retrieve Confirmation Sheet ( required for packet pickup )
            [:es]Recuperar hoja de confirmaci&#243;n ( necesaria para la recogida del paquete )
            [:pt]Recuperar o comprovativo de inscri&#231;&#227;o ( necess&#225;rio para levantamento do dorsal )" );
          ?>
        </h2>
        <?php /*
          <div class="lang_chooser">
            <ul><?php if (  !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar()  ) : endif; ?></ul>
          </div>
        */ ?>
        <p>
          <?php
            _e( "[:en]Please complete the information below to retrieve your Confirmation Sheet. <strong>Please note: Confirmation sheets on mobile devices will  not be accepted. You must print your confirmation sheet.</strong>
            [:es]Rellena la siguiente informaci&#243;n para recuperar tu hoja de confirmaci&#243;n. <strong>Recuerda: no se aceptar&#225;n hojas de confirmaci&#243;n en dispositivos m&#243;viles. Debes imprimir tu hoja de confirmaci&#243;n.</strong>
            [:pt]Por favor preencha as informa&#231;&#245;es abaixo para recuperar o seu comprovativo de inscri&#231;&#227;o. <strong>Nota: comprovativos apresentados em dispositivos m&#243;veis n&#227;o s&#227;o aceites. Deve imprimir o comprovativo de inscri&#231;&#227;o.</strong>" );
           ?>
        </p>

        <?php
          if( get_post_meta( get_the_ID(), '_confcard_lisbon_database_current_to', true ) != null  ) {
            $db_current_to = get_post_meta( get_the_ID(), '_confcard_lisbon_database_current_to', true );
            $db_current_to_convert = date( "F j, Y", $db_current_to );

            $db_updated_on = get_post_meta( get_the_ID(), '_confcard_lisbon_database_updated_on', true );
            $db_updated_on_convert = date( "F j, Y", $db_updated_on );

            echo '<div class="attn_warning">
              <p>';
                _e( '[:en]<strong>Please Note: </strong> If you registered after <strong>'. $db_current_to_convert .'</strong>, your Confirmation Sheet is not yet available. Please check back starting <strong>'. $db_updated_on_convert .'</strong>.
                [:es]<strong>NOTA IMPORTANTE:</strong> Si usted se registr&oacute; despu&eacute;s del <strong>'. $db_current_to_convert .'</strong>, su hoja de confirmaci&oacute;n no est&aacute; todav&iacute;a disponible. Por favor, vuelva a intentarlo a partir del <strong>'. $db_updated_on_convert .'</strong>
                [:pt]Aten&ccedil;&atilde;o: Se inscreveu-se ap&oacute;s <strong>'. $db_current_to_convert .'</strong>, o seu comprovativo ainda n&atilde;o est&aacute; dispon&iacute;vel. Por favor volte a consultar a partir de <strong>'. $db_updated_on_convert .'</strong>.' );
              echo '</p>
            </div>';
          }
        ?>

        <form class="usr_conf" name="form" action="<?php echo get_permalink(); ?>" method="post" onsubmit="return checkform( this );">
          <ol>
            <li>
              <label for="first_name">
                <?php
                  _e( "[:en]First Name
                  [:es]Nombre
                  [:pt]Nome" );
                ?>
              </label>
              <input id="first_name" name="first_name" type="text" />
            </li>
            <li>
              <label for="email">
                <?php
                  _e( "[:en]Email
                  [:es]Correo electr&#243;nico
                  [:pt]Correio eletr&ocirc;nico" );
                 ?>
              </label>
              <input id="email" name="email" type="text" />
            </li>
            <li>
              <label for="birthyear">
                <?php
                  _e( "[:en]Year of Birth
                  [:es]A&ntilde;o de nacimiento
                  [:pt]Ano de Nascimento" );
                ?>
              </label>
              <?php
                echo '<select name="birthyear" type="text" id="birthyear">
                  <option value="-1">YYYY</option>';
                  for ( $i = date( "Y" ); $i >= 1920; $i-- ) {
                    echo '<option value="'. $i;
                      if (  $birthyear == $i  ) {
                        echo 'selected ';
                      }
                    echo '>'. $i .'</option>';
                  }
                echo '</select>';
              ?>
            </li>
            <li>
              <input type="submit" name="submit" value="<?php _e( "[:en]Retrieve Confirmation Sheet[:es]Recuperar hoja de confirmaci&oacute;n[:pt]Recuperar o comprovativo de inscri&#231;&#227;o" ); ?>" />
            </li>
          </ol>
        </form>
        <div id="conf_sheet_faq">
          <h2>
            <?php
              _e( "[:en]Confirmation Sheet FAQ
              [:es]Hoja de confirmaci&#243;n: preguntas frecuentes
              [:pt]Comprovativo de inscri&#231;&#227;o FAQ" );
            ?>
          </h2>
          <p><strong>
            <?php
              _e( "[:en]Q. What if my confirmation sheet is blank?
              [:es]P. &#191;Qu&#233; sucede si mi hoja de confirmaci&#243;n est&#225; en blanco?
              [:pt]E se o meu comprovativo de inscri&#231;&#227;o est&#225; em branco?" );
            ?>
          </strong></p>
          <p>
            <?php
              _e( "[:en]A. Were you pulling up the Confirmation Sheet in Firefox or AOL? Often times those browsers do not support the file, so please attempt to use Internet Explorer to open the document. If for whatever reason you are still unable to pull up your confirmation sheet and you are sure that you're registered ( either by an e-mailed receipt/confirmation or credit card statement/cleared check ), simply bring your photo ID with you to the expo to fill out a blank confirmation sheet to retrieve your race packet.
              [:es]R. &#191;Intentabas recuperar la hoja de confirmaci&#243;n en Firefox o AOL? A menudo, estos navegadores no son compatibles con el archivo. Por favor, intenta utilizar Internet Explorer para abrir el documento. Si por cualquier motivo sigues sin poder recuperar tu hoja de confirmaci&#243;n y est&#225;s seguro de que te has inscrito ( mediante un recibo/confirmaci&#243;n por correo electr&#243;nico o extracto bancario de tarjeta de cr&#233;dito/cheque autorizado ), trae simplemente un documento de identidad con foto a la expo para rellenar una hoja de confirmaci&#243;n en blanco para obtener tu paquete para la carrera.
              [:pt]Est&#225; a utilizar o Firefox ou AOL para obter o comprovativo de inscri&#231;&#227;o? Muitas vezes estes navegadores n&#227;o suportam o ficheiro, por favor tente usar o Internet Explorer para abrir o documento. Se por qualquer motivo mesmo assim n&#227;o consegue obter o comprovativo, e tem a certeza de que est&#225; inscrito ( seja porque recebeu um recibo por e-mail/comprovativo ou extracto de cart&#227;o de cr&#233;dito ), apresente o seu documento de identifica&#231;&#227;o com fotografia na Expo e preencha o impresso em branco para levantar o seu dorsal." );
            ?>

          </p>
          <p><strong>
            <?php
              _e( "[:en]Q. What if it shows that I am Not found in the database?
              [:es]P. &#191;Qu&#233; sucede si dice que no es posible encontrarme en la base de datos?
              [:pt]E se o meu comprovativo de inscri&#231;&#227;o revela que n&#227;o sou encontrado na base de dados?" );
            ?>
          </strong></p>
          <p>
            <?php
              _e( "[:en]A. Please put in your information exactly as it shows on your receipt/confirmation that you received when you originally registered. Often times, names are entered backwards or we have an incorrect date of birth for you. If you can confirm your registration ( either by an e-mailed receipt/confirmation or credit card statement/cleared check ) and there is a typo with your name or date of birth, please be sure to visit the Solutions table at the Health and Fitness Expo and we will be able to change your information in the system. We will have blank confirmation sheets for you to fill out at the expo to retrieve your race packet, make sure that you bring your Photo ID.
              [:es]R. Introduce tu informaci&#243;n exactamente como aparece en el recibo/confirmaci&#243;n que recibiste al registrarte. A veces, los apellidos se introducen al rev&#233;s o tenemos una fecha de nacimiento incorrecta. Si puedes confirmar tu inscripci&#243;n ( mediante un recibo/confirmaci&#243;n por correo electr&#243;nico o extracto bancario de tarjeta de cr&#233;dito/cheque autorizado ) y hay alg&#250;n error en tu nombre o fecha de nacimiento, visita la mesa Solutions en la Health and Fitness Expo y podremos modificar tu informaci&#243;n en el sistema. Tendremos hojas de confirmaci&#243;n en blanco para que las rellenes en la expo para obtener tu paquete para la carrera. No olvides traer un documento de identidad con foto.
              [:pt]Por favor preencha a sua informa&#231;&#227;o exactamente como estava no seu recibo/comprovativo de inscri&#231;&#227;o, que recebeu quando se inscreveu. Muitas vezes os nomes s&#227;o inseridos ao contr&#225;rio ou temos a informa&#231;&#227;o da sua data de nascimento errada. Se consegue comprovar que se encontra inscrito ( seja porque recebeu um recibo por e-mail/comprovativo ou extracto de cart&#227;o de cr&#233;dito ), ou existe um erro no seu nome ou data de nascimento, n&#227;o deixe de visitar a Expo de desporto e sa&#250;de, onde podemos alterar as suas informa&#231;&#245;es no sistema. Iremos ter impressos em branco para voc&#234; preencher e assim poder levantar o seu kit de participa&#231;&#227;o. N&#227;o se esque&#231;a de levar o seu cart&#227;o de identifica&#231;&#227;o com fotografia." );
            ?>
          </p>
        </div>
        <script type="text/javascript">
          function checkform( form ) {
            var fname = document.getElementById( 'firstname' ).value;
            var email = document.getElementById( 'email' ).value;
            var birthyear = document.getElementById( 'birthyear' ).value;
            if ( !fname ) {
              alert( "<?php _e( '[:en]Please enter a first name[:es]Por favor, introduzca un nombre[:pt]Por favor insira um nome' ); ?>" );
              return false;
            }
            if ( !email ) {
              alert( "<?php _e( '[:en]Please enter an email address[:es]Introduzca una direcci&oacute;n de correo electr&oacute;nico[:pt]Por favor insira um endere&ccedil;o de e-mail' ); ?>" );
              return false;
            }
            if ( birthyear == "-1" )  {
              alert( "<?php _e( '[:en]Please select your year of birth[:es]Por favor seleccione su a&ntilde;o de nacimiento[:pt]Por favor, selecione o seu ano de nascimento' ); ?>" );
              return false;
            } else {
              document.form.submit();
            }
          }
        </script>
      <?php }
    ?>

  </div>


<?php get_footer(); ?>
