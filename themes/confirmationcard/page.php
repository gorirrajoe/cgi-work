<?php
	header( 'P3P: CP="CAO PSA OUR"' );
	session_start(); // start up your PHP session!

	global $wpdb;

	$pageurl		= get_permalink();
	$pageurl_pieces	= explode( "/", $pageurl );

	$slug	= basename( get_permalink() );
	$prefix	= '_confcard_';

	$event				= get_post_meta( get_the_ID(), $prefix . 'event_code', 1 );
	$isInternational	= get_post_meta( get_the_ID(), $prefix . 'international', 1 );
	$isMexican			= get_post_meta( get_the_ID(), $prefix . 'mexico', 1 );

	if( $pageurl_pieces[3] == "es" ) {
		$lang2 = "es";
	} elseif( $pageurl_pieces[3] == "pt" ) {
		$lang2 = "pt";
	} elseif( $pageurl_pieces[3] == "fr" ) {
		$lang2 = "fr";
	} elseif( $pageurl_pieces[3] == "mx" ) {
		$lang2 = "mx";
	} else {
		$lang2 = "en";
	}

	if( $lang2 == "fr" ) {
		setlocale( LC_ALL, 'fr_FR' );
	} elseif( $lang2 == "es" ) {
		setlocale( LC_ALL, 'es_ES' );
	} elseif( $lang2 == "pt" ) {
		setlocale( LC_ALL, 'pt_PT' );
	} elseif( $lang2 == "mx" ) {
		setlocale( LC_ALL, 'es_MX' );
	}

	$url_vars = parse_url( $pageurl );
	// echo $url_vars[host];

	if( $url_vars[host] == 'confcard.lan' ) {  // dev environment
		$parent_url = 'http://runrocknroll.cgitesting.lan/';
	} elseif( $url_vars[host] == 'confirmation.competitor.com' ) { // production environment
		$parent_url = 'http://runrocknroll.competitor.com/';
	}

	get_header();


	if( isset( $_POST['submit'] ) ) {

		if( $isMexican != 'on' ) {
			// Define form variables, if isset, if not default
			if( isset( $_POST['firstname'] ) ) {
				$first_name = stripslashes( $_POST['firstname'] );
			} else {
				$first_name = "";
			}

			if( isset( $_POST['lastname'] ) ){
				$last_name = stripslashes( $_POST['lastname'] );
			} else {
				$last_name = "";
			}

			if( isset( $_POST['dob_month'] ) ){
				$dob_month = $_POST['dob_month'];
			} else {
				$dob_month = "";
			}

			if( isset( $_POST['dob_day'] ) ){
				$dob_day = $_POST['dob_day'];
			} else {
				$dob_day = "";
			}

			if( isset( $_POST['dob_year'] ) ){
				$dob_year = $_POST['dob_year'];
			} else {
				$dob_year = "";
			}

			$birthday = $dob_month . "/" . $dob_day . "/" . $dob_year;

			/*echo $first_name . "<br />";
			echo $last_name . "<br />";
			echo $birthday ."<br />";
			echo $event;*/

			$last_name_var	= '%' . $last_name;
			$event_var		= '%' . $event . '%';

			$racer = $wpdb->get_results(
				$wpdb->prepare(
					"SELECT last_name, first_name, birthdate, event_code, division FROM wp_confirmation_card_data WHERE birthdate = %s AND first_name = %s AND last_name LIKE %s AND event_code LIKE %s",
					$birthday, $first_name, $last_name_var, $event_var
				)
			);

		} else {
			// Define form variables, if isset, if not default
			if( isset( $_POST['firstname'] ) ) {
				$first_name = stripslashes( $_POST['firstname'] );
			} else {
				$first_name = "";
			}

			if( isset( $_POST['email'] ) ) {
				$email = stripslashes( $_POST['email'] );
			} else {
				$email = "";
			}

			if( isset( $_POST['dob_year'] ) ){
				$dob_year = $_POST['dob_year'];
			} else {
				$dob_year = "";
			}

			/*
			echo $first_name . "<br />";
			echo $email . "<br />";
			echo $dob_year ."<br />";
			echo $event;
			*/
			$racer = $wpdb->get_results(
				$wpdb->prepare(
					"SELECT last_name, first_name, birthdate, event_code, division FROM wp_confirmation_card_data WHERE birthdate = %s AND first_name = %s AND email LIKE %s AND event_code LIKE %s",
					$dob_year, $first_name, '%' . $email . '%', '%' . $event . '%'
				)
			);

		}
		//print_r( $racer );

		$page_url = site_url() . "/" . $permalink_slug . "/" . $slug;

		if( $racer != NULL  ) {
			$event_slug				= $permalink_slug;
			$stylesheet_directory	= get_bloginfo( "stylesheet_directory" );
			$conf_card_printout		= $stylesheet_directory . "/confirmation-card-printout-v2.php";

			echo '<h2>';
				if( count( $racer ) > 1 ) { // plural
					_e( "[:en]Confirmation Sheets Found!
					[:es]Hojas de confirmaci&oacute;n encontrada
					[:pt]O comprovativo de inscri&ccedil;&atilde;os foi encontrado
					[:fr]Feuilles de confirmation trouv&#233;e
					[:mx]Hojas de confirmaci&oacute;n encontrada" );
				} else {  // singular
					_e( "[:en]Confirmation Sheet Found!
					[:es]Hoja de confirmaci&oacute;n encontrada
					[:pt]O comprovativo de inscri&ccedil;&atilde;o foi encontrado
					[:fr]Feuille de confirmation trouv&#233;e
					[:mx]Hoja de confirmaci&oacute;n encontrada" );
				}
			echo '</h2>

			<p>';
				_e( "[:en]Your participant information was located in our system. Please press the button below to retrieve a printable version of your Confirmation Sheet
				[:es]Su informaci&oacute;n como participante se ha introducido en nuestro sistema con exito. Por favor, pulse el bot&oacute;n de abajo para obtener una versi&oacute;n imprimible de la hoja de confirmaci&oacute;n.
				[:pt]A sua informa&ccedil;&atilde;o de participa&ccedil;&atilde;o foi localizada no nosso sistema. Por favor pressione o bot&atilde;o abaixo para recuperar uma vers&atilde;o de impress&atilde;o do seu comprovativo de inscri&ccedil;&atilde;o.
				[:fr]Les renseignements confirmant votre inscription figurent dans notre base de donn&#233;es. Veuillez appuyer sur le bouton ci-dessous pour obtenir une version imprimable de votre feuille de confirmation.
				[:mx]Su informaci&oacute;n como participante se localiz&oacute; exitosamente  en nuestro sistema. Por favor, pulse el bot&oacute;n de abajo para obtener una versi&oacute;n impresa de la hoja de confirmaci&oacute;n." );

			echo '</p>';

			$confcard_return_url = get_post_meta( get_the_ID(), $prefix . 'return_url', 1 );

			if( $confcard_return_url != '' ) {
				$event_home = $confcard_return_url;
			} else {
				$event_home = $parent_url.$slug;
			}

			$count = 0;
			if( count( $racer ) > 1 ) {
				foreach( $racer as $racer_event ) {
					$guid_value						= guid();
					$_SESSION['last_name_'.$count]	= $racer_event->last_name;
					$_SESSION['first_name_'.$count]	= $racer_event->first_name;
					$_SESSION['birthdate_'.$count]	= $racer_event->birthdate;
					$_SESSION['division_'.$count]	= $racer_event->division;
					$_SESSION['event']				= $event;
					$_SESSION['post_id']			= get_the_ID();

					/*
					echo $_SESSION['last_name_'.$count] . "<br />";
					echo $_SESSION['first_name_'.$count] . "<br />";
					echo $_SESSION['birthdate_'.$count] ."<br />";
					echo $_SESSION['division_'.$count] ."<br />";
					echo $_SESSION['event'] ."<br />";
					echo $count;
					*/

					$count++;
				}
				echo '<div class="print_confirm_btn">
					<a class="print_confirmation_sheet" target="_blank" href="'.$conf_card_printout.'?language='.$lang2.'&count='.$count.'&result='.$guid_value.'">';
						_e( "[:en]Print Confirmation Sheet
						[:es]Imprimir hoja de confirmaci&oacute;n
						[:pt]Imprimir o documento comprovativo de inscri&ccedil;&atilde;o
						[:fr]Imprimer la feuille de confirmation
						[:mx]Imprimir hoja de confirmaci&oacute;n" );
					echo '</a>
				</div>';

			} else {
				$_SESSION['last_name']	= $racer[0]->last_name;
				$_SESSION['first_name']	= $racer[0]->first_name;
				$_SESSION['birthdate']	= $racer[0]->birthdate;
				$_SESSION['division']	= $racer[0]->division;
				$_SESSION['event']		= $event;
				$_SESSION['post_id']	= get_the_ID();

				/*
				echo $_SESSION['last_name'] . "<br />";
				echo $_SESSION['first_name'] . "<br />";
				echo $_SESSION['birthdate'] ."<br />";
				echo $_SESSION['event'] ."<br />";
				echo $_SESSION['division'] ."<br />";
				*/

				$guid_value = guid();

				echo '<div class="print_confirm_btn">
					<a class="print_confirmation_sheet" target="_blank" href="'.$conf_card_printout.'?language='.$lang2.'&result='.$guid_value.'">';
						_e( "[:en]Print Confirmation Sheet
						[:es]Imprimir hoja de confirmaci&oacute;n
						[:pt]Imprimir o documento comprovativo de inscri&ccedil;&atilde;o
						[:fr]Imprimer la feuille de confirmation
						[:mx]Imprimir hoja de confirmaci&oacute;n" );
					echo '</a>
				</div>';
			}
		} else {
			echo '<h2>';
				_e( "[:en]No Matching Participant Found
				[:es]No se ha encontrado ningun registro asociado
				[:pt]N&atilde;o encontramos nenhum registo do participante
				[:fr]Aucune correspondance trouv&#233;e dans le syst&#232;me
				[:mx]No se ha encontrado ning&uacute;n registro asociado" );
			echo '</h2>

			<p>';
				_e( "[:en]Please double check your information and try submitting again. In case you have difficulties downloading or printing this mandatory document, blank confirmation cards will be available in the lobby of the Health &amp; Fitness Expo for you to complete and turn in. Please bring photo ID
				[:es]Por favor revise su informaci&oacute;n e intente enviarlo de nuevo. En caso de tener problemas para descargar o imprimir este documento, habr&aacute; hojas de confirmaci&oacute;n disponibles en Expodepor-Feria del Corredor para que usted pueda completar y entregar. Por favor traiga su DNI con foto.
				[:pt]Por favor confirme as informa&ccedil;&otilde;es e tente submeter novamente.  Caso tenha dificuldade em descarregar ou imprimir este documento, vamos disponibilizar impressos em branco &agrave; entrada da  Feira do desporto e sa&uacute;de , para preencher e nos entregar. Por favor traga o seu documento de identifica&ccedil;&atilde;o com fotografia.
				[:fr]V&#233;rifiez les renseignements entr&#233;s et soumettrez votre demande de nouveau. Si vous ne parvenez pas &#224; t&#233;l&#233;charger ou &#224; imprimer ce document obligatoire, des feuilles de confirmation vierges sont disponibles dans le lobby de l'expo Sant&#233; et forme physique. Veuillez apporter une pi&#232;ce d'identit&#233; avec photo
				[:mx]Por favor revise que su informaci&oacute;n est&eacute; correcta e int&eacute;ntelo de nuevo. En caso de tener problemas para descargar o imprimir este documento, habr&aacute; hojas de confirmaci&oacute;n disponibles en Expodepor-Feria del Corredor para que usted pueda completar y entregar. Por favor traiga  una identificaci&oacute;n oficial con foto." );
			echo '</p>

			<div class="print_confirm_btn">
				<a class="try_again print_confirmation_sheet" href="'.$page_url.'">';
					_e( "[:en]Try Again
					[:es]Intentar de nuevo
					[:pt]Tente novamente
					[:fr]Veuillez r&#233;essayer
					[:mx]Intentar de nuevo" );
				echo '</a>
			</div>';
		}
	} else { ?>
		<h2>
			<?php
				_e( "[:en]Retrieve Confirmation Sheet ( required for packet pickup )
				[:es]Recuperar hoja de confirmaci&oacute;n ( necesario para recoger la bolsa del corredor )
				[:pt]Recuperar o comprovativo de inscri&ccedil;&atilde;o ( necess&aacute;rio para levantamento do dorsal )
				[:fr]R&#233;cup&#233;rer la feuille de confirmation
				[:mx]Imprimir hoja de confirmaci&oacute;n ( esta es necesario para recoger tu paquete de corredor )" );
			?>
		</h2>

		<?php if( $isInternational == 'on' || $isMexican == 'on' ) { ?>
			<div class="lang_chooser">
				<ul><?php if(  !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar()  ) : endif; ?></ul>
			</div>
		<?php } ?>

		<p>
			<?php
				_e( "[:en]Please complete the information below to retrieve your Confirmation Sheet. <strong>Please note: Confirmation sheets on mobile devices will  not be accepted. You must print your confirmation sheet.</strong>
				[:es]Por favor complete la informaci&oacute;n de abajo para recuperar su hoja de confirmaci&oacute;n. <strong>Nota: Las hojas de confirmaci&oacute;n de los dispositivos m&oacute;viles no ser&aacute;n aceptadas. Usted debe imprimir su hoja de confirmaci&oacute;n.</strong>
				[:pt]Por favor preencha as informa&ccedil;&otilde;es abaixo para recuperar o seu comprovativo de inscri&ccedil;&atilde;o. <strong>Nota: comprovativos apresentados em dispositivos m&oacute;veis n&atilde;o s&atilde;o aceites. Deve imprimir o comprovativo de inscri&ccedil;&atilde;o.</strong>
				[:fr]Veuillez entrer les renseignements ci-dessous afin de r&#233;cup&#233;rer la feuille de confirmation. <strong>Note : les feuilles de confirmation sur appareil mobile ne seront pas accept&#233;es. Veuillez imprimer votre feuille de confirmation.</strong>
				[:mx]Por favor complete la informaci&oacute;n de abajo para obtener su hoja de confirmaci&oacute;n. Nota: Las hojas de confirmaci&oacute;n de  dispositivos m&oacute;viles no ser&aacute;n aceptadas. Usted debe de imprimir su hoja de confirmaci&oacute;n." );
			?>
		</p>

		<?php
			if( get_post_meta( get_the_ID(), $prefix . 'database_current_to', true ) != null  ) {
				$db_current_to			= get_post_meta( get_the_ID(), $prefix . 'database_current_to', 1 );
				$db_current_to_convert	= date( "F j, Y", $db_current_to );

				if( $isInternational == 'on' ) {
					$db_current_to_convert = strftime( "%d %B %Y", $db_current_to );
				}

				$db_updated_on			= get_post_meta( get_the_ID(), $prefix . 'database_updated_on', 1 );
				$db_updated_on_convert	= date( "F j, Y", $db_updated_on );

				if( $isInternational == 'on' ) {
					$db_updated_on_convert = strftime( "%d %B %Y", $db_updated_on );
				}

				echo "<div class=\"attn_warning\"><p>";
					_e( "[:en]<strong>Please Note: </strong> If you registered after <strong>$db_current_to_convert</strong>, your Confirmation Sheet is not yet available. Please check back starting <strong>$db_updated_on_convert</strong>.
					[:es]<strong>NOTA IMPORTANTE:</strong> Si usted se registr&oacute; despu&eacute;s del <strong>$db_current_to_convert</strong>, su hoja de confirmaci&oacute;n no est&aacute; todav&iacute;a disponible. Por favor, vuelva a intentarlo a partir del <strong>$db_updated_on_convert</strong>
					[:pt]Aten&ccedil;&atilde;o: Se inscreveu-se ap&oacute;s <strong>$db_current_to_convert</strong>, o seu comprovativo ainda n&atilde;o est&aacute; dispon&iacute;vel. Por favor volte a consultar a partir de <strong>$db_updated_on_convert</strong>.
					[:fr]<strong>Veuillez noter:</strong> si vous vous &#234;tes inscrit apr&#232;s le <strong>$db_current_to_convert</strong>, votre feuille de confirmation n'est pas encore disponible. Veuillez v&#233;rifier de nouveau &#224; compter du <strong>$db_updated_on_convert</strong>.
					[:mx]NOTA IMPORTANTE: Si usted se registr&oacute; despu&eacute;s de esta fecha <strong>$db_current_to_convert</strong>, su hoja de confirmaci&oacute;n aun no esta disponible. Por favor, vuelva a intentarlo a partir de <strong>$db_updated_on_convert</strong>." );
				echo "</p></div>";
			}
		?>

		<form class="usr_conf" name="form" action="<?php echo get_permalink(); ?>" method="post" onsubmit="return checkform( this );">
			<?php if( $isMexican != 'on' ) { ?>
				<ol>
					<li>
						<label for="firstname">
							<?php
								_e( "[:en]First Name
								[:es]NOMBRE
								[:pt]Nome
								[:fr]Pr&#233;nom
								[:mx]NOMBRE" );
							 ?>
						</label>
						<input id="firstname" name="firstname" type="text" />
					</li>
					<li>
						<label for="lastname">
							<?php
								_e( "[:en]Last Name
								[:es]APELLIDOS
								[:pt]Apelido
								[:fr]Nom de famille
								[:mx]APELLIDOS" );
							 ?>
						</label>
						<input id="lastname" name="lastname" type="text" />
					</li>
					<li>
						<label for="birthdate">
							<?php
								_e( "[:en]Birthdate
								[:es]FECHA DE NACIMIENTO
								[:pt]Data de nascimento
								[:fr]Date de naissance
								[:mx]FECHA DE NACIMIENTO" );
							 ?>
						</label>
						<?php
							if( $isInternational == "on" ) {
								if( $lang2 == 'fr' ) {
									$day_var	= 'J';
									$month_var	= 'M';
									$year_var	= 'AAAA';
								} else {
									$day_var	= 'D';
									$month_var	= 'M';
									$year_var	= 'YYYY';
								}

								echo "<select name=\"dob_day\" type=\"text\" id=\"dob_day\">
									<option value=\"-1\">".$day_var."</option>\n";

									for ( $i = 1; $i <= 31; $i++ ) {
										echo "<option value=\"$i\"";

										if( $dob_day == $i ) {
											echo "selected ";
										};

										echo ">$i</option>\n";
									}
								echo "</select>\n";

								echo "<select name=\"dob_month\" type=\"text\" id=\"dob_month\">
									<option value=\"-1\">".$month_var."</option>\n";

									for ( $i = 1; $i <= 12; $i++ ) {
										echo "<option value=\"$i\"";

										if( $dob_month == $i ) {
											echo "selected ";
										};

										echo ">$i</option>\n";
									}
								echo "</select>\n";

								echo "<select name=\"dob_year\" type=\"text\" id=\"dob_year\">
									<option value = \"-1\">".$year_var."</option>\n";

									for ( $i = date( "Y" ); $i >= 1920; $i-- ) {
										echo "<option value=\"$i\"";

										if(  $dob_year == $i  ) {
											echo "selected ";
										} ;

										echo ">$i</option>\n";
									}
								echo "</select>\n";

							} else {
								echo "<select name=\"dob_month\" type=\"text\" id=\"dob_month\">
									<option value=\"-1\">M</option>\n";

									for ( $i = 1; $i <= 12; $i++ ) {
										echo "<option value=\"$i\"";

										if( $dob_month == $i ) {
											echo "selected ";
										};

										echo ">$i</option>\n";
									}
								echo "</select>\n";

								echo "<select name=\"dob_day\" type=\"text\" id=\"dob_day\">
									<option value=\"-1\">D</option>\n";

									for ( $i = 1; $i <= 31; $i++ ) {
										echo "<option value=\"$i\"";

										if( $dob_day == $i ) {
											echo "selected ";
										};

										echo ">$i</option>\n";
									}
								echo "</select>\n";

								echo "<select name=\"dob_year\" type=\"text\" id=\"dob_year\">
									<option value = \"-1\">YYYY</option>\n";

									for ( $i = date( "Y" ); $i >= 1920; $i-- ) {
										echo "<option value=\"$i\"";

										if(  $dob_year == $i  ) {
											echo "selected ";
										} ;

										echo ">$i</option>\n";
									}
								echo "</select>\n";
							}
						?>
					</li>
					<li>
						<input type="submit" name="submit" value="<?php _e( "[:en]Retrieve Confirmation Sheet[:es]Recuperar hoja de confirmaci&oacute;n[:pt]Recuperar o comprovativo de inscri&ccedil;&atilde;o[:fr]R&#233;cup&#233;rer la feuille de confirmation[:mx]Imprimir  hoja de confirmaci&oacute;n" ); ?>" />
					</li>
				</ol>

			<?php } else {

				// is mexican event ?>
				<ol>
					<li>
						<label for="firstname">
							<?php
								_e( "[:en]First Name
								[:es]NOMBRE
								[:mx]NOMBRE" );
							 ?>
						</label>
						<input id="firstname" name="firstname" type="text" />
					</li>
					<li>
						<label for="email">
							<?php
								_e( "[:en]Email
								[:es]Correo electr&#243;nico
								[:mx]Correo electr&#243;nico
								[:pt]Correio eletr&ocirc;nico" );
							 ?>
						</label>
						<input id="email" name="email" type="text" />
					</li>
					<li>
						<label for="dob_year">
							<?php
								_e( "[:en]Year of Birth
								[:es]A&ntilde;o de nacimiento
								[:mx]A&ntilde;o de nacimiento" );
							 ?>
						</label>
						<input id="dob_year" name="dob_year" type="text" />
					</li>
					<li>
						<input type="submit" name="submit" value="<?php _e( "[:en]Retrieve Confirmation Sheet[:es]Recuperar hoja de confirmaci&oacute;n[:pt]Recuperar o comprovativo de inscri&ccedil;&atilde;o[:fr]R&#233;cup&#233;rer la feuille de confirmation[:mx]Imprimir  hoja de confirmaci&oacute;n" ); ?>" />
					</li>
				</ol>

			<?php } ?>
		</form>
		<div id="conf_sheet_faq">
			<h2>
				<?php
					_e( "[:en]Confirmation Sheet FAQ
					[:es]Hoja de confirmaci&oacute;n FAQ
					[:pt]Comprovativo de inscri&ccedil;&atilde;o FAQ
					[:fr]FAQ - Feuille de confirmation
					[:mx]Hoja de confirmaci&oacute;n FAQ" );
				?>
			</h2>
			<p><strong>
				<?php
					_e( "[:en]Q. What if my confirmation sheet is blank?
					[:es]&#x00bf;Qu&eacute; pasa si mi hoja de confirmaci&oacute;n est&aacute; en blanco?
					[:pt]E se o meu comprovativo de inscri&ccedil;&atilde;o est&aacute; em branco?
					[:fr]Qu'arrive-t-il si la feuille de confirmation n'est pas remplie?
					[:mx]&iquest;Qu&eacute; pasa si mi hoja de confirmaci&oacute;n est&aacute; en blanco?" );
				?>
			</strong></p>
			<p>
				<?php
					_e( "[:en]A. Were you pulling up the Confirmation Sheet on a mobile device or AOL? Often times those devices/browsers do not support the file, so please attempt to use Internet Explorer or computer to open the document. If for whatever reason you are still unable to pull up your confirmation sheet and you are sure that you're registered ( either by an e-mailed receipt/confirmation or credit card statement/cleared check ), simply bring your photo ID with you to the expo to fill out a blank confirmation sheet to retrieve your race packet.
					[:es]&#x00bf;Est&aacute;s utilizando Firefox o AOL? Muchas veces los navegadores no soportan el archivo, as&iacute; que por favor intenta utilizar Internet Explorer para abrir el documento. Si por cualquier raz&oacute;n usted todav&iacute;a no puede verificar su hoja de confirmaci&oacute;n y  est&aacute; seguro de que usted est&aacute; registrado ( ya sea mediante un recibo por correo electr&oacute;nico / declaraci&oacute;n de confirmaci&oacute;n o tarjeta de cr&eacute;dito ) Simplemente identif&iacute;quese con su DNI en Expodepor-Feria del Corredor, y en ese mismo lugar tras hacer  las comprobaciones oportunas tendr&aacute; que rellenar una hoja de confirmaci&oacute;n para retirar su bolsa del corredor.
					[:pt]Est&aacute; a utilizar o Firefox ou AOL para obter o comprovativo de inscri&ccedil;&atilde;o? Muitas vezes estes navegadores n&atilde;o suportam o ficheiro, por favor tente usar o Internet Explorer para abrir o documento. Se por qualquer motivo mesmo assim n&atilde;o consegue obter o comprovativo, e tem a certeza de que est&aacute; inscrito ( seja porque recebeu um recibo por e-mail/comprovativo ou extracto de cart&atilde;o de cr&eacute;dito ), apresente o seu documento de identifica&ccedil;&atilde;o com fotografia na Expo e preencha o impresso em branco para levantar o seu dorsal.
					[:fr]Avez-vous extrait la feuille de confirmation en utilisant Firefox ou AOL ? Souvent, ces navigateurs ne peuvent prendre en charge ce fichier. Veuillez donc utiliser Explorer pour ouvrir le document. Si, pour quelque raison que ce soit, vous ne parvenez toujours pas &#224; r&#233;cup&#233;rer votre feuille de confirmation et que vous avez une preuve de votre inscription ( un courriel de confirmation ou le relev&#233; de votre carte de cr&#233;dit ) apportez une pi&#232;ce d'identit&#233; avec photo et vous pourrez remplir une nouvelle feuille de confirmation pour obtenir votre pochette de course.
					[:mx]&iquest;Est&aacute;s utilizando Firefox o AOL? Muchas veces los navegadores no soportan el archivo, as&iacute; que por favor intenta utilizando Internet Explorer para abrir el documento. Si por cualquier raz&oacute;n usted todav&iacute;a no puede verificar su hoja de confirmaci&oacute;n y  est&aacute; seguro de que usted est&aacute; registrado ( ya sea mediante un recibo por correo electr&oacute;nico / declaraci&oacute;n de confirmaci&oacute;n o tarjeta de cr&eacute;dito ) Simplemente identif&iacute;quese con una identificaci&oacute;n oficial  en Expodepor-Feria del Corredor, en este m&oacute;dulo se le pedir&aacute;n las comprobaciones necesaria  y tendr&aacute; que rellenar una hoja de confirmaci&oacute;n para retirar su paquete del corredor." );
				?>

			</p>
			<p><strong>
				<?php
					_e( "[:en]Q. What if it shows that I am Not found in the database?
					[:es]&#x00bf;Qu&eacute; pasa si mi hoja de confirmaci&oacute;n indica que no estoy  en la base de datos?
					[:pt]E se o meu comprovativo de inscri&ccedil;&atilde;o revela que eu n&atilde;o sou encontrado na base de dados?
					[:fr]Qu'arrive-t-il si ma feuille de confirmation indique que mon nom ne figure pas dans la base de donn&#233;es?
					[:mx]&iquest;Qu&eacute; pasa si mi hoja de confirmaci&oacute;n indica que no estoy  en la base de datos?" );
				?>
			</strong></p>
			<p>
				<?php
					_e( "[:en]A. Please put in your information exactly as it shows on your receipt/confirmation that you received when you originally registered. Often times, names are entered backwards or we have an incorrect date of birth for you. If you can confirm your registration ( either by an e-mailed receipt/confirmation or credit card statement/cleared check ) and there is a typo with your name or date of birth, please be sure to visit the Solutions table at the Health and Fitness Expo and we will be able to change your information in the system. We will have blank confirmation sheets for you to fill out at the expo to retrieve your race packet, make sure that you bring your Photo ID.
					[:es]Por favor, ponga su informaci&oacute;n tal y como se muestra en el recibo / confirmaci&oacute;n que recibi&oacute; cuando se registr&oacute; originalmente. Muchas veces, los nombres se introducen mal o la fecha de nacimiento no coincide. Si usted puede confirmar su registro ( ya sea mediante un recibo por correo electr&oacute;nico / declaraci&oacute;n de confirmaci&oacute;n o tarjeta de cr&eacute;dito / cheque cobrado ) y hay un error con su nombre o fecha de nacimiento, por favor no deje de visitar la mesa de Soluciones en Expodepor-Feria del Corredor y sin  ning&uacute;n problema solucionaremos las incidencias. No olvide facilitar su DNI para que en caso de incidencia, usted pueda rellenar su hoja de confirmaci&oacute;n en la feria antes mencionada y realizar su tr&aacute;mite.
					[:pt]Por favor preencha a sua informa&ccedil;&atilde;o exactamente como estava no seu recibo/comprovativo de inscri&ccedil;&atilde;o, que recebeu quando se inscreveu. Muitas vezes os nomes s&atilde;o inseridos ao contr&aacute;rio ou temos sua data de nascimento errada. Se consegue comprovar que se encontra inscrito ( seja porque recebeu um recibo por e-mail/comprovativo ou extracto de cart&atilde;o de cr&eacute;dito ), ou existe um erro no seu nome ou data de nascimento, n&atilde;o deixe de visitar a Expo de desporto e sa&uacute;de, onde podemos alterar as suas informa&ccedil;&otilde;es no sistema. Iremos ter impressos em branco para voc&ecirc; preencher e assim poder levantar o seu kit de participa&ccedil;&atilde;o. N&atilde;o se esque&ccedil;a de levar o seu cart&atilde;o de identifica&ccedil;&atilde;o com fotografia.
					[:fr]Veuillez inscrire les renseignements exactement comme ils apparaissent sur la confirmation que vous avez re&#231;ue lorsque vous vous &#234;tes inscrit. Il arrive fr&#233;quemment que les noms et pr&#233;nom aient &#233;t&#233; invers&#233;s, ou que votre date de naissance soit incorrecte dans nos dossiers. Si vous pouvez d&#233;montrer que vous &#234;tes inscrit ( soit au moyen du courriel de confirmation ou du relev&#233; de votre carte de cr&#233;dit ) et que votre nom ou votre date de naissance y figure, veuillez vous rendre &#224; la table Solutions &#224; l'expo Sant&#233; et forme physique pour que nous puissions corriger les renseignements dans notre syst&#232;me. Nous vous remettrons une nouvelle feuille de confirmation que vous pourrez remplir pour obtenir votre pochette de course. Assurez-vous d'avoir une pi&#232;ce d'identit&#233; avec photo.
					[:mx]Por favor, ponga su informaci&oacute;n tal y como se muestra en el recibo / confirmaci&oacute;n que recibi&oacute; cuando se registr&oacute; originalmente. Muchas veces, los nombres se introducen mal o la fecha de nacimiento no coincide. Si usted puede confirmar su registro ( ya sea mediante un recibo por correo electr&oacute;nico / declaraci&oacute;n de confirmaci&oacute;n o tarjeta de cr&eacute;dito / cheque cobrado ) y hay un error con su nombre o fecha de nacimiento, por favor no deje de visitar la mesa de Soluciones en Expodepor-Feria del Corredor y sin ning&uacute;n problema solucionaremos los errores. No olvide facilitar una credencial oficial  para que en caso de incidencia, usted pueda rellenar su hoja de confirmaci&oacute;n en la feria antes mencionada y realizar su tr&aacute;mite." );
				?>

			</p>
		</div>
		<script type="text/javascript">
			function checkform( form ) {
				var fname		= document.getElementById( 'firstname' ).value;
				var lname		= document.getElementById( 'lastname' ).value;
				var dobmonth	= document.getElementById( 'dob_month' ).value;
				var dobday		= document.getElementById( 'dob_day' ).value;
				var dobyear		= document.getElementById( 'dob_year' ).value;

				if( !fname ) {
					alert( "<?php _e( '[:en]Please enter a first name[:es]Por favor, introduzca un nombre[:pt]Por favor insira um nome[:fr]S\'il vous pla&#238;t, entrez un pr&#233;nom[:mx]Por favor ingresa tu nombre' ); ?>" );
					return false;
				}

				if( !lname ) {
					alert( "<?php _e( '[:en]Please enter a last name[:es]Por favor, introduzca un apellido[:pt]Por favor insira um sobrenome[:fr]S\'il vous pla&#238;t entrer un nom de famille[:mx]Por favor ingresa tu apellido' ); ?>" );
					return false;
				}

				if( dobmonth == "-1" || dobday == "-1" || dobyear == "-1" )  {
					alert( "<?php _e( '[:en]Please enter your complete birthday[:es]Por favor, introduzca su fecha de nacimiento completa[:pt]Digite seu anivers\xE1rio completo[:fr]S\'il vous pla&#238;t entrer votre enti&#232;re anniversaire[:mx]Por favor ingresa tu fecha de nacimiento' ); ?>" );
					return false;
				} else {
					document.form.submit();
				}
			}
		</script>
	<?php }
get_footer(); ?>
