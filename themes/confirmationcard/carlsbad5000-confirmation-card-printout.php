<?php
	header( 'P3P: CP="CAO PSA OUR"' );
	session_start(); // start up your PHP session!
	require_once( "../../../wp-load.php" );
	global $wpdb;

	$post_id	= $_SESSION['post_id'];
	$count		= $_REQUEST['count'];

	$first_name		= addslashes( $_SESSION['first_name'] );
	$last_name		= addslashes( $_SESSION['last_name'] );
	$division		= $_SESSION['raceday'];
	$last_name_var	= '%' . $last_name;
	$birthday		= $_SESSION['birthdate'];

	$racer = $wpdb->get_results(
		$wpdb->prepare( "SELECT last_name, first_name, birthdate, bib_number, division FROM wp_cbad_confirmation_card_data WHERE birthdate = %s AND first_name = %s AND last_name LIKE %s", $birthday, $first_name, $last_name_var )
	);

	$racenumber_pickup_1			= get_post_meta( $post_id, '_cbad5000_racenumber_pickup_1', true );
	$racenumber_pickup_1_convert	= date( "l, F j, Y", $racenumber_pickup_1 );
	$racenumber_pickup_2			= get_post_meta( $post_id, '_cbad5000_racenumber_pickup_2', true );
	$racenumber_pickup_2_convert	= date( "l, F j, Y", $racenumber_pickup_2 );

	$start_times		= get_post_meta( $post_id, '_cbad5000_start_times', true );
	$course_map			= get_post_meta( $post_id, '_cbad5000_course_map', true );
	$timerange1			= get_post_meta( $post_id, '_cbad5000_time_range_1', true );
	$pickup_location1	= get_post_meta( $post_id, '_cbad5000_pickup_1_location', true );
	$timerange2			= get_post_meta( $post_id, '_cbad5000_time_range_2', true );
	$pickup_location2	= get_post_meta( $post_id, '_cbad5000_pickup_2_location', true );

	get_header(); ?>

	<script type="text/javascript">
		function printPage() {
			if (window.print)
				window.print()
			else
				alert("Sorry, your browser doesn't support this feature.\n\nPlease use the print button on your browser instead.");
		}
	</script>

	<?php if( !isset( $_SESSION['post_id'] ) ) {
		echo '<div class="cbad_form_found">
			<p>Session expired. Please try again!</p>
		</div>';
	} else { ?>

		<div id="print_this">
			<a href="#" onclick="javascript:printPage()">Click Here To Print This Page</a>
		</div>

		<?php if( count( $racer ) > 1 ) {
			$count = 0;

			foreach( $racer as $racer_event ) {
				if( $count == ( ( count( $racer ) ) - 1 ) ) {
					$pagebreak = '';
				} else {
					$pagebreak = 'style="page-break-after:always;"';
				} ?>

				<div class="cbad_form_found" <?php echo $pagebreak; ?>>
					<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/cbad_15_logo.jpg" class="alignleft" alt="Carlsbad 5000" />
					<h1>Race Number Confirmation</h1>
					<p>You MUST present this printed page and a photo identification IN PERSON to pick up your race number. </p>

					<div class="clear"></div>

					<p class="racer_info">
						<span class="racer_info_label">Name:</span> <?php echo $racer_event->first_name . " " . $racer_event->last_name; ?>
					</p>

					<p class="racer_info">
						<span class="racer_info_label">Race Number:</span> <?php echo $racer_event->bib_number; ?>
					</p>
					<p class="divider">At packet pick-up, please line up under the numerical range sign corresponding to your race number.</p>

					<img src="<?php echo $course_map; ?>" class="alignright" alt="Carlsbad 5000 Course Map" />

					<h2>Saturday Race Number Pickup:</h2>

					<h3><?php echo $racenumber_pickup_1_convert; ?>: <?php echo $timerange1; ?></h3>
					<ul>
						<li><strong>Location:</strong> <?php echo $pickup_location1; ?></li>
					</ul>

					<h2>Sunday Race Number Pickup:</h2>

					<h3><?php echo $racenumber_pickup_2_convert; ?>: <?php echo $timerange2; ?></h3>
					<ul>
						<li><strong>Location:</strong> <?php echo $pickup_location2; ?></li>
					</ul>

					<h2>Start Times:</h2>

					<h3><?php echo $racenumber_pickup_2_convert; ?></h3>
					<?php
						if( !empty( $start_times ) ) {
							echo '<ul>';
								foreach( $start_times as $key => $entry ) {
									echo '<li>
										<strong>'. $entry['start_time_time'] .'</strong> - '. $entry['start_time_name'] .'
									</li>';
								}
							echo '</ul>';
						}
					?>
				</div>

				<?php $count++;
			}
		} else { ?>
			<div class="cbad_form_found">
				<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/cbad_15_logo.jpg" class="alignleft" alt="Carlsbad 5000" />
				<h1>Race Number Confirmation</h1>
				<p>You MUST present this printed page and a photo identification IN PERSON to pick up your race number. </p>

				<div class="clear"></div>

				<p class="racer_info">
					<span class="racer_info_label">Name:</span> <?php echo $racer[0]->first_name . " " . $racer[0]->last_name; ?>
				</p>

				<p class="racer_info">
					<span class="racer_info_label">Race Number:</span> <?php echo $racer[0]->bib_number; ?>
				</p>
				<p class="divider">At packet pick-up, please line up under the numerical range sign corresponding to your race number.</p>

				<img src="<?php echo $course_map; ?>" class="alignright" alt="Carlsbad 5000 Course Map" />

				<h2>Saturday Race Number Pickup:</h2>

				<h3><?php echo $racenumber_pickup_1_convert; ?>: <?php echo $timerange1; ?></h3>
				<ul>
					<li><strong>Location:</strong> <?php echo $pickup_location1; ?></li>
				</ul>

				<h2>Sunday Race Number Pickup:</h2>

				<h3><?php echo $racenumber_pickup_2_convert; ?>: <?php echo $timerange2; ?></h3>
				<ul>
					<li><strong>Location:</strong> <?php echo $pickup_location2; ?></li>
				</ul>

				<h2>Start Times:</h2>

				<h3><?php echo $racenumber_pickup_2_convert; ?></h3>
				<?php
					if( !empty( $start_times ) ) {
						echo '<ul>';
							foreach( $start_times as $key => $entry ) {
								echo '<li>
									<strong>'. $entry['start_time_time'] .'</strong> - '. $entry['start_time_name'] .'
								</li>';
							}
						echo '</ul>';
					}
				?>
			</div>

		<?php }

		get_footer();
	}

session_destroy(); ?>