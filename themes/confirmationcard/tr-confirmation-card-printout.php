<?php
  header('P3P: CP="CAO PSA OUR"');
	session_start(); // start up your PHP session!
  require_once("../../../wp-load.php");
  global $wpdb;

  $first_name = $_SESSION['first_name'];
  $last_name = $_SESSION['last_name'];
  $birthday = $_SESSION['birthdate'];
  $event = $_SESSION['event'];
  $post_id = $_SESSION['post_id'];

  //echo $first_name . $last_name . $birthday . $event;

  $query = "SELECT last_name, first_name, address_1, address_2, city, state, zip, email, phone, gender, birthdate, age, division, bib_number, wave, membership, event FROM wp_tr_confirmation_card_data WHERE birthdate = '$birthday' AND first_name = '$first_name' AND last_name = '$last_name' AND event LIKE '%$event%'";
  // echo $query;
  $racer = $wpdb->get_row($query);
?>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"> 
 <html xmlns="http://www.w3.org/1999/xhtml">
  <head profile="http://gmpg.org/xfn/11">
  <title>
    <?php echo get_post_meta ($post_id, '_trirock_full_event_name', true) . " Confirmation Sheet"; ?>
  </title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/confcard.css" />
  <script type="text/javascript">
    function printPage() {
      if (window.print)
        window.print()
      else
        alert("Sorry, your browser doesn't support this feature.\n\nPlease use the print button on your browser instead.");
    }
  </script>
 </head>

 <body>
  <div id="print_this">
    <a href="#" onclick="javascript:printPage()">Click Here To Print This Page</a>
  </div>
  <div id="wrapper">
    <div id="header_text">
      <h1>
        <?php echo get_post_meta ($post_id, '_trirock_full_event_name', true); ?><br />
        Packet Pickup Confirmation Sheet
      </h1>
      <p><strong><?php echo get_post_meta ($post_id, '_trirock_confirmation_message', true); ?></strong></p>
    </div>
    <div class="group">
      <ol id="tr_raceday_info">
        <li class="first_row">Your Wave Number is: <span class="result"><?php echo $racer->wave; ?></span></li>
        <li class="first_row rightalign">Your Race Number is: <span class="result"><?php echo $racer->bib_number; ?></span></li>
        <li class="second_row">Your Gender is: <span class="result"><?php echo $racer->gender; ?></span></li>
        <li class="second_row centeralign">Your Age is: <span class="result"><?php echo $racer->age; ?></span></li>
        <li class="second_row rightalign">Your Division is: <span class="result"><?php echo $racer->division; ?></span></li>
      </ol>
    </div>
    <div class="group">
      <h2 class="title_underline">Name/Address</h2>
      <ol>
        <li class="left_column">
          <p class="result">
            <?php
              echo $racer->first_name . " " . $racer->last_name . "<br />" .
              $racer->address_1 . "<br />";
              if ( $racer->address_2 ) {
                echo $racer->address_2 . "<br />";
              }
              echo $racer->city . ", " . $racer->state . "<br />" .
              $racer->zip;
            ?>
          </p>
        </li>
        <li class="right_column">
          <ol>
            <li>USAT Membership: <span class="result"><?php echo $racer->membership; ?></span></li>
            <li>Email: <span class="result"><?php echo $racer->email; ?></span></li>
            <li>Phone: <span class="result"><?php echo $racer->phone; ?></span></li>
            <li>DOB: <span class="result"><?php echo $racer->birthdate; ?></span></li>
          </ol>
        </li>
      </ol>
    </div>
    <div class="group">
      <div id="waiver_text">
        <h2><?php echo get_post_meta ($post_id, '_pre_trirock_waiver_header', true); ?></h2>
        <p><?php echo get_post_meta ($post_id, '_trirock_waiver_text', true); ?></p>
      </div>
    </div>
    <ol id="signature">
      <li>Signature of Athlete</li>
      <li>(Signature of parent if under 18 years)</li>
      <li class="signature_date">Date</li>
    </ol>
    <div class="separator"></div>

    <p class="minor_disclaimer"><?php echo get_post_meta ($post_id, '_trirock_minor_waiver_text', true); ?></p>
  </form>
 </body>
</html>
<?php session_destroy(); ?>