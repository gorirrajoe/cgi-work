<?php
/* Template Name: Women's Running Series */

  header('P3P: CP="CAO PSA OUR"');
  session_start(); // start up your PHP session!

  global $wpdb;
  
  $pageurl = get_permalink();

  /* Get the current post ID. */
  $post_id = get_the_ID();

  $slug = basename(get_permalink());

  $event = get_post_meta ($post_id, '_wrs_event_code', true);

  $url_vars = parse_url($pageurl);
  // echo $url_vars[host];
  if ($url_vars[host] == 'confcard.lan') {  // dev environment
    $parent_url = 'http://womenshalfmarathon.lan/';
  } elseif ($url_vars[host] == 'confirmation.competitor.com') { // production environment
    $parent_url = 'http://womenshalf.competitor.com/';
  }

  get_header();


  if(isset($_POST['submit'])) {
    // Define form variables, if isset, if not default
    if(isset($_POST['firstname'])) {
      $first_name = $_POST['firstname'];
    } else {
      $first_name = "";
    }
    if(isset($_POST['lastname'])){
      $last_name = $_POST['lastname'];
    } else {
      $last_name = "";
    }
    if(isset($_POST['dob_month'])){
      $dob_month = $_POST['dob_month'];
    } else {
      $dob_month = "";
    }
    if(isset($_POST['dob_day'])){
      $dob_day = $_POST['dob_day'];
    } else {
      $dob_day = "";
    }
    if(isset($_POST['dob_year'])){
      $dob_year = $_POST['dob_year'];
    } else {
      $dob_year = "";
    }
    $birthday = $dob_month . "/" . $dob_day . "/" . $dob_year;

    /*
    echo $first_name . "<br />";
    echo $last_name . "<br />";
    echo $birthday ."<br />";
    echo $event;
    */

    $last_name_var = '%' . $last_name;
    $event_var = '%' . $event . '%';
    $racer = $wpdb->get_row(
      $wpdb->prepare("SELECT last_name, first_name, birthdate, event_code FROM wp_wr_confirmation_card_data WHERE birthdate = %s AND first_name = %s AND last_name LIKE %s AND event_code LIKE %s", $birthday, $first_name, $last_name_var, $event_var));

    // print_r($racer);
    $_SESSION['last_name'] = $racer->last_name;
    $_SESSION['first_name'] = $racer->first_name;
    $_SESSION['birthdate'] = $racer->birthdate;
    $_SESSION['event'] = $event;
    $_SESSION['post_id'] = $post_id;

    /*
    echo $_SESSION['last_name'] . "<br />";
    echo $_SESSION['first_name'] . "<br />";
    echo $_SESSION['birthdate'] ."<br />";
    */

    $page_url = site_url() . "/" . $permalink_slug . "/" . $slug;

    if ($racer != NULL ) {
      $event_slug = $permalink_slug;
      $stylesheet_directory = get_bloginfo("stylesheet_directory");
      $conf_card_printout = $stylesheet_directory . "/wrs-confirmation-card-printout.php";

      echo '<h2>Confirmation Sheet Found!</h2>
      <p>Your participant information was located in our system. Please press the button below to retrieve a printable version of your Confirmation Sheet</p>';

      $guid_value = guid();

      $wrs_return_url = get_post_meta ($post_id, '_wrs_return_url', true);
      if($wrs_return_url != '') {
        $event_home = $wrs_return_url;
      } else {
        $event_home = $parent_url.$slug;
      }

      echo '<div class="print_confirm_btn">
        <a class="print_confirmation_sheet" href="'.$event_home.'" target="_parent" onclick="window.open(\''.$conf_card_printout.'?result='.$guid_value.'\', \'confirmation\', \'toolbar=yes, location=yes, directories=yes, status=yes, menubar=yes, scrollbars=yes, copyhistory=yes, resizable=yes\')">Print Confirmation Sheet</a></div>';
    } else {
      echo '<h2>No Matching Participant Found</h2>
      <p>Please double check your information and try submitting again. In case you have difficulties downloading or printing this mandatory document, blank confirmation cards will be available in the lobby of the Health &amp; Fitness Expo for you to complete and turn in. Please bring photo ID</p>
      <div class="print_confirm_btn"><a class="try_again print_confirmation_sheet" href="'.$page_url.'">Try Again</a></div>';
    }
  } else {
      ?>

      <h2>Retrieve Confirmation Sheet (required for packet pickup)</h2>
      <p>Please complete the information below to retrieve your Confirmation Sheet. <strong>Please note: Confirmation sheets on mobile devices will  not be accepted. You must print your confirmation sheet.</strong></p>
      <?php
        if (get_post_meta ($post_id, '_wrs_database_current_to', true) != null ) {
          $db_current_to = strtotime(get_post_meta ($post_id, '_wrs_database_current_to', true));
          $db_current_to_convert = date("F j, Y", $db_current_to);

          $db_updated_on = strtotime(get_post_meta ($post_id, '_wrs_database_updated_on', true));
          $db_updated_on_convert = date("F j, Y", $db_updated_on);

          echo "<div class=\"attn_warning\"><p><strong>Please Note: </strong> If you registered after <strong>$db_current_to_convert</strong>, your Confirmation Sheet is not yet available. Please check back starting <strong>$db_updated_on_convert</strong>.</p></div>";
        }
      ?>

      <form class="usr_conf" name="form" action="<?php echo get_permalink(); ?>" method="post" onsubmit="return checkform(this);">
        <ol>
          <li>
            <label for="firstname">First Name</label>
            <input id="firstname" name="firstname" type="text" />
          </li>
          <li>
            <label for="lastname">Last Name</label>
            <input id="lastname" name="lastname" type="text" />
          </li>
          <li>
            <label for="birthdate">Birthdate</label>
            <?php
                echo "<select name=\"dob_month\" type=\"text\" id=\"dob_month\">\n";
                echo "<option value=\"-1\">M</option>\n";
                for ($i = 1; $i <= 12; $i++) {
                  echo "<option value = $i ";
                  if ($dob_month == $i) {
                    echo "selected ";	
                  };
                  echo ">$i</option>\n";
                }
                echo "</select>\n";

                echo "<select name=\"dob_day\" type=\"text\" id=\"dob_day\">\n";
                echo "<option value=\"-1\">D</option>\n";
                for ($i = 1; $i <= 31; $i++) {
                  echo "<option value = $i ";
                  if ($dob_day == $i) {
                    echo "selected ";
                  };
                  echo ">$i</option>\n";
                }
                echo "</select>\n";
                
                echo "<select name=\"dob_year\" type=\"text\" id=\"dob_year\">\n";
                echo "<option value = \"-1\">YYYY</option>\n";
                for ($i = date("Y"); $i >= 1920; $i--) {
                  echo "<option value = $i ";
                  if ( $dob_year == $i ) {
                    echo "selected ";
                  } ;
                  echo ">$i</option>\n";
                }
                echo "</select>\n";
            ?>
          </li>
          <li>
            <input type="submit" name="submit" value="Retrieve Confirmation Sheet" />
          </li>
        </ol>
      </form>
      <div id="conf_sheet_faq">
        <h2>Confirmation Sheet FAQ</h2>
        <p><strong>Q. What if my confirmation sheet is blank?</strong></p>
        <p>A. Were you pulling up the Confirmation Sheet in Firefox or AOL? Often times those browsers do not support the file, so please attempt to use Internet Explorer to open the document. If for whatever reason you are still unable to pull up your confirmation sheet and you are sure that you're registered (either by an e-mailed receipt/confirmation or credit card statement/cleared check), simply bring your photo ID with you to the expo to fill out a blank confirmation sheet to retrieve your race packet.</p>
        <p><strong>Q. What if it shows that I am Not found in the database?</strong></p>
        <p>A. Please put in your information exactly as it shows on your receipt/confirmation that you received when you originally registered. Often times, names are entered backwards or we have an incorrect date of birth for you. If you can confirm your registration (either by an e-mailed receipt/confirmation or credit card statement/cleared check) and there is a typo with your name or date of birth, please be sure to visit the Solutions table at the Health and Fitness Expo and we will be able to change your information in the system. We will have blank confirmation sheets for you to fill out at the expo to retrieve your race packet, make sure that you bring your Photo ID.</p>
      </div>
      <script type="text/javascript">
        function checkform(form) {
          var fname = document.getElementById('firstname').value;
          var lname = document.getElementById('lastname').value;
          var dobmonth = document.getElementById('dob_month').value;
          var dobday = document.getElementById('dob_day').value;
          var dobyear = document.getElementById('dob_year').value;
          if (!fname) {
            alert("Please enter a first name");
            return false;
          }
          if (!lname) {
            alert("Please enter a last name");
            return false;
          }
          if (dobmonth == "-1" || dobday == "-1" || dobyear == "-1")  {
            alert("Please enter your complete birthday");
            return false;
          } else {
            document.form.submit();
          }
        }
      </script>
      <?php } ?>
      <?php // end code ?>



<?php get_footer(); ?>
