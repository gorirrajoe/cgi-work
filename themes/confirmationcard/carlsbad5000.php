<?php
/* Template Name: Carlsbad 5000 */
	header( 'P3P: CP="CAO PSA OUR"' );
	session_start(); // start up your PHP session!

	global $wpdb;

	get_header(); 

	if( isset( $_POST['submit'] ) ) {
		/**
		 * Define form variables, if isset, if not default
		 */
		$first_name	= ( isset( $_POST['firstname'] ) ) ? $_POST['firstname'] : '';
		$last_name	= ( isset( $_POST['lastname'] ) ) ? $_POST['lastname'] : '';
		$dob_month	= ( isset( $_POST['dob_month'] ) ) ? $_POST['dob_month'] : '';
		$dob_day	= ( isset( $_POST['dob_day'] ) ) ? $_POST['dob_day'] : '';
		$dob_year	= ( isset( $_POST['dob_year'] ) ) ? $_POST['dob_year'] : '';
		$birthday	= $dob_month . "/" . $dob_day . "/" . $dob_year;

		/*
		if( isset( $_POST['firstname'] ) ) {
			$first_name = $_POST['firstname'];
		} else {
			$first_name = "";
		}
		if( isset( $_POST['lastname'] ) ){
			$last_name = $_POST['lastname'];
		} else {
			$last_name = "";
		}
		if( isset( $_POST['dob_month'] ) ){
			$dob_month = $_POST['dob_month'];
		} else {
			$dob_month = "";
		}
		if( isset( $_POST['dob_day'] ) ){
			$dob_day = $_POST['dob_day'];
		} else {
			$dob_day = "";
		}
		if( isset( $_POST['dob_year'] ) ){
			$dob_year = $_POST['dob_year'];
		} else {
			$dob_year = "";
		}

		echo $first_name . "<br />";
		echo $last_name . "<br />";
		echo $birthday ."<br />";
		echo $event;
		*/

		$last_name_var = '%' . $last_name;
		$event_var = '%' . $event . '%';

		$racer = $wpdb->get_results( 
			$wpdb->prepare( "SELECT last_name, first_name, birthdate, bib_number, division, event_code FROM wp_cbad_confirmation_card_data WHERE birthdate = %s AND first_name = %s AND last_name LIKE %s AND event_code LIKE %s", $birthday, $first_name, $last_name_var, $event_var )
		);

		if( !isset( $racer->bib_number ) ) {
			$err = '<h2>No Matching Participant Found</h2>
			<p>'.$first_name.' '.$last_name.' with a birthday of '.$birthday.' was not found in our database.</p>
			<p>Please double check your information and try submitting again.</p>';
		}

		$page_url = site_url() . "/" . $permalink_slug . "/" . $slug;
	}

	/**
	 * runner found!
	 */
	if( $racer != NULL	) {
		$stylesheet_directory = get_bloginfo( "stylesheet_directory" );
		$conf_card_printout = $stylesheet_directory . "/carlsbad5000-confirmation-card-printout.php"; ?>

		<style type="text/css">
			body {
				background-image:none;
				background-color:#fff;
			}
		</style>
		<script type="text/javascript">
			function printPage() {
				if( window.print )
					window.print()
				else
					alert( "Sorry, your browser doesn't support this feature.\n\nPlease use the print button on your browser instead." );
			}
		</script>

		<?php
			echo '<h2>';
				if( count( $racer ) > 1 ) { // plural
					echo 'Confirmation Sheets Found!';
				} else {  // singular
					echo 'Confirmation Sheet Found!';
				}
			echo '</h2>

			<p>Your participant information was located in our system. Please press the button below to retrieve a printable version of your Confirmation Sheet</p>';

			$count = 0;

			if( count( $racer ) > 1 ) {
				/**
				 * multiple events for this runner
				 */
				foreach( $racer as $racer_event ) {
					// resetting vars so they are typed as they appear in the database
					$guid_value					= guid();
					$_SESSION['first_name']		= $racer_event->first_name;
					$_SESSION['last_name']		= $racer_event->last_name;
					$_SESSION['race_number']	= $racer_event->bib_number;
					$_SESSION['raceday']		= $racer_event->division;
					$_SESSION['post_id']		= get_the_ID();
					$_SESSION['birthdate']		= $racer_event->birthdate;
					$count++;

				}

				echo '<div class="print_confirm_btn">
					<a class="print_confirmation_sheet" target="_blank" href="'. $conf_card_printout .'?count='. $count .'&result='. $guid_value .'">Print Confirmation Sheet</a>
				</div>';

			} else {
				// resetting vars so they are typed as they appear in the database
				$guid_value					= guid();
				$_SESSION['first_name']		= $racer[0]->first_name;
				$_SESSION['last_name']		= $racer[0]->last_name;
				$_SESSION['race_number']	= $racer[0]->bib_number;
				$_SESSION['raceday']		= $racer[0]->division;
				$_SESSION['post_id']		= get_the_ID();
				$_SESSION['birthdate']		= $racer[0]->birthdate;

				echo '<div class="print_confirm_btn">
					<a class="print_confirmation_sheet" target="_blank" href="'. $conf_card_printout .'?result='. $guid_value .'">Print Confirmation Sheet</a>
				</div>';

			}
		?>
	<?php }


	/**
	 * runner NOT found
	 */
	else {
		if( $err ) {
			echo '<div class="attn_warning">'. $err .'</div>';
		} ?>
		<div class="cbad_form_group">
			<p><strong>Please enter your first, last name and birthdate used when you registered for the Carlsbad 5000 to retrieve your Race Number.</strong></p>
			<form class="usr_conf" name="form" action="<?php echo get_permalink(); ?>" method="post" onsubmit="return checkform( this );">
				<ol>
					<li>
						<label for="firstname">First Name</label>
						<input id="firstname" name="firstname" type="text" />
					</li>
					<li>
						<label for="lastname">Last Name</label>
						<input id="lastname" name="lastname" type="text" />
					</li>
					<li>
						<label for="birthdate">Birthdate</label>
						<?php
							echo "<select name=\"dob_month\" type=\"text\" id=\"dob_month\">
								<option value=\"-1\">M</option>\n";
								for ( $i = 1; $i <= 12; $i++ ) {
									echo "<option value = $i ";
										if( $dob_month == $i ) {
											echo "selected ";
										}
									echo ">$i</option>\n";
								}
							echo "</select>
							
							<select name=\"dob_day\" type=\"text\" id=\"dob_day\">
								<option value=\"-1\">D</option>\n";
									for ( $i = 1; $i <= 31; $i++ ) {
										echo "<option value = $i ";
											if( $dob_day == $i ) {
												echo "selected ";
											}
										echo ">$i</option>\n";
									}
							echo "</select>
							
							<select name=\"dob_year\" type=\"text\" id=\"dob_year\">
								<option value = \"-1\">YYYY</option>\n";
								for ( $i = date( "Y" ); $i >= 1920; $i-- ) {
									echo "<option value = $i ";
										if(	$dob_year == $i	) {
											echo "selected ";
										}
									echo ">$i</option>\n";
								}
							echo "</select>\n";
						?>
					</li>
					<li>
						<input type="submit" name="submit" value="Retrieve Race Number" />
					</li>
				</ol>
			</form>
		</div>

		<script type="text/javascript">
			function checkform( form ) {
				var fname = document.getElementById( 'firstname' ).value;
				var lname = document.getElementById( 'lastname' ).value;
				var dobmonth = document.getElementById( 'dob_month' ).value;
				var dobday = document.getElementById( 'dob_day' ).value;
				var dobyear = document.getElementById( 'dob_year' ).value;
				if( !fname ) {
					alert( "<?php _e( '[:en]Please enter a first name[:es]Por favor, introduzca un nombre[:pt]Por favor insira um nome' ); ?>" );
					return false;
				}
				if( !lname ) {
					alert( "<?php _e( '[:en]Please enter a last name[:es]Por favor, introduzca un apellido[:pt]Por favor insira um sobrenome' ); ?>" );
					return false;
				}
				if( dobmonth == "-1" || dobday == "-1" || dobyear == "-1" )	{
					alert( "<?php _e( '[:en]Please enter your complete birthday[:es]Por favor, introduzca su fecha de nacimiento completa[:pt]Digite seu anivers\xE1rio completo' ); ?>" );
					return false;
				} else {
					document.form.submit();
				}
			}
		</script>
	<?php }

get_footer(); ?>