<?php
	header( 'P3P: CP="CAO PSA OUR"' );
	session_start(); // start up your PHP session!
	require_once( "../../../wp-load.php" );
	global $wpdb;

	$event		= $_SESSION['event'];
	$lang		= $_REQUEST['language'];
	$post_id	= $_SESSION['post_id'];
	$count		= $_REQUEST['count'];
	$lang2		= substr( $_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 5 );
	// print_r( $_SESSION );
	// echo $lang;

	if( $count > 0 ) {

		for( $i = 0; $i < $count; $i++ ) {

			$first_name	= addslashes( $_SESSION['first_name_'.$i] );
			$last_name	= addslashes( $_SESSION['last_name_'.$i] );
			$birthday	= $_SESSION['birthdate_'.$i];
			$division	= $_SESSION['division_'.$i];

			${'query_' . $i} = "SELECT last_name, first_name, address_1, address_2, city, state, zip, country, gender, birthdate, age, predicted, bib_number,	corral, event_code, division FROM wp_confirmation_card_data WHERE birthdate = '$birthday' AND first_name = '$first_name' AND last_name = '$last_name' AND event_code LIKE '%$event%' AND division LIKE '%$division%'";
			${'racer_'.$i} = $wpdb->get_row( ${'query_'.$i} );
		}

	} else {

		$first_name	= addslashes( $_SESSION['first_name'] );
		$last_name	= addslashes( $_SESSION['last_name'] );
		$birthday	= $_SESSION['birthdate'];
		$division	= trim( $_SESSION['division'] );

		$query = "SELECT last_name, first_name, address_1, address_2, city, state, zip, country, gender, birthdate, age, predicted, bib_number,	corral, event_code, division FROM wp_confirmation_card_data WHERE birthdate = '$birthday' AND first_name = '$first_name' AND last_name = '$last_name' AND event_code LIKE '%$event%' AND division LIKE '%$division%'";
		$racer = $wpdb->get_row( $query );

	}
	// echo $first_name . $last_name . $birthday . $event . $division;


	/**
	 * languages
	 */
	switch ( $lang ) {
		case "es":
			$page_title			= "Hoja de confirmaci&oacute;n";
			$print_label		= 'Haga clic aqu&iacute; para imprimir esta p&aacute;gina';
			$title				= "Hoja de confirmaci&oacute;n";
			$name_label			= "Nombre";
			$bib_label			= "numero de dorsal";
			$predicted_label	= "El tiempo previsto final es";
			$age_label			= 'Edad';
			$corral_label		= "Corral";
			$release_title		= "Acuerdo de exenci&oacute;n y renuncia de responsabilidad. Por favor firme abajo.";
			$signature_athlete	= "Firma del Atleta";
			$signature_parent	= "Autorizaci&oacute;n expresa de los padres si usted es menor de 18 a&ntilde;os";
			$signature_date		= "Fecha";
			$minor_disclaimer	= "Si el atleta es menor de edad su padre/madre o tutor, deber&aacute; firmar esta autorizaci&oacute;n y acuerdo de exenci&oacute;n. Como padre del deportista o tutor antes indicado, certifico que mi hijo/hija/tutelado tiene mi permiso para participar en el evento. Como padre /madre o tutor he le&iacute;do y comprendido el documento ACUERDO DE LIBERACI&Oacute;N Y RENUNCIA DE RESPONSABILIDAD ( ARRIBA ) y mediante la firma de forma intencionada y voluntaria estoy de acuerdo con sus t&eacute;rminos y condiciones.Como padre/madre o tutor del atleta, certifico que se encuentra en perfecta condici&oacute;n f&iacute;sica y es capaz de participar de manera segura en el evento. Por la presente autorizo el tratamiento m&eacute;dico para &eacute;l/ella y permito el acceso a sus registros m&eacute;dicos cuando sea estrictamente necesario como se ha indicado anteriormente.";

			break;
		case "pt":
			$page_title			= "Folha de confirma&ccedil;&atilde;o";
			$print_label		= 'Clique aqui para imprimir esta p&aacute;gina';
			$name_label			= "Nome";
			$bib_label			= "numero do dorsal";
			$predicted_label	= "Seu acabamento tempo previsto &#233;";
			$age_label			= 'Idade';
			$corral_label		= "Corral";
			$release_title		= "Termo de responsabilidade - Por favor assine abaixo";
			$signature_athlete	= "Assinatura do atleta";
			$signature_parent	= "Assinatura do representante legal se for menor de 18 anos";
			$signature_date		= "Data";
			$minor_disclaimer	= "Se o atleta for menor de 18 anos o seu representante legal tem de assinar o presente termo de responsabilidade. A assinatura do representante legal acima certifica que o representado tem autoriza&ccedil;&atilde;o para participar no evento. O representante legal leu e compreendeu&#x00a0; o termo de responsabilidade acima e mediante a assinatura do mesmo, intencional e voluntariamente concorda com&#x00a0; os seus termos e condi&ccedil;&otilde;es. O representante legal do atleta atesta ainda que o representado&#x00a0; se encontra em boas condi&ccedil;&otilde;es f&iacute;sicas sendo capaz de participar no evento em seguran&ccedil;a e sem preju&iacute;zo para a sua sa&uacute;de. Mediante o presente termo de responsabilidade o representante legal autoriza o tratamento m&eacute;dico do representado e o acesso aos registos m&eacute;dicos do representado na medida do necess&aacute;rio e conforme acima declarado.";

			break;
		case "en":
			$page_title			=  "Confirmation Sheet";
			$print_label		= 'Click Here To Print This Page';
			$title				= "Confirmation Sheet";
			$name_label			= "Name";
			$bib_label			= "Bib Number";
			$predicted_label	= "Predicted Finish Time";
			$age_label			= 'Age';
			$corral_label		= "Corral";
			$release_title		= "Release and Waiver of Liability Agreement - Please Sign Below";
			$signature_athlete	= "Signature of Athlete";
			$signature_parent	= "( Signature of parent if under 18 years )";
			$signature_date		= "Date";
			$minor_disclaimer	= "IF ATHLETE IS UNDER AGE 18 HIS/HER PARENT OR GUARDIAN MUST SIGN THIS RELEASE AND WAIVER AGREEMENT. Athlete's Parent or Guardian's signature above certifies that my son/daughter/ward has my permission to participate in the Event. Athlete's Parent/Guardian has read and understands the foregoing RELEASE AND WAIVER OF LIABILITY AGREEMENT ( above ) and by signing intentionally and voluntarily agrees to its terms and conditions. Athlete's Parent/Guardian further certifies that my son/daughter/ward is in good physical condition and is able to safely participate in the Event. I hereby authorize medical treatment for him/her and grant access to my child's medical records as necessary and as stated above";


			break;
		case "fr":
			$page_title			=  "Feuille de confirmation de r&#233;ception de votre pochette de course";
			$print_label		= 'Imprimez cette page';
			$title				= "Feuille de confirmation de r&#233;ception de votre pochette de course";
			$name_label			= "Nom";
			$bib_label			= "num&#233;ro de dossard";
			$predicted_label	= "La dur&#233;e pr&#233;vue de votre course";
			$age_label			= '&#226;ge';
			$corral_label		= "Corral";
			$release_title		= "Entente de renonciation et de non-responsabilit&#233; - Veuillez signer ci-dessous";
			$signature_athlete	= "Signature de l'athl&#232;te";
			$signature_parent	= "( Signature du parent ou tuteur si moins de 18 ans )";
			$signature_date		= "Date";
			$minor_disclaimer	= "Si l'athl&#232;te est &#226;g&#233; de moins de 18 ans, un parent ou son tuteur doit signer cette entente de renonciation et de non-responsabilit&#233;. Par sa signature ci-dessous, le parent ou tuteur confirme qu'il autorise l'enfant &#224; participer &#224; l'&#233;v&#233;nement. Le parent ou tuteur de l'athl&#232;te a lu et compris l'ENTENTE DE RENONCIATION ET DE NON-RESPONSABILIT&#201; CI-DESSUS et, par sa signature libre et responsable, il en accepte les modalit&#233;s. De plus, le parent ou tuteur atteste que son enfant ou l'enfant dont il est le tuteur est en bonne condition physique et peut en toute s&#233;curit&#233; participer &#224; l'&#233;v&#233;nement. Par la pr&#233;sente, j'autorise tout traitement m&#233;dical requis ainsi que l'acc&#232;s au dossier m&#233;dical de mon enfant ou de l'enfant dont je suis le tuteur.";

			break;
		case 'mx';
			$page_title			=  "Hoja de confirmaci&oacute;n";
			$print_label		= 'Imprima esta p&aacute;gina';
			$title				= "Hoja de Confirmaci&oacute;n";
			$name_label			= "Nombre";
			$bib_label			= "n&uacute;mero de carrera";
			$predicted_label	= "Su tiempo estimado de carrera es";
			$age_label			= 'Edad';
			$corral_label		= "Corral";
			$release_title		= "Exoneraci&oacute;n y renuncia de responsabilidad. Por favor firme abajo.";
			$signature_athlete	= "Firma del Atleta";
			$signature_parent	= "Autorizaci&oacute;n expresa de los padres si usted es menor de 18 a&ntilde;os";
			$signature_date		= "Fecha";
			$minor_disclaimer	= "Si el atleta es menor de edad su padre/madre o tutor, deber&aacute; firmar esta autorizaci&oacute;n y exoneraci&oacute;n. Como padre del deportista o tutor antes indicado, certifico que mi hijo/hija/tutelado tiene mi permiso para participar en el evento. Como padre /madre o tutor he le&iacute;do y comprendo en su totalidad el documento	DE LIBERACI&oacute;N Y RENUNCIA DE RESPONSABILIDAD ( ARRIBA ) y al firmar de forma voluntaria estoy de acuerdo con sus t&eacute;rminos y condiciones. Como padre/madre o tutor del atleta, certifico que se encuentra en perfecta condici&oacute;n f&iacute;sica y es capaz de participar de manera segura en el evento. Por la presente autorizo el tratamiento m&eacute;dico para &eacute;l/ella y permito	obtener acceso a sus registros m&eacute;dicos cuando este	sea estrictamente necesario como se ha indicado anteriormente.";

			break;
		default:
			$page_title			=  "Confirmation Sheet";
			$print_label		= 'Click Here To Print This Page';
			$title				= "Confirmation Sheet";
			$name_label			= "Name";
			$bib_label			= "Bib Number";
			$predicted_label	= "Predicted Finish Time";
			$age_label			= 'Age';
			$corral_label		= "Corral";
			$release_title		= "Release and Waiver of Liability Agreement - Please Sign Below";
			$signature_athlete	= "Signature of Athlete";
			$signature_parent	= "( Signature of parent if under 18 years )";
			$signature_date		= "Date";
			$minor_disclaimer	= "IF ATHLETE IS UNDER AGE 18 HIS/HER PARENT OR GUARDIAN MUST SIGN THIS RELEASE AND WAIVER AGREEMENT. Athlete's Parent or Guardian's signature above certifies that my son/daughter/ward has my permission to participate in the Event. Athlete's Parent/Guardian has read and understands the foregoing RELEASE AND WAIVER OF LIABILITY AGREEMENT ( above ) and by signing intentionally and voluntarily agrees to its terms and conditions. Athlete's Parent/Guardian further certifies that my son/daughter/ward is in good physical condition and is able to safely participate in the Event. I hereby authorize medical treatment for him/her and grant access to my child's medical records as necessary and as stated above";

			break;
	}


?><!doctype html>
	<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $lang2; ?>">
	<head profile="http://gmpg.org/xfn/11">
		<title>
			<?php echo $page_title; ?>
		</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_directory' ); ?>/confcard.css" />
		<script type="text/javascript">
			function printPage() {
				if ( window.print )
					window.print()
				else
					alert( "Sorry, your browser doesn't support this feature.\n\nPlease use the print button on your browser instead." );
			}
		</script>
	</head>


	<body class="printout-v2">

		<?php if( !isset( $_SESSION['post_id'] ) && empty( $_SESSION['post_id'] ) ) {

			echo '<div id="wrapper">
				<h1>Session has expired.</h1>
				<p>Please try again.</p>
			</div>';

		} else { ?>


			<div id="print_this">
				<a href="#" onclick="javascript:printPage()">
					<?php echo $print_label; ?>
				</a>
			</div>


			<?php if( $count > 0 ) {	// multiple events

				for( $i = 0; $i < $count; $i++ ) {

					$division			= strtolower( $_SESSION['division_' . $i] );
					$division_parsed	= get_race_display_name( $division, $lang );

					if(  $i == (  $count - 1  )  ) {
						$pagebreak = '';
					} else {
						$pagebreak = 'style="page-break-after:always;"';
					} ?>

					<div id="wrapper" <?php echo $pagebreak; ?>>
						<div id="header_container">
							<div class="header">

								<?php
									$event_logo = ( get_post_meta( $post_id, '_confcard_event_logo', 1 ) != '' ) ? get_post_meta( $post_id, '_confcard_event_logo', 1 ): '';

									if( $event_logo != '' ) {

										echo '<div class="event_logo">
											<img src="'. $event_logo .'">
										</div>';

									}

									$division_parsed = get_race_display_name( $division, $lang );

									$event_date_start	= ( get_post_meta( $post_id, '_confcard_event_date_start', 1 ) != '' ) ? strtotime( get_post_meta( $post_id, '_confcard_event_date_start', 1 ) ) : '';
									$event_date_end		= ( get_post_meta( $post_id, '_confcard_event_date_end', 1 ) != '' ) ? strtotime( get_post_meta( $post_id, '_confcard_event_date_end', 1 ) ) : '';

									/**
									 * event date checks
									 * if date is the same, if months are the same
									 */
									if( $event_date_start == $event_date_end ) {
										$display_date = date( 'F j, Y', $event_date_start );
									} else {

										$event_date_start_num	= date( 'n', $event_date_start );
										$event_date_start_month	= get_month_name( $lang, $event_date_start_num );

										$event_date_end_num		= date( 'n', $event_date_end );
										$event_date_end_month	= get_month_name( $lang, $event_date_end_num );

										if( $event_date_start_num == $event_date_end_num ) {
											$display_date = $event_date_start_month . ' ' . date( 'j', $event_date_start ) . ' &ndash; ' . date( 'j, Y', $event_date_end );
										} else {
											$display_date = $event_date_start_month . ' ' . date( 'j', $event_date_start ) . ' &ndash; ' . $event_date_end_month . ' ' . date( 'j, Y', $event_date_end );
										}

									}

									echo '<div class="header_txt">
										<span class="event_distance">' . $division_parsed . '</span><br>' .
										$title . '<br>
										<span class="event_date">' . $display_date . '</span>
									</div>';
								?>

							</div>
						</div>


						<div class="custom_alerts">
							<p><strong><?php _e( get_post_meta ( $post_id, '_confcard_confirmation_message', true ) ); ?></strong></p>

							<?php // don't show alert if the event is on a saturday
								if( strpos( $division, 'sat' ) === false ) { ?>
									<p class="alert"><?php _e( get_post_meta ( $post_id, '_confcard_custom_alert', true ) ); ?></p>
								<?php }
							?>

						</div>


						<div class="runner_info">
							<table class="runner_info_table">
								<tr>
									<td class="nostretch">
										<?php echo $name_label; ?>:
									</td>
									<td>
										<?php echo ${'racer_'.$i}->first_name . " " . ${'racer_'.$i}->last_name;
										?>
									</td>

									<td colspan="2" class="nostretch">
										<?php echo $bib_label; ?>:
									</td>
									<td colspan="2">
										<?php echo ${'racer_'.$i}->bib_number; ?>
									</td>
								</tr>
								<tr>
									<td class="nostretch">
										<?php echo $predicted_label; ?>:
									</td>
									<td>
										<?php echo ${'racer_'.$i}->predicted; ?>
									</td>
									<td class="nostretch">
										<?php echo $age_label; ?>:
									</td>
									<td>
										<?php echo ${'racer_'.$i}->age; ?>
									</td>
									<td class="nostretch">
										<?php echo $corral_label; ?>:
									</td>
									<td>
										<?php echo ${'racer_'.$i}->corral; ?>
									</td>
								</tr>
							</table>
						</div>


						<div class="group">
							<div id="waiver_text">
								<h2><?php echo $release_title; ?></h2>
								<p><?php _e( get_post_meta ( $post_id, '_confcard_waiver_text', true ) ); ?></p>
							</div>
						</div>


						<ol id="signature">
							<li><?php echo $signature_athlete; ?></li>
							<li><?php echo $signature_parent; ?></li>
							<li class="signature_date"><?php echo $signature_date; ?></li>
						</ol>

						<div class="separator"></div>

						<p class="minor_disclaimer"><?php echo $minor_disclaimer; ?></p>
					</div>
				<?php }

			} else {	// single event ?>

				<div id="wrapper">
					<div id="header_container">
						<div class="header">

							<?php
								$event_logo = ( get_post_meta( $post_id, '_confcard_event_logo', 1 ) != '' ) ? get_post_meta( $post_id, '_confcard_event_logo', 1 ): '';

								if( $event_logo != '' ) {

									echo '<div class="event_logo">
										<img src="'. $event_logo .'">
									</div>';

								}

								$division_parsed = get_race_display_name( strtolower( $division ), $lang );

								$event_date_start	= ( get_post_meta( $post_id, '_confcard_event_date_start', 1 ) != '' ) ? strtotime( get_post_meta( $post_id, '_confcard_event_date_start', 1 ) ) : '';
								$event_date_end		= ( get_post_meta( $post_id, '_confcard_event_date_end', 1 ) != '' ) ? strtotime( get_post_meta( $post_id, '_confcard_event_date_end', 1 ) ) : '';

								$event_date_start_num	= date( 'n', $event_date_start );
								$event_date_start_month	= get_month_name( $lang, $event_date_start_num );

								$event_date_end_num		= date( 'n', $event_date_end );
								$event_date_end_month	= get_month_name( $lang, $event_date_end_num );

								/**
								 * event date checks
								 * if date is the same, if months are the same
								 */
								$isInternational	= get_post_meta( $post_id, '_confcard_international', 1 );
								$isMexico			= get_post_meta( $post_id, '_confcard_mexico', 1 );


								if( $event_date_start == $event_date_end ) {
									if( $isInternational == 'on' || $isMexico == 'on' ) {

										$display_date =  date( 'j', $event_date_start ) . ' ' . $event_date_start_month . ', ' . date( 'Y', $event_date_end );

									} else {

										$display_date = date( 'F j, Y', $event_date_start );

									}
								} else {
									if( $isInternational == 'on' || $isMexico == 'on' ) {
										// if the month is the same
										if( $event_date_start_num == $event_date_end_num ) {

											$display_date =  date( 'j', $event_date_start ) . ' &ndash; ' . date( 'j', $event_date_end ) . ' ' . $event_date_start_month . ', ' . date( 'Y', $event_date_end );

										} else {
											// if the month is different
											$display_date = date( 'j', $event_date_start ) . ' ' . $event_date_start_month . ' &ndash; ' . date( 'j', $event_date_end ) . ' ' . $event_date_end_month . ', ' . date( 'Y', $event_date_end );
										}
									} else {
										// if the month is the same
										if( $event_date_start_num == $event_date_end_num ) {

											$display_date =  $event_date_start_month . ' ' . date( 'j', $event_date_start ) . ' &ndash; ' . date( 'j, Y', $event_date_end );

										} else {
											// if the month is different
											$display_date = $event_date_start_month . ' ' . date( 'j', $event_date_start ) . ' &ndash; ' . $event_date_end_month . date( 'j, Y', $event_date_end );
										}
									}

								}

								echo '<div class="header_txt">
									<span class="event_distance">' . $division_parsed . '</span><br>' .
									$title . '<br>
									<span class="event_date">' . $display_date . '</span>
								</div>';
							?>

						</div>
					</div>

					<p><strong><?php _e( get_post_meta ( $post_id, '_confcard_confirmation_message', true ) ); ?></strong></p>

					<?php // don't show alert if the event is on a saturday
						if( strpos( $division, 'sat' ) === false ) { ?>
							<p class="alert"><?php _e( get_post_meta ( $post_id, '_confcard_custom_alert', true ) ); ?></p>
						<?php }
					?>


					<div class="runner_info">
						<table class="runner_info_table">
							<tr>
								<td class="nostretch">
									<?php echo $name_label; ?>:
								</td>
								<td>
									<?php echo $racer->first_name . " " . $racer->last_name;
									?>
								</td>

								<td colspan="2" class="nostretch">
									<?php echo $bib_label; ?>:
								</td>
								<td colspan="2">
									<?php echo $racer->bib_number; ?>
								</td>
							</tr>
							<tr>
								<td class="nostretch">
									<?php echo $predicted_label; ?>:
								</td>
								<td>
									<?php echo $racer->predicted; ?>
								</td>
								<td class="nostretch">
									<?php echo $age_label; ?>:
								</td>
								<td>
									<?php echo $racer->age; ?>
								</td>
								<td class="nostretch">
									<?php echo $corral_label; ?>:
								</td>
								<td>
									<?php echo $racer->corral; ?>
								</td>
							</tr>
						</table>
					</div>


					<div class="group">
						<div id="waiver_text">
							<h2><?php echo $release_title; ?></h2>
							<p><?php _e(  get_post_meta ( $post_id, '_confcard_waiver_text', true ) ); ?></p>

						</div>
					</div>

					<ol id="signature">
						<li><?php echo $signature_athlete; ?></li>
						<li><?php echo $signature_parent; ?></li>
						<li class="signature_date"><?php echo $signature_date; ?></li>
					</ol>

					<div class="separator"></div>

					<p class="minor_disclaimer"><?php echo $minor_disclaimer; ?></p>
				</div>
			<?php }
		} ?>
	</body>
	</html>
<?php session_destroy(); ?>