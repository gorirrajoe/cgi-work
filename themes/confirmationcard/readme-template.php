<?php
/* Template Name: Readme */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/readme-style.css" type="text/css" media="screen" />
  <link href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

</head>
 <body>
  <div id="wrapper">
    <div id="container">
      <div id="contentBody">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
          <h1><?php the_title(); ?></h1>
          <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
        <?php endwhile; endif; ?>
      </div> <!-- End Content Body -->
    </div> <!-- End Container -->
  </div>
 </body>