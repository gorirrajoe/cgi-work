<?php
	header( 'P3P: CP="CAO PSA OUR"' );
	session_start(); // start up your PHP session!
	require_once( "../../../wp-load.php" );
	global $wpdb;

	$event		= $_SESSION['event'];
	$lang		= $_REQUEST['lang'];
	$post_id	= $_SESSION['post_id'];
	$count		= $_REQUEST['count'];
	$lang2		= substr( $_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 5 );
	// print_r( $_SESSION );
	// echo $lang;

	if( $count > 0 ) {
		for( $i=0; $i < $count; $i++ ) {
			$first_name	= addslashes( $_SESSION['first_name_'.$i] );
			$last_name	= addslashes( $_SESSION['last_name_'.$i] );
			$birthday	= $_SESSION['birthdate_'.$i];
			$division	= $_SESSION['division_'.$i];

			${'query_' . $i} = "SELECT last_name, first_name, address_1, address_2, city, state, zip, country, gender, birthdate, age, predicted, bib_number,	corral, event_code, division FROM wp_confirmation_card_data WHERE birthdate = '$birthday' AND first_name = '$first_name' AND last_name = '$last_name' AND event_code LIKE '%$event%' AND division LIKE '%$division%'";
			${'racer_'.$i} = $wpdb->get_row( ${'query_'.$i} );
		}
	} else {
		$first_name	= addslashes( $_SESSION['first_name'] );
		$last_name	= addslashes( $_SESSION['last_name'] );
		$birthday	= $_SESSION['birthdate'];
		$division	= trim( $_SESSION['division'] );

		$query = "SELECT last_name, first_name, address_1, address_2, city, state, zip, country, gender, birthdate, age, predicted, bib_number,	corral, event_code, division FROM wp_confirmation_card_data WHERE birthdate = '$birthday' AND first_name = '$first_name' AND last_name = '$last_name' AND event_code LIKE '%$event%' AND division LIKE '%$division%'";
		$racer = $wpdb->get_row( $query );
	}
	// echo $first_name . $last_name . $birthday . $event . $division;

	?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $lang2; ?>">
	<head profile="http://gmpg.org/xfn/11">
		<title>
			<?php switch ( $lang ) {
				case "es":
					echo "Hoja de confirmaci&oacute;n";
					break;
				case "pt":
					echo "Folha de confirma&ccedil;&atilde;o";
					break;
				case "en":
					echo "Confirmation Sheet";
					break;
				case "fr":
					echo "Feuille de confirmation de r&#233;ception de votre pochette de course";
					break;
				case "mx":
					echo "Hoja de confirmaci&oacute;n";
					break;
				default:
					echo "Folha de confirma&ccedil;&atilde;o";
					break;
			} ?>
		</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_directory' ); ?>/confcard.css" />
		<script type="text/javascript">
			function printPage() {
				if ( window.print )
					window.print()
				else
					alert( "Sorry, your browser doesn't support this feature.\n\nPlease use the print button on your browser instead." );
			}
		</script>
	</head>

	<body>
		<?php if( !isset( $_SESSION['post_id'] ) && empty( $_SESSION['post_id'] ) ) {
			echo '<div id="wrapper">
				<h1>Session has expired.</h1>
				<p>Please try again.</p>
			</div>';
		} else { ?>
			<div id="print_this">
				<a href="#" onclick="javascript:printPage()">
					<?php switch ( $lang ) {
						case "es":
							echo 'Haga clic aqu&iacute; para imprimir esta p&aacute;gina';
							break;
						case "pt":
							echo 'Clique aqui para imprimir esta p&aacute;gina';
							break;
						case "en":
							echo 'Click Here To Print This Page';
							break;
						case "fr":
							echo 'Imprimez cette page';
							break;
						case 'mx';
							echo 'Imprima esta p&aacute;gina';
							break;
						default:
							echo 'Click Here To Print This Page';
							break;
					} ?>
				</a>
			</div>

			<?php if( $count > 0 ) {	// multiple events
				for( $i=0; $i < $count; $i++ ) {
					$division = $_SESSION['division_'.$i];
					if(  $i == (  $count - 1  )  ) {
						$pagebreak = '';
					} else {
						$pagebreak = 'style="page-break-after:always;"';
					} ?>
					<div id="wrapper" <?php echo $pagebreak; ?>>
						<div id="header_text">
							<h1>
								<?php switch ( $lang ) {
									case "en":
										$title = " Packet Pickup Confirmation Sheet";
										break;
									case "fr":
										$title = " Imprimez cette page";
										break;
									case "mx":
										$title = " Hoja de Confirmaci&oacute;n ( necesaria para retirar paquete de corredor )";
										break;
									default:
										$title = " Packet Pickup Confirmation Sheet";
										break;
								}
								echo '['. trim( $_SESSION['division_'.$i] ) .'] | ' . qtrans_use( $lang, get_post_meta ( $post_id, '_confcard_full_event_name', true ), true ) . $title; ?>
							</h1>
							<p><strong><?php _e( get_post_meta( $post_id, '_confcard_confirmation_message', true ) ); ?></strong></p>

							<?php // don't show alert if the event is on a saturday
								if( strpos( $division, 'sat' ) === false ) { ?>
									<p class="alert"><?php _e( get_post_meta ( $post_id, '_confcard_custom_alert', true ) ); ?></p>
								<?php }
							?>
						</div>


						<div class="group">
							<ol id="raceday_info">
								<li class="bib_number">
									<?php switch ( $lang ) {
										case "es":
											echo "numero de dorsal: ";
											break;
										case "pt":
											echo "numero do dorsal: ";
											break;
										case "en":
											echo "Bib Number: ";
											break;
										case "fr":
											echo "num&#233;ro de dossard: ";
											break;
										case "mx":
											echo "n&uacute;mero de carrera: ";
											break;
										default:
											echo "Bib Number: ";
											break;
									} ?>
									<span class="result"><?php echo ${'racer_'.$i}->bib_number; ?></span>
								</li>
							</ol>
						</div>


						<div class="group">
							<ol id="address_mandatory">
								<li class="address">
									<h2>
										<?php switch ( $lang ) {
											case "es":
												echo "Nombre y direcci&oacute;n";
												break;
											case "pt":
												echo "Nome /Morada";
												break;
											case "en":
												echo "Name/Address";
												break;
											case "fr":
												echo "Nom/Adresse";
												break;
											case "mx":
												echo "Nombre y direcci&oacute;n";
												break;
											default:
												echo "Name/Address";
												break;
										} ?>
									</h2>
									<p class="result">
										<?php
											echo ${'racer_'.$i}->first_name . " " . ${'racer_'.$i}->last_name . "<br />" .
											${'racer_'.$i}->address_1 . "<br />";

											if (  ${'racer_'.$i}->address_2  ) {
												echo ${'racer_'.$i}->address_2 . "<br />";
											}

											echo ${'racer_'.$i}->city . ", " . ${'racer_'.$i}->state . "<br />" .
											${'racer_'.$i}->zip;
										?>
									</p>
								</li>
								<li class="mandatory_info">
									<h2>&nbsp;</h2>
									<p>
										<?php switch ( $lang ) {
											case "es":
												echo "Corral: ";
												break;
											case "pt":
												echo "Corral: ";
												break;
											case "en":
												echo "Corral: ";
												break;
											case "fr":
												echo "corral: ";
												break;
											case "mx":
												echo "corral: ";
												break;
											default:
												echo "Corral: ";
												break;
										} ?>
										<span class="result"><?php echo ${'racer_'.$i}->corral; ?></span>
									<br>
										<?php switch ( $lang ) {
											case "es":
												echo "genero: ";
												break;
											case "pt":
												echo "Sexo ";
												break;
											case "en":
												echo "Gender: ";
												break;
											case "fr":
												echo "sexe: ";
												break;
											case "mx":
												echo "g&eacute;nero: ";
												break;
											default:
												echo "Gender: ";
												break;
										} ?>
										<span class="result"><?php echo ${'racer_'.$i}->gender; ?></span>
									<br>
										<?php switch ( $lang ) {
											case "es":
												echo "edad: ";
												break;
											case "pt":
												echo "idade: ";
												break;
											case "en":
												echo "Age: ";
												break;
											case "fr":
												echo "&#226;ge: ";
												break;
											case "mx":
												echo "edad: ";
												break;
											default:
												echo "Age: ";
												break;
										} ?>
										<span class="result"><?php echo ${'racer_'.$i}->age; ?></span>
									<br>
										<?php switch ( $lang ) {
											case "es":
												echo "El tiempo previsto final es: ";
												break;
											case "pt":
												echo "Seu acabamento tempo previsto &#233;: ";
												break;
											case "en":
												echo "Predicted Finish Time: ";
												break;
											case "fr":
												echo "La dur&#233;e pr&#233;vue de votre course: ";
												break;
											case "mx":
												echo "Su tiempo estimado de carrera es: ";
												break;
											default:
												echo "Predicted Finish Time: ";
												break;
										} ?>
										<span class="result"><?php echo ${'racer_'.$i}->predicted; ?></span>
									</p>
								</li>

							</ol>
						</div>


						<div class="group">
							<div id="waiver_text">
								<h2>
									<?php switch ( $lang ) {
										case "es":
											echo "Acuerdo de exenci&oacute;n y renuncia de responsabilidad. Por favor firme abajo.";
											break;
										case "pt":
											echo "Termo de responsabilidade - Por favor assine abaixo";
											break;
										case "en":
											echo "Release and Waiver of Liability Agreement - Please Sign Below";
											break;
										case "fr":
											echo "Entente de renonciation et de non-responsabilit&#233; - Veuillez signer ci-dessous";
											break;
										case "mx":
											echo "Exoneraci&oacute;n y renuncia de responsabilidad. Por favor firme abajo.";
											break;
										default:
											echo "Release and Waiver of Liability Agreement - Please Sign Below";
											break;
									} ?>
								</h2>
								<p><?php _e( get_post_meta ( $post_id, '_confcard_waiver_text', true ) ); ?></p>

							</div>
						</div>


						<?php switch ( $lang ) {
							case "es":
								$signature_athlete	= "Firma del Atleta";
								$signature_parent	= "Autorizaci&oacute;n expresa de los padres si usted es menor de 18 a&ntilde;os";
								$signature_date		= "Fecha";
								break;
							case "pt":
								$signature_athlete	= "Assinatura do atleta";
								$signature_parent	= "Assinatura do representante legal se for menor de 18 anos";
								$signature_date		= "Data";
								break;
							case "en":
								$signature_athlete	= "Signature of Athlete";
								$signature_parent	= "( Signature of parent if under 18 years )";
								$signature_date		= "Date";
								break;
							case "fr":
								$signature_athlete	= "Signature de l'athl&#232;te";
								$signature_parent	= "( Signature du parent ou tuteur si moins de 18 ans )";
								$signature_date		= "Date";
								break;
							case "mx":
								$signature_athlete	= "Firma del Atleta";
								$signature_parent	= "Autorizaci&oacute;n expresa de los padres si usted es menor de 18 a&ntilde;os";
								$signature_date		= "Fecha";
								break;
							default:
								$signature_athlete	= "Signature of Athlete";
								$signature_parent	= "( Signature of parent if under 18 years )";
								$signature_date		= "Date";
								break;
						} ?>

						<ol id="signature">
							<li><?php echo $signature_athlete; ?></li>
							<li><?php echo $signature_parent; ?></li>
							<li class="signature_date"><?php echo $signature_date; ?></li>
						</ol>
						<div class="separator"></div>

						<?php switch ( $lang ) {
							case "es":
								$minor_disclaimer = "Si el atleta es menor de edad su padre/madre o tutor, deber&aacute; firmar esta autorizaci&oacute;n y acuerdo de exenci&oacute;n. Como padre del deportista o tutor antes indicado, certifico que mi hijo/hija/tutelado tiene mi permiso para participar en el evento. Como padre /madre o tutor he le&iacute;do y comprendido el documento ACUERDO DE LIBERACI&Oacute;N Y RENUNCIA DE RESPONSABILIDAD ( ARRIBA ) y mediante la firma de forma intencionada y voluntaria estoy de acuerdo con sus t&eacute;rminos y condiciones.Como padre/madre o tutor del atleta, certifico que se encuentra en perfecta condici&oacute;n f&iacute;sica y es capaz de participar de manera segura en el evento. Por la presente autorizo el tratamiento m&eacute;dico para &eacute;l/ella y permito el acceso a sus registros m&eacute;dicos cuando sea estrictamente necesario como se ha indicado anteriormente.";
								break;
							case "pt":
								$minor_disclaimer = "Se o atleta for menor de 18 anos o seu representante legal tem de assinar o presente termo de responsabilidade. A assinatura do representante legal acima certifica que o representado tem autoriza&ccedil;&atilde;o para participar no evento. O representante legal leu e compreendeu&#x00a0; o termo de responsabilidade acima e mediante a assinatura do mesmo, intencional e voluntariamente concorda com&#x00a0; os seus termos e condi&ccedil;&otilde;es. O representante legal do atleta atesta ainda que o representado&#x00a0; se encontra em boas condi&ccedil;&otilde;es f&iacute;sicas sendo capaz de participar no evento em seguran&ccedil;a e sem preju&iacute;zo para a sua sa&uacute;de. Mediante o presente termo de responsabilidade o representante legal autoriza o tratamento m&eacute;dico do representado e o acesso aos registos m&eacute;dicos do representado na medida do necess&aacute;rio e conforme acima declarado.";
								break;
							case "en":
								$minor_disclaimer = "IF ATHLETE IS UNDER AGE 18 HIS/HER PARENT OR GUARDIAN MUST SIGN THIS RELEASE AND WAIVER AGREEMENT. Athlete's Parent or Guardian's signature above certifies that my son/daughter/ward has my permission to participate in the Event. Athlete's Parent/Guardian has read and understands the foregoing RELEASE AND WAIVER OF LIABILITY AGREEMENT ( above ) and by signing intentionally and voluntarily agrees to its terms and conditions. Athlete's Parent/Guardian further certifies that my son/daughter/ward is in good physical condition and is able to safely participate in the Event. I hereby authorize medical treatment for him/her and grant access to my child's medical records as necessary and as stated above";
								break;
							case "fr":
								$minor_disclaimer = "Si l'athl&#232;te est &#226;g&#233; de moins de 18 ans, un parent ou son tuteur doit signer cette entente de renonciation et de non-responsabilit&#233;. Par sa signature ci-dessous, le parent ou tuteur confirme qu'il autorise l'enfant &#224; participer &#224; l'&#233;v&#233;nement. Le parent ou tuteur de l'athl&#232;te a lu et compris l'ENTENTE DE RENONCIATION ET DE NON-RESPONSABILIT&#201; CI-DESSUS et, par sa signature libre et responsable, il en accepte les modalit&#233;s. De plus, le parent ou tuteur atteste que son enfant ou l'enfant dont il est le tuteur est en bonne condition physique et peut en toute s&#233;curit&#233; participer &#224; l'&#233;v&#233;nement. Par la pr&#233;sente, j'autorise tout traitement m&#233;dical requis ainsi que l'acc&#232;s au dossier m&#233;dical de mon enfant ou de l'enfant dont je suis le tuteur.";
								break;
							case "mx":
								$minor_disclaimer = "Si el atleta es menor de edad su padre/madre o tutor, deber&aacute; firmar esta autorizaci&oacute;n y exoneraci&oacute;n. Como padre del deportista o tutor antes indicado, certifico que mi hijo/hija/tutelado tiene mi permiso para participar en el evento. Como padre /madre o tutor he le&iacute;do y comprendo en su totalidad el documento	DE LIBERACI&oacute;N Y RENUNCIA DE RESPONSABILIDAD ( ARRIBA ) y al firmar de forma voluntaria estoy de acuerdo con sus t&eacute;rminos y condiciones. Como padre/madre o tutor del atleta, certifico que se encuentra en perfecta condici&oacute;n f&iacute;sica y es capaz de participar de manera segura en el evento. Por la presente autorizo el tratamiento m&eacute;dico para &eacute;l/ella y permito	obtener acceso a sus registros m&eacute;dicos cuando este	sea estrictamente necesario como se ha indicado anteriormente.";
								break;
							default:
								$minor_disclaimer = "IF ATHLETE IS UNDER AGE 18 HIS/HER PARENT OR GUARDIAN MUST SIGN THIS RELEASE AND WAIVER AGREEMENT. Athlete's Parent or Guardian's signature above certifies that my son/daughter/ward has my permission to participate in the Event. Athlete's Parent/Guardian has read and understands the foregoing RELEASE AND WAIVER OF LIABILITY AGREEMENT ( above ) and by signing intentionally and voluntarily agrees to its terms and conditions. Athlete's Parent/Guardian further certifies that my son/daughter/ward is in good physical condition and is able to safely participate in the Event. I hereby authorize medical treatment for him/her and grant access to my child's medical records as necessary and as stated above";
								break;
						} ?>

						<p class="minor_disclaimer"><?php echo $minor_disclaimer; ?></p>
					</div>
				<?php }
			} else {	// single event ?>
				<div id="wrapper">
					<div id="header_text">
						<h1>
							<?php switch ( $lang ) {
								case "en":
									$title = " Packet Pickup Confirmation Sheet";
									break;
								case "fr":
									$title = " Imprimez cette page";
									break;
								case "mx":
									$title = " Hoja de Confirmaci&oacute;n ( necesaria para retirar paquete de corredor )";
									break;
								default:
									$title = " Packet Pickup Confirmation Sheet";
									break;
							}
							echo '['. $division .'] | ' . qtrans_use( $lang, get_post_meta ( $post_id, '_confcard_full_event_name', true ), true ) . $title; ?>
						</h1>
						<p><strong><?php _e( get_post_meta ( $post_id, '_confcard_confirmation_message', true ) ); ?></strong></p>
						<p class="alert"><?php _e(  get_post_meta ( $post_id, '_confcard_custom_alert', true ) ); ?></p>
					</div>


					<div class="group">
						<ol id="raceday_info">
							<li class="bib_number">
								<?php switch ( $lang ) {
									case "es":
										echo "numero de dorsal: ";
										break;
									case "pt":
										echo "numero do dorsal: ";
										break;
									case "en":
										echo "Bib Number: ";
										break;
									case "fr":
										echo "num&#233;ro de dossard: ";
										break;
									case "mx":
										echo "n&uacute;mero de carrera: ";
										break;
									default:
										echo "Bib Number: ";
										break;
								} ?>
								<span class="result"><?php echo $racer->bib_number; ?></span>
							</li>
						</ol>
					</div>
					<div class="group">
						<ol id="address_mandatory">
							<li class="address">
								<h2>
									<?php switch ( $lang ) {
										case "es":
											echo "Nombre y direcci&oacute;n";
											break;
										case "pt":
											echo "Nome /Morada";
											break;
										case "en":
											echo "Name/Address";
											break;
										case "fr":
											echo "Nom/Adresse";
											break;
										case "mx":
											echo "Nombre y direcci&oacute;n";
											break;
										default:
											echo "Name/Address";
											break;
									} ?>
								</h2>
								<p class="result">
									<?php
										echo $racer->first_name . " " . $racer->last_name . "<br />" . $racer->address_1 . "<br />";

										if (  $racer->address_2  ) {
											echo $racer->address_2 . "<br />";
										}

										echo $racer->city . ", " . $racer->state . "<br />" . $racer->zip;
									?>
								</p>
							</li>
							<li class="mandatory_info">
								<h2>&nbsp;</h2>
								<p>
									<?php switch ( $lang ) {
										case "es":
											echo "Corral: ";
											break;
										case "pt":
											echo "Corral: ";
											break;
										case "en":
											echo "Corral: ";
											break;
										case "fr":
											echo "corral: ";
											break;
										case "mx":
											echo "corral: ";
											break;
										default:
											echo "Corral: ";
											break;
									} ?>
									<span class="result"><?php echo $racer->corral; ?></span>
								<br>
									<?php switch ( $lang ) {
										case "es":
											echo "genero: ";
											break;
										case "pt":
											echo "Sexo ";
											break;
										case "en":
											echo "Gender: ";
											break;
										case "fr":
											echo "sexe: ";
											break;
										case "mx":
											echo "g&eacute;nero: ";
											break;
										default:
											echo "Gender: ";
											break;
									} ?>
									<span class="result"><?php echo $racer->gender; ?></span>
								<br>
									<?php switch ( $lang ) {
										case "es":
											echo "edad: ";
											break;
										case "pt":
											echo "idade: ";
											break;
										case "en":
											echo "Age: ";
											break;
										case "fr":
											echo "&#226;ge: ";
											break;
										case "mx":
											echo "edad: ";
											break;
										default:
											echo "Age: ";
											break;
									} ?>
									<span class="result"><?php echo $racer->age; ?></span>
								<br>
									<?php switch ( $lang ) {
										case "es":
											echo "El tiempo previsto final es: ";
											break;
										case "pt":
											echo "Seu acabamento tempo previsto &#233;: ";
											break;
										case "en":
											echo "Predicted Finish Time: ";
											break;
										case "fr":
											echo "La dur&#233;e pr&#233;vue de votre course: ";
											break;
										case "mx":
											echo "Su tiempo estimado de carrera es: ";
											break;
										default:
											echo "Predicted Finish Time: ";
											break;
									} ?>
									<span class="result"><?php echo $racer->predicted; ?></span>
								</p>
							</li>
						</ol>
					</div>
					<div class="group">
						<div id="waiver_text">
							<h2>
								<?php switch ( $lang ) {
									case "es":
										echo "Acuerdo de exenci&oacute;n y renuncia de responsabilidad. Por favor firme abajo.";
										break;
									case "pt":
										echo "Termo de responsabilidade - Por favor assine abaixo";
										break;
									case "en":
										echo "Release and Waiver of Liability Agreement - Please Sign Below";
										break;
									case "fr":
										echo "Entente de renonciation et de non-responsabilit&#233; - Veuillez signer ci-dessous";
										break;
									case "mx":
										echo "Exoneraci&oacute;n y renuncia de responsabilidad. Por favor firme abajo.";
										break;
									default:
										echo "Release and Waiver of Liability Agreement - Please Sign Below";
										break;
								} ?>
							</h2>
							<p><?php _e(  get_post_meta ( $post_id, '_confcard_waiver_text', true ) ); ?></p>

						</div>
					</div>

					<?php switch ( $lang ) {
						case "es":
							$signature_athlete = "Firma del Atleta";
							$signature_parent = "Autorizaci&oacute;n expresa de los padres si usted es menor de 18 a&ntilde;os";
							$signature_date = "Fecha";
							break;
						case "pt":
							$signature_athlete = "Assinatura do atleta";
							$signature_parent = "Assinatura do representante legal se for menor de 18 anos";
							$signature_date = "Data";
							break;
						case "en":
							$signature_athlete = "Signature of Athlete";
							$signature_parent = "( Signature of parent if under 18 years )";
							$signature_date = "Date";
							break;
						case "fr":
							$signature_athlete = "Signature de l'athl&#232;te";
							$signature_parent = "( Signature du parent ou tuteur si moins de 18 ans )";
							$signature_date = "Date";
							break;
						case "mx":
							$signature_athlete = "Firma del Atleta";
							$signature_parent = "Autorizaci&oacute;n expresa de los padres si usted es menor de 18 a&ntilde;os";
							$signature_date = "Fecha";
							break;
						default:
							$signature_athlete = "Signature of Athlete";
							$signature_parent = "( Signature of parent if under 18 years )";
							$signature_date = "Date";
							break;
					} ?>

					<ol id="signature">
						<li><?php echo $signature_athlete; ?></li>
						<li><?php echo $signature_parent; ?></li>
						<li class="signature_date"><?php echo $signature_date; ?></li>
					</ol>
					<div class="separator"></div>

					<?php switch ( $lang ) {
						case "es":
							$minor_disclaimer = "Si el atleta es menor de edad su padre/madre o tutor, deber&aacute; firmar esta autorizaci&oacute;n y acuerdo de exenci&oacute;n. Como padre del deportista o tutor antes indicado, certifico que mi hijo/hija/tutelado tiene mi permiso para participar en el evento. Como padre /madre o tutor he le&iacute;do y comprendido el documento ACUERDO DE LIBERACI&Oacute;N Y RENUNCIA DE RESPONSABILIDAD ( ARRIBA ) y mediante la firma de forma intencionada y voluntaria estoy de acuerdo con sus t&eacute;rminos y condiciones.Como padre/madre o tutor del atleta, certifico que se encuentra en perfecta condici&oacute;n f&iacute;sica y es capaz de participar de manera segura en el evento. Por la presente autorizo el tratamiento m&eacute;dico para &eacute;l/ella y permito el acceso a sus registros m&eacute;dicos cuando sea estrictamente necesario como se ha indicado anteriormente.";
							break;
						case "pt":
							$minor_disclaimer = "Se o atleta for menor de 18 anos o seu representante legal tem de assinar o presente termo de responsabilidade. A assinatura do representante legal acima certifica que o representado tem autoriza&ccedil;&atilde;o para participar no evento. O representante legal leu e compreendeu&#x00a0; o termo de responsabilidade acima e mediante a assinatura do mesmo, intencional e voluntariamente concorda com&#x00a0; os seus termos e condi&ccedil;&otilde;es. O representante legal do atleta atesta ainda que o representado&#x00a0; se encontra em boas condi&ccedil;&otilde;es f&iacute;sicas sendo capaz de participar no evento em seguran&ccedil;a e sem preju&iacute;zo para a sua sa&uacute;de. Mediante o presente termo de responsabilidade o representante legal autoriza o tratamento m&eacute;dico do representado e o acesso aos registos m&eacute;dicos do representado na medida do necess&aacute;rio e conforme acima declarado.";
							break;
						case "en":
							$minor_disclaimer = "IF ATHLETE IS UNDER AGE 18 HIS/HER PARENT OR GUARDIAN MUST SIGN THIS RELEASE AND WAIVER AGREEMENT. Athlete's Parent or Guardian's signature above certifies that my son/daughter/ward has my permission to participate in the Event. Athlete's Parent/Guardian has read and understands the foregoing RELEASE AND WAIVER OF LIABILITY AGREEMENT ( above ) and by signing intentionally and voluntarily agrees to its terms and conditions. Athlete's Parent/Guardian further certifies that my son/daughter/ward is in good physical condition and is able to safely participate in the Event. I hereby authorize medical treatment for him/her and grant access to my child's medical records as necessary and as stated above";
							break;
						case "fr":
							$minor_disclaimer = "Si l'athl&#232;te est &#226;g&#233; de moins de 18 ans, un parent ou son tuteur doit signer cette entente de renonciation et de non-responsabilit&#233;. Par sa signature ci-dessous, le parent ou tuteur confirme qu'il autorise l'enfant &#224; participer &#224; l'&#233;v&#233;nement. Le parent ou tuteur de l'athl&#232;te a lu et compris l'ENTENTE DE RENONCIATION ET DE NON-RESPONSABILIT&#201; CI-DESSUS et, par sa signature libre et responsable, il en accepte les modalit&#233;s. De plus, le parent ou tuteur atteste que son enfant ou l'enfant dont il est le tuteur est en bonne condition physique et peut en toute s&#233;curit&#233; participer &#224; l'&#233;v&#233;nement. Par la pr&#233;sente, j'autorise tout traitement m&#233;dical requis ainsi que l'acc&#232;s au dossier m&#233;dical de mon enfant ou de l'enfant dont je suis le tuteur.";
							break;
						case "mx":
							$minor_disclaimer = "Si el atleta es menor de edad su padre/madre o tutor, deber&aacute; firmar esta autorizaci&oacute;n y exoneraci&oacute;n. Como padre del deportista o tutor antes indicado, certifico que mi hijo/hija/tutelado tiene mi permiso para participar en el evento. Como padre /madre o tutor he le&iacute;do y comprendo en su totalidad el documento	DE LIBERACI&oacute;N Y RENUNCIA DE RESPONSABILIDAD ( ARRIBA ) y al firmar de forma voluntaria estoy de acuerdo con sus t&eacute;rminos y condiciones. Como padre/madre o tutor del atleta, certifico que se encuentra en perfecta condici&oacute;n f&iacute;sica y es capaz de participar de manera segura en el evento. Por la presente autorizo el tratamiento m&eacute;dico para &eacute;l/ella y permito	obtener acceso a sus registros m&eacute;dicos cuando este	sea estrictamente necesario como se ha indicado anteriormente.";
							break;
						default:
							$minor_disclaimer = "IF ATHLETE IS UNDER AGE 18 HIS/HER PARENT OR GUARDIAN MUST SIGN THIS RELEASE AND WAIVER AGREEMENT. Athlete's Parent or Guardian's signature above certifies that my son/daughter/ward has my permission to participate in the Event. Athlete's Parent/Guardian has read and understands the foregoing RELEASE AND WAIVER OF LIABILITY AGREEMENT ( above ) and by signing intentionally and voluntarily agrees to its terms and conditions. Athlete's Parent/Guardian further certifies that my son/daughter/ward is in good physical condition and is able to safely participate in the Event. I hereby authorize medical treatment for him/her and grant access to my child's medical records as necessary and as stated above";
							break;
					} ?>

					<p class="minor_disclaimer"><?php echo $minor_disclaimer; ?></p>
				</div>
			<?php }
		} ?>
	</body>
	</html>
<?php session_destroy(); ?>