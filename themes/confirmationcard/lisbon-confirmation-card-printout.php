<?php
  header('P3P: CP="CAO PSA OUR"');
	session_start(); // start up your PHP session!
  require_once("../../../wp-load.php");
  global $wpdb;

  $first_name = $_SESSION['first_name'];
  $email = $_SESSION['email'];
  $birthyear = $_SESSION['birthyear'];
  $event = $_SESSION['event'];
  $lang = $_REQUEST['lang'];
  $post_id = $_SESSION['post_id'];
  //echo $first_name . '<br />' . $email . '<br />' . $birthyear . '<br />' . $event .'<br />';

  $query = "SELECT bib_number, first_name, last_name, email, birthdate, gender, predicted, regtype, event_code FROM wp_lis_confirmation_card_data WHERE birthdate = '$birthyear' AND first_name = '$first_name' AND email = '$email' AND event_code LIKE '%$event%'";
  //echo $query;
  $racer = $wpdb->get_row($query);

  $lang2 = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 5);

?>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"> 
 <html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $lang2; ?>">
  <head profile="http://gmpg.org/xfn/11">
  <title>
    <?php _e(get_post_meta ($post_id, '_confcard_lisbon_full_event_name', true) . " Packet Pickup Confirmation Sheet"); ?>
  </title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/confcard.css" />
  <script type="text/javascript">
    function printPage() {
      if (window.print)
        window.print()
      else
        alert("Sorry, your browser doesn't support this feature.\n\nPlease use the print button on your browser instead.");
    }
  </script>
 </head>

 <body>
  <div id="print_this">
    <a href="#" onclick="javascript:printPage()">
      <?php
        switch ($lang){
          case "es":
            echo 'Haz clic aqu&#237; para imprimir esta p&#225;gina';
            break;
          case "pt":
            echo 'Clique aqui para imprimir esta p&aacute;gina';
            break;
          case "en":
            echo 'Click Here To Print This Page';
            break;
          default:
            echo 'Click Here To Print This Page';
            break;
        }
      ?>
    </a>
  </div>
  <div id="wrapper">
    <div id="header_text">
      <h1>
        <?php _e(get_post_meta ($post_id, '_confcard_lisbon_full_event_name', true)); ?>
      </h1>
      <?php $event_logo = get_post_meta ($post_id, '_confcard_lisbon_event_logo', true);
        if($event_logo) {
          echo '<img src="'.$event_logo.'">';
        }
      ?>
    </div>
    <p><strong><?php _e( get_post_meta ($post_id, '_confcard_lisbon_confirmation_message', true)); ?></strong></p>
    <div class="group">
      <p class="result">
        <?php
          // print_r($racer);
          _e(
            "[:en]Race Number: [:pt]N&#250;mero de dorsal: [:es]N&#250;mero de carrera: ");
          echo $racer->bib_number . "<br />";
          _e(
            "[:en]Name: [:pt]Nome: [:es]Nombre: ");
          echo $racer->first_name . ' ' . $racer->last_name . "<br />";
          _e(
            "[:en]Year of Birth: [:pt]Ano de Nascimento: [:es]A&#241;o de nacimiento: ");
          echo $racer->birthdate . "<br />";
          _e(
            "[:en]Gender: [:pt]Sexo: [:es]Sexo: ");
          echo $racer->gender . "<br />";
          _e(
            "[:en]Distance: [:pt]Dist&#226;ncia: [:es]Distancia: ");
          echo $racer->regtype . "<br />";
        ?>
      </p>
    </div>
    <div class="group">
      <h2>
        <?php _e("
          [:en]Race Number Pick up
          [:pt]Levantamento dos dorsais
          [:es]Recogida del n&#250;mero de carrera
        "); ?>
      </h2>
      <?php _e( get_post_meta ($post_id, '_confcard_lisbon_pick_up', true)); ?>
    </div>
    <div class="group">
      <h2>
        <?php _e("
          [:en]Sport Expo
          [:pt]Sport Expo
          [:es]Sport Expo
        "); ?>
      </h2>
      <?php _e(nl2br( stripslashes( get_post_meta ($post_id, '_confcard_lisbon_sport_expo', true)))); ?>
    </div>
    <div class="group">
      <h2>
        <?php _e("
          [:en]Race Start Times
          [:pt]Hor&aacute;rios das corridas
          [:es]Horas De Inicio De Las Carreras
        "); ?>
      </h2>
      <?php _e(nl2br( stripslashes( get_post_meta ($post_id, '_confcard_lisbon_start_times', true)))); ?>
    </div>
  </form>
 </body>
</html>
<?php session_destroy(); ?>