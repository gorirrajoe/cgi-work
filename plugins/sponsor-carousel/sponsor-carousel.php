<?php 
/*
Plugin Name: Sponsor Carousel
Plugin URI: http://runrocknroll.com/
Description: A simple plugin to update the sponsor carousel
Author: Joey Hernandez
Author URI: http://runrocknroll.com
Version: 1.0
*/

/* global var */
global $sponsor_settings_page;
$pri_sponsor_list   = get_option('pri_sponsor_list');
$pri_sponsor_logos  = get_option('pri_sponsor_logos');
$pri_sponsor_url    = get_option('pri_sponsor_url');

$sponsor_list   = get_option('sponsor_list');
$sponsor_logos  = get_option('sponsor_logos');
$sponsor_url    = get_option('sponsor_url');

/* includes */
include(dirname(__FILE__) . '/includes/scripts.php');
include(dirname(__FILE__) . '/includes/process-ajax.php');
include(dirname(__FILE__) . '/includes/admin-page.php');

function sponsor_install() {
  $list = array();

  $pri_sponsor_list = get_option('pri_sponsor_list');
  add_option('pri_sponsor_list', $list);

  $pri_sponsor_logos = get_option('pri_sponsor_logos');
  add_option('pri_sponsor_logos', $list);

  $pri_sponsor_url = get_option('pri_sponsor_url');
  add_option('pri_sponsor_url', $list);

  $sponsor_list = get_option('sponsor_list');
  add_option('sponsor_list', $list);

  $sponsor_logos = get_option('sponsor_logos');
  add_option('sponsor_logos', $list);

  $sponsor_url = get_option('sponsor_url');
  add_option('sponsor_url', $list);

}
register_activation_hook(__FILE__, 'sponsor_install');



?>
