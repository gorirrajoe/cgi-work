<?php
function sponsor_options_page() {

  $list = array();
  if(!isset( $pri_sponsor_list )) {
    $pri_sponsor_list = get_option('pri_sponsor_list');
    add_option('pri_sponsor_list', $list);

    $pri_sponsor_logos = get_option('pri_sponsor_logos');
    add_option('pri_sponsor_logos', $list);

    $pri_sponsor_url = get_option('pri_sponsor_url');
    add_option('pri_sponsor_url', $list);
  }
  if(!isset( $sponsor_list )) {
    $sponsor_list = get_option('sponsor_list');
    add_option('sponsor_list', $list);

    $sponsor_logos = get_option('sponsor_logos');
    add_option('sponsor_logos', $list);

    $sponsor_url = get_option('sponsor_url');
    add_option('sponsor_url', $list);
  
  }

  global $pri_sponsor_list;
  global $pri_sponsor_logos;
  global $pri_sponsor_url;
  global $sponsor_list;
  global $sponsor_logos;
  global $sponsor_url;

  $temp_list = array();
  $updateImageLink = plugin_dir_url(__FILE__) ."/images/add.png"; 

  ob_start(); ?>

  <div class="wrap">

    <h2><?php _e('Sponsor Carousel', 'rnr3'); ?></h2>
    <div class="error">
      <p>Welcome to the Sponsor Carousel Manager. <strong>Please do not add or remove any sponsors below.</strong> Instead, create a Workfront ticket and we'll get to it shortly!</p>
    </div>
    <?php 
      if( current_user_can( 'edit_theme_options' ) ) {
        /* PRIMARY SPONSOR ACTIONS */
        if(isset($_POST['add_pri_sponsor'])) { // ADD SPONSOR
          if($_POST['add_pri_sponsor_name'] != '') {
            $temp_list = $pri_sponsor_list;
            array_push($temp_list, $_POST['add_pri_sponsor_name']);
            update_option('pri_sponsor_list', $temp_list);
            $pri_sponsor_list = get_option('pri_sponsor_list');
          } else {
            echo '<div id="message" class="updated fade">Please enter a Sponsor Name</div>';
          }


        } else if(isset($_POST['pri_delete'])) {  // DELETE SPONSOR
          unset($pri_sponsor_list[$_POST['pri-index']]);
          unset($pri_sponsor_logos[$_POST['pri-index']]);
          unset($pri_sponsor_url[$_POST['pri-index']]);
          update_option('pri_sponsor_list', $pri_sponsor_list);
          update_option('pri_sponsor_logos', $pri_sponsor_logos);
          update_option('pri_sponsor_url', $pri_sponsor_url);


        } else if(isset($_POST['pri_save'])) {  // SAVE SPONSOR
          // update name array
          $name = $_POST['pri-name'];
          $temp_list = $pri_sponsor_list;
          $temp_list[$_POST['pri-index']] = $name;
          update_option('pri_sponsor_list', $temp_list);
          $pri_sponsor_list = get_option('pri_sponsor_list');

          // update logo array
          $logo = $_POST['pri-logo'];
          $temp_list = $pri_sponsor_logos;
          $temp_list[$_POST['pri-index']] = $logo;
          update_option('pri_sponsor_logos', $temp_list);
          $pri_sponsor_logos = get_option('pri_sponsor_logos');

          // update url array
          $url = $_POST['pri-url'];
          $temp_list = $pri_sponsor_url;
          $temp_list[$_POST['pri-index']] = $url;
          update_option('pri_sponsor_url', $temp_list);
          $pri_sponsor_url = get_option('pri_sponsor_url');
        }


        /* SECONDARY SPONSOR ACTIONS */
        if(isset($_POST['add_sponsor'])) { // ADD SPONSOR
          if($_POST['add_sponsor_name'] != '') {
            $temp_list = $sponsor_list;
            array_push($temp_list, $_POST['add_sponsor_name']);
            update_option('sponsor_list', $temp_list);
            $sponsor_list = get_option('sponsor_list');
          } else {
            echo '<div id="message" class="updated fade">Please enter a Sponsor Name</div>';
          }


        } else if(isset($_POST['delete'])) {  // DELETE SPONSOR
          unset($sponsor_list[$_POST['index']]);
          unset($sponsor_logos[$_POST['index']]);
          unset($sponsor_url[$_POST['index']]);
          update_option('sponsor_list', $sponsor_list);
          update_option('sponsor_logos', $sponsor_logos);
          update_option('sponsor_url', $sponsor_url);


        } else if(isset($_POST['save'])) {  // SAVE SPONSOR
          // update name array
          $name = $_POST['name'];
          $temp_list = $sponsor_list;
          $temp_list[$_POST['index']] = $name;
          update_option('sponsor_list', $temp_list);
          $sponsor_list = get_option('sponsor_list');

          // update logo array
          $logo = $_POST['logo'];
          $temp_list = $sponsor_logos;
          $temp_list[$_POST['index']] = $logo;
          update_option('sponsor_logos', $temp_list);
          $sponsor_logos = get_option('sponsor_logos');

          // update url array
          $url = $_POST['url'];
          $temp_list = $sponsor_url;
          $temp_list[$_POST['index']] = $url;
          update_option('sponsor_url', $temp_list);
          $sponsor_url = get_option('sponsor_url');
        }
      ?>
      <?php /* PRIMARY SPONSORS */ ?>
      <h3><?php _e('Primary Sponsors', 'rnr3'); ?></h3>
      <form action="?page=sponsor-options" id="add_pri_sponsor_form" name="add_pri_sponsor_form" method="post">
        <label for="add_pri_sponsor_name">Sponsor Name: </label>
        <input type="text" name="add_pri_sponsor_name" id="add_pri_sponsor_name"  />
        <input class="button-primary" type="submit" value="Add Sponsor" name="add_pri_sponsor"/>
      </form>

      <table class="wp-list-table widefat fixed posts pri-sponsor-list">
        <thead>
          <tr>
            <th><?php _e('Order', 'rnr3'); ?></th>
            <th><?php _e('Edit', 'rnr3'); ?></th>
            <th><?php _e('Name', 'rnr3'); ?></th>
            <th><?php _e('Delete', 'rnr3'); ?></th>
          </tr>
        </thead>
        <!--
        <tfoot>
          <tr>
            <th><?php _e('Order', 'rnr3'); ?></th>
            <th><?php _e('Edit', 'rnr3'); ?></th>
            <th><?php _e('Name', 'rnr3'); ?></th>
            <th><?php _e('Delete', 'rnr3'); ?></th>
          </tr>
        </tfoot>
        -->
        <tbody>
          <?php
            $count = 0;
            if( $pri_sponsor_list != NULL ) {
              foreach( $pri_sponsor_list as $key => $item ) {
                echo '<tr id="list_items_' . $key . '" class="list_item">
                  <td colspan="4" class="no-vertical-padding">
                    <div>
                      <table class="wp-list-table-item">
                        <tr>
                          <td>' . $count . '</td>
                          <td>
                            <span onclick="priEditMarquee(' . $key . ')" style="background: url(' . $updateImageLink . '); background-repeat: no-repeat; border:none; width:16px; height:16px; cursor:pointer; display:block;" title="Edit Marquee"></span>
                          </td>
                          <td>' . stripslashes( $item ) . '</td>
                          <td>
                            <form action="?page=sponsor-options" method="post">
                              <input type="hidden" value="' . $key . '" name="pri-index" />
                              <input class="button-secondary" type="submit" value="Delete" name="pri_delete">
                            </form>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div style="display:none;" class="pri_edit_div" id="pri_edit_' . $key . '_div">
                      <form action="?page=sponsor-options" method="post">
                        <table>
                          <tr>
                            <td>
                              <label for="pri-name-' . $key . '">Name:</label>
                            </td>
                            <td>
                              <input type="text" class="large-text" id="pri-name-' . $key . '" name="pri-name" value="' . stripslashes( $item ). '">
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <label for="logo-' . $key . '">Logo: </label>
                            </td>
                            <td>
                              <input type="button" class="media-button button" name="pri-logo-'.$key.'-btn" value="Choose Image" />';
                              if( isset( $pri_sponsor_logos[$key] ) ) {
                                $prisponsorlogo = $pri_sponsor_logos[$key];
                              } else {
                                $prisponsorlogo = '';
                              }

                              echo '<input type="text" size="75" id="pri-logo-'.$key.'" name="pri-logo" value="'. $prisponsorlogo. '" />
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <label for="pri-url-' . $key . '">URL:</label>
                            </td>
                            <td>';
                            if( isset( $pri_sponsor_url[$key] ) ) {
                              $prisponsorurl = $pri_sponsor_url[$key];
                            } else {
                              $prisponsorurl = '';
                            }

                              echo '<input type="text" class="large-text" id="pri-url-' . $key . '" name="pri-url" value="' . $prisponsorurl . '">
                            </td>
                          </tr>
                        </table>
                        <input type="hidden" value="' . $key . '" name="pri-index" />
                        <input class="button-primary" type="submit" value="Save" name="pri_save" />
                      </form>
                    </div>
                  </td>
                </tr>';
                $count ++;
              }
            } else { echo '<tbody><tr><td colspan="4">No Primary Sponsors!</td></tr></tbody>'; }
          ?>
        </tbody>
      </table>

      <?php /* SECONDARY SPONSORS */ ?>
      <h3><?php _e('Secondary Sponsors', 'rnr3'); ?></h3>
      <form action="?page=sponsor-options" id="add_sponsor_form" name="add_sponsor_form" method="post">
        <label for="add_sponsor_name">Sponsor Name: </label>
        <input type="text" name="add_sponsor_name" id="add_sponsor_name"  />
        <input class="button-primary" type="submit" value="Add Sponsor" name="add_sponsor"/>
      </form>

      <table class="wp-list-table widefat fixed posts sponsor-list">
        <thead>
          <tr>
            <th><?php _e('Order', 'rnr3'); ?></th>
            <th><?php _e('Edit', 'rnr3'); ?></th>
            <th><?php _e('Name', 'rnr3'); ?></th>
            <th><?php _e('Delete', 'rnr3'); ?></th>
          </tr>
        </thead>
        <!--
        <tfoot>
          <tr>
            <th><?php _e('Order', 'rnr3'); ?></th>
            <th><?php _e('Edit', 'rnr3'); ?></th>
            <th><?php _e('Name', 'rnr3'); ?></th>
            <th><?php _e('Delete', 'rnr3'); ?></th>
          </tr>
        </tfoot>
        -->
        <tbody>
          <?php
            $count = 0;
            if( $sponsor_list != NULL ) {
              foreach($sponsor_list as $key=>$item) {
                echo '<tr id="list_items_' . $key . '" class="list_item">
                  <td colspan="4" class="no-vertical-padding">
                    <div>
                      <table class="wp-list-table-item">
                        <tr>
                          <td>' . $count . '</td>
                          <td>
                            <span onclick="editMarquee(' . $key . ')" style="background: url(' . $updateImageLink . '); background-repeat: no-repeat; border:none; width:16px; height:16px; cursor:pointer; display:block;" title="Edit Marquee"></span>
                          </td>
                          <td>' . stripslashes( $item ) . '</td>
                          <td>
                            <form action="?page=sponsor-options" method="post">
                              <input type="hidden" value="' . $key . '" name="index" />
                              <input class="button-secondary" type="submit" value="Delete" name="delete">
                            </form>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div style="display:none;" class="edit_div" id="edit_' . $key . '_div">
                      <form action="?page=sponsor-options" method="post">
                        <table>
                          <tr>
                            <td>
                              <label for="name-' . $key . '">Name:</label>
                            </td>
                            <td>
                              <input type="text" class="large-text" id="name-' . $key . '" name="name" value="' . stripslashes( $item )  . '">
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <label for="logo-' . $key . '">Logo: </label>
                            </td>
                            <td>
                              <input type="button" class="media-button button" name="logo-'.$key.'-btn" value="Choose Image" />';
                              if( isset( $sponsor_logos[$key] ) ) {
                                $sponsorlogo = $sponsor_logos[$key];
                              } else {
                                $sponsorlogo = '';
                              }
                              echo '<input type="text" size="75" id="logo-'.$key.'" name="logo" value="'. $sponsorlogo. '" />
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <label for="url-' . $key . '">URL:</label>
                            </td>';
                            if( isset( $sponsor_url[$key] ) ) {
                              $sponsorurl = $sponsor_url[$key];
                            } else {
                              $sponsorurl = '';
                            }
                            echo '<td>
                              <input type="text" class="large-text" id="url-' . $key . '" name="url" value="' . $sponsorurl . '">
                            </td>
                          </tr>
                        </table>
                        <input type="hidden" value="' . $key . '" name="index" />
                        <input class="button-primary" type="submit" value="Save" name="save" />
                      </form>
                    </div>
                  </td>
                </tr>';
                $count ++;
              }
            } else { echo '<tbody><tr><td colspan="4">No Secondary Sponsors!</td></tr></tbody>'; }
          ?>
        </tbody>
      </table>

    </div>
    <script>
      function priEditMarquee(id){
        var priEditDiv = document.getElementById("pri_edit_"+id+"_div");
        if(priEditDiv.style.display == "block"){
          priEditDiv.style.display = "none";
        }
        else{
          priEditDiv.style.display = "block";
        }
      }
      function editMarquee(id){
        var editDiv = document.getElementById("edit_"+id+"_div");
        if(editDiv.style.display == "block"){
          editDiv.style.display = "none";
        }
        else{
          editDiv.style.display = "block";
        }
      }

    </script>
    <?php echo ob_get_clean();
    /* DEBUG
    print_r($sponsor_list);
    echo '<br>';
    print_r($sponsor_logos);
    echo '<br>';
    print_r($sponsor_url);
    */
  }
}



function sponsor_add_options_link() {
  global $sponsor_settings_page;
  $sponsor_settings_page = add_options_page('Sponsor Carousel', 'Sponsor Carousel', 'upload_files', 'sponsor-options', 'sponsor_options_page');
  add_action('admin_print_styles-' . $sponsor_settings_page, 'sponsor_load_scripts');
}
add_action('admin_menu', 'sponsor_add_options_link');




?>
