jQuery(document).ready(function($) {
	$('.pri-sponsor-list').sortable({
		items: '.list_item',
		opacity: 0.6,
		cursor: 'move',
		axis: 'y',
		update: function() {
			var order = $(this).sortable('serialize') + '&action=pri_sponsor_update_order';
			$.post(ajaxurl, order, function(response) {
				// success
				// alert('Updated order');
			});
		}
	});


	$('.sponsor-list').sortable({
		items: '.list_item',
		opacity: 0.6,
		cursor: 'move',
		axis: 'y',
		update: function() {
			var order = $(this).sortable('serialize') + '&action=sponsor_update_order';
			$.post(ajaxurl, order, function(response) {
				// success
				// alert('Updated order');
			});
		}
	});
});
