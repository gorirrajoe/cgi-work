<?php
  function pri_sponsor_save_order() {
    global $pri_sponsor_list;
    
    $list = $pri_sponsor_list;
    $new_order = $_POST['list_items'];
    $new_list = array();
    
    // update order
    foreach( $new_order as $v ) {
      if(isset($list[$v])) {
        $new_list[$v] = $list[$v];
      }
    
    }

    // save the new order
    update_option('pri_sponsor_list', $new_list);
    die();

  }
  add_action('wp_ajax_pri_sponsor_update_order', 'pri_sponsor_save_order');


  function sponsor_save_order() {
    global $sponsor_list;
    
    $list = $sponsor_list;
    $new_order = $_POST['list_items'];
    $new_list = array();
    
    // update order
    foreach( $new_order as $v ) {
      if(isset($list[$v])) {
        $new_list[$v] = $list[$v];
      }
    
    }

    // save the new order
    update_option('sponsor_list', $new_list);
    die();

  }
  add_action('wp_ajax_sponsor_update_order', 'sponsor_save_order');
?>