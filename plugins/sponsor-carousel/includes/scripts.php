<?php
/* script control */

function sponsor_load_scripts() {
  wp_enqueue_script('jquery-ui-sortable');
  wp_enqueue_script('update-order', plugin_dir_url(__FILE__) . 'js/update-order.js');
  wp_enqueue_style('sponsor-admin', plugin_dir_url(__FILE__) . 'css/admin.css');
  wp_enqueue_media();
  wp_register_script( 'my-uploader', plugin_dir_url(__FILE__) . 'js/uploader.js', array('jquery', 'media-upload', 'thickbox') );
  wp_enqueue_script('my-uploader');

}

?>