<?php
	/*
	Plugin Name: RNR User Management
	Plugin URI: http://runrocknroll.com
	Description: Manage users for Rock 'n' Roll
	Author: Joey Hernandez
	Version: 1.0
	Author URI: http://runrocknroll.com/
	*/

// create the menu and the sub menu
function rnr_users_admin() {
	// Add a new top-level menu since this is a tool all on it's own
	add_menu_page( 'RNR User Management', 'RNR User Management', 'update_plugins', 'rnr_users', 'rnr_users','dashicons-businessman' );
}
add_action('admin_menu', 'rnr_users_admin');


function rnr_users () {
	$all_sites = wp_get_sites();

	echo '<style>
		ol {
			margin:0;
			list-style-type:none;
		}
		.belongto {
			list-style-type:disc;
			margin-left:35px;
		}
		.description {
			margin-left:180px;
		}
		form {
			margin:20px 0;
		}
		fieldset {
			border-top:1px solid #ddd;
			padding:10px 0;
		}
		legend {
			font-weight:bold;
			padding:0 30px 0 0;
		}
		input[type="checkbox"] {
			padding:0;
			vertical-align:text-top;
		}
		td {
			padding:5px 10px;
			border-bottom:1px solid #ddd;
		}
	</style>

	<div class="wrap">
		<h2>Rock \'n\' Roll User Management</h2>
		<p>This plugin will assist in assigning users to the blogs under RNR.</p>
		<p>Currently, when you enable a user to a site, they will be set as an editor.</p>
		<p>If a user is disabled from a site, any of their posts will go to the default runrocknroll account.</p>';

		// search
		if( isset( $_POST['search'] ) && $_POST['search'] ) {
			$search_email = $_POST['rnr_user_email'];
		} elseif( isset( $_POST['update'] ) && $_POST['update'] ) {
			$userid			= $_POST['userid'];
			$search_email	= get_user_by( 'id', $userid );
			$search_email	= $search_email->user_email;
		} else {
			$search_email = '';
		}

		echo '<form name="rnr_users_form" method="post" action="'. $_SERVER['REQUEST_URI']. '">

			<fieldset>
				<legend>Search</legend>
				<ol>
					<li>
						<label for="rnr_user_email">Look for user by email:</label> <input type="text" name="rnr_user_email" id="rnr_user_email" value="'.$search_email.'" size="40">
						<input class="button-primary" type="submit" name="search" value="Search" />

					</li>
				</ol>
			</fieldset>

		</form>';


		// update
		if( isset( $_POST['update'] ) && $_POST['update'] ) {

			$role			= 'editor';
			$reassign		= get_user_by( 'email', 'runrocknroll@competitorgroup.com');
			$reassign_id	= $reassign->ID;

			foreach( $all_sites as $site ) {
				$selected = $_POST['onoff-'.$site['blog_id']];

				if( $selected == 'enabled' ) {

					if( !is_user_member_of_blog( $userid, $site['blog_id'] ) ) {
						add_user_to_blog( $site['blog_id'], $userid, $role );
					}

				} else {

					if( is_user_member_of_blog( $userid, $site['blog_id'] ) ) {
						remove_user_from_blog( $userid, $site['blog_id'], $reassign_id );
					}

				}
			}

			$user				= get_user_by( 'id', $userid );
			$user_blog_array	= get_blogs_of_user( $userid );

			echo '<h3>Modified! "'. $user->user_nicename .'" now belongs to:</h3>';

			if( count( $user_blog_array ) == 0 ) {

				echo '-- none --';

			} else {

				echo '<ul class="belongto">';

					foreach( $user_blog_array as $blog ) {

						echo '<li>'. $blog->blogname .' ['. $blog->userblog_id .']</li>';

					}

				echo '</ul>';

			}

		}


		// search
		if( isset( $_POST['search'] ) && $_POST['search'] ) {

			$user = get_user_by( 'email', $_POST['rnr_user_email'] );

			if( $user != false ) {

				echo '<h3>User found! "'. $user->user_nicename .'"</h3>';

				$user_blog_array = get_blogs_of_user( $user->ID );

				if( count( $user_blog_array ) > 0 ) {
					ksort( $user_blog_array );

					echo '<p>Member of:</p>
					<ul class="belongto">';

						foreach( $user_blog_array as $blog ) {
							echo '<li>'.$blog->blogname.' ['.$blog->userblog_id.']</li>';
						}

					echo '</ul>';
				}

			} else {
				echo '<h3>Womp womp!</h3>
				<p>Email: "'. $_POST['rnr_user_email'] .'" not found. Search again!</p>';
			}

		}

		if( ( isset( $_POST['search'] ) && $_POST['search'] && $user != false) || isset( $_POST['update'] ) && $_POST['update'] ) {

			echo '<h3>Add "'.$user->user_nicename.'" to sites</h3>
			<form name="rnr_userupdate_form" method="post" action="'. $_SERVER['REQUEST_URI']. '">
				<fieldset>
					<legend>Add to site</legend>
					<table>';

						foreach( $all_sites as $single_site ) {

							$site				= get_blog_details( $single_site['blog_id'] );
							$is_member_enabled	= $is_member_disabled = '';

							if( is_user_member_of_blog( $user->ID, $single_site['blog_id'] ) ) {
								$is_member_enabled = 'checked="checked"';
							} else {
								$is_member_disabled = 'checked="checked"';
							}

							echo '<tr>
								<td>
									'.$site->blogname.'
								</td>
								<td>';
									echo '<input id="enabled-'.$site->blog_id.'" type="radio" name="onoff-'.$site->blog_id.'" '.$is_member_enabled.' value="enabled"><label for="enabled-'.$site->blog_id.'">Enabled</label>
								</td>
								<td>
									<input type="radio" id="disabled-'.$site->blog_id.'" name="onoff-'.$site->blog_id.'" '.$is_member_disabled.' value="disabled"><label for="disabled-'.$site->blog_id.'">Disabled</label>
								</td>
							</tr>';

						}
					echo '</table>
				</fieldset>
				<input type="hidden" name="userid" value="'.$user->ID.'">
				<input class="button-primary" type="submit" name="update" value="Update" />

			</form>';

		}

	echo '</div>';

}
