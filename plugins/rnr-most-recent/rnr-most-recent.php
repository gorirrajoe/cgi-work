<?php
/**
 * Plugin Name: RNR Most Recent Posts
 * Plugin URI: http://runrocknroll.com
 * Description: This plugin will show the most recent posts across the RNR3 MU
 * Version: 1.2
 * Author: Joey Hernandez
 * Author URI: http://runrocknroll.com
 *
 */

function wp_recent_across_network( $size = 50, $expires = 7200 ) {
	if( !is_multisite() ) return false;

	if ( ( $recent_across_network = get_transient( 'recent_across_network' ) ) === false ) {
		global $wpdb;

		$base_prefix	= $wpdb->get_blog_prefix( 0 );
		$base_prefix	= str_replace( '1_', '' , $base_prefix );

		if ( false === ( $site_list = get_transient( 'multisite_site_list' ) ) ) {
			global $wpdb;
			$site_list = $wpdb->get_results( 'SELECT * FROM wp_blogs WHERE blog_id NOT IN (37, 38, 39, 40, 41, 42, 43, 44, 47) ORDER BY blog_id' );
			set_transient( 'multisite_site_list', $site_list, $expires );
		}

		$limit	= absint( $size );
		$query	= '';

		// Merge the wp_posts results from all Multisite websites into a single result with MySQL "UNION"
		foreach ( $site_list as $site ) {
			if( $site == $site_list[0] ) {
				$posts_table = $base_prefix . "posts";
			} else {
				$posts_table = $base_prefix . $site->blog_id . "_posts";
			}

			$posts_table	= esc_sql( $posts_table );
			$blogs_table	= esc_sql( $base_prefix . 'blogs' );

			$query .= "
				(
					SELECT $posts_table.ID, $posts_table.post_title, $posts_table.post_date, $blogs_table.blog_id
					FROM $posts_table, $blogs_table
					WHERE $posts_table.post_type = 'post'
					AND $posts_table.post_status = 'publish'
					AND $blogs_table.blog_id = {$site->blog_id}
				)
			";

			if( $site !== end( $site_list ) ) {
				$query .= "UNION";
			} else {
				$query .= "ORDER BY post_date DESC LIMIT 0, $limit";
			}
		}

		$recent_across_network = $wpdb->get_results( $query );

		set_transient( 'recent_across_network', $recent_across_network, 60*60*2 );
	}

	// Format the HTML output
	$html		= '';
	$separator	= ', ';


	foreach ( $recent_across_network as $post ) {
		switch_to_blog( $post->blog_id );
			$blog = get_blog_details( $post->blog_id );

			$blogname		= $blog->blogname;
			$blogurl		= $blog->siteurl;
			$permalink		= get_blog_permalink( $post->blog_id, $post->ID );
			$post_cats		= get_the_category( $post->ID );
			$post_cats_list	= '';

			foreach( $post_cats as $cat ) {
				$post_cats_list .= esc_html( $cat->name ) . $separator;
			}

			$post_cats_list = trim( $post_cats_list, $separator );

			$author_id		= get_post_field( 'post_author', $post->ID );
			$author			= get_the_author_meta( 'user_login', $author_id );
			$user_count		= count_user_posts( $author_id );
			$featured_img	= has_post_thumbnail( $post->ID );

			$featured = $featured_img == 1 ? 'Yes' : '- Nope -';


			$html .= '<tr>
				<td>'. date( 'M j, Y @ g:i a', strtotime( $post->post_date ) ) .'</td>
				<td><a href="'. $blogurl .'" target="_blank">' . $blogname . '</a></td>
				<td><a href="'. get_edit_post_link( $post->ID ) . '" target="_blank">' . $post->ID . '</a></td>
				<td><a href="'.$permalink.'" target="_blank">' . $post->post_title . '</a></td>
				<td>' . $post_cats_list . '</td>
				<td>' . $author . ' ( '. $user_count .' )</td>
				<td>' . $featured . '</td>
			</tr>';

		restore_current_blog();
	}

	return $html;
}


function rnr3_most_recent_page() { ?>
	<div class="wrap">
		<h2>RNR3.0 Most Recent Posts</h2>
		<p>Shows the most recent 50 posts. Results cached by 2 hours.</p>

		<table class="wp-list-table widefat fixed posts">
			<thead>
				<tr>
					<th><?php _e('Publish Date', 'rnr3'); ?></th>
					<th><?php _e('Blog', 'rnr3'); ?></th>
					<th><?php _e('Post ID', 'rnr3'); ?></th>
					<th><?php _e('Post Title/URL', 'rnr3'); ?></th>
					<th><?php _e('Categories', 'rnr3'); ?></th>
					<th><?php _e('Author (Post Count)', 'rnr3'); ?></th>
					<th><?php _e('Has Featured Image?', 'rnr3'); ?></th>
				</tr>
			</thead>

			<tbody>

				<?php // Display recent posts across the entire network
					$recent_network_posts = wp_recent_across_network();
					if( $recent_network_posts ) {
						echo $recent_network_posts;
					}

				?>

			</tbody>

			<tfoot>
				<tr>
					<th><?php _e('Publish Date', 'rnr3'); ?></th>
					<th><?php _e('Blog', 'rnr3'); ?></th>
					<th><?php _e('Post ID', 'rnr3'); ?></th>
					<th><?php _e('Post Title/URL', 'rnr3'); ?></th>
					<th><?php _e('Categories', 'rnr3'); ?></th>
					<th><?php _e('Author (Post Count)', 'rnr3'); ?></th>
					<th><?php _e('Has Featured Image?', 'rnr3'); ?></th>
				</tr>
			</tfoot>

		</table>

	</div>


<?php }


function rnr3_most_recent_add_options_link() {
	global $rnr3mu_settings_page;

	$rnr3mu_settings_page = add_options_page(
		'RNR3 Most Recent',
		'RNR3 Most Recent',
		'upload_files',
		'rnr3-most-recent',
		'rnr3_most_recent_page'
	);
}
add_action('admin_menu', 'rnr3_most_recent_add_options_link');

?>
