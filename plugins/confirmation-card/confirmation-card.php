<?php
/*
Plugin Name: RNR Confirmation Card
Plugin URI: http://www.competitor.com
Description: Manage confirmation card for RNR
Author: Joey Hernandez
Version: 1.4
Author URI: http://competitor.com/
*/

// actaually register the function to create menus and build pages.
add_action('admin_menu', 'confirmation_card_admin');

// create the menu and the sub menu
function confirmation_card_admin() {
	// Add a new top-level menu since this is a tool all on it's own
	add_menu_page( 'RNR Confirmation Card Manager', 'RNR ConfCard ', 'upload_files', 'confirmation_card_manager', 'confirmation_card_manager','' );
}

// For file uploads
function confirmation_card_admin_scripts() {
	wp_enqueue_script('jquery');
	wp_deregister_script( 'jquery-ui-core' );
	wp_register_script('jquery-ui-core', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js');
	wp_enqueue_script('jquery-ui-core');
}


if (isset($_GET['page']) && $_GET['page'] == 'confirmation_card_manager') {
	add_action('admin_print_scripts', 'confirmation_card_admin_scripts');
}

function confirmation_card_manager () {
	global $wpdb;
	echo '<link type="text/css" rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" />' . "\n";
	echo '
		<style type="text/css">
			.feature_group {
				border-bottom:1px solid #ccc;
				padding-bottom:10px;
				margin-bottom:10px;
				width:98%;
			}
			.feature_group label {
				width:120px;
				float:left;
			}
			.event_edit_form {
				border:1px solid #ccc;
				padding:10px;
				width:50%;
			}
			.event_edit {
				margin-left:0;
			}
			.event_edit li {
				list-style-type:none;
			}
			.event_edit label {
				width:100px;
				float:left;
				padding-top:5px;
			}
			.description {
				margin-left:100px;
			}
			.runnerTotals td {
				padding:5px 10px;
				border-bottom:1px solid #ddd;
			}
			.runnerTotals .runnerCount {
				text-align:right;
			}
			.runnerList li {
				float:left;
				width:150px;
				margin:0 20px 20px 0;
				border:1px solid #ddd;
				border-radius:8px;
				padding:5px;
			}
			.clear {
				clear:both;
			}
			.radio_event_selection li {
				list-style-type:none;
				display:inline-block;
			}
				.radio_event_selection input {
					float:left;
					margin-right:5px;
				}
		</style>
	';
	// Upload File
	if(isset($_POST['upload_file'])){
		if ($_FILES["uploadedfile"]["error"] > 0) {
			echo "
				<script type='text/javascript'>
					window.alert('FAILED to upload file.');
				</script>
			";
		} elseif ($_FILES["uploadedfile"]["type"] == "text/plain") {
			$file_name = $_FILES['uploadedfile']['tmp_name'];
			$lines = file($file_name);

			foreach ($lines as $line) {
				$line_bits = explode("\t", $line);
				if ($line_bits[0] != "last_name") {
					$wpdb->insert(
						'wp_confirmation_card_data',
						array(
							'last_name'		=> $line_bits[0],
							'first_name'	=> $line_bits[1],
							'address_1'		=> $line_bits[2],
							'address_2'		=> $line_bits[3],
							'city'			=> $line_bits[4],
							'state'			=> $line_bits[5],
							'zip'			=> $line_bits[6],
							'country'		=> $line_bits[7],
							'gender'		=> $line_bits[8],
							'birthdate'		=> $line_bits[9],
							'age'			=> $line_bits[10],
							'predicted'		=> $line_bits[11],
							'bib_number'	=> $line_bits[12],
							'corral'		=> $line_bits[13],
							'event_code'	=> $line_bits[14],
							'division'		=> $line_bits[15],
							'email'			=> $line_bits[16]
						)
					);
				}
			}
			// success summary message
			echo "
				<script type='text/javascript'>
					window.alert('Successfully uploaded ".$_FILES["uploadedfile"]["name"]."!');
				</script>
			";
		} else {
			// fail summary message
			echo "
				<script type='text/javascript'>
					window.alert('FAILED to upload ".$_FILES["uploadedfile"]["name"]."!');
				</script>
			";
		}
	}
	elseif(isset($_POST['runner_search'])){
		if ($_POST['event_code'] != -1) {
			if(isset($_POST['first_name'])){
				$first_name = $_POST['first_name'];
			} else {
				$first_name = "";
			}
			if(isset($_POST['last_name'])){
				$last_name = $_POST['last_name'];
			} else {
				$last_name = "";
			}
			if(isset($_POST['event_code'])){
				$event_code = $_POST['event_code'];
			} else {
				$event_code = "";
			}
			if(isset($_POST['dob_month'])){
				$dob_month = $_POST['dob_month'];
			} else {
				$dob_month = "";
			}
			if(isset($_POST['dob_day'])){
				$dob_day = $_POST['dob_day'];
			} else {
				$dob_day = "";
			}
			if(isset($_POST['dob_year'])){
				$dob_year = $_POST['dob_year'];
			} else {
				$dob_year = "";
			}
			$birthday = $dob_month . "/" . $dob_day . "/" . $dob_year;
			if ($birthday == "-1/-1/-1") {
				$birthday = "";
			}
			// echo $first_name . " " . $last_name . "<br />" . $birthday . "<br />" . $event_code;
		} else {
			echo "
				<script type='text/javascript'>
					window.alert('Please select an Event Code.');
				</script>
			";
		}
	}
	elseif(isset($_POST['delete_event'])){
		$delete_event_code = trim($_POST['delete_event_code']);
		// echo $delete_event_code;
		$wpdb->query(
			"DELETE FROM wp_confirmation_card_data
			WHERE event_code LIKE '%$delete_event_code%'"
		);
	}
	elseif(isset($_POST['delete_all'])){
		$wpdb->query(
			"TRUNCATE TABLE wp_confirmation_card_data"
		);
		echo "
			<script type='text/javascript'>
				window.alert('Successfully DELETED all entries in table wp_confirmation_card');
			</script>
		";
	}

	echo '<h1>RNR Confirmation Card Manager</h1>';
	echo '
	<div class="feature_group">
		<h2>Upload Event File</h2>
		<div class="wrap">
			<div class="updated">
				<p>Browse for the file to upload. Note that the file must be a <strong>TAB-DELIMITED TEXT FILE</strong>.</p>
				<p>Clicking Upload File will add to the database. <strong>If you are replacing records, make sure you click DELETE RECORDS below to clear out that event\'s records</strong>.</p>
				<p>If the upload keeps timing out, split the excel in half and upload each file.</p>
				<p><strong>IMPORTANT:</strong> Excel sheet columns: last_name, first_name, address1, address2, city, state, zip, country, gender, dob/yob, age, predicted, bib#, corral, event, division, email</p>
			</div>
		</div>
		<form enctype="multipart/form-data" action="?page=confirmation_card_manager" method="POST">
			<label>Choose a file to upload:</label> <input name="uploadedfile" type="file" /><br />';
			?>
			<input type="submit" name="upload_file" value="Upload File" onclick="return checkUpload()" />

		<?php echo '</form>
	</div>
	';
	$myrows = $wpdb->get_results( "SELECT event_code FROM wp_confirmation_card_data ORDER BY event_code ASC" );
	if ($myrows != NULL) {
		echo '
		<div class="feature_group">
			<h2>Runner Search</h2>
			<form action="?page=confirmation_card_manager" method="post">
				<label for="first_name">First Name: </label>
				<input id="first_name" type="text" name="first_name" /><br />
				<label for="last_name">Last Name: </label>
				<input type="text" id="last_name" name="last_name" /><br />
				<label for="birthdate">Birthdate</label>';
				echo "<select name=\"dob_month\" type=\"text\" id=\"dob_month\">\n";
					echo "<option value=\"-1\">M</option>\n";
					for ($i = 1; $i <= 12; $i++) {
						echo "<option value = $i ";
						if ($dob_month == $i) {
							echo "selected ";
						};
						echo ">$i</option>\n";
					}
				echo "</select>\n";
				echo "<select name=\"dob_day\" type=\"text\" id=\"dob_day\">\n
					<option value=\"-1\">D</option>\n";
					for ($i = 1; $i <= 31; $i++) {
						echo "<option value = $i ";
						if ($dob_day == $i) {
							echo "selected ";
						};
						echo ">$i</option>\n";
					}
				echo "</select>\n";
				echo "<select name=\"dob_year\" type=\"text\" id=\"dob_year\">\n";
					echo "<option value = \"-1\">YYYY</option>\n";
					for ($i = date("Y"); $i >= 1920; $i--) {
						echo "<option value = $i ";
						if ( $dob_year == $i ) {
							echo "selected ";
						} ;
						echo ">$i</option>\n";
					}
				echo "</select>\n";
				echo '<br />
				<label for="event_code">Event Code: </label>
				<select id="event_code" name="event_code">
					<option value="-1">--View All--</option>';
						$myslugs = $wpdb->get_results( "SELECT DISTINCT event_code FROM wp_confirmation_card_data ORDER BY event_code" );
						foreach($myslugs as $slug) {
							echo '<option value="'.$slug->event_code.'">'.$slug->event_code.'</option>';
						}
				echo '</select><br />
				';
				echo '<input type="submit" value="Search For Runner" name="runner_search"/>
			</form>';
			if ($_POST['event_code'] != -1 && isset($_POST['runner_search'])) {
				$query = "SELECT * FROM wp_confirmation_card_data WHERE event_code LIKE '%$event_code%'";
				if ($first_name) {
					$query .= " AND first_name LIKE '%$first_name%'";
				}
				if ($last_name) {
					$query .= " AND last_name LIKE '%$last_name%'";
				}
				if ($birthday) {
					$query .= " AND birthdate = '$birthday'";
				}
				$query .= "ORDER BY last_name ASC LIMIT 20";
				// echo $query;
				$runners = $wpdb->get_results($query);

				if ($runners != NULL) {
					echo '<h3>Runner(s) Found!</h3>';
					echo '<ul class="runnerList">';
					foreach ($runners as $runner) {
						echo '<li>' . $runner->first_name . " " . $runner->last_name . '<br />' .
							$runner->bib_number."<br />" .
							$runner->birthdate . "<br />" .
							$runner->event_code . "<br />" .
							$runner->email . '<br />' .
							$runner->division;
						echo '</li>';
					}
					echo '</ul>';
					echo '<div class="clear"></div>';
				} else {
					echo '<h3>Runner NOT Found</h3>';
				}
			}
		echo '</div>
		';
		echo '<h2>RNR Totals</h2>';
		$events = array();
		foreach($myrows as $row){
			$events[] = $row->event_code;
		}
		$events = array_unique($events);
		echo '<table cellspacing="0" cellpadding="0" border="0" class="runnerTotals">
			<tr>
				<th>Event Code</th>
				<th>Count</th>
				<th>Last Updated</th>
			</tr>
		';
		foreach($events as $unique_event){
			$eventcount = $wpdb->get_var( "SELECT COUNT(*) FROM wp_confirmation_card_data WHERE event_code = '$unique_event'" );
			$eventcount_format = number_format($eventcount, 0, '.', ',');
			$last_update = $wpdb->get_var("SELECT date_modified FROM wp_confirmation_card_data WHERE event_code = '$unique_event'");
			$last_update_format = date('n/j/Y @g:i a', strtotime($last_update));
			if ($last_update_format == "1/1/1970 @12:00 am") {
				$last_update_format = "";
			}
			echo '
				<tr>
					<td>'.$unique_event.'</td>
					<td class="runnerCount">'.$eventcount_format.'</td>
					<td>'.$last_update_format.'</td>
					<td>
						<form action="?page=confirmation_card_manager" method="post">
							<input type="hidden" value="'.$unique_event.'" name="delete_event_code"/>
							<input type="submit" value="Delete Records" name="delete_event" onclick="return checkDelete()"/>
							</form>
					</td>
				</tr>';
		}
		$record_count = $wpdb->get_var("SELECT COUNT(*) FROM wp_confirmation_card_data");
		$record_count_format = number_format($record_count, 0, '.', ',');
		echo '
			<tr>
				<td colspan="3">
					Overall Number of Records: '.$record_count_format.'
				</td>
				<td>
					<form style="display:none;" action="?page=confirmation_card_manager" method="post">
						<input type="submit" value="Delete ALL Records" name="delete_all" onclick="return checkDeleteAll()"/>
					</form>
				</td>
			</tr>';
		echo '</table>';
	} else {
		echo '<p>There are no events uploaded.</p>';
	}

	echo '
		<script>
			function checkDelete(){
				var r=confirm("Click Ok to continue DELETING the records in this event.");
				if (r==true){
					return true;
				}
				return false;
			}
			function checkDeleteAll(){
				var r=confirm("Make sure you are ABSOLUTELY POSITIVE that you want to DELETE every record in this table.");
				if (r==true){
					return true;
				}
				return false;
			}
			function checkUpload(){
				var r=confirm("Click Ok to continue UPLOADING to this event.");
				if (r==true){
					return true;
				}
				return false;
			}
		</script>
	';
}

