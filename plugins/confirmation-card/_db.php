<?php
/*
This file will create the proper table in the database when the plugin is activated.
It will also delete the table if the plugin is ever deactivated.
*/

global $confirmation_card_version;
$confirmation_card_version = "1.3";

// create table
function confirmation_card_db_install () {
  global $wpdb;
  $installed_ver = get_option( "confirmation_card_version" );
  global $confirmation_card_version;

  if( $installed_ver != $confirmation_card_version ) {

    //check to see if wp_confirmation_card_data table exists.
    //if($wpdb->get_var("show tables like 'wp_confirmation_card_data'") != 'wp_confirmation_card_data') {

      // SQL to create tables
      $query = "CREATE TABLE wp_confirmation_card_data (
        last_name varchar(30),
        first_name varchar(30),
        address_1 varchar(255),
        address_2 varchar(255),
        city varchar(30),
        state varchar(3),
        zip varchar(10),
        country varchar(255),
        gender char(1),
        birthdate varchar(10),
        age char(2),
        predicted varchar(20),
        bib_number varchar(6),
        corral varchar(10),
        event_code varchar(8)
      );
      ALTER TABLE wp_confirmation_card_data
        ADD date_modified TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
      ";
      require_once(ABSPATH . 'wp-admin/upgrade-functions.php');
      dbDelta($query);
      update_option("confirmation_card_version", $confirmation_card_version);
    //}
  }
}

// delete table
function confirmation_card_db_uninstall () {
  /*
  global $wpdb;
  // Drop tables if you uninstall plugin. This will remove it from site.
  $query  = "DROP TABLE IF EXISTS wp_confirmation_card_data;";
  $wpdb->query($query);
  */
}



?>